package net.ihe.gazelle.gitb;

import com.gitb.core.v1.ActorConfiguration;
import com.gitb.core.v1.AnyContent;
import com.gitb.core.v1.ValueEmbeddingEnumeration;
import com.gitb.ms.v1.SendResponse;
import com.gitb.tr.v1.BAR;
import com.gitb.tr.v1.TestResultType;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;

import static net.ihe.gazelle.gitb.GITBConstant.PDQM_PDC;

public class GITBMapper {


    /**
     * Create the {@link AnyContent} object corresponding to a SearchCriteria for a single id criterion
     *
     * @param id value of the ID to search
     * @return the {@link AnyContent} object corresponding to the search criteria
     */
    AnyContent getSearchCriteriaAnyContent(String id) {

        AnyContent searchCriteriaAnyContent = createAnyContentSimple("PatientSearchCriteria", "object", null, null);

        AnyContent logicalOperatorAnyContent = createAnyContentSimple("logicalOperator", "string", "AND", ValueEmbeddingEnumeration.STRING);
        searchCriteriaAnyContent.getItem().add(logicalOperatorAnyContent);

        AnyContent searchCriterionAnyContent = createAnyContentSimple("searchCriterion", "list", null, null);

        searchCriterionAnyContent.getItem().add(createAnyContentStringCriterion("UUID", id, true));

        searchCriteriaAnyContent.getItem().add(searchCriterionAnyContent);

        return searchCriteriaAnyContent;
    }


    /**
     * Create the {@link AnyContent} object corresponding to a SearchCriteria for a single id criterion
     *
     * @param id value of the ID to search
     * @return the {@link AnyContent} object corresponding to the search criteria
     */
    AnyContent getSearchCriteriaAnyContentRetrieve(String id) {
        AnyContent searchCriteriaAnyContent = createAnyContentSimple("PatientSearchCriteria", "object", null, null);
        AnyContent searchCriterionAnyContent = createAnyContentSimple("searchCriterion", "list", null, null);
        searchCriterionAnyContent.getItem().add(createAnyContentSimpleString("UUID", id));
        searchCriteriaAnyContent.getItem().add(searchCriterionAnyContent);
        return searchCriteriaAnyContent;
    }


    /**
     * Get the actor configuration with the endPoint
     *
     * @param endpoint The endPoint as String
     * @return the actor configuration
     */
    ActorConfiguration getEndpointAnyContent(String endpoint) {
        ActorConfiguration actorConfiguration = new ActorConfiguration();
        actorConfiguration.setEndpoint(endpoint);
        actorConfiguration.setActor(PDQM_PDC);
        return actorConfiguration;
    }

    /**
     * Create an {@link AnyContent} object for a String criterion
     *
     * @param key             key of the criterion
     * @param value           value of the criterion
     * @param isCaseSensitive option on case sensitivity for the criterion
     * @return the {@link AnyContent} object corresponding to the string criterion
     */
    private AnyContent createAnyContentStringCriterion(String key, String value, boolean isCaseSensitive) {

        AnyContent idCriterionAnyContent = createAnyContentSimple(":net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion" +
                ".StringSearchCriterion", "object", null, null);

        AnyContent idOptionsAnyContent = createAnyContentSimple("options", "object", null, null);
        AnyContent caseSensitiveAnyContent = createAnyContentSimple("isCaseSensitive", "boolean", Boolean.toString(isCaseSensitive),
                ValueEmbeddingEnumeration.STRING);
        idOptionsAnyContent.getItem().add(caseSensitiveAnyContent);
        idCriterionAnyContent.getItem().add(idOptionsAnyContent);

        AnyContent operatorAnyContent = createAnyContentSimple("operator", "string", "EXACT", ValueEmbeddingEnumeration.STRING);
        idCriterionAnyContent.getItem().add(operatorAnyContent);

        AnyContent valueAnyContent = createAnyContentSimple("value", "string", value, ValueEmbeddingEnumeration.STRING);
        idCriterionAnyContent.getItem().add(valueAnyContent);

        AnyContent keyAnyContent = createAnyContentSimple("key:net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey", "string"
                , key, ValueEmbeddingEnumeration.STRING);
        idCriterionAnyContent.getItem().add(keyAnyContent);

        return idCriterionAnyContent;
    }


    /**
     * Create a AnyContent object value based on the provided parameters.
     *
     * @param name            The name of the value.
     * @param value           The value itself.
     * @param embeddingMethod The way in which this value is to be considered.
     * @return The value.
     */
    private AnyContent createAnyContentSimple(String name, String type, String value, ValueEmbeddingEnumeration embeddingMethod) {
        AnyContent input = new AnyContent();
        input.setName(name);
        if (value != null) {
            input.setValue(value);
        }
        if (type != null) {
            input.setType(type);
        } else {
            throw new IllegalArgumentException("Cannot create AnyContent Element without a type !");
        }
        if (embeddingMethod != null) {
            input.setEmbeddingMethod(embeddingMethod);
        }
        return input;
    }

    /**
     * Create a AnyContent object with the string type
     *
     * @param name  The name of the value.
     * @param value The value itself.
     * @return The AnyContent
     */
    public AnyContent createAnyContentSimpleString(String name, String value) {
        return createAnyContentSimple(name, "string", value, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Get the result of the response
     *
     * @param sendResponse The message response
     * @return The result of the report
     */
    TestResultType getSendResponseResultValue(SendResponse sendResponse) {
        return sendResponse.getReport().getResult();
    }

    /**
     * Get the record id as String
     *
     * @param sendResponse The message response
     * @return The record id
     */
    String getRecordIdFromSendResponse(SendResponse sendResponse) {
        return sendResponse.getReport().getContext().getValue();
    }

    /**
     * Throw the exception reported in the description
     *
     * @param sendResponse The message response
     * @throws SimulationException
     */
    void throwExceptionForErrorInBARFromSendResponse(SendResponse sendResponse) throws SimulationException {
        BAR bar;
        try {
            bar = (BAR) sendResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue();
        } catch (Exception e) {
            throw new SimulationException("No information reported");
        }
        throw new SimulationException(bar.getDescription());
    }

}
