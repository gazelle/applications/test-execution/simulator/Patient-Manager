package net.ihe.gazelle.gitb;

import com.gitb.ms.MessagingService;
import com.gitb.ms.MessagingServiceService;
import com.gitb.ms.v1.Void;
import com.gitb.ms.v1.*;

import javax.xml.namespace.QName;
import java.net.URL;

/**
 * Client for GITB messaging service
 *
 * @author Wylem BARS
 */
public class GITBMessagingClient implements MessagingService {

    /**
     * the messaging service to point to
     */
    private MessagingService service;

    /**
     * Client constructor with associated server url
     *
     * @param endPoint    the url of the server implementing the processing service
     * @param serviceName name of the ProcessingServiceService service
     * @param portName    name of the port of the ProcessingServiceService
     */
    public GITBMessagingClient(URL endPoint, String serviceName, String portName) {
        MessagingServiceService serviceHandler = new MessagingServiceService(endPoint,
                new QName("http://www.gitb.com/ms/v1/", serviceName));
        service = serviceHandler.getPort(new QName("http://www.gitb.com/ms/v1/", portName), MessagingService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        return service.getModuleDefinition(aVoid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InitiateResponse initiate(InitiateRequest initiateRequest) {
        return service.initiate(initiateRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void receive(ReceiveRequest receiveRequest) {
        return service.receive(receiveRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SendResponse send(SendRequest sendRequest) {
        return service.send(sendRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void beginTransaction(BeginTransactionRequest beginTransactionRequest) {
        return service.beginTransaction(beginTransactionRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest basicRequest) {
        return service.endTransaction(basicRequest);
    }

    /**
     * {@inheritDoc}
     */
    // We suppress warning on "The signature of "finalize()" should match that of "Object.finalize()"
    // we need to follow the interface but it's dangerous
    @java.lang.SuppressWarnings("java:S1175")
    @Override
    public Void finalize(FinalizeRequest finalizeRequest) {
        return service.finalize(finalizeRequest);
    }
}
