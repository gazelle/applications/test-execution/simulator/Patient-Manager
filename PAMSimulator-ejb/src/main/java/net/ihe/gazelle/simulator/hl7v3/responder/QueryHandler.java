package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsresp.RecordingSOAPHandler;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class QueryHandler {

    protected TransactionInstance instance;
    private MessageContext messageContext;

    private static Logger log = LoggerFactory.getLogger(QueryHandler.class);

    /**
     * <p>Constructor for QueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     * @param messageContext   a {@link MessageContext} object.
     */
    public QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                        HttpServletRequest servletRequest, MessageContext messageContext) {
        this.messageContext = messageContext;
        this.instance = new TransactionInstance();
        instance.setTransaction(inTransaction);
        instance.setDomain(inDomain);
        instance.setSimulatedActor(inSimulatedActor);
        instance.setTimestamp(new Date());
        instance.getRequest().setIssuingActor(inSutActor);
        if (this.messageContext != null){
            instance.getRequest().setPayload((byte[]) messageContext.get(RecordingSOAPHandler.PAYLOAD));
        }
        instance.getResponse().setIssuingActor(inSimulatedActor);
        instance.getResponse().setIssuer("PatientManager");
        instance.getRequest().setIssuer(servletRequest.getRemoteHost());
        instance.getRequest().setIssuerIpAddress(servletRequest.getRemoteAddr());
        instance.setStandard(EStandard.HL7V3);
    }

    /**
     * <p>Constructor for QueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     */
    public QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                        HttpServletRequest servletRequest) {
        this.instance = new TransactionInstance();
        instance.setTransaction(inTransaction);
        instance.setDomain(inDomain);
        instance.setSimulatedActor(inSimulatedActor);
        instance.setTimestamp(new Date());
        instance.getRequest().setIssuingActor(inSutActor);
        instance.getResponse().setIssuingActor(inSimulatedActor);
        instance.getResponse().setIssuer("PatientManager");
        instance.getRequest().setIssuer(servletRequest.getRemoteHost());
        instance.getRequest().setIssuerIpAddress(servletRequest.getRemoteAddr());
        instance.setStandard(EStandard.HL7V3);
    }

    public QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                        String issuer, String issuerIP) {
        this.instance = new TransactionInstance();
        instance.setTransaction(inTransaction);
        instance.setDomain(inDomain);
        instance.setSimulatedActor(inSimulatedActor);
        instance.setTimestamp(new Date());
        instance.getRequest().setIssuingActor(inSutActor);
        instance.getResponse().setIssuingActor(inSimulatedActor);
        instance.getResponse().setIssuer("PatientManager");
        instance.getRequest().setIssuer(issuer);
        instance.getRequest().setIssuerIpAddress(issuerIP);
        instance.setStandard(EStandard.HL7V3);
    }

    /**
     * <p>sendResponse.</p>
     *
     * @param responseType a {@link java.lang.Class} object.
     * @param response     a T object.
     * @param <T>          a T object.
     * @return a T object.
     */
    public <T> T sendResponse(Class<T> responseType, T response) {
        if (response != null) {
            ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(responseType, responseStream, response);
                instance.getResponse().setContent(responseStream.toByteArray());
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            instance.getResponse().setType(null);
        }
        instance = instance.save(EntityManagerService.provideEntityManager());
        if (this.messageContext != null){
            this.messageContext.put(RecordingSOAPHandler.TRANSACTION_ID, instance.getId());
            this.messageContext.setScope(RecordingSOAPHandler.TRANSACTION_ID, MessageContext.Scope.APPLICATION);
        }
        return response;
    }

    /**
     * <p>getTransactionInstance.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getTransactionInstance() {
        return instance;
    }
}
