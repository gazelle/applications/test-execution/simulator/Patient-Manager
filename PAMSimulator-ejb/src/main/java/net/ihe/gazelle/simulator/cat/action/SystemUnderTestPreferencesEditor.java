package net.ihe.gazelle.simulator.cat.action;

import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerPages;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>SystemUnderTestPreferencesEditor class.</p>
 *
 * @author abe
 * @version 1.0: 07/12/17
 */

@Name("sutPreferencesEditor")
@Scope(ScopeType.PAGE)
public class SystemUnderTestPreferencesEditor implements Serializable {

    public static final String SUT_KEY_NAME = "sut";
    private SystemUnderTestPreferences selectedPreferences;
    private SystemConfiguration selectedConfiguration;
    private boolean displayOnlyConnectathonAuthorities;

    @Create
    public void initializePage(){
        Map<String, String> urlsParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlsParams.containsKey(SUT_KEY_NAME)){
            String sutIdAsString = urlsParams.get(SUT_KEY_NAME);
            try{
                Integer sutPrefId = Integer.decode(sutIdAsString);
                selectedPreferences = SystemUnderTestPreferencesDAO.getPreferencesById(sutPrefId);
                if (selectedPreferences == null){
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, sutIdAsString + " is not a valid identifier");
                }
            }catch (NumberFormatException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, sutIdAsString + " is not a valid identifier");
                selectedPreferences = null;
            }
        } else {
            selectedPreferences = null;
            selectedConfiguration = null;
        }
    }

    public List<SystemConfiguration> getSystemConfigurationWithoutPreferences(){
        return SystemUnderTestPreferencesDAO.getSUTWithoutPreferences();
    }

    public boolean isDisplayOnlyConnectathonAuthorities() {
        return displayOnlyConnectathonAuthorities;
    }

    public void setDisplayOnlyConnectathonAuthorities(boolean displayOnlyConnectathonAuthorities) {
        this.displayOnlyConnectathonAuthorities = displayOnlyConnectathonAuthorities;
    }

    public SystemConfiguration getSelectedConfiguration() {
        return selectedConfiguration;
    }

    public void setSelectedConfiguration(SystemConfiguration selectedConfiguration) {
        this.selectedConfiguration = selectedConfiguration;
    }

    public SystemUnderTestPreferences getSelectedPreferences() {
        return selectedPreferences;
    }

    public void setSelectedPreferences(SystemUnderTestPreferences selectedPreferences) {
        this.selectedPreferences = selectedPreferences;
    }

    public List<HierarchicDesignator> getAvailableAssigningAuthorities(){
        if (displayOnlyConnectathonAuthorities){
            return HierarchicDesignatorDAO.getAssigningAuthoritiesForConnectathon();
        } else {
            return HierarchicDesignatorDAO.getAllAssigningAuthoritiesForUsage(DesignatorType.PATIENT_ID);
        }
    }


    public void createPreferencesForSelectedSystem(){
        if (selectedConfiguration != null){
            // PAM-549
            selectedPreferences = SystemUnderTestPreferencesDAO.getPreferencesForSUT(selectedConfiguration);
            if (selectedPreferences == null) {
                selectedPreferences = new SystemUnderTestPreferences(selectedConfiguration);
                saveChanges();
            }
        }
        selectedConfiguration = null;
    }

    public String backToList(){
        return PatientManagerPages.CAT_AA.getLink();
    }

    public void saveChanges(){
        selectedPreferences = SystemUnderTestPreferencesDAO.save(selectedPreferences);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preferences have been saved");
    }

    public boolean isConnectathonFeatureActivated(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.CONNECTATHON);
    }
}
