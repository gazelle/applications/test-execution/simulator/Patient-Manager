package net.ihe.gazelle.simulator.pdq.util;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.messages.PRPAIN201306UV02Parser;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import net.ihe.gazelle.simulator.testdata.action.ATestDataManager;
import net.ihe.gazelle.simulator.testdata.model.PatientTestData;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * <p>Abstract AbstractPDQPDC class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractPDQPDC extends ATestDataManager {


    private static Logger log = LoggerFactory.getLogger(AbstractPDQPDC.class);
    private static final String ITI = "ITI";
    private static final String EPD = "EPD";
    protected Patient patientCriteria;
    protected HashMap<Integer, List<Patient>> receivedPatients;
    protected Integer selectedPage;
    protected Patient selectedPatient;
    protected Transaction selectedTransaction;
    protected Integer limitValue;
    protected boolean limit;
    protected boolean moreResultsAvailable;
    protected List<AssigningAuthority> domainsReturned;
    protected AssigningAuthority currentDomainReturned;
    protected boolean displayCriteriaForm;
    protected Integer pageNumber;
    protected Boolean cancellation;
    protected String simulatorDomainKeyword;
    private static final Integer INCREMENT = 1;

    /**
     * Use to hide/show fields depending on the domain (here EPD is for ehealthsuisse)
     *
     * @return a boolean.
     */
    public boolean isEPDDomain(){
        return EPD.equals(simulatorDomainKeyword);
    }

    /**
     * <p>initPage.</p>
     */
    public void initPage(){
        simulatorDomainKeyword = PreferenceService.getString("default_pdq_domain");
        if (simulatorDomainKeyword == null){
            simulatorDomainKeyword = ITI;
        }
    }

    /**
     * <p>listReceivedPatients.</p>
     *
     * @param parser a {@link net.ihe.gazelle.simulator.hl7v3.messages.PRPAIN201306UV02Parser} object.
     * @param patients a {@link java.util.List} object.
     */
    protected void listReceivedPatients(PRPAIN201306UV02Parser parser, List<Patient> patients) {
        if (patients != null) {
            if (receivedPatients == null) {
                receivedPatients = new HashMap<Integer, List<Patient>>();
            }
            receivedPatients.put(pageNumber, patients);
            selectedPage = pageNumber;
            moreResultsAvailable = parser.isMoreResultsAvailable();
        }
    }

    /**
     * allows the user to store a patient retrieved using a PDQ query for a future use
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void storePatient(Patient inPatient) {
        inPatient.setSimulatedActor(Actor.findActorWithKeyword("PDC"));
        inPatient.setCreationDate(new Date());
        if (Identity.instance().isLoggedIn()) {
            inPatient.setCreator(Identity.instance().getCredentials().getUsername());
        }
        inPatient.setStillActive(true);
        try {
            inPatient.setEncounters(null);
            List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(inPatient
                    .getPatientIdentifiers());
            inPatient.setPatientIdentifiers(mergedIdentifiers);
            inPatient.savePatient(EntityManagerService.provideEntityManager());
            FacesMessages.instance().add(StatusMessage.Severity.INFO,"Patient successfully stored in database");
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "This patient cannot be stored in database for the following reason: " + e.getMessage());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * <p>addPatientIdentifierCriteria.</p>
     */
    public void addPatientIdentifierCriteria(){
        PatientIdentifier currentIdentifier = new PatientIdentifier();
        HierarchicDesignator hd = new HierarchicDesignator();
        hd.setUsage(DesignatorType.PATIENT_ID);
        currentIdentifier.setDomain(hd);
        if (patientCriteria.getPatientIdentifiers() == null){
            patientCriteria.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        }
        patientCriteria.getPatientIdentifiers().add(currentIdentifier);
    }

    /**
     * <p>addAddressCriteria.</p>
     */
    public void addAddressCriteria() {
        patientCriteria.addPatientAddress(new PatientAddress(patientCriteria));
    }

    /**
     * <p>addPhoneNumberCriteria.</p>
     */
    public void addPhoneNumberCriteria() {
        patientCriteria.addPhoneNumber(new PatientPhoneNumber(patientCriteria));
    }

    /**
     * <p>addAssigningAuthorityToDomains.</p>
     */
    public void addAssigningAuthorityToDomains() {
        if ((currentDomainReturned != null) && !currentDomainReturned.isEmpty()) {
            domainsReturned.add(currentDomainReturned);
            currentDomainReturned = new AssigningAuthority();
        }
    }

    /**
     * <p>removeAssigningAuthorityFromDomains.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
     */
    public void removeAssigningAuthorityFromDomains(AssigningAuthority domain) {
        if (domainsReturned.contains(domain)) {
            domainsReturned.remove(domain);
        }
    }

    /**
     * <p>sendMessage.</p>
     */
    public abstract void sendMessage();

    /**
     * <p>getNextResults.</p>
     */
    public abstract void getNextResults();

    /**
     * <p>cancelQuery.</p>
     */
    public abstract void cancelQuery();

    /**
     * <p>initializeRequest.</p>
     */
    public abstract void initializeRequest();

    /**
     * <p>Getter for the field <code>patientCriteria</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getPatientCriteria() {
        return patientCriteria;
    }

    /**
     * <p>Setter for the field <code>patientCriteria</code>.</p>
     *
     * @param patientCriteria a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setPatientCriteria(Patient patientCriteria) {
        this.patientCriteria = patientCriteria;
    }

    /**
     * <p>Setter for the field <code>receivedPatients</code>.</p>
     *
     * @param receivedPatients a {@link java.util.HashMap} object.
     */
    public void setReceivedPatients(HashMap<Integer, List<Patient>> receivedPatients) {
        this.receivedPatients = receivedPatients;
    }

    /**
     * <p>Getter for the field <code>selectedPage</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedPage() {
        return selectedPage;
    }

    /**
     * <p>Setter for the field <code>selectedPage</code>.</p>
     *
     * @param selectedPage a {@link java.lang.Integer} object.
     */
    public void setSelectedPage(Integer selectedPage) {
        this.selectedPage = selectedPage;
    }

    /**
     * <p>Getter for the field <code>receivedPatients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Patient> getReceivedPatients() {
        if ((receivedPatients != null) && (selectedPage != null)) {
            return receivedPatients.get(selectedPage);
        } else {
            return null;
        }
    }

    /**
     * <p>getPages.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Integer> getPages() {
        if (receivedPatients != null) {
            return new ArrayList<Integer>(receivedPatients.keySet());
        } else {
            return null;
        }
    }

    /**
     * <p>cleanUpQuery.</p>
     */
    public void cleanUpQuery() {
        patientCriteria = null;
        initializeRequest();
    }

    /**
     * <p>Setter for the field <code>selectedTransaction</code>.</p>
     *
     * @param selectedTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    /**
     * <p>Getter for the field <code>selectedTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getSelectedTransaction() {
        return selectedTransaction;
    }

    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * <p>Getter for the field <code>limitValue</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLimitValue() {
        return limitValue;
    }

    /**
     * <p>Setter for the field <code>limitValue</code>.</p>
     *
     * @param limitValue a {@link java.lang.Integer} object.
     */
    public void setLimitValue(Integer limitValue) {
        this.limitValue = limitValue;
    }

    /**
     * <p>Getter for the field <code>limit</code>.</p>
     *
     * @return a boolean.
     */
    public boolean getLimit() {
        return limit;
    }

    /**
     * <p>Setter for the field <code>limit</code>.</p>
     *
     * @param limit a boolean.
     */
    public void setLimit(boolean limit) {
        this.limit = limit;
    }

    /**
     * <p>isMoreResultsAvailable.</p>
     *
     * @return a boolean.
     */
    public boolean isMoreResultsAvailable() {
        return moreResultsAvailable;
    }

    /**
     * <p>Getter for the field <code>domainsReturned</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<AssigningAuthority> getDomainsReturned() {
        return domainsReturned;
    }

    /**
     * <p>Setter for the field <code>domainsReturned</code>.</p>
     *
     * @param domainsReturned a {@link java.util.List} object.
     */
    public void setDomainsReturned(List<AssigningAuthority> domainsReturned) {
        this.domainsReturned = domainsReturned;
    }

    /**
     * <p>Getter for the field <code>currentDomainReturned</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
     */
    public AssigningAuthority getCurrentDomainReturned() {
        return currentDomainReturned;
    }

    /**
     * <p>Setter for the field <code>currentDomainReturned</code>.</p>
     *
     * @param currentDomainReturned a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
     */
    public void setCurrentDomainReturned(AssigningAuthority currentDomainReturned) {
        this.currentDomainReturned = currentDomainReturned;
    }

    /**
     * <p>isDisplayCriteriaForm.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayCriteriaForm() {
        return displayCriteriaForm;
    }

    /**
     * <p>Setter for the field <code>displayCriteriaForm</code>.</p>
     *
     * @param displayCriteriaForm a boolean.
     */
    public void setDisplayCriteriaForm(boolean displayCriteriaForm) {
        this.displayCriteriaForm = displayCriteriaForm;
    }

    /**
     * <p>Getter for the field <code>pageNumber</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * <p>Setter for the field <code>pageNumber</code>.</p>
     *
     * @param pageNumber a {@link java.lang.Integer} object.
     */
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * <p>Getter for the field <code>cancellation</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getCancellation() {
        return cancellation;
    }

    /**
     * <p>Setter for the field <code>cancellation</code>.</p>
     *
     * @param cancellation a {@link java.lang.Boolean} object.
     */
    public void setCancellation(Boolean cancellation) {
        this.cancellation = cancellation;
    }

    /**
     * <p>getAvailablePhoneUse.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getAvailablePhoneUse() {
        return PhoneNumberType.getListAsSelectItems();
    }

    /**
     * <p>initializePatientCriteria.</p>
     */
    protected void initializePatientCriteria() {
        if (patientCriteria == null) {
            patientCriteria = new Patient();
            patientCriteria.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
            limit = false;
            limitValue = 10;
            domainsReturned = new ArrayList<AssigningAuthority>();
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void setLimitNumberForTestData(TestData testData) {
        if (this.limit) {
            testData.setLimitValue(this.limitValue);
        } else {
            testData.setLimitValue(null);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void createNewTestData() {
        Patient favoritePatient = new Patient(patientCriteria, Actor.findActorWithKeyword("UNKNOWN"));
        createPatientTestData(favoritePatient, selectedTransaction, domainsReturned);
    }

    /** {@inheritDoc} */
    @Override
    protected void setDeferredOption(TestData testData) {
        testData.setDeferredOption(false);
    }

    /** {@inheritDoc} */
    @Override
    protected void setCredentialsForTestData(TestData testData) {
        testData.setCredentials(null);
    }

    /** {@inheritDoc} */
    @Override
    protected void setFhirResponseFormatForTestData(TestData testData) {
        testData.setFhirReturnType(null);
    }

    /** {@inheritDoc} */
    @Override
    public Transaction getTestedTransaction() {
        return selectedTransaction;
    }

    /** {@inheritDoc} */
    @Override
    protected void resetPage(){
        this.patientCriteria = null;
        initializeRequest();
    }

    /** {@inheritDoc} */
    protected void initializePageUsingTestData(TestData selectedTestData){
        if (selectedTestData != null && selectedTestData instanceof PatientTestData){
            PatientTestData patientTestData = (PatientTestData) selectedTestData;
            patientCriteria = patientTestData.getPatient();
            this.limit = (patientTestData.getLimitValue() != null);
            if (this.limit){
                this.limitValue = patientTestData.getLimitValue();
            }
            domainsReturned = getAssigningAuthoritiesFromRestrictedDomains(selectedTestData.getRestrictedDomain());
        }
    }

    /**
     * <p>addBirthplaceCriteria.</p>
     */
    public void addBirthplaceCriteria(){
        PatientAddress birthplace = new PatientAddress(AddressType.BIRTH);
        birthplace.setPatient(patientCriteria);
        patientCriteria.addPatientAddress(birthplace);
    }

    /**
     * <p>isPatientAddressListEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isPatientAddressListEmpty(){
        if (patientCriteria.getAddressList().isEmpty()){
            return true;
        } else {
            for (PatientAddress address: patientCriteria.getAddressList()){
                if (!AddressType.BIRTH.equals(address.getAddressType())){
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * <p>isBirthPlaceAddressEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isBirthPlaceAddressEmpty(){
        if (patientCriteria.getAddressList().isEmpty()){
            return true;
        } else {
            for (PatientAddress address: patientCriteria.getAddressList()){
                if (AddressType.BIRTH.equals(address.getAddressType())){
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * <p>isPhoneNumberListEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isPhoneNumberListEmpty(){
        return patientCriteria.getPhoneNumbers().isEmpty();
    }

    protected void nextPage(){
        setPageNumber(pageNumber + INCREMENT);
    }

}
