package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.security.utils.MarshallUtils;
import net.ihe.gazelle.sequoia.security.SOAPHeaderModifier;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapWebServiceClient;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.sts.constants.AssertionProfile;
import net.ihe.gazelle.xua.actors.XServiceUser;
import net.ihe.gazelle.xua.model.PicketLinkCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;

/**
 * <p>Abstract SoapHL7V3WebServiceClient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class SoapHL7V3WebServiceClient extends SoapWebServiceClient {

    private static final String ITI = "ITI";
    private static final String IHE = "IHE";
    private static Logger log = LoggerFactory.getLogger(SoapHL7V3WebServiceClient.class);
    /**
     * Constant <code>PDQV3_TNS="urn:ihe:iti:pdqv3:2007"</code>
     */
    protected static final String PDQV3_TNS = "urn:ihe:iti:pdqv3:2007";
    /**
     * Constant <code>PIXV3_TNS="urn:ihe:iti:pixv3:2007"</code>
     */
    protected static final String PIXV3_TNS = "urn:ihe:iti:pixv3:2007";
    /**
     * Constant <code>XCPD_TNS="urn:ihe:iti:xcpd:2009"</code>
     */
    protected static final String XCPD_TNS = "urn:ihe:iti:xcpd:2009";

    private XServiceUser xServiceUser;

    /**
     * <p>Constructor for SoapHL7V3WebServiceClient.</p>
     *
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public SoapHL7V3WebServiceClient(HL7v3ResponderSUTConfiguration inSUT) {
        super();
        if (inSUT != null) {
            setSutUrl(inSUT.getUrl());
            setSutName(inSUT.getName());
        }
    }

    /**
     * <p>Constructor for SoapHL7V3WebServiceClient.</p>
     */
    public SoapHL7V3WebServiceClient() {
        super();
    }

    /**
     * <p>getDomainForTransactionInstance.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomainForTransactionInstance() {
        String domain = PreferenceService.getString("default_pdq_domain");
        if (domain == null || domain.isEmpty() || IHE.equals(domain)) {
            return Domain.getDomainByKeyword(ITI);
        } else {
            return Domain.getDomainByKeyword(domain);
        }
    }

    /**
     * <p>getIssuerName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIssuerName() {
        return "PatientManager";
    }

    /**
     * <p>sendPRPAIN201305UV02Type.</p>
     *
     * @param request     a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param action      a {@link java.lang.String} object.
     * @param credentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type sendPRPAIN201305UV02Type(PRPAIN201305UV02Type request, String action, PicketLinkCredentials credentials)
            throws SoapSendException {
        transactionInstance = null;
        PRPAIN201306UV02Type response;
        if (credentials != null) {
            AssertionProfile profile = credentials.getAssertionProfile();
            response = sendWithXua(PRPAIN201306UV02Type.class, request.get_xmlNodePresentation(), action, profile.getName(), profile
                    .getPassword());
            logTransactionDetailsForXUA();
        } else {
            response = send(PRPAIN201306UV02Type.class, request.get_xmlNodePresentation(), action);
        }
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
        }
        transactionInstance = saveTransactionInstance(findCandidatesQueryToByteArray(request),
                findCandidatesQueryResponseToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
                responseType);
        if (transactionInstance != null) {
            HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
            if (response != null) {
                HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
            }
        }
        return response;
    }

    /**
     * <p>sendPRPAIN201302UV02.</p>
     *
     * @param request     a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
     * @param action      a {@link java.lang.String} object.
     * @param credentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     *
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type sendPRPAIN201302UV02(PRPAIN201302UV02Type request, String action, PicketLinkCredentials credentials)
            throws SoapSendException {
        transactionInstance = null;
        MCCIIN000002UV01Type response;
        if (credentials != null) {
            AssertionProfile profile = credentials.getAssertionProfile();
            response = sendWithXua(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), action, profile.getName(), profile
                    .getPassword());
            logTransactionDetailsForXUA();
        } else {
            response = send(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), action);
        }
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
        }
        transactionInstance = saveTransactionInstance(getRevisePatientRecordToByteArray(request),
                getAcknowledgementToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
                responseType);
        if (transactionInstance != null) {
            HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
            if (response != null) {
                HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
            }
        }
        return response;
    }

    private byte[] getRevisePatientRecordToByteArray(PRPAIN201302UV02Type request) {
        if (request != null) {
            return HL7V3Utils.getRevisePatientRecordToByteArray(request);
        } else {
            return null;
        }
    }

    /**
     * <p>getAcknowledgementToByteArray.</p>
     *
     * @param ack a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     *
     * @return an array of byte.
     */
    protected byte[] getAcknowledgementToByteArray(MCCIIN000002UV01Type ack) {
        if (ack != null) {
            return HL7V3Utils.acknowledgementToByteArray(ack);
        } else {
            return null;
        }
    }

    /**
     * <p>findCandidatesQueryToByteArray.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     *
     * @return an array of byte.
     */
    protected byte[] findCandidatesQueryToByteArray(PRPAIN201305UV02Type request) {
        if (request != null) {
            return HL7V3Utils.findCandidatesQueryToByteArray(request);
        } else {
            return null;
        }
    }

    /**
     * <p>findCandidatesQueryResponseToByteArray.</p>
     *
     * @param response a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     *
     * @return an array of byte.
     */
    protected byte[] findCandidatesQueryResponseToByteArray(PRPAIN201306UV02Type response) {
        if (response != null) {
            return HL7V3Utils.findCandidatesQueryResponseToByteArray(response);
        } else {
            return null;
        }
    }

    /**
     * <p>acknowledgementToByteArray.</p>
     *
     * @param ack a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     *
     * @return an array of byte.
     */
    protected byte[] acknowledgementToByteArray(MCCIIN000002UV01Type ack) {
        if (ack != null) {
            return HL7V3Utils.acknowledgementToByteArray(ack);
        } else {
            return null;
        }

    }

    /**
     * {@inheritDoc}
     */
    protected Element getAssertionFromSTS(String username, String password) {
        Element assertion = null;
        if (username != null) {
            String stsUrl = PreferenceService.getString("gazelle_sts_url");
            String appliesToUrl = PreferenceService.getString("sts_default_audience_url");
            this.xServiceUser = new XServiceUser(stsUrl);
            assertion = xServiceUser.getAssertionForCredentials(username, password, appliesToUrl);
        }
        return assertion;
    }

    /**
     * {@inheritDoc}
     */
    protected void appendAssertionToSoapHeader(SOAPMessage msg, Element assertion) {
        try {
            XServiceUser.appendAssertionToSoapHeader(msg, assertion);
            Boolean isSequoia = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEQUOIA);
            if (isSequoia != null && isSequoia) {
                String defautlSTSUsername = PreferenceService.getString("sts_default_username");
                String defautlSTSPassword = PreferenceService.getString("sts_defaut_password");
                String stsUrl = PreferenceService.getString("gazelle_sts_url");
                String appliesToUrl = PreferenceService.getString("sts_default_audience_url");
                String keystore = PreferenceService.getString("path_to_keystore");
                String keystorepwd = PreferenceService.getString("keystore_password");
                String privateKeyAlias = PreferenceService.getString("private_key_alias");
                String keypwd = PreferenceService.getString("private_key_password");
                SOAPHeaderModifier sequoiaModifier = new SOAPHeaderModifier(defautlSTSUsername, defautlSTSPassword, appliesToUrl, stsUrl);
                try {
                    Document modifiedMessage = sequoiaModifier.appendSequoiaSecurityHeaders(msg.getSOAPPart(), keystore, privateKeyAlias,
                            keystorepwd, keypwd);
                    if (modifiedMessage != null) {
                        MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                        byte[] content = MarshallUtils.nodeToByteArray(modifiedMessage);
                        // reassign message with its new content
                        factory.createMessage(null, new ByteArrayInputStream(content));
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        } catch (SOAPException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * <p>getStandard.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.EStandard} object.
     */
    protected EStandard getStandard() {
        return EStandard.HL7V3;
    }

    /**
     * <p>logTransactionDetailsForXUA.</p>
     */
    protected void logTransactionDetailsForXUA() {
        xServiceUser.logXServiceProviderResponse(getFault(), getSutUrl());
    }


}
