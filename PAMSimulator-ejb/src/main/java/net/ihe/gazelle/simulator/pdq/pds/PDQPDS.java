package net.ihe.gazelle.simulator.pdq.pds;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.QCN_J01;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.model.MovementQuery;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.pdq.hl7.PDSMessageBuilder;
import net.ihe.gazelle.simulator.pdq.hl7.QBPMessageDecoder;
import net.ihe.gazelle.simulator.pdq.util.ContinuationPointerManager;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * <p>PDQPDS class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDQPDS extends IHEDefaultHandler {

    private static Logger log = LoggerFactory.getLogger(PDQPDS.class);
    private static final String[] ACCEPTED_EVENTS = {"Q22", "ZV1", "J01"};
    private static final String[] ACCEPTED_MESSAGE_TYPES = {"QBP", "QCN"};

    /**
     * <p>Constructor for PDQPDS.</p>
     */
    public PDQPDS() {
        super();
    }

    private static ContinuationPointerManager pointerManager;

    static {
        pointerManager = new ContinuationPointerManager();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IHEDefaultHandler newInstance() {
        PDQPDS handler = new PDQPDS();
        return handler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Message processMessage(Message incomingMessage) throws HL7Exception {
        String simulatedTransactionKeyword = "ITI-21";
        pointerManager.clear();
        String triggerEvent = terser.get("/.MSH-9-2");
        String messageStructure = terser.get("/.MSH-9-3");
        Message ack;
        ACKBuilder ackBuilder = new ACKBuilder(serverApplication, serverFacility, Arrays.asList(ACCEPTED_MESSAGE_TYPES), Arrays.asList
                (ACCEPTED_EVENTS));
        try {
            ackBuilder.isMessageAccepted(incomingMessage);
        } catch (HL7v2ParsingException e) {
            ack = ackBuilder.buildNack(incomingMessage, e);
            simulatedTransaction = Transaction.GetTransactionByKeyword("UNKNOWN");
            logIfError(ack, entityManager);
            setSutActor(Actor.findActorWithKeyword("PDC", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(ack));
            return ack;
        }

        try {
            if (messageStructure.equals("QCN_J01")) {
                PDSMessageBuilder messageBuilder = processCancellationRequest(incomingMessage);
                return sendMessage(messageBuilder.build(incomingMessage, "J01", null),
                        simulatedTransactionKeyword);
            } else {
                PipeParser parser = PipeParser.getInstanceWithNoValidation();
                QBP_Q21 request = (QBP_Q21) parser.parseForSpecificPackage(incomingMessage.encode(),
                        "net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message");
                QBPMessageDecoder messageDecoder = new QBPMessageDecoder(request);
                String pointer = messageDecoder.getContinuationPointerFromMessage();
                if ((pointer == null) || pointer.isEmpty()) {
                    // new request
                    // check the simulator knows all the provided domains
                    List<Integer> unknownDomains = messageDecoder.checkWhatDomainsReturned(entityManager);
                    if (!unknownDomains.isEmpty()) // if some of the given domains
                    // are unknown, send a nack !
                    {
                        PDSMessageBuilder messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AE);
                        messageBuilder.setFieldRepetition(unknownDomains);
                        messageBuilder.setPatients(null);
                        messageBuilder.setMovements(null);
                        return sendMessage(messageBuilder.build(request, triggerEvent, null),
                                simulatedTransactionKeyword);
                    }
                    // nb of hits
                    Integer nbOfHits = messageDecoder.getNbOfHitsToReturnFromQuery();
                    if (triggerEvent.equals("Q22")) {
                        return processDemographicsQuery(simulatedTransactionKeyword, triggerEvent, request, messageDecoder, nbOfHits);
                    } else {
                        simulatedTransactionKeyword = "ITI-22";
                        return processDemographicsAndVisitQuery(simulatedTransactionKeyword, triggerEvent, request, messageDecoder, nbOfHits);
                    }
                } else {
                    Integer nbOfHits = messageDecoder.getNbOfHitsToReturnFromQuery();
                    if (triggerEvent.equals("Q22")) {
                        return processContinuationForDemographics(simulatedTransactionKeyword, triggerEvent, request, messageDecoder, pointer, nbOfHits);

                    } else {
                        simulatedTransactionKeyword = "ITI-22";
                        return processContinuationForDemographicsAndVisit(simulatedTransactionKeyword, triggerEvent, request, messageDecoder, pointer,
                                nbOfHits);
                    }
                }
            }
        }catch (HL7v2ParsingException e){
            return ackBuilder.buildNack(incomingMessage, e);
        }
    }

    private Message processContinuationForDemographicsAndVisit(String simulatedTransactionKeyword, String triggerEvent, QBP_Q21 request,
                                                               QBPMessageDecoder messageDecoder, String pointer, Integer nbOfHits) throws HL7v2ParsingException {
        List<Movement> movements = pointerManager.getMovementListForPointer(pointer, nbOfHits);
        PDSMessageBuilder messageBuilder;
        if (movements == null) {
            return buildResponseForUnrecognizePointer(simulatedTransactionKeyword, triggerEvent, request, pointer);
        } else if (pointerManager.pointerExists(pointer)) {
            messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AA);
        } else {
            messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
        }
        messageBuilder.setMovements(movements);
        return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                simulatedTransactionKeyword);
    }


    private Message buildResponseForUnrecognizePointer(String simulatedTransactionKeyword, String triggerEvent, QBP_Q21 request, String pointer)
            throws HL7v2ParsingException {
        PDSMessageBuilder messageBuilder;
        log.debug("This continuation pointer has not been recognized");
        messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AE);
        messageBuilder.setFieldRepetition(null);
        messageBuilder.setPatients(null);
        messageBuilder.setMovements(null);
        return sendMessage(messageBuilder.build(request, triggerEvent, null),
                simulatedTransactionKeyword);
    }

    private Message processContinuationForDemographics(String simulatedTransactionKeyword, String triggerEvent, QBP_Q21 request, QBPMessageDecoder
            messageDecoder, String pointer, Integer nbOfHits) throws HL7v2ParsingException {
        List<Patient> patients = pointerManager.getPatientListForPointer(pointer, nbOfHits);
        PDSMessageBuilder messageBuilder;
        if (patients == null) {
            return buildResponseForUnrecognizePointer(simulatedTransactionKeyword, triggerEvent, request, pointer);
        } else if (pointerManager.pointerExists(pointer)) {
            messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AA);
        } else {
            messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
        }
        messageBuilder.setPatients(patients);
        return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                simulatedTransactionKeyword);
    }

    private Message processDemographicsAndVisitQuery(String simulatedTransactionKeyword, String triggerEvent, QBP_Q21 request, QBPMessageDecoder
            messageDecoder, Integer nbOfHits) throws HL7v2ParsingException {
        String pointer;
        MovementQuery hqlQuery = messageDecoder.getMovementQueryFromMessage(new HQLQueryBuilder<Movement>(
                entityManager, Movement.class));
        if (hqlQuery == null) {
            return sendMessage(null, simulatedTransactionKeyword);
        } else {
            List<Movement> movements = hqlQuery.getListNullIfEmpty();
            if (movements == null) {
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
                messageBuilder.setMovements(null);
                return sendMessage(messageBuilder.build(request, triggerEvent, null),
                        simulatedTransactionKeyword);
            } else if ((nbOfHits == 0) || (movements.size() <= nbOfHits)) {
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
                messageBuilder.setMovements(movements);
                return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                        simulatedTransactionKeyword);
            } else {
                pointer = messageDecoder.generateContinuationPointer();
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(pointer,
                        AcknowledgmentCode.AA);
                messageBuilder.setMovements(movements.subList(0, nbOfHits));
                pointerManager.addPointer(pointer, nbOfHits, null,
                        movements.subList(nbOfHits, movements.size()));
                return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                        simulatedTransactionKeyword);
            }
        }
    }

    private Message processDemographicsQuery(String simulatedTransactionKeyword, String triggerEvent, QBP_Q21 request, QBPMessageDecoder
            messageDecoder, Integer nbOfHits) throws HL7v2ParsingException {
        String pointer;
        PatientQuery hqlQuery = messageDecoder.getPatientQueryFromMessage(new HQLQueryBuilder<Patient>(
                entityManager, Patient.class));
        if (hqlQuery == null) {
            return sendMessage(null, "ITI-21");
        } else {
            List<Patient> patients = hqlQuery.getListNullIfEmpty();
            if (patients == null) {
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
                messageBuilder.setPatients(null);
                return sendMessage(messageBuilder.build(request, triggerEvent, null),
                        simulatedTransactionKeyword);
            } else if ((nbOfHits == 0) || (patients.size() <= nbOfHits)) {
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(null, AcknowledgmentCode.AA);
                messageBuilder.setPatients(patients);
                return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                        simulatedTransactionKeyword);
            } else {
                pointer = messageDecoder.generateContinuationPointer();
                PDSMessageBuilder messageBuilder = new PDSMessageBuilder(pointer,
                        AcknowledgmentCode.AA);
                messageBuilder.setPatients(patients.subList(0, nbOfHits));
                pointerManager.addPointer(pointer, nbOfHits, patients.subList(nbOfHits, patients.size()),
                        null);

                return sendMessage(messageBuilder.build(request, triggerEvent, messageDecoder.getDomainsToReturn()),
                        simulatedTransactionKeyword);
            }
        }
    }

    private PDSMessageBuilder processCancellationRequest(Message incomingMessage) throws HL7Exception {
        QCN_J01 request = (QCN_J01) PipeParser.getInstanceWithNoValidation().parse(incomingMessage.encode());
        String pointer = QBPMessageDecoder.getPointerFromCancellationRequest(request);
        // simulatedTransactionKeyword is updated at cancellation time
        boolean queryCancelled = pointerManager.cancelQuery(pointer);
        PDSMessageBuilder messageBuilder;
        if (queryCancelled) {
            messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AA);
        } else {
            messageBuilder = new PDSMessageBuilder(pointer, AcknowledgmentCode.AE);
        }
        return messageBuilder;
    }

    @Transactional
    private Message sendMessage(Message outcomingMessage, String transactionKeyword) throws HL7v2ParsingException {
        if (outcomingMessage == null) {
            throw new HL7v2ParsingException("No search parameter provided");
        } else {
            simulatedTransaction = Transaction.GetTransactionByKeyword(transactionKeyword);
            logIfError(outcomingMessage, entityManager);
            setSutActor(Actor.findActorWithKeyword("PDC", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(outcomingMessage));
            return outcomingMessage;
        }
    }

}
