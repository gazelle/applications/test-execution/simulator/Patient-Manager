package net.ihe.gazelle.simulator.fhir.resources;

import ca.uhn.fhir.rest.annotation.Destroy;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.simulator.hl7v3.responder.IHEQueryParserCode;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pdqm.pds.PDQmQueryHandler;
import net.ihe.gazelle.simulator.pdqm.pds.PDQmQueryParameters;
import net.ihe.gazelle.simulator.pixm.manager.PIXmQueryHandler;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.Parameters;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * All resource providers must implement IResourceProvider
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RestfulPatientResourceProvider implements IResourceProvider {

    /**
     * Constant <code>TARGET_SYSTEM="targetSystem"</code>
     */
    private static final String TARGET_SYSTEM = "targetSystem";
    /**
     * Constant <code>SOURCE_IDENTIFIER="sourceIdentifier"</code>
     */
    private static final String SOURCE_IDENTIFIER = "sourceIdentifier";


    private static final Logger LOGGER = LoggerFactory.getLogger(RestfulPatientResourceProvider.class);


    /**
     * {@inheritDoc}
     * The getResourceType method comes from IResourceProvider, and must
     * be overridden to indicate what type of resource this provider
     * supplies.
     */
    @Override
    public Class<PatientFhirIHE> getResourceType() {
        return PatientFhirIHE.class;
    }

    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param theId The read operation takes one parameter, which must be of type
     *              IdDt and must be annotated with the "@Read.IdParam" annotation.
     *
     * @return Returns a resource matching this identifier, or null if none exists.
     */
    @Read()
    public PatientFhirIHE getResourceById(@IdParam IdType theId) {
        if (theId.isIdPartValid()) {
            Integer patientId = Integer.decode(theId.getIdPart());
            net.ihe.gazelle.simulator.pam.model.Patient patientDB = PatientDAO.getPatientById(patientId);
            if (patientDB == null) {
                OperationOutcome.OperationOutcomeIssueComponent notFoundError = new OperationOutcome.OperationOutcomeIssueComponent();
                notFoundError.setSeverity(OperationOutcome.IssueSeverity.ERROR);
                notFoundError.setCode(OperationOutcome.IssueType.NOTFOUND);
                OperationOutcome outcome = new OperationOutcome();
                outcome.addIssue(notFoundError);
                throw new ResourceNotFoundException("No patient matches provided identifier", outcome);
            } else {
                return patientDB.toFhirPatient();
            }
        } else {
            return null;
        }
    }


    // PDQm ITI-78 SEARCH

    /**
     * <p>findPatients.</p>
     *
     * @param id             a {@link StringParam} object.
     * @param identifierList a {@link TokenAndListParam} object.
     * @param family         a {@link StringParam} object.
     * @param given          a {@link StringParam} object.
     * @param birthdate      a {@link StringParam} object.
     * @param address        a {@link StringParam} object.
     * @param city           a {@link StringParam} object.
     * @param country        a {@link StringParam} object.
     * @param postalCode     a {@link StringParam} object.
     * @param state          a {@link StringParam} object.
     * @param gender         a {@link StringParam} object.
     * @param telecom        a {@link StringParam} object.
     *
     * @return a {@link org.hl7.fhir.dstu3.model.Bundle} object.
     */
    @Search(type = PatientFhirIHE.class)
    @Transactional
    public List<PatientFhirIHE> findPatients(@OptionalParam(name = PatientFhirIHE.SP_RES_ID) final StringParam id,
                                             @OptionalParam(name = PatientFhirIHE.SP_ACTIVE) final TokenParam active,
                                             @OptionalParam(name = PatientFhirIHE.SP_IDENTIFIER) final TokenAndListParam identifierList,
                                             @OptionalParam(name = PatientFhirIHE.SP_FAMILY) final StringAndListParam family,
                                             @OptionalParam(name = PatientFhirIHE.SP_GIVEN) final StringAndListParam given,
                                             @OptionalParam(name = PatientFhirIHE.SP_BIRTHDATE) final DateParam birthdate,
                                             @OptionalParam(name = PatientFhirIHE.SP_ADDRESS) final StringParam address,
                                             @OptionalParam(name = PatientFhirIHE.SP_ADDRESS_CITY) final StringParam city,
                                             @OptionalParam(name = PatientFhirIHE.SP_ADDRESS_COUNTRY) final StringParam country,
                                             @OptionalParam(name = PatientFhirIHE.SP_ADDRESS_POSTALCODE) final StringParam postalCode,
                                             @OptionalParam(name = PatientFhirIHE.SP_ADDRESS_STATE) final StringParam state,
                                             @OptionalParam(name = PatientFhirIHE.SP_GENDER) final TokenParam gender,
                                             @OptionalParam(name = PatientFhirIHE.SP_TELECOM) final TokenParam telecom,
                                             @OptionalParam(name = PatientFhirIHE.SP_MOTHERS_MAIDEN_NAME_FAMILY) final StringParam
                                                     mothersMaidenFamilyName) {

        final PDQmQueryHandler queryHandler = new PDQmQueryHandler();
        final PDQmQueryParameters queryParameters = new PDQmQueryParameters();
        queryParameters.setActive(active);
        queryParameters.setId(id);
        queryParameters.setAddress(address);
        queryParameters.setAddressCity(city);
        queryParameters.setIdentifierList(identifierList);
        queryParameters.setFamilyNames(family);
        queryParameters.setGivenNames(given);
        queryParameters.setBirthdate(birthdate);
        queryParameters.setAddressCountry(country);
        queryParameters.setAddressPostalcode(postalCode);
        queryParameters.setAddressState(state);
        queryParameters.setGender(gender);
        queryParameters.setTelecom(telecom);
        queryParameters.setMotherMaidenFamilyName(mothersMaidenFamilyName);
        if (queryParameters.hasParam()) {
            IHEQueryParserCode responseCode = queryHandler.executeQuery(queryParameters);

            switch (responseCode) {
                case UNKNOWN_DOMAINS:
                    OperationOutcome outcome = new OperationOutcome();
                    OperationOutcome.OperationOutcomeIssueComponent issueComponent = new OperationOutcome.OperationOutcomeIssueComponent();
                    issueComponent.setSeverity(OperationOutcome.IssueSeverity.ERROR);
                    issueComponent.setCode(OperationOutcome.IssueType.VALUE);
                    List<String> unknownDomains = queryHandler.getUnknownDomains();
                    issueComponent.setDiagnostics(unknownDomains.get(0) + " not found");
                    outcome.addIssue(issueComponent);
                    throw new ResourceNotFoundException("targetSystem not found", outcome);
                case NO_PATIENT_FOUND:
                    return new ArrayList<PatientFhirIHE>();
                case RETURN_ALL_PATIENTS:
                    return toFhirPatients(queryHandler.getReturnedPatients());
                default:
                    throw new InternalErrorException("Unknown exception");
            }
        } else {
            // if no parameter is set we don't want to perform the query
            return new ArrayList<PatientFhirIHE>();
        }

    }


    private List<PatientFhirIHE> toFhirPatients(List<Patient> patientListToReturn) {
        List<PatientFhirIHE> fhirPatients = new ArrayList<PatientFhirIHE>();
        for (net.ihe.gazelle.simulator.pam.model.Patient currentDBPatient : patientListToReturn) {
            Patient patient = PatientDAO.getPatientById(currentDBPatient.getId());
            fhirPatients.add(patient.toFhirPatient());
        }
        return fhirPatients;
    }


    /**
     * <p>findCrossReferenceIdentifiers.</p>
     *
     * @param sourceIdentifier a {@link ca.uhn.fhir.rest.param.TokenParam} object.
     * @param targetSystem     a {@link java.lang.String} object.
     *
     * @return a {@link org.hl7.fhir.dstu3.model.Parameters} object.
     */
    @Operation(name = "$ihe-pix", idempotent = true)
    public Parameters findCrossReferenceIdentifiers(
            @OperationParam(name = SOURCE_IDENTIFIER, min = 1, max = 1) TokenParam sourceIdentifier,
            @OperationParam(name = TARGET_SYSTEM, max = 1) String targetSystem) {

        String assigningAuthority = PatientIdentifierDAO.urnToOid(sourceIdentifier.getSystem());
        String patientIdentifier = sourceIdentifier.getValue();
        String targetSystemAsOID = PatientIdentifierDAO.urnToOid(targetSystem);
        PIXmQueryHandler handler = new PIXmQueryHandler(patientIdentifier, assigningAuthority, targetSystemAsOID);
        IHEQueryParserCode responseCode = handler.findPatientsByIdentifiers();

        switch (responseCode) {
            case UNKNOWN_DOMAINS:
                OperationOutcome outcome = handler.buildOperationOutcome(responseCode);
                throw new ForbiddenOperationException(handler.getErrorMessage(), outcome);
            case UNKNOWN_SOURCE_ASSIGNING_AUTHORITY:
            case NO_QUERY_PARAMETER:
                outcome = handler.buildOperationOutcome(responseCode);
                throw new InvalidRequestException(handler.getErrorMessage(), outcome);
            case UNKNOWN_SOURCE_PATIENT_ID:
                outcome = handler.buildOperationOutcome(responseCode);
                throw new ResourceNotFoundException(handler.getErrorMessage(), outcome);
            case NO_PATIENT_FOUND:
                return new Parameters();
            case RETURN_ALL_PATIENTS:
                return handler.getResultList();
            default:
                throw new InternalErrorException("Unknown exception");
        }
    }


    /**
     * <p>destroy.</p>
     */
    @Destroy
    public void destroy() {
        LOGGER.error("Patient Resource Provider destroyed");
    }

}
