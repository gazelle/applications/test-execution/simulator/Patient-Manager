package net.ihe.gazelle.simulator.cat.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.annotations.Name;

import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>SystemUnderTestPreferences class.</p>
 *
 * @author aberge
 * @version 1.0: 29/11/17
 */

@Entity
@Name("systemUnderTestPreferences")
@Table(name = "pam_systemundertest_preferences", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"system_under_test_id"}))
@SequenceGenerator(name = "pam_systemundertest_preferences_sequence", sequenceName = "pam_systemundertest_preferences_id_seq", allocationSize = 1)
public class SystemUnderTestPreferences implements Serializable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_systemundertest_preferences_sequence")
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToOne(targetEntity = SystemConfiguration.class)
    @JoinColumn(name = "system_under_test_id")
    private SystemConfiguration systemUnderTest;

    @ManyToMany(targetEntity = HierarchicDesignator.class)
    @JoinTable(name = "pam_sut_assigning_authorities",
            joinColumns = @JoinColumn(name = "sut_preference_id"),
            inverseJoinColumns = @JoinColumn(name = "hierarchic_designator_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"sut_preference_id", "hierarchic_designator_id"}))
    private List<HierarchicDesignator> assigningAuthorities;

    @Column(name = "preferred_message")
    private EConnectathonMessageType preferredMessage;

    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @Column(name = "last_modifier")
    private String lastModifier;

    @Transient
    private List<SelectItem> availableMessageTypes;

    public SystemUnderTestPreferences(){

    }

    public SystemUnderTestPreferences(SystemConfiguration sut){
        this.systemUnderTest = sut;
        initializeAvailableMessageTypes();
    }

    private void initializeAvailableMessageTypes() {
        if (this.systemUnderTest != null){
            List<SelectItem> availableItems = SystemUnderTestPreferencesDAO.getListPossibleMessageTypes(this.systemUnderTest, false);
            if (availableItems.size() == 1){
                SelectItem uniqueEntry = availableItems.get(0);
                setPreferredMessage((EConnectathonMessageType) uniqueEntry.getValue());
            }
            availableItems.add(new SelectItem(null, "Please select..."));
            setAvailableMessageTypes(availableItems);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemConfiguration getSystemUnderTest() {
        return systemUnderTest;
    }

    public void setSystemUnderTest(SystemConfiguration systemUnderTest) {
        this.systemUnderTest = systemUnderTest;
    }

    public List<HierarchicDesignator> getAssigningAuthorities() {
        if (assigningAuthorities == null){
            assigningAuthorities = new ArrayList<HierarchicDesignator>();
        }
        return assigningAuthorities;
    }

    public void addAssigningAuthority(HierarchicDesignator assigningAuthority){
        if (!getAssigningAuthorities().contains(assigningAuthority)) {
            getAssigningAuthorities().add(assigningAuthority);
        }
    }

    public void removeAssigningAuthority(HierarchicDesignator assigningAuthority){
        if (!getAssigningAuthorities().contains(assigningAuthority)) {
            getAssigningAuthorities().remove(assigningAuthority);
        }
    }

    public void setAssigningAuthorities(List<HierarchicDesignator> assigningAuthorities) {
        this.assigningAuthorities = assigningAuthorities;
    }

    public EConnectathonMessageType getPreferredMessage() {
        return preferredMessage;
    }

    public void setPreferredMessage(EConnectathonMessageType preferredMessage) {
        this.preferredMessage = preferredMessage;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SystemUnderTestPreferences)) {
            return false;
        }

        SystemUnderTestPreferences that = (SystemUnderTestPreferences) o;

        if (systemUnderTest != null ? !systemUnderTest.equals(that.systemUnderTest) : that.systemUnderTest != null) {
            return false;
        }
        if (assigningAuthorities != null ? !assigningAuthorities.equals(that.assigningAuthorities) : that.assigningAuthorities != null) {
            return false;
        }
        if (preferredMessage != that.preferredMessage) {
            return false;
        }
        if (lastUpdate != null ? !lastUpdate.equals(that.lastUpdate) : that.lastUpdate != null) {
            return false;
        }
        return lastModifier != null ? lastModifier.equals(that.lastModifier) : that.lastModifier == null;
    }

    @Override
    public int hashCode() {
        int result = systemUnderTest != null ? systemUnderTest.hashCode() : 0;
        result = 31 * result + (assigningAuthorities != null ? assigningAuthorities.hashCode() : 0);
        result = 31 * result + (preferredMessage != null ? preferredMessage.hashCode() : 0);
        result = 31 * result + (lastUpdate != null ? lastUpdate.hashCode() : 0);
        result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
        return result;
    }

    public List<SelectItem> getAvailableMessageTypes() {
        if (availableMessageTypes == null){
            setAvailableMessageTypes(SystemUnderTestPreferencesDAO.getListPossibleMessageTypes(this.systemUnderTest, true));
        }
        return availableMessageTypes;
    }

    public String getName() {
        return getSystemUnderTest().getName();
    }

    public String getSystemName() {
        return getSystemUnderTest().getSystemName();
    }

    public void setAvailableMessageTypes(List<SelectItem> availableMessageTypes) {
        this.availableMessageTypes = availableMessageTypes;
    }
}
