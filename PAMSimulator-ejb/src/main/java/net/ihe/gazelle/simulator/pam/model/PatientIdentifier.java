package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>PatientIdentifier class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "patientIdentifier")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("patientIdentifier")
@Table(name = "pam_patient_identifier", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
		"full_patient_id", "identifier_type_code" }))
@SequenceGenerator(name = "pam_patient_identifier_sequence", sequenceName = "pam_patient_identifier_id_seq", allocationSize = 1)
public class PatientIdentifier implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3283628371977314516L;

	@Id
	@GeneratedValue(generator = "pam_patient_identifier_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@Column(name = "full_patient_id")
	@NotNull
	@XmlElement(name = "identifier")
	private String fullPatientId;

	/**
	 * PID-3-1 (ST) or extension (HL7v3)
	 */
	@Column(name = "id_number")
	private String idNumber;

	/**
	 * PID-3-4 (HD)
	 */
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "domain_id")
	private HierarchicDesignator domain;

	/**
	 * used to fill PID-3-5 (ID)
	 */
	@Column(name = "identifier_type_code")
	@XmlElement(name = "identifierTypeCode")
	private String identifierTypeCode;

	/**
	 *
	 */
	@OneToMany(mappedBy = "patientIdentifierOne")
	private List<PatientLink> linksAsFirst;

	@OneToMany(mappedBy = "patientIdentifierTwo")
	private List<PatientLink> linksAsSecond;

	/**
	 * <p>Constructor for PatientIdentifier.</p>
	 */
	public PatientIdentifier() {

	}

	/**
	 * <p>Constructor for PatientIdentifier.</p>
	 *
	 * @param patientId a {@link java.lang.String} object.
	 * @param identifierTypeCode a {@link java.lang.String} object.
	 */
	public PatientIdentifier(String patientId, String identifierTypeCode) {
		this.fullPatientId = patientId;
		if ((identifierTypeCode != null) && !identifierTypeCode.isEmpty()) {
			this.identifierTypeCode = identifierTypeCode;
		} else {
			this.identifierTypeCode = null;
		}
	}

	/**
	 * <p>setFullPatientIdentifierIfEmpty.</p>
	 */
	@PrePersist
	@PreUpdate
	public void setFullPatientIdentifierIfEmpty(){
		if (this.fullPatientId == null){
			fullPatientId = (this.getIdNumber() != null ?this.getIdNumber(): "");
			if (domain != null){
				fullPatientId = fullPatientId.concat("^^^");
				if (domain.getNamespaceID() != null) {
					fullPatientId = fullPatientId.concat(domain.getNamespaceID());
				}
				fullPatientId = fullPatientId.concat("&");
				if (domain.getUniversalID() != null) {
					fullPatientId = fullPatientId.concat(domain.getUniversalID());
				}
				fullPatientId = fullPatientId.concat("&");
				if (domain.getUniversalIDType() != null) {
					fullPatientId = fullPatientId.concat(domain.getUniversalIDType());
				}
			}
		}
	}

	/**
	 * Constructor for copy
	 *
	 * @param pid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public PatientIdentifier(PatientIdentifier pid) {
		this.fullPatientId = pid.getFullPatientId();
		this.idNumber = pid.getIdNumber();
		this.identifierTypeCode = pid.getIdentifierTypeCode();
		this.domain = pid.getDomain();
	}

	/**
	 * <p>Setter for the field <code>fullPatientId</code>.</p>
	 *
	 * @param patientId a {@link java.lang.String} object.
	 */
	public void setFullPatientId(String patientId) {
		this.fullPatientId = patientId;
	}

	/**
	 * <p>Getter for the field <code>fullPatientId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFullPatientId() {
		return fullPatientId;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Setter for the field <code>identifierTypeCode</code>.</p>
	 *
	 * @param identifierTypeCode a {@link java.lang.String} object.
	 */
	public void setIdentifierTypeCode(String identifierTypeCode) {
		if ((identifierTypeCode != null) && identifierTypeCode.isEmpty()) {
			this.identifierTypeCode = null;
		}
		this.identifierTypeCode = identifierTypeCode;
	}

	/**
	 * <p>Getter for the field <code>identifierTypeCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIdentifierTypeCode() {
		return identifierTypeCode;
	}

	/**
	 * <p>Setter for the field <code>linksAsFirst</code>.</p>
	 *
	 * @param linksAsFirst a {@link java.util.List} object.
	 */
	public void setLinksAsFirst(List<PatientLink> linksAsFirst) {
		this.linksAsFirst = linksAsFirst;
	}

	/**
	 * <p>Getter for the field <code>linksAsFirst</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<PatientLink> getLinksAsFirst() {
		return linksAsFirst;
	}

	/**
	 * <p>Setter for the field <code>linksAsSecond</code>.</p>
	 *
	 * @param linksAsSecond a {@link java.util.List} object.
	 */
	public void setLinksAsSecond(List<PatientLink> linksAsSecond) {
		this.linksAsSecond = linksAsSecond;
	}

	/**
	 * <p>Getter for the field <code>linksAsSecond</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<PatientLink> getLinksAsSecond() {
		return linksAsSecond;
	}

	/**
	 * <p>save.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public PatientIdentifier save() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		PatientIdentifierQuery query = new PatientIdentifierQuery(new HQLQueryBuilder<PatientIdentifier>(entityManager,
				PatientIdentifier.class));
		query.fullPatientId().eq(this.fullPatientId);
		query.identifierTypeCode().eq(this.identifierTypeCode);
		List<PatientIdentifier> identifiers = query.getListNullIfEmpty();
		PatientIdentifier returnedIdentifier;
		if (identifiers != null) {
			returnedIdentifier = identifiers.get(0);
		} else {
			if (this.fullPatientId != null && this.fullPatientId.contains("^")) {
				Map<String, String> components = splitPatientId();
				if (this.getDomain() == null) {
					HierarchicDesignator domain = HierarchicDesignator.getDomainAndCreateItIfMissing(
							components.get("CX-4-1"), components.get("CX-4-2"), components.get("CX-4-3"), entityManager,
							DesignatorType.PATIENT_ID, components.get("CX-5"));
					this.setDomain(domain);
				}
				if (this.idNumber == null) {
					this.idNumber = components.get("CX-1");
				}
			}
			PatientIdentifier pid = entityManager.merge(this);
			entityManager.flush();
			returnedIdentifier = pid;
		}
		return returnedIdentifier;
	}

	/**
	 * <p>displayIdentifier.</p>
	 *
	 * @param pid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 * @return a {@link java.lang.String} object.
	 */
	// use fullPatientId attribute instead
	@Deprecated
	public String displayIdentifier(PatientIdentifier pid) {
		if (pid != null) {
			return pid.getFullPatientId();
		} else {
			return null;
		}
	}

	/**
	 * <p>splitPatientId.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, String> splitPatientId() {
		return PatientIdentifierDAO.splitCXField(this.fullPatientId);
	}

	/**
	 * <p>alreadyExists.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public PatientIdentifier alreadyExists() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		HQLQueryBuilder<PatientIdentifier> builder = new HQLQueryBuilder<PatientIdentifier>(entityManager,
				PatientIdentifier.class);
		builder.addEq("fullPatientId", this.fullPatientId);
		builder.addEq("identifierTypeCode", this.identifierTypeCode);
		List<PatientIdentifier> identifiers = builder.getList();
		PatientIdentifier returnedIdentifier;
		if ((identifiers != null) && !identifiers.isEmpty()) {
			returnedIdentifier = identifiers.get(0);
		} else {
			returnedIdentifier = null;
		}
		return returnedIdentifier;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof PatientIdentifier)) {
			return false;
		}

		PatientIdentifier that = (PatientIdentifier) o;

		if (fullPatientId != null ? !fullPatientId.equals(that.fullPatientId) : that.fullPatientId != null) {
			return false;
		}
		if (idNumber != null ? !idNumber.equals(that.idNumber) : that.idNumber != null) {
			return false;
		}
		return identifierTypeCode != null ? identifierTypeCode.equals(that.identifierTypeCode) : that.identifierTypeCode == null;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		int result = fullPatientId != null ? fullPatientId.hashCode() : 0;
		result = 31 * result + (idNumber != null ? idNumber.hashCode() : 0);
		result = 31 * result + (identifierTypeCode != null ? identifierTypeCode.hashCode() : 0);
		return result;
	}

	/**
	 * <p>Setter for the field <code>domain</code>.</p>
	 *
	 * @param domain a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
	 */
	public void setDomain(HierarchicDesignator domain) {
		this.domain = domain;
	}

	/**
	 * <p>Getter for the field <code>domain</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
	 */
	public HierarchicDesignator getDomain() {
		return domain;
	}

	/**
	 * <p>Setter for the field <code>idNumber</code>.</p>
	 *
	 * @param idNumber a {@link java.lang.String} object.
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	/**
	 * <p>Getter for the field <code>idNumber</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIdNumber() {
		return idNumber;
	}

}
