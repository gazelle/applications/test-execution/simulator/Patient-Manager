package net.ihe.gazelle.simulator.pam.automaton.helper;

import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import org.jboss.seam.annotations.async.Asynchronous;

import javax.ejb.Local;

@Local
/**
 * <p>GraphWalkerAsyncExecutionInterface interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public interface GraphWalkerAsyncExecutionInterface {

    /**
     * <p>asyncRun.</p>
     *
     * @param selectedGraphExecutionResults a net$ihe$gazelle$simulator$pam$automaton$model$graph$GraphExecutionResults object.
     * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    @Asynchronous
    void asyncRun(net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults selectedGraphExecutionResults, Encounter encounter);

    /**
     * <p>asyncReplay.</p>
     *
     * @param graphExecutionResults a net$ihe$gazelle$simulator$pam$automaton$model$graph$GraphExecutionResults object.
     */
    @Asynchronous
    void asyncReplay(GraphExecutionResults graphExecutionResults);

}
