package net.ihe.gazelle.simulator.pam.automaton.Manager;

/**
 * Created by xfs on 30/11/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum GraphExecutionStatus {
    NOT_EXECUTED("Not executed"),
    EXECUTING("Executing"),
    COMPLETED("Completed"),
    VALIDATED("Validated"),
    FAILED("Failed");

    private String label;

    GraphExecutionStatus(String inLabel){
        this.label = inLabel;
    }

    public String getLabel() {
        return label;
    }
}
