package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by aberge on 16/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PatientIdentitySourceSender extends SoapHL7V3WebServiceClient implements Serializable {

	private Logger log = Logger.getLogger(PatientIdentitySourceSender.class);

	private static final String ACTION_ADD = "urn:hl7-org:v3:PRPA_IN201301UV02";
	private static final String ACTION_REVISE = "urn:hl7-org:v3:PRPA_IN201302UV02";
	private static final String ACTION_MERGE = "urn:hl7-org:v3:PRPA_IN201304UV02";
	private static final String SERVICE_NAME = "PIXManager_Service";
	private static final String SERVICE_PORT_TYPE = "PIXManager_Port_Soap12";

	/**
	 * <p>Constructor for PatientIdentitySourceSender.</p>
	 *
	 * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public PatientIdentitySourceSender(HL7v3ResponderSUTConfiguration inSUT) {
		super(inSUT);
	}

	/** {@inheritDoc} */
	@Override protected QName getServiceQName() {
		return new QName(PIXV3_TNS, SERVICE_NAME);
	}

	/** {@inheritDoc} */
	@Override protected QName getPortQName() {
		return new QName(PIXV3_TNS, SERVICE_PORT_TYPE);
	}

	/** {@inheritDoc} */
	@Override protected Actor getSimulatedActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_SRC");
	}

	/** {@inheritDoc} */
	@Override protected Transaction getSimulatedTransaction() {
		return Transaction.GetTransactionByKeyword("ITI-44");
	}

	/** {@inheritDoc} */
	@Override protected Actor getSutActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
	}

	/**
	 * <p>sendAddPatientRecord.</p>
	 *
	 * @param request a {@link net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
	 */
	public MCCIIN000002UV01Type sendAddPatientRecord(PRPAIN201301UV02Type request) throws SoapSendException {
		transactionInstance = null;
		MCCIIN000002UV01Type response = send(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), ACTION_ADD);
		String responseType = null;
		if (response != null) {
			responseType = response.get_xmlNodePresentation().getLocalName();
		}
		transactionInstance = saveTransactionInstance(getAddPatientRecordToByteArray(request),
				getAcknowledgementToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
				responseType);
		HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
		if (response != null) {
			HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
		}
		return response;
	}

	/**
	 * <p>sendRevisePatientRecord.</p>
	 *
	 * @param request a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
	 */
	public MCCIIN000002UV01Type sendRevisePatientRecord(PRPAIN201302UV02Type request) throws SoapSendException {
        return sendPRPAIN201302UV02(request, ACTION_REVISE, null);
	}

	/**
	 * <p>sendMergePatientIdentity.</p>
	 *
	 * @param request a {@link net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
	 */
	public MCCIIN000002UV01Type sendMergePatientIdentity(PRPAIN201304UV02Type request) throws SoapSendException {
		transactionInstance = null;
		MCCIIN000002UV01Type response = send(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), ACTION_MERGE);
		String responseType = null;
		if (response != null) {
			responseType = response.get_xmlNodePresentation().getLocalName();
		}
		transactionInstance = saveTransactionInstance(getMergePatientIdentityToByteArray(request),
				getAcknowledgementToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
				responseType);
		HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
		if (response != null) {
			HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
		}
		return response;
	}

	private byte[] getAddPatientRecordToByteArray(PRPAIN201301UV02Type request) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			HL7V3Transformer.marshallMessage(PRPAIN201301UV02Type.class, baos, request);
			return baos.toByteArray();
		} catch (JAXBException e) {
			log.error(e.getMessage(), e);
			return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
		}
	}

	private byte[] getMergePatientIdentityToByteArray(PRPAIN201304UV02Type request) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			HL7V3Transformer.marshallMessage(PRPAIN201304UV02Type.class, baos, request);
			return baos.toByteArray();
		} catch (JAXBException e) {
			log.error(e.getMessage(), e);
			return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
		}
	}
}
