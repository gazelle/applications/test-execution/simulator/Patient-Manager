package net.ihe.gazelle.simulator.pdqv3.pds;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PDQv3PDSGuiManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqv3pdsGuiManager")
@Scope(ScopeType.PAGE)
public class PDQv3PDSGuiManager extends AbstractPDQPDSGuiManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2657551111877383278L;
	private String endpointUrl;
	private String deviceId;
	private String organizationOid;
	
	/**
	 * <p>getUrlForHL7Messages.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForHL7Messages() {
        Actor actor = Actor.findActorWithKeyword(PatientManagerConstants.PDS);
        Transaction transaction = Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_47);
        return "/messages/browser.seam?simulatedActor=" + actor.getId() + "&transaction=" + transaction.getId();
	}

	/** {@inheritDoc} */
	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
		this.endpointUrl = PreferenceService.getString(PatientManagerConstants.PDQV_3_PDS_URL_PREF);
		this.deviceId = PreferenceService.getString(PatientManagerConstants.HL_7_V_3_PDQ_PDS_DEVICE_ID_PREF);
		this.organizationOid = PreferenceService.getString(PatientManagerConstants.HL7V3_ORGANIZATION_OID_PREF);
	}

	/**
	 * <p>Getter for the field <code>endpointUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getEndpointUrl() {
		return endpointUrl;
	}

	/**
	 * <p>Getter for the field <code>deviceId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * <p>Getter for the field <code>organizationOid</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOrganizationOid() {
		return organizationOid;
	}

}
