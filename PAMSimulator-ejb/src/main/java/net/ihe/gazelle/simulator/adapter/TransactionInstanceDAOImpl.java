package net.ihe.gazelle.simulator.adapter;

import net.ihe.gazelle.simulator.application.TransactionInstanceDAO;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.EntityManager;

/**
 * DAO for {@link TransactionInstance}
 */
@Name("transactionInstanceDAO")
public class TransactionInstanceDAOImpl implements TransactionInstanceDAO {

    /**
     * Empty Constructor for injection.
     */
    public TransactionInstanceDAOImpl() {
        //Empty
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TransactionInstance retrieveTransactionInstanceById(String id) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        TransactionInstanceQuery query = new TransactionInstanceQuery(entityManager);
        try {
            query.id().eq(Integer.valueOf(id));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(String.format("Not valid Transaction Instance ID : %s", id));
        }
        return query.getUniqueResult();
    }
}
