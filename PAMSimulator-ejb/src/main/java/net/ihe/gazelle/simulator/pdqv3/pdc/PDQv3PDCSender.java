package net.ihe.gazelle.simulator.pdqv3.pdc;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * <p>PDQv3PDCSender class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDQv3PDCSender extends SoapHL7V3WebServiceClient implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5525858837170285232L;
    private static final String ACTION_FIND = "urn:hl7-org:v3:PRPA_IN201305UV02";
    private static final String ACTION_CONTINUE = "urn:hl7-org:v3:QUQI_IN000003UV01_Continue";
    private static final String ACTION_CANCEL = "urn:hl7-org:v3:QUQI_IN000003UV01_Cancel";
    private static final String SERVICE_NAME = "PDQSupplier_Service";
    private static final String SERVICE_PORT_TYPE = "PDQSupplier_Port_Soap12";

    private static Logger log = LoggerFactory.getLogger(PDQv3PDCSender.class);

    /**
     * <p>Constructor for PDQv3PDCSender.</p>
     *
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public PDQv3PDCSender(HL7v3ResponderSUTConfiguration inSUT) {
        super(inSUT);
        this.transactionInstance = null;
    }

    /** {@inheritDoc} */
    @Override
    protected QName getServiceQName() {
        return new QName(PDQV3_TNS, SERVICE_NAME);
    }

    /** {@inheritDoc} */
    @Override
    protected QName getPortQName() {
        return new QName(PDQV3_TNS, SERVICE_PORT_TYPE);
    }

    /** {@inheritDoc} */
    @Override
    protected Actor getSimulatedActor() {
        return Actor.findActorWithKeyword("PDC");
    }

    /** {@inheritDoc} */
    @Override
    protected Transaction getSimulatedTransaction() {
        return Transaction.GetTransactionByKeyword("ITI-47");
    }

    /** {@inheritDoc} */
    @Override
    protected Actor getSutActor() {
        return Actor.findActorWithKeyword(PatientManagerConstants.PDS);
    }

    /**
     * <p>sendPatientRegistryFindCandidatesQuery.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type sendPatientRegistryFindCandidatesQuery(PRPAIN201305UV02Type request) throws SoapSendException {
       return sendPRPAIN201305UV02Type(request, ACTION_FIND, null);
    }

    /**
     * <p>sendGeneralQueryActivateQueryContinue.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type sendGeneralQueryActivateQueryContinue(QUQIIN000003UV01Type request) throws SoapSendException {
        transactionInstance = null;
        PRPAIN201306UV02Type response = send(PRPAIN201306UV02Type.class, request.get_xmlNodePresentation(), ACTION_CONTINUE);
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
        }
        transactionInstance = saveTransactionInstance(findCandidatesQueryContinueToByteArray(request),
                findCandidatesQueryResponseToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
                responseType);
        HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
        if (response != null) {
            HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
        }
        return response;
    }

    /**
     * <p>sendGeneralQueryActiveQueryCancel.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type sendGeneralQueryActiveQueryCancel(QUQIIN000003UV01CancelType request) throws SoapSendException {
        transactionInstance = null;
        MCCIIN000002UV01Type response = send(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(),
                ACTION_CANCEL);
        String responseType = null;
        byte[] responseContent = null;
        byte[] requestContent = findCandidatesQueryCancelToByteArray(request);
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
            responseContent = acknowledgementToByteArray(response);
        }
        transactionInstance = saveTransactionInstance(requestContent,
                responseContent, request.get_xmlNodePresentation().getLocalName(), responseType);
        HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
        if (response != null) {
            HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
        }
        return response;
    }


    private byte[] findCandidatesQueryContinueToByteArray(QUQIIN000003UV01Type request) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(QUQIIN000003UV01Type.class, baos, request);
            return baos.toByteArray();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
        }
    }

    private byte[] findCandidatesQueryCancelToByteArray(QUQIIN000003UV01CancelType request) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(QUQIIN000003UV01CancelType.class, baos, request);
            return baos.toByteArray();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
        }
    }

}
