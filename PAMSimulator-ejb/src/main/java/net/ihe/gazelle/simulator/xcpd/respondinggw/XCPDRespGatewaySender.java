package net.ihe.gazelle.simulator.xcpd.respondinggw;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.io.Serializable;

/**
 * <p>XCPDRespGatewaySender class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XCPDRespGatewaySender extends SoapHL7V3WebServiceClient implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5525858837170285232L;
	private static final String SERVICE_NAME = "InitiatingGateway_Service";
	private static final String SERVICE_PORT_TYPE = "InitiatingGateway_Port_Soap12";
    private static final String ACTION_RESPONSE_DEFERRED = "urn:hl7-org:v3:PRPA_IN201306UV02:Deferred:CrossGatewayPatientDiscovery";

	/**
	 * <p>Constructor for XCPDRespGatewaySender.</p>
	 *
	 * @param inURL a {@link java.lang.String} object.
	 * @param inSutName a {@link java.lang.String} object.
	 */
	public XCPDRespGatewaySender(String inURL, String inSutName) {
		super(null);
        setSutUrl(inURL);
        setSutName(inSutName);
	}

	/** {@inheritDoc} */
	@Override
	protected QName getServiceQName() {
		return new QName(XCPD_TNS, SERVICE_NAME);
	}

	/** {@inheritDoc} */
	@Override
	protected QName getPortQName() {
		return new QName(XCPD_TNS, SERVICE_PORT_TYPE);
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSimulatedActor() {
		return Actor.findActorWithKeyword("RESP_GW");
	}

	/** {@inheritDoc} */
	@Override
	protected Transaction getSimulatedTransaction() {
		return Transaction.GetTransactionByKeyword("ITI-55");
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSutActor() {
		return Actor.findActorWithKeyword("INIT_GW");
	}

    /**
     * <p>sendDeferredCrossGatewayPatientDiscoveryResponse.</p>
     *
     * @param message a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type sendDeferredCrossGatewayPatientDiscoveryResponse(PRPAIN201306UV02Type message) throws SoapSendException {
        transactionInstance = null;
        MCCIIN000002UV01Type response = send(MCCIIN000002UV01Type.class, message.get_xmlNodePresentation(), ACTION_RESPONSE_DEFERRED);
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
        }
        transactionInstance = saveTransactionInstance(findCandidatesQueryResponseToByteArray(message),
                acknowledgementToByteArray(response), message.get_xmlNodePresentation().getLocalName(),
                responseType);
        transactionInstance.getRequest().setType("PRPA_IN201306UV02");
		HL7V3Utils.saveMessageId(message.getId(), transactionInstance.getRequest());
		if (response != null) {
			HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
		}
        return response;
    }


}
