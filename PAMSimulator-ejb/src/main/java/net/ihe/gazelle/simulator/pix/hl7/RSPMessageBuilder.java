package net.ihe.gazelle.simulator.pix.hl7;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.RSP_K23;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * <p>RSPMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RSPMessageBuilder extends SegmentBuilder {

	private List<PatientIdentifier> identifiers;
	private List<Integer> domainReps;
	private QBP_Q21 incomingMessage;

	/**
	 * <p>Constructor for RSPMessageBuilder.</p>
	 *
	 * @param inIdentifiers a {@link java.util.List} object.
	 * @param inDomainReps a {@link java.util.List} object.
	 * @param inIncomingMessage a {@link net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21} object.
	 */
	public RSPMessageBuilder(List<PatientIdentifier> inIdentifiers, List<Integer> inDomainReps,
			QBP_Q21 inIncomingMessage) {
		this.identifiers = inIdentifiers;
		this.domainReps = inDomainReps;
		this.incomingMessage = inIncomingMessage;
	}

	/**
	 * <p>build.</p>
	 *
	 * @param status a {@link net.ihe.gazelle.simulator.pix.hl7.QueryResponseStatus} object.
	 * @param sendingApplication a {@link java.lang.String} object.
	 * @param sendingFacility a {@link java.lang.String} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 * @throws ca.uhn.hl7v2.HL7Exception if any.
	 */
	public Message build(QueryResponseStatus status, String sendingApplication, String sendingFacility)
			throws HL7Exception {
		RSP_K23 rspMessage = new RSP_K23();

		fillMSHSegment(rspMessage.getMSH(), incomingMessage.getMSH().getSendingApplication().getNamespaceID()
				.getValue(), incomingMessage.getMSH().getSendingFacility().getNamespaceID().getValue(),
				sendingApplication, sendingFacility, "RSP", "K23", "RSP_K23", "2.5", new SimpleDateFormat(
						"yyyyMMddHHmmss"), incomingMessage.getMSH().getCharacterSet(0).getValue());
		fillMSASegment(rspMessage.getMSA(), incomingMessage.getMSH().getMessageControlID().getValue(),
				status.getMSACode());
		if (status.getMSACode().equals(AcknowledgmentCode.AE)) {
			// fill ERR segment
			if (status.getComponentNumber() != null) {
				rspMessage.getERR().getErrorLocation(0).getSegmentID().setValue("QPD");
				rspMessage.getERR().getErrorLocation(0).getSegmentSequence().setValue("1");
				rspMessage.getERR().getErrorLocation(0).getFieldPosition().setValue("3");
				rspMessage.getERR().getErrorLocation(0).getFieldRepetition().setValue("1");
				rspMessage.getERR().getErrorLocation(0).getComponentNumber().setValue(status.getComponentNumber());
			} else {
				int index = 0;
				for (Integer rep : domainReps) {
					rspMessage.getERR().getErrorLocation(index).getSegmentID().setValue("QPD");
					rspMessage.getERR().getErrorLocation(index).getSegmentSequence().setValue("1");
					rspMessage.getERR().getErrorLocation(index).getFieldPosition().setValue("4");
					rspMessage.getERR().getErrorLocation(index).getFieldRepetition().setValue(rep.toString());
					index++;
				}
			}
			rspMessage.getERR().getHL7ErrorCode().getIdentifier().setValue("204");
			rspMessage.getERR().getHL7ErrorCode().getText().setValue("unknown key identifier");
			rspMessage.getERR().getHL7ErrorCode().getNameOfCodingSystem().setValue("HL7");
			rspMessage.getERR().getSeverity().setValue("E");
		} else if (status.getQAKCode().equals("OK")) {
			int rep = 0;
			for (PatientIdentifier pid : identifiers) {
				rspMessage.getQUERY_RESPONSE().getPID().getPatientIdentifierList(rep).parse(pid.getFullPatientId());
				rep++;
			}
			rspMessage.getQUERY_RESPONSE().getPID().getPatientName(0); // create the first empty repetition in order to be able to access the second repetition
			rspMessage.getQUERY_RESPONSE().getPID().getPatientName(1).getNameTypeCode().setValue("S");
		}// else no other segment to fill out
		rspMessage.getQAK().getQueryResponseStatus().setValue(status.getQAKCode());
		rspMessage.getQAK().getQueryTag().setValue(Terser.get(incomingMessage.getQPD(), 2, 0, 1, 1));
		rspMessage.getQPD().parse(incomingMessage.getQPD().encode());
		return rspMessage;
	}


}
