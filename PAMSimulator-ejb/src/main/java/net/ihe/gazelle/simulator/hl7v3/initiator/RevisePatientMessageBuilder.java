package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02MFMIMT700701UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02MFMIMT700701UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02MFMIMT700701UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientPatientPerson;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientStatusCode;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.ParticipationTargetSubject;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.hl7v3.responder.COCTMT150003UV03PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>Abstract RevisePatientMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class RevisePatientMessageBuilder extends HL7v3MessageBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(RevisePatientMessageBuilder.class);
    private static final String SUBJECT_TYPE_CODE = "SUBJ";
    private static final String PATIENT_CLASS_CODE = "PAT";

    protected HL7v3ResponderSUTConfiguration sut;
    protected MessageIdGenerator generator = null;
    protected SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
    protected Patient activePatient = null;

    /**
     * <p>Constructor for RevisePatientMessageBuilder.</p>
     *
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     * @param active a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public RevisePatientMessageBuilder(HL7v3ResponderSUTConfiguration inSUT, Patient active) {
        this.sut = inSUT;
        this.activePatient = active;
    }

    /**
     * <p>buildRevisePatientRecordMessage.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
     */
    public PRPAIN201302UV02Type buildRevisePatientRecordMessage() {
        PRPAIN201302UV02Type request = new PRPAIN201302UV02Type();
        request.setITSVersion(VERSION);
        PRPAIN201302UV02MFMIMT700701UV01ControlActProcess controlActProcess = new PRPAIN201302UV02MFMIMT700701UV01ControlActProcess();
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201302UV02", INTERACTION_ID_NAMESPACE, null));

        generator = MessageIdGenerator.getGeneratorForActor(getActorKeyword());
        if (generator != null) {
            request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
            request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid()));
        } else {
            LOG.error("No MessageIdGenerator instance found for actor " + getActorKeyword());
            request.setSender(MCCIMT000100UV01PartBuilder.buildSender((PreferenceService
                    .getString(getOIDPreferenceName()))));
        }
        request.setCreationTime(new TS(sdfDateTime.format(new Date())));
        request.setInteractionId(new II(INTERACTION_ID_NAMESPACE, "PRPA_IN201302UV02"));
        request.setProcessingCode(new CS("T", null, null));
        request.setProcessingModeCode(new CS("T", null, null));
        request.setAcceptAckCode(new CS("AL", null, null));
        request.addReceiver(MCCIMT000100UV01PartBuilder.buildReceiver(sut));
        populateControlActProcess(controlActProcess);
        request.setControlActProcess(controlActProcess);
        return request;
    }


    private void populateControlActProcess(PRPAIN201302UV02MFMIMT700701UV01ControlActProcess controlActProcess) {
        PRPAIN201302UV02MFMIMT700701UV01Subject1 subject1 = new PRPAIN201302UV02MFMIMT700701UV01Subject1();
        subject1.setTypeCode(SUBJECT_TYPE_CODE);
        subject1.setContextConductionInd(false);
        subject1.setRegistrationEvent(populateRegistrationEventForRevisePatientRecord());
        controlActProcess.addSubject(subject1);
    }

    private PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent populateRegistrationEventForRevisePatientRecord() {
        PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent event = new PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent();
        event.setClassCode(ActClass.REG);
        event.setMoodCode(ActMood.EVN);
        event.setStatusCode(new CS("active", null, null));
        event.setSubject1(populateSubject1ForRevisePatientRecord());
        event.setCustodian(MFMIMT700701UV01PartBuilder.populateCustodian());

        return event;
    }

    private PRPAIN201302UV02MFMIMT700701UV01Subject2 populateSubject1ForRevisePatientRecord() {
        PRPAIN201302UV02MFMIMT700701UV01Subject2 subject2 = new PRPAIN201302UV02MFMIMT700701UV01Subject2();
        subject2.setTypeCode(ParticipationTargetSubject.SBJ);
        subject2.setPatient(populatePatientForRevisePatientRecord());
        return subject2;
    }

    private PRPAMT201302UV02Patient populatePatientForRevisePatientRecord() {
        PRPAMT201302UV02Patient patient = new PRPAMT201302UV02Patient();
        patient.setStatusCode(new PRPAMT201302UV02PatientStatusCode("active", null, null));
        patient.setClassCode(PATIENT_CLASS_CODE);
        patient.setProviderOrganization(COCTMT150003UV03PartBuilder.populateProviderOrganization(
                "http://gazelle.ihe.net", false));
        for (PatientIdentifier pid : PatientIdentifierDAO.getPatientIdentifiers(activePatient)) {
            addPatientIdToList(patient, pid);
        }
        patient.setPatientPerson(populatePatientPersonForRevisePatientRecord());
        return patient;
    }

    /**
     * <p>populatePatientPersonForRevisePatientRecord.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientPatientPerson} object.
     */
    protected abstract PRPAMT201302UV02PatientPatientPerson populatePatientPersonForRevisePatientRecord();

    /**
     * <p>addPatientIdToList.</p>
     *
     * @param patient a {@link net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient} object.
     * @param pid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    protected abstract void addPatientIdToList(PRPAMT201302UV02Patient patient, PatientIdentifier pid);

    /**
     * <p>getActorKeyword.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    protected abstract String getActorKeyword();

    /**
     * <p>getOIDPreferenceName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    protected abstract String getOIDPreferenceName();
}
