package net.ihe.gazelle.simulator.pam.iti31.model;

import net.ihe.gazelle.simulator.pam.pes.Selection;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
/**
 * <p>EncounterManagementEvent class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("encounterManagementEvent")
@Table(name = "pam_encounter_management_event", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
		"event_name", "page_id" }))
@SequenceGenerator(name = "pam_encounter_management_event_sequence", sequenceName = "pam_encounter_management_event_id_seq", allocationSize = 1)
public class EncounterManagementEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8407324183245099909L;

	@Id
	@GeneratedValue(generator = "pam_encounter_management_event_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	/**
	 * Category of event
	 */
	@Column(name = "event_name")
	private String eventName;

	/**
	 * trigger used for INSERT action
	 */
	@Column(name = "insert_trigger_event")
	private String insertTriggerEvent;

	/**
	 * trigger used for CANCEL action
	 */
	@Column(name = "cancel_trigger_event")
	private String cancelTriggerEvent;

	/**
	 * trigger used for UPDATE action
	 */
	@Column(name = "update_trigger_event")
	private String updateTriggerEvent;

	@Column(name = "available")
	private Boolean available;

	@Column(name = "page_id")
	private String pageId;

    @Column(name="selection")
	@Enumerated(EnumType.STRING)
    private Selection selection;

	/**
	 * <p>Constructor for EncounterManagementEvent.</p>
	 */
	public EncounterManagementEvent() {

	}

    /**
     * <p>Getter for the field <code>selection</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.pes.Selection} object.
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * <p>Setter for the field <code>selection</code>.</p>
     *
     * @param selection a {@link net.ihe.gazelle.simulator.pam.pes.Selection} object.
     */
    public void setSelection(Selection selection) {
        this.selection = selection;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>eventName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * <p>Setter for the field <code>eventName</code>.</p>
	 *
	 * @param eventName a {@link java.lang.String} object.
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * <p>Getter for the field <code>insertTriggerEvent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getInsertTriggerEvent() {
		return insertTriggerEvent;
	}

	/**
	 * <p>Setter for the field <code>insertTriggerEvent</code>.</p>
	 *
	 * @param insertTriggerEvent a {@link java.lang.String} object.
	 */
	public void setInsertTriggerEvent(String insertTriggerEvent) {
		this.insertTriggerEvent = insertTriggerEvent;
	}

	/**
	 * <p>Getter for the field <code>cancelTriggerEvent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCancelTriggerEvent() {
		return cancelTriggerEvent;
	}

	/**
	 * <p>Setter for the field <code>cancelTriggerEvent</code>.</p>
	 *
	 * @param cancelTriggerEvent a {@link java.lang.String} object.
	 */
	public void setCancelTriggerEvent(String cancelTriggerEvent) {
		this.cancelTriggerEvent = cancelTriggerEvent;
	}

	/**
	 * <p>Getter for the field <code>updateTriggerEvent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUpdateTriggerEvent() {
		return updateTriggerEvent;
	}

	/**
	 * <p>Setter for the field <code>updateTriggerEvent</code>.</p>
	 *
	 * @param updateTriggerEvent a {@link java.lang.String} object.
	 */
	public void setUpdateTriggerEvent(String updateTriggerEvent) {
		this.updateTriggerEvent = updateTriggerEvent;
	}

	/**
	 * <p>Setter for the field <code>available</code>.</p>
	 *
	 * @param available a {@link java.lang.Boolean} object.
	 */
	public void setAvailable(Boolean available) {
		this.available = available;
	}

	/**
	 * <p>Getter for the field <code>available</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getAvailable() {
		return available;
	}

	/**
	 * <p>Setter for the field <code>pageId</code>.</p>
	 *
	 * @param pageId a {@link java.lang.String} object.
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	/**
	 * <p>Getter for the field <code>pageId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPageId() {
		return pageId;
	}


	/**
	 * <p>listEventsForPage.</p>
	 *
	 * @param pageId a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<EncounterManagementEvent> listEventsForPage(String pageId) {
        EncounterManagementEventQuery query = new EncounterManagementEventQuery();
        query.pageId().eq(pageId);
        query.available().eq(true);
        query.eventName().order(true);
		return query.getListNullIfEmpty();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((eventName == null) ? 0 : eventName.hashCode());
		result = (prime * result) + ((pageId == null) ? 0 : pageId.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EncounterManagementEvent other = (EncounterManagementEvent) obj;
		if (eventName == null) {
			if (other.eventName != null) {
				return false;
			}
		} else if (!eventName.equals(other.eventName)) {
			return false;
		}
		if (pageId == null) {
			if (other.pageId != null) {
				return false;
			}
		} else if (!pageId.equals(other.pageId)) {
			return false;
		}
		return true;
	}

}
