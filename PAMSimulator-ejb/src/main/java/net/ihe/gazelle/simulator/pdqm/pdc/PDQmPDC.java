package net.ihe.gazelle.simulator.pdqm.pdc;

import ca.uhn.fhir.rest.gclient.IQuery;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfigurationQuery;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.fhir.util.FhirReturnType;
import net.ihe.gazelle.simulator.fhir.util.GazelleFhirClient;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDC;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>PDQmPDC class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqmpdc")
@Scope(ScopeType.PAGE)
public class PDQmPDC extends AbstractPDQPDC implements Serializable {

    /**
     *
     */
    private static final Logger LOG = LoggerFactory.getLogger(PDQmPDC.class);
    private static final long serialVersionUID = -3989360713596354135L;


    public static final int FIRST_PAGE = 1;

    private transient GazelleFhirClient fhirClient;
    private List<TransactionInstance> messages;
    private FHIRResponderSUTConfiguration selectedSUT;
    private List<FHIRResponderSUTConfiguration> availableSystems;
    private FhirReturnType responseType;
    private Bundle response;
    private transient PDQmQueryBuilder builder;


    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage() {
        if (selectedSUT != null) {
            if (!limit) {
                limitValue = null;
            }
            pageNumber = FIRST_PAGE;
            builder = new PDQmQueryBuilder(patientCriteria, domainsReturned, limitValue, selectedSUT);
            fhirClient = new GazelleFhirClient(builder, selectedSUT, responseType, PatientManagerConstants.PDC, PatientManagerConstants.ITI_78,
                    PatientManagerConstants.PDS);
            buildAndSendMessage();
            if (response != null) {
                parseResponse();
            }
            if (messages == null) {
                messages = new ArrayList<TransactionInstance>();
            }
            messages = fhirClient.getMessages();
            displayCriteriaForm = false;
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,
                    "Please, first select the system under test to which you want to send the message");
        }
    }

    private void parseResponse() {
        List<net.ihe.gazelle.simulator.pam.model.Patient> patientsFromResponse = FhirResourceParser.getPatientsFromBundle(response);
        if (!patientsFromResponse.isEmpty()) {
            selectedPage = pageNumber;
            if (receivedPatients == null) {
                receivedPatients = new HashMap<Integer, List<Patient>>();
            }
            receivedPatients.put(pageNumber, patientsFromResponse);
            Bundle.BundleLinkComponent link = response.getLink(Bundle.LINK_NEXT);
            moreResultsAvailable = (link != null);
            receivedPatients.put(pageNumber, patientsFromResponse);
        } else {
            FacesMessages.instance().add("No patient received from supplier");
        }
    }

    /**
     * @return returns true if the response is a instance of Resource and then can be parsed to extract Patients
     */
    private void buildAndSendMessage() {
        if (pageNumber == FIRST_PAGE) {
            IQuery<IBaseBundle> query = fhirClient.initiateSearchQuery(org.hl7.fhir.dstu3.model.Patient.class);
            builder.populatePDQmQuery(query);
            response = fhirClient.executeSearchQuery(query, FacesMessages.instance());
        } else {
            response = fhirClient.getNextResults(response, FacesMessages.instance());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getNextResults() {
        nextPage();
        buildAndSendMessage();
        parseResponse();
        displayCriteriaForm = false;
    }


    /**
     * <p>initializeRequest.</p>
     */
    @Create
    public void initializeRequest() {
        super.initPage();
        if (selectedTransaction == null) {
            selectedTransaction = Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_78);
        }
        if (availableSystems == null || availableSystems.isEmpty()) {
            FHIRResponderSUTConfigurationQuery query = new FHIRResponderSUTConfigurationQuery();
            query.listUsages().transaction().eq(selectedTransaction);
            query.name().order(true);
            availableSystems = query.getList();
        }
        initializePatientCriteria();
        currentDomainReturned = new AssigningAuthority();
        messages = null;
        receivedPatients = null;
        displayCriteriaForm = true;
        cancellation = null;
        responseType = FhirReturnType.XML;
    }

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<FHIRResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>Getter for the field <code>messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getMessages() {
        return messages;
    }

    /**
     * <p>Getter for the field <code>selectedSUT</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration} object.
     */
    public FHIRResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     * <p>Setter for the field <code>selectedSUT</code>.</p>
     *
     * @param selectedSUT a {@link net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration} object.
     */
    public void setSelectedSUT(FHIRResponderSUTConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    /**
     * <p>Getter for the field <code>responseType</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public FhirReturnType getResponseType() {
        return responseType;
    }

    /**
     * <p>Setter for the field <code>responseType</code>.</p>
     *
     * @param responseType a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public void setResponseType(FhirReturnType responseType) {
        this.responseType = responseType;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelQuery() {
        // no cancel query defined for PDQm
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setLimitNumberForTestData(TestData testData) {
        testData.setLimitValue(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setFhirResponseFormatForTestData(TestData testData) {
        testData.setFhirReturnType(responseType);
    }

    /**
     * <p>getAvailableResponseTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getAvailableResponseTypes() {
        return FhirReturnType.getFhirReturnTypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initializePageUsingTestData(TestData inTestData) {
        super.initializePageUsingTestData(inTestData);
        this.responseType = inTestData.getFhirReturnType();
    }
}
