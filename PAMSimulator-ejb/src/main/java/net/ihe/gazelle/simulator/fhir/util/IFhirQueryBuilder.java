package net.ihe.gazelle.simulator.fhir.util;

/**
 * <p>IFhirQueryBuilder class.</p>
 *
 * @author aberge
 * @version 1.0: 13/11/17
 */

public interface IFhirQueryBuilder {

    String getRequestType();

}
