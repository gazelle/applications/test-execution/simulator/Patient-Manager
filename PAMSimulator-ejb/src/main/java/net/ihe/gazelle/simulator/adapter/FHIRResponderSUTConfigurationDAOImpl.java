package net.ihe.gazelle.simulator.adapter;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfigurationQuery;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * DAO for {@link FHIRResponderSUTConfiguration}
 */
@Name("fhirResponderSUTConfigurationDAO")
public class FHIRResponderSUTConfigurationDAOImpl implements net.ihe.gazelle.simulator.application.FHIRResponderSUTConfigurationDAO {

    /**
     * Empty Constructor for injection.
     */
    public FHIRResponderSUTConfigurationDAOImpl() {
        //Empty
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FHIRResponderSUTConfiguration> retrieveSUTByTransaction(String transactionKeyword) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        FHIRResponderSUTConfigurationQuery query = new FHIRResponderSUTConfigurationQuery(entityManager);
        query.listUsages().transaction().keyword().eq(transactionKeyword);
        query.name().order(true);
        return query.getList();
    }
}
