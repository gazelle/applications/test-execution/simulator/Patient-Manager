package net.ihe.gazelle.simulator.fhir.util;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.fhir.context.FhirContextProvider;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>GazelleFhirClient class.</p>
 *
 * @author aberge
 * @version 1.0: 13/11/17
 */

public class GazelleFhirClient {

    private static final Logger LOG = LoggerFactory.getLogger(GazelleFhirClient.class);

    private static final String ITI = "ITI";
    private IGenericClient genericClient;
    private FhirReturnType responseType;
    private String sendingActorKeyword;
    private String receivingActorKeyword;
    private String transactionKeyword;
    private IFhirQueryBuilder queryBuilder;
    private List<TransactionInstance> messages;
    private FHIRResponderSUTConfiguration selectedSUT;
    private GazelleClientInterceptor loggingInterceptor;

    public GazelleFhirClient(IFhirQueryBuilder inQueryBuilder, FHIRResponderSUTConfiguration inSUT, FhirReturnType
            inResponseType, String simulatedActorKeyword, String simulatedTransaction, String sutActorKeyword) {
        instanciateDSTU3Client(inSUT);
        this.queryBuilder = inQueryBuilder;
        this.selectedSUT = inSUT;
        this.responseType = inResponseType;
        this.sendingActorKeyword = simulatedActorKeyword;
        this.receivingActorKeyword = sutActorKeyword;
        this.transactionKeyword = simulatedTransaction;
        this.genericClient.setEncoding(responseType.getHapiEnum());
    }

    private void instanciateDSTU3Client(FHIRResponderSUTConfiguration sut) {
        FhirContext context = FhirContextProvider.instance();

        // Create a logging interceptor
        loggingInterceptor = new GazelleClientInterceptor();
        context.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        this.genericClient = context.newRestfulGenericClient(sut.getEndpoint());
        this.genericClient.registerInterceptor(loggingInterceptor);
    }

    public List<TransactionInstance> getMessages() {
        return messages;
    }

    private void saveTransactionInstanceForUnknownResponse(IBaseResource receivedResource) {
        String responseMessageType = "Unknown";
        saveTransactionInstance(responseMessageType, receivedResource);
    }

    private void saveTransactionInstanceWithOperationOutcome(OperationOutcome outcome) {
        String responseMessageType = FhirConstants.OPERATION_OUTCOME;
        saveTransactionInstance(responseMessageType, outcome);
    }

    private void saveTransactionInstanceWithBundle(Bundle response) {
        String responseMessageType = FhirConstants.PDQM_RESPONSE;
        saveTransactionInstance(responseMessageType, response);
    }

    private void saveTransactionInstanceWithParameters(Parameters response) {
        String responseMessageType = FhirConstants.PIXM_RESPONSE;
        saveTransactionInstance(responseMessageType, response);
    }

    private void saveTransactionInstance(String responseMessageType, IBaseResource resource) {

        String requestContent = loggingInterceptor.getRequestContent();
        TransactionInstance message = new TransactionInstance();
        message.setDomain(Domain.getDomainByKeyword(ITI));
        message.setSimulatedActor(Actor.findActorWithKeyword(sendingActorKeyword));
        MessageInstance request = new MessageInstance();

        request.setContent(requestContent);
        request.setIssuer("PatientManager");
        request.setType(queryBuilder.getRequestType());
        request.setIssuingActor(Actor.findActorWithKeyword(sendingActorKeyword));
        message.setRequest(request);
        MessageInstance responseMessageInstance = new MessageInstance();
        responseMessageInstance.setIssuingActor(Actor.findActorWithKeyword(receivingActorKeyword));
        responseMessageInstance.setIssuer(selectedSUT.getName());
        if (resource != null) {
            responseMessageInstance.setType(responseMessageType);

            switch (responseType) {
                case XML:
                    message.setStandard(EStandard.FHIR_XML);
                    responseMessageInstance.setContent(FhirParserProvider.getXmlParser().encodeResourceToString(resource));
                    break;
                case JSON:
                    message.setStandard(EStandard.FHIR_JSON);
                    responseMessageInstance.setContent(FhirParserProvider.getJsonParser().encodeResourceToString
                            (resource));
                    break;
                default:
                    LOG.error("Not a supported response type");
            }
        } else {
            String responseTypeFromQuery = getQueryMap(request.getContentAsString()).get("_format");
            switch (responseTypeFromQuery) {
                case "xml":
                    message.setStandard(EStandard.FHIR_XML);
                    break;
                case "json":
                    message.setStandard(EStandard.FHIR_JSON);
                    break;
                default:
                    LOG.error("Not a supported response type");
            }
            responseMessageInstance.setType("HTTP Code " + loggingInterceptor.getHttpCode());
        }
        message.setResponse(responseMessageInstance);
        message.setTimestamp(new Date());
        message.setTransaction(Transaction.GetTransactionByKeyword(transactionKeyword));
        message.setVisible(true);
        message = message.save(EntityManagerService.provideEntityManager());
        if (messages == null) {
            messages = new ArrayList<TransactionInstance>();
        }
        messages.add(message);
    }

    public Parameters executeOperation(Parameters parameters, String operation, FacesMessages facesMessages) {
        IBaseResource response;
        try {
            response = genericClient
                    .operation()
                    .onType(Patient.class)
                    .named(operation)
                    .withParameters(parameters)
                    .useHttpGet() // Use HTTP GET instead of POST
                    .execute();
        } catch (BaseServerResponseException e) {
            response = e.getOperationOutcome();
        }
        if (response instanceof Parameters) {
            Parameters outgoingParameters = (Parameters) response;
            saveTransactionInstanceWithParameters(outgoingParameters);
            return outgoingParameters;
        } else {
            handleException(response, facesMessages);
            return null;
        }
    }

    public Bundle executeSearchQuery(IQuery<IBaseBundle> query, FacesMessages facesMessages) {
        IBaseResource response;
        try {
            response = query.returnBundle(Bundle.class).execute();
        } catch (BaseServerResponseException e) {
            response = e.getOperationOutcome();
        } catch (DataFormatException e) {
            facesMessages.add(StatusMessage.Severity.ERROR, e.getMessage());
            response = null;
        }
        return getBundle(facesMessages, response);
    }

    private Bundle getBundle(FacesMessages facesMessages, IBaseResource response) {
        if (response == null) {
            return null;
        } else if (response instanceof Bundle) {
            Bundle bundle = (Bundle) response;
            saveTransactionInstanceWithBundle(bundle);
            return bundle;
        } else {
            handleException(response, facesMessages);
            return null;
        }
    }

    public Bundle getNextResults(Bundle previousResponse, FacesMessages facesMessages) {
        IBaseResource response;
        try {
            Bundle.BundleLinkComponent nextPageLink = previousResponse.getLink(Bundle.LINK_NEXT);
            if (nextPageLink != null) {
                response = genericClient.loadPage().next(previousResponse).execute();
            } else {
                return null;
            }
        } catch (BaseServerResponseException e) {
            response = e.getOperationOutcome();
        }
        return getBundle(facesMessages, response);
    }

    private void handleException(IBaseResource exceptionResponse, FacesMessages facesMessages) {
        if (exceptionResponse instanceof OperationOutcome) {
            OperationOutcome outcome = (OperationOutcome) exceptionResponse;
            saveTransactionInstanceWithOperationOutcome(outcome);
            facesMessages.add(StatusMessage.Severity.WARN, "Outcome operation message received from supplier");
            facesMessages.instance().add(StatusMessage.Severity.WARN, outcome.getIssueFirstRep().getDiagnostics());
        } else {
            saveTransactionInstanceForUnknownResponse(exceptionResponse);
            facesMessages.instance().add(StatusMessage.Severity.WARN, "Unexpected response received");
        }
    }

    public PatientFhirIHE read(String patientId, FacesMessages facesMessages) {
        try {
            return genericClient.read().resource(PatientFhirIHE.class).withId(patientId).execute();
        } catch (BaseServerResponseException e) {
            facesMessages.add(StatusMessage.Severity.WARN, e.getMessage());
            return null;
        }
    }

    public IQuery<IBaseBundle> initiateSearchQuery(Class resourceClass) {
        return genericClient.search().forResource(resourceClass);
    }

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }
}
