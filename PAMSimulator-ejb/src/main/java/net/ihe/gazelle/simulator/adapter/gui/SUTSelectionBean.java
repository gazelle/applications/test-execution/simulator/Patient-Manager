package net.ihe.gazelle.simulator.adapter.gui;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import java.util.List;

/**
 * Interface necessary for a Bean to control the sutSelection.xhtml template.
 *
 * @param <T> {@link SystemConfiguration} of the systems available for the simulation.
 */
public interface SUTSelectionBean<T extends SystemConfiguration> {

    /**
     * Get selected SUT to display.
     *
     * @return the selected SUT configuration
     */
    T getSelectedSUT();

    /**
     * Set the value of selectedSUT property.
     *
     * @param selectedSUT value to set to the property.
     */
    void setSelectedSUT(T selectedSUT);

    /**
     * Get the list of available systems.
     *
     * @return the list of available system to display.
     */
    List<T> getAvailableSystems();

    /**
     * Get the path to the simulator diagram.
     *
     * @return the litteral path to the diagram.
     */
    String getDiagram();
}
