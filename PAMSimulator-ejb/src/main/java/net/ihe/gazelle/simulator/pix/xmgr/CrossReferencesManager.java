package net.ihe.gazelle.simulator.pix.xmgr;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import net.ihe.gazelle.simulator.pixv3.manager.ITI46ManagerLocal;
import net.ihe.gazelle.simulator.pixv3.manager.ITI46MessageBuilder;
import net.ihe.gazelle.simulator.pixv3.manager.PIXv3ManagerSender;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>CrossReferencesManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("crossReferencesManager")
@Scope(ScopeType.PAGE)
public class CrossReferencesManager implements Serializable, QueryModifier<Patient> {

    /**
     *
     */
    private static final long serialVersionUID = 7166314149461401260L;
    /** Constant <code>PAT_IDENTITY_X_REF_MGR="PAT_IDENTITY_X_REF_MGR"</code> */
    public static final String PAT_IDENTITY_X_REF_MGR = "PAT_IDENTITY_X_REF_MGR";
    /** Constant <code>PAT_IDENTITY_CONSUMER="PAT_IDENTITY_CONSUMER"</code> */
    public static final String PAT_IDENTITY_CONSUMER = "PAT_IDENTITY_CONSUMER";
    private static String ITI10 = "ITI-10";
    private static String ITI46 = "ITI-46";

    private Filter<Patient> filter;
    private FilterDataModel<Patient> patients;
    private Patient selectedPatient;
    private CrossReference selectedReference;
    private String firstName;
    private String lastName;
    private String motherMaidenName;
    private HL7V2ResponderSUTConfiguration sut;
    private List<TransactionInstance> hl7Messages = new ArrayList<TransactionInstance>();
    private List<HierarchicDesignator> selectedAuthorities;
    private List<HierarchicDesignator> availableAuhorities;
    private boolean manageIdentities;
    private boolean sendNotificationToSut;
    private static final String FACILITY = "ITI";
    private static final String APPLICATION = "PatientManager";
    private List<HL7V2ResponderSUTConfiguration> availableHL7v2Systems;
    ;
    protected Transaction selectedTransaction;
    private List<Transaction> transactions;

    /**
     * <p>isSendNotificationToSut.</p>
     *
     * @return a boolean.
     */
    public boolean isSendNotificationToSut() {
        return sendNotificationToSut;
    }

    /**
     * <p>Setter for the field <code>sendNotificationToSut</code>.</p>
     *
     * @param sendNotificationToSut a boolean.
     */
    public void setSendNotificationToSut(boolean sendNotificationToSut) {
        this.sendNotificationToSut = sendNotificationToSut;
    }

    /**
     * <p>Getter for the field <code>transactions</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * <p>Setter for the field <code>transactions</code>.</p>
     *
     * @param transactions a {@link java.util.List} object.
     */
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * <p>Getter for the field <code>selectedTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getSelectedTransaction() {
        return selectedTransaction;
    }

    /**
     * <p>Setter for the field <code>selectedTransaction</code>.</p>
     *
     * @param selectedTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    /**
     * <p>init.</p>
     */
    @Create
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.get("manageIdentities") != null) {
            setManageIdentities(Boolean.parseBoolean(params.get("manageIdentities")));
        } else {
            setManageIdentities(false);
        }
        if (params.get("transaction") != null) {
            selectedTransaction = Transaction.GetTransactionByKeyword(params.get("transaction"));
        }
        if (selectedTransaction == null) {
            selectedTransaction = Transaction.GetTransactionByKeyword(ITI10);
        }
        sendNotificationToSut = !manageIdentities;
        transactions = new ArrayList<Transaction>();
        transactions.add(Transaction.GetTransactionByKeyword(ITI10));
        transactions.add(Transaction.GetTransactionByKeyword(ITI46));
        if (ITI46.equals(selectedTransaction.getKeyword())){
            ITI46ManagerLocal manager = (ITI46ManagerLocal) Component.getInstance("iti46Manager");
            manager.refreshListOfSystems();
        }
        availableHL7v2Systems = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(ITI10, HL7Protocol.MLLP);
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> map) {
        if ((firstName != null) && !firstName.isEmpty()) {
            queryBuilder.addLike("firstName", firstName);
        }
        if ((lastName != null) && !lastName.isEmpty()) {
            queryBuilder.addLike("lastName", lastName);
        }
        if ((motherMaidenName != null) && !motherMaidenName.isEmpty()) {
            queryBuilder.addLike("motherMaidenName", motherMaidenName);
        }
    }

    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSelectedPatient(Patient selectedPatient) {
        PatientQuery query = new PatientQuery();
        query.id().eq(selectedPatient.getId());
        this.selectedPatient = query.getUniqueResult();
        selectedReference = this.selectedPatient.getPixReference();
    }

    /**
     * <p>resetPatientChoice.</p>
     */
    public void resetPatientChoice() {
        selectedPatient = null;
        selectedReference = null;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Patient> getFilter() {
        if (filter == null) {
            filter = new Filter<Patient>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance()
                    .getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Patient> getHQLCriterionsForFilter() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("active", query.stillActive(), true);
        criterions.addPath("simulatedActor", query.simulatedActor().keyword(), PAT_IDENTITY_X_REF_MGR);
        criterions.addPath("creator", query.creator());
        criterions.addPath("date", query.creationDate());
        criterions.addPath("genderCode", query.genderCode());
        criterions.addQueryModifier(this);
        return criterions;
    }

    /**
     * <p>Getter for the field <code>patients</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Patient> getPatients() {
        if (patients == null) {
            patients = new FilterDataModel<Patient>(getFilter()) {

                @Override
                protected Object getId(Patient t) {
                    return t.getId();
                }

            };
        }
        return patients;
    }

    /**
     * <p>reset.</p>
     */
    public void reset() {
        firstName = null;
        lastName = null;
        motherMaidenName = null;
        getFilter().clear();
    }

    /**
     * <p>referencePatient.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void referencePatient(Patient patient) {
        String username = null;
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        boolean sendUpdateIfAsked = false;
        if (Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        }
        if (selectedPatient.getPixReference() == null) {
            if (patient.getPixReference() == null) {
                CrossReference reference = new CrossReference(username);
                entityManager.persist(reference);
                reference.setPatients(new ArrayList<Patient>());
                reference.getPatients().add(selectedPatient);
                reference.getPatients().add(patient);
                patient.setPixReference(reference);
                selectedPatient.setPixReference(reference);
                selectedReference = reference.updateReference(entityManager, username);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "A new cross reference has been created");
                sendUpdateIfAsked = true;
            } else {
                sendUpdateIfAsked = updateReference(patient.getPixReference(), selectedPatient, username);
            }
        } else if (patient.getPixReference() == null) {
            updateReference(selectedPatient.getPixReference(), patient, username);
        } else if (selectedPatient.getPixReference().getPatients().contains(patient)) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Those patients are already referenced together");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Those patients are referenced by two different sets of patients, cannot linked them together");
        }
        if (sendUpdateIfAsked && sendNotificationToSut) {
            sendUpdateNotification(selectedPatient);
        }
    }

    /**
     * <p>updateReference.</p>
     *
     * @param reference a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
     * @param patientToAdd a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param username a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean updateReference(CrossReference reference, Patient patientToAdd, String username) {
        reference.getPatients().add(patientToAdd);
        patientToAdd.setPixReference(reference);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        selectedReference = reference.updateReference(entityManager, username);
        FacesMessages.instance().add(StatusMessage.Severity.INFO,"Patients are now referenced together");
        return true;
    }

    /**
     * <p>removePatientFromSet.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void removePatientFromSet(Patient inPatient) {
        String username = null;
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        }
        inPatient.setPixReference(null);
        selectedReference.getPatients().remove(inPatient);
        Patient removedPatient = inPatient.savePatient(entityManager);
        if (sendNotificationToSut) {
            if (selectedTransaction == null) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please first select a transaction");
            } else {
                sendUpdateNotification(removedPatient);
            }
        }
        if (selectedReference != null && selectedReference.getPatients().size() <= 1) {
            Iterator<Patient> iterator = selectedReference.getPatients().iterator();
            while (iterator.hasNext()) {
                Patient patient = iterator.next();
                patient.setPixReference(null);
                patient.savePatient(entityManager);
            }
            CrossReference refToRemove = entityManager.find(CrossReference.class, selectedReference.getId());
            entityManager.remove(refToRemove);
            entityManager.flush();
            selectedReference = null;
            setSelectedPatient(selectedPatient);
        } else if (selectedReference != null) {
            selectedReference = selectedReference.updateReference(entityManager, username);
        }
    }

    /**
     * <p>sendUpdateNotification.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void sendUpdateNotification(Patient inPatient) {
        selectedPatient = EntityManagerService.provideEntityManager().find(Patient.class, inPatient.getId());
        selectedReference = selectedPatient.getPixReference();
        hl7Messages = new ArrayList<TransactionInstance>();
        if (selectedReference == null) { // patient is not reference with other patients
            selectedReference = new CrossReference();
            selectedReference = selectedReference.updateReference(EntityManagerService.provideEntityManager(), null);
            selectedReference.setPatients(new ArrayList<Patient>());
            selectedReference.getPatients().add(selectedPatient);
        }
        if (selectedTransaction == null) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please first choose a transaction");
        } else if (selectedTransaction.getKeyword().equals("ITI-10")) {
            sendHL7v2Notification(null);
        } else {
            sendHL7v3Notification(selectedPatient);
        }
    }

    private void sendHL7v2Notification(Patient inPatient) {
        if (sut != null) {
            UpdateNotificationBuilder builder = new UpdateNotificationBuilder(selectedReference, inPatient);
            List<String> messages = builder.build(sut, APPLICATION, FACILITY, selectedAuthorities);
            if (messages.isEmpty()) {
                FacesMessages
                        .instance()
                        .add(StatusMessage.Severity.WARN,"No message has been generated, check the patients you have selected have identifiers in the domain(s) supported by " +
                                "your system");
            } else {
                for (String messageToSend : messages) {
                    Initiator initiator = new Initiator(sut, FACILITY, APPLICATION,
                            Actor.findActorWithKeyword(PAT_IDENTITY_X_REF_MGR),
                            Transaction.GetTransactionByKeyword("ITI-10"), messageToSend, "ADT^A31^ADT_A05", FACILITY, Actor.findActorWithKeyword
                            (PAT_IDENTITY_CONSUMER));
                    try {
                        hl7Messages.add(initiator.sendMessageAndGetTheHL7Message());
                        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                                "An update notification message has been sent to " + sut.getName()
                                        + ", see details below");
                    } catch (HL7Exception e) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                    }
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,"Please select the System Under Test to use and retry");
        }
    }

    private void sendHL7v3Notification(Patient inPatient) {
        ITI46ManagerLocal manager = (ITI46ManagerLocal) Component.getInstance("iti46Manager");
        HL7v3ResponderSUTConfiguration sut = manager.getSelectedSystem();
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select a system under test and retry");
        } else {

            ITI46MessageBuilder builder = new ITI46MessageBuilder(sut, inPatient);
            PRPAIN201302UV02Type message = builder.buildRevisePatientRecordMessage();
            PIXv3ManagerSender sender = new PIXv3ManagerSender(sut);
            try {
                sender.sendRevisePatientRecord(message);
            } catch (SoapSendException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
                return;
            }
            hl7Messages.add(sender.getTransactionInstance());
            FacesMessages.instance().add(StatusMessage.Severity.INFO,
                    "Update notification messages have been sent to " + sut.getName() + ", see details below");
        }
    }

    private List<HierarchicDesignator> getAvailableHierarchicDesignators() {
        PatientQuery query = new PatientQuery();
        query.simulatedActor().keyword().eq(PAT_IDENTITY_X_REF_MGR);
        return query.patientIdentifiers().domain().getListDistinct();
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
        getFilter().modified();
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
        getFilter().modified();
    }

    /**
     * <p>Getter for the field <code>motherMaidenName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    /**
     * <p>Setter for the field <code>motherMaidenName</code>.</p>
     *
     * @param motherMaidenName a {@link java.lang.String} object.
     */
    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
        getFilter().modified();
    }

    /**
     * <p>Getter for the field <code>selectedReference</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
     */
    public CrossReference getSelectedReference() {
        return selectedReference;
    }

    /**
     * <p>Setter for the field <code>selectedReference</code>.</p>
     *
     * @param selectedReference a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
     */
    public void setSelectedReference(CrossReference selectedReference) {
        this.selectedReference = selectedReference;
    }

    /**
     * <p>Getter for the field <code>sut</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration getSut() {
        return sut;
    }

    /**
     * <p>Setter for the field <code>sut</code>.</p>
     *
     * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public void setSut(HL7V2ResponderSUTConfiguration sut) {
        this.sut = sut;
    }

    /**
     * <p>Getter for the field <code>hl7Messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    /**
     * <p>Getter for the field <code>selectedAuthorities</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getSelectedAuthorities() {
        if (selectedAuthorities == null) {
            selectedAuthorities = new ArrayList<HierarchicDesignator>();
        }
        return selectedAuthorities;
    }

    /**
     * <p>Setter for the field <code>selectedAuthorities</code>.</p>
     *
     * @param selectedAuthorities a {@link java.util.List} object.
     */
    public void setSelectedAuthorities(List<HierarchicDesignator> selectedAuthorities) {
        this.selectedAuthorities = selectedAuthorities;
    }

    /**
     * <p>Getter for the field <code>availableAuhorities</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getAvailableAuhorities() {
        if (this.availableAuhorities == null) {
            this.availableAuhorities = getAvailableHierarchicDesignators();
        }
        return availableAuhorities;
    }

    /**
     * <p>Setter for the field <code>availableAuhorities</code>.</p>
     *
     * @param availableAuhorities a {@link java.util.List} object.
     */
    public void setAvailableAuhorities(List<HierarchicDesignator> availableAuhorities) {
        this.availableAuhorities = availableAuhorities;
    }

    /**
     * <p>isManageIdentities.</p>
     *
     * @return a boolean.
     */
    public boolean isManageIdentities() {
        return manageIdentities;
    }

    /**
     * <p>Setter for the field <code>manageIdentities</code>.</p>
     *
     * @param manageIdentities a boolean.
     */
    public void setManageIdentities(boolean manageIdentities) {
        this.manageIdentities = manageIdentities;
    }

    /**
     * <p>Getter for the field <code>availableHL7v2Systems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderSUTConfiguration> getAvailableHL7v2Systems() {
        return availableHL7v2Systems;
    }
}
