package net.ihe.gazelle.simulator.xcpd.common;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransaction;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionQuery;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * <b>Class Description : </b>DeferredTransactionBrowser<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/01/16
 */
@Name("deferredTransactionBrowser")
@Scope(ScopeType.PAGE)
public class DeferredTransactionBrowser implements Serializable{

    private Filter<DeferredTransaction> filter;
    private Actor simulatedActor;
    private boolean autoRefresh = false;

    /**
     * <p>readSimulatedActorFromUrl.</p>
     */
    @Create
    public void readSimulatedActorFromUrl(){
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("actorKw")){
            simulatedActor = Actor.findActorWithKeyword(params.get("actorKw"));
        } else {
            simulatedActor = null;
        }
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<DeferredTransaction> getFilter() {
        if (filter == null){
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            filter = new Filter<DeferredTransaction>(getHQLCriteriaForFilter(), params);
        }
        return filter;
    }

    private HQLCriterionsForFilter<DeferredTransaction> getHQLCriteriaForFilter() {
        DeferredTransactionQuery query = new DeferredTransactionQuery();
        HQLCriterionsForFilter<DeferredTransaction> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("actor", query.simulatedActor(), simulatedActor);
        criteria.addPath("status", query.status());
        criteria.addPath("queryTS", query.query().timestamp());
        criteria.addPath("responseTS", query.response().timestamp());
        criteria.addPath("init", query.query().request().issuer());
        criteria.addPath("resp", query.query().response().issuer());
        return criteria;
    }

    /**
     * <p>getDeferredTransactionFiltered.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<DeferredTransaction> getDeferredTransactionFiltered(){
        return new FilterDataModel<DeferredTransaction>(getFilter()) {
            @Override
            protected Object getId(DeferredTransaction deferredTransaction) {
                return deferredTransaction.getId();
            }
        };
    }

    /**
     * <p>reset.</p>
     */
    public void reset(){
        getFilter().clear();
    }

    /**
     * <p>isEverythingProcessed.</p>
     */
    public void isEverythingProcessed(){
        DeferredTransactionQuery query = new DeferredTransactionQuery();
        query.simulatedActor().eq(simulatedActor);
        query.status().eq(DeferredTransactionStatus.WAITING);
        if (query.getListNullIfEmpty() == null){
            autoRefresh = false;
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "All messages have been processed");
        }
    }

    /**
     * <p>isAutoRefresh.</p>
     *
     * @return a boolean.
     */
    public boolean isAutoRefresh() {
        return autoRefresh;
    }

    /**
     * <p>Setter for the field <code>autoRefresh</code>.</p>
     *
     * @param autoRefresh a boolean.
     */
    public void setAutoRefresh(boolean autoRefresh) {
        this.autoRefresh = autoRefresh;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>permanentLink.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @return a {@link java.lang.String} object.
     */
    public String permanentLink(TransactionInstance instance) {
        try {
            return ApplicationConfiguration.getValueOfVariable("message_permanent_link").concat(instance.getId().toString());
        } catch (NullPointerException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"message_permanent_link is not defined in app_configuration table of this application !");
            return null;
        }
    }
}
