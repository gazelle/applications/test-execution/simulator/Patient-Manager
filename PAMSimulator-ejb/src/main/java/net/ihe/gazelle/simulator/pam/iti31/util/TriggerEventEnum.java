package net.ihe.gazelle.simulator.pam.iti31.util;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>TriggerEventEnum<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 04/11/15
 */
public enum TriggerEventEnum {

    A01 ("A01", "ADT^A01^ADT_A01", "ADT_A01",true), //0
    A02 ("A02", "ADT^A02^ADT_A02", "ADT_A02",true), //1
    A03 ("A03", "ADT^A03^ADT_A03", "ADT_A03",true), //2
    A04 ("A04", "ADT^A04^ADT_A01", "ADT_A01",true), //3
    A05 ("A05", "ADT^A05^ADT_A05", "ADT_A05",true), //4
    A06 ("A06", "ADT^A06^ADT_A06", "ADT_A06",true), //5
    A07 ("A07", "ADT^A07^ADT_A06", "ADT_A06",true), //6
    A08 ("A08", "ADT^A08^ADT_A01", "ADT_A01",true), //7
    A09 ("A09", "ADT^A09^ADT_A09", "ADT_A09",false), //8
    A10 ("A10", "ADT^A10^ADT_A09", "ADT_A09",false), //9
    A11 ("A11", "ADT^A11^ADT_A09", "ADT_A09",true), //10
    A12 ("A12", "ADT^A12^ADT_A12", "ADT_A12",true), //11
    A13 ("A13", "ADT^A13^ADT_A01", "ADT_A01",true), //12
    A14 ("A14", "ADT^A14^ADT_A05", "ADT_A05",false), //13
    A15 ("A15", "ADT^A15^ADT_A15", "ADT_A15",false), //14
    A16 ("A16", "ADT^A16^ADT_A16", "ADT_A16",false), //15
    A21 ("A21", "ADT^A21^ADT_A21", "ADT_A21",true), //16
    A22 ("A22", "ADT^A22^ADT_A21", "ADT_A21",true), //17
    A25 ("A25", "ADT^A25^ADT_A21", "ADT_A21",false), //18
    A26 ("A26", "ADT^A26^ADT_A21", "ADT_A21",false), //19
    A27 ("A27", "ADT^A27^ADT_A21", "ADT_A21",false), //20
    A32 ("A32", "ADT^A32^ADT_A21", "ADT_A21",false), //21
    A33 ("A33", "ADT^A33^ADT_A21", "ADT_A21",false), //22
    A38 ("A38", "ADT^A38^ADT_A38", "ADT_A38",true), //23
    A40 ("A40", "ADT^A40^ADT_A39", "ADT_A39",true), //24
    A44 ("A44", "ADT^A44^ADT_A43", "ADT_A43",true), //25
    A52 ("A52", "ADT^A52^ADT_A52", "ADT_A52",true), //26
    A53 ("A53", "ADT^A53^ADT_A52", "ADT_A52",true), //27
    A54 ("A54", "ADT^A54^ADT_A54", "ADT_A54",true), //28
    A55 ("A55", "ADT^A55^ADT_A52", "ADT_A52",true), //29
    Z99 ("Z99", "ADT^Z99^ADT_A01", "ADT_A01",true); //30

    String triggerEvent;
    String messageType;
    String messageStructure;
    boolean active;

    /**
     * <p>Getter for the field <code>messageStructure</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageStructure() {
        return messageStructure;
    }

    /**
     * <p>Getter for the field <code>triggerEvent</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTriggerEvent() {
        return triggerEvent;
    }

    /**
     * <p>Getter for the field <code>messageType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>isActive.</p>
     *
     * @return a boolean.
     */
    public boolean isActive() {
        return active;
    }

    TriggerEventEnum(String trigger, String messageType, String structure, boolean active){
        this.triggerEvent = trigger;
        this.messageType = messageType;
        this.messageStructure = structure;
        this.active = active;
    }


    /**
     * <p>getEventForTrigger.</p>
     *
     * @param trigger a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.util.TriggerEventEnum} object.
     */
    public static TriggerEventEnum getEventForTrigger(String trigger){
        for (TriggerEventEnum event: values()){
            if (event.getTriggerEvent().equals(trigger)){
                return event;
            } else {
                continue;
            }
        }
        return null;
    }

    /**
     * <p>getAllEvents.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<String> getAllEvents(){
        List<String> res = new ArrayList<String>();
        for (TriggerEventEnum event: values()){
            res.add(event.getTriggerEvent());
        }
        return res;
    }

    /**
     * <p>getAllActiveEvents.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<String> getAllActiveEvents(){
        List<String> res = new ArrayList<String>();
        for (TriggerEventEnum event: values()){
            if(event.isActive()){
                res.add(event.getTriggerEvent());
            }
        }
        return res;
    }
}
