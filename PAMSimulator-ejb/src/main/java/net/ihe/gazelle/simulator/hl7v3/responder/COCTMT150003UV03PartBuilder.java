package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03ContactParty;
import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.voc.EntityClassOrganization;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.hl7v3.voc.RoleClassContact;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;

/**
 * Created by aberge on 12/03/15.
 */
public class COCTMT150003UV03PartBuilder extends HL7v3MessageBuilder{

	public static COCTMT150003UV03Organization populateProviderOrganization(String telecom, boolean populateId) {
		COCTMT150003UV03Organization organization = new COCTMT150003UV03Organization();
		organization.setClassCode(EntityClassOrganization.ORG);
		organization.setDeterminerCode(EntityDeterminer.INSTANCE);
		if (populateId) {
			organization.addId(new II(PreferenceService.getString("hl7v3_organization_oid"), null));
		}
		organization.addName(populateOn("IHE-Europe")); // FIXME from database ?
		organization.addContactParty(populateContactParty(telecom));
		return organization;
	}

	public static COCTMT150003UV03ContactParty populateContactParty(String contact) {
		COCTMT150003UV03ContactParty contactParty = new COCTMT150003UV03ContactParty();
		contactParty.addId(new II(PreferenceService.getString("application_universal_id"), null));
		contactParty.setClassCode(RoleClassContact.CON);
		contactParty.addTelecom(populateTelecom(contact));
		return contactParty;
	}
}
