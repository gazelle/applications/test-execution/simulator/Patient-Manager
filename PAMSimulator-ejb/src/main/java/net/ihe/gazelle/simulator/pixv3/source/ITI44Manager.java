package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;
import java.util.List;

@Stateful
/**
 * <p>ITI44Manager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("iti44Manager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ITI44ManagerLocal")
public class ITI44Manager implements Serializable, ITI44ManagerLocal {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -6844674348612437538L;
	private ITI44Event selectedEvent = null;
	private HL7v3ResponderSUTConfiguration selectedSystem;
	private List<HL7v3ResponderSUTConfiguration> availableSystems;

	/**
	 * <p>init.</p>
	 */
	@Create
	public void init(){
		refreshListOfSystems();
		selectedEvent = ITI44Event.ADD;
	}

	/**
	 * <p>refreshListOfSystems.</p>
	 */
	public void refreshListOfSystems(){
		HL7v3ResponderSUTConfigurationQuery query = new HL7v3ResponderSUTConfigurationQuery();
		query.listUsages().transaction().keyword().eq("ITI-44");
		query.name().order(true);
		availableSystems = query.getList();
	}


	@Override
	public String getPage(ITI44Event event) {
		selectedEvent = event;
		String url = "/hl7v3/pix/source/";
		switch (event) {
		case ADD:
			url = url + "addPatient";
			break;
		case REVISE:
			url = url + "revisePatient";
			break;
		case MERGE:
			url = url + "mergePatient";
			break;
		default:
			return getPage(ITI44Event.ADD);
		}
		return url + ".seam";
	}


	@Override
	public String getStyleClass(ITI44Event event) {
		if (event.equals(selectedEvent)) {
			return "gzl-btn-blue";
		} else {
			return "gzl-btn";
		}
	}

	public enum ITI44Event {
		ADD,
		REVISE,
		MERGE;
	}

	/**
	 * <p>getCurrentPage.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCurrentPage(){
		return getPage(selectedEvent);
	}


	@Override
	public ITI44Event getSelectedEvent() {
		return selectedEvent;
	}


	@Override
	@Remove
	public void destroy() {

	}

	/**
	 * <p>Getter for the field <code>availableSystems</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<HL7v3ResponderSUTConfiguration> getAvailableSystems() {
		return availableSystems;
	}


	public void setAvailableSystems(List<HL7v3ResponderSUTConfiguration> availableSystems) {
		this.availableSystems = availableSystems;
	}

	/**
	 * <p>Getter for the field <code>selectedSystem</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public HL7v3ResponderSUTConfiguration getSelectedSystem() {
		return selectedSystem;
	}


	public void setSelectedSystem(HL7v3ResponderSUTConfiguration selectedSystem) {
		this.selectedSystem = selectedSystem;
	}
}
