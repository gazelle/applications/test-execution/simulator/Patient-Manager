package net.ihe.gazelle.simulator.pixm.manager;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.responder.IHEQueryParserCode;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.CrossReferenceDAO;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Reference;
import org.jboss.seam.annotations.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>PIXmQueryHandler class.</p>
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */

public class PIXmQueryHandler {

    private Patient foundPatient;
    private String sourceIdentifierAA;
    private String sourceIdentifierId;
    private String targetSystem;
    private String errorMessage;

    public PIXmQueryHandler(String sourceIdentifierId, String sourceIdentifierAA, String targetSystem) {
        this.sourceIdentifierAA = sourceIdentifierAA;
        this.sourceIdentifierId = sourceIdentifierId;
        this.targetSystem = targetSystem;
    }

    /**
     * PAM-510
     * @return
     */
    private List<String> getActorKeywords() {
        return Arrays.asList(PatientManagerConstants.PAT_IDENTITY_X_REF_MGR, PatientManagerConstants.CONNECTATHON);
    }

    /**
     * <p>findPatientsByIdentifiers.</p>
     *
     * @return the status of the query
     */
    @Transactional
    public IHEQueryParserCode findPatientsByIdentifiers() {


        // case 4: sourceIdentifier assigning authority not found
        if (sourceIdentifierAA != null) {
            if (!HierarchicDesignatorDAO.domainExists(sourceIdentifierAA, DesignatorType.PATIENT_ID)) {
                return IHEQueryParserCode.UNKNOWN_SOURCE_ASSIGNING_AUTHORITY;
            }
            // else continue processing the query
        } else {
            // requirements on query parameter: both assigning autority and identifier are expected
            return IHEQueryParserCode.NO_QUERY_PARAMETER;
        }
        if (sourceIdentifierId != null && !sourceIdentifierId.isEmpty()) {
            PatientQuery query = new PatientQuery();
            query.simulatedActor().keyword().in(getActorKeywords());
            query.stillActive().eq(true);
            query.patientIdentifiers().domain().isNotNull();
            PatientQuery intermediateQuery = new PatientQuery();
            query.addRestriction(HQLRestrictions.and(
                    intermediateQuery.patientIdentifiers().idNumber().eqRestriction(sourceIdentifierId),
                    intermediateQuery.patientIdentifiers().domain().universalID().eqRestriction(sourceIdentifierAA)
            ));
            foundPatient = query.getUniqueResult();
            // case 3: sourceIdentifier assigning authority known but no patient with sourceIdentifierId
            if (foundPatient == null) {
                return IHEQueryParserCode.UNKNOWN_SOURCE_PATIENT_ID;
            }
        } else {
            // requirements on query parameter: both assigning authority and identifier are expected
            return IHEQueryParserCode.NO_QUERY_PARAMETER;
        }
        if (targetSystem != null) {
            if (!HierarchicDesignatorDAO.domainExists(targetSystem, DesignatorType.PATIENT_ID)) {
                return IHEQueryParserCode.UNKNOWN_DOMAINS;
            }
        }
        return IHEQueryParserCode.RETURN_ALL_PATIENTS;
    }

    public OperationOutcome buildOperationOutcome(IHEQueryParserCode parserCode) {
        OperationOutcome outcome = new OperationOutcome();
        OperationOutcome.OperationOutcomeIssueComponent issueComponent = new OperationOutcome.OperationOutcomeIssueComponent();
        issueComponent.setSeverity(OperationOutcome.IssueSeverity.ERROR);
        if (IHEQueryParserCode.UNKNOWN_DOMAINS.equals(parserCode)) {
            issueComponent.setCode(OperationOutcome.IssueType.CODEINVALID);
            errorMessage = "targetSystem not found";
        } else if (IHEQueryParserCode.UNKNOWN_SOURCE_ASSIGNING_AUTHORITY.equals(parserCode)) {
            issueComponent.setCode(OperationOutcome.IssueType.CODEINVALID);
            errorMessage = "sourceIdentifier Assigning Authority not found";
        } else if (IHEQueryParserCode.UNKNOWN_SOURCE_PATIENT_ID.equals(parserCode)) {
            issueComponent.setCode(OperationOutcome.IssueType.NOTFOUND);
            errorMessage = "sourceIdentifier Patient Identifier not found";
        } else if (IHEQueryParserCode.NO_QUERY_PARAMETER.equals(parserCode)) {
            issueComponent.setCode(OperationOutcome.IssueType.INCOMPLETE);
            errorMessage = "sourceIdentifier shall include both the Assigning Authority and the identifier value";
        }
        issueComponent.setDiagnostics(errorMessage);
        outcome.addIssue(issueComponent);
        return outcome;
    }

    public static TransactionInstance populatePIXmTransaction(EncodingEnum format, String requestContent, String responseContent, String callerIP) {

        TransactionInstance message = new TransactionInstance();
        message.setDomain(Domain.getDomainByKeyword(PatientManagerConstants.ITI));
        message.setSimulatedActor(Actor.findActorWithKeyword(PatientManagerConstants.PAT_IDENTITY_CONSUMER));
        MessageInstance requestMessageInstance = new MessageInstance();

        requestMessageInstance.setContent(requestContent);
        requestMessageInstance.setIssuingActor(Actor.findActorWithKeyword(PatientManagerConstants.PAT_IDENTITY_CONSUMER));
        requestMessageInstance.setIssuer(callerIP);
        requestMessageInstance.setIssuerIpAddress(callerIP);
        message.setRequest(requestMessageInstance);
        MessageInstance responseMessageInstance = new MessageInstance();
        responseMessageInstance.setIssuingActor(Actor.findActorWithKeyword(PatientManagerConstants.PAT_IDENTITY_X_REF_MGR));
        responseMessageInstance.setIssuer("PatientManager");
        requestMessageInstance.setType(FhirConstants.PIXM_REQUEST_TYPE);
        responseMessageInstance.setType(FhirConstants.PIXM_RESPONSE);
        if (EncodingEnum.JSON.equals(format)) {
            message.setStandard(EStandard.FHIR_JSON);
        } else {
            message.setStandard(EStandard.FHIR_XML);
        }
        responseMessageInstance.setContent(responseContent);
        message.setResponse(responseMessageInstance);
        message.setTimestamp(new Date());
        message.setTransaction(Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_83));
        message.setVisible(true);
        return message;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }


    public Parameters getResultList() {
        Parameters parameters = new Parameters();
        if (foundPatient != null) {
            List<CrossReference> crossReferences = CrossReferenceDAO.getReferencesForPatient(foundPatient);
            setPatientIdentifiersInResponse(foundPatient, parameters);
            if (crossReferences != null && !crossReferences.isEmpty()) {
                for (CrossReference crossReference : crossReferences) {
                    List<Patient> patients = crossReference.getPatients();
                    for (Patient patient : patients) {
                        // we already looked through the retrieved patient's identifiers
                        if (!patient.getId().equals(foundPatient.getId())) {
                            setPatientIdentifiersInResponse(patient, parameters);
                        }
                    }
                }
            }
        }
        return parameters;
    }

    private void setPatientIdentifiersInResponse(Patient patient, Parameters parameters) {
        List<PatientIdentifier> identifierList = getPatientsIdentifiers(patient);
        if (!identifierList.isEmpty()) {
            PatientIdentifierDAO.toParametersList(parameters, identifierList);
        }
        parameters.addParameter().setName(FhirConstants.PARAM_TARGET_ID).setValue(getPatientReference(patient));
    }

    private List<PatientIdentifier> getPatientsIdentifiers(Patient patient) {
        List<PatientIdentifier> returnedIdentifiers = new ArrayList<PatientIdentifier>();
        for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
            if (pid.getDomain() != null) {
                if (!(this.sourceIdentifierId.equals(pid.getIdNumber()) &&
                        this.sourceIdentifierAA.equals(pid.getDomain().getUniversalID()))) {
                    if (this.targetSystem == null || targetSystem.equals(pid.getDomain().getUniversalID())) {
                        returnedIdentifiers.add(pid);
                    }
                }
            }
        }
        return returnedIdentifiers;
    }

    private Reference getPatientReference(Patient patient) {
        Reference reference = new Reference();
        String applicationUrl = PreferenceService.getString(PatientManagerConstants.APPLICATION_URL_PREFERENCE);
        StringBuilder resourceUrl = new StringBuilder(applicationUrl);
        if (!applicationUrl.endsWith("/")) {
            resourceUrl.append("/");
        }
        resourceUrl.append("fhir/Patient/");
        resourceUrl.append(patient.getId());
        reference.setReference(resourceUrl.toString());
        /*reference.setReference(foundPatient.getId().toString());*/
        return reference;
    }
}
