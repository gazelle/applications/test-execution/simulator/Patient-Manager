package net.ihe.gazelle.simulator.pam.automaton.report;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.automaton.Manager.GraphVertices;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by xfs on 26/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("testStep")
@Table(name = "pam_test_step", schema = "public")
@SequenceGenerator(name = "pam_test_step_sequence", sequenceName = "pam_test_step_id_seq", allocationSize = 1)
public class TestStep implements Serializable {

    private static Logger log = LoggerFactory.getLogger(TestStep.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_test_step_sequence")
    @Column(name = "id")
    @NotNull
    private Integer id;

    @ManyToOne
    @Cascade(CascadeType.MERGE)
    @JoinColumn(name = "requestandresponse_id")
    private TransactionInstance requestAndResponse;

    @Column(name = "initialstatus")
    private GraphVertices initialStatus;

    @Column(name = "finalstatus")
    private GraphVertices finalStatus;

    @Column(name = "event")
    private String event;

    @Column(name = "ackcode")
    private String ackCode;

    @Column(name = "errormessage")
    private String errorMessage;

    @ManyToOne
    @JoinColumn(name = "execution_result_id")
    private GraphExecutionResults executionResult;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>requestAndResponse</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getRequestAndResponse() {
        return requestAndResponse;
    }

    /**
     * <p>Setter for the field <code>requestAndResponse</code>.</p>
     *
     * @param requestAndResponse a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void setRequestAndResponse(TransactionInstance requestAndResponse) {
        this.requestAndResponse = requestAndResponse;
    }

    /**
     * <p>Getter for the field <code>initialStatus</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphVertices} object.
     */
    public GraphVertices getInitialStatus() {
        return initialStatus;
    }

    /**
     * <p>Setter for the field <code>initialStatus</code>.</p>
     *
     * @param initialStatus a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphVertices} object.
     */
    public void setInitialStatus(GraphVertices initialStatus) {
        this.initialStatus = initialStatus;
    }

    /**
     * <p>Getter for the field <code>finalStatus</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphVertices} object.
     */
    public GraphVertices getFinalStatus() {
        return finalStatus;
    }

    /**
     * <p>Setter for the field <code>finalStatus</code>.</p>
     *
     * @param finalStatus a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphVertices} object.
     */
    public void setFinalStatus(GraphVertices finalStatus) {
        this.finalStatus = finalStatus;
    }

    /**
     * <p>Getter for the field <code>event</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEvent() {
        return event;
    }

    /**
     * <p>Setter for the field <code>event</code>.</p>
     *
     * @param event a {@link java.lang.String} object.
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * <p>Getter for the field <code>ackCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAckCode() {
        return ackCode;
    }

    /**
     * <p>Setter for the field <code>ackCode</code>.</p>
     *
     * @param ackCode a {@link java.lang.String} object.
     */
    public void setAckCode(String ackCode) {
        this.ackCode = ackCode;
    }

    /**
     * <p>Getter for the field <code>errorMessage</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * <p>Setter for the field <code>errorMessage</code>.</p>
     *
     * @param errorMessage a {@link java.lang.String} object.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * <p>Getter for the field <code>executionResult</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults} object.
     */
    public GraphExecutionResults getExecutionResult() {
        return executionResult;
    }

    /**
     * <p>Setter for the field <code>executionResult</code>.</p>
     *
     * @param executionResult a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults} object.
     */
    public void setExecutionResult(GraphExecutionResults executionResult) {
        this.executionResult = executionResult;
    }

    /**
     * <p>setAckAndErrorMessage.</p>
     */
    public void setAckAndErrorMessage() {

        String response = this.getRequestAndResponse().getResponse().getContentAsString();
        PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
        Message ackMessage;
        try {
            ackMessage = pipeParser.parse(response);
            Terser terser = new Terser(ackMessage);
            setAckCode(terser.get("/.MSA-1"));
            setErrorMessage(terser.get("/.MSA-8"));
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
