package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import net.ihe.gazelle.simulator.pix.model.CrossReferenceQuery;

import java.util.List;

/**
 * <p>CrossReferenceDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 14/11/17
 */

public class CrossReferenceDAO {
    public static List<CrossReference> getReferencesForPatient(Patient foundPatient) {
        CrossReferenceQuery query = new CrossReferenceQuery();
        query.patients().id().eq(foundPatient.getId());
        return query.getList();
    }
}
