package net.ihe.gazelle.simulator.pam.pdc;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ACK;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.utils.ADTDecoder;
import net.ihe.gazelle.simulator.pam.utils.Consumer;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.annotations.Transactional;

import java.util.Arrays;

/**
 * Class description PDC This class is the implementation of the PDC part of the simulator. This class is called when messages are received on PDC
 * port in order to build and send the response
 *
 * @author aberge > anne-gaelle.berge@inria.fr
 * @version $Id: $Id
 */
public class PDC extends Consumer {

    private static final String[] ACCEPTED_EVENTS = {"A28", "A31", "A47", "A40", "A24", "A37"};
    private static final String[] ACCEPTED_MESSAGE_TYPES = {"ADT"};

    /**
     * {@inheritDoc}
     */
    @Override
    public IHEDefaultHandler newInstance() {
        return new PDC();
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public Message processMessage(Message message) throws HL7Exception {
        if (message == null) {
            return null;
        }
        Message response = null;
        ACKBuilder ackBuilder = new ACKBuilder(serverApplication, serverFacility, Arrays.asList(ACCEPTED_MESSAGE_TYPES), Arrays.asList
                (ACCEPTED_EVENTS));
        try {
            // check if we must accept the message
            ackBuilder.isMessageAccepted(message);
        } catch (HL7v2ParsingException e) {
            response = ackBuilder.buildNack(message, e);
        }
        // if no NACK is generated, process the message
        if (response == null) {
            String triggerEvent = terser.get("/.MSH-9-2");
            messageControlId = terser.get("/.MSH-10");
            ADTDecoder decoder = new ADTDecoder(domain, simulatedTransaction, simulatedActor, ackBuilder,
                    entityManager, this);
            if (triggerEvent != null) {
                response = decoder.decode(message, triggerEvent);
            }
        }
        if (response != null) {
            if (domain != null && domain.getKeyword().equals(PatientManagerConstants.ITI_FR)) {
                ((ACK) response).getMSH().getVersionID().getVid2_InternationalizationCode().getCe1_Identifier().setValue(PatientManagerConstants.FRA);
                ((ACK) response).getMSH().getVersionID().getVid3_InternationalVersionID().getCe1_Identifier().setValue(PatientManagerConstants
                        .PAM_FR_VERSION);
            }
            logIfError(response, entityManager);
        }
        setSutActor(Actor.findActorWithKeyword(getSutActorKeyword(), entityManager));
        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        return response;
    }

    @Override
    public String getSutActorKeyword() {
        return PatientManagerConstants.PDS;
    }
}
