package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.security.Identity;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum PatientManagerAuthorizations implements Authorization {

    ALL,
    LOGGED,
    ADMIN,
    EPD,
    ADT,
    PAM,
    PAM_AUTOMATON,
    PIX,
    PIXV3,
    PIXM,
    PDQ,
    PDQV3,
    PDQM,
    XCPD,
    XUA,
    IHE,
    BP6,
    SEQUOIA,
    MOD_WORKLIST,
    SEHE,
    CONNECTATHON;

    /** {@inheritDoc} */
    @Override
    public boolean isGranted(Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
            default:
                return SimulatorFeatureDAO.isFeatureEnabled(this);
        }
    }

}
