package net.ihe.gazelle.simulator.pam.automaton.model.graph;



import net.ihe.gazelle.simulator.pam.automaton.Manager.GraphExecutionStatus;
import net.ihe.gazelle.simulator.pam.automaton.report.TestStep;
import org.graphwalker.core.machine.ExecutionStatus;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by xfs on 26/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("graphExecutionResults")
@Table(name = "pam_graph_execution_results", schema = "public")
@SequenceGenerator(name = "pam_graph_execution_results_sequence", sequenceName = "pam_graph_execution_results_id_seq", allocationSize = 1)
public class GraphExecutionResults implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_graph_execution_results_sequence")
    @NotNull
    @Column(name = "id", nullable = true, unique = false)
    private Integer id;

    @ManyToOne
    @Cascade(CascadeType.MERGE)
    @JoinColumn(name = "graphexecution_id")
    private GraphExecution graphExecution;

    @OneToMany(targetEntity = TestStep.class, mappedBy = "executionResult")
    @Cascade(CascadeType.MERGE)
    private List<TestStep> messagesList;

    @Column(name = "executionstatus")
    private GraphExecutionStatus executionStatus;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>graphExecution</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecution} object.
     */
    public GraphExecution getGraphExecution() {
        return graphExecution;
    }

    /**
     * <p>Setter for the field <code>graphExecution</code>.</p>
     *
     * @param graphExecution a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecution} object.
     */
    public void setGraphExecution(GraphExecution graphExecution) {
        this.graphExecution = graphExecution;
    }

    /**
     * <p>Getter for the field <code>messagesList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestStep> getMessagesList() {
        return messagesList;
    }

    /**
     * <p>Setter for the field <code>messagesList</code>.</p>
     *
     * @param messagesList a {@link java.util.List} object.
     */
    public void setMessagesList(List<TestStep> messagesList) {
        this.messagesList = messagesList;
    }

    /**
     * <p>Getter for the field <code>executionStatus</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphExecutionStatus} object.
     */
    public GraphExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    /**
     * <p>Setter for the field <code>executionStatus</code>.</p>
     *
     * @param executionStatus a {@link net.ihe.gazelle.simulator.pam.automaton.Manager.GraphExecutionStatus} object.
     */
    public void setExecutionStatus(GraphExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    /**
     * <p>Getter for the field <code>timestamp</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getTimestamp() {
        if (timestamp != null){
            return (Date) timestamp.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>timestamp</code>.</p>
     *
     * @param timestamp a {@link java.util.Date} object.
     */
    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = (Date) timestamp.clone();
        } else {
            this.timestamp = null;
        }
    }
}
