package net.ihe.gazelle.simulator.pixm.consumer;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.simulator.fhir.util.FhirReturnType;
import net.ihe.gazelle.simulator.fhir.util.GazelleFhirClient;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.pix.consumer.AbstractPIXQueryManager;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import org.hl7.fhir.dstu3.model.Parameters;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xfs on 25/04/16.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Name("pixmConsumer")
@Scope(ScopeType.PAGE)
public class PIXmConsumer extends AbstractPIXQueryManager {

    private static final Logger LOG = LoggerFactory.getLogger(PIXmConsumer.class);

    private static final String ITI_83 = "ITI-83";

    private List<FHIRResponderSUTConfiguration> availableSystems;
    private List<String> referencedPatients;
    private FhirReturnType responseType;
    private PatientFhirIHE currentPatient;
    private GazelleFhirClient fhirClient;

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<FHIRResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>initialize.</p>
     */
    @Create
    public void initialize() {
        super.initialize();
        availableSystems = FHIRResponderSUTConfiguration.getAllConfigurationsForSelection(ITI_83);
        responseType = FhirReturnType.XML;
        setSelectedTestData(null);
    }

    @Override
    protected void initializePageUsingTestData(TestData selectedTestData){
        super.initializePageUsingTestData(selectedTestData);
        if (selectedTestData != null)
        {
            responseType = selectedTestData.getFhirReturnType();
            List<HierarchicDesignator> restrictedDomain = selectedTestData.getRestrictedDomain();
            if (restrictedDomain != null && !restrictedDomain.isEmpty()){
                // only one restricted domain in PIXm, take the first entry
                currentDomainReturned = new AssigningAuthority();
                currentDomainReturned.setUniversalId(restrictedDomain.get(0).getUniversalIDAsUrn());
            }
        }
    }
    /**
     * <p>Getter for the field <code>referencedPatients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getReferencedPatients() {
        return referencedPatients;
    }

    /**
     * <p>Setter for the field <code>referencedPatients</code>.</p>
     *
     * @param referencedPatients a {@link java.util.List} object.
     */
    public void setReferencedPatients(List<String> referencedPatients) {
        this.referencedPatients = referencedPatients;
    }

    /**
     * <p>Getter for the field <code>responseType</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public FhirReturnType getResponseType() {
        return responseType;
    }

    /**
     * <p>Setter for the field <code>responseType</code>.</p>
     *
     * @param responseType a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public void setResponseType(FhirReturnType responseType) {
        this.responseType = responseType;
    }

    /**
     * <p>Getter for the field <code>currentPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.fhir.resources.PatientFhirIHE} object.
     */
    public PatientFhirIHE getCurrentPatient() {
        return currentPatient;
    }

    /**
     * <p>Setter for the field <code>currentPatient</code>.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.fhir.resources.PatientFhirIHE} object.
     */
    public void setCurrentPatient(PatientFhirIHE currentPatient) {
        this.currentPatient = currentPatient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage() {
        if (selectedSUT != null) {


            FHIRResponderSUTConfiguration fhirSut = (FHIRResponderSUTConfiguration) selectedSUT;

            PIXmQueryBuilder builder = new PIXmQueryBuilder();
            Parameters parameters = builder.buildQuery(searchedPid, currentDomainReturned);

            fhirClient = new GazelleFhirClient(builder, fhirSut, responseType, PAT_IDENTITY_CONSUMER, ITI_83, PAT_IDENTITY_X_REF_MGR);

            Parameters response = fhirClient.executeOperation(parameters, FhirConstants.PIXM_OPERATION, FacesMessages.instance());
            hl7Messages = fhirClient.getMessages();

            if (response != null) {
                receivedIdentifiers = PIXmResponseParser.getReceivedIdentifiers(response);
                referencedPatients = PIXmResponseParser.getReferencedPatients(response);
            }

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please select a system under test and retry");
        }
    }

    public String getPatientUrl(String urlFromResponse) {
        if (urlFromResponse != null) {
            if (urlFromResponse.startsWith("http")) {
                return urlFromResponse;
            } else {
                return selectedSUT.getEndpoint().concat("/").concat(urlFromResponse);
            }
        }
        return null;
    }


    /**
     * <p>getAvailableResponseTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getAvailableResponseTypes() {
        return FhirReturnType.getFhirReturnTypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void resetPage() {
        performAnotherTest();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction getTestedTransaction() {
        return Transaction.GetTransactionByKeyword(ITI_83);
    }

    @Override
    protected void setFhirResponseFormatForTestData(TestData testData) {
        testData.setFhirReturnType(responseType);
    }

    /** {@inheritDoc} */
    @Override
    public void createNewTestData() {
        if (currentDomainReturned != null && !currentDomainReturned.isEmpty()){
            domainsReturned = new ArrayList<AssigningAuthority>();
            domainsReturned.add(currentDomainReturned);
        }
        super.createNewTestData();
    }
}
