package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.Person;
import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 16/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("revisePatientRecordBean")
@Scope(ScopeType.PAGE)
public class RevisePatientRecord extends PatientIdentitySource {

	private static Logger log = Logger.getLogger(RevisePatientRecord.class);

	/** {@inheritDoc} */
	@Override
	public void sendMessage() {
		ITI44ManagerLocal manager = (ITI44ManagerLocal) Component.getInstance("iti44Manager");
		HL7v3ResponderSUTConfiguration sut = manager.getSelectedSystem();
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"Please, first select a system under test and retry");
			return;
		} else {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			// save patient's identifiers
			if (secondaryPatient.getPatientIdentifiers() != null) {
				List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(secondaryPatient
						.getPatientIdentifiers());
				secondaryPatient.setPatientIdentifiers(pidList);
			}
			secondaryPatient = secondaryPatient.savePatient(entityManager);

			// populate patients' histories
			if (secondaryPatient != null) {
				PatientHistory history = new PatientHistory(secondaryPatient, null,
						PatientHistory.ActionPerformed.CREATED, null, secondaryPatient.getCreator(), null, null);
				history.save(entityManager);
				history = new PatientHistory(secondaryPatient, null, PatientHistory.ActionPerformed.SENT,
						"PRPA_IN201302UV02", secondaryPatient.getCreator(), sut.getOrganizationOID(),
						sut.getDeviceOID());
				history.save(entityManager);
			}
			if (selectedPatient != null) {
				PatientHistory history = new PatientHistory(selectedPatient, secondaryPatient,
						PatientHistory.ActionPerformed.UPDATE, null, secondaryPatient.getCreator(), null, null);
				history.save(entityManager);
				// deactivate old patient
				selectedPatient.setStillActive(false);
				selectedPatient = selectedPatient.savePatient(entityManager);
			}

			ITI44MessageBuilder builder = new ITI44MessageBuilder(sut, secondaryPatient, null);
			PRPAIN201302UV02Type message = builder.buildRevisePatientRecordMessage();
			PatientIdentitySourceSender sender = new PatientIdentitySourceSender(sut);
			try {
				sender.sendRevisePatientRecord(message);
			} catch (SoapSendException e){
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
				return;
			}
			messages = new ArrayList<TransactionInstance>();
			messages.add(sender.getTransactionInstance());
			secondaryPatient = null;
			selectedPatient = null;
		}
	}

	/**
	 * <p>generateAPatient.</p>
	 */
	public void generateAPatient() {
		selectedPatient = null;
		secondaryPatient = super.generateNewPatient(sendingActor);
		if (secondaryPatient != null) {
			displayDDSPanel = false;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void actionOnSelectedPatient(Patient inPatient) {
		log.info("patient selected");
		if (inPatient != null) {
			selectedPatient = inPatient;
			secondaryPatient = new Patient(selectedPatient, sendingActor);
			if (Identity.instance().getCredentials() != null) {
				secondaryPatient.setCreator(Identity.instance().getCredentials().getUsername());
			} else {
				secondaryPatient.setCreator(null);
			}
			displayPatientsList = false;
			displayDDSPanel = false;
		} else {
			selectedPatient = null;
			secondaryPatient = null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void createNewRelationship() {
		setCurrentRelationship(new Person(secondaryPatient));
	}

	/** {@inheritDoc} */
	@Override
	public void saveRelationship() {
		secondaryPatient.addPersonalRelationship(getCurrentRelationship());
		setCurrentRelationship(null);
	}
}
