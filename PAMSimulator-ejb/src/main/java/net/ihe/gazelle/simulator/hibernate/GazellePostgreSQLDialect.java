package net.ihe.gazelle.simulator.hibernate;

import org.hibernate.dialect.PostgreSQL82Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

/**
 * Defines a new postgreSQL dialect in order to use the levenshtein and dmetaphone functions available in the fuzzystrmatch module of postgreSQL
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazellePostgreSQLDialect extends PostgreSQL82Dialect {

	/**
	 * <p>Constructor for GazellePostgreSQLDialect.</p>
	 */
	public GazellePostgreSQLDialect() {
		super();
		registerFunction("levenshtein", new StandardSQLFunction("levenshtein", StandardBasicTypes.INTEGER));
		registerFunction("dmetaphone", new StandardSQLFunction("dmetaphone"));
	}
}
