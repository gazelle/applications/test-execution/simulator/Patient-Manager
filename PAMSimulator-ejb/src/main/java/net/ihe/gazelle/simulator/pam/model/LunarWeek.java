package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.translate.TranslateService;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>LunarWeek<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */
public enum LunarWeek {

    FIRST_WEEK(31, "1.week"),
    SECOND_WEEK(32, "2.week"),
    THIRD_WEEK(33, "3.week"),
    FOURTH_WEEK(34, "4.week"),
    FIFTH_WEEK(35, "5.week");

    private Integer day;
    private String label;

    LunarWeek(Integer day, String label){
        this.day = day;
        this.label = label;
    }

    /**
     * <p>Getter for the field <code>day</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getDay() {
        return day;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return TranslateService.getTranslation(label);
    }

    /**
     * <p>getWeekBasedOnDay.</p>
     *
     * @param day a {@link java.lang.Integer} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.LunarWeek} object.
     */
    public static LunarWeek getWeekBasedOnDay(Integer day){
        for (LunarWeek week: values()){
            if (week.getDay().equals(day)){
                return week;
            }
            else {
                continue;
            }
        }
        return null;
    }

    /**
     * <p>getLunarWeekValues.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<SelectItem> getLunarWeekValues(){
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem(null, TranslateService.getTranslation("net.ihe.gazelle.pam.PleaseSelect")));
        for (LunarWeek week: values()){
            items.add(new SelectItem(week, week.getLabel()));
        }
        return items;
    }
}
