package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson;
import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.hl7v3.responder.PIXV3QueryHandler;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.Person;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class PRPAIN201301UV02Parser extends ITI44MessageParser {

    public void extractPatientFromAddRecord(PRPAIN201301UV02Type request) throws HL7V3ParserException {
        // first parse the control act process
        List<Patient> patients = parseControlActProcess(request.getControlActProcess());
        // store patient and its identifiers
        Actor pixManager = Actor.findActorWithKeyword(PIX_MANAGER_ACTOR_KEYWORD);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        for (Patient patient : patients) {
            // check identifiers are not already used in the tool for this actor
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                pid.setFullPatientIdentifierIfEmpty();
                Patient dbPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, pixManager);
                if (dbPatient != null) {
                    throw new HL7V3ParserException(null, pid.getFullPatientId() + " is already used");
                }
            }
            // if we reach this point, the identifiers are new, save them and assign them to the patient
            List<PatientIdentifier> savedIdentifiers = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                PatientIdentifier savedPID = pid.save();
                savedIdentifiers.add(savedPID);
            }
            patient.setPatientIdentifiers(savedIdentifiers);
            patient.savePatient(entityManager);
        }
    }

    private List<Patient> parseControlActProcess(PRPAIN201301UV02MFMIMT700701UV01ControlActProcess controlActProcess) throws
            HL7V3ParserException {
        if (controlActProcess == null) {
            throw new HL7V3ParserException("/PRPA_IN201301UV02", "Received message does not contain ControlActProcess element");
        } else if (controlActProcess.getSubject().isEmpty()) {
            throw new HL7V3ParserException("/PRPA_IN201301UV02/controlActProcess", "The ControlActProcess does not contain Subject element");
        } else {
            List<Patient> patients = new ArrayList<Patient>();
            for (PRPAIN201301UV02MFMIMT700701UV01Subject1 subject1 : controlActProcess.getSubject()) {
                patients.add(parseSubject(subject1));
            }
            return patients;
        }
    }

    private Patient parseSubject(PRPAIN201301UV02MFMIMT700701UV01Subject1 subject) throws HL7V3ParserException {
        if (subject.getRegistrationEvent() == null) {
            throw new HL7V3ParserException("/PRPA_IN201301UV02/controlActProcess/subject", "Subject does not contain any RegistrationEvent element");
        }
        return parseRegistrationEvent(subject.getRegistrationEvent());
    }

    private Patient parseRegistrationEvent(PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent registrationEvent) throws HL7V3ParserException {
        if (registrationEvent.getSubject1() == null) {
            throw new HL7V3ParserException("/PRPA_IN201301UV02/controlActProcess/subject/registrationEvent", "RegistrationEvent does not contain " +
                    "any subject1 element");
        }
        return parseSubject1(registrationEvent.getSubject1());
    }

    private Patient parseSubject1(PRPAIN201301UV02MFMIMT700701UV01Subject2 subject1) throws HL7V3ParserException {
        if (subject1.getPatient() == null) {
            throw new HL7V3ParserException("/PRPA_IN201301UV02/controlActProcess/subject/registrationEvent/subject1", "Subject1 does not contain " +
                    "any Patient element");
        } else {
            return parsePatient(subject1.getPatient());
        }
    }

    private void checkEprSpidForPatient(Patient patient) throws HL7V3ParserException {
        boolean epr = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD);
        if (epr) {
            PatientIdentifier eprSpid = null;
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                if (pid.getDomain() != null && PIXV3QueryHandler.SPID_ROOT.equals(pid.getDomain().getUniversalID())) {
                    eprSpid = pid;
                }
            }
            if (eprSpid == null) {
                throw new HL7V3ParserException(PATIENT_XPATH, "Patient is expected to contain an EPR-SPID");
            }
        }
    }

    private Patient parsePatient(PRPAMT201301UV02Patient subject1Patient) throws HL7V3ParserException {
        Patient patient = new Patient();
        List<PatientIdentifier> identifierList = parsePatientIdentifier(subject1Patient.getId());
        if (identifierList.size() != 2) {
            throw new HL7V3ParserException(PATIENT_XPATH, "A patient must contain exactly 2 patient identifier (" + identifierList.size() + " provided)");

        }

        patient.setSimulatedActor(Actor.findActorWithKeyword(PIX_MANAGER_ACTOR_KEYWORD));
        if (subject1Patient.getPatientNonPersonLivingSubject() != null) {
            throw new HL7V3ParserException(PATIENT_XPATH, "Patient is expected to be a patientPerson");
        } else if (subject1Patient.getPatientPerson() == null) {
            throw new HL7V3ParserException(PATIENT_XPATH, "Subject1 does not contain a patientPerson element");
        } else {
            parsePatientPerson(subject1Patient.getPatientPerson(), patient);
            List<PatientIdentifier> othersIds = parseOtherIds(subject1Patient.getPatientPerson().getAsOtherIDs());
            if (!othersIds.isEmpty()) {
                identifierList.addAll(othersIds);
            }
            patient.setPatientIdentifiers(identifierList);
            COCTMT150003UV03Organization providerOrganization = subject1Patient.getProviderOrganization();
            setPatientsCreator(providerOrganization, patient);
        }
        checkEprSpidForPatient(patient);
        return patient;
    }

    private List<PatientIdentifier> parseOtherIds(List<PRPAMT201301UV02OtherIDs> asOtherIDs) {
        if (asOtherIDs.isEmpty()) {
            return new ArrayList<PatientIdentifier>();
        } else {
            List<PatientIdentifier> identifierList = new ArrayList<PatientIdentifier>();
            for (PRPAMT201301UV02OtherIDs otherId : asOtherIDs) {
                List<PatientIdentifier> localIdentifiers = parsePatientIdentifier(otherId.getId());
                if (!localIdentifiers.isEmpty()) {
                    identifierList.addAll(localIdentifiers);
                }
            }
            return identifierList;
        }
    }

    private void parsePatientPerson(PRPAMT201301UV02Person patientPerson, Patient patient) throws HL7V3ParserException {
        setPatientNames(patientPerson.getName(), patient);
        setGenderCode(patientPerson.getAdministrativeGenderCode(), patient);
        setBirthTime(patientPerson.getBirthTime(), patient);
        setPatientAddresses(patientPerson.getAddr(), patient);
        setPatientPhoneNumbers(patientPerson.getTelecom(), patient);
        setReligionCode(patientPerson.getReligiousAffiliationCode(), patient);
        setRaceCode(patientPerson.getRaceCode(), patient);
        setMultipleBirthInd(patientPerson.getMultipleBirthInd(), patient);
        setMultipleBirthOrder(patientPerson.getMultipleBirthOrderNumber(), patient);
        setIsPatientDead(patientPerson.getDeceasedInd(), patient);
        setPatientDeathTime(patientPerson.getDeceasedTime(), patient);
        setPersonalRelationships(patientPerson.getPersonalRelationship(), patient);
    }

    private void setPersonalRelationships(List<PRPAMT201301UV02PersonalRelationship> personalRelationships, Patient patient) throws
            HL7V3ParserException {
        if (!personalRelationships.isEmpty()) {
            int index = FIRST_OCCURRENCE;
            for (PRPAMT201301UV02PersonalRelationship relationship : personalRelationships) {
                Person person = new Person(patient);
                //TODO: setRelationshipAddress(relationship.getAddr(), person);
                //TODO: setRelationshipPhoneNumber(relationship.getTelecom());
                setRelationshipIdentifierFromList(relationship.getId(), person);
                setRelationshipCode(relationship.getCode(), person);
                COCTMT030007UVPerson relationshipHolder1 = relationship.getRelationshipHolder1();
                parseRelationshipHolder1(patient, index, person, relationshipHolder1);
                index++;
            }
        }
    }


    private void setRelationshipIdentifierFromList(List<II> id, Person person) {
        if (!id.isEmpty()) {
            // We are currently handling only one identifier, take the first one
            II firstId = id.get(FIRST_OCCURRENCE);
            setPersonIdentifier(person, firstId);
        }
    }
}
