package net.ihe.gazelle.simulator.pam.pds;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientHistory.ActionPerformed;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * <p>LinkUnlinkManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("linkUnlinkManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class LinkUnlinkManager extends PDS implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private boolean link = true;


	/**
	 * <p>isLink.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isLink() {
		return link;
	}

	/**
	 * <p>Setter for the field <code>link</code>.</p>
	 *
	 * @param link a boolean.
	 */
	public void setLink(boolean link) {
		this.link = link;
	}


	/** {@inheritDoc} */
	@Override
	protected  void updateSelectedEvent(){
		this.canSendMessage = false;
		ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
		bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A24);
	}

	@Override
	protected List<TransactionInstance> send() {
		try {
			List<TransactionInstance> transactions = null;
			if (link) {
				PatientHistory h = patientService.link(selectedPatient,secondaryPatient,sendingActor);
				transactions = patientService.sendADTMessage(
						new PatientService.Exchange(
								selectedSUT,
								sendingApplication,
								sendingFacility,
								sendingActor,
								receivingActor,
								simulatedTransaction,
								getHL7Domain(),
								PatientService.ADT.A24,
								selectedPatient,
								secondaryPatient
						));
			} else {
				PatientHistory h = patientService.unlink(selectedPatient,secondaryPatient,sendingActor);
				transactions = patientService.sendADTMessage(
						new PatientService.Exchange(
								selectedSUT,
								sendingApplication,
								sendingFacility,
								sendingActor,
								receivingActor,
								simulatedTransaction,
								getHL7Domain(),
								PatientService.ADT.A37,
								selectedPatient,
								secondaryPatient
						));
			}
			displayDDSPanel = false;
			displayPatientsList = false;
			// TODO ne faut-il pas réinitialiser selectedPatient et secondaryPatient
			// secondaryPatient = null;
			// selectedPatient = null;
			return transactions;
		} catch (HL7Exception e) {
			throw new SendindException(e.getMessage(),e);
		}
	}
	/** {@inheritDoc} */
	@Override
	public void setSelectedPatient(Patient selectedPatient) {
		super.setSelectedPatient(selectedPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "First patient has been selected");
		this.canSendMessage = this.selectedPatient!=null&&secondaryPatient!=null;
	}

	/** {@inheritDoc} */
	@Override
	public void setSecondaryPatient(Patient selectedPatient) {
		super.setSecondaryPatient(selectedPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Second patient has been selected");
		this.canSendMessage = this.selectedPatient!=null&&secondaryPatient!=null;
	}
	/**
	 * <p>generatePatientOne.</p>
	 */
	@SuppressWarnings("unchecked") // used in linkPatient.xhtml
	public void generatePatientOne() {
		selectedPatient = generateAPatient();
	}

	/**
	 * <p>generatePatientTwo.</p>
	 */
	@SuppressWarnings("unchecked") // used in linkPatient.xhtml
	public void generatePatientTwo() {
		secondaryPatient = generateAPatient();
	}

	private Patient generateAPatient() {
		Patient newPatient = super.generateNewPatient(sendingActor);
		if (newPatient != null) {
			String username = null;
			if (Identity.instance().getCredentials() != null) {
				username = Identity.instance().getCredentials().getUsername();
			}
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			newPatient = newPatient.savePatient(entityManager);
			PatientHistory history = new PatientHistory(newPatient, null, ActionPerformed.CREATED, null, username,
					null, null);
			history.save(entityManager);
			displayDDSPanel = true;
		}
		return newPatient;
	}

	/** {@inheritDoc} */
	@Override
	public void actionOnSelectedPatient(Patient patient) {
		// TODO Auto-generated method stub

	}

	/**
	 * <p>getLinkButtonClass.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLinkButtonClass(){
		if (link){
			return "gzl-btn-green";
		} else {
			return "gzl-btn";
		}
	}

	/**
	 * <p>getUnlinkButtonClass.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUnlinkButtonClass(){
		if (link){
			return "gzl-btn";
		} else {
			return "gzl-btn-green";
		}
	}

	/** {@inheritDoc} */
	@Override
	public void createNewRelationship() {
		// patients are not editable on that page
	}

	/** {@inheritDoc} */
	@Override
	public void saveRelationship() {
		// patients are not editable on that page
	}
}
