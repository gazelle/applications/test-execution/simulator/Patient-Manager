package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * <b>Class Description : </b>WorklistManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 29/09/15
 */

@Name("worklistManager")
@Scope(ScopeType.PAGE)
public class WorklistManager implements Serializable {

    public static final String UTF_8 = StandardCharsets.UTF_8.name();
    private static Logger log = LoggerFactory.getLogger(WorklistManager.class);

    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    public Encounter getSelectedEncounter() {
        return selectedEncounter;
    }

    public PatientIdentifier getPidUsedForWorklistCreation() {
        return pidUsedForWorklistCreation;
    }

    public void setPidUsedForWorklistCreation(PatientIdentifier pidUsedForWorklistCreation) {
        this.pidUsedForWorklistCreation = pidUsedForWorklistCreation;
    }

    @Create
    public void initPage() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Map<String, String> queryParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (queryParams.containsKey("pid")) {
            String patientIdDb = queryParams.get("pid");
            try {
                Integer pid = Integer.parseInt(patientIdDb);
                selectedPatient = entityManager.find(Patient.class, pid);
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(patientIdDb + " is not a correct identifier");
            }
        } else if (queryParams.containsKey("eid")) {
            String encounterIdDb = queryParams.get("eid");
            try {
                Integer eid = Integer.parseInt(encounterIdDb);
                selectedEncounter = entityManager.find(Encounter.class, eid);
                if (selectedEncounter != null){
                    selectedPatient = selectedEncounter.getPatient();
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(encounterIdDb + " is not a correct identifier");
            }
        }
    }

    private Patient selectedPatient;
    private Encounter selectedEncounter;
    private PatientIdentifier pidUsedForWorklistCreation = null;

    public String createWorklistForSelectedEncounter() {
        if ((selectedEncounter != null) && (pidUsedForWorklistCreation != null)) {
            selectedPatient = selectedEncounter.getPatient();
            try {
                return createWorklistUrl(pidUsedForWorklistCreation.getFullPatientId());
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "First select a patient identifier to send to Order Manager");
            return null;
        }
    }

    public String createWorklistForSelectedPatient() {
        if ((selectedPatient != null) && (pidUsedForWorklistCreation != null)) {
            selectedEncounter = null;
            try {
                return createWorklistUrl(pidUsedForWorklistCreation.getFullPatientId());
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "First select a patient identifier to send to Order Manager");
            return null;
        }
    }

    private String createWorklistUrl(String patientIdentifier) throws UnsupportedEncodingException {
        String createWorklistBaseUrl = ApplicationConfiguration.getValueOfVariable("create_worklist_url");
        String url;
        if (createWorklistBaseUrl != null) {
            StringBuilder bufferUrl = new StringBuilder(createWorklistBaseUrl);
            bufferUrl.append("?pid=");
            bufferUrl.append(patientIdentifier.replace("&", "%26").replace("^", "%5E"));
            if (selectedPatient.getFirstName() != null) {
                bufferUrl.append("&firstName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getFirstName().trim(), UTF_8));
            }
            if (selectedPatient.getAlternateFirstName() != null) {
                bufferUrl.append("&alternateFirstName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAlternateFirstName().trim(), UTF_8));
            }
            if (selectedPatient.getLastName() != null) {
                bufferUrl.append("&lastName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getLastName().trim(), UTF_8));
            }
            if (selectedPatient.getAlternateLastName() != null) {
                bufferUrl.append("&alternateLastName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAlternateLastName().trim(), UTF_8));
            }
            if (selectedPatient.getDateOfBirth() != null) {
                bufferUrl.append("&dateOfBirth=");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
                bufferUrl.append(sdf.format(selectedPatient.getDateOfBirth().getTime()));
            }
            if (selectedPatient.getAlternateMothersMaidenName() != null) {
                bufferUrl.append("&alternateMothersMaidenName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAlternateMothersMaidenName().trim(), UTF_8));
            }
            if (selectedPatient.getSecondName() != null) {
                bufferUrl.append("&secondName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getSecondName().trim(), UTF_8));
            }
            if (selectedPatient.getAlternateSecondName() != null) {
                bufferUrl.append("&alternateSecondName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAlternateSecondName().trim(), UTF_8));
            }
            if (selectedPatient.getThirdName() != null) {
                bufferUrl.append("&thirdName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getThirdName().trim(), UTF_8));
            }
            if (selectedPatient.getAlternateThirdName() != null) {
                bufferUrl.append("&alternateThirdName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAlternateThirdName().trim(), UTF_8));
            }
            bufferUrl.append("&genderCode=");
            bufferUrl.append(selectedPatient.getGenderCode());
            bufferUrl.append("&countryCode=");
            bufferUrl.append(selectedPatient.getCountryCode());
            bufferUrl.append("&sender=PatientManager");
            if (selectedPatient.getAddressLine() != null) {
                bufferUrl.append("&street=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getAddressLine(), UTF_8));
            }
            if (selectedPatient.getState() != null) {
                bufferUrl.append("&state=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getState(), UTF_8));
            }
            if (selectedPatient.getMotherMaidenName() != null) {
                bufferUrl.append("&motherMaidenName=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getMotherMaidenName(), UTF_8));
            }
            if (selectedPatient.getCity() != null) {
                bufferUrl.append("&city=");
                bufferUrl.append(URLEncoder.encode(selectedPatient.getCity(), UTF_8));
            }
            if (selectedPatient.getZipCode() != null) {
                bufferUrl.append("&zipCode=");
                bufferUrl.append(selectedPatient.getZipCode());
            }

            if (selectedEncounter != null) {
                if (selectedEncounter.getPatientClassCode() != null) {
                    bufferUrl.append("&patientClass=");
                    bufferUrl.append(selectedEncounter.getPatientClassCode());
                }
                if (selectedEncounter.getVisitNumber() != null) {
                    bufferUrl.append("&visitNumber=");
                    bufferUrl.append(selectedEncounter.getVisitNumber().replace("&", "%26").replace("^", "%5E"));
                }
                if (selectedEncounter.getReferringDoctorCode() != null) {
                    bufferUrl.append("&doctor=");
                    bufferUrl.append(selectedEncounter.getReferringDoctorCode());
                }
            }

            url = bufferUrl.toString();
        } else {
            url = null;
        }
        selectedPatient = null;
        pidUsedForWorklistCreation = null;
        selectedEncounter = null;
        return url;
    }

    public void setSelectedEncounter(Encounter selectedEncounter) {
        if (selectedEncounter != null) {
            this.selectedPatient = selectedEncounter.getPatient();
        } else {
            this.selectedPatient = null;
        }
        this.selectedEncounter = selectedEncounter;
    }

    /**
     * <p>getListOfIdentifiers.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PatientIdentifier> getListOfIdentifiers() {
        if (selectedPatient != null) {
            PatientQuery query = new PatientQuery();
            query.id().eq(selectedPatient.getId());
            Patient patient = query.getUniqueResult();
            return patient.getPatientIdentifiers();
        } else {
            return null;
        }
    }
}
