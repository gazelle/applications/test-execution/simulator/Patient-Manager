package net.ihe.gazelle.simulator.pam.pes;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.model.MovementQuery;
import org.jboss.seam.security.Identity;

import java.util.Map;

/**
 * <b>Class Description : </b>FilterMovementManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 21/09/15
 */
public class FilterMovementManager implements QueryModifier<Movement> {

    private Filter<Movement> filter;
    private String triggerEvent;
    private ActionType actionType;
    private Actor simulatedActor;
    private String visitNumber;

    /**
     * <p>Constructor for FilterMovementManager.</p>
     *
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param forEvent a {@link java.lang.String} object.
     * @param forAction a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
     */
    public FilterMovementManager(Actor actor, String forEvent, ActionType forAction) {
        this.simulatedActor = actor;
        this.triggerEvent = forEvent;
        this.actionType = forAction;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Movement> getFilter() {
        if (filter == null) {
            filter = new Filter<Movement>(getHQLCriteriaForFilters());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Movement> getHQLCriteriaForFilters() {
        MovementQuery query = new MovementQuery();
        HQLCriterionsForFilter<Movement> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstName", query.encounter().patient().firstName());
        criteria.addPath("lastName", query.encounter().patient().lastName());
        criteria.addPath("gender", query.encounter().patient().genderCode());
        criteria.addPath("creator", query.creator());
        criteria.addPath("timestamp", query.movementDate());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /**
     * <p>getMovements.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Movement> getMovements() {
        return new FilterDataModel<Movement>(getFilter()) {
            @Override
            protected Object getId(Movement movement) {
                return movement.getId();
            }
        };
    }

    /**
     * <p>reset.</p>
     */
    public void reset() {
        getFilter().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Movement> hqlQueryBuilder, Map<String, Object> map) {
        MovementQuery query = new MovementQuery();
        if (actionType != null) {
            switch (actionType) {
                case INSERT:
                    break;
                case CANCEL:
                    hqlQueryBuilder.addRestriction(HQLRestrictions.and(query.currentMovement().eqRestriction(true),
                            query.triggerEvent().eqRestriction(triggerEvent)));
                    hqlQueryBuilder.addRestriction(query.actionType().neqRestriction(ActionType.CANCEL));
                    hqlQueryBuilder.addRestriction(query.currentMovement().eqRestriction(true));
                    break;
                case UPDATE:
                    hqlQueryBuilder.addRestriction(query.triggerEvent().eqRestriction(triggerEvent));
                    hqlQueryBuilder.addRestriction(query.actionType().neqRestriction(ActionType.CANCEL));
                    break;
                default:
                    break;
            }
        }
        if (simulatedActor != null) {
            hqlQueryBuilder.addRestriction(query.simulatedActor().eqRestriction(simulatedActor));
        }
        if (visitNumber != null) {
            hqlQueryBuilder.addRestriction(query.encounter().visitNumber().likeRestriction(visitNumber, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
    }

    /**
     * <p>Getter for the field <code>visitNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVisitNumber() {
        return visitNumber;
    }

    /**
     * <p>Setter for the field <code>visitNumber</code>.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     */
    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }
}
