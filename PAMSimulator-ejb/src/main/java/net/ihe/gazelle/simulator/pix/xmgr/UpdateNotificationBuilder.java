package net.ihe.gazelle.simulator.pix.xmgr;

import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.message.ADT_A05;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.parser.ParserConfiguration;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to build the update notification message (ADT^A31^ADT_A05) to send to the Patient Identity Consumer actor (and issued by the PIX Manager)
 *
 * @author aberge
 * @version $Id: $Id
 */
public class UpdateNotificationBuilder extends SegmentBuilder {

	private CrossReference referenceToSend;
	private Patient dereferencedPatient;
	private static SimpleDateFormat DATEFORMATTER = new SimpleDateFormat("yyyyMMddHHmmss");

	private static Logger log = LoggerFactory.getLogger(UpdateNotificationBuilder.class);

	/**
	 * <p>Constructor for UpdateNotificationBuilder.</p>
	 *
	 * @param reference a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
	 * @param dereferencedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public UpdateNotificationBuilder(CrossReference reference, Patient dereferencedPatient) {
		this.referenceToSend = reference;
		this.dereferencedPatient = dereferencedPatient;
	}

	/**
	 * <p>build.</p>
	 *
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 * @param simulatorApplication a {@link java.lang.String} object.
	 * @param simulatorFacility a {@link java.lang.String} object.
	 * @param domainsToSend a {@link java.util.List} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<String> build(HL7V2ResponderSUTConfiguration sut, String simulatorApplication, String simulatorFacility,
			List<HierarchicDesignator> domainsToSend) {
		List<String> messages = new ArrayList<String>();
		// parser
		ParserConfiguration config = new ParserConfiguration();
		config.addForcedEncode("PID-5");
		PipeParser pipeParser = new PipeParser();
		pipeParser.setParserConfiguration(config);
		if (referenceToSend != null) {
			ADT_A05 adtMessage = new ADT_A05();
			try {
				populateMSH(adtMessage.getMSH(), sut, simulatorApplication, simulatorFacility);
				fillEVNSegment(adtMessage.getEVN(), DATEFORMATTER);
				int rep = 0;
				for (Patient patient : referenceToSend.getPatients()) {
					for (PatientIdentifier identifier : patient.getPatientIdentifiers()) {
						if ((domainsToSend != null) && domainsToSend.contains(identifier.getDomain())) {
							adtMessage.getPID().getPatientIdentifierList(rep).parse(identifier.getFullPatientId());
							rep++;
						}
					}
				}
				if (rep == 0) {
					log.warn("None of the selected patients have identifiers in the domain supported by this system");
				} else {
					adtMessage.getPID().getPatientName(0).getFamilyName().getSurname().setValue(" ");
					adtMessage.getPID().getPatientName(0).getNameTypeCode().setValue("S");
					adtMessage.getPV1().getPatientClass().setValue("N");
					messages.add(pipeParser.encode(adtMessage));
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		if (dereferencedPatient != null) {
			ADT_A05 adtMessage = new ADT_A05();
			try {
				populateMSH(adtMessage.getMSH(), sut, simulatorApplication, simulatorFacility);
				fillEVNSegment(adtMessage.getEVN(), DATEFORMATTER);
				int rep = 0;
				for (PatientIdentifier identifier : dereferencedPatient.getPatientIdentifiers()) {
					if ((domainsToSend != null) && domainsToSend.contains(identifier.getDomain())) {
						adtMessage.getPID().getPatientIdentifierList(rep).parse(identifier.getFullPatientId());
						rep++;
					}
				}
				if (rep == 0) {
					log.warn("The dereferenced patient has no identifier in the domain supported by this system");
				} else {
					adtMessage.getPID().getPatientName(0).getFamilyName().getSurname().setValue(" ");
					adtMessage.getPID().getPatientName(0).getNameTypeCode().setValue("S");
					adtMessage.getPV1().getPatientClass().setValue("N");
					messages.add(pipeParser.encode(adtMessage));
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		return messages;
	}

	/**
	 * <p>populateMSH.</p>
	 *
	 * @param mshSegment a {@link ca.uhn.hl7v2.model.v25.segment.MSH} object.
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 * @param sendingApplication a {@link java.lang.String} object.
	 * @param sendingFacility a {@link java.lang.String} object.
	 * @throws ca.uhn.hl7v2.model.DataTypeException if any.
	 */
	public void populateMSH(MSH mshSegment, HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility)
			throws DataTypeException {

		fillMSHSegment(mshSegment, sut.getApplication(), sut.getFacility(), sendingApplication, sendingFacility, "ADT",
				"A31", "ADT_A05", "2.5", DATEFORMATTER, sut.getCharset().getHl7Code());
	}
}
