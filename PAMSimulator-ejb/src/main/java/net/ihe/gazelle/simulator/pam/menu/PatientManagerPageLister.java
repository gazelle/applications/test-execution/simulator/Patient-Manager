package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@MetaInfServices(PageLister.class)
public class PatientManagerPageLister implements PageLister {

    /** {@inheritDoc} */
    @Override
    public Collection<Page> getPages() {
        Collection<Page> pages = new ArrayList<Page>();
        pages.addAll(Arrays.asList(PatientManagerPages.values()));
        return pages;
    }
}
