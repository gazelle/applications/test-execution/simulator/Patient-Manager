package net.ihe.gazelle.simulator.pix.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.pix.hl7v2.model.v25.segment.MSH;
import net.ihe.gazelle.pix.hl7v2.model.v25.segment.QPD;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PIXMessageBuilder extends SegmentBuilder {

	private HL7V2ResponderSUTConfiguration sut;

	public PIXMessageBuilder(HL7V2ResponderSUTConfiguration sut) {
		this.sut = sut;
	}

	public String buildQ21Message(PatientIdentifier personIdentifier, List<AssigningAuthority> domainsReturned) {
		QBP_Q21 qbpMessage = new QBP_Q21();
		try {
			fillMSHSegment(qbpMessage.getMSH());
			fillQPDSegment(qbpMessage.getQPD(), domainsReturned, personIdentifier);
			fillRCPSegment(qbpMessage.getRCP());
			PipeParser parser = PipeParser.getInstanceWithNoValidation();
			return parser.encode(qbpMessage);
		} catch (HL7Exception e) {
			return null;
		}
	}

	private void fillMSHSegment(MSH mshSegment) throws HL7Exception {
		ca.uhn.hl7v2.model.v25.message.QBP_Q21 transientMessage = new ca.uhn.hl7v2.model.v25.message.QBP_Q21();
		fillMSHSegment(transientMessage.getMSH(), sut.getApplication(), sut.getFacility(), "PatientManager", "IHE",
				"QBP", "Q23", "QBP_Q21", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"), sut.getCharset().getHl7Code());
		String mshSegmentAsString = transientMessage.getMSH().encode();
		mshSegment.getFieldSeparator().setValue("|");
		mshSegment.getEncodingCharacters().setValue("^~\\&");
		mshSegment.parse(mshSegmentAsString);
	}

	private void fillQPDSegment(QPD qpdSegment, List<AssigningAuthority> returnedDomains,
			PatientIdentifier personIdentifier) throws DataTypeException {
		// query tag
		qpdSegment.getQueryTag().setValue(
				"GazellePIXConsumer." + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		qpdSegment.getMessageQueryName().getIdentifier().setValue("IHE PIX Query");
		if ((personIdentifier.getFullPatientId() != null) && !personIdentifier.getFullPatientId().isEmpty()) {
			qpdSegment.getPersonIdentifier().getIDNumber().setValue(personIdentifier.getFullPatientId());
		}
		if (personIdentifier.getDomain() != null) {
			if ((personIdentifier.getDomain().getNamespaceID() != null)
					&& !personIdentifier.getDomain().getNamespaceID().isEmpty()) {
				qpdSegment.getPersonIdentifier().getAssigningAuthority().getNamespaceID()
						.setValue(personIdentifier.getDomain().getNamespaceID());
			}
			if ((personIdentifier.getDomain().getUniversalID() != null)
					&& !personIdentifier.getDomain().getUniversalID().isEmpty()) {
				qpdSegment.getPersonIdentifier().getAssigningAuthority().getUniversalID()
						.setValue(personIdentifier.getDomain().getUniversalID());
			}
			if ((personIdentifier.getDomain().getUniversalIDType() != null)
					&& !personIdentifier.getDomain().getUniversalIDType().isEmpty()) {
				qpdSegment.getPersonIdentifier().getAssigningAuthority().getUniversalIDType()
						.setValue(personIdentifier.getDomain().getUniversalIDType());
			}
		}
		// what domains returned
		if ((returnedDomains != null) && !returnedDomains.isEmpty()) {
			int domrep = 0;
			for (AssigningAuthority domain : returnedDomains) {
				if ((domain.getNamespace() != null) && !domain.getNamespace().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getNamespaceID()
							.setValue(domain.getNamespace());
				}
				if ((domain.getUniversalId() != null) && !domain.getNamespace().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getUniversalID()
							.setValue(domain.getUniversalId());
				}
				if ((domain.getUniversalIdType() != null) && !domain.getUniversalIdType().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getUniversalIDType()
							.setValue(domain.getUniversalIdType());
				}
				domrep++;
			}
		}
	}

	private void fillRCPSegment(net.ihe.gazelle.pix.hl7v2.model.v25.segment.RCP rcpSegment) throws DataTypeException {
		rcpSegment.getQueryPriority().setValue("I"); // immediate mode
	}
}
