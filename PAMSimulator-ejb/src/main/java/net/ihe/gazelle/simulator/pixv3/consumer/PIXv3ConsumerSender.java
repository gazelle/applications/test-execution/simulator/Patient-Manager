package net.ihe.gazelle.simulator.pixv3.consumer;

import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class PIXv3ConsumerSender extends SoapHL7V3WebServiceClient implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5525858837170285232L;

	private static Logger log = LoggerFactory.getLogger(PIXv3ConsumerSender.class);

	private static final String ACTION_FIND = "urn:hl7-org:v3:PRPA_IN201309UV02";
	private static final String SERVICE_NAME = "PIXManager_Service";
	private static final String SERVICE_PORT_TYPE = "PIXManager_Port_Soap12";

	public PIXv3ConsumerSender(HL7v3ResponderSUTConfiguration inSUT) {
		super(inSUT);
		this.transactionInstance = null;
	}

	@Override
	protected QName getServiceQName() {
		return new QName(PIXV3_TNS, SERVICE_NAME);
	}

	@Override
	protected QName getPortQName() {
		return new QName(PIXV3_TNS, SERVICE_PORT_TYPE);
	}

	@Override
	protected Actor getSimulatedActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER");
	}

	@Override
	protected Transaction getSimulatedTransaction() {
		return Transaction.GetTransactionByKeyword("ITI-45");
	}

	@Override
	protected Actor getSutActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
	}

	public PRPAIN201310UV02Type sendPatientRegistryGetIdentifierQuery(PRPAIN201309UV02Type request) throws SoapSendException {
		transactionInstance = null;
		PRPAIN201310UV02Type response =  send(PRPAIN201310UV02Type.class, request.get_xmlNodePresentation(), ACTION_FIND);
		String responseType = null;
		if (response != null) {
			responseType = response.get_xmlNodePresentation().getLocalName();
		}
		transactionInstance = saveTransactionInstance(getIdentifierQueryToByteArray(request),
				getIdentifierQueryResponseToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
				responseType);
		HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
		if (response != null) {
			HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
		}
		return response;
	}

	private byte[] getIdentifierQueryToByteArray(PRPAIN201309UV02Type request) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			HL7V3Transformer.marshallMessage(PRPAIN201309UV02Type.class, baos, request);
			return baos.toByteArray();
		} catch (JAXBException e) {
			log.error(e.getMessage(), e);
			return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
		}
	}

	private byte[] getIdentifierQueryResponseToByteArray(PRPAIN201310UV02Type response) {
		if (response != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				HL7V3Transformer.marshallMessage(PRPAIN201310UV02Type.class, baos, response);
				return baos.toByteArray();
			} catch (JAXBException e) {
				log.error(e.getMessage(), e);
				return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
			}
		} else {
			return null;
		}
	}

}
