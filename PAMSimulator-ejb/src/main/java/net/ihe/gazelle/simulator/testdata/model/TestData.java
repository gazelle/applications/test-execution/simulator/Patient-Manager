package net.ihe.gazelle.simulator.testdata.model;

import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.fhir.util.FhirReturnType;
import net.ihe.gazelle.xua.model.PicketLinkCredentials;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by aberge on 08/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Name("testData")
@Table(name = "pam_test_data", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@SequenceGenerator(name = "pam_test_data_sequence", sequenceName = "pam_test_data_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class TestData implements Serializable{

    @Id
    @GeneratedValue(generator = "pam_test_data_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "is_shared")
    private boolean shared;

    @Column(name = "last_modifier")
    private String lastModifier;

    @Column(name = "last_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    @ManyToOne(targetEntity = PicketLinkCredentials.class)
    @JoinColumn(name = "credentials_id")
    private PicketLinkCredentials credentials;

    @Column(name = "deferred_option")
    private boolean deferredOption;

    @ManyToOne(targetEntity = Transaction.class)
    @JoinColumn(name = "transaction_id")
    private Transaction applyToTransaction;

    @Column(name = "fhir_return_type")
    private FhirReturnType fhirReturnType;

    @ManyToMany
    @JoinTable(name = "pam_test_data_restricted_domain",
            joinColumns = @JoinColumn(name = "restricted_domain_id"),
            inverseJoinColumns = @JoinColumn(name = "test_data_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"restricted_domain_id", "test_data_id"}))
    private List<HierarchicDesignator> restrictedDomain;

    @Column(name = "limit_value")
    private Integer limitValue;

    /**
     * <p>updateModifierAndDate.</p>
     */
    public void updateModifierAndDate(){
        this.lastModified = new Date();
        if (Identity.instance().isLoggedIn()){
            this.lastModifier = Identity.instance().getCredentials().getUsername();
        }
    }

    /**
     * <p>Constructor for TestData.</p>
     */
    public TestData(){
        this.shared = false;
        this.deferredOption = false;
        this.limitValue = -1;
        this.fhirReturnType = null;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>isShared.</p>
     *
     * @return a boolean.
     */
    public boolean isShared() {
        return shared;
    }

    /**
     * <p>Setter for the field <code>shared</code>.</p>
     *
     * @param shared a boolean.
     */
    public void setShared(boolean shared) {
        this.shared = shared;
    }

    /**
     * <p>Getter for the field <code>lastModifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastModifier() {
        return lastModifier;
    }

    /**
     * <p>Setter for the field <code>lastModifier</code>.</p>
     *
     * @param lastModifier a {@link java.lang.String} object.
     */
    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    /**
     * <p>Getter for the field <code>lastModified</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastModified() {
        return lastModified;
    }

    /**
     * <p>Setter for the field <code>lastModified</code>.</p>
     *
     * @param lastModified a {@link java.util.Date} object.
     */
    public void setLastModified(Date lastModified) {
        if (lastModified != null) {
            this.lastModified = (Date) lastModified.clone();
        } else {
            this.lastModified = null;
        }
    }


    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>credentials</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     */
    public PicketLinkCredentials getCredentials() {
        return credentials;
    }

    /**
     * <p>Setter for the field <code>credentials</code>.</p>
     *
     * @param credentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     */
    public void setCredentials(PicketLinkCredentials credentials) {
        this.credentials = credentials;
    }

    /**
     * <p>isDeferredOption.</p>
     *
     * @return a boolean.
     */
    public boolean isDeferredOption() {
        return deferredOption;
    }

    /**
     * <p>Setter for the field <code>deferredOption</code>.</p>
     *
     * @param deferredOption a boolean.
     */
    public void setDeferredOption(boolean deferredOption) {
        this.deferredOption = deferredOption;
    }

    /**
     * <p>Getter for the field <code>restrictedDomain</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getRestrictedDomain() {
        return restrictedDomain;
    }

    /**
     * <p>Setter for the field <code>restrictedDomain</code>.</p>
     *
     * @param restrictedDomain a {@link java.util.List} object.
     */
    public void setRestrictedDomain(List<HierarchicDesignator> restrictedDomain) {
        this.restrictedDomain = restrictedDomain;
    }

    /**
     * <p>Getter for the field <code>limitValue</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLimitValue() {
        return limitValue;
    }

    /**
     * <p>Setter for the field <code>limitValue</code>.</p>
     *
     * @param limitValue a {@link java.lang.Integer} object.
     */
    public void setLimitValue(Integer limitValue) {
        this.limitValue = limitValue;
    }

    /**
     * <p>useXua.</p>
     *
     * @return a boolean.
     */
    public boolean useXua(){
        return credentials != null;
    }

    /**
     * <p>limitNumberOfResponse.</p>
     *
     * @return a boolean.
     */
    public boolean limitNumberOfResponse(){
        return limitValue >= 0;
    }

    /**
     * <p>Getter for the field <code>applyToTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getApplyToTransaction() {
        return applyToTransaction;
    }

    /**
     * <p>Setter for the field <code>applyToTransaction</code>.</p>
     *
     * @param applyToTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setApplyToTransaction(Transaction applyToTransaction) {
        this.applyToTransaction = applyToTransaction;
    }

    /**
     * <p>Getter for the field <code>fhirReturnType</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public FhirReturnType getFhirReturnType() {
        return fhirReturnType;
    }

    /**
     * <p>Setter for the field <code>fhirReturnType</code>.</p>
     *
     * @param fhirReturnType a {@link net.ihe.gazelle.simulator.fhir.util.FhirReturnType} object.
     */
    public void setFhirReturnType(FhirReturnType fhirReturnType) {
        this.fhirReturnType = fhirReturnType;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TestData testData = (TestData) o;

        if (shared != testData.shared) {
            return false;
        }
        if (deferredOption != testData.deferredOption) {
            return false;
        }
        if (name != null ? !name.equals(testData.name) : testData.name != null) {
            return false;
        }
        if (description != null ? !description.equals(testData.description) : testData.description != null) {
            return false;
        }
        if (lastModifier != null ? !lastModifier.equals(testData.lastModifier) : testData.lastModifier != null) {
            return false;
        }
        if (lastModified != null ? !lastModified.equals(testData.lastModified) : testData.lastModified != null) {
            return false;
        }
        return limitValue != null ? limitValue.equals(testData.limitValue) : testData.limitValue == null;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (shared ? 1 : 0);
        result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
        result = 31 * result + (lastModified != null ? lastModified.hashCode() : 0);
        result = 31 * result + (deferredOption ? 1 : 0);
        result = 31 * result + (limitValue != null ? limitValue.hashCode() : 0);
        return result;
    }
}
