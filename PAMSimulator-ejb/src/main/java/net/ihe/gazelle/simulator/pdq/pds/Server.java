package net.ihe.gazelle.simulator.pdq.pds;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.ejb.DependsOn;

/**
 * <p>Server class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Startup(depends = {"entityManager"})
@Name("pdqpdsServer")
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
public class Server  extends AbstractServer<PDQPDS> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	@Override
    @Create
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword(PatientManagerConstants.PDS, entityManager);
		Domain domain = Domain.getDomainByKeyword(PatientManagerConstants.ITI, entityManager);
		handler = new PDQPDS();
		startServers(simulatedActor, null, domain);
	}

}
