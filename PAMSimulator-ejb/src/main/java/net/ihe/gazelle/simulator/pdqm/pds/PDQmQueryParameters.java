package net.ihe.gazelle.simulator.pdqm.pds;

import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.NumberParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.StringOrListParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;

/**
 * <p>PDQmQueryParameters class.</p>
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */

public class PDQmQueryParameters {

    private StringParam id;
    private TokenParam active;
    private TokenAndListParam identifierList;
    private StringAndListParam familyNames;
    private StringAndListParam givenNames;
    private DateParam birthdate;
    private StringParam address;
    private StringParam addressCity;
    private StringParam addressCountry;
    private StringParam addressPostalcode;
    private StringParam addressState;
    private TokenParam gender;
    private TokenParam telecom;
    private StringParam motherMaidenFamilyName;

    public PDQmQueryParameters(){

    }

    public boolean hasParam(){
        if (id != null && !id.isEmpty()){
            return true;
        } else if (active != null && active.isEmpty()){
            return true;
        } else if (identifierList != null){
            return true;
        } else if (familyNames != null){
            return true;
        } else if (birthdate != null && !birthdate.isEmpty()){
            return true;
        } else if (givenNames != null){
            return true;
        } else if (address != null && !address.isEmpty()){
            return true;
        } else if (addressCity != null && !addressCity.isEmpty()){
            return true;
        } else if (addressPostalcode != null && !addressPostalcode.isEmpty()){
            return true;
        } else if (addressCountry != null && !addressCountry.isEmpty()){
            return true;
        } else if (addressState != null && !addressState.isEmpty()){
            return true;
        } else if (gender != null && !gender.isEmpty()){
            return true;
        } else if (telecom != null && !telecom.isEmpty()){
            return true;
        } else if (motherMaidenFamilyName != null && !motherMaidenFamilyName.isEmpty()){
            return true;
        }
        return false;
    }

    public StringParam getId() {
        return id;
    }

    public void setId(StringParam id) {
        this.id = id;
    }

    public TokenParam getActive() {
        return active;
    }

    public void setActive(TokenParam active) {
        this.active = active;
    }

    public TokenAndListParam getIdentifierList() {
        return identifierList;
    }

    public void setIdentifierList(TokenAndListParam identifierList) {
        this.identifierList = identifierList;
    }

    public StringAndListParam getFamilyNames() {
        return familyNames;
    }

    public void setFamilyNames(StringAndListParam familyNames) {
        this.familyNames = familyNames;
    }

    public StringAndListParam getGivenNames() {
        return givenNames;
    }

    public void setGivenNames(StringAndListParam givenNames) {
        this.givenNames = givenNames;
    }

    public DateParam getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(DateParam birthdate) {
        this.birthdate = birthdate;
    }

    public StringParam getAddress() {
        return address;
    }

    public void setAddress(StringParam address) {
        this.address = address;
    }

    public StringParam getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(StringParam addressCity) {
        this.addressCity = addressCity;
    }

    public StringParam getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(StringParam addressCountry) {
        this.addressCountry = addressCountry;
    }

    public StringParam getAddressPostalcode() {
        return addressPostalcode;
    }

    public void setAddressPostalcode(StringParam addressPostalcode) {
        this.addressPostalcode = addressPostalcode;
    }

    public StringParam getAddressState() {
        return addressState;
    }

    public void setAddressState(StringParam addressState) {
        this.addressState = addressState;
    }

    public TokenParam getGender() {
        return gender;
    }

    public void setGender(TokenParam gender) {
        this.gender = gender;
    }

    public TokenParam getTelecom() {
        return telecom;
    }

    public void setTelecom(TokenParam telecom) {
        this.telecom = telecom;
    }

    public StringParam getMotherMaidenFamilyName() {
        return motherMaidenFamilyName;
    }

    public void setMotherMaidenFamilyName(StringParam motherMaidenFamilyName) {
        this.motherMaidenFamilyName = motherMaidenFamilyName;
    }
}
