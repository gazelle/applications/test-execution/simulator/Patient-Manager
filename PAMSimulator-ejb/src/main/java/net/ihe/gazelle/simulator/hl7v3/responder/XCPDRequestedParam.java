package net.ihe.gazelle.simulator.hl7v3.responder;

/**
 * <b>Class Description : </b>XCPDRequestedParam<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 20/01/16
 *


 */
public enum XCPDRequestedParam {

    GENDER("LivingSubjectAdministrativeGenderRequested"),
    ADDRESS("PatientAddressRequested"),
    TELECOM("PatientTelecomRequested"),
    BIRTH_PLACE_NAME("LivingSubjectBirthPlaceNameRequested"),
    BIRTH_PLACE_ADDRESS("LivingSubjectBirthPlaceAddressRequested"),
    MOTHERS_MAIDEN_NAME("MothersMaidenNameRequested");

    String value;

    XCPDRequestedParam(String inValue) {
        this.value = inValue;
    }

    public String getValue() {
        return value;
    }
}
