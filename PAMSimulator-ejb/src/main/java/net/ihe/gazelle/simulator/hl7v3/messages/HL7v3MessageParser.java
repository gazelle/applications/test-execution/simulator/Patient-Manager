package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.datatypes.AD;
import net.ihe.gazelle.hl7v3.datatypes.AdxpCity;
import net.ihe.gazelle.hl7v3.datatypes.AdxpCountry;
import net.ihe.gazelle.hl7v3.datatypes.AdxpState;
import net.ihe.gazelle.hl7v3.datatypes.AdxpStreetAddressLine;
import net.ihe.gazelle.hl7v3.datatypes.BL;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.EN;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.EnGiven;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.datatypes.TEL;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>HL7v3MessageParser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7v3MessageParser {

    /** Constant <code>FIRST_OCCURRENCE=0</code> */
    protected static final int FIRST_OCCURRENCE = 0;
    protected II messageId;

    private static final int PRINCIPAL_NAME = 0;
    private static final int FIRST_OCCURENCE = 0;
    private static final int FIRST_NAME = 0;
    private static final int SECOND_NAME = 1;
    private static final int THIRD_NAME = 2;
    private static final int ALTERNATE_NAME = 1;

    /**
     * <p>parsePatientIdentifier.</p>
     *
     * @param idList a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    protected static List<PatientIdentifier> parsePatientIdentifier(List<? extends II> idList) {
        List<PatientIdentifier> identifierList = new ArrayList<PatientIdentifier>();
        for (II pid : idList) {
            PatientIdentifier identifier = getPatientIdentifierFromII(pid);
            if (identifier != null && !identifierList.contains(identifier)) {
                identifierList.add(identifier);
            }
        }
        return identifierList;
    }

    /**
     * <p>getPatientIdentifierFromII.</p>
     *
     * @param pid a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    protected static PatientIdentifier getPatientIdentifierFromII(II pid) {
        String authorityOid = pid.getRoot();
        String authorityName = pid.getAssigningAuthorityName();
        if (authorityName != null || authorityOid != null) {
            HierarchicDesignator authority;
            authority = HierarchicDesignatorDAO.getAssigningAuthority(authorityName, authorityOid, DesignatorType.PATIENT_ID);
            if (authority == null) {
                HierarchicDesignatorDAO.createAssigningAuthority(authorityName, authorityOid, null, DesignatorType.PATIENT_ID);
            }
            PatientIdentifier identifier = new PatientIdentifier();
            identifier.setIdNumber(pid.getExtension());
            identifier.setDomain(authority);
            // set it now since we use it to find duplicates in DB
            identifier.setFullPatientIdentifierIfEmpty();
            return identifier;
        }
        return null;
    }

    /**
     * <p>setPatientAddresses.</p>
     *
     * @param addr a {@link java.util.List} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setPatientAddresses(List<AD> addr, Patient patient) {
        boolean isMainAddress = true;
        for (AD address : addr) {
            PatientAddress patientAddress = new PatientAddress();
            patientAddress.setMainAddress(isMainAddress);
            List<AdxpCity> cities = address.getCity();
            List<AdxpStreetAddressLine> streets = address.getStreetAddressLine();
            List<AdxpCountry> countries = address.getCountry();
            List<AdxpState> states = address.getState();
            String countryCode = null;
            if (!cities.isEmpty() && !cities.get(FIRST_OCCURENCE).getListStringValues().isEmpty()) {
                patientAddress.setCity(cities.get(FIRST_OCCURRENCE).getListStringValues().get(FIRST_OCCURRENCE));
            }
            if (!streets.isEmpty() && !streets.get(FIRST_OCCURENCE).getListStringValues().isEmpty()) {
                patientAddress.setAddressLine(streets.get(FIRST_OCCURRENCE).getListStringValues().get(FIRST_OCCURRENCE));
            }
            if (!countries.isEmpty() && !countries.get(FIRST_OCCURENCE).getListStringValues().isEmpty()) {
                countryCode = countries.get(FIRST_OCCURRENCE).getListStringValues().get(FIRST_OCCURRENCE);
                patientAddress.setCountryCode(countryCode);
            }
            if (!states.isEmpty() && !states.get(FIRST_OCCURENCE).getListStringValues().isEmpty()) {
                patientAddress.setState(states.get(FIRST_OCCURRENCE).getListStringValues().get(FIRST_OCCURRENCE));
            }
            if (isMainAddress) {
                patient.setCountryCode(countryCode);
            }
            patientAddress.setPatient(patient);
            patient.addPatientAddress(patientAddress);
            isMainAddress = false;
        }
    }

    /**
     * <p>setPatientNames.</p>
     *
     * @param names a {@link java.util.List} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setPatientNames(List<PN> names, Patient patient) {
        int index = PRINCIPAL_NAME;
        for (PN pn : names) {
            switch (index) {
                case PRINCIPAL_NAME:
                    patient.setLastName(extractLastName(pn));
                    patient.setFirstName(extractFirstName(pn));
                    patient.setSecondName(extractSecondName(pn));
                    patient.setThirdName(extractThirdName(pn));
                    break;
                case ALTERNATE_NAME:
                    patient.setAlternateLastName(extractLastName(pn));
                    patient.setAlternateSecondName(extractSecondName(pn));
                    patient.setAlternateFirstName(extractFirstName(pn));
                    patient.setAlternateThirdName(extractThirdName(pn));
                    break;
                default:
                    break;
            }
            index++;
        }
    }

    private static String extractLastName(EN en){
        return getFirstFamily(en.getFamily());
    }

    private static String extractFirstName(EN en){
        return getGivenNameInstance(en, FIRST_NAME);
    }

    private static String extractSecondName(EN en){
        return getGivenNameInstance(en, SECOND_NAME);
    }

    private static String extractFirstName(PN pn) {
        return getGivenNameInstance(pn, FIRST_NAME);
    }

    private static String extractSecondName(PN pn) {
        return getGivenNameInstance(pn, SECOND_NAME);
    }

    private static String extractThirdName(PN pn) {
        return getGivenNameInstance(pn, THIRD_NAME);
    }

    private static String getGivenNameInstance(PN pn, int occurrence) {
        return getGivenNameOccurrence(pn.getGiven(), occurrence);
    }

    private static String getGivenNameInstance(EN pn, int occurrence) {
        return getGivenNameOccurrence(pn.getGiven(), occurrence);
    }

    private static String getGivenNameOccurrence(List<EnGiven> enGivenList, int occurrence){
        if (!enGivenList.isEmpty()) {
            try {
                EnGiven given = enGivenList.get(occurrence);
                if (!given.getListStringValues().isEmpty()) {
                    return given.getListStringValues().get(FIRST_OCCURENCE);
                }
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }
        return null;
    }

    private static String extractLastName(PN pn) {
        return getFirstFamily(pn.getFamily());
    }

    private static String getFirstFamily(List<EnFamily> enFamilyList){
        if (!enFamilyList.isEmpty()) {
            EnFamily firstOccurence = enFamilyList.get(FIRST_OCCURENCE);
            if (!firstOccurence.getListStringValues().isEmpty()) {
                return firstOccurence.getListStringValues().get(FIRST_OCCURENCE);
            }
        }
        return null;
    }

    /**
     * <p>setBirthTime.</p>
     *
     * @param birthTime a {@link net.ihe.gazelle.hl7v3.datatypes.TS} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setBirthTime(TS birthTime, Patient patient) {
        if (birthTime != null) {
            patient.setDateOfBirth(birthTime.getValue());
        }
    }

    /**
     * <p>setGenderCode.</p>
     *
     * @param administrativeGenderCode a {@link net.ihe.gazelle.hl7v3.datatypes.CE} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setGenderCode(CE administrativeGenderCode, Patient patient) {
        if (administrativeGenderCode != null && administrativeGenderCode.getCode() != null) {
            patient.setGenderCode(administrativeGenderCode.getCode());
        }
    }

    /**
     * <p>setReligionCode.</p>
     *
     * @param religiousAffiliationCode a {@link net.ihe.gazelle.hl7v3.datatypes.CE} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setReligionCode(CE religiousAffiliationCode, Patient patient) {
        if (religiousAffiliationCode != null) {
            patient.setReligionCode(religiousAffiliationCode.getCode());
        }
    }

    /**
     * <p>setRaceCode.</p>
     *
     * @param raceCodes a {@link java.util.List} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setRaceCode(List<CE> raceCodes, Patient patient){
        if (!raceCodes.isEmpty()){
            patient.setRaceCode(raceCodes.get(FIRST_OCCURRENCE).getCode());
        }
    }

    /**
     * <p>setPatientPhoneNumbers.</p>
     *
     * @param telecomList a {@link java.util.List} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setPatientPhoneNumbers(List<TEL> telecomList, Patient patient) {
        boolean principal = true;
        for (TEL telecom : telecomList) {
            PatientPhoneNumber phoneNumber = new PatientPhoneNumber();
            phoneNumber.setPrincipal(principal);
            phoneNumber.setValue(telecom.getValue());
            phoneNumber.setType(PhoneNumberType.getTypeForHL7v3(telecom.getUse()));
            phoneNumber.setPatient(patient);
            patient.addPhoneNumber(phoneNumber);
            principal = false;
        }
    }

    /**
     * <p>setMultipleBirthInd.</p>
     *
     * @param multipleBirthInd a {@link net.ihe.gazelle.hl7v3.datatypes.BL} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setMultipleBirthInd(BL multipleBirthInd, Patient patient) {
        if (multipleBirthInd != null){
            patient.setMultipleBirthIndicator(multipleBirthInd.getValue());
        }
    }

    /**
     * <p>setMultipleBirthOrder.</p>
     *
     * @param multipleBirthOrderNumber a {@link net.ihe.gazelle.hl7v3.datatypes.INT} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setMultipleBirthOrder(INT multipleBirthOrderNumber, Patient patient){
        if (multipleBirthOrderNumber != null){
            patient.setBirthOrder(multipleBirthOrderNumber.getValue());
        }
    }

    /**
     * <p>setPatientDeathTime.</p>
     *
     * @param deceasedTime a {@link net.ihe.gazelle.hl7v3.datatypes.TS} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setPatientDeathTime(TS deceasedTime, Patient patient) {
        if (deceasedTime != null && deceasedTime.getValue() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                patient.setPatientDeathTime(sdf.parse(deceasedTime.getValue()));
            } catch (ParseException e) {
                patient.setPatientDeathTime(null);
            }
        }
    }

    /**
     * <p>setIsPatientDead.</p>
     *
     * @param deceasedInd a {@link net.ihe.gazelle.hl7v3.datatypes.BL} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected static void setIsPatientDead(BL deceasedInd, Patient patient) {
        if (deceasedInd != null && deceasedInd.getValue() != null){
            patient.setPatientDead(deceasedInd.getValue());
        } else {
            patient.setPatientDead(false);
        }
    }

    /**
     * <p>setPersonName.</p>
     *
     * @param personName a {@link net.ihe.gazelle.hl7v3.datatypes.EN} object.
     * @param person a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    protected static void setPersonName(EN personName, Person person) {
        person.setLastName(extractLastName(personName));
        person.setFirstName(extractFirstName(personName));
        person.setSecondName(extractSecondName(personName));
    }


    /**
     * <p>Getter for the field <code>messageId</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     */
    public II getMessageId() {
        return messageId;
    }
}
