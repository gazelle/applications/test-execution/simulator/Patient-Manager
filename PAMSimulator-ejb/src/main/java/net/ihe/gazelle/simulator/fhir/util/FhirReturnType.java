package net.ihe.gazelle.simulator.fhir.util;

import ca.uhn.fhir.rest.api.EncodingEnum;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 15/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum FhirReturnType {
    XML("xml", EncodingEnum.XML),
    JSON("json", EncodingEnum.JSON);

    private String label;
    private EncodingEnum hapiEnum;

    FhirReturnType(String inLabel, EncodingEnum hapiEnum) {
        this.label = inLabel;
        this.hapiEnum = hapiEnum;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    public EncodingEnum getHapiEnum() {
        return hapiEnum;
    }

    /**
     * <p>getFhirReturnTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<SelectItem> getFhirReturnTypes(){
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (FhirReturnType value: values()){
            selectItems.add(new SelectItem(value, value.getLabel()));
        }
        return selectItems;
    }
}
