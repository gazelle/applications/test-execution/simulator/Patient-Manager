package net.ihe.gazelle.simulator.pam.pds;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientHistory.ActionPerformed;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * <p>MergeManager class.</p>
 *
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 * @author Anne-Gaëlle Bergé > INRIA Rennes IHE development
 * @version $Id: $Id
 */

@Name("mergeManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class MergeManager extends PDS implements Serializable {
	private static final long serialVersionUID = 1L;


	/** {@inheritDoc} */
	@Override
	protected  void updateSelectedEvent(){
		this.canSendMessage = false;
		ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
		bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A40);
	}

	@Override
	protected List<TransactionInstance> send() {
		try {
			PatientHistory h = patientService.mergePatientVersion(secondaryPatient, selectedPatient);
			if (h.getOtherPatient()==null) {
				secondaryPatient = h.getCurrentPatient();
			} else {
				selectedPatient = h.getCurrentPatient();
				secondaryPatient = h.getOtherPatient();
			}
			List<TransactionInstance> transactions = patientService.sendADTMessage(
					new PatientService.Exchange(
							selectedSUT,
							sendingApplication,
							sendingFacility,
							sendingActor,
							receivingActor,
							simulatedTransaction,
							getHL7Domain(),
							PatientService.ADT.A40,
							selectedPatient,
							secondaryPatient
					));
			secondaryPatient = null;
			selectedPatient = null;
			return transactions;
		} catch (HL7Exception e) {
			throw new SendindException(e.getMessage(),e);
		}
	}


	/**
	 * <p>generateCorrectPatient.</p>
	 */
	public void generateCorrectPatient() {
		selectedPatient = generateAPatient();
	}

	/**
	 * <p>generateIncorrectPatient.</p>
	 */
	public void generateIncorrectPatient() {
		secondaryPatient = generateAPatient();
	}

	/** {@inheritDoc} */
	@Override
	public void setSelectedPatient(Patient selectedPatient) {
		super.setSelectedPatient(selectedPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Correct patient has been selected");
		this.canSendMessage = this.selectedPatient!=null&&secondaryPatient!=null;
	}

	/** {@inheritDoc} */
	@Override
	public void setSecondaryPatient(Patient selectedPatient) {
		super.setSecondaryPatient(selectedPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Incorrect patient has been selected");
		this.canSendMessage = this.selectedPatient!=null&&secondaryPatient!=null;
	}

	private Patient generateAPatient() {
		Patient newPatient = super.generateNewPatient(sendingActor);
		String username = null;
		if (Identity.instance().isLoggedIn()) {
			username = Identity.instance().getCredentials().getUsername();
		}
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if (newPatient.getPatientIdentifiers() != null) {
			List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(newPatient
					.getPatientIdentifiers());
			newPatient.setPatientIdentifiers(pidList);
		}
		newPatient = newPatient.savePatient(entityManager);
		PatientHistory history = new PatientHistory(newPatient, null, ActionPerformed.CREATED, null, username, null,
				null);
		history.save(entityManager);
		displayDDSPanel = true;
		return newPatient;
	}

	/** {@inheritDoc} */
	@Override
	public void actionOnSelectedPatient(Patient patient) {
		// Nothing to do on that page
	}

	/** {@inheritDoc} */
	@Override
	public void createNewRelationship() {
		// patients are not editable on that page
	}

	/** {@inheritDoc} */
	@Override
	public void saveRelationship() {
		// patients are not editable on that page
	}
}
