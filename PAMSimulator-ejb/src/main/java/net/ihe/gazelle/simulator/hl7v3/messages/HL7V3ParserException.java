package net.ihe.gazelle.simulator.hl7v3.messages;

/**
 * <p>HL7V3ParserException class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7V3ParserException extends Exception {

    private String errorLocation;

    /**
     * <p>Constructor for HL7V3ParserException.</p>
     *
     * @param xpath a {@link java.lang.String} object.
     * @param message a {@link java.lang.String} object.
     * @param cause a {@link java.lang.Exception} object.
     */
    public HL7V3ParserException(String xpath, String message, Exception cause){
        super(message, cause);
        this.errorLocation = xpath;
    }

    /**
     * <p>Constructor for HL7V3ParserException.</p>
     *
     * @param xpath a {@link java.lang.String} object.
     * @param message a {@link java.lang.String} object.
     */
    public HL7V3ParserException(String xpath, String message){
        super(message);
        this.errorLocation = xpath;
    }

    /**
     * <p>Getter for the field <code>errorLocation</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getErrorLocation() {
        return errorLocation;
    }

    /**
     * <p>Setter for the field <code>errorLocation</code>.</p>
     *
     * @param errorLocation a {@link java.lang.String} object.
     */
    public void setErrorLocation(String errorLocation) {
        this.errorLocation = errorLocation;
    }
}
