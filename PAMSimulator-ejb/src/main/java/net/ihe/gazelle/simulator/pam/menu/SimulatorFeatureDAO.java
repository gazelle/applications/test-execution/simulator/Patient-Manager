package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Name("simulatorFeatureDAO")
@Scope(ScopeType.STATELESS)
public class SimulatorFeatureDAO {

    /**
     * <p>isFeatureEnabled.</p>
     *
     * @param keyword a {@link net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations} object.
     * @return a boolean.
     */
    public static boolean isFeatureEnabled(PatientManagerAuthorizations keyword){
        SimulatorFeatureQuery query = new SimulatorFeatureQuery();
        query.keyword().eq(keyword);
        SimulatorFeature feature = query.getUniqueResult();
        return (feature != null && feature.isEnabled());
    }

    /**
     * <p>save.</p>
     *
     * @param inFeature a {@link net.ihe.gazelle.simulator.pam.menu.SimulatorFeature} object.
     */
    public static void save(SimulatorFeature inFeature) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(inFeature);
        entityManager.flush();
    }

    /**
     * <p>isEPDEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isEPDEnabled(){
        return isFeatureEnabled(PatientManagerAuthorizations.EPD);
    }
}
