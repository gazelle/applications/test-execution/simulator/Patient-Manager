package net.ihe.gazelle.simulator.xcpd.initiatinggw;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

/**
 * <b>Class Description : </b>XCPDRespondingGatewayGUIManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/01/16
 */
@Name("xcpdInitGatewayConfManager")
@Scope(ScopeType.PAGE)
public class XCPDInitiatingGatewayConfManager extends AbstractPDQPDSGuiManager implements Serializable {

    private String endpointUrl;
    private String deviceId;
    private String organizationOid;
    private String homeCommunityId;

    /**
     * <p>getUrlForHL7Messages.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrlForHL7Messages() {
        Actor actor = Actor.findActorWithKeyword("INIT_GW");
        return "/messages/browser.seam?simulatedActor=" + actor.getId() + "&responder=" + actor.getId();
    }

    /** {@inheritDoc} */
    @Override
    @Create
    public void getSimulatorResponderConfiguration() {
        this.endpointUrl = PreferenceService.getString("hl7v3_xcpd_initgw_url");
        this.deviceId = PreferenceService.getString("hl7v3_xcpd_initgw_device_id");
        this.organizationOid = PreferenceService.getString("hl7v3_organization_oid");
        this.homeCommunityId = PreferenceService.getString("hl7v3_xcpd_initgw_home_community_id");
    }

    /**
     * <p>Getter for the field <code>endpointUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEndpointUrl() {
        return endpointUrl;
    }

    /**
     * <p>Getter for the field <code>deviceId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * <p>Getter for the field <code>organizationOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrganizationOid() {
        return organizationOid;
    }

    /**
     * <p>Getter for the field <code>homeCommunityId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHomeCommunityId() {
        return homeCommunityId;
    }

    @Override
    public boolean displayLinkToPatients(){
        return false;
    }

}
