package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.simulator.pam.model.Patient;

public class IHEPatientBuilder extends AbstractPatientBuilder {
    @Override
    public Patient build(PatientBuilder.Context context) {
        return newPatient(context);
    }
}
