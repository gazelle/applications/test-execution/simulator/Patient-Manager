package net.ihe.gazelle.simulator.pam.automaton.Manager;

import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * Created by xfs on 29/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Name("graphmlListManager")
@Scope(ScopeType.PAGE)
public class GraphmlListManager implements Serializable {

    static Logger log = LoggerFactory.getLogger(GraphmlListManager.class);

    private List<GraphDescription> listGraphDescription;

    /**
     * <p>Getter for the field <code>listGraphDescription</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<GraphDescription> getListGraphDescription() {
        return listGraphDescription;
    }

    /**
     * <p>Setter for the field <code>listGraphDescription</code>.</p>
     *
     * @param listGraphDescription a {@link java.util.List} object.
     */
    public void setListGraphDescription(List<GraphDescription> listGraphDescription) {
        this.listGraphDescription = listGraphDescription;
    }

    /**
     * <p>runAutomaton.</p>
     *
     * @param graphDescription a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     * @return a {@link java.lang.String} object.
     */
    public String runAutomaton(GraphDescription graphDescription) {
        return "/automaton/pamAutomaton.seam?graphId=" + graphDescription.getId();
    }

    /**
     * <p>displayGraphml.</p>
     *
     * @param graphDescription a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     * @return a {@link java.lang.String} object.
     */
    public String displayGraphml(GraphDescription graphDescription) {
        return "/automaton/viewGraphml.seam?id=" + graphDescription.getId();
    }

    /**
     * <p>editGraphml.</p>
     *
     * @param graphDescription a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     * @return a {@link java.lang.String} object.
     */
    public String editGraphml(GraphDescription graphDescription) {
        return "/automaton/viewGraphml.seam?id=" + graphDescription.getId() + "&?editMode=true";
    }

    /**
     * <p>createGraphml.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createGraphml() {
        return "/automaton/createGraphml.seam";
    }

    /**
     * <p>init.</p>
     */
    public void init(){

        GraphDescriptionQuery gdq = new GraphDescriptionQuery();
        listGraphDescription = gdq.getList();
    }

    /**
     * <p>displayCurrentPage.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayCurrentPage() {
        return "/automaton/listGraphml.seam";
    }

}
