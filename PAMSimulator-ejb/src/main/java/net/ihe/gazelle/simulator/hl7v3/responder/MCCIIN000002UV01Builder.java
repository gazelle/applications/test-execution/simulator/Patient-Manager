package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Device;
import net.ihe.gazelle.hl7v3.mccimt000200UV01.*;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Device;
import net.ihe.gazelle.hl7v3.voc.CommunicationFunctionType;
import net.ihe.gazelle.hl7v3.voc.EntityClassDevice;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.pdqv3.pdc.PDQv3MessageBuilder;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by aberge on 12/03/15.
 */
public class MCCIIN000002UV01Builder extends HL7v3MessageBuilder {


	private static Logger log = LoggerFactory.getLogger(MCCIIN000002UV01Builder.class);

	public static MCCIIN000002UV01Type buildAcceptAcknowledgement(String sutDeviceOid, II messageId,
																  Object senderDevice, String ackCodeValue) {
		MCCIIN000002UV01Type response = new MCCIIN000002UV01Type();
		SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
		response.setITSVersion(PDQv3MessageBuilder.VERSION);
		response.addReceiver(buildReceiverForMCCIIN00002UV1(sutDeviceOid));
		response.setCreationTime(new TS(sdfDateTime.format(new Date())));
		response.setInteractionId(new II(INTERACTION_ID_NAMESPACE, PatientManagerConstants.MCCI_IN_000002_UV_01));
		response.setProcessingCode(new CS("P", null, null));
		response.setProcessingModeCode(new CS("T", null, null));
		response.setAcceptAckCode(new CS("NE", null, null));
		MessageIdGenerator generator = MessageIdGenerator.getGeneratorForActor(PatientManagerConstants.PDS);
		if (generator != null) {
			response.setId(new II(generator.getOidForMessage(), generator.getNextId()));
		} else {
			log.error("No MessageIdGenerator instance found for actor PDS");
		}
		response.setSender(buildSenderForMCCIIN00002UV01(senderDevice));
		response.addAcknowledgement(populateAcknowledgementForMCCIIN00002UV01(messageId, ackCodeValue));
		return response;
	}

	private static MCCIMT000200UV01Acknowledgement populateAcknowledgementForMCCIIN00002UV01(II incomingMessageId,
																							 String typeCode) {
		MCCIMT000200UV01Acknowledgement acknowledgement = new MCCIMT000200UV01Acknowledgement();
		acknowledgement.setTypeCode(new CS(typeCode, null, null));
		acknowledgement.setTargetMessage(populateTargetMessageForMCCIIN00002UV01(incomingMessageId));
		return acknowledgement;
	}

	private static MCCIMT000200UV01TargetMessage populateTargetMessageForMCCIIN00002UV01(II targetMessageId) {
		MCCIMT000200UV01TargetMessage targetMessage = new MCCIMT000200UV01TargetMessage();
		targetMessage.setId(new II(targetMessageId.getRoot(), targetMessageId.getExtension()));
		return targetMessage;
	}

	private static MCCIMT000200UV01Sender buildSenderForMCCIIN00002UV01(Object device) {
		MCCIMT000200UV01Sender sender = new MCCIMT000200UV01Sender();
		sender.setTypeCode(CommunicationFunctionType.SND);
		MCCIMT000200UV01Device senderDevice = buildDeviceForMCCIIN00002UV01();
		if (device instanceof MCCIMT000100UV01Device) {
			MCCIMT000100UV01Device mccimt000100UV01Device = (MCCIMT000100UV01Device) device;
			senderDevice.setId(mccimt000100UV01Device.getId());
			senderDevice.setTelecom(mccimt000100UV01Device.getTelecom());
		} else if (device instanceof MCCIMT000300UV01Device){
			MCCIMT000300UV01Device mccimt000300UV01Device = (MCCIMT000300UV01Device) device;
			senderDevice.setId(mccimt000300UV01Device.getId());
			senderDevice.setTelecom(mccimt000300UV01Device.getTelecom());
		}
		sender.setDevice(senderDevice);
		return sender;
	}

	private static MCCIMT000200UV01Device buildDeviceForMCCIIN00002UV01() {
		MCCIMT000200UV01Device device = new MCCIMT000200UV01Device();
		device.setClassCode(EntityClassDevice.DEV);
		device.setDeterminerCode(EntityDeterminer.INSTANCE);
		return device;
	}

	private static MCCIMT000200UV01Device buildDeviceForMCCIIN00002UV01(String deviceOID, String value) {
		MCCIMT000200UV01Device device = buildDeviceForMCCIIN00002UV01();
		device.addId(new II(deviceOID, null));
		if (value != null && !value.isEmpty()) {
			device.addTelecom(populateTelecom(value));
		}
		return device;
	}

	private static MCCIMT000200UV01Receiver buildReceiverForMCCIIN00002UV1(String deviceOid) {
		MCCIMT000200UV01Receiver receiver = new MCCIMT000200UV01Receiver();
		receiver.setTypeCode(CommunicationFunctionType.RCV);
		receiver.setDevice(buildDeviceForMCCIIN00002UV01(deviceOid, null));
		return receiver;
	}
}
