package net.ihe.gazelle.simulator.pix.xmgr;

import net.ihe.gazelle.HL7Common.action.SimulatorResponderConfigurationDisplay;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PIXManagerConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixManagerConfiguration")
@Scope(ScopeType.PAGE)
public class PIXManagerConfiguration extends SimulatorResponderConfigurationDisplay {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	@Override
    public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
        return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&domain=" + conf.getDomain().getId();
    }

	/** {@inheritDoc} */
	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
		getSimulatorResponderConfigurations(Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR"), null, null);
	}
}
