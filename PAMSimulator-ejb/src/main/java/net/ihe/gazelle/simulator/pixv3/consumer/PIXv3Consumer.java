package net.ihe.gazelle.simulator.pixv3.consumer;

import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.messages.PRPAIN201310UV02Parser;
import net.ihe.gazelle.simulator.hl7v3.sut.action.HL7v3ResponderSUTConfigurationDAO;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.pix.consumer.AbstractPIXQueryManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("pixv3consumer")
@Scope(ScopeType.PAGE)
public class PIXv3Consumer extends AbstractPIXQueryManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8745566905500266147L;
	private static final String ITI45_KEYWORD = "ITI-45";

	private static Logger log = LoggerFactory.getLogger(PIXv3Consumer.class);

	private Patient receivedPatient;
	private String senderDeviceId;
	private String organizationOid;
	private List<HL7v3ResponderSUTConfiguration> availableSystems;


	public String getSenderDeviceId() {
		if (senderDeviceId == null) {
			senderDeviceId = PreferenceService.getString("hl7v3_pix_consumer_device_id");
		}
		return senderDeviceId;
	}

	public String getOrganizationOid() {
		if (organizationOid == null) {
			organizationOid = PreferenceService.getString("hl7v3_pix_organization_oid");
		}
		return organizationOid;
	}

	public List<HL7v3ResponderSUTConfiguration> getAvailableSystems() {
		if (availableSystems == null){
			availableSystems = HL7v3ResponderSUTConfigurationDAO.getSUTForTransactionDomain(ITI45_KEYWORD, "IHE");
		}
		return availableSystems;
	}

	public List<PatientIdentifier> getReceivedIdentifiers() {
		return receivedIdentifiers;
	}

	@Create
	public void initialize() {
		currentDomainReturned = new AssigningAuthority();
		searchedPid = new PatientIdentifier();
		searchedPid.setDomain(new HierarchicDesignator());
		domainsReturned = new ArrayList<AssigningAuthority>();
		receivedIdentifiers = new ArrayList<PatientIdentifier>();
	}

	public void sendMessage() {
		if (selectedSUT != null) {

			PIXv3MessageBuilder messageBuilder = new PIXv3MessageBuilder(searchedPid, (HL7v3ResponderSUTConfiguration)selectedSUT, domainsReturned);
			PRPAIN201309UV02Type request = messageBuilder.buildPatientRegistryGetIdentifierQuery();
			PIXv3ConsumerSender sender = new PIXv3ConsumerSender((HL7v3ResponderSUTConfiguration) selectedSUT);
			try {
				sender.sendPatientRegistryGetIdentifierQuery(request);
			} catch (SoapSendException e){
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
				return;
			}
			hl7Messages = new ArrayList<TransactionInstance>();
			hl7Messages.add(sender.getTransactionInstance());
			// at the end
			try {
				parseResponse(sender.getTransactionInstance().getResponse().getContent(), sender
						.getTransactionInstance().getResponse().getType(), messageBuilder);
			} catch (NullPointerException e) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the supplier");
			} catch (JAXBException e) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to parse the response");
				log.error(e.getMessage(), e);
			}
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN,
					"Please, first select the system under test to which you want to send the message");
		}
	}

	/**
	 * <p>addDomainToList.</p>
	 */
	@Override
	public void addDomainToList() {
		if (currentDomainReturned.getUniversalId() != null && !currentDomainReturned.getUniversalId().isEmpty()) {
			domainsReturned.add(currentDomainReturned);
			currentDomainReturned = new AssigningAuthority();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "The assigning authority OID shall not be null");
		}
	}


	private void parseResponse(byte[] response, String responseType, PIXv3MessageBuilder messageBuilder) throws JAXBException {
		if (response != null) {
			if (responseType.equals("PRPA_IN201310UV02")) {
				PRPAIN201310UV02Parser parser = new PRPAIN201310UV02Parser(FacesMessages.instance());
				Patient patient = parser.extractPatient(response);
				messageBuilder.setTargetMessageId(parser.getMessageId());
				if (patient != null) {
					receivedPatient = patient;
					receivedIdentifiers = patient.getPatientIdentifiers();
				}
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Received " + responseType + ": cannot be parsed");
			}
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the supplier");
		}
	}

	@Override
	protected void resetPage() {

	}

	@Override
	public Transaction getTestedTransaction() {
		return Transaction.GetTransactionByKeyword(ITI45_KEYWORD);
	}

	public Patient getReceivedPatient() {
		return receivedPatient;
	}
}
