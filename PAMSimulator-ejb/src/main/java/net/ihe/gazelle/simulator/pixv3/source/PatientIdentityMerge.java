package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 16/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("patientIdentityMergeBean")
@Scope(ScopeType.PAGE)
public class PatientIdentityMerge extends PatientIdentitySource{

	/** {@inheritDoc} */
	@Override public void sendMessage() {
		ITI44ManagerLocal manager = (ITI44ManagerLocal) Component.getInstance("iti44Manager");
		HL7v3ResponderSUTConfiguration sut = manager.getSelectedSystem();
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"Please, first select a system under test and retry");
			return;
		}
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		PatientHistory history = new PatientHistory(secondaryPatient, selectedPatient, PatientHistory.ActionPerformed.MERGE, null,
				null, null, null);
		history.save(entityManager);
		history = new PatientHistory(selectedPatient, null, PatientHistory.ActionPerformed.SENT, "PRPA_IN201304UV02", null,
				sut.getOrganizationOID(), sut.getDeviceOID());
		history.save(entityManager);
		secondaryPatient.setStillActive(false);
		secondaryPatient = secondaryPatient.savePatient(entityManager);
		ITI44MessageBuilder builder = new ITI44MessageBuilder(sut, selectedPatient, secondaryPatient);
		PRPAIN201304UV02Type message = builder.buildPatientIdentityMergeMessage();
		PatientIdentitySourceSender sender = new PatientIdentitySourceSender(sut);
		try {
			sender.sendMergePatientIdentity(message);
		} catch (SoapSendException e){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
			return;
		}
		messages = new ArrayList<TransactionInstance>();
		messages.add(sender.getTransactionInstance());
		displayDDSPanel = false;
		selectedPatient = null;
		secondaryPatient = null;
		displayPatientsList = false;

	}

	/** {@inheritDoc} */
	@Override public void actionOnSelectedPatient(Patient patient) {
		// patients are dropped, this method is not used in this case
	}

	/**
	 * <p>generateCorrectPatient.</p>
	 */
	public void generateCorrectPatient() {
		selectedPatient = generateAPatient();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "The newly generated patient has been set as correct patient");
	}

	/**
	 * <p>generateIncorrectPatient.</p>
	 */
	public void generateIncorrectPatient() {
		secondaryPatient = generateAPatient();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "The newly generated patient has been set as incorrect patient");
	}

	private Patient generateAPatient() {
		Patient newPatient = super.generateNewPatient(sendingActor);
		String username = null;
		if (Identity.instance().isLoggedIn()) {
			username = Identity.instance().getCredentials().getUsername();
		}
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if (newPatient.getPatientIdentifiers() != null) {
			List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(newPatient
					.getPatientIdentifiers());
			newPatient.setPatientIdentifiers(pidList);
		}
		newPatient = newPatient.savePatient(entityManager);
		PatientHistory history = new PatientHistory(newPatient, null, PatientHistory.ActionPerformed.CREATED, null, username, null,
				null);
		history.save(entityManager);
		displayDDSPanel = true;
		return newPatient;
	}

	/** {@inheritDoc} */
	@Override
	public void createNewRelationship() {
		// TODO
	}

	/** {@inheritDoc} */
	@Override
	public void saveRelationship() {
		// TODO
	}

	/**
	 * <p>setCorrectPatient.</p>
	 *
	 * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public void setCorrectPatient(Patient inPatient){
		super.setSelectedPatient(inPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "The selected patient will be used as correct patient");
	}

	/**
	 * <p>setIncorrectPatient.</p>
	 *
	 * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public void setIncorrectPatient(Patient inPatient){
		super.setSecondaryPatient(inPatient);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "The selected patient will be used as incorrect patient");
	}
}
