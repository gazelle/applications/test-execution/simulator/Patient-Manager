package net.ihe.gazelle.simulator.pixv3.consumer;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.hl7v3.responder.PIXV3QueryHandler;
import net.ihe.gazelle.simulator.pixv3.manager.PIXManagerService;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.Addressing;

/**
 * Created by aberge on 23/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Stateless
@Name("PIXConsumerService")
@WebService(portName = "PIXConsumer_Port_Soap12", name = "PIXConsumer_PortType", targetNamespace = "urn:ihe:iti:pixv3:2007", serviceName = "PIXConsumer_Service")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@Addressing(enabled = true, required = true)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@HandlerChain(file = "soap-handler.xml")
@GenerateInterface(value = "PIXConsumerServiceRemote", isLocal = false, isRemote = true)
public class PIXConsumerService implements  PIXConsumerServiceRemote{

	private static Logger log = LoggerFactory.getLogger(PIXManagerService.class);
	private static final String HL7_NS = "urn:hl7-org:v3";

	@Resource
	private WebServiceContext context;

	private HttpServletRequest getServletRequest() {
		return HL7V3Utils.getServletRequestFromContext(context);
	}

	@WebMethod(operationName = "PIXConsumer_PRPA_IN201302UV02", action = "urn:hl7-org:v3:PRPA_IN201302UV02")
	@WebResult(name = "MCCI_IN000002UV01", partName = "Body", targetNamespace = HL7_NS)
	@Action(input = "urn:hl7-org:v3:PRPA_IN201302UV02", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
	public MCCIIN000002UV01Type updateNotification(
			@WebParam(name = "PRPA_IN201302UV02", partName = "Body", targetNamespace = HL7_NS) PRPAIN201302UV02Type request) {
    	Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		PIXV3QueryHandler queryHandler = new PIXV3QueryHandler(domain,
				Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER"), Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR"),
				Transaction.GetTransactionByKeyword("ITI-46"), getServletRequest(), context.getMessageContext());
		try {
			return queryHandler.generateDefaultAcknowledgement(request);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

}
