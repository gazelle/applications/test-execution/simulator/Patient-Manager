package net.ihe.gazelle.simulator.pdq.util;

import java.io.Serializable;


/**
 * <p>AssigningAuthority class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class AssigningAuthority implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -62074734632030680L;
	private static final String URN_PREFIX = "urn:oid:";
	String namespace;
	String universalId;
	String universalIdType;

	/**
	 * <p>Constructor for AssigningAuthority.</p>
	 */
	public AssigningAuthority() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>Getter for the field <code>namespace</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * <p>Setter for the field <code>namespace</code>.</p>
	 *
	 * @param namespace a {@link java.lang.String} object.
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * <p>Getter for the field <code>universalId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUniversalId() {
		return universalId;
	}

	/**
	 * <p>Setter for the field <code>universalId</code>.</p>
	 *
	 * @param universalId a {@link java.lang.String} object.
	 */
	public void setUniversalId(String universalId) {
		this.universalId = universalId;
	}

	/**
	 * <p>Getter for the field <code>universalIdType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUniversalIdType() {
		return universalIdType;
	}

	/**
	 * <p>Setter for the field <code>universalIdType</code>.</p>
	 *
	 * @param universalIdType a {@link java.lang.String} object.
	 */
	public void setUniversalIdType(String universalIdType) {
		this.universalIdType = universalIdType;
	}

	/**
	 * <p>isEmpty.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isEmpty() {
		if ((namespace == null || namespace.isEmpty()) && (universalId == null || universalId.isEmpty())
				&& (universalIdType == null || universalIdType.isEmpty())) {
			return true;
		} else {
			return false;
		}
	}

	public String getUniversalIDAsUrn(){
		if (this.universalId == null){
			return null;
		} else if (universalId.startsWith(URN_PREFIX)){
			return universalId;
		} else {
			return URN_PREFIX + universalId;
		}
	}

}
