package net.ihe.gazelle.simulator.pam.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.pam.filter.MovementFilter;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;

import java.util.Date;

/**
 * <p>MovementDataModel class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MovementDataModel extends FilterDataModel<Movement> {

	private Encounter encounter;
	private Actor simulatedActor;
	private String status;
	private String movementUniqueId;
	private String triggerEvent;
	private Boolean isCurrentMovement;
	private Boolean isPendingMovement;
	private String creator;
	private Date startDate;
	private Date endDate;

	/**
	 * <p>Constructor for MovementDataModel.</p>
	 *
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param status a {@link java.lang.String} object.
	 * @param movementUniqueId a {@link java.lang.String} object.
	 * @param triggerEvent a {@link java.lang.String} object.
	 * @param isCurrentMovement a {@link java.lang.Boolean} object.
	 * @param isPendingMovement a {@link java.lang.Boolean} object.
	 * @param creator a {@link java.lang.String} object.
	 * @param startDate a {@link java.util.Date} object.
	 * @param endDate a {@link java.util.Date} object.
	 */
	public MovementDataModel(Encounter encounter, Actor simulatedActor, String status, String movementUniqueId,
			String triggerEvent, Boolean isCurrentMovement, Boolean isPendingMovement, String creator, Date startDate,
			Date endDate) {
		super(new MovementFilter());
		this.encounter = encounter;
		this.simulatedActor = simulatedActor;
		this.status = status;
		this.movementUniqueId = movementUniqueId;
		this.isCurrentMovement = isCurrentMovement;
		this.isPendingMovement = isPendingMovement;
		this.triggerEvent = triggerEvent;
		this.creator = creator;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/** {@inheritDoc} */
	@Override
	public void appendFiltersFields(HQLQueryBuilder<Movement> queryBuilder) {
		if (encounter != null) {
			queryBuilder.addRestriction(HQLRestrictions.eq("encounter", encounter));
		}
		if (simulatedActor != null) {
			queryBuilder.addRestriction(HQLRestrictions.eq("simulatedActor", simulatedActor));
		}
		if ((status != null) && !status.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("status", status, HQLRestrictionLikeMatchMode.EXACT));
		}
		if ((movementUniqueId != null) && !movementUniqueId.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("movementUniqueId", movementUniqueId,
					HQLRestrictionLikeMatchMode.EXACT));
		}
		if (isCurrentMovement != null) {
			queryBuilder.addRestriction(HQLRestrictions.eq("currentMovement", isCurrentMovement));
		}
		if (isPendingMovement != null) {
			queryBuilder.addRestriction(HQLRestrictions.eq("pendingMovement", isPendingMovement));
		}
		if ((triggerEvent != null) && !triggerEvent.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("triggerEvent", triggerEvent,
					HQLRestrictionLikeMatchMode.EXACT));
		}
		if ((creator != null) && !creator.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("creator", creator, HQLRestrictionLikeMatchMode.EXACT));
		}
		if (endDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.lt("creationDate", endDate));
		}

		if (startDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.gt("creationDate", startDate));
		}
	}

	/**
	 * <p>Getter for the field <code>encounter</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	/**
	 * <p>Setter for the field <code>encounter</code>.</p>
	 *
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * <p>Getter for the field <code>simulatedActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	/**
	 * <p>Setter for the field <code>simulatedActor</code>.</p>
	 *
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * <p>Getter for the field <code>status</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * <p>Setter for the field <code>status</code>.</p>
	 *
	 * @param status a {@link java.lang.String} object.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * <p>Getter for the field <code>movementUniqueId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMovementUniqueId() {
		return movementUniqueId;
	}

	/**
	 * <p>Setter for the field <code>movementUniqueId</code>.</p>
	 *
	 * @param movementUniqueId a {@link java.lang.String} object.
	 */
	public void setMovementUniqueId(String movementUniqueId) {
		this.movementUniqueId = movementUniqueId;
	}

	/**
	 * <p>Getter for the field <code>triggerEvent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTriggerEvent() {
		return triggerEvent;
	}

	/**
	 * <p>Setter for the field <code>triggerEvent</code>.</p>
	 *
	 * @param triggerEvent a {@link java.lang.String} object.
	 */
	public void setTriggerEvent(String triggerEvent) {
		this.triggerEvent = triggerEvent;
	}

	/**
	 * <p>Getter for the field <code>isCurrentMovement</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsCurrentMovement() {
		return isCurrentMovement;
	}

	/**
	 * <p>Setter for the field <code>isCurrentMovement</code>.</p>
	 *
	 * @param isCurrentMovement a {@link java.lang.Boolean} object.
	 */
	public void setIsCurrentMovement(Boolean isCurrentMovement) {
		this.isCurrentMovement = isCurrentMovement;
	}

	/**
	 * <p>Getter for the field <code>isPendingMovement</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsPendingMovement() {
		return isPendingMovement;
	}

	/**
	 * <p>Setter for the field <code>isPendingMovement</code>.</p>
	 *
	 * @param isPendingMovement a {@link java.lang.Boolean} object.
	 */
	public void setIsPendingMovement(Boolean isPendingMovement) {
		this.isPendingMovement = isPendingMovement;
	}

	/**
	 * <p>Setter for the field <code>creator</code>.</p>
	 *
	 * @param creator a {@link java.lang.String} object.
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * <p>Getter for the field <code>creator</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * <p>Setter for the field <code>startDate</code>.</p>
	 *
	 * @param startDate a {@link java.util.Date} object.
	 */
	public void setStartDate(Date startDate) {
		if (startDate != null) {
			this.startDate = (Date) startDate.clone();
		} else {
			this.startDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>startDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * <p>Setter for the field <code>endDate</code>.</p>
	 *
	 * @param endDate a {@link java.util.Date} object.
	 */
	public void setEndDate(Date endDate) {
		if (endDate != null) {
			this.endDate = (Date) endDate.clone();
		} else {
			this.endDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>endDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getEndDate() {
		return endDate;
	}

/** {@inheritDoc} */
@Override
    protected Object getId(Movement t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
