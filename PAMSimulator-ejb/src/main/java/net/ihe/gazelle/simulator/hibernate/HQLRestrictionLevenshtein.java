package net.ihe.gazelle.simulator.hibernate;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;

/**
 * This class creates a criteria to compare the levenshtein distance of two strings to the given max distance
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HQLRestrictionLevenshtein implements HQLRestriction {

	private String pathToString_1;
	private String pathToString_2;
	private int maxDistance;

	/**
	 * <p>Constructor for HQLRestrictionLevenshtein.</p>
	 *
	 * @param path
	 *            : path to the string to compare
	 * @param dmetaphone
	 *            : dmetaphone to compare
	 * @param maxDistance
	 *            : maximum distance
	 */
	public HQLRestrictionLevenshtein(String path, String dmetaphone, int maxDistance) {
		this.pathToString_1 = path;
		this.pathToString_2 = dmetaphone;
		this.maxDistance = maxDistance;
	}

	/** {@inheritDoc} */
	@Override
	public void toHQL(HQLQueryBuilder<?> queryBuilder, HQLRestrictionValues values, StringBuilder sb) {
		if ((pathToString_1 != null) && (pathToString_2 != null)) {
			sb.append("levenshtein(");
			sb.append(queryBuilder.getShortProperty(pathToString_1));
			sb.append(", :");
			sb.append(values.addValue(pathToString_1, pathToString_2));
			sb.append(") < ");
			sb.append(maxDistance);
		}
	}

}
