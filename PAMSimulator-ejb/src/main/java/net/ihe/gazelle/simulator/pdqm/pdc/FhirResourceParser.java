package net.ihe.gazelle.simulator.pdqm.pdc;

import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.hl7v3.voc.AdministrativeGender;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PersonDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>PDQmResponseParser class.</p>
 *
 * @author aberge
 * @version 1.0: 03/11/17
 */

public class FhirResourceParser {

    private static final Logger LOG = LoggerFactory.getLogger(FhirResourceParser.class);

    private static ArrayList<org.hl7.fhir.dstu3.model.Patient> getFhirPatientsFromBundle(Bundle response) {

        ArrayList<org.hl7.fhir.dstu3.model.Patient> patientList = new ArrayList<org.hl7.fhir.dstu3.model.Patient>();
        for (Bundle.BundleEntryComponent currentPatient : response.getEntry()) {
            patientList.add((org.hl7.fhir.dstu3.model.Patient) currentPatient.getResource());
        }
        return patientList;
    }

    public static List<Patient> getPatientsFromBundle(Bundle response) {
        List<org.hl7.fhir.dstu3.model.Patient> fhirPatients = getFhirPatientsFromBundle(response);

        List<Patient> dbPatientList = new ArrayList<Patient>();
        for (org.hl7.fhir.dstu3.model.Patient currentFhirPatient : fhirPatients) {

            Patient currentDbPatient = extractSinglePatient(currentFhirPatient);

            dbPatientList.add(currentDbPatient);

        }

        return dbPatientList;

    }

    public static Patient extractSinglePatient(org.hl7.fhir.dstu3.model.Patient currentFhirPatient) {
        Patient currentDbPatient = new Patient();
        List<HumanName> fhirPatientName = currentFhirPatient.getName();
        boolean officialNameSet = false;
        for (HumanName name : fhirPatientName){
            if (name.getUse().toCode().equals("official")){
                List<StringType> givenNames = name.getGiven();
                if (givenNames.size() == 1){
                    currentDbPatient.setFirstName(givenNames.get(0).toString());
                } else if (givenNames.size() ==2){
                    currentDbPatient.setFirstName(givenNames.get(0).toString());
                    currentDbPatient.setSecondName(givenNames.get(1).toString());
                } else if (givenNames.size()>=3){
                    currentDbPatient.setFirstName(givenNames.get(0).toString());
                    currentDbPatient.setSecondName(givenNames.get(1).toString());
                    currentDbPatient.setThirdName(givenNames.get(2).toString());
                }
                currentDbPatient.setLastName(name.getFamily());
                officialNameSet = true;
            } else if (name.getUse().toCode().equals("usual")){
                List<StringType> givenNames = name.getGiven();
                if (givenNames.size() == 1){
                    currentDbPatient.setAlternateFirstName(givenNames.get(0).toString());
                } else if (givenNames.size() ==2){
                    currentDbPatient.setAlternateFirstName(givenNames.get(0).toString());
                    currentDbPatient.setAlternateSecondName(givenNames.get(1).toString());
                } else if (givenNames.size()>=3){
                    currentDbPatient.setAlternateFirstName(givenNames.get(0).toString());
                    currentDbPatient.setAlternateSecondName(givenNames.get(1).toString());
                    currentDbPatient.setAlternateThirdName(givenNames.get(2).toString());
                }
            }
        }
        if (!officialNameSet){
            currentDbPatient.setFirstName(currentFhirPatient.getNameFirstRep().getGivenAsSingleString());
            currentDbPatient.setLastName(currentFhirPatient.getNameFirstRep().getFamily());
        }

        currentDbPatient.setDateOfBirth(currentFhirPatient.getBirthDate());
        currentDbPatient.setGenderCode(convertGenderToFhirFormat(currentFhirPatient.getGender()));
        boolean mainAddress = true; // take the first address in the list as the main one
        for (Address address : currentFhirPatient.getAddress()) {
            PatientAddress patientAddress = new PatientAddress();
            patientAddress.setMainAddress(mainAddress);
            if (mainAddress) {
                mainAddress = false;
            }
            if (!address.getLine().isEmpty()) {
                patientAddress.setAddressLine(address.getLine().get(0).getValue());
            } else if (address.getText() != null) {
                patientAddress.setAddressLine(address.getText());
            }
            patientAddress.setZipCode(address.getPostalCode());
            patientAddress.setCity(address.getCity());
            patientAddress.setState(address.getState());
            patientAddress.setCountryCode(address.getCountry());
            patientAddress.setPatient(currentDbPatient);
            patientAddress.setAddressType(getAddressType(address.getUse()));
            currentDbPatient.addPatientAddress(patientAddress);
        }
        currentDbPatient.setCountryCode(currentFhirPatient.getAddressFirstRep().getCountry());
        // read extensions from resource to get mother's maiden name
        List<Extension> extensions = currentFhirPatient.getExtensionsByUrl(FhirConstants.MOTHERS_MAIDEN_NAME_EXTENSION_URL);
        if (extensions != null && !extensions.isEmpty()) {
            Extension mothersMaidenNameExt = extensions.get(0); // there shall be only one entry
            if (mothersMaidenNameExt.getValue() instanceof HumanName) {
                HumanName mothersMaidenName = (HumanName) mothersMaidenNameExt.getValue();
                String family = mothersMaidenName.getFamily();
                if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
                    Person mother = PersonDAO.newMother(family, currentDbPatient);
                    mother.setFirstName(mothersMaidenName.getGivenAsSingleString());
                    currentDbPatient.addPersonalRelationship(mother);
                } else {
                    currentDbPatient.setMotherMaidenName(family);
                }

            }
        }
        for (ContactPoint contactPoint : currentFhirPatient.getTelecom()) {
            String value = contactPoint.getValue();
            if (value != null) {
                switch (contactPoint.getSystem()) {
                    case EMAIL:
                        currentDbPatient.setEmail(value);
                        break;
                    case SMS:
                    case PHONE:
                        PatientPhoneNumber phoneNumber = new PatientPhoneNumber();
                        setPhoneNumberType(phoneNumber, contactPoint.getUse());
                        phoneNumber.setValue(value);
                        phoneNumber.setPatient(currentDbPatient);
                        currentDbPatient.addPhoneNumber(phoneNumber);
                        break;
                    case PAGER:
                        PatientPhoneNumber pagerNumber = new PatientPhoneNumber();
                        pagerNumber.setType(PhoneNumberType.BEEPER);
                        pagerNumber.setValue(value);
                        pagerNumber.setPatient(currentDbPatient);
                        currentDbPatient.addPhoneNumber(pagerNumber);
                        break;
                    default:
                        LOG.info(contactPoint.getSystem().getDisplay() + " is not supported");
                        break;
                }
            }
        }
        // for deceased patients
        if (currentFhirPatient.hasDeceasedBooleanType()) {
            try {
                currentDbPatient.setPatientDead(currentFhirPatient.getDeceasedBooleanType().booleanValue());
            } catch (FHIRException e) {
                LOG.warn("Cannot decode deceased as boolean: " + e.getMessage());
            }
        }
        if (currentFhirPatient.hasDeceasedDateTimeType()) {
            try {
                currentDbPatient.setPatientDead(true);
                currentDbPatient.setPatientDeathTime(currentFhirPatient.getDeceasedDateTimeType().getValue());
            } catch (FHIRException e) {
                LOG.warn("Cannot decode deceased as date: " + e.getMessage());
            }
        }
        List<PatientIdentifier> patientIdentifierList = getIdentifierToDbFormat(currentFhirPatient.getIdentifier());
        currentDbPatient.setPatientIdentifiers(patientIdentifierList);
        return currentDbPatient;
    }

    private static void setPhoneNumberType(PatientPhoneNumber phoneNumber, ContactPoint.ContactPointUse use) {
        switch (use) {
            case HOME:
                phoneNumber.setType(PhoneNumberType.HOME);
                break;
            case WORK:
                phoneNumber.setType(PhoneNumberType.WORK);
                break;
            case MOBILE:
                phoneNumber.setType(PhoneNumberType.MOBILE);
                break;
            case TEMP:
                phoneNumber.setType(PhoneNumberType.VACATION);
                break;
            default:
                phoneNumber.setType(PhoneNumberType.OTHER);
        }
    }

    private static String convertGenderToFhirFormat(Enumerations.AdministrativeGender gender) {
        switch (gender) {
            case FEMALE:
                return AdministrativeGender.F.value();
            case MALE:
                return AdministrativeGender.M.value();
            default:
                return "U";
        }
    }

    private static List<PatientIdentifier> getIdentifierToDbFormat(List<Identifier> currentFhirIdentifierList) {

        ArrayList<PatientIdentifier> patientIdentifierList = new ArrayList<PatientIdentifier>();
        for (Identifier currentFhirIdentifier : currentFhirIdentifierList) {
            PatientIdentifier currentPatientIdentifier = new PatientIdentifier();
            currentPatientIdentifier.setIdNumber(currentFhirIdentifier.getValue());
            String universalId = currentFhirIdentifier.getSystem();
            universalId = universalId.replace("urn:oid:", "");
            HierarchicDesignator domain = HierarchicDesignatorDAO.getAssigningAuthority(null, universalId, DesignatorType.PATIENT_ID);
            if (domain == null) {
                domain = new HierarchicDesignator(DesignatorType.PATIENT_ID);
                domain.setUniversalID(universalId);
            }
            currentPatientIdentifier.setDomain(domain);
            currentPatientIdentifier.setFullPatientIdentifierIfEmpty();
            patientIdentifierList.add(currentPatientIdentifier);
        }

        return patientIdentifierList;

    }

    private static AddressType getAddressType(Address.AddressUse use) {
        if (use != null) {
            switch (use) {
                case HOME:
                    return AddressType.HOME;
                case WORK:
                    return AddressType.BUSINESS;
                case TEMP:
                    return AddressType.CURRENT;
                case OLD:
                    return AddressType.BAD_ADDRESS;
                default:
                    return AddressType.HOME;
            }
        }
        return null;
    }

}
