package net.ihe.gazelle.simulator.pam.pec;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;

/**
 * <p>Server class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pecServer")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
public class Server extends AbstractServer<PEC> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7668933581911952357L;

	/** {@inheritDoc} */
	@Override
    @Create
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("PEC", entityManager);
		handler = new PEC();
		startServers(simulatedActor, null, null);

	}
}
