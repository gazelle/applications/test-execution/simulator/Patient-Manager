package net.ihe.gazelle.simulator.pam.hl7;

public interface HL7Domain<T extends AbstractHL7MessageGenerator> {
    T getFrameGenerator();

    String getDomainKeyword();
}
