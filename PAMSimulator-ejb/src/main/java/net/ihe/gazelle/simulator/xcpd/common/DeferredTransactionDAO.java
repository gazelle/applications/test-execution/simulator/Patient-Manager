package net.ihe.gazelle.simulator.xcpd.common;

import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransaction;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class DeferredTransactionDAO {

    /**
     * <p>buildAndSaveDeferredTransaction.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @param ackCode a {@link java.lang.String} object.
     */
    public static void buildAndSaveDeferredTransaction(PRPAIN201305UV02Type request, TransactionInstance instance, String ackCode) {
        DeferredTransaction deferredTransaction = new DeferredTransaction(Actor.findActorWithKeyword("INIT_GW"),
                instance, PreferenceService.getString("hl7v3_xcpd_initgw_url"));
        deferredTransaction.setMessageIdExtension(request.getId().getExtension());
        deferredTransaction.setMessageIdRoot(request.getId().getRoot());
        deferredTransaction.setStatusFromAckCode(ackCode);
        deferredTransaction.save();
    }
}
