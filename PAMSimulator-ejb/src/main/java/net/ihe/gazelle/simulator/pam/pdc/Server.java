package net.ihe.gazelle.simulator.pam.pdc;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DependsOn;

@Name("pdcServer")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
public class Server extends AbstractServer<PDC> {

    private static Logger log = LoggerFactory.getLogger(Server.class);
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Create
	@Override
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("PDC", entityManager);
		handler = new PDC();
		startServers(simulatedActor, null, null);
	}

}
