package net.ihe.gazelle.simulator.pam.iti31.model;

import net.ihe.gazelle.simulator.common.model.ValueSet;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class Description : </b>EncounterAdditionalInfo<br>
 * <br>
 *  This class gathers the attributes used to populate the ZFV segment described in ITI Volume4, section 4 (French extension)
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */
@SuppressWarnings("JavadocReference")
@Entity
@Name("encounterAdditionalInfo")
@Table(name = "pam_fr_encounter_additional_info", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_fr_encounter_additional_info_sequence", sequenceName = "pam_fr_encounter_additional_info_id_seq", allocationSize = 1)
public class EncounterAdditionalInfo implements Serializable{

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "pam_fr_encounter_additional_info_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "establishment_of_origin")
    private String establishmentOfOrigin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_visit_date")
    private Date lastVisitDate;

    @Column(name = "transport_mode")
    private String transportMode;

    @Column(name="placement_start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date placementStartDate;

    @Column(name="placement_end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date placementEndDate;

    @Column(name = "establishment_of_origin_address")
    private String establishmentOfOriginAddress;

    @Column(name = "establishment_of_destination_address")
    private String establishmentOfDestinationAddress;

    @Column(name = "origin_account_number")
    private String originAccountNumber;

    @Column(name = "discharge_mode")
    private String dischargeMode;

    @Column(name = "legal_care_mode")
    private String legalCareMode;

    /**
     * ZFV-11
     */
    @Column(name = "care_during_transport")
    private String careDuringTransport;

    @OneToOne(mappedBy = "encounterAdditionalInfo", targetEntity = Encounter.class)
    private Encounter relatedEncounter;

    @Transient
    private String establishmentOfDestination;

    /**
     * <p>Getter for the field <code>establishmentOfDestination</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEstablishmentOfDestination() {
        return establishmentOfDestination;
    }

    /**
     * <p>Setter for the field <code>establishmentOfDestination</code>.</p>
     *
     * @param establishmentOfDestinationAddressCode a {@link java.lang.String} object.
     */
    public void setEstablishmentOfDestination(String establishmentOfDestinationAddressCode) {
        this.establishmentOfDestination = establishmentOfDestinationAddressCode;
    }


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>establishmentOfOrigin</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEstablishmentOfOrigin() {
        return establishmentOfOrigin;
    }

    /**
     * <p>Setter for the field <code>establishmentOfOrigin</code>.</p>
     *
     * @param establishmentOfOrigin a {@link java.lang.String} object.
     */
    public void setEstablishmentOfOrigin(String establishmentOfOrigin) {
        this.establishmentOfOrigin = establishmentOfOrigin;
    }

    /**
     * <p>Getter for the field <code>lastVisitDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastVisitDate() {
        return lastVisitDate;
    }

    /**
     * <p>Setter for the field <code>lastVisitDate</code>.</p>
     *
     * @param lastVisitDate a {@link java.util.Date} object.
     */
    public void setLastVisitDate(Date lastVisitDate) {
        if (lastVisitDate != null) {
            this.lastVisitDate = (Date) lastVisitDate.clone();
        } else {
            this.lastVisitDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>transportMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTransportMode() {
        return transportMode;
    }

    /**
     * <p>Setter for the field <code>transportMode</code>.</p>
     *
     * @param transportMode a {@link java.lang.String} object.
     */
    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    /**
     * <p>Getter for the field <code>placementStartDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getPlacementStartDate() {
        return placementStartDate;
    }

    /**
     * <p>Setter for the field <code>placementStartDate</code>.</p>
     *
     * @param placementStartDate a {@link java.util.Date} object.
     */
    public void setPlacementStartDate(Date placementStartDate) {
        if (placementStartDate != null) {
            this.placementStartDate = (Date) placementStartDate.clone();
        } else {
            this.placementStartDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>placementEndDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getPlacementEndDate() {
        return placementEndDate;
    }

    /**
     * <p>Setter for the field <code>placementEndDate</code>.</p>
     *
     * @param placementEndDate a {@link java.util.Date} object.
     */
    public void setPlacementEndDate(Date placementEndDate) {
        if (placementEndDate != null) {
            this.placementEndDate = (Date) placementEndDate.clone();
        } else {
            this.placementEndDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>establishmentOfOriginAddress</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEstablishmentOfOriginAddress() {
        return establishmentOfOriginAddress;
    }

    /**
     * <p>Setter for the field <code>establishmentOfOriginAddress</code>.</p>
     *
     * @param establishmentOfOriginAddress a {@link java.lang.String} object.
     */
    public void setEstablishmentOfOriginAddress(String establishmentOfOriginAddress) {
        this.establishmentOfOriginAddress = establishmentOfOriginAddress;
    }

    /**
     * <p>Getter for the field <code>establishmentOfDestinationAddress</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEstablishmentOfDestinationAddress() {
        return establishmentOfDestinationAddress;
    }

    /**
     * <p>Setter for the field <code>establishmentOfDestinationAddress</code>.</p>
     *
     * @param establishmentOfDestinationAddress a {@link java.lang.String} object.
     */
    public void setEstablishmentOfDestinationAddress(String establishmentOfDestinationAddress) {
        this.establishmentOfDestinationAddress = establishmentOfDestinationAddress;
    }

    /**
     * <p>Getter for the field <code>originAccountNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOriginAccountNumber() {
        return originAccountNumber;
    }

    /**
     * <p>Setter for the field <code>originAccountNumber</code>.</p>
     *
     * @param originAccountNumber a {@link java.lang.String} object.
     */
    public void setOriginAccountNumber(String originAccountNumber) {
        this.originAccountNumber = originAccountNumber;
    }

    /**
     * <p>Getter for the field <code>dischargeMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDischargeMode() {
        return dischargeMode;
    }

    /**
     * <p>Setter for the field <code>dischargeMode</code>.</p>
     *
     * @param dischargeMode a {@link java.lang.String} object.
     */
    public void setDischargeMode(String dischargeMode) {
        this.dischargeMode = dischargeMode;
    }

    /**
     * <p>Getter for the field <code>legalCareMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLegalCareMode() {
        return legalCareMode;
    }

    /**
     * <p>Setter for the field <code>legalCareMode</code>.</p>
     *
     * @param legalCareMode a {@link java.lang.String} object.
     */
    public void setLegalCareMode(String legalCareMode) {
        this.legalCareMode = legalCareMode;
    }


    /**
     * <p>Getter for the field <code>relatedEncounter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getRelatedEncounter() {
        return relatedEncounter;
    }

    /**
     * <p>Setter for the field <code>relatedEncounter</code>.</p>
     *
     * @param relatedEncounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setRelatedEncounter(Encounter relatedEncounter) {
        this.relatedEncounter = relatedEncounter;
    }

    /**
     * <p>setAddressForEstablishmentOfOrigin.</p>
     */
    public void setAddressForEstablishmentOfOrigin(){
        if (establishmentOfOrigin != null){
            establishmentOfOriginAddress = ValueSet.getDisplayNameForCode("FACILITY_ADDR", establishmentOfOrigin);
        } else {
            establishmentOfOriginAddress = null;
        }
    }

    /**
     * <p>setAddressForEstablishmentOfDestination.</p>
     */
    public void setAddressForEstablishmentOfDestination(){
        if (establishmentOfDestination != null){
            establishmentOfDestinationAddress = ValueSet.getDisplayNameForCode("FACILITY_ADDR", establishmentOfDestination);
        } else {
            establishmentOfDestinationAddress = null;
        }
    }

    public String getCareDuringTransport() {
        return careDuringTransport;
    }

    public void setCareDuringTransport(String careDuringTransport) {
        this.careDuringTransport = careDuringTransport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EncounterAdditionalInfo)) {
            return false;
        }

        EncounterAdditionalInfo that = (EncounterAdditionalInfo) o;

        if (establishmentOfOrigin != null ? !establishmentOfOrigin.equals(that.establishmentOfOrigin) : that.establishmentOfOrigin != null) {
            return false;
        }
        if (lastVisitDate != null ? !lastVisitDate.equals(that.lastVisitDate) : that.lastVisitDate != null) {
            return false;
        }
        if (transportMode != null ? !transportMode.equals(that.transportMode) : that.transportMode != null) {
            return false;
        }
        if (placementStartDate != null ? !placementStartDate.equals(that.placementStartDate) : that.placementStartDate != null) {
            return false;
        }
        if (placementEndDate != null ? !placementEndDate.equals(that.placementEndDate) : that.placementEndDate != null) {
            return false;
        }
        if (establishmentOfOriginAddress != null ? !establishmentOfOriginAddress.equals(that.establishmentOfOriginAddress) : that
                .establishmentOfOriginAddress != null) {
            return false;
        }
        if (establishmentOfDestinationAddress != null ? !establishmentOfDestinationAddress.equals(that.establishmentOfDestinationAddress) : that
                .establishmentOfDestinationAddress != null) {
            return false;
        }
        if (originAccountNumber != null ? !originAccountNumber.equals(that.originAccountNumber) : that.originAccountNumber != null) {
            return false;
        }
        if (dischargeMode != null ? !dischargeMode.equals(that.dischargeMode) : that.dischargeMode != null) {
            return false;
        }
        if (legalCareMode != null ? !legalCareMode.equals(that.legalCareMode) : that.legalCareMode != null) {
            return false;
        }
        if (careDuringTransport != null ? !careDuringTransport.equals(that.careDuringTransport) : that.careDuringTransport != null) {
            return false;
        }
        return establishmentOfDestination != null ? establishmentOfDestination.equals(that.establishmentOfDestination) : that
                .establishmentOfDestination == null;
    }

    @Override
    public int hashCode() {
        int result = establishmentOfOrigin != null ? establishmentOfOrigin.hashCode() : 0;
        result = 31 * result + (lastVisitDate != null ? lastVisitDate.hashCode() : 0);
        result = 31 * result + (transportMode != null ? transportMode.hashCode() : 0);
        result = 31 * result + (placementStartDate != null ? placementStartDate.hashCode() : 0);
        result = 31 * result + (placementEndDate != null ? placementEndDate.hashCode() : 0);
        result = 31 * result + (establishmentOfOriginAddress != null ? establishmentOfOriginAddress.hashCode() : 0);
        result = 31 * result + (establishmentOfDestinationAddress != null ? establishmentOfDestinationAddress.hashCode() : 0);
        result = 31 * result + (originAccountNumber != null ? originAccountNumber.hashCode() : 0);
        result = 31 * result + (dischargeMode != null ? dischargeMode.hashCode() : 0);
        result = 31 * result + (legalCareMode != null ? legalCareMode.hashCode() : 0);
        result = 31 * result + (careDuringTransport != null ? careDuringTransport.hashCode() : 0);
        result = 31 * result + (establishmentOfDestination != null ? establishmentOfDestination.hashCode() : 0);
        return result;
    }
}
