package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PatientDAO class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PatientDAO {

    private static final Logger LOG = LoggerFactory.getLogger(PatientDAO.class);
    /**
     * <p>getActivePatientByIdentifierByActor.</p>
     *
     * @param foundIdentifier a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @param simulatedActor  TODO
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public static Patient getActivePatientByIdentifierByActor(PatientIdentifier foundIdentifier,
                                                              EntityManager entityManager, Actor simulatedActor) {
        if (foundIdentifier == null) {
            return null;
        } else {
            PatientQuery query = new PatientQuery(entityManager);
            query.stillActive().eq(true);
            query.simulatedActor().id().eq(simulatedActor.getId());
            query.patientIdentifiers().fullPatientId().eq(foundIdentifier.getFullPatientId());
            try {
                List<Patient> patients = query.getListNullIfEmpty();
                if (patients == null) {
                    LOG.debug("no patient matching");
                    return null;
                } else if (patients.size() == 1) {
                    LOG.debug("patient exists");
                    return patients.get(0);
                } else {
                    LOG.debug("more than one patient is active and uses this id !!!");
                    return null;
                }
            } catch (Exception e) {
                LOG.error("no patient matching");
                return null;
            }
        }
    }

    /**
     * <p>getPatientByPidByActor.</p>
     *
     * @param pid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Patient> getPatientByPidByActor(PatientIdentifier pid, Actor simulatedActor) {
        if ((pid != null) && (simulatedActor != null)) {
            PatientQuery query = new PatientQuery();
            query.simulatedActor().id().eq(simulatedActor.getId());
            query.patientIdentifiers().fullPatientId().eq(pid.getFullPatientId());
            List<Patient> patients = query.getListNullIfEmpty();
            if (patients != null) {
                return patients;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    public static Patient getPatientById(Integer dbIdentifier){
        PatientQuery query = new PatientQuery();
        query.id().eq(dbIdentifier);
        return query.getUniqueResult();
    }


    public static List<PatientFhirIHE> toFhirPatients(List<net.ihe.gazelle.simulator.pam.model.Patient> patientList) {
        List<PatientFhirIHE> patientFhirIHEList = new ArrayList<PatientFhirIHE>();
        for (net.ihe.gazelle.simulator.pam.model.Patient patient : patientList) {
            patientFhirIHEList.add(patient.toFhirPatient());
        }
        return patientFhirIHEList;

    }
}
