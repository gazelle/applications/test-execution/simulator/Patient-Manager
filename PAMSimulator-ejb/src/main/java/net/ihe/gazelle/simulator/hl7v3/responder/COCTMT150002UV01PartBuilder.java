package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.coctmt150002UV01.COCTMT150002UV01Organization;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.ON;
import net.ihe.gazelle.hl7v3.voc.EntityClassOrganization;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;

import java.util.Arrays;

/**
 * Created by aberge on 12/03/15.
 */
public class COCTMT150002UV01PartBuilder extends HL7v3MessageBuilder{

	public static COCTMT150002UV01Organization populateProviderOrganization(String name, boolean populateId) {
		COCTMT150002UV01Organization organization = new COCTMT150002UV01Organization();
		organization.setClassCode(EntityClassOrganization.ORG);
		organization.setDeterminerCode(EntityDeterminer.INSTANCE);
		if (populateId) {
			organization.addId(new II(PreferenceService.getString("hl7v3_organization_oid"), null));
		}
		ON on = new ON();
		on.getMixed().add(name);
		organization.setName(Arrays.asList(on));
		return organization;
	}
}
