package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Acknowledgement;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient;
import net.ihe.gazelle.hl7v3.voc.AcknowledgementDetailType;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3AcknowledgmentCode;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ResponseCode;
import net.ihe.gazelle.simulator.hl7v3.messages.MCCIMT00300UV01PartBuilder;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;

import java.util.List;

/**
 * <b>Class Description : </b>PDQV3PDSResponseBuilder<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 20/01/16
 */
public class PDQV3PDSResponseBuilder extends PRPAIN201306UV02Builder {

    private static final String KSA_HEALTHID_OID = "2.16.840.1.113883.3.3731.1.1.100.1";

    /**
     * <p>Constructor for PDQV3PDSResponseBuilder.</p>
     *
     * @param inDomainKeyword a {@link java.lang.String} object.
     */
    public PDQV3PDSResponseBuilder(String inDomainKeyword) {
        super(PDQV3_QUERY_TRANSACTION, inDomainKeyword);
    }

    /** {@inheritDoc} */
    @Override
    public PRPAIN201306UV02Type buildFindCandidatesQueryResponse(II messageId, List<Patient> patients,
                                                                 List<Integer> unknownDomains, List<HierarchicDesignator> domainsToReturn,
                                                                 PRPAMT201306UV02QueryByParameter queryByParameter, String sutDeviceOid) {
        PRPAIN201306UV02Type response = super.buildFindCandidatesQueryResponse(messageId, patients, unknownDomains,
                domainsToReturn, queryByParameter, sutDeviceOid);
        response.setSender(MCCIMT00300UV01PartBuilder.buildSender(
                PreferenceService.getString("hl7v3_pdq_pds_device_id"), PreferenceService.getString("pdqv3_pds_url")));
        return response;
    }

    /** {@inheritDoc} */
    @Override
    protected void buildControlActWrapper(II messageId, List<Patient> patients, List<Integer> unknownDomains,
                                                 List<HierarchicDesignator> domainsToReturn,
                                                 PRPAMT201306UV02QueryByParameter queryByParameter,
                                                 PRPAIN201306UV02Type response,
                                                 PRPAIN201306UV02MFMIMT700711UV01ControlActProcess controlActProcess) {
        if (queryByParameter == null) {
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AR.getValue()));
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(null, null, HL7V3ResponseCode.QE.getValue(), "aborted",
                    null));
        } else if (unknownDomains != null){
            MCCIMT000300UV01Acknowledgement ack = MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AE.getValue());
            for (Integer index: unknownDomains){
                ack.addAcknowledgementDetail(MCCIMT00300UV01PartBuilder.populateAcknowledgementDetail(
                        AcknowledgementDetailType.E, "204", OTHERIDS_VALUE_XPATH.replace("$index$",index.toString())));
            }
            response.addAcknowledgement(ack);
            // AE (application error) is returned in QueryAck.queryResponseCode (control act wrapper).
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.AE.getValue(),
                    "deliveredResponse", null));
        }else if (patients != null) {
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
            // OK (data found, no errors) is returned in QueryAck.queryResponseCode (control act wrapper)
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.OK.getValue(),
                    "deliveredResponse", patients.size()));
            // One Registration Event is returned from the patient information source for each patient record found
            for (Patient currentPatient : patients) {
                PRPAIN201306UV02MFMIMT700711UV01Subject1 subject = populateSubject();
                PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent event = populateRegistrationEvent();
                event.setCustodian(MFMIMT700711UV01PartBuilder.populateCustodian());
                PRPAIN201306UV02MFMIMT700711UV01Subject2 subject1 = populateSubject1();
                PRPAMT201310UV02Patient msgPatient = populatePatient(currentPatient, domainsToReturn);
                subject1.setPatient(msgPatient);
                event.setSubject1(subject1);
                subject.setRegistrationEvent(event);
                controlActProcess.addSubject(subject);
            }
        } else {
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
            // OK (data found, no errors) is returned in QueryAck.queryResponseCode (control act wrapper)
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.NF.getValue(),
                    "deliveredResponse", null));
        }
    }

    /** {@inheritDoc} */
    protected void buildPatientId(Patient currentPatient, List<HierarchicDesignator> domainsToReturn, PRPAMT201310UV02Patient msgPatient) {
        if (domainsToReturn == null || domainsToReturn.isEmpty()) {
            for (PatientIdentifier identifier : currentPatient.getPatientIdentifiers()) {
                if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEHE)
                        || identifier.getDomain().getUniversalID().equals(KSA_HEALTHID_OID)) {
                    II id = new II(identifier.getDomain().getUniversalID(), identifier.getIdNumber());
                    msgPatient.addId(id);
                }

            }
        }else{
            for (HierarchicDesignator domain: domainsToReturn){
                List<PatientIdentifier> pids = PatientIdentifierDAO.getPatientIdentifiers(currentPatient, domain);
                if (pids.isEmpty()){
                    msgPatient.getPatientPerson().addAsOtherIDs(populateAsOtherIDsForNotFoundID(domain));
                }else{
                    for (PatientIdentifier pid: pids){
                        msgPatient.addId(new II(pid.getDomain().getUniversalID(), pid.getIdNumber()));
                    }
                }
            }
            // patient.id cardinality is [2..2], a value shall appear even though no identifier exists for this patient
            while (msgPatient.getId().size() < 2){
                II nullFlavor = new II();
                nullFlavor.setNullFlavor(NullFlavor.UNK);
                msgPatient.addId(nullFlavor);
            }
        }
    }
}
