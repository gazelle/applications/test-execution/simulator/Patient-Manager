package net.ihe.gazelle.simulator.pam.pds;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.Person;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.util.List;

/**
 * <p>UpdateManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("updateManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class UpdateManager extends PDS implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String PIX_UPDATE_TRANSACTION = "ITI-10";

    /**
     * <p>generateAPatient.</p>
     */
    public void generateAPatient() {
        selectedPatient = null;
        secondaryPatient = super.generateNewPatient(sendingActor);
        if (secondaryPatient != null) {
            displayDDSPanel = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateSelectedEvent() {
        ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
        bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A31);
    }

    @Override
    protected List<TransactionInstance> send() {
        if (secondaryPatient == null) {
            throw new SendindException("Please, first select a patient to be sent");
        }
        try {
            PatientHistory h = patientService.updatePatientVersion(secondaryPatient, selectedPatient);
            if (h.getOtherPatient()==null) {
                secondaryPatient = h.getCurrentPatient();
            } else {
                selectedPatient = h.getCurrentPatient();
                secondaryPatient = h.getOtherPatient();
            }
            List<TransactionInstance> transactions = patientService.sendADTMessage(
                    new PatientService.Exchange(
                            selectedSUT,
                            sendingApplication,
                            sendingFacility,
                            sendingActor,
                            receivingActor,
                            simulatedTransaction,
                            getHL7Domain(),
                            PatientService.ADT.A31,
                            secondaryPatient
                    ).setPid3Only(PIX_UPDATE_TRANSACTION.equals(simulatedTransaction.getKeyword())));
            secondaryPatient = null;
            selectedPatient = null;
            return transactions;
        } catch (HL7Exception e) {
            throw new SendindException(e.getMessage(),e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionOnSelectedPatient(Patient inPatient) {
        if (inPatient != null) {
            selectedPatient = inPatient;
            secondaryPatient = new Patient(selectedPatient, sendingActor);
            if (Identity.instance().getCredentials() != null) {
                secondaryPatient.setCreator(Identity.instance().getCredentials().getUsername());
            } else {
                secondaryPatient.setCreator(null);
            }
            displayPatientsList = false;
            displayDDSPanel = false;
        } else {
            selectedPatient = null;
            secondaryPatient = null;
        }
    }

    /**
     * <p>generateAdditionalIdentifiers.</p>
     */
    public void generateAdditionalIdentifiers() {
        patientBuilderFactory.make(getHL7Domain()).buildIdentifiers(secondaryPatient,selectedAuthorities);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createNewRelationship() {
        setCurrentRelationship(new Person(secondaryPatient));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRelationship() {
        secondaryPatient.addPersonalRelationship(getCurrentRelationship());
        setCurrentRelationship(null);
    }

    /**
     * For update, the remaining patient is secondaryPatient
     */
    @Override
    public void createNewLegalCareMode() {
        setCurrentLegalCareMode(secondaryPatient.addLegalCareMode());
    }

    @Override
    public List<LegalCareMode> getCareModesForPatient() {
        return this.secondaryPatient.getLegalCareModes();
    }
}
