package net.ihe.gazelle.simulator.xcpd.respondinggw;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.xcpd.common.XCPDQueryHandler;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransaction;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionQuery;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.async.CronSchedule;
import org.jboss.seam.async.QuartzDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DependsOn;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <b>Class Description : </b>DeferredHandler<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 13/01/16
 *


 */
@Name("deferredHandler")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
@GenerateInterface("DeferredHandlerLocal")
public class DeferredHandler implements DeferredHandlerLocal {

    private static Logger log = LoggerFactory.getLogger(DeferredHandler.class);
    private static final String DEFAULT_TRIGGER = "1 * * * * ?";

    @Override
    @Observer("processDeferredRequests")
    @Transactional
    public void processDeferredRequests(){
        List<DeferredTransaction> transactions = listTransactionsToBeProcessed();
        if (transactions != null){
            for (DeferredTransaction dt : transactions){
                processSingleDeferredRequest(dt);
            }
        }
    }

    @Override
    @Observer("processSingleRequest")
    @Transactional
    public void processSingleDeferredRequest(DeferredTransaction deferredTransaction){
        log.info("Processing deferred transaction with ID " + deferredTransaction.getId());
        XCPDRespGatewaySender sender;
        try{
            PRPAIN201305UV02Type request = buildRequest(deferredTransaction.getQuery().getRequest().getContent());
            if (request != null) {
                Domain domain = HL7V3ResponderUtils.getDefaultDomain();
                XCPDQueryHandler handler = new XCPDQueryHandler(Domain.getDomainByKeyword("ITI"),
                        Actor.findActorWithKeyword("RESP_GW"), Actor.findActorWithKeyword("INIT_GW"),
                        Transaction.GetTransactionByKeyword("ITI-55"), deferredTransaction.getQuery().getRequest().getIssuer(),
                        deferredTransaction.getQuery().getRequest().getIssuerIpAddress());
                PRPAIN201306UV02Type response = handler.handleCrossGatewayPatientDiscoveryQuery(request, false, domain);
                sender = new XCPDRespGatewaySender(deferredTransaction.getInitGatewayEndpoint(), deferredTransaction.getQuery().getRequest().getIssuer());
                try {
                    sender.sendDeferredCrossGatewayPatientDiscoveryResponse(response);
                } catch (SoapSendException e){
                    log.error("Cannot send message to SUT : " + e.getMessage(), e);
                    return;
                }
                deferredTransaction.setResponse(sender.getTransactionInstance());
                deferredTransaction.setStatus(DeferredTransactionStatus.DONE);
                deferredTransaction.save();
            } else {
                log.error("This query has no content");
            }
        } catch (JAXBException e) {
            log.error("Cannot process transaction with ID " + deferredTransaction.getId() + ": " + e.getMessage(), e);
        }
    }

    @Create
    public void startQuartz(){
        log.info("starting quartz");
        String trigger = PreferenceService.getString("xcpd_respw_cron_trigger");
        if (trigger == null){
            trigger = DEFAULT_TRIGGER;
        }
        QuartzDispatcher.instance().scheduleTimedEvent("processDeferredRequests", new CronSchedule((Long) null, trigger));
    }

    private List<DeferredTransaction> listTransactionsToBeProcessed(){
        DeferredTransactionQuery query = new DeferredTransactionQuery();
        query.simulatedActor().keyword().eq("RESP_GW");
        query.status().eq(DeferredTransactionStatus.WAITING);
        return query.getListNullIfEmpty();
    }

    private PRPAIN201305UV02Type buildRequest(byte[] requestAsBytes) throws JAXBException {
        if (requestAsBytes != null) {
            String requestAsString = new String(requestAsBytes, StandardCharsets.UTF_8);
            requestAsString = requestAsString.replace("UTF8", "UTF-8");
            return HL7V3Transformer.unmarshallMessage(PRPAIN201305UV02Type.class,
                    new ByteArrayInputStream(requestAsString.getBytes(StandardCharsets.UTF_8)));
        } else {
            return null;
        }
    }
}
