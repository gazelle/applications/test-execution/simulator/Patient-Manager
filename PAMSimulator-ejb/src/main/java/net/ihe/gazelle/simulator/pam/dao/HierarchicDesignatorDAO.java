package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignatorQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>HierarchicDesignatorDAO<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/04/16
 */
public class HierarchicDesignatorDAO {

    private static Logger log = LoggerFactory.getLogger(HierarchicDesignatorDAO.class);

    /**
     * <p>getToolsAssigningAuthoritiesNotForConnectathon.</p>
     *
     * @param usage a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HierarchicDesignator> getToolsAssigningAuthoritiesNotForConnectathon(DesignatorType usage){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.toolAssigningAuthority().eq(true);
        query.namespaceID().order(true);
        query.usage().eq(usage);
        query.catUsage().eq(false);
        return query.getList();
    }

    public static List<HierarchicDesignator> getAllAssigningAuthoritiesForUsage(DesignatorType usage){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.namespaceID().order(true);
        query.usage().eq(usage);
        return query.getList();
    }

    /**
     * <p>generatePatientIdentifier.</p>
     *
     * @param hd a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     * @return a {@link java.lang.String} object.
     */
    public static String generatePatientIdentifier(HierarchicDesignator hd){
        StringBuilder pid = new StringBuilder(hd.nextId());
        pid.append("^^^");
        String hdString = hd.toString().replace('^', '&');
        pid.append(hdString);
        return pid.toString();
    }

    /**
     * <p>getToolsDefaultAssigningAuthorities.</p>
     *
     * @param usage a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HierarchicDesignator> getToolsDefaultAssigningAuthorities(DesignatorType usage) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.toolAssigningAuthority().eq(true);
        query.namespaceID().order(true);
        query.defaultAssigningAuthority().eq(true);
        query.usage().eq(usage);
        query.catUsage().eq(false);
        return query.getList();
    }

    public static List<HierarchicDesignator> getAssigningAuthoritiesForConnectathon(){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.usage().eq(DesignatorType.PATIENT_ID);
        query.catUsage().eq(true);
        query.namespaceID().order(true);
        return query.getList();
    }

    /**
     * <p>getAssigningAuthority.</p>
     *
     * @param numberType a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     */
    public static HierarchicDesignator getAssigningAuthority(DesignatorType numberType) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        Integer aaId = PreferenceService.getInteger(numberType.getAppPreference());
        if (aaId == null){
            log.warn("No default assigning authority set for " + numberType.name());
            return null;
        }
        query.id().eq(aaId);
        return query.getUniqueResult();
    }

    /**
     * <p>getAssigningAuthorities.</p>
     *
     * @param type a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link java.util.List} object.
     */
    public static List<SelectItem> getAssigningAuthorities(DesignatorType type) {
        List<HierarchicDesignator> values = getToolAssigningAuthoritiesForUsage(type);
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem(null, "Please select..."));
        for (HierarchicDesignator hd: values){
            items.add(new SelectItem(hd.getId().toString(), hd.toString()));
        }
        return items;
    }

    public static List<HierarchicDesignator> getToolAssigningAuthoritiesForUsage(DesignatorType type) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.toolAssigningAuthority().eq(true);
        query.usage().eq(type);
        query.namespaceID().order(true);
        return query.getList();
    }

    public static List<HierarchicDesignator> getToolAssigningAuthoritiesForPatientId(){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.usage().eq(DesignatorType.PATIENT_ID);
        query.catUsage().order(false);
        query.namespaceID().order(true);
        return query.getList();
    }

    /**
     * <p>getAssigningAuthority.</p>
     *
     * @param namespaceId a {@link java.lang.String} object.
     * @param universalId a {@link java.lang.String} object.
     * @param usage a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     */
    public static HierarchicDesignator getAssigningAuthority(String namespaceId, String universalId, DesignatorType usage){
        HierarchicDesignatorQuery query = getAssigningAuthorityBase(namespaceId, universalId, usage);
        return query.getUniqueResult();
    }

    public static HierarchicDesignator getAssigningAuthorityOwnedByToolAndNotForCat(String namespaceId, String universalId, DesignatorType usage){
        HierarchicDesignatorQuery query = getAssigningAuthorityBase(namespaceId, universalId, usage);
        query.toolAssigningAuthority().eq(true);
        query.catUsage().eq(false);
        return query.getUniqueResult();
    }

    private static HierarchicDesignatorQuery getAssigningAuthorityBase(String namespaceId, String universalId, DesignatorType usage){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        if (namespaceId != null && !namespaceId.isEmpty()){
            query.namespaceID().eq(namespaceId);
        }
        if (universalId != null && !universalId.isEmpty()){
            query.universalID().eq(universalId);
        }
        query.usage().eq(usage);
        return query;
    }

    /**
     * <p>createAssigningAuthority.</p>
     *
     * @param namespaceId a {@link java.lang.String} object.
     * @param universalId a {@link java.lang.String} object.
     * @param universalIdType a {@link java.lang.String} object.
     * @param usage a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     */
    public static HierarchicDesignator createAssigningAuthority(String namespaceId, String universalId, String universalIdType, DesignatorType usage){
        HierarchicDesignator hierarchicDesignator = new HierarchicDesignator();
        hierarchicDesignator.setNamespaceID(namespaceId);
        hierarchicDesignator.setUniversalID(universalId);
        hierarchicDesignator.setUniversalIDType(universalIdType);
        hierarchicDesignator.setUsage(usage);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        hierarchicDesignator = entityManager.merge(hierarchicDesignator);
        entityManager.flush();
        return hierarchicDesignator;
    }

    public static boolean domainExists(String universalId, DesignatorType usage){
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.universalID().eq(universalId);
        query.usage().eq(usage);
        return query.getCount() > 0;
    }
}
