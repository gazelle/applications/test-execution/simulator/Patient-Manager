package net.ihe.gazelle.simulator.pam.automaton.helper;


import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.PatientGenerator;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.report.TestStep;
import net.ihe.gazelle.simulator.pam.dao.EncounterDAO;
import net.ihe.gazelle.simulator.pam.dao.MovementDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.async.QuartzDispatcher;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class AutomatonHelper extends PatientGenerator {

    private static final String SENDING_FACILITY = "Gazelle";
    private static final String SENDING_APPLICATION = "Automaton";

    private boolean isBP6Mode;
    private transient PatientService patientService;
    private transient HL7Domain<?> hl7Domain = null;

    public AutomatonHelper(boolean isBP6Mode, PatientService patientService) {
        super();
        this.isBP6Mode = isBP6Mode;
        this.patientService = patientService;
    }

    public Patient generatePatient(String countryCodeString) {
        setSelectedCountryString(countryCodeString);
        super.initializeGenerator();
        selectCountry();
        AbstractPatient aPatient = generatePatient();
        if (aPatient != null) {
            Patient newPatient = new Patient(aPatient);
            newPatient.setAccountNumber(NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER));
            Actor pes = Actor.findActorWithKeyword("PES");
            newPatient.setSimulatedActor(pes);
            newPatient.setStillActive(true);
            newPatient.setCreator("automaton");
            generatePatientIdentifiers(newPatient);
            return newPatient;
        } else {
            return null;
        }
    }

    private void generatePatientIdentifiers(Patient newPatient) {
        List<PatientIdentifier> pidList = PatientIdentifierDAO.createListFromNationalAndDDSIdentifiers(
                newPatient.getNationalPatientIdentifier(), newPatient.getDdsIdentifier());
        if (pidList != null) {
            newPatient.setPatientIdentifiers(pidList);
        }
    }


    public Encounter createEncounter(Patient patient) {
        Encounter encounter = new Encounter(patient);
        EncounterDAO.randomlyFillEncounter(encounter);
        Movement movement = new Movement(encounter);
        MovementDAO.randomlyFillMovement(movement, isBP6Mode);
        List<Movement> movements = new ArrayList<>();
        movements.add(movement);
        encounter.setMovements(movements);
        return encounter;
    }

    public void sendA28(HL7V2ResponderSUTConfiguration sut, Encounter currentEncounter) throws HL7Exception {
        patientService.sendADTMessage(
                new PatientService.Exchange(
                        sut,
                        SENDING_APPLICATION,
                        SENDING_FACILITY,
                        Actor.findActorWithKeyword(PatientManagerConstants.PDS),
                        Actor.findActorWithKeyword(PatientManagerConstants.PDC),
                        Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_30),
                        getHL7Domain(),
                        PatientService.ADT.A28,
                        currentEncounter.getPatient()
                )
        );
    }


    public Integer execute(GraphExecutionResults graphExecutionResults, Encounter encounter) {

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        graphExecutionResults.setMessagesList(new ArrayList<TestStep>());
        graphExecutionResults = entityManager.merge(graphExecutionResults);
        entityManager.flush();

        QuartzDispatcher.instance().scheduleAsynchronousEvent(GraphWalkerObserver.getObserverName(), graphExecutionResults, encounter);
        return graphExecutionResults.getId();
    }

    private HL7Domain<?> getHL7Domain() {
        if (isBP6Mode) {
            hl7Domain = hl7Domain instanceof BP6HL7Domain ? hl7Domain : new BP6HL7Domain();
        } else {
            hl7Domain = hl7Domain instanceof IHEHL7Domain ? hl7Domain : new IHEHL7Domain();
        }
        return hl7Domain;
    }


}


