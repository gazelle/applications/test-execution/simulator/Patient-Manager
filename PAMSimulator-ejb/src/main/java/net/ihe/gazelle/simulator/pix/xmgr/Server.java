package net.ihe.gazelle.simulator.pix.xmgr;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;


/**
 * <p>Server class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Startup(depends = {"entityManager"})
@Name("pixManager")
@Scope(ScopeType.APPLICATION)
public class Server  extends AbstractServer<PIXManager> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8388615324083549942L;

	/** {@inheritDoc} */
	@Override
    @Create
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword(
				"PAT_IDENTITY_X_REF_MGR", entityManager);
		Domain domain = Domain
				.getDomainByKeyword("ITI", entityManager);
		handler = new PIXManager();
		startServers(simulatedActor, null, domain);
	}

}
