package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PIXV3ManagerGUIManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixv3managerGuiManager")
@Scope(ScopeType.PAGE)
public class PIXV3ManagerGUIManager extends AbstractPDQPDSGuiManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2657551111877383278L;
	private String endpointUrl;
	private String deviceId;
	private String organizationOid;
	
	/**
	 * <p>getUrlForHL7Messages.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForHL7Messages() {
        Actor actor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
		return "/messages/browser.seam?simulatedActor=" + actor.getId() + "&responder=" + actor.getId();
	}

	@Override
	public String linkToPatients(){
		Actor actor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
		return "/patient/allPatients.seam?actor=" + actor.getId();
	}

	/** {@inheritDoc} */
	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
		this.endpointUrl = PreferenceService.getString("pixv3_mgr_url");
		this.deviceId = PreferenceService.getString("hl7v3_pix_mgr_device_id");
		this.organizationOid = PreferenceService.getString("hl7v3_organization_oid");
	}

	/**
	 * <p>Getter for the field <code>endpointUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getEndpointUrl() {
		return endpointUrl;
	}

	/**
	 * <p>Getter for the field <code>deviceId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * <p>Getter for the field <code>organizationOid</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOrganizationOid() {
		return organizationOid;
	}

}
