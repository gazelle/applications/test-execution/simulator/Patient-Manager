package net.ihe.gazelle.simulator.pam.automaton.Manager;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicNumber;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicString;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionQuery;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResultsQuery;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by xfs on 26/11/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("automatonListManager")
@Scope(ScopeType.PAGE)
public class AutomatonListManager implements Serializable{

    private static Logger log = LoggerFactory.getLogger(AutomatonListManager.class);
    protected Filter<GraphExecutionResults> filter;

    /**
     * <p>displayGraphExecutionResults.</p>
     *
     * @param graphExecutionResults a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults} object.
     * @return a {@link java.lang.String} object.
     */
    public String displayGraphExecutionResults(GraphExecutionResults graphExecutionResults) {
        return "/automaton/pamAutomaton.seam?graphExecutionResultsId=" + graphExecutionResults.getId();
    }

    /**
     * <p>createGraphExecutionResults.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createGraphExecutionResults() {
        return "/automaton/pamAutomaton.seam";
    }


    /**
     * <p>getGraphExecutionResultsList.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<GraphExecutionResults> getGraphExecutionResultsList() {
        return new FilterDataModel<GraphExecutionResults>(getFilter()) {
            @Override
            protected Object getId(GraphExecutionResults graphExecutionResults) {
                return graphExecutionResults.getId();
            }
        };
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<GraphExecutionResults> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            filter = new Filter<GraphExecutionResults>(getFilterCriteria(), params);
        }
        return filter;
    }

    private HQLCriterionsForFilter<GraphExecutionResults> getFilterCriteria() {
        GraphExecutionResultsQuery query = new GraphExecutionResultsQuery();
        HQLCriterionsForFilter<GraphExecutionResults> criteria = query.getHQLCriterionsForFilter();
        criteria.getQueryModifiers();
        criteria.addPath("timestamp", query.timestamp());
        criteria.addPath("graph", query.graphExecution().graph());
        criteria.addPath("sut", query.graphExecution().sut());
        criteria.addPath("fullEdgeCoverage", query.graphExecution().fullEdgeCoverage());
        return criteria;
    }

    /**
     * <p>displayCurrentPage.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayCurrentPage() {
        return "/automaton/pamAutomatonList.seam";

    }

}
