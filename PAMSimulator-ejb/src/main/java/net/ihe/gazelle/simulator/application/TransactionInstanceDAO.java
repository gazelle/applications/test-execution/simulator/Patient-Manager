package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;

/**
 * Interface for {@link TransactionInstance} DAO.
 */
public interface TransactionInstanceDAO {

    /**
     * Get a Transaction Instance by its identifier.
     *
     * @param id identifier of the transaction instance to retrieve.
     * @return the retrieved {@link TransactionInstance}
     */
    TransactionInstance retrieveTransactionInstanceById(String id);
}
