package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.List;

public abstract class PatientSupplierManager extends PatientSelectorManager {

    protected Actor sendingActor;
    protected Actor receivingActor;
    protected Transaction simulatedTransaction;
    protected String sendingApplication;
    protected String sendingFacility;
    protected Patient selectedPatient; // correctPatient for merge - patientOne for Link
    protected Patient secondaryPatient; // incorrect Patient for merge and changeId - patientTwo for link
    protected HL7V2ResponderSUTConfiguration selectedSUT;
    protected transient List<TransactionInstance> hl7Messages;
    protected transient List<HL7V2ResponderSUTConfiguration> availableSystems;
    /**
     * used in the context of PAM FR to populate ZFS segments
     */
    protected LegalCareMode currentLegalCareMode;

    /**
     * <p>Getter for the field <code>receivingActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getReceivingActor() {
        return receivingActor;
    }

    /**
     * <p>Getter for the field <code>simulatedTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getSimulatedTransaction() {
        return simulatedTransaction;
    }

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }


    /**
     * <p>Getter for the field <code>sendingApplication</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSendingApplication() {
        return sendingApplication;
    }

    /**
     * <p>Getter for the field <code>sendingFacility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSendingFacility() {
        return sendingFacility;
    }

    /**
     * <p>Getter for the field <code>hl7messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    /**
     * <p>Getter for the field <code>secondaryPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSecondaryPatient() {
        return secondaryPatient;
    }

    /**
     * <p>Getter for the field <code>sendingActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSendingActor() {
        return sendingActor;
    }

    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * <p>Getter for the field <code>selectedSUT</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     *
     * @return LegalCareMode
     */
    public LegalCareMode getCurrentLegalCareMode(){
        return this.currentLegalCareMode;
    }

    /**
     * <p>Setter for the field <code>secondaryPatient</code>.</p>
     *
     * @param secondaryPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSecondaryPatient(Patient secondaryPatient) {
        this.secondaryPatient = secondaryPatient;
    }

    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * <p>Setter for the field <code>selectedSUT</code>.</p>
     *
     * @param selectedSystemConfig a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public void setSelectedSUT(HL7V2ResponderSUTConfiguration selectedSystemConfig) {
        this.selectedSUT = selectedSystemConfig;
    }
    /**
     * <p>createNewPhoneNumber.</p>
     */
    public  void createNewPhoneNumber(){
        this.newPhoneNumber = new PatientPhoneNumber();
        newPhoneNumber.setPatient(selectedPatient);
    }
    /**
     * <p>addNewPhoneNumber.</p>
     */
    public void addNewPhoneNumber(){
        if (this.newPhoneNumber.getValue() == null || this.newPhoneNumber.getValue().isEmpty()){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Phone number shall have a value");
        } else {
            selectedPatient.addPhoneNumber(newPhoneNumber);
            newPhoneNumber = null;
        }
    }

    public void setCurrentLegalCareMode(LegalCareMode currentLegalCareMode) {
        this.currentLegalCareMode = currentLegalCareMode;
    }
    /**
     * Change action to UPDATE and allow the user to edit attributes
     * @param inLegalCareMode
     */
    public void updateCareMode(LegalCareMode inLegalCareMode){
        setCurrentLegalCareMode(inLegalCareMode);
        this.currentLegalCareMode.setAction(ActionType.UPDATE);
        this.currentLegalCareMode.setIncludeInMessage(true);
    }

    /**
     * Change action to CANCEL
     * @param inLegalCareMode
     */
    public void cancelCareMode(LegalCareMode inLegalCareMode){
        inLegalCareMode.setAction(ActionType.CANCEL);
        inLegalCareMode.setIncludeInMessage(true);
    }

    public void createNewLegalCareMode(){
        this.currentLegalCareMode = selectedPatient.addLegalCareMode();
    }

    public List<LegalCareMode> getCareModesForPatient(){
        return this.selectedPatient.getLegalCareModes();
    }

    public abstract void sendMessage();

    public void qualifiedIdentityCheckboxListener() {
        if (isQualifiedIdentity()) {
            setGenerateIdsForConnectathon(true);
            setGenerateIdsForAuthorities(true);
            addInsToSelectedAuthorities();
        }
    }

    public void addInsToSelectedAuthorities() {
        addSelectedAuthorities(HierarchicDesignatorDAO.getAssigningAuthorityOwnedByToolAndNotForCat("ASIP-SANTE-INS-NIR", "1.2.250.1.213.1.4.10", DesignatorType.PATIENT_ID));
        addSelectedAuthorities(HierarchicDesignatorDAO.getAssigningAuthorityOwnedByToolAndNotForCat("ASIP-SANTE-INS-NIA", "1.2.250.1.213.1.4.9", DesignatorType.PATIENT_ID));
    }

    public void populateBP6selectedAuthorities() {
        if (isQualifiedIdentity()) {
            addInsToSelectedAuthorities();
        }
    }

    public boolean isSynchronizedWithSUT() {
        return false;
    }

}
