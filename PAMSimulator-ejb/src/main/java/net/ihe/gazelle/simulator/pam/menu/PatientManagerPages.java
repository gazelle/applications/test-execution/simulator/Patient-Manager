package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.AuthorizationOr;
import net.ihe.gazelle.common.pages.Page;
import org.jboss.seam.core.ResourceBundle;

import java.util.MissingResourceException;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum  PatientManagerPages implements Page {

    ADT("", "net.ihe.gazelle.PatientManager.ADT", PatientManagerAuthorizations.ADT),
    ADT_RAD_1("/pes/pes.seam?transaction=RAD-1", "net.ihe.gazelle.PatientManager.RAD1PatientRegistration", PatientManagerAuthorizations.ADT),
    ADT_RAD_12("/pes/pes.seam?transaction=RAD-12", "net.ihe.gazelle.PatientManager.RAD12PatientUpdate", PatientManagerAuthorizations.ADT),
    PAM("", "PAM", PatientManagerAuthorizations.PAM),
    PAM_PDS("", "gazelle.simulator.pam.PatientDemographicSupplier", PatientManagerAuthorizations.PAM),
    PAM_CREATE("/pds/createPatient.seam?initiator=PDS", "Create patient", PatientManagerAuthorizations.PAM),
    PAM_UPDATE("/pds/updatePatient.seam?initiator=PDS", "Update patient", PatientManagerAuthorizations.PAM),
    PAM_CHANGE_ID("/pds/changeId.seam?initiator=PDS", "Change identifier", PatientManagerAuthorizations.PAM),
    PAM_REMOVE_INS("/pds/removeInsIds.seam?initiator=PDS", "Remove INS identifiers", PatientManagerAuthorizations.PAM),
    PAM_LINK("/pds/linkPatient.seam?initiator=PDS", "Link/Unlink patients", PatientManagerAuthorizations.PAM),
    PAM_MERGE("/pds/mergePatient.seam?initiator=PDS", "Merge patients", PatientManagerAuthorizations.PAM),
    PAM_PDC("/pdc/PDC.seam", "gazelle.simulator.pam.PatientDemographicConsumer", PatientManagerAuthorizations.PAM),
    PAM_PES("/pes/pes.seam", "gazelle.simulator.pam.PatientEncounterSupplier", PatientManagerAuthorizations.PAM),
    PAM_PEC("/pec/PEC.seam", "gazelle.simulator.pam.PatientEncounterConsumer", PatientManagerAuthorizations.PAM),
    PAM_AUTOMATON("", "net.ihe.gazelle.simulators.Automation", PatientManagerAuthorizations.PAM_AUTOMATON),
    PAM_AUTOMATON_LOGS("/automaton/pamAutomatonList.seam", "net.ihe.gazelle.simulators.ExecutionLogs", PatientManagerAuthorizations.PAM_AUTOMATON),
    PAM_AUTOMATON_EXEC("/automaton/pamAutomaton.seam", "net.ihe.gazelle.simulators.RunAutomaton", PatientManagerAuthorizations.PAM_AUTOMATON),
    PAM_AUTOMATON_ADMIN("/automaton/listGraphml.seam", "net.ihe.gazelle.simulators.AvailableAutomatedTests", PatientManagerAuthorizations.PAM_AUTOMATON, PatientManagerAuthorizations.ADMIN),
    PAM_AUTOMATON_ADMIN_CREATE("/automaton/createGraphml.seam", "gazelle.pam.AddGraph", PatientManagerAuthorizations.PAM_AUTOMATON, PatientManagerAuthorizations.ADMIN),
    ALL_PDQ("", "PDQ*", getPDQAuthorization()),
    ALL_PDQ_PDC("", "gazelle.simulator.pam.PatientDemographicConsumer", getPDQAuthorization()),
    ALL_PDQ_PDS("", "gazelle.simulator.pam.PatientDemographicSupplier", getPDQAuthorization()),
    ALL_PIX("", "PIX*", getPIXAuthorization()),
    ALL_PIX_CONSUMER("", "gazelle.PAMSimulator.PatientIdentityConsumer", getPIXAuthorization()),
    ALL_PIX_SOURCE("", "gazelle.PAMSimulator.PatientIdentitySource", getPIXSourceAuthorization()),
    PIX_CONSUMER("/pixconsumer/pixQuery.seam", "gazelle.PAMSimulator.ITI9PIXQuery", PatientManagerAuthorizations.PIX),
    PIX_CONSUMER_CONF("/pixconsumer/PIXConsumer.seam", "net.ihe.gazelle.PatientManager.HL7v2Configuration", PatientManagerAuthorizations.PIX),
    ALL_PIX_MANAGER("", "gazelle.PAMSimulator.PatientIdentityCrossReferenceManager", getPIXAuthorization()),
    PIX_MANAGER_ADMIN("/pixmanager/referenceManager.seam?manageIdentities=true", "net.ihe.gazelle.PatientManager.CrossreferencesManagement", getPIXAuthorization()),
    PIX_MANAGER_UPDATE("/pixmanager/referenceManager.seam?manageIdentities=false&amp;transaction=ITI-10", "gazelle.PAMSimulator.ITI10PIXUpdateNotification", PatientManagerAuthorizations.PIX),
    PIX_MANAGER_CONF("/pixmanager/PIXManager.seam", "net.ihe.gazelle.PatientManager.HL7v2Configuration", PatientManagerAuthorizations.PIX),
    PIX_SOURCE("/pes/pes.seam?initiator=PAT_IDENTITY_SRC", "gazelle.PAMSimulator.ITI8PatientIdentityFeed", PatientManagerAuthorizations.PIX),
    PIX_SOURCE_ITI30("/hl7v3/pix/source/iti30.seam", "gazelle.PAMSimulator.ITI30PatientIdentityManagement", PatientManagerAuthorizations.PIX),
    PIX_CREATE("/pds/createPatient.seam?initiator=PAT_IDENTITY_SRC", "Create patient", PatientManagerAuthorizations.PIX),
    PIX_UPDATE("/pds/updatePatient.seam?initiator=PAT_IDENTITY_SRC", "Update patient", PatientManagerAuthorizations.PIX),
    PIX_CHANGE_ID("/pds/changeId.seam?initiator=PAT_IDENTITY_SRC", "Change identifier", PatientManagerAuthorizations.PIX),
    PIX_LINK("/pds/linkPatient.seam?initiator=PAT_IDENTITY_SRC", "Link/Unlink patients", PatientManagerAuthorizations.PIX),
    PIX_MERGE("/pds/mergePatient.seam?initiator=PAT_IDENTITY_SRC", "Merge patients", PatientManagerAuthorizations.PIX),
    PIXV3_SOURCE("/hl7v3/pix/source/iti44.seam", "gazelle.pam.ITI44PatientIdentityFeedHL7V3", PatientManagerAuthorizations.PIXV3),
    PIXV3_SOURCE_ADD("/hl7v3/pix/source/addPatient.seam", "", PatientManagerAuthorizations.PIXV3),
    PIXV3_SOURCE_UPDATE("/hl7v3/pix/source/revisePatient.seam", "", PatientManagerAuthorizations.PIXV3),
    PIXV3_SOURCE_MERGE("/hl7v3/pix/source/mergePatient.seam", "", PatientManagerAuthorizations.PIXV3),
    PIXV3_MANAGER_UPDATE("/pixmanager/referenceManager.seam?managedIdentities=false&amp;transaction=ITI-46", "gazelle.pam.ITI46PIXv3UpdateNotification", PatientManagerAuthorizations.PIXV3),
    PIXV3_MANAGER_CONF("/hl7v3/pix/xmanager.seam", "net.ihe.gazelle.PatientManager.HL7v3Configuration", PatientManagerAuthorizations.PIXV3),
    PIXV3_CONSUMER("/pixconsumer/pixv3Query.seam", "gazelle.pam.ITI45PIXv3Query", PatientManagerAuthorizations.PIXV3),
    PIXV3_CONSUMER_CONF("/hl7v3/pix/xconsumer.seam", "net.ihe.gazelle.PatientManager.HL7v3Configuration", PatientManagerAuthorizations.PIXV3),
    PIXM_CONSUMER("/hl7fhir/pixm/consumer.seam", "[ITI-83] PIXm Query", PatientManagerAuthorizations.PIXM),
    PIXM_MANAGER("/hl7fhir/pixm/supplier.seam", "FHIR Configuration", PatientManagerAuthorizations.PIXM),
    PDQ_SUPPLIER("/pds/PDQPDS.seam", "net.ihe.gazelle.PatientManager.HL7v2Configuration", PatientManagerAuthorizations.PDQ),
    PDQ_CONSUMER("/pdc/PDQPDC.seam", "net.ihe.gazelle.PatientManager.ITI21ITI22PatientDemographicsandVisitsQuery", PatientManagerAuthorizations.PDQ),
    PDQV3_SUPPLIER("/hl7v3/pdq/supplier.seam", "net.ihe.gazelle.PatientManager.HL7v3Configuration", PatientManagerAuthorizations.PDQV3),
    PDQV3_CONSUMER("/hl7v3/pdq/consumer.seam", "net.ihe.gazelle.PatientManager.ITI47PatientDemographicsQueryHL7v3", PatientManagerAuthorizations.PDQV3),
    PQDM_CONSUMER("/hl7fhir/pdqm/consumer.seam", "[ITI-78] Patient Demographics Query FHIR", PatientManagerAuthorizations.PDQM),
    PQDM_SUPPLIER("/hl7fhir/pdqm/supplier.seam", "FHIR Configuration", PatientManagerAuthorizations.PDQM),
    PQDM_CONSUMER_R4("/hl7fhir/pdqm/r4/consumer.seam", "[ITI-78] Patient Demographics Query FHIR (R4)", PatientManagerAuthorizations.PDQM),
    XCPD_INIT_GW("", "Initiating Gateway", PatientManagerAuthorizations.XCPD),
    XCPD_INIT_GW_IHE("/hl7v3/xcpd/initGw.seam", "[ITI-55] Cross Gateway Patient Discovery", PatientManagerAuthorizations.XCPD, PatientManagerAuthorizations.IHE),
    XCPD_INIT_GW_CONF("/hl7v3/xcpd/initGwConf.seam?actorKw=INIT_GW", "Configuration and Deferred queries", PatientManagerAuthorizations.XCPD),
    XCPD_RESP_GW("/hl7v3/xcpd/respGw.seam?actorKw=RESP_GW", "Responding Gateway", PatientManagerAuthorizations.XCPD),
    ALL_SUT("", "gazelle.HL7Common.SystemsConfiguration", PatientManagerAuthorizations.ALL),
    SUT_HL7V2("/sut/sutAsHL7v2Responder.seam", "gazelle.pam.HL7v2Responders", getSUTV2Authorization()),
    SUT_HL7V3("/hl7v3/sut/sutAsHL7v3Responder.seam", "net.ihe.gazelle.PatientManager.HL7v3Responders", getSUTV3Authorization()),
    SUT_FHIR("/sut/sutAsFHIRResponder.seam", "FHIR Responders", getSUTFhirAuthorization()),
    PATIENTS("/patient/allPatients.seam", "gazelle.simulator.pam.AllPatients", PatientManagerAuthorizations.ALL),
    XUA_ALL("", "XUA", PatientManagerAuthorizations.XUA),
    XUA_XSERVICEUSER("/xua/xServiceUser.seam", "X-Service User logs", PatientManagerAuthorizations.XUA),
    XUA_XSERVICEPROVIDER("/xua/xServiceProvider.seam", "X-Service Provider logs", PatientManagerAuthorizations.XUA),
    XUA_LOG_DETAILS("/xua/xuaLogDetails.seam", "XUA Assertion details", PatientManagerAuthorizations.XUA),
    ADMIN_VALUE_SETS("/administration/valueSetManager.seam", "Value Sets", PatientManagerAuthorizations.ALL),
    ADMIN_HL7_RESPONDER_DISPLAY("/administration/displayHL7Responder.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_HL7_RESPONDER_EDIT("/administration/editHL7Responder.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_HL7_RESPONDER_LIST("/administration/responderManager.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_PATIENT_IMPORT("/administration/", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_XUA("/", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_PREFERENCES("/administration/configure.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_TEST_DATA("/testData/list.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_VALIDATORS("/configuration/validationParametersList.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_VALIDATORS_VIEW("/configuration/viewValidationParameters.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_VALIDATORS_EDIT("/configuration/editalidationParameters.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_VALIDATORS_CREATE("/configuration/createValidationParameters.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_ASSIGNING_AUTHORITIES("/administration/assigningAuthorities.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_PATIENT_CREATION("/administration/patientCreation.seam", "", PatientManagerAuthorizations.ADMIN),
    ADMIN_FEATURES("/administration/simulatorFeatures.seam", "Enable/Disable features", PatientManagerAuthorizations.ADMIN),
    ADMIN_STATS("/administration/transactionInstanceStatistics.seam", "Usage statistics", PatientManagerAuthorizations.ALL),
    HL7_MESSAGES("/messages/browser.seam", "gazelle.HL7Common.Hl7Messages", PatientManagerAuthorizations.ALL),
    XCPD("", "XCPD", PatientManagerAuthorizations.XCPD),
    CAT_MAIN("", "Connectathon", PatientManagerAuthorizations.CONNECTATHON),
    CAT_IMPORT("/cat/importFhirPatients.seam", "Import Patient resources", PatientManagerAuthorizations.ADMIN),
    CAT_SHARE("/cat/sharePatients.seam", "Send demographics to your system", PatientManagerAuthorizations.CONNECTATHON),
    CAT_AA("/cat/sutAssigningAuthorities.seam", "SUT's assigning authorities", PatientManagerAuthorizations.ALL),
    CAT_PATIENTS("/patient/allPatients.seam?cat=true", "Patient demographics", PatientManagerAuthorizations.CONNECTATHON),
    CAT_EDIT_SUT_PREF("/cat/editSystemPreferences.seam", "Edit system's preferences", PatientManagerAuthorizations.ALL),
    CAT_CREATE("/administration/patientCreation.seam?actor=CONNECTATHON", "Create patients", PatientManagerAuthorizations.ADMIN),
    MESSAGE_DETAILS("/messages/messageDisplay.seam", "", PatientManagerAuthorizations.ALL),
    HOME("/home.seam", "", PatientManagerAuthorizations.ALL),
    DEFAULT_ERROR_PAGE("/error.seam", "", PatientManagerAuthorizations.ALL),
    VIEW_ENCOUNTER("/encounter.seam", "", PatientManagerAuthorizations.ALL),
    VIEW_PATIENT("/patient.seam", "", PatientManagerAuthorizations.ALL),
    COMPANY_LIST("/messages/companyDetailsList.seam", "Manage companies", PatientManagerAuthorizations.ADMIN),
    ADMIN_STS_EDIT_CREDENTIALS("/sts/editCredentials.seam", "Edit credentials", PatientManagerAuthorizations.ADMIN, PatientManagerAuthorizations.XUA),
    ADMIN_STS_LIST_CREDENTIAL("/sts/list.seam", "Manage STS credentials", PatientManagerAuthorizations.ADMIN, PatientManagerAuthorizations.XUA);

    private String link;

    private Authorization[] authorizations;

    private String label;


    PatientManagerPages(String link, String label, Authorization... authorizations) {
        this.link = link;
        this.authorizations = authorizations;
        this.label = label;
    }

    /** {@inheritDoc} */
    @Override
    public String getId() {
        return name();
    }

    /** {@inheritDoc} */
    @Override
    public String getIcon() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public Authorization[] getAuthorizations() {
        if (authorizations != null) {
            return authorizations.clone();
        } else {
            return null;
        }
    }

    private static Authorization getPDQAuthorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PDQ, PatientManagerAuthorizations.PDQM, PatientManagerAuthorizations.PDQV3);
    }

    private static Authorization getPIXAuthorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PIX, PatientManagerAuthorizations.PIXM, PatientManagerAuthorizations.PIXV3);
    }

    private static Authorization getPIXSourceAuthorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PIX, PatientManagerAuthorizations.PIXV3);
    }

    private static Authorization getSUTV2Authorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PDQ, PatientManagerAuthorizations.PAM, PatientManagerAuthorizations.PIX, PatientManagerAuthorizations.ADT);
    }

    private static Authorization getSUTV3Authorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PDQV3, PatientManagerAuthorizations.XCPD, PatientManagerAuthorizations.PIXV3);
    }

    private static Authorization getSUTFhirAuthorization(){
        return new AuthorizationOr(PatientManagerAuthorizations.PDQM, PatientManagerAuthorizations.PIXM);
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        try {
            return ResourceBundle.instance().getString(this.label);
        }catch (MissingResourceException e){
            return this.label;
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getLink(){
        return link;
    }

    /** {@inheritDoc} */
    @Override
    public String getMenuLink(){
        return link;
    }
}
