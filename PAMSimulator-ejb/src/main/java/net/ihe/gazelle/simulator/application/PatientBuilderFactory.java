package net.ihe.gazelle.simulator.application;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domains;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.text.MessageFormat;
import java.util.Map;

@Name("patientBuilderFactory")
@Scope(ScopeType.APPLICATION)
public class PatientBuilderFactory implements Factory<PatientBuilder, HL7Domain<?>> {
    private HL7Domains domains = new HL7Domains();

    private Map<Class<? extends HL7Domain<?>>, PatientBuilder> generators;

    public PatientBuilderFactory() {
        super();
    }

    @Create
    public void postContruct() {
        this.generators = new THashMap<>();
        try {
            for (Class<? extends HL7Domain<?>> domain : domains) {
                generators.put(domain, (PatientBuilder)Class.forName(PatientBuilder.class.getPackage().getName() + "." + domain.getSimpleName().replace("HL7Domain", "PatientBuilder")).newInstance());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PatientBuilder make(HL7Domain<?> context) {
        PatientBuilder pg = generators.get(context.getClass());
        if (pg==null) {
            throw new RuntimeException(MessageFormat.format("no-patient-builder {0}",context.getClass().getSimpleName()));
        }
        return pg;
    }
}
