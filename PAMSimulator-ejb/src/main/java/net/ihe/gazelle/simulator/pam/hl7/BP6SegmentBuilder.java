package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZBE;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFA;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFD;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFM;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFP;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFS;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFV;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.pam.iti31.model.DRGMovement;
import net.ihe.gazelle.simulator.pam.iti31.model.EncounterAdditionalInfo;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.iti31.model.PatientsPHRInfo;
import net.ihe.gazelle.simulator.pam.model.AdditionalDemographics;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Class Description : </b>BP6SegmentBuilder<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 02/11/15
 */
public class BP6SegmentBuilder {

    /**
     * <p>fillZFDSegment.</p>
     *
     * @param zfd                    a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFD} object.
     * @param additionalDemographics a {@link net.ihe.gazelle.simulator.pam.model.AdditionalDemographics} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillZFDSegment(ZFD zfd, AdditionalDemographics additionalDemographics) throws DataTypeException {
        if (additionalDemographics.getSmsConsent() != null && additionalDemographics.getSmsConsent()) {
            zfd.getSMSConsent().setValue("Y");
        } else {
            zfd.getSMSConsent().setValue("N");
        }
        if (additionalDemographics.getNumberOfWeeksOfGestation() != null && additionalDemographics.getNumberOfWeeksOfGestation() > 0) {
            zfd.getNumberOfWeeksOfGestation().setValue(additionalDemographics.getNumberOfWeeksOfGestation().toString());
        }
        if (additionalDemographics.getWeekInMonth() != null) {
            zfd.getLunarDate().getNa1_Value1().setValue(additionalDemographics.getWeekInMonth().getDay().toString());
            if (additionalDemographics.getMonth() != null && additionalDemographics.getMonth() > 0) {
                zfd.getLunarDate().getNa2_Value2().setValue(additionalDemographics.getMonth().toString());
            }
            if (additionalDemographics.getYearOfBirth() != null && additionalDemographics.getYearOfBirth() > 0) {
                zfd.getLunarDate().getNa3_Value3().setValue(additionalDemographics.getYearOfBirth().toString());
            }
        }
        zfd.getDateDeFinDeValiditéDuDocument().getTime().setValue(additionalDemographics.getDocumentExpirationDate());
        zfd.getDateDInterrogationDuTéléserviceINSi().getTime().setValue(additionalDemographics.getDateOfTheInsiWebserviceRequest());
        zfd.getModeDObtentionDeLIdentité().setValue(additionalDemographics.getIdentityAcquisitionMode());
        zfd.getTypeDeJustificatifDIdentité().setValue(additionalDemographics.getProofOfIdentity());
        if (additionalDemographics.getDateOfBirthCorrected() != null && additionalDemographics.getDateOfBirthCorrected()) {
            zfd.getIndicateurDeDateDeNaissanceCorrigée().setValue("Y");
        } else {
            zfd.getIndicateurDeDateDeNaissanceCorrigée().setValue("N");
        }
    }

    /**
     * <p>fillZFPSegment.</p>
     *
     * @param zfp                    a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFP} object.
     * @param additionalDemographics a {@link net.ihe.gazelle.simulator.pam.model.AdditionalDemographics} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillZFPSegment(ZFP zfp, AdditionalDemographics additionalDemographics) throws DataTypeException {
        if (additionalDemographics.getSocioProfessionalGroup() != null) {
            zfp.getSocioProfessionalGroup().setValue(additionalDemographics.getSocioProfessionalGroup());
        }
        if (additionalDemographics.getSocioProfessionalOccupation() != null) {
            zfp.getSocioProfessionalOccupation().setValue(additionalDemographics.getSocioProfessionalOccupation());
        }
    }

    /**
     * <p>fillZFASegment.</p>
     *
     * @param zfa  a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFA} object.
     * @param info a {@link net.ihe.gazelle.simulator.pam.iti31.model.PatientsPHRInfo} object.
     * @param sdf  a {@link java.text.SimpleDateFormat} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillZFASegment(ZFA zfa, PatientsPHRInfo info, SimpleDateFormat sdf) throws DataTypeException {
        if (info.getStatus() != null && !info.getStatus().isEmpty()) {
            zfa.getPatientSPHRStatus().setValue(info.getStatus());
        }
        if (info.getStatusCollectionDate() != null) {
            zfa.getPatientSPHRStatusCollectionDate().getTime().setValue(formatDate(sdf, info.getStatusCollectionDate()));
        }
        if (info.getClosingDate() != null) {
            zfa.getPatientSPHRClosingDate().getTime().setValue(formatDate(sdf, info.getClosingDate()));
        }
        zfa.getValidAccessAuthorizationToThePatientSPHRGrantedToTheFacility().setValue(getYesNoIndicator(info.getFacilityAccess()));
        if (info.getFacilityAccessCollectionDate() != null) {
            zfa.getCollectionDateOfTheStatusOfTheFacilitySAccessAuthorizationToThePatientSPHR().getTime().setValue(formatDate(sdf, info.getOppositionCollectionDate()));
        }
        zfa.getZfa6_OppositionOfThePatientToTheBrisDeGlaceModeAccessModeAccessSee1stnote().setValue(getYesNoIndicator(info.getBrisDeGlace()));
        zfa.getOppositionOfThePatientToTheCentre15ModeAccess().setValue(getYesNoIndicator(info.getCentre15()));
        if (info.getOppositionCollectionDate() != null) {
            zfa.getOppositionOfThePatientToTheBrisDeGlaceModeAccessModeAccessSee1stnote().setValue(formatDate(sdf, info.getOppositionCollectionDate()));
        }
    }

    /**
     * <p>fillZFMSegment.</p>
     *
     * @param zfm a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFM} object.
     * @param drg a {@link net.ihe.gazelle.simulator.pam.iti31.model.DRGMovement} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillZFMSegment(ZFM zfm, DRGMovement drg) throws DataTypeException {
        if (drg.getPmsiAdmissionMode() != null) {
            zfm.getPMSIAdmissionMode().setValue(drg.getPmsiAdmissionMode());
        }
        if (drg.getPmsiDischargeMode() != null) {
            zfm.getPMSIDischargeMode().setValue(drg.getPmsiDischargeMode());
        }
        if (drg.getPmsiEstablishmentOfOriginMode() != null) {
            zfm.getPMSIEstblishmentOfOriginMode().setValue(drg.getPmsiEstablishmentOfOriginMode());
        }
        if (drg.getPmsiDestinationMode() != null) {
            zfm.getPMSIDestinationMode();
        }
    }

    /**
     * <p>fillZFVSegment.</p>
     *
     * @param zfv  a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFV} object.
     * @param info a {@link net.ihe.gazelle.simulator.pam.iti31.model.EncounterAdditionalInfo} object.
     * @param sdf  a {@link java.text.SimpleDateFormat} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public static void fillZFVSegment(ZFV zfv, EncounterAdditionalInfo info, SimpleDateFormat sdf) throws HL7Exception {
        if (info.getEstablishmentOfOrigin() != null) {
            zfv.getEstablishmentOfOriginAndDateOfTheLastVisitToThisFacility().getDischargeLocation().setValue(info.getEstablishmentOfOrigin());
        }
        if (info.getLastVisitDate() != null) {
            zfv.getEstablishmentOfOriginAndDateOfTheLastVisitToThisFacility().getEffectiveDate().getTime().setValue(formatDate(sdf, info.getLastVisitDate()));
        }
        if (info.getDischargeMode() != null) {
            zfv.getPersonalizedDischargeMode().setValue(info.getDischargeMode());
        }
        if (info.getPlacementStartDate() != null) {
            zfv.getPlacementStartingDatePsy().getTime().setValue(formatDate(sdf, info.getPlacementStartDate()));
        }
        if (info.getPlacementEndDate() != null) {
            zfv.getPlacementEndingDatePsy().getTime().setValue(formatDate(sdf, info.getPlacementEndDate()));
        }
        int index = 0;
        if (info.getEstablishmentOfOriginAddress() != null && !info.getEstablishmentOfOriginAddress().isEmpty()) {
            zfv.getEstablishmentOfOriginOrDestinationEstablishmentAddress(index).parse(info.getEstablishmentOfOriginAddress());
            zfv.getEstablishmentOfOriginOrDestinationEstablishmentAddress(index).getAddressType().setValue("ORI");
            index++;
        }
        if (info.getEstablishmentOfDestinationAddress() != null && !info.getEstablishmentOfDestinationAddress().isEmpty()) {
            zfv.getEstablishmentOfOriginOrDestinationEstablishmentAddress(index).parse(info.getEstablishmentOfDestinationAddress());
            zfv.getEstablishmentOfOriginOrDestinationEstablishmentAddress(index).getAddressType().setValue("DST");
        }
        if (info.getOriginAccountNumber() != null) {
            zfv.getEstablishmentOfOriginAccountNumber().parse(info.getOriginAccountNumber());
        }
        if (info.getTransportMode() != null) {
            Concept mode = ValueSet.getConceptForCode("HL70430", info.getTransportMode());
            zfv.getDischargeTransportMode().getIdentifier().setValue(info.getTransportMode());
            zfv.getDischargeTransportMode().getText().setValue(mode.getDisplayName());
            zfv.getDischargeTransportMode().getNameOfCodingSystem().setValue("HL70430");
        }
        if (info.getLegalCareMode() != null) {
            zfv.getLegalCareModeRIMPCodeTransmittedInThePV23().setValue(info.getLegalCareMode());
        }
        if (info.getCareDuringTransport() != null) {
            Concept care = ValueSet.getConceptForCode("HL73306", info.getCareDuringTransport());
            zfv.getCareDuringTransport().getIdentifier().setValue(info.getCareDuringTransport());
            zfv.getCareDuringTransport().getText().setValue(care.getDisplayName());
        }
    }

    /**
     * fill a single instance of the ZFS segment based on the content of the LegalCareMode object
     *
     * @param zfs
     * @param legalCareMode
     * @throws HL7Exception
     */
    public static void fillZFSSegment(ZFS zfs, LegalCareMode legalCareMode) throws HL7Exception {
        zfs.getSetIDZFS().setValue(legalCareMode.getSetId().toString());
        zfs.getLegalModeOfCareStartDateAndHour().getTime().setValue(legalCareMode.getCareStartDateTime());
        if (legalCareMode.getCareEndDateTime() != null) {
            zfs.getLegalModeOfCareEndDateAndHour().getTime().setValue(legalCareMode.getCareEndDateTime());
        }
        zfs.getLegalModeOfCareIdentifier().parse(legalCareMode.getIdentifier());
        if (legalCareMode.getComment() != null) {
            zfs.getComment().setValue(legalCareMode.getComment());
        }
        if (legalCareMode.getRimpCode() != null) {
            zfs.getLegalModeOfCareRIMPCode().getIdentifier().setValue(legalCareMode.getRimpCode());
            // datatype is CNE: fetch displayname and code system from SVS
            Concept rimp = ValueSet.getConceptForCode("HL73302", legalCareMode.getRimpCode());
            if (rimp != null) {
                zfs.getLegalModeOfCareRIMPCode().getText().setValue(rimp.getDisplayName());
                zfs.getLegalModeOfCareRIMPCode().getNameOfCodingSystem().setValue(rimp.getCodeSystemName());
            }
        }
        zfs.getLegalModeOfCareAction().setValue(legalCareMode.getAction().getName());
        if (legalCareMode.getMode() != null) {
            zfs.getLegalModeOfCare().getIdentifier().setValue(legalCareMode.getMode());
            // datatype is S=CWE: fetch displayname and code system from SVS
            Concept mode = ValueSet.getConceptForCode("HL79110", legalCareMode.getRimpCode());
            if (mode != null) {
                zfs.getLegalModeOfCare().getText().setValue(mode.getDisplayName());
                zfs.getLegalModeOfCare().getNameOfCodingSystem().setValue(mode.getCodeSystemName());
            }
        }
    }

    private static String formatDate(SimpleDateFormat sdf, Date date) {
        return sdf.format(date);
    }

    private static String getYesNoIndicator(Boolean condition) {
        if (condition == null) {
            return "N";
        } else {
            return condition ? "Y" : "N";
        }
    }

    /**
     * <p>fillZBE.</p>
     *
     * @param zbe                a {@link net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZBE} object.
     * @param careResponsability a {@link java.lang.String} object.
     * @param natureOfMovement   a {@link java.lang.String} object.
     * @param facility           a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public static void fillZBE(ZBE zbe, String careResponsability, String natureOfMovement, String facility) throws HL7Exception {
        if (natureOfMovement != null) {
            Concept nature = ValueSet.getConceptForCode("HL79106", natureOfMovement);
            zbe.getNatureOfThisMovement().getIdentifier().setValue(natureOfMovement);
            if (nature != null) {
                zbe.getNatureOfThisMovement().getText().setValue(nature.getDisplayName());
            }
        }
        if (careResponsability != null) {
            zbe.getResponsibleWardCare().getOrganizationName().setValue(ValueSet.getDisplayNameForCode("NURSING_WARD", careResponsability));
            zbe.getResponsibleWardCare().getIdentifierTypeCode().setValue("UF");
            if (facility != null) {
                zbe.getResponsibleWardCare().getAssigningAuthority().getNamespaceID().setValue(facility);
            }
            zbe.getResponsibleWardCare().getOrganizationIdentifier().setValue(careResponsability);
        }
        if (!zbe.getResponsibleWardMedical().isEmpty()) {
            zbe.getResponsibleWardMedical().getIdentifierTypeCode().setValue("UF");
            if (facility != null) {
                zbe.getResponsibleWardMedical().getAssigningAuthority().getNamespaceID().setValue(facility);
            }
        }
    }
}
