package net.ihe.gazelle.simulator.xcpd.model;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <b>Class Description : </b>DeferredTransaction<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/01/16
 */
@Entity
@Name("deferredTransaction")
@Table(name = "pam_xcpd_deferred_transaction", schema = "public")
@SequenceGenerator(name = "pam_xcpd_deferred_transaction_sequence", sequenceName = "pam_xcpd_deferred_transaction_id_seq", allocationSize = 1)
public class DeferredTransaction implements Serializable {

    @Id
    @GeneratedValue(generator = "pam_xcpd_deferred_transaction_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @JoinColumn(name = "simulated_actor_id")
    @ManyToOne
    private Actor simulatedActor;

    @JoinColumn(name = "query_id")
    @OneToOne
    private TransactionInstance query;

    @JoinColumn(name = "response_id")
    @OneToOne
    private TransactionInstance response;

    @Column(name = "status")
    private DeferredTransactionStatus status;

    @Column(name = "message_id_root")
    private String messageIdRoot;

    @Column(name = "message_id_extension")
    private String messageIdExtension;

    @Column(name = "init_gw_endpoint")
    private String initGatewayEndpoint;

    @Column(name = "discarded_reason")
    private String discardedRreason;

    /**
     * <p>Constructor for DeferredTransaction.</p>
     */
    public DeferredTransaction(){

    }

    /**
     * <p>Constructor for DeferredTransaction.</p>
     *
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param query a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @param url a {@link java.lang.String} object.
     */
    public DeferredTransaction(Actor actor, TransactionInstance query, String url){
        this.simulatedActor = actor;
        this.query = query;
        this.initGatewayEndpoint = url;
        this.status = DeferredTransactionStatus.WAITING;
    }

    /**
     * <p>save.</p>
     */
    public void save(){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(this);
        entityManager.flush();
    }

    /**
     * <p>Getter for the field <code>discardedRreason</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDiscardedRreason() {
        return discardedRreason;
    }

    /**
     * <p>Setter for the field <code>discardedRreason</code>.</p>
     *
     * @param discardedRreason a {@link java.lang.String} object.
     */
    public void setDiscardedRreason(String discardedRreason) {
        this.discardedRreason = discardedRreason;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>query</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getQuery() {
        return query;
    }

    /**
     * <p>Setter for the field <code>query</code>.</p>
     *
     * @param query a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void setQuery(TransactionInstance query) {
        this.query = query;
    }

    /**
     * <p>Getter for the field <code>response</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getResponse() {
        return response;
    }

    /**
     * <p>Setter for the field <code>response</code>.</p>
     *
     * @param response a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void setResponse(TransactionInstance response) {
        this.response = response;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionStatus} object.
     */
    public DeferredTransactionStatus getStatus() {
        return status;
    }

    /**
     * <p>Setter for the field <code>status</code>.</p>
     *
     * @param status a {@link net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionStatus} object.
     */
    public void setStatus(DeferredTransactionStatus status) {
        this.status = status;
    }

    /**
     * <p>Getter for the field <code>messageIdRoot</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageIdRoot() {
        return messageIdRoot;
    }

    /**
     * <p>Setter for the field <code>messageIdRoot</code>.</p>
     *
     * @param messageIdRoot a {@link java.lang.String} object.
     */
    public void setMessageIdRoot(String messageIdRoot) {
        this.messageIdRoot = messageIdRoot;
    }

    /**
     * <p>Getter for the field <code>messageIdExtension</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageIdExtension() {
        return messageIdExtension;
    }

    /**
     * <p>Setter for the field <code>messageIdExtension</code>.</p>
     *
     * @param messageIdExtension a {@link java.lang.String} object.
     */
    public void setMessageIdExtension(String messageIdExtension) {
        this.messageIdExtension = messageIdExtension;
    }

    /**
     * <p>Getter for the field <code>initGatewayEndpoint</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getInitGatewayEndpoint() {
        return initGatewayEndpoint;
    }

    /**
     * <p>Setter for the field <code>initGatewayEndpoint</code>.</p>
     *
     * @param initGatewayEndpoint a {@link java.lang.String} object.
     */
    public void setInitGatewayEndpoint(String initGatewayEndpoint) {
        this.initGatewayEndpoint = initGatewayEndpoint;
    }

    /**
     * <p>setStatusFromAckCode.</p>
     *
     * @param ackCode a {@link java.lang.String} object.
     */
    public void setStatusFromAckCode(String ackCode){
        if (ackCode == null){
            setStatus(DeferredTransactionStatus.WAITING);
        } else if (ackCode.equals("NS250")){
            setStatus(DeferredTransactionStatus.OPTION_NOT_SUPPORTED);
        } else {
            setStatus(DeferredTransactionStatus.DISCARDED);
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o){
        return HibernateHelper.getLazyEquals(this, o);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode(){
        return HibernateHelper.getLazyHashcode(this);
    }
}

