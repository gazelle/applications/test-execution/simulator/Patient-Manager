package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.datatypes.TEL;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01QueryAck;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import org.jboss.seam.faces.FacesMessages;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class PRPAIN201306UV02Parser extends HL7v3MessageParser {

    private FacesMessages facesMessages;
    private boolean moreResultsAvailable;

    public PRPAIN201306UV02Parser(FacesMessages inFacesMessages) {
        this.facesMessages = inFacesMessages;
        this.moreResultsAvailable = false;
    }

    /**
     * Builds a list of patient from the ControlActProcess of PRPA_IN06UV02 message
     *
     * @param message
     * @return the list of patients created from the data contained in the message
     * @throws JAXBException
     */
    public List<Patient> extractPatients(byte[] message) throws JAXBException {
        List<Patient> receivedPatients = null;
        if (message != null) {
            String messageString = new String(message, StandardCharsets.UTF_8);
            messageString = messageString.replace("UTF8", "UTF-8");
            PRPAIN201306UV02Type hl7v3Response = HL7V3Transformer.unmarshallMessage(PRPAIN201306UV02Type.class,
                    new ByteArrayInputStream(messageString.getBytes(StandardCharsets.UTF_8)));
            messageId = hl7v3Response.getId();
            String responseCode = hl7v3Response.getControlActProcess().getQueryAck().getQueryResponseCode().getCode();
            if (responseCode.equals(HL7V3ResponseCode.OK.getValue())) {
                receivedPatients = new ArrayList<Patient>();
                facesMessages.add("This response contains " + hl7v3Response.getControlActProcess().getSubject().size()
                        + " matches");
                for (PRPAIN201306UV02MFMIMT700711UV01Subject1 subject : hl7v3Response.getControlActProcess()
                        .getSubject()) {
                    Patient patient = new Patient();
                    PRPAIN201306UV02MFMIMT700711UV01Subject2 subject1 = subject.getRegistrationEvent().getSubject1();
                    PRPAMT201310UV02Patient hl7v3Patient = subject1.getPatient();
                    PRPAMT201310UV02Person patientPerson = hl7v3Patient.getPatientPerson();
                    List<II> ids = hl7v3Patient.getId();
                    if (!ids.isEmpty()) {
                        patient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
                        for (II id : ids) {
                            patient.getPatientIdentifiers().add(
                                    new PatientIdentifier(id.getExtension() + "^^^&" + id.getRoot(), "PI"));
                        }
                    }
                    setGenderCode(patientPerson.getAdministrativeGenderCode(), patient);
                    setBirthTime(patientPerson.getBirthTime(), patient);
                    setReligionCode(patientPerson.getReligiousAffiliationCode(), patient);
                    setPatientPhoneNumbers(patientPerson.getTelecom(), patient);
                    setPatientAddresses(patientPerson.getAddr(), patient);
                    setRaceCode(patientPerson.getRaceCode(), patient);
                    setPatientNames(patientPerson.getName(), patient);
                    setMultipleBirthInd(patientPerson.getMultipleBirthInd(), patient);
                    setMultipleBirthOrder(patientPerson.getMultipleBirthOrderNumber(), patient);
                    receivedPatients.add(patient);
                    setIsPatientDead(patientPerson.getDeceasedInd(), patient);
                    setPatientDeathTime(patientPerson.getDeceasedTime(), patient);
                }
                MFMIMT700711UV01QueryAck queryAck = hl7v3Response.getControlActProcess().getQueryAck();
                if (queryAck.getResultRemainingQuantity() != null) {
                    Integer remainingQuantity = queryAck.getResultRemainingQuantity().getValue();
                    moreResultsAvailable = (remainingQuantity != null && remainingQuantity > FIRST_OCCURRENCE);
                    facesMessages.add(remainingQuantity + " additional patient(s) available on supplier side");
                }
            } else if (responseCode.equals(HL7V3ResponseCode.NF.getValue())) {
                facesMessages.add("No match found for this query");
            } else if (responseCode.equals(HL7V3ResponseCode.QE.getValue())) {
                facesMessages
                        .add("QE code received : your query is not correct, check that at least one parameter is set");
            } else {
                facesMessages.add("Unknown query response code received : " + responseCode);
            }
        } else {
            facesMessages.add("No response received from the supplier");
        }
        return receivedPatients;
    }


    public boolean isMoreResultsAvailable() {
        return moreResultsAvailable;
    }

    public Boolean isCancelAccept(byte[] response) throws JAXBException {
        if (response == null) {
            return false;
        } else {
            String messageString = new String(response, StandardCharsets.UTF_8);
            messageString = messageString.replace("UTF8", "UTF-8");
            MCCIIN000002UV01Type ackMessage = HL7V3Transformer.unmarshallMessage(MCCIIN000002UV01Type.class,
                    new ByteArrayInputStream(messageString.getBytes(StandardCharsets.UTF_8)));
            return (!ackMessage.getAcknowledgement().isEmpty()
                    && ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode() != null
                    && ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode().getCode() != null && ackMessage
                    .getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode().getCode().equals(HL7V3AcknowledgmentCode.AA.getValue()));
        }
    }
}
