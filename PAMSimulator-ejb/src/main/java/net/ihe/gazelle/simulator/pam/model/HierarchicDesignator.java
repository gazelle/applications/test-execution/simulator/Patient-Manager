package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Name("patientIdentifierDomain")
@Table(name = "pam_hierarchic_designator", schema = "public")
@SequenceGenerator(name = "pam_hierarchic_designator_sequence", sequenceName = "pam_hierarchic_designator_id_seq", allocationSize = 1)
public class HierarchicDesignator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String DEFAULT_UNIVERSAL_ID_TYPE = "ISO";
    private static final String URN_PREFIX = "urn:oid:";
    private static final int INCREMENT = 1;

    @Id
    @GeneratedValue(generator = "pam_hierarchic_designator_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    /**
     * HD-1
     */
    @Column(name = "namespace_id")
    private String namespaceID;

    /**
     * HD-2
     */
    @Column(name = "universal_id")
    private String universalID;

    /**
     * HD-3
     */
    @Column(name = "universal_id_type")
    private String universalIDType;

    /**
     * populates CX-5
     */
    @Column(name = "type")
    private String type;

    /**
     * if true, can be used by the user to generate identifiers
     */
    @Column(name = "is_tool_assigning_authority")
    private boolean toolAssigningAuthority;

    /**
     * used to build the identifiers in this domain
     */
    @Column(name = "index")
    private Integer index;

    /**
     * default assigning authorities will be preselected in the GUI for identifier creation
     */
    @Column(name = "is_default")
    private boolean defaultAssigningAuthority;

    @Column(name = "cat_usage")
    private boolean catUsage;

    /**
     * prefix used for generating Identifiers
     */
    @Column(name = "prefix")
    private String prefix;

    @Column(name = "usage")
    @NotNull
    private DesignatorType usage;

    public HierarchicDesignator(DesignatorType inUsage) {
        this.defaultAssigningAuthority = false;
        this.toolAssigningAuthority = false;
        this.usage = inUsage;
        this.catUsage = false;
        this.setUniversalIDType("ISO");
    }

    public HierarchicDesignator() {
        this.defaultAssigningAuthority = false;
        this.toolAssigningAuthority = false;
        this.catUsage = false;
    }

    public HierarchicDesignator(String namespaceID, String universalID, String universalIDType, DesignatorType hdType) {
        this.namespaceID = namespaceID;
        this.universalID = universalID;
        this.universalIDType = universalIDType;
        if (hdType != null) {
            this.type = hdType.getIdentifierTypeCode();
        }
        this.toolAssigningAuthority = false;
        this.index = 0;
        this.defaultAssigningAuthority = false;
        this.usage = hdType;
        if (universalIDType == null || universalIDType.isEmpty()) {
            this.universalIDType = DEFAULT_UNIVERSAL_ID_TYPE;
        }
        this.catUsage = false;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public boolean isDefaultAssigningAuthority() {
        return defaultAssigningAuthority;
    }

    public void setDefaultAssigningAuthority(boolean defaultAssigningAuthority) {
        this.defaultAssigningAuthority = defaultAssigningAuthority;
    }

    /**
     * Merges instance is database
     *
     * @param entityManager
     * @param checkUnicity
     * @return the saved instance of the hierarchicDesignator
     */
    public HierarchicDesignator save(EntityManager entityManager, boolean checkUnicity) throws Exception {
        if (!checkUnicity) {
            return save(entityManager);
        } else if (checkUnicity(this)) {
            return save(entityManager);
        } else {
            throw new Exception("Another assigning authority exists with the same characteristics");
        }
    }

    private HierarchicDesignator save(EntityManager entityManager) {
        HierarchicDesignator domain = entityManager.merge(this);
        entityManager.flush();
        return domain;
    }

    public static boolean checkUnicity(HierarchicDesignator hd) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.namespaceID().eq(hd.getNamespaceID());
        query.universalIDType().eq(hd.getUniversalIDType());
        query.universalID().eq(hd.getUniversalID());
        query.usage().eq(hd.getUsage());
        if (hd.getId() != null) {
            query.id().neq(hd.getId());
        }
        List<HierarchicDesignator> list = query.getList();
        return (list.isEmpty());
    }

    public static List<HierarchicDesignator> getDomainFiltered(String namespaceID, String universalID,
                                                               String universalIDType, EntityManager entityManager, DesignatorType hdType) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery(new HQLQueryBuilder<HierarchicDesignator>(
                entityManager, HierarchicDesignator.class));
        if ((namespaceID != null) && !namespaceID.isEmpty()) {
            query.namespaceID().eq(namespaceID);
        }
        if ((universalID != null) && !universalID.isEmpty()) {
            query.universalID().eq(universalID);
        }
        if ((universalIDType != null) && !universalIDType.isEmpty()) {
            query.universalIDType().eq(universalIDType);
        }
        if (hdType != null) {
            query.usage().eq(hdType);
        }
        return query.getList();
    }

    public static HierarchicDesignator getExactDomain(String namespaceID, String universalID,
                                                               String universalIDType, EntityManager entityManager, DesignatorType hdType) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery(new HQLQueryBuilder<HierarchicDesignator>(
                entityManager, HierarchicDesignator.class));
        query.namespaceID().eq(namespaceID);
        query.universalID().eq(universalID);
        query.universalIDType().eq(universalIDType);
        query.usage().eq(hdType);
        return query.getUniqueResult();
    }

    public static HierarchicDesignator getDomainAndCreateItIfMissing(String namespaceID, String universalID,
                                                                     String universalIDType, EntityManager entityManager, DesignatorType hdType, String identifierTypeCode) {
        try {
            HierarchicDesignator domain = new HierarchicDesignator(namespaceID, universalID, universalIDType, hdType);
            if (identifierTypeCode != null && !identifierTypeCode.isEmpty()) {
                domain.setType(identifierTypeCode);
            }
            return domain.save(entityManager, true);
        } catch (Exception e) {
            return getExactDomain(namespaceID, universalID, universalIDType, entityManager, hdType);
        }
    }

    public String getNamespaceID() {
        return namespaceID;
    }

    public void setNamespaceID(String namespaceID) {
        this.namespaceID = namespaceID;
    }

    public String getUniversalID() {
        return universalID;
    }

    public String getUniversalIDAsUrn() {
        if (universalID == null) {
            return null;
        } else if (universalID.startsWith(URN_PREFIX)) {
            return universalID;
        } else {
            return URN_PREFIX + universalID;
        }
    }

    public void setUniversalID(String universalID) {
        this.universalID = universalID;
    }

    public String getUniversalIDType() {
        return universalIDType;
    }

    public void setUniversalIDType(String universalIDType) {
        this.universalIDType = universalIDType;
    }

    /**
     * returns the current object as an encoding HD datatype (uses ^ as components separator)
     */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        if (this.namespaceID != null) {
            string.append(this.namespaceID);
        }
        string.append("^");
        if (this.universalID != null) {
            string.append(universalID);
        }
        string.append("^");
        if (this.universalIDType != null) {
            string.append(universalIDType);
        }
        return string.toString();
    }

    public String nextId() {
        setIndex(index + INCREMENT);
        try {
            save(EntityManagerService.provideEntityManager(), false);
        } catch (Exception e) {
            // must never been thrown in that case
        }
        if (prefix == null) {
            return index.toString();
        } else {
            return prefix + index;
        }
    }

    public String displayWithAmp() {
        String string = toString();
        string = string.replace('^', '&');
        return string.concat(" (" + type + ')');
    }

    public boolean isToolAssigningAuthority() {
        return toolAssigningAuthority;
    }

    public void setToolAssigningAuthority(boolean toolAssigningAuthority) {
        this.toolAssigningAuthority = toolAssigningAuthority;
        if (!this.toolAssigningAuthority) {
            setCatUsage(false);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HierarchicDesignator)) {
            return false;
        }

        HierarchicDesignator that = (HierarchicDesignator) o;

        if (namespaceID != null ? !namespaceID.equals(that.namespaceID) : that.namespaceID != null) {
            return false;
        }
        if (universalID != null ? !universalID.equals(that.universalID) : that.universalID != null) {
            return false;
        }
        if (universalIDType != null ? !universalIDType.equals(that.universalIDType) : that.universalIDType != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        return prefix != null ? prefix.equals(that.prefix) : that.prefix == null;

    }

    @Override
    public int hashCode() {
        int result = namespaceID != null ? namespaceID.hashCode() : 0;
        result = 31 * result + (universalID != null ? universalID.hashCode() : 0);
        result = 31 * result + (universalIDType != null ? universalIDType.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (prefix != null ? prefix.hashCode() : 0);
        return result;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public DesignatorType getUsage() {
        return usage;
    }

    public void setUsage(DesignatorType usage) {
        this.usage = usage;
    }

    public boolean isEmpty() {
        return hashCode() == 0;
    }

    public boolean isCatUsage() {
        return catUsage;
    }

    public void setCatUsage(boolean catUsage) {
        this.catUsage = catUsage;
    }
}
