package net.ihe.gazelle.simulator.testdata.model;

import net.ihe.gazelle.simulator.pam.model.Patient;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


/**
 * Created by aberge on 08/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("patientTestData")
@DiscriminatorValue("patient")
public class PatientTestData extends TestData{

    @OneToOne(targetEntity = Patient.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    /**
     * <p>Constructor for PatientTestData.</p>
     */
    public PatientTestData(){
        super();
    }

    /**
     * <p>Constructor for PatientTestData.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public PatientTestData(Patient patient) {
        this.patient = patient;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

}
