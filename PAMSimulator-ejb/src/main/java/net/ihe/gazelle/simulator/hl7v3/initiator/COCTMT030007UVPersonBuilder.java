package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson;
import net.ihe.gazelle.hl7v3.datatypes.EnGiven;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.voc.EntityClass;
import net.ihe.gazelle.hl7v3.voc.XDeterminerInstanceKind;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.pam.model.Person;

/**
 * Created by aberge on 18/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class COCTMT030007UVPersonBuilder extends HL7v3MessageBuilder{

	/**
	 * <p>buildMotherMaidenName.</p>
	 *
	 * @param familyName a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson} object.
	 */
	public static COCTMT030007UVPerson buildMotherMaidenName(String familyName){
		COCTMT030007UVPerson holder = new COCTMT030007UVPerson();
		holder.setClassCode(EntityClass.PSN);
		holder.setDeterminerCode(XDeterminerInstanceKind.INSTANCE);
		holder.addName(buildPersonName(null, familyName, null));
		return holder;
	}

	/**
	 * <p>buildHolderForRelationship.</p>
	 *
	 * @param relative a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson} object.
	 */
	public static COCTMT030007UVPerson buildHolderForRelationship(Person relative) {
		COCTMT030007UVPerson personHolder = new COCTMT030007UVPerson();
		personHolder.setClassCode(EntityClass.PSN);
		personHolder.setDeterminerCode(XDeterminerInstanceKind.INSTANCE);
		PN name = buildPersonName(relative.getFirstName(), relative.getLastName(), null);
		if (relative.getSecondName() != null && !relative.getSecondName().isEmpty())
		{
			EnGiven second = new EnGiven();
			second.addMixed(relative.getSecondName());
			name.addGiven(second);
		}
		personHolder.addName(name);
		return personHolder;
	}
}
