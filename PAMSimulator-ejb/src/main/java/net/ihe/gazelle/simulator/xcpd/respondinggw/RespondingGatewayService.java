package net.ihe.gazelle.simulator.xcpd.respondinggw;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.xcpd.common.XCPDQueryHandler;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryRequestType;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryResponseType;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.Addressing;


@Stateless
/**
 * <p>RespondingGatewayService class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("XCPDRespondingGatewayService")
@WebService(portName="RespondingGateway_Port_Soap12", name="RespondingGateway_PortType", targetNamespace= "urn:ihe:iti:xcpd:2009", serviceName="RespondingGateway_Service")
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
@Addressing(enabled = true, required = true)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@GenerateInterface(value="RespondingGatewayServiceRemote", isLocal = false, isRemote = true)
@HandlerChain(file = "soap-handler.xml")
public class RespondingGatewayService implements RespondingGatewayServiceRemote{

    private static final String RESPGW_KEYWORD = "RESP_GW";
    private static final String INITGW_KEYWORD = "INIT_GW";
    private static final String ITI55_KEYWORD = "ITI-55";
    private static Logger log = LoggerFactory.getLogger(RespondingGatewayService.class);
    
    @Resource
    private WebServiceContext context;


	@WebMethod(operationName = "RespondingGateway_PRPA_IN201305UV02", action = "urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery")
	@WebResult(name = "PRPA_IN201306UV02", partName = "body", targetNamespace = "urn:hl7-org:v3")
	@Action(input = "urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery", output = "urn:hl7-org:v3:PRPA_IN201306UV02:CrossGatewayPatientDiscovery")
	public PRPAIN201306UV02Type crossGatewayPatientDiscovery(@WebParam(name="PRPA_IN201305UV02", partName = "body",
            targetNamespace = "urn:hl7-org:v3") PRPAIN201305UV02Type request) {
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		XCPDQueryHandler handler = new XCPDQueryHandler(domain,
        Actor.findActorWithKeyword(RESPGW_KEYWORD), Actor.findActorWithKeyword(INITGW_KEYWORD),
                Transaction.GetTransactionByKeyword(ITI55_KEYWORD), getServletRequest(), context.getMessageContext());
        try {
            return handler.handleCrossGatewayPatientDiscoveryQuery(request, true, domain);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
	}


	@WebMethod(operationName = "RespondingGateway_PatientLocationQuery", action = "urn:ihe:iti:xcpd:2009:PatientLocationQuery")
	@WebResult(name = "PatientLocationQueryResponse", partName = "body", targetNamespace = "urn:ihe:iti:xcpd:2009")
	@Action(input = "urn:ihe:iti:xcpd:2009:PatientLocationQuery", output = "urn:ihe:iti:xcpd:2009:PatientLocationQueryResponse")
	public PatientLocationQueryResponseType patientLocationQuery(@WebParam(name="PatientLocationQueryRequest", partName = "body",
            targetNamespace = "urn:ihe:iti:xcpd:2009") PatientLocationQueryRequestType request) {
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
        XCPDQueryHandler handler = new XCPDQueryHandler(domain,
                Actor.findActorWithKeyword(RESPGW_KEYWORD), Actor.findActorWithKeyword(INITGW_KEYWORD),
                Transaction.GetTransactionByKeyword(ITI55_KEYWORD), getServletRequest(), context.getMessageContext());
        try {
            return handler.handlePatientLocationQuery(request);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
	}
	

	@WebMethod(operationName = "RespondingGateway_Deferred_PRPA_IN201305UV02", action = "urn:hl7-org:v3:PRPA_IN201305UV02:Deferred:CrossGatewayPatientDiscovery")
	@WebResult(name = "MCCI_IN000002UV01", partName = "body", targetNamespace = "urn:hl7-org:v3")
	@Action(input = "urn:hl7-org:v3:PRPA_IN201305UV02:Deferred:CrossGatewayPatientDiscovery", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
	public MCCIIN000002UV01Type crossGatewayPatientDiscoveryDeferred(@WebParam(name="PRPA_IN201305UV02", partName = "body",
            targetNamespace = "urn:hl7-org:v3") PRPAIN201305UV02Type request) {
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
        XCPDQueryHandler handler = new XCPDQueryHandler(domain,
                Actor.findActorWithKeyword(RESPGW_KEYWORD), Actor.findActorWithKeyword(INITGW_KEYWORD),
                Transaction.GetTransactionByKeyword(ITI55_KEYWORD), getServletRequest(), context.getMessageContext());
        try {
            return handler.handleDeferredCrossGatewayPatientDiscoveryQuery(request);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
	}

    private HttpServletRequest getServletRequest() {
        return HL7V3Utils.getServletRequestFromContext(context);
    }
}
