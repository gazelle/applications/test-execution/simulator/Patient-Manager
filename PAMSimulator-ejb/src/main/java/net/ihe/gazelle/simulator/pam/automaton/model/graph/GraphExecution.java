package net.ihe.gazelle.simulator.pam.automaton.model.graph;

/**
 * Created by xfs on 26/10/15.
 */

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Link a patient in a System Under Test to an associate graphML
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("graphExecution")
@Table(name = "pam_graph_execution", schema = "public")
@SequenceGenerator(name = "pam_graph_execution_sequence", sequenceName = "pam_graph_execution_id_seq", allocationSize = 1)
public class GraphExecution implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_graph_execution_sequence")
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    private Integer id;

    @ManyToOne
    @Cascade(CascadeType.MERGE)
    @JoinColumn(name = "graph_id")
    private GraphDescription graph;

    @ManyToOne
    @Cascade(CascadeType.MERGE)
    @JoinColumn(name = "sut_id")
    private HL7V2ResponderSUTConfiguration sut;

    @Column(name = "fulledgecoverage")
    private boolean fullEdgeCoverage;

    @Column(name = "bp6mode")
    private boolean bp6mode;

    @Column(name = "validate_transaction_instance")
    private boolean validateTransactionInstance;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>graph</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     */
    public GraphDescription getGraph() {
        return graph;
    }

    /**
     * <p>Setter for the field <code>graph</code>.</p>
     *
     * @param graph a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     */
    public void setGraph(GraphDescription graph) {
        this.graph = graph;
    }

    /**
     * <p>Getter for the field <code>sut</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration getSut() {
        return sut;
    }

    /**
     * <p>Setter for the field <code>sut</code>.</p>
     *
     * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public void setSut(HL7V2ResponderSUTConfiguration sut) {
        this.sut = sut;
    }

    /**
     * <p>isFullEdgeCoverage.</p>
     *
     * @return a boolean.
     */
    public boolean isFullEdgeCoverage() {
        return fullEdgeCoverage;
    }

    /**
     * <p>Setter for the field <code>fullEdgeCoverage</code>.</p>
     *
     * @param fullEdgeCoverage a boolean.
     */
    public void setFullEdgeCoverage(boolean fullEdgeCoverage) {
        this.fullEdgeCoverage = fullEdgeCoverage;
    }

    /**
     * <p>isBp6mode.</p>
     *
     * @return a boolean.
     */
    public boolean isBp6mode() {
        return bp6mode;
    }

    /**
     * <p>Setter for the field <code>bp6mode</code>.</p>
     *
     * @param bp6mode a boolean.
     */
    public void setBp6mode(boolean bp6mode) {
        this.bp6mode = bp6mode;
    }

    /**
     * <p>isValidateTransactionInstance.</p>
     *
     * @return a boolean.
     */
    public boolean isValidateTransactionInstance() {
        return validateTransactionInstance;
    }

    /**
     * <p>Setter for the field <code>validateTransactionInstance</code>.</p>
     *
     * @param validateTransactionInstance a boolean.
     */
    public void setValidateTransactionInstance(boolean validateTransactionInstance) {
        this.validateTransactionInstance = validateTransactionInstance;
    }
}
