package net.ihe.gazelle.simulator.application;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.message.ACK;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.LegalCareModeDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientLink;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Name("patientService")
public class PatientService {
    private static org.slf4j.Logger log = LoggerFactory.getLogger(PatientService.class);

    public enum ADT {
        A24("ADT^A24^ADT_A24"), // link
        A28("ADT^A28^ADT_A05"), // create
        A37("ADT^A37^ADT_A37"), // unlink
        A31("ADT^A31^ADT_A05"), // update
        A47("ADT^A47^ADT_A30"), // changeid/removeid
        A40("ADT^A40^ADT_A39"); // merge

        ADT(String definition) {
            this.definition = definition;
        }

        private final String definition;

        String getDefinition() {
            return definition;
        }
    }

    public static class Exchange {
        private HL7V2ResponderSUTConfiguration selectedSUT;
        private String sendingApplication;
        private String sendingFacility;
        private Actor sendingActor;
        private Actor receivingActor;
        private Transaction simulatedTransaction;
        private HL7Domain<?> hl7Domain;
        private ADT type;
        private Patient patient;
        private Patient secondaryPatient;
        private PatientIdentifier correctPatientIdentifier;
        private PatientIdentifier incorrectPatientIdentifier;
        private boolean pid3Only;
        private List<TransactionInstance> transactions = new ArrayList<>();

        public Exchange(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Actor sendingActor, Actor receivingActor, Transaction simulatedTransaction, HL7Domain<?> hl7Domain, ADT type, Patient patient, Patient secondaryPatient, PatientIdentifier correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier) {
            this(selectedSUT, sendingApplication, sendingFacility, sendingActor, receivingActor, simulatedTransaction, hl7Domain, type, patient, secondaryPatient);
            this.correctPatientIdentifier = correctPatientIdentifier;
            this.incorrectPatientIdentifier = incorrectPatientIdentifier;
        }

        public Exchange(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Actor sendingActor, Actor receivingActor, Transaction simulatedTransaction, HL7Domain<?> hl7Domain, ADT type, Patient patient, Patient secondaryPatient) {
            this(selectedSUT, sendingApplication, sendingFacility, sendingActor, receivingActor, simulatedTransaction, hl7Domain, type, patient);
            this.secondaryPatient = secondaryPatient;
        }

        public Exchange(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Actor sendingActor, Actor receivingActor, Transaction simulatedTransaction, HL7Domain<?> hl7Domain, ADT type, Patient patient) {
            this.selectedSUT = selectedSUT;
            this.sendingApplication = sendingApplication;
            this.sendingFacility = sendingFacility;
            this.sendingActor = sendingActor;
            this.receivingActor = receivingActor;
            this.simulatedTransaction = simulatedTransaction;
            this.hl7Domain = hl7Domain;
            this.type = type;
            this.patient = patient;
        }

        public boolean isPid3Only() {
            return pid3Only;
        }

        public Exchange setPid3Only(boolean pid3Only) {
            this.pid3Only = pid3Only;
            return this;
        }

        public List<TransactionInstance> getTransactions() {
            return transactions;
        }

        public Exchange setTransactions(List<TransactionInstance> transactions) {
            this.transactions = transactions;
            return this;
        }
    }

    private EntityManager entityManager;

    public PatientService() {
        this.entityManager = EntityManagerService.provideEntityManager();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected String getFrame(Exchange exchange) throws HL7Exception {
        switch (exchange.type) {
            case A24:
                return exchange.hl7Domain.getFrameGenerator().buildADTA24Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.patient, exchange.secondaryPatient);
            case A28:
                return exchange.hl7Domain.getFrameGenerator().buildADTA28Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.patient);
            case A37:
                return exchange.hl7Domain.getFrameGenerator().buildADTA37Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.patient, exchange.secondaryPatient);
            case A47:
                return exchange.hl7Domain.getFrameGenerator().buildADTA47Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.correctPatientIdentifier, exchange.incorrectPatientIdentifier, exchange.patient, exchange.secondaryPatient);
            case A31:
                return exchange.hl7Domain.getFrameGenerator().buildADTA31Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.patient, exchange.isPid3Only());
            case A40:
                return exchange.hl7Domain.getFrameGenerator().buildADTA40Frame(exchange.selectedSUT, exchange.sendingApplication, exchange.sendingFacility, exchange.patient, exchange.secondaryPatient);
            default:
                throw new HL7Exception("invalid-hl7-message-type");
        }
    }

    public List<TransactionInstance> sendADTMessage(Exchange exchange) throws HL7Exception {
        return sendADTMessage(exchange, getFrame(exchange));
    }

    public List<TransactionInstance> sendADTMessage(Exchange exchange, String frame) throws HL7Exception {
        log.info(MessageFormat.format("send-hl7-message.sent-frame\n{0}", frame));
        Initiator hl7Initiator = new Initiator(
                exchange.selectedSUT,
                exchange.sendingFacility,
                exchange.sendingApplication,
                exchange.sendingActor,
                exchange.simulatedTransaction,
                frame,
                exchange.type.getDefinition(),
                exchange.hl7Domain.getDomainKeyword(),
                exchange.receivingActor);
        TransactionInstance t = hl7Initiator.sendMessageAndGetTheHL7Message();
        exchange.getTransactions().add(t);
        log.info(MessageFormat.format("send-hl7-message.response-frame\n{0}", t.getReceivedMessageContent()));
        historizeSent(exchange);
        return exchange.getTransactions();
    }

    private void historizeSent(Exchange exchange) {
        PatientHistory history = new PatientHistory(
                exchange.patient,
                null,
                PatientHistory.ActionPerformed.SENT,
                exchange.type.getDefinition(),
                exchange.patient.getCreator(),
                exchange.selectedSUT.getFacility(),
                exchange.selectedSUT.getApplication());
        history.save(entityManager);
        if (ADT.A24.equals(exchange.type) || ADT.A37.equals(exchange.type)) {
            history = new PatientHistory(
                    exchange.secondaryPatient,
                    null,
                    PatientHistory.ActionPerformed.SENT,
                    exchange.type.getDefinition(),
                    exchange.patient.getCreator(),
                    exchange.selectedSUT.getFacility(),
                    exchange.selectedSUT.getApplication());
            history.save(entityManager);
        }
    }

    public void assertAcknowleged(TransactionInstance message) throws HL7Exception {
        if (message != null && message.getAckMessageType() != null && message.getAckMessageType().contains("ACK")) {
            ACK ack = (ACK) PipeParser.getInstanceWithNoValidation().parse(message.getReceivedMessageContent());
            if (!ack.getMSA().getAcknowledgmentCode().getValue().equals("AA")) {
                throw new HL7Exception("Request was not acknowledged by consumer");
            }
        } else {
            throw new HL7Exception("The received response is not of type ACK");
        }
    }

    public PatientHistory link(Patient selectedPatient, Patient secondaryPatient, Actor sendingActor) {
        PatientLink.linkPatients(selectedPatient, secondaryPatient, sendingActor);
        return historizeLinkage(selectedPatient, secondaryPatient, PatientHistory.ActionPerformed.LINK);
    }

    public PatientHistory unlink(Patient selectedPatient, Patient secondaryPatient, Actor sendingActor) {
        PatientLink.unlinkPatients(selectedPatient, secondaryPatient, sendingActor);
        return historizeLinkage(selectedPatient, secondaryPatient, PatientHistory.ActionPerformed.LINK);
    }

    private PatientHistory historizeLinkage(Patient selectedPatient, Patient secondaryPatient, String linkage) {
        String username = null;
        if (Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        }
        PatientHistory history = new PatientHistory(selectedPatient, secondaryPatient, linkage, null, username,
                null, null);
        history.save(entityManager);
        history = new PatientHistory(secondaryPatient, selectedPatient, linkage, null, username,
                null, null);
        history.save(entityManager);
        return history;
    }

    public PatientHistory upgradePatientIdVersion(Patient inPatient, Actor sendingActor) {
        // TODO pourquoi la façon de déterminer le user est-elle différente entre le upgradePatientIdVersion et le updatePatientVersion
        String user = null;
        if (Identity.instance().isLoggedIn()) {
            user = Identity.instance().getCredentials().getUsername();
        }
        // create a new patient from the selected patient, deactive the selected patient
        Patient newPatient = new Patient(inPatient, sendingActor);
        newPatient = newPatient.savePatient(entityManager);
        inPatient.setStillActive(false);
        // history
        PatientHistory history = new PatientHistory(inPatient.savePatient(entityManager), newPatient, PatientHistory.ActionPerformed.CHANGEID, null, user,
                null, null);
        history.save(entityManager);
        PatientHistory h = new PatientHistory(newPatient, null, PatientHistory.ActionPerformed.CREATED, null, user, null, null);
        h.save(entityManager);
        return history;
    }

    protected void attachLegalCareModeToPatient(Patient selectedPatient) {
        for (LegalCareMode careMode : selectedPatient.getLegalCareModes()) {
            careMode.setPatient(selectedPatient);
            LegalCareModeDAO.saveEntity(careMode);
        }
    }

    public PatientHistory updatePatientVersion(Patient newPatient, Patient oldPatient) {
        // save patient's identifiers
        if (newPatient.getPatientIdentifiers() != null) {
            List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(newPatient
                    .getPatientIdentifiers());
            newPatient.setPatientIdentifiers(pidList);
        }
        newPatient = newPatient.savePatient(entityManager);
        attachLegalCareModeToPatient(newPatient);
        // populate patients' histories
        PatientHistory history1 = new PatientHistory(newPatient, null, PatientHistory.ActionPerformed.CREATED, null,
                newPatient.getCreator(), null, null);
        history1.save(entityManager);

        // deactivate old patient
        if (oldPatient != null) {
            oldPatient.setStillActive(false);
            oldPatient = oldPatient.savePatient(entityManager);
            PatientHistory history = new PatientHistory(oldPatient, newPatient, PatientHistory.ActionPerformed.UPDATE,
                    null, newPatient.getCreator(), null, null);
            history.save(entityManager);
            return history;
        } else {
            return history1;
        }
    }

    public PatientHistory mergePatientVersion(Patient newPatient, Patient oldPatient) {
        oldPatient.setStillActive(false);
        oldPatient = oldPatient.savePatient(entityManager);
        // TODO est-il normal que le user soit null dans l'historisation du merge
        PatientHistory history = new PatientHistory(oldPatient, newPatient, PatientHistory.ActionPerformed.MERGE, null,
                null, null, null);
        history.save(entityManager);
        return history;
    }

    public PatientHistory createPatientVersion(Patient patient) {
        String user = null;
        if (Identity.instance().isLoggedIn()) {
            user = Identity.instance().getCredentials().getUsername();
        }
        if (patient.getId() == null) {
            if (patient.getPatientIdentifiers() != null) {
                List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(patient
                        .getPatientIdentifiers());
                patient.setPatientIdentifiers(pidList);
            }
            PatientHistory history = new PatientHistory(patient.savePatient(entityManager), null, PatientHistory.ActionPerformed.CREATED, null,
                    user, null, null);
            history.save(entityManager);
            return history;
        }
        return null;
    }

    public Patient removeIdentifier(Patient selectedPatient, PatientIdentifier identifier) {
        if (selectedPatient.getPatientIdentifiers().contains(identifier)) {
            selectedPatient.getPatientIdentifiers().remove(identifier);
            return selectedPatient.savePatient(entityManager);
        } else {
            return selectedPatient;
        }
    }

    public Patient changeIdentifier(Patient selectedPatient, PatientIdentifier modifiedPid, String newIdString) {
        String[] pidComponents = newIdString.split("\\^");
        String fullId = null;
        String newIdentifierTypeCode = null;
        if (pidComponents.length > 4) {
            int cx5Index = newIdString.lastIndexOf('^');
            fullId = newIdString.substring(0, cx5Index);
            newIdentifierTypeCode = newIdString.substring(cx5Index + 1);
        } else {
            fullId = newIdString;
        }
        PatientIdentifier newIdentifier = new PatientIdentifier(fullId, newIdentifierTypeCode);
        if (newIdentifier.equals(modifiedPid)) {
            return selectedPatient;
        } else {
            Patient x = removeIdentifier(selectedPatient, modifiedPid);
            if (x != selectedPatient) { // la suppression de l'identifiant a réussi
                newIdentifier = newIdentifier.save();
                x.getPatientIdentifiers().add(newIdentifier);
                return x.savePatient(entityManager);
            } else {
                return selectedPatient;
            }
        }
    }


}
