package net.ihe.gazelle.simulator.xcpd.model;

import net.ihe.gazelle.hql.FilterLabel;

/**
 * <b>Class Description : </b>DeferredTransactionStatus<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 11/01/16
 *


 */
public enum DeferredTransactionStatus {

    WAITING("WAITING"),
    DONE("DONE"),
    OPTION_NOT_SUPPORTED("OPTION NOT SUPPORTED"),
    DISCARDED("DISCARDED");

    private String label;

    DeferredTransactionStatus(String label){
        this.label = label;
    }

    @FilterLabel
    public String getLabel() {
        return label;
    }
}
