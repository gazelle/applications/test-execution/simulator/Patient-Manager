package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.hl7v3.voc.EntityNameUse;
import org.hl7.fhir.dstu3.model.HumanName;

/**
 * <p>NameType enum.</p>
 *
 * @author abe
 * @version 1.0: 08/06/18
 */
public enum NameType {

    OFFICIAL("Legal name", "L", HumanName.NameUse.OFFICIAL, EntityNameUse.L),
    TEMP("Temporary name", "", HumanName.NameUse.TEMP, EntityNameUse.ASGN),
    NICKNAME ("Nickname", "N", HumanName.NameUse.NICKNAME, EntityNameUse.P),
    ANONYMOUS ("Anonymous", "S", HumanName.NameUse.ANONYMOUS,null),
    MAIDEN("Maiden name", "M", HumanName.NameUse.MAIDEN, null),
    NULL("Not defined", "U", HumanName.NameUse.NULL, null);

    String label;
    String hl7v2Code;
    HumanName.NameUse fhirCode;
    EntityNameUse hl7v3Code;

    NameType(String label, String hl7v2Code, HumanName.NameUse fhirCode, EntityNameUse hl7v3Code){

    }

    public String getLabel() {
        return label;
    }

    public String getHl7v2Code() {
        return hl7v2Code;
    }

    public HumanName.NameUse getFhirCode() {
        return fhirCode;
    }

    public EntityNameUse getHl7v3Code() {
        return hl7v3Code;
    }
}
