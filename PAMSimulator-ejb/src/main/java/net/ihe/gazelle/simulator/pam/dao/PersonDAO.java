package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.hl7v3.voc.ContactRoleType;
import net.ihe.gazelle.hl7v3.voc.PersonalRelationshipRoleType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.model.PersonQuery;

import java.util.List;

/**
 * Created by aberge on 18/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PersonDAO {

    /**
     * <p>getRelationsForPatient.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Person> getRelationsForPatient(Patient currentPatient) {
        PersonQuery query = new PersonQuery();
        query.patient().id().eq(currentPatient.getId());
        return query.getListNullIfEmpty();
    }

    /**
     * <p>getIdentifierExtension.</p>
     *
     * @param person a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getIdentifierExtension(Person person){
        String identifier = person.getIdentifier();
        String[] identifierParts = identifier.split("\\^");
        if (identifierParts.length > 0){
            return identifierParts[0];
        }
        return null;
    }

    /**
     * <p>getIdentifierRoot.</p>
     *
     * @param person a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getIdentifierRoot(Person person){
        String identifier = person.getIdentifier();
        String[] identifierParts = identifier.split("&");
        if (identifierParts.length > 1){
            return identifierParts[1];
        }
        return null;
    }

    /**
     * <p>newMother.</p>
     *
     * @param motherMaidenName a {@link java.lang.String} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public static Person newMother(String motherMaidenName, Patient patient) {
        if (motherMaidenName != null && !motherMaidenName.isEmpty()) {
            Person mother = new Person(PersonalRelationshipRoleType.MTH);
            mother.setContactRoleCode("N"); // Next of Kin
            mother.setLastName(motherMaidenName);
            mother.setPatient(patient);
            return mother;
        } else {
            return null;
        }
    }

    /**
     * <p>getMotherForPatient.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public static Person getMotherForPatient(Patient patient) {
        PersonQuery query = new PersonQuery();
        query.patient().id().eq(patient.getId());
        query.relationshipCode().eq(PersonalRelationshipRoleType.MTH.value());
        return query.getUniqueResult();
    }
}
