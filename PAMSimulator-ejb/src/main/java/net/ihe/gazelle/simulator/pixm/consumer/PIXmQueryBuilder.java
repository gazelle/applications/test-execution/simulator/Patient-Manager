package net.ihe.gazelle.simulator.pixm.consumer;

import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.simulator.fhir.util.IFhirQueryBuilder;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.StringType;

/**
 * <p>PIXmQueryBuilder class.</p>
 *
 * @author aberge
 * @version 1.0: 13/11/17
 */

public class PIXmQueryBuilder implements IFhirQueryBuilder {

    private static final String PARAM_SOURCE_IDENTIFIER = "sourceIdentifier";
    private static final String PARAM_TARGET_SYSTEM = "targetSystem";

    public PIXmQueryBuilder() {

    }

    public Parameters buildQuery(PatientIdentifier searchedPid, AssigningAuthority domainToReturn) {
        // Create the input parameters to pass to the server
        Parameters inParams = new Parameters();

        if (searchedPid != null) {
            StringType fhirIdentifier = patientIdentifierToFhirIdentifier(searchedPid);
            inParams.addParameter().setName(PARAM_SOURCE_IDENTIFIER).setValue(fhirIdentifier);
        }
        if (domainToReturn.getUniversalId() != null && !domainToReturn.getUniversalId().isEmpty()) {
            // only one domain can be provided in PIXm
            String universalIDAsUrn = domainToReturn.getUniversalIDAsUrn();
            inParams.addParameter().setName(PARAM_TARGET_SYSTEM).setValue(new StringType(universalIDAsUrn));
        }
        return inParams;
    }


    @Override
    public String getRequestType() {
        return FhirConstants.PIXM_REQUEST_TYPE;
    }


    private StringType patientIdentifierToFhirIdentifier(PatientIdentifier identifier) {
        StringBuilder fhirIdentifier = new StringBuilder();
        if (identifier.getDomain() != null && identifier.getDomain().getUniversalID() != null) {
            String universalId = identifier.getDomain().getUniversalIDAsUrn();
            fhirIdentifier.append(universalId);
            fhirIdentifier.append('|');
        }
        String idNumber = identifier.getIdNumber();
        if (idNumber != null && !idNumber.isEmpty()) {
            fhirIdentifier.append(idNumber);
        }
        return new StringType(fhirIdentifier.toString());
    }
}
