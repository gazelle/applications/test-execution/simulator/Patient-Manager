package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.coctmt090003UV01.COCTMT090003UV01AssignedEntity;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mfmimt700701UV01.*;
import net.ihe.gazelle.hl7v3.voc.*;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;

import java.util.List;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MFMIMT700701UV01PartBuilder extends HL7v3MessageBuilder {

	private static final String REPLACEMENT_TYPE_CODE = "RPLC";
	private static final String PATIENT_CLASS_CODE = "PAT";

	/**
	 * <p>populateCustodian.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01Custodian} object.
	 */
	public static MFMIMT700701UV01Custodian populateCustodian() {
		MFMIMT700701UV01Custodian custodian = new MFMIMT700701UV01Custodian();
		custodian.setTypeCode(ParticipationType.CST);
		COCTMT090003UV01AssignedEntity entity = new COCTMT090003UV01AssignedEntity();
		entity.setClassCode(RoleClassAssignedEntity.ASSIGNED);
		entity.addId(new II(PreferenceService.getString("hl7v3_organization_oid"), null));
		custodian.setAssignedEntity(entity);
		return custodian;
	}

	/**
	 * <p>populateReplaceOf.</p>
	 *
	 * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01ReplacementOf} object.
	 */
	public static MFMIMT700701UV01ReplacementOf populateReplaceOf(Patient patient) {
		MFMIMT700701UV01ReplacementOf replacementOf = new MFMIMT700701UV01ReplacementOf();
		replacementOf.setTypeCode(REPLACEMENT_TYPE_CODE);
		replacementOf.setPriorRegistration(populatePriorRegistration(patient));
		return replacementOf;
	}

	private static MFMIMT700701UV01PriorRegistration populatePriorRegistration(Patient patient) {
		MFMIMT700701UV01PriorRegistration priorRegistration = new MFMIMT700701UV01PriorRegistration();
		priorRegistration.setClassCode(ActClass.REG);
		priorRegistration.setMoodCode(ActMood.EVN);
		priorRegistration.setStatusCode(new CS("obsolete", null, null));
		priorRegistration.setSubject1(populateSubject1(patient));
		return priorRegistration;
	}

	private static MFMIMT700701UV01Subject3 populateSubject1(Patient patient) {
		MFMIMT700701UV01Subject3 subject3 = new MFMIMT700701UV01Subject3();
		subject3.setTypeCode(ParticipationTargetSubject.SBJ);
		subject3.setPriorRegisteredRole(populatePriorRegisteredRole(patient));
		return subject3;
	}

	private static MFMIMT700701UV01PriorRegisteredRole populatePriorRegisteredRole(Patient patient) {
		MFMIMT700701UV01PriorRegisteredRole role = new MFMIMT700701UV01PriorRegisteredRole();
		role.setClassCode(PATIENT_CLASS_CODE);
		List<PatientIdentifier> pids = PatientIdentifierDAO.getPatientIdentifiers(patient);
		PatientIdentifier subsumedPatientId = pids.get(0);
		II ii = new II(subsumedPatientId.getDomain().getUniversalID(), subsumedPatientId.getIdNumber());
		ii.setAssigningAuthorityName(subsumedPatientId.getDomain().getNamespaceID());
		role.addId(ii);

		return role;
	}
}
