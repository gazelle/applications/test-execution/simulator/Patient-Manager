package net.ihe.gazelle.simulator.testdata.action;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.testdata.dao.TestDataDAO;
import net.ihe.gazelle.simulator.testdata.model.PatientIdentifierTestData;
import net.ihe.gazelle.simulator.testdata.model.PatientTestData;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.event.ValueChangeEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 08/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class ATestDataManager {

    private TestData selectedTestData = null;
    private List<TestData> availableTestData = null;
    private TestData favoriteQueryToSave = null;

    /**
     * <p>save.</p>
     */
    public void save() {
        if (favoriteQueryToSave.getName() == null || favoriteQueryToSave.getName().isEmpty()){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You shall name your query");
        }
        if (isNameUnique()) {
            setCredentialsForTestData(favoriteQueryToSave);
            setLimitNumberForTestData(favoriteQueryToSave);
            setFhirResponseFormatForTestData(favoriteQueryToSave);
            setDeferredOption(favoriteQueryToSave);
            TestDataDAO.saveEntity(favoriteQueryToSave);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, favoriteQueryToSave.getName() + " has been saved");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Another query already uses this name, please choose another one");
        }
    }

    private boolean isNameUnique() {
        return !TestDataDAO.entryExistsWithName(favoriteQueryToSave.getName());
    }

    /**
     * <p>cancel.</p>
     */
    public void cancel() {
        favoriteQueryToSave = null;
    }

    /**
     * <p>createPatientTestData.</p>
     *
     * @param patient       a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param inTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domains       a {@link java.util.List} object.
     */
    protected void createPatientTestData(Patient patient, Transaction inTransaction, List<AssigningAuthority> domains) {
        patient.setSimulatedActor(Actor.findActorWithKeyword("UNKNOWN"));
        patient.setTestData(true);
        List<PatientIdentifier> identifierList = patient.getPatientIdentifiers();
        if (identifierList != null && !identifierList.isEmpty()){
            List<PatientIdentifier> savedIdentifiers = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier identifier: identifierList){
                prepareIdentifierForSaving(identifier);
                savedIdentifiers.add(identifier);
            }
            patient.setPatientIdentifiers(savedIdentifiers);
        }
        favoriteQueryToSave = new PatientTestData(patient);
        favoriteQueryToSave.setApplyToTransaction(inTransaction);
        favoriteQueryToSave.setRestrictedDomain(assigningAuthoritiesToDomains(domains));
    }

    private void prepareIdentifierForSaving(PatientIdentifier identifier) {
        HierarchicDesignator domain = identifier.getDomain();
        if (domain != null && !domain.isEmpty()){
            HierarchicDesignator hd = HierarchicDesignatorDAO.createAssigningAuthority(domain.getNamespaceID(), domain.getUniversalID(),
                    domain.getUniversalIDType(), DesignatorType.FILTERED_DOMAIN);
            identifier.setDomain(hd);
        }
    }

    /**
     * <p>createPatientIdentifierTestData.</p>
     *
     * @param identifier    a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     * @param inTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domains       a {@link java.util.List} object.
     */
    protected void createPatientIdentifierTestData(PatientIdentifier identifier, Transaction inTransaction, List<AssigningAuthority> domains) {
        HierarchicDesignator domain = identifier.getDomain();
        if (domain.isEmpty()){
            identifier.setDomain(null);
        } else {
            HierarchicDesignator identifierDomain = HierarchicDesignatorDAO.createAssigningAuthority(domain.getNamespaceID(), domain.getUniversalID(),
                    domain.getUniversalIDType(), DesignatorType.FILTERED_DOMAIN);
            identifier.setDomain(identifierDomain);
        }
        favoriteQueryToSave = new PatientIdentifierTestData(identifier);
        favoriteQueryToSave.setApplyToTransaction(inTransaction);
        favoriteQueryToSave.setRestrictedDomain(assigningAuthoritiesToDomains(domains));
    }

    private List<HierarchicDesignator> assigningAuthoritiesToDomains(List<AssigningAuthority> domains) {
        if (domains != null && !domains.isEmpty()) {
            List<HierarchicDesignator> hierarchicDesignators = new ArrayList<HierarchicDesignator>();
            for (AssigningAuthority authority : domains) {
                HierarchicDesignator hd = HierarchicDesignatorDAO.createAssigningAuthority(authority.getNamespace(), authority.getUniversalId(), authority
                        .getUniversalIdType(), DesignatorType.FILTERED_DOMAIN);
                hierarchicDesignators.add(hd);
            }
            return hierarchicDesignators;
        } else {
            return null;
        }
    }

    /**
     * <p>getAssigningAuthoritiesFromRestrictedDomains.</p>
     *
     * @param domains a {@link java.util.List} object.
     *
     * @return a {@link java.util.List} object.
     */
    protected List<AssigningAuthority> getAssigningAuthoritiesFromRestrictedDomains(List<HierarchicDesignator> domains) {
        List<AssigningAuthority> assigningAuthorities = new ArrayList<AssigningAuthority>();
        if (domains != null && !domains.isEmpty()) {
            for (HierarchicDesignator domain : domains) {
                AssigningAuthority authority = new AssigningAuthority();
                authority.setNamespace(domain.getNamespaceID());
                authority.setUniversalId(domain.getUniversalID());
                authority.setUniversalIdType(domain.getUniversalIDType());
                assigningAuthorities.add(authority);
            }
        }
        return assigningAuthorities;
    }

    /**
     * <p>setCredentialsForTestData.</p>
     *
     * @param testData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    protected abstract void setCredentialsForTestData(TestData testData);

    /**
     * <p>setLimitNumberForTestData.</p>
     *
     * @param testData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    protected abstract void setLimitNumberForTestData(TestData testData);

    /**
     * <p>setDeferredOption.</p>
     *
     * @param testData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    protected abstract void setDeferredOption(TestData testData);

    /**
     * <p>createNewTestData.</p>
     */
    public abstract void createNewTestData();

    /**
     * <p>Getter for the field <code>selectedTestData</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public TestData getSelectedTestData() {
        return selectedTestData;
    }

    /**
     * <p>Setter for the field <code>selectedTestData</code>.</p>
     *
     * @param selectedTestData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public void setSelectedTestData(TestData selectedTestData) {
        this.selectedTestData = selectedTestData;
    }

    /**
     * <p>testDataChangeListener.</p>
     *
     * @param listener a {@link javax.faces.event.ValueChangeEvent} object.
     */
    public void testDataChangeListener(ValueChangeEvent listener) {
        Object newValue = listener.getNewValue();
        if (newValue == null) {
            this.selectedTestData = null;
            resetPage();
        } else if (newValue instanceof TestData) {
            TestData newTestData = (TestData) newValue;
            if (this.selectedTestData == null || !this.selectedTestData.getId().equals(newTestData.getId())) {
                this.selectedTestData = newTestData;
                initializePageUsingTestData(selectedTestData);
            }
        }
    }

    /**
     * <p>resetPage.</p>
     */
    protected abstract void resetPage();

    /**
     * <p>initializePageUsingTestData.</p>
     *
     * @param selectedTestData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    protected abstract void initializePageUsingTestData(TestData selectedTestData);

    /**
     * <p>Getter for the field <code>availableTestData</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestData> getAvailableTestData() {
        if (availableTestData == null) {
            setAvailableTestData();
        }
        return availableTestData;
    }

    /**
     * <p>Setter for the field <code>availableTestData</code>.</p>
     */
    public void setAvailableTestData() {
        Transaction appliesTo = getTestedTransaction();
        String username = null;
        if (Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        }
        availableTestData = TestDataDAO.getAvailableTestData(appliesTo, username);
    }

    /**
     * <p>isAvailableTestData.</p>
     *
     * @return a boolean.
     */
    public boolean isAvailableTestData() {
        return !getAvailableTestData().isEmpty();
    }

    /**
     * <p>setFhirResponseFormatForTestData.</p>
     *
     * @param testData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    protected abstract void setFhirResponseFormatForTestData(TestData testData);

    /**
     * <p>getTestedTransaction.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public abstract Transaction getTestedTransaction();

    /**
     * <p>unselectTestData.</p>
     */
    public void unselectTestData() {
        this.selectedTestData = null;
        resetPage();
    }

    /**
     * <p>Getter for the field <code>favoriteQueryToSave</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public TestData getFavoriteQueryToSave() {
        return favoriteQueryToSave;
    }

    /**
     * <p>Setter for the field <code>favoriteQueryToSave</code>.</p>
     *
     * @param favoriteQueryToSave a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public void setFavoriteQueryToSave(TestData favoriteQueryToSave) {
        this.favoriteQueryToSave = favoriteQueryToSave;
    }
}
