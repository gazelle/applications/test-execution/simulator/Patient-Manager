package net.ihe.gazelle.simulator.pam.util;

import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.utils.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 05/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public final class PatientIdentifierUtil {

    private PatientIdentifierUtil(){

    }

    /**
     * <p>generatePatientIdentifierList.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Pair<Integer, String>> generatePatientIdentifierList(Patient inPatient) {
        if (inPatient.getPatientIdentifiers() != null) {
            List<Pair<Integer, String>> patientIdentifiers = new ArrayList<Pair<Integer, String>>();
            for (int index = 0; index < inPatient.getPatientIdentifiers().size(); index++) {
                String fullPatientId = inPatient.getPatientIdentifiers().get(index).getFullPatientId();
                if (inPatient.getPatientIdentifiers().get(index).getIdentifierTypeCode() != null) {
                    fullPatientId = fullPatientId.concat("^").concat(
                            inPatient.getPatientIdentifiers().get(index).getIdentifierTypeCode());
                }
                patientIdentifiers.add(new Pair<Integer, String>(index, fullPatientId));
            }
            return patientIdentifiers;
        } else {
            return null;
        }
    }
}
