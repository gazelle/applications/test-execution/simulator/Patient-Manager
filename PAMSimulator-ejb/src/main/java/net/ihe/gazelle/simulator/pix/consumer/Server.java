package net.ihe.gazelle.simulator.pix.consumer;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;


/**
 * <p>Server class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixConsumer")
@Scope(ScopeType.APPLICATION)
@Startup(depends = {"entityManager"})
public class Server extends AbstractServer<PIXConsumer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6220136727424344072L;

	/** {@inheritDoc} */
	@Override
	@Create
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER", entityManager);
		Domain domain = Domain
				.getDomainByKeyword("ITI", entityManager);
		handler = new PIXConsumer();
		startServers(simulatedActor, null, domain);
	}

}
