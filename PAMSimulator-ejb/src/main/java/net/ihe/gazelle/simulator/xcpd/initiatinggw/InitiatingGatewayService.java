package net.ihe.gazelle.simulator.xcpd.initiatinggw;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.xcpd.common.XCPDQueryHandler;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.Addressing;

/**
 * <b>Class Description : </b>InitiatingGatewayService<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 03/11/15
 */
@Stateless
@Name("XCPDInitiatingGatewayService")
@WebService(portName="InitiatingGateway_Port_Soap12", name="InitiatingGateway_PortType", targetNamespace= "urn:ihe:iti:xcpd:2009", serviceName="InitiatingGateway_Service")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@Addressing(enabled = true, required = true)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@GenerateInterface(value="InitiatingGatewayServiceRemote", isLocal = false, isRemote = true)
@HandlerChain(file = "soap-handler.xml")
public class InitiatingGatewayService implements InitiatingGatewayServiceRemote {

    private static Logger log = LoggerFactory.getLogger(InitiatingGatewayService.class);

    @Resource
    private WebServiceContext context;

    @WebMethod(operationName = "InitiatingGateway_Deferred_PRPA_IN201306UV02", action = "urn:hl7-org:v3:PRPA_IN201306UV02:Deferred:CrossGatewayPatientDiscovery")
    @WebResult(name = "MCCI_IN000002UV01", partName = "body", targetNamespace = "urn:hl7-org:v3")
    @Action(input = "urn:hl7-org:v3:PRPA_IN201306UV02:Deferred:CrossGatewayPatientDiscovery", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
    public MCCIIN000002UV01Type crossGatewayPatientDiscoveryDeferredResponse(@WebParam(name="PRPA_IN201306UV02", partName = "body",
            targetNamespace = "urn:hl7-org:v3") PRPAIN201306UV02Type request){
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
        XCPDQueryHandler handler = new XCPDQueryHandler(domain,
                Actor.findActorWithKeyword("INIT_GW"), Actor.findActorWithKeyword("RESP_GW"),
                Transaction.GetTransactionByKeyword("ITI-55"), getServletRequest(), context.getMessageContext());
        try {
            return handler.handleCrossGatewayPatientDiscoveryDeferredResponse(request);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private HttpServletRequest getServletRequest() {
        return HL7V3Utils.getServletRequestFromContext(context);
    }
}
