package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>MainMenu<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version $Id: $Id
 */
public class MainMenu {

    private static MenuGroup MENU = null;

    private MainMenu() {
        super();
    }

    /**
     * <p>getMenu.</p>
     *
     * @return a {@link net.ihe.gazelle.common.pages.menu.MenuGroup} object.
     */
    public static final MenuGroup getMenu() {
        synchronized (MainMenu.class) {
            if (MENU == null) {
                ArrayList<Menu> children = new ArrayList<Menu>();

                children.add(getADT());
                children.add(getPAM());
                children.add(getPDQALL());
                children.add(getPIXALL());
                children.add(getXCPD());
                children.add(getXUA());
                children.add(new MenuEntry(PatientManagerPages.PATIENTS));
                children.add(getSUT());
                children.add(getCAT());
                children.add(new MenuEntry(PatientManagerPages.HL7_MESSAGES));
                children.add(new MenuEntry(PatientManagerPages.ADMIN_VALUE_SETS));
                MENU = new MenuGroup(null, children);
            }
        }
        return MENU;
    }

    private static Menu getCAT() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.CAT_PATIENTS));
        children.add(new MenuEntry(PatientManagerPages.CAT_IMPORT));
        children.add(new MenuEntry(PatientManagerPages.CAT_CREATE));
        return new MenuGroup(PatientManagerPages.CAT_MAIN, children);
    }

    private static Menu getSUT() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.SUT_HL7V2));
        children.add(new MenuEntry(PatientManagerPages.SUT_HL7V3));
        children.add(new MenuEntry(PatientManagerPages.SUT_FHIR));
        children.add(new MenuEntry(PatientManagerPages.CAT_AA));
        return new MenuGroup(PatientManagerPages.ALL_SUT, children);
    }

    private static Menu getXUA() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.XUA_XSERVICEUSER));
        children.add(new MenuEntry(PatientManagerPages.XUA_XSERVICEPROVIDER));
        return new MenuGroup(PatientManagerPages.XUA_ALL, children);
    }

    private static Menu getXCPD() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(getXCPDInit());
        children.add(new MenuEntry(PatientManagerPages.XCPD_RESP_GW));
        return new MenuGroup(PatientManagerPages.XCPD, children);
    }

    private static Menu getXCPDInit() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.XCPD_INIT_GW_IHE));
        children.add(new MenuEntry(PatientManagerPages.XCPD_INIT_GW_CONF));
        return new MenuGroup(PatientManagerPages.XCPD_INIT_GW, children);
    }

    private static Menu getPIXALL() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(getPIXConsumer());
        children.add(getPIXManager());
        children.add(getPIXSource());
        return new MenuGroup(PatientManagerPages.ALL_PIX, children);
    }

    private static Menu getPIXSource() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PIX_SOURCE));
        children.add(new MenuEntry(PatientManagerPages.PIX_SOURCE_ITI30));
        children.add(new MenuEntry(PatientManagerPages.PIXV3_SOURCE));
        return new MenuGroup(PatientManagerPages.ALL_PIX_SOURCE, children);
    }

    private static Menu getPIXManager() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PIX_MANAGER_ADMIN));
        children.add(new MenuEntry(PatientManagerPages.PIX_MANAGER_UPDATE));
        children.add(new MenuEntry(PatientManagerPages.PIXV3_MANAGER_UPDATE));
        children.add(new MenuEntry(PatientManagerPages.PIX_MANAGER_CONF));
        children.add(new MenuEntry(PatientManagerPages.PIXV3_MANAGER_CONF));
        children.add(new MenuEntry(PatientManagerPages.PIXM_MANAGER));
        return new MenuGroup(PatientManagerPages.ALL_PIX_MANAGER, children);
    }

    private static Menu getPIXConsumer() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PIX_CONSUMER));
        children.add(new MenuEntry(PatientManagerPages.PIXV3_CONSUMER));
        children.add(new MenuEntry(PatientManagerPages.PIXM_CONSUMER));
        children.add(new MenuEntry(PatientManagerPages.PIX_CONSUMER_CONF));
        children.add(new MenuEntry(PatientManagerPages.PIXV3_CONSUMER_CONF));
        return new MenuGroup(PatientManagerPages.ALL_PIX_CONSUMER, children);
    }

    private static Menu getPDQALL() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(getPDQPDC());
        children.add(getPDQPDS());
        return new MenuGroup(PatientManagerPages.ALL_PDQ, children);
    }

    private static Menu getPDQPDS() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PDQ_SUPPLIER));
        children.add(new MenuEntry(PatientManagerPages.PDQV3_SUPPLIER));
        children.add(new MenuEntry(PatientManagerPages.PQDM_SUPPLIER));
        return new MenuGroup(PatientManagerPages.ALL_PDQ_PDS, children);
    }

    private static Menu getPDQPDC() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PDQ_CONSUMER));
        children.add(new MenuEntry(PatientManagerPages.PDQV3_CONSUMER));
        children.add(new MenuEntry(PatientManagerPages.PQDM_CONSUMER));
        //children.add(new MenuEntry(PatientManagerPages.PQDM_CONSUMER_R4));
        return new MenuGroup(PatientManagerPages.ALL_PDQ_PDC, children);
    }



    private static MenuGroup getADT(){
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.ADT_RAD_1));
        children.add(new MenuEntry(PatientManagerPages.ADT_RAD_12));
        return new MenuGroup(PatientManagerPages.ADT, children);
    }

    private static MenuGroup getPAM(){
        List<Menu> children = new ArrayList<Menu>();
        children.add(getPAMPDS());
        children.add(new MenuEntry(PatientManagerPages.PAM_PDC));
        children.add(new MenuEntry(PatientManagerPages.PAM_PES));
        children.add(new MenuEntry(PatientManagerPages.PAM_PEC));
        children.add(getPAMAutomaton());
        return new MenuGroup(PatientManagerPages.PAM, children);
    }

    private static Menu getPAMAutomaton() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PAM_AUTOMATON_EXEC));
        children.add(new MenuEntry(PatientManagerPages.PAM_AUTOMATON_LOGS));
        children.add(new MenuEntry(PatientManagerPages.PAM_AUTOMATON_ADMIN));
        return new MenuGroup(PatientManagerPages.PAM_AUTOMATON, children);
    }

    private static Menu getPAMPDS() {
        List<Menu> children = new ArrayList<Menu>();
        children.add(new MenuEntry(PatientManagerPages.PAM_CREATE));
        children.add(new MenuEntry(PatientManagerPages.PAM_UPDATE));
        if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.BP6)) {
            children.add(new MenuEntry(PatientManagerPages.PAM_REMOVE_INS));
        }
        children.add(new MenuEntry(PatientManagerPages.PAM_CHANGE_ID));
        children.add(new MenuEntry(PatientManagerPages.PAM_LINK));
        children.add(new MenuEntry(PatientManagerPages.PAM_MERGE));
        return new MenuGroup(PatientManagerPages.PAM_PDS, children);
    }



    /**
     * <p>refreshMenu.</p>
     *
     * @return a {@link net.ihe.gazelle.common.pages.menu.MenuGroup} object.
     */
    public static final MenuGroup refreshMenu() {
      synchronized (MainMenu.class) {
          MENU = null;
      }
        return getMenu();
    }
}
