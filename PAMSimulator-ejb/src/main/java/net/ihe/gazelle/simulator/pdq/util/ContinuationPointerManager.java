package net.ihe.gazelle.simulator.pdq.util;

import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.simulator.hl7v3.responder.ContinuationPointer;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * <p>ContinuationPointerManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ContinuationPointerManager {

    private static Logger log = LoggerFactory.getLogger(ContinuationPointerManager.class);

    private Map<String, ContinuationPointer> pointers;

    /**
     * constructor
     */
    public ContinuationPointerManager() {
        pointers = new HashMap<String, ContinuationPointer>();
    }

    /**
     * For a given pointer value, tells if the continuation pointer exists or not
     *
     * @param searchedPointer a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean pointerExists(String searchedPointer) {
        return pointers.containsKey(searchedPointer);
    }

    /**
     * Returns the list of patients for a given pointer
     *
     * @param pointer a {@link java.lang.String} object.
     * @param nbOfHits TODO
     * @return a {@link java.util.List} object.
     */
    public List<Patient> getPatientListForPointer(String pointer, Integer nbOfHits) {
        List<Patient> patients = null;
        ContinuationPointer continuationPointer = pointers.get(pointer);
        if (continuationPointer != null) {
            continuationPointer.setNbOfHitsToReturn(nbOfHits);
            if (continuationPointer.getPatients().size() <= continuationPointer.getNbOfHitsToReturn()) {
                patients = continuationPointer.getPatients();
                pointers.remove(pointer);
                log.info("No more patient available, sending the last " + patients.size()
                        + " entries and removing continuation pointer: " + continuationPointer.getValue());
            } else {
                patients = continuationPointer.getPatients().subList(0, continuationPointer.getNbOfHitsToReturn());
                List<Patient> remainingPatients = continuationPointer.getPatients().subList(
                        continuationPointer.getNbOfHitsToReturn(), continuationPointer.getPatients().size());
                continuationPointer.setPatients(remainingPatients);
                continuationPointer.setLastRequestTime(new Date());
            }
            return patients;
        } else {
            log.error("pointer " + pointer + " is not registered");
            // TODO how to behave when the received pointer is unknown ?
            return null;
        }
    }

    /**
     * Returns the list of encounters for a given pointer
     *
     * @param pointer a {@link java.lang.String} object.
     * @param nbOfHits TODO
     * @return a {@link java.util.List} object.
     */
    public List<Movement> getMovementListForPointer(String pointer, Integer nbOfHits) {
        List<Movement> movements = null;
        ContinuationPointer continuationPointer = pointers.get(pointer);
        continuationPointer.setNbOfHitsToReturn(nbOfHits);
        if (continuationPointer.getMovements().size() <= continuationPointer.getNbOfHitsToReturn()) {
            movements = continuationPointer.getMovements();
            pointers.remove(pointer);
            log.info("No more movement available, removing continuation pointer: " + continuationPointer.getValue());
        } else {
            movements = continuationPointer.getMovements().subList(0, continuationPointer.getNbOfHitsToReturn());
            List<Movement> remainingEncounters = continuationPointer.getMovements().subList(
                    continuationPointer.getNbOfHitsToReturn(), continuationPointer.getMovements().size());
            continuationPointer.setMovements(remainingEncounters);
            continuationPointer.setLastRequestTime(new Date());
        }
        return movements;
    }

    /**
     * <p>getRemainingQuantity.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getRemainingQuantity(String pointer) {
        if (pointerExists(pointer)) {
            return pointers.get(pointer).getPatients().size();
        } else {
            return 0;
        }
    }

    /**
     * <p>getTotalQuantity.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getTotalQuantity(String pointer) {
        if (pointerExists(pointer)) {
            return pointers.get(pointer).getResultTotalQuantity();
        } else {
            return 0;
        }
    }

    /**
     * <p>getQueryByParameter.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     */
    public PRPAMT201306UV02QueryByParameter getQueryByParameter(String pointer) {
        if (pointerExists(pointer)) {
            return pointers.get(pointer).getQueryByParameter();
        } else {
            return null;
        }
    }

    /**
     * Create a new ContinuationPointer instance for future use
     *
     * @param pointer           TODO
     * @param nbOfHits a {@link java.lang.Integer} object.
     * @param retrievedPatients a {@link java.util.List} object.
     * @param retrievedMovement a {@link java.util.List} object.
     * @return a {@link net.ihe.gazelle.simulator.hl7v3.responder.ContinuationPointer} object.
     */
    public ContinuationPointer addPointer(String pointer, Integer nbOfHits, List<Patient> retrievedPatients,
                                          List<Movement> retrievedMovement) {
        ContinuationPointer newPointer = new ContinuationPointer(pointer, nbOfHits);
        newPointer.setPatients(retrievedPatients);
        newPointer.setMovements(retrievedMovement);
        newPointer.setQueryByParameter(null);
        pointers.put(pointer, newPointer);
        return newPointer;
    }

    /**
     * <p>addPDQv3Pointer.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @param nbOfHits a {@link java.lang.Integer} object.
     * @param retrievedPatients a {@link java.util.List} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     * @param numberOfResults a {@link java.lang.Integer} object.
     * @param domainsToReturn a {@link java.util.List} object.
     * @return a {@link net.ihe.gazelle.simulator.hl7v3.responder.ContinuationPointer} object.
     */
    public ContinuationPointer addPDQv3Pointer(String pointer, Integer nbOfHits, List<Patient> retrievedPatients,
                                               PRPAMT201306UV02QueryByParameter queryByParameter, Integer numberOfResults,
                                               List<HierarchicDesignator> domainsToReturn) {
        ContinuationPointer newPointer = new ContinuationPointer(pointer, nbOfHits);
        newPointer.setPatients(retrievedPatients);
        newPointer.setMovements(null);
        newPointer.setQueryByParameter(queryByParameter);
        newPointer.setResultTotalQuantity(numberOfResults);
        if (domainsToReturn != null) {
            newPointer.setDomainsToReturn(domainsToReturn);
        }
        pointers.put(pointer, newPointer);
        return newPointer;
    }

    /**
     * When the system receives a J01 event, remove the pointer from the list
     *
     * @param queryTag a {@link java.lang.String} object.
     * @return true if the pointer exists
     */
    public boolean cancelQuery(String queryTag) {
        boolean pointerExists = false;
        for (Map.Entry<String, ContinuationPointer> pointer : pointers.entrySet()) {
            if (pointer.getKey().startsWith(queryTag)) {
                log.info("Cancelling query with continuation pointer: " + pointer.getKey());
                pointerExists = true;
                pointers.remove(pointer.getKey());
                break;
            }
        }
        return pointerExists;
    }

    /**
     * <p>cancelPDQv3Query.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean cancelPDQv3Query(String pointer) {
        return (pointers.remove(pointer) != null);
    }

    /**
     * Removes all pointers for which no request has been received within the last hour
     */
    public void clear() {
        if (pointers == null || pointers.entrySet() == null || pointers.entrySet().isEmpty()) {
            // nothing to do;
            return;
        } else {
            Calendar calendar = Calendar.getInstance();
            if (calendar.get(Calendar.HOUR_OF_DAY) > 0) {
                calendar.add(Calendar.HOUR_OF_DAY, -1);
            } else {
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.DAY_OF_YEAR, -1);
            }
            calendar.add(Calendar.MINUTE, -1);
            Iterator<Map.Entry<String, ContinuationPointer>> iterator = pointers.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, ContinuationPointer> entry = iterator.next();
                ContinuationPointer pointer = entry.getValue();
                if (pointer.getLastRequestTime().before(calendar.getTime())) {
                    log.warn("Removing continuation pointer: " + pointer.getValue());
                    pointers.remove(entry.getKey());
                }
            }
        }
    }

    /**
     * <p>getDomainsToReturn.</p>
     *
     * @param pointer a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getDomainsToReturn(String pointer) {
        if (pointerExists(pointer)) {
            return pointers.get(pointer).getDomainsToReturn();
        } else {
            return null;
        }
    }
}
