package net.ihe.gazelle.simulator.pix.hl7;

import ca.uhn.hl7v2.model.v25.datatype.CX;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>QBPMessageDecoder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class QBPMessageDecoder {

	private QBP_Q21 qbpMessage;
	private List<HierarchicDesignator> domainsToReturn;
	private QueryResponseStatus status;

	/**
	 * <p>getQueryResponseStatus.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pix.hl7.QueryResponseStatus} object.
	 */
	public QueryResponseStatus getQueryResponseStatus() {
		return this.status;
	}

	/**
	 * <p>Constructor for QBPMessageDecoder.</p>
	 *
	 * @param incomingMessage a {@link net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21} object.
	 */
	public QBPMessageDecoder(QBP_Q21 incomingMessage) {
		this.qbpMessage = incomingMessage;
		this.domainsToReturn = new ArrayList<HierarchicDesignator>();
		this.status = null;
	}

	/**
	 * <p>getListOfPatientIdentifiers.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<PatientIdentifier> getListOfPatientIdentifiers(EntityManager entityManager) {
		PatientQuery query = new PatientQuery(new HQLQueryBuilder<Patient>(entityManager, Patient.class));
		query.stillActive().eq(true);
		query.simulatedActor().keyword().in(getActorKeywords());

		String personId = qbpMessage.getQPD().getPersonIdentifier().getIDNumber().getValue();
		String namespaceID = qbpMessage.getQPD().getPersonIdentifier().getAssigningAuthority().getNamespaceID()
				.getValue();
		String universalServiceID = qbpMessage.getQPD().getPersonIdentifier().getAssigningAuthority().getUniversalID()
				.getValue();
		String universalIDType = qbpMessage.getQPD().getPersonIdentifier().getAssigningAuthority().getUniversalIDType()
				.getValue();

		if ((personId != null) && !personId.isEmpty()) {
			query.patientIdentifiers().idNumber().eq(personId);
		}
		if ((namespaceID != null) && !namespaceID.isEmpty()) {
			query.patientIdentifiers().domain().namespaceID().eq(namespaceID);
		}
		if ((universalServiceID != null) && !universalServiceID.isEmpty()) {
			query.patientIdentifiers().domain().universalID().eq(universalServiceID);
		}
		if ((universalIDType != null) && !universalIDType.isEmpty()) {
			query.patientIdentifiers().domain().universalIDType().eq(universalIDType);
		}
		List<Integer> ids = query.patientIdentifiers().id().getListDistinct();
		if (ids != null && !ids.isEmpty()) {
			Integer matchingIdentifierId = ids.get(0);
			Patient patient = query.getUniqueResult();
			List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
			// check if cross-references exist
			if (patient.getPixReference() != null) {
				for (Patient xpatient : patient.getPixReference().getPatients()) {
					for (PatientIdentifier pid : xpatient.getPatientIdentifiers()) {
						if (!pid.getId().equals(matchingIdentifierId) // do not include the identifier given in the request
								&& (domainsToReturn.isEmpty() || domainsToReturn.contains(pid.getDomain()))) {
							identifiers.add(pid);
						}
					}
				}
			}
			// else list only the identifiers of the patient
			else {
				for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
					if (!pid.getId().equals(matchingIdentifierId) // do not include the identifier given in the request
							&& (domainsToReturn.isEmpty() || domainsToReturn.contains(pid.getDomain()))) {
						identifiers.add(pid);
					}
				}
			}
			if (identifiers.isEmpty()) {
				status = QueryResponseStatus.NF;
			} else {
				status = QueryResponseStatus.OK;
			}
			return identifiers;
		} else {
			status = QueryResponseStatus.AE_UNKNOWN_ID;
			return null;
		}
	}

	/**
	 * PAM-510
	 * @return
	 */
	private List<String> getActorKeywords() {
		return Arrays.asList(PatientManagerConstants.PAT_IDENTITY_X_REF_MGR, PatientManagerConstants.CONNECTATHON);
	}

	/**
	 * parses the QPD-8 field and for each defined domain, check if the PDS knows it
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return the list reps which contain an unknown domain
	 */
	public List<Integer> checkWhatDomainsReturned(EntityManager entityManager) {
		// first check if the domain in QPD-3-4 is known by the tool
		CX personIdentifier = qbpMessage.getQPD().getPersonIdentifier();
		HierarchicDesignator personIdentifierDomain = checkDomainIsKnown(entityManager, personIdentifier
				.getAssigningAuthority().getNamespaceID().getValue(), personIdentifier.getAssigningAuthority()
				.getUniversalID().getValue(), personIdentifier.getAssigningAuthority().getUniversalIDType().getValue());
		if (personIdentifierDomain == null) {
			status = QueryResponseStatus.AE_UNKNOWN_INCOMING_DOMAIN;
			return null;
		}
		List<Integer> unknownDomains = new ArrayList<Integer>();
		CX[] domains = qbpMessage.getQPD().getWhatDomainsReturned();
		Integer index = 0;
		if ((domains != null) && (domains.length > 0)) {
			for (CX domain : domains) {

				String namespaceID = domain.getAssigningAuthority().getNamespaceID().getValue();
				String universalServiceID = domain.getAssigningAuthority().getUniversalID().getValue();
				String universalIDType = domain.getAssigningAuthority().getUniversalIDType().getValue();
				HierarchicDesignator designator = checkDomainIsKnown(entityManager, namespaceID, universalServiceID,
						universalIDType);
				if (designator == null) {
					unknownDomains.add(index);
				} else {
					domainsToReturn.add(designator);
				}
				index++;
			}
		}
		if (!unknownDomains.isEmpty()) {
			status = QueryResponseStatus.AE_UNKNOWN_DOMAIN_TO_RETURN;
		}
		return unknownDomains;
	}

	private HierarchicDesignator checkDomainIsKnown(EntityManager entityManager, String namespaceID,
			String universalID, String universalIDType) {
		PatientQuery query = new PatientQuery(new HQLQueryBuilder<Patient>(entityManager, Patient.class));
		query.simulatedActor().keyword().in(getActorKeywords());
		if ((namespaceID != null) && !namespaceID.isEmpty()) {
			query.patientIdentifiers().domain().namespaceID().eq(namespaceID);
		}
		if ((universalID != null) && !universalID.isEmpty()) {
			query.patientIdentifiers().domain().universalID().eq(universalID);
		}
		if ((universalIDType != null) && !universalIDType.isEmpty()) {
			query.patientIdentifiers().domain().universalIDType().eq(universalIDType);
		}
		List<HierarchicDesignator> designators = query.patientIdentifiers().domain().getListDistinct();
		if ((designators == null) || designators.isEmpty()) {
			return null;
		} else {
			return designators.get(0);
		}
	}
}
