package net.ihe.gazelle.simulator.pix.consumer;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pix.hl7.PIXMessageBuilder;
import net.ihe.gazelle.simulator.pix.hl7.RSPMessageDecoder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xfs on 25/04/16.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixQueryManager")
@Scope(ScopeType.PAGE)
public class PIXQueryManager extends AbstractPIXQueryManager {

    /** Constant <code>ITI_9="ITI-9"</code> */
    public static final String ITI_9 = "ITI-9";

    private List<HL7V2ResponderSUTConfiguration> availableSystems;

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>initialize.</p>
     */
    @Create
    public void initialize() {
        super.initialize();
        availableSystems = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(ITI_9, HL7Protocol.MLLP);
    }

    /** {@inheritDoc} */
    @Override
    public void sendMessage() {
        if (selectedSUT != null) {
            HL7V2ResponderSUTConfiguration selectedHL7v2SUT = (HL7V2ResponderSUTConfiguration) selectedSUT;
            PIXMessageBuilder builder = new PIXMessageBuilder(selectedHL7v2SUT);
            String messageToSend = builder.buildQ21Message(searchedPid, domainsReturned);
            if (messageToSend == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred when building the message, please retry");
            } else {
                Initiator hl7Initiator = new Initiator(selectedHL7v2SUT, "IHE", "PatientManager",
                        Actor.findActorWithKeyword(PAT_IDENTITY_CONSUMER),
                        Transaction.GetTransactionByKeyword(ITI_9), messageToSend, "QBP^Q23^QBP_Q21", "ITI", Actor.findActorWithKeyword(PAT_IDENTITY_X_REF_MGR));
                try {
                    TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
                    if (message != null) {
                        hl7Messages = new ArrayList<TransactionInstance>();
                        hl7Messages.add(message);
                        if (message.getReceivedMessageContent() != null) {
                            try {
                                receivedIdentifiers = RSPMessageDecoder.extractIdentifiersFromRequest(message
                                        .getReceivedMessageContent());
                            } catch (HL7Exception e) {
                                FacesMessages.instance().add(e.getMessage());
                            }
                        }
                    }
                } catch (HL7Exception e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,"An error occurred when sending the message: " + e.getMessage());
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,"Please select a system under test and retry");
        }
    }


    /** {@inheritDoc} */
    @Override
    protected void resetPage() {
        performAnotherTest();
    }

    /** {@inheritDoc} */
    @Override
    public Transaction getTestedTransaction() {
        return Transaction.GetTransactionByKeyword(ITI_9);
    }
}
