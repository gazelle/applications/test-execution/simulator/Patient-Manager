package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.AbstractSegment;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.ST;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.datatype.XTN;
import ca.uhn.hl7v2.model.v25.segment.PID;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.exception.MissingRequiredSegmentException;
import net.ihe.gazelle.HL7Common.exception.SegmentParsingException;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV1;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.ZBE;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>HL7MessageDecoder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7MessageDecoder extends net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder {

    private static final Log log = Logging.getLog(HL7MessageDecoder.class);

    /**
     * Returns the patient contains in the PID segment (create especially for PAM) given as parameter
     *
     * @param segment a {@link net.ihe.gazelle.pam.hl7v2.model.v25.segment.PID} object.
     * @return the Patient created from the content of the PID segment
     */
    public static Patient createPatientFromPIDSegment(net.ihe.gazelle.pam.hl7v2.model.v25.segment.PID segment) throws HL7v2ParsingException {
        try {
            if (segment == null || segment.isEmpty()) {
                throw new MissingRequiredSegmentException("PID", "PID");
            }
        } catch (HL7Exception e) {
            throw new SegmentParsingException("PID", "PID");
        }
        Patient patient = new Patient();
        patient.setCreationDate(new Date());
        setPatientNames(segment, patient);
        try {
            Date dob = segment.getDateTimeOfBirth().getTime().getValueAsDate();
            patient.setDateOfBirth(dob);
        } catch (DataTypeException e) {
            // unable to parse the date of birth
            log.warn("invalid-date-of-birth", e);
        }
        patient.setGenderCode(segment.getAdministrativeSex().getValue());
        patient.setRaceCode(segment.getRace(0).getIdentifier().getValue());

        patient.setReligionCode(segment.getReligion().getIdentifier().getValue());
        if (segment.getPhoneNumberHome() != null && segment.getPhoneNumberHomeReps() > 0) {
            setPatientPhoneNumbersAndEmail(segment.getPhoneNumberHome(), patient);
        }
        try {
            patient.setAccountNumber(segment.getPatientAccountNumber().encode());
        } catch (HL7Exception e) {
            patient.setAccountNumber(null);
        }

        XAD[] addresses = segment.getPatientAddress();
        setPatientAddresses(patient, addresses, BP6HL7Domain.isBP6Facility((AbstractMessage) segment.getMessage()));
        CX[] identifiers = segment.getPatientIdentifierList();
        setPatientIdentifiersFromCX(patient, identifiers);
        if(segment.getIdentityReliabilityCode() != null && segment.getIdentityReliabilityCode().length > 0) {
            patient.setIdentityReliabilityCode(segment.getIdentityReliabilityCode()[0].toString());
        }
        return patient;
    }

    private static void setPatientNames(net.ihe.gazelle.pam.hl7v2.model.v25.segment.PID segment, Patient patient) throws HL7v2ParsingException {
        XPN[] patientNames = segment.getPatientName();
        if (BP6HL7Domain.isBP6Facility((AbstractMessage) segment.getMessage())) {
            XPN legal = null;
            XPN surname = null;
            XPN displayname = null;
            for (XPN xpn : patientNames) {
                switch (xpn.getNameTypeCode().getValue()) {
                    case "L":
                        legal = xpn;
                        break;
                    case "S":
                        surname = xpn;
                        break;
                    case "D":
                        displayname = xpn;
                        break;
                    default:
                        log.warn("name-type-ignored", xpn.getNameTypeCode());
                }
            }
            if (legal == null) {
                throw new HL7v2ParsingException("legal-name-mandatory");
            }
            if (segment.getIdentityReliabilityCode() != null && segment.getIdentityReliabilityCode().length > 0 && "VALI".equals(segment.getIdentityReliabilityCode()[0].toString())) {
                patient.setAlternateFirstName(legal.getGivenName().getValue());
                String[] m = extractGivenNamesOrInitialsThereofBP6(legal);
                if (m != null) {
                    if (m.length > 0) {
                        patient.setFirstName(m[0]);
                    }
                    if (m.length > 1) {
                        patient.setSecondName(m[1]);
                    }
                    if (m.length > 2) {
                        patient.setThirdName(m[2]);
                    }
                }
            } else {
                fillNamesDefault(patient, patientNames);
            }
            if ("F".equals(segment.getAdministrativeSex().getValue())
                    && (displayname != null && !legal.getFamilyName().equals(displayname.getFamilyName()))) {
                // with MotherMaidenName
                patient.setLastName(displayname.getFamilyName().getSurname().getValue());
                patient.setMotherMaidenName(legal.getFamilyName().getSurname().getValue());
            } else {
                // without MotherMaidenName
                patient.setLastName(legal.getFamilyName().getSurname().getValue());
            }
        } else {
            if ((patientNames != null) && (patientNames.length > 0)) {
                XPN patientName = patientNames[0];
                patient.setFirstName(patientName.getGivenName().getValue());
                patient.setLastName(patientName.getFamilyName().getSurname().getValue());
            }
        }
        patient.setMotherMaidenName(segment.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
    }

    private static void setPatientNames(PID segment, Patient patient) throws HL7v2ParsingException {
        XPN[] patientNames = segment.getPatientName();
        if (BP6HL7Domain.isBP6Facility((AbstractMessage) segment.getMessage())) {
            XPN legal = null;
            XPN surname = null;
            XPN displayname = null;
            for (XPN xpn : patientNames) {
                switch (xpn.getNameTypeCode().getValue()) {
                    case "L":
                        legal = xpn;
                        break;
                    case "S":
                        surname = xpn;
                        break;
                    case "D":
                        displayname = xpn;
                        break;
                    default:
                        log.warn("name-type-ignored", xpn.getNameTypeCode());
                }
            }
            if (legal == null) {
                throw new HL7v2ParsingException("legal-name-mandatory");
            }
            if (segment.getIdentityReliabilityCode() != null && segment.getIdentityReliabilityCode().length > 0 && "VALI".equals(segment.getIdentityReliabilityCode()[0].toString())) {
                patient.setAlternateFirstName(legal.getGivenName().getValue());
                String[] m = extractGivenNamesOrInitialsThereofBP6(legal);
                if (m != null) {
                    if (m.length > 0) {
                        patient.setFirstName(m[0]);
                    }
                    if (m.length > 1) {
                        patient.setSecondName(m[1]);
                    }
                    if (m.length > 2) {
                        patient.setThirdName(m[2]);
                    }
                }
            } else {
                fillNamesDefault(patient, patientNames);
            }
            if ("F".equals(segment.getAdministrativeSex().getValue())
                    && (displayname != null && !legal.getFamilyName().equals(displayname.getFamilyName()))) {
                // with MotherMaidenName
                patient.setLastName(displayname.getFamilyName().getSurname().getValue());
                patient.setMotherMaidenName(legal.getFamilyName().getSurname().getValue());
            } else {
                // without MotherMaidenName
                patient.setLastName(legal.getFamilyName().getSurname().getValue());
            }
        } else {
            fillNamesDefault(patient, patientNames);
        }
        patient.setMotherMaidenName(segment.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
    }

    private static void fillNamesDefault(Patient patient, XPN[] patientNames) {
        if ((patientNames != null) && (patientNames.length > 0)) {
            XPN patientName = patientNames[0];
            patient.setFirstName(patientName.getGivenName().getValue());
            patient.setLastName(patientName.getFamilyName().getSurname().getValue());
        }
    }

    private static void setAlternateName(Patient patient, XPN xpn) {
        patient.setAlternateFirstName(xpn.getGivenName().getValue());
        Matcher m = extractSecondAndFurtherGivenNamesOrInitialsThereof(xpn);
        if (m.find()) {
            patient.setAlternateSecondName(m.group(1));
            patient.setAlternateThirdName(m.group(2));
        }
        patient.setAlternateLastName(xpn.getFamilyName().getSurname().getValue());
    }

    private static final Pattern GIVEN_NAMES_PATTERN = Pattern.compile("\\s*([^\\,\\s]+)(?:[^\\,\\s]*(.*))?");

    private static Matcher extractSecondAndFurtherGivenNamesOrInitialsThereof(XPN legal) {
        ST names = legal.getSecondAndFurtherGivenNamesOrInitialsThereof();
        return GIVEN_NAMES_PATTERN.matcher(names.getValueOrEmpty());
    }

    private static String[] extractGivenNamesOrInitialsThereofBP6(XPN legal) {
        ST names = legal.getSecondAndFurtherGivenNamesOrInitialsThereof();
        return names.getValueOrEmpty().split("\\s+");
    }

    /**
     * Returns the patient contains in the PID segment given as parameter
     *
     * @param segment a {@link ca.uhn.hl7v2.model.v25.segment.PID} object.
     * @return the Patient created from the content of the PID segment
     */
    public static Patient createPatientFromPIDSegment(PID segment) throws HL7v2ParsingException {
        try {
            if (segment == null || segment.isEmpty()) {
                throw new MissingRequiredSegmentException("PID", "PID");
            }
        } catch (HL7Exception e) {
            throw new SegmentParsingException("PID", "PID");
        }
        Patient patient = new Patient();
        patient.setCreationDate(new Date());
        setPatientNames(segment, patient);
        try {
            Date dob = segment.getDateTimeOfBirth().getTime().getValueAsDate();
            patient.setDateOfBirth(dob);
        } catch (DataTypeException e) {
            // unable to parse the date of birth
        }
        patient.setGenderCode(segment.getAdministrativeSex().getValue());
        patient.setRaceCode(segment.getRace(0).getIdentifier().getValue());

        patient.setReligionCode(segment.getReligion().getIdentifier().getValue());
        if (segment.getPhoneNumberHome() != null && segment.getPhoneNumberHomeReps() > 0) {
            setPatientPhoneNumbersAndEmail(segment.getPhoneNumberHome(), patient);
        }
        try {
            patient.setAccountNumber(segment.getPatientAccountNumber().encode());
        } catch (HL7Exception e) {
            patient.setAccountNumber(null);
        }

        XAD[] addresses = segment.getPatientAddress();
        setPatientAddresses(patient, addresses, BP6HL7Domain.isBP6Facility((AbstractMessage) segment.getMessage()));
        CX[] identifiers = segment.getPatientIdentifierList();
        setPatientIdentifiersFromCX(patient, identifiers);
        if(segment.getIdentityReliabilityCode() != null && segment.getIdentityReliabilityCode().length > 0) {
            patient.setIdentityReliabilityCode(segment.getIdentityReliabilityCode()[0].toString());
        }
        return patient;
    }

    private static void setPatientIdentifiersFromCX(Patient patient, CX[] identifiers) {
        if ((identifiers != null) && (identifiers.length > 0)) {
            List<PatientIdentifier> patientIdentifiers = new ArrayList<PatientIdentifier>();
            for (int index = 0; index < identifiers.length; index++) {
                try {
                    PatientIdentifier pid = buildPatientIdentifier(identifiers[index]);
                    patientIdentifiers.add(pid);
                } catch (HL7v2ParsingException e) {
                    log.warn(e.getMessage());
                }
            }
            patient.setPatientIdentifiers(patientIdentifiers);
        } else {
            patient.setPatientIdentifiers(null);
        }
    }

    private static void setPatientPhoneNumbersAndEmail(XTN[] xtnList, Patient patient) {
        for (XTN xtn : xtnList) {
            if (PatientManagerConstants.XTN_USE_EMAIL.equals(xtn.getTelecommunicationUseCode().getValue())) {
                patient.setEmail(xtn.getEmailAddress().getValue());
            } else {
                PatientPhoneNumber phoneNumber = new PatientPhoneNumber();
                phoneNumber.setValue(xtn.getAnyText().getValue());
                phoneNumber.setPatient(patient);
                phoneNumber.setType(PhoneNumberType.getTypeForHL7v2(xtn.getTelecommunicationUseCode().getValue()));
                patient.addPhoneNumber(phoneNumber);
            }
        }
    }

    /**
     * <p>setPatientAddresses.</p>
     *
     * @param patient   a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param addresses an array of {@link ca.uhn.hl7v2.model.v25.datatype.XAD} objects.
     */
    protected static void setPatientAddresses(Patient patient, XAD[] addresses) {
        setPatientAddresses(patient, addresses, false);
    }

    protected static void setPatientAddresses(Patient patient, XAD[] addresses, boolean isBP6) {
        boolean mainAddress = true;
        for (XAD addr : addresses) {
            PatientAddress patientAddress = new PatientAddress();
            patientAddress.setMainAddress(mainAddress);
            patientAddress.setAddressLine(addr.getStreetAddress().getStreetOrMailingAddress().getValue());
            patientAddress.setCity(addr.getCity().getValue());
            patientAddress.setState(addr.getStateOrProvince().getValue());
            patientAddress.setZipCode(addr.getZipOrPostalCode().getValue());
            patientAddress.setCountryCode(addr.getCountry().getValue());
            patientAddress.setPatient(patient);
            if (addr.getAddressType() != null) {
                patientAddress.setAddressType(AddressType.getAddressTypeByHl7code(addr.getAddressType().toString()));
            }
            if (isBP6) {
                patientAddress.setInseeCode(addr.getCountyParishCode().getValue());
            }
            if (mainAddress) {
                patient.setCountryCode(addr.getCountry().getValue());
            }
            patient.addPatientAddress(patientAddress);
            mainAddress = false;
        }
    }

    /**
     * <p>buildPatientIdentifier.</p>
     *
     * @param identifier a {@link ca.uhn.hl7v2.model.v25.datatype.CX} object.
     * @return the PatientIdentifier created from the content of the CX repetition
     */

    public static PatientIdentifier buildPatientIdentifier(CX identifier) throws HL7v2ParsingException {
        String patientId = identifier.getIDNumber().getValue();
        String assigningAuthorityNameSpace = identifier.getAssigningAuthority().getNamespaceID().getValue();
        String assigningAuthorityID = identifier.getAssigningAuthority().getUniversalID().getValue();
        String assigningAuthorityType = identifier.getAssigningAuthority().getUniversalIDType().getValue();
        StringBuilder fullPid = new StringBuilder();
        if ((patientId != null) && !patientId.isEmpty()) {
            fullPid.append(patientId);
        }
        fullPid.append("^^^");
        if ((assigningAuthorityNameSpace != null) && !assigningAuthorityNameSpace.isEmpty()) {
            fullPid.append(assigningAuthorityNameSpace);
        }
        fullPid.append("&");
        if ((assigningAuthorityID != null) && !assigningAuthorityID.isEmpty()) {
            fullPid.append(assigningAuthorityID);
        }
        fullPid.append("&");
        if ((assigningAuthorityType != null) && !assigningAuthorityType.isEmpty()) {
            fullPid.append(assigningAuthorityType);
        }
        if (fullPid.length() > 5) {
            return new PatientIdentifier(fullPid.toString(), identifier.getIdentifierTypeCode().getValue());
        } else {
            throw new HL7v2ParsingException("Cannot build identifier");
        }
    }

    public static Encounter populateEncounterFromPV1Segment(PV1 pv1Segment) throws HL7v2ParsingException {
        try {
            if (pv1Segment == null || pv1Segment.isEmpty()) {
                throw new MissingRequiredSegmentException("PV1", "PV1");
            }
        } catch (HL7Exception e) {
            throw new SegmentParsingException("PV1", "PV1");
        }
        Encounter encounter = new Encounter();
        encounter.setPatientClassCode(pv1Segment.getPatientClass().getValue());
        encounter.setHospitalServiceCode(pv1Segment.getHospitalService().getValue());
        try {
            encounter.setAttendingDoctorCode(pv1Segment.getAttendingDoctor(0).encode());
        } catch (HL7Exception e) {
            log.info("No attending doctor given");
        }
        try {
            encounter.setReferringDoctorCode(pv1Segment.getReferringDoctor(0).encode());
        } catch (HL7Exception e) {
            log.info("No referring doctor given");
        }
        try {
            encounter.setVisitNumber(pv1Segment.getVisitNumber().encode());
        } catch (HL7Exception e) {
            log.warn("visit number is not given !!!");
            encounter.setVisitNumber(null);
        }
        try {
            encounter.setAdmitDate(pv1Segment.getAdmitDateTime().getTime().getValueAsDate());
        } catch (NullPointerException | DataTypeException e) {
            log.info("No admit date given");
        }
        try {
            encounter.setDischargeDate(pv1Segment.getDischargeDateTime(0).getTime().getValueAsDate());
        } catch (NullPointerException | DataTypeException e) {
            log.info("No discharge date given");
        }
        try {
            encounter.setAdmittingDoctorCode(pv1Segment.getAdmittingDoctor(0).encode());
        } catch (NullPointerException | HL7Exception e) {
            log.info("No admitting doctor provided");
        }
        return encounter;
    }

    /**
     * <p>createEncounterForPatientFromPV1Segment.</p>
     *
     * @param pv1Segment a {@link net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV1} object.
     * @param inPatient  a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return the Encounter created from the content of the PV1 segment
     */
    public static Encounter createEncounterForPatientFromPV1Segment(PV1 pv1Segment, Patient inPatient) throws
            HL7v2ParsingException {
        Encounter encounter = populateEncounterFromPV1Segment(pv1Segment);
        encounter.setPatient(inPatient);
        return encounter;
    }

    /**
     * <p>fillMovementWithPV1AndZBE.</p>
     *
     * @param movement   a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     * @param pv1Segment a {@link net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV1} object.
     * @param zbeSegment a {@link net.ihe.gazelle.pam.hl7v2.model.v25.segment.ZBE} object.
     * @return the Movement created from the content of the PV1 and ZBE segments
     */
    public static Movement fillMovementWithPV1AndZBE(Movement movement, PV1 pv1Segment, ZBE zbeSegment) {
        try {
            movement.setAssignedPatientLocation(pv1Segment.getAssignedPatientLocation().encode());
        } catch (HL7Exception e) {
            movement.setAssignedPatientLocation(null);
        }
        try {
            movement.setPriorPatientLocation(pv1Segment.getPriorPatientLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode prior patient location");
        }
        try {
            movement.setPendingPatientLocation(pv1Segment.getPendingLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode pending patient location");
        }
        try {
            movement.setPriorTemporaryLocation(pv1Segment.getPriorTemporaryLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode prior temporary location");
        }
        try {
            movement.setTemporaryPatientLocation(pv1Segment.getTemporaryLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode temporary patient location");
        }
        if (zbeSegment != null) {
            try {
                String segmentAsString = zbeSegment.encode();
                // if the received message does not contain a ZBE segment, segmentAsString = "ZBE"
                if ((segmentAsString == null) || segmentAsString.isEmpty() || (segmentAsString.length() < 4)) {
                    log.warn("No ZBE segment found");
                } else {
                    try {
                        movement.setMovementUniqueId(zbeSegment.getMovementID(0).encode());
                    } catch (HL7Exception e) {
                        // TODO Auto-generated catch block
                        log.warn("Unable to parse the movement ID");
                    }
                    try {
                        movement.setMovementDate(zbeSegment.getStartMovementDateTime().getTime().getValueAsDate());
                    } catch (DataTypeException e) {
                        // TODO Auto-generated catch block
                        log.error(e.getMessage(), e);
                    }
                }
            } catch (HL7Exception e) {
                log.error("unable to encode ZBE segment");
            }
        } else {
            log.info("No ZBE segment");
        }
        return movement;
    }
}
