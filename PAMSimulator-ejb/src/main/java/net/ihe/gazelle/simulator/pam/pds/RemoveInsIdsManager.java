package net.ihe.gazelle.simulator.pam.pds;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>ChangePatientIdManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("removeInsIdsManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class RemoveInsIdsManager extends AbstractPatientIdManager implements Serializable {
    private static org.slf4j.Logger log = LoggerFactory.getLogger(RemoveInsIdsManager.class);
    private static final long serialVersionUID = 1L;

    class Failure extends RuntimeException {
        public Failure(Throwable cause) {
            super(cause);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateSelectedEvent() {
        this.canSendMessage = false;
        log.info("updating selected event on bean creation");
        ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
        bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A47_INS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void actionOnSelectedPatient(Patient patient) {
        try {
            correctPatientIdentifier = null;
            incorrectPatientIdentifier = null;
            patientService.getEntityManager().joinTransaction();
            upgradePatientVersion(patient);
            PatientIdentifier[] pids = selectedPatient.getPatientIdentifiers().toArray(new PatientIdentifier[]{});
            // collect first ins identifier
            for (PatientIdentifier pid : pids) {
                if (BP6HL7Domain.INS.hasINSAuthority(pid) && incorrectPatientIdentifier == null) {
                    // prepare remove message parameters
                    incorrectPatientIdentifier = pid;
                    correctPatientIdentifier = new PatientIdentifier(pid);
                    correctPatientIdentifier.setIdNumber("\"\"");
                    correctPatientIdentifier.setFullPatientId(correctPatientIdentifier.getFullPatientId().replaceFirst("^.*?\\^","\"\"^"));
                    break;
                }
            }
            // send HL7 message
            sendMessage();
            if (hl7Messages !=null&&!hl7Messages.isEmpty()) {
                // check if message was correctly handled by consumer
                patientService.assertAcknowleged(hl7Messages.iterator().next());

                // remove all ins identifiers for this patient
                for (PatientIdentifier pid : pids) {
                    if (BP6HL7Domain.INS.hasINSAuthority(pid)) {
                        selectedPatient = patientService.removeIdentifier(selectedPatient, pid);
                    }
                }
                // change validity status
                selectedPatient.setIdentityReliabilityCode("VIDE");
            }
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,e.getMessage());
            throw new Failure(e);
        } finally {
            selectedPatient = null;
            secondaryPatient = null;
            displayPatientsList = true; // always show all patients with ins identifier
        }
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> params) {
        super.modifyQuery(queryBuilder,params);
        PatientQuery query = new PatientQuery();
        List<HQLRestriction> oidrestrictions = new ArrayList<>();
        for (String oid: BP6HL7Domain.INS.getAuthorities()) {
            oidrestrictions.add(query.patientIdentifiers().fullPatientId().likeRestriction("&"+oid+"&", HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        queryBuilder.addRestriction(
            HQLRestrictions.or(
                    oidrestrictions.toArray(new HQLRestriction[]{})
            ));
    }

    @Override
    public boolean isProfileDedicated() {
        return true;
    }

    @Override
    public boolean isSynchronizedWithSUT() {
        return true;
    }

}
