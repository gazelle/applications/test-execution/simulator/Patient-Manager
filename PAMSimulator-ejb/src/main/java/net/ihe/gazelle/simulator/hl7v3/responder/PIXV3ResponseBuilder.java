package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.coctmt150002UV01.COCTMT150002UV01Organization;
import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization;
import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Acknowledgement;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201304UV02.PRPAMT201304UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201304UV02.PRPAMT201304UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201304UV02.PRPAMT201304UV02Person;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.voc.AcknowledgementDetailType;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.EntityClass;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.hl7v3.voc.ParticipationTargetSubject;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.*;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PIXV3ResponseBuilder extends HL7v3MessageBuilder {

    private static Logger log = LoggerFactory.getLogger(PIXV3ResponseBuilder.class);
    private static final String DATA_SOURCE_VALUE_XPATH = "/PRPA_IN201309UV02/controlActProcess[1]/queryByParameter[1]/parameterList[1]/dataSource" +
            "[$index$]/value[1]";


    /**
     * <p>buildPRPAIN2010UV02Message.</p>
     *
     * @param messageId        a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @param patient          a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param identifiers      a {@link java.util.List} object.
     * @param unknownDomains   a {@link java.util.List} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02QueryByParameter} object.
     * @param sutDeviceOid     a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type} object.
     */
    public static PRPAIN201310UV02Type buildPRPAIN2010UV02Message(II messageId, Patient patient, List<II> identifiers, List<Integer> unknownDomains,
                                                                  PRPAMT201307UV02QueryByParameter queryByParameter, String sutDeviceOid) {
        PRPAIN201310UV02Type response = new PRPAIN201310UV02Type();
        fillResponseHeader(messageId, sutDeviceOid, response);
        PRPAIN201310UV02MFMIMT700711UV01ControlActProcess controlActProcess = new PRPAIN201310UV02MFMIMT700711UV01ControlActProcess();
        controlActProcess.setQueryByParameter(queryByParameter);
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201310UV02", INTERACTION_ID_NAMESPACE, null));
        if (queryByParameter == null) {
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AR.getValue()));
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(null, null, HL7V3ResponseCode.QE.getValue(), "aborted",
                    null));
        } else if (unknownDomains != null) {
            // AE (application error) is returned in Acknowledgement.typeCode (transmission wrapper)
            MCCIMT000300UV01Acknowledgement ack = MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AE.getValue());
            for (Integer index : unknownDomains) {
                ack.addAcknowledgementDetail(MCCIMT00300UV01PartBuilder.populateAcknowledgementDetail(
                        AcknowledgementDetailType.E, "204", DATA_SOURCE_VALUE_XPATH.replace("$index$", index.toString())));
            }
            response.addAcknowledgement(ack);
            // AE (application error) is returned in QueryAck.queryResponseCode (control act wrapper).
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.AE.getValue(),
                    "deliveredResponse", null));
        } else if (identifiers != null) {
            PRPAIN201310UV02MFMIMT700711UV01Subject1 subject = new PRPAIN201310UV02MFMIMT700711UV01Subject1();
            subject.setTypeCode("SUBJ");
            PRPAIN201310UV02MFMIMT700711UV01RegistrationEvent registrationEvent = new PRPAIN201310UV02MFMIMT700711UV01RegistrationEvent();
            registrationEvent.setClassCode(ActClass.REG);
            registrationEvent.setMoodCode(ActMood.EVN);
            registrationEvent.setStatusCode(new CS("active", null, null));
            registrationEvent.setCustodian(MFMIMT700711UV01PartBuilder.populateCustodian());
            PRPAIN201310UV02MFMIMT700711UV01Subject2 subject1 = new PRPAIN201310UV02MFMIMT700711UV01Subject2();
            subject1.setTypeCode(ParticipationTargetSubject.SBJ);
            subject1.setPatient(populatePatient(patient, identifiers));
            registrationEvent.setSubject1(subject1);
            subject.setRegistrationEvent(registrationEvent);
            controlActProcess.addSubject(subject);
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.OK.getValue(),
                    "deliveredResponse", null));
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
        } else {
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
            // NF (not found) is returned in QueryAck.queryResponseCode (control act wrapper).
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.NF.getValue(),
                    "deliveredResponse", null));
        }
        response.setControlActProcess(controlActProcess);
        return response;
    }

    /**
     * Set classCode, statusCode, patientIdentifiers, Id, providerOrganization for a patient.
     * If the EPD mode is set in DB, there is an additional check for an EPR-SPID
     *
     * @param patient
     * @param identifiers
     * @return
     */
    private static PRPAMT201304UV02Patient populatePatient(Patient patient, List<II> identifiers) {
        PRPAMT201304UV02Patient msgPatient = new PRPAMT201304UV02Patient();
        msgPatient.setClassCode("PAT");
        msgPatient.setStatusCode(new CS("active", null, null));
        List<II> patientIdentifiers = new ArrayList<II>();
        List<PRPAMT201304UV02OtherIDs> othersIdsList = new ArrayList<PRPAMT201304UV02OtherIDs>();
        COCTMT150003UV03Organization organization = COCTMT150003UV03PartBuilder
                .populateProviderOrganization("gazelle@ihe-europe.net", false);
        for (II ii : identifiers) {
            patientIdentifiers.add(ii);
            II orgId = new II(ii.getRoot(), null);
            if (!organization.getId().contains(orgId)) {
                organization.addId(orgId);
            }
        }

        msgPatient.setProviderOrganization(organization);

        // Additional constraints when we are testing the Swiss EPR
        boolean epr = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD);
        if (epr) {
            // 1.8.2.1 The otherId MUST contain the EPR-SPID. See also ITI   TF-2b, chapter 3.45.4.2.2.1
            PatientIdentifier eprSpid = getEPRSPIDForPatient(patient);
            if (eprSpid != null) {
                II iiEprSpid = new II(eprSpid.getDomain().getUniversalID(), eprSpid.getIdNumber(), eprSpid.getDomain().getNamespaceID());
                patientIdentifiers.add(iiEprSpid);
            }
        }
        msgPatient.setId(patientIdentifiers);
        msgPatient.setPatientPerson(populatePerson(patient, othersIdsList));
        return msgPatient;
    }

    private static PatientIdentifier getEPRSPIDForPatient(Patient patient) {
        for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
            if (pid.getDomain() != null && PIXV3QueryHandler.SPID_ROOT.equals(pid.getDomain().getUniversalID())) {
                return pid;
            }
        }
        return null;
    }

    private static void setEprSpidAsOtherId(List<II> patientIdentifiers, List<PRPAMT201304UV02OtherIDs> othersIdsList, II ii) {
        COCTMT150002UV01Organization otherIdOrg = COCTMT150002UV01PartBuilder
                .populateProviderOrganization("e-health-suisse", false);
        otherIdOrg.addId(new II(PIXV3QueryHandler.SPID_ROOT, null));
        PRPAMT201304UV02OtherIDs otherIDs = new PRPAMT201304UV02OtherIDs();
        otherIDs.setId(Arrays.asList(ii));
        otherIDs.setClassCode("ROL");
        otherIDs.setScopingOrganization(otherIdOrg);
        othersIdsList.add(otherIDs);
    }

    private static PRPAMT201304UV02Person populatePerson(Patient patient, List<PRPAMT201304UV02OtherIDs> otherIds) {
        PRPAMT201304UV02Person person = new PRPAMT201304UV02Person();
        person.setClassCode(EntityClass.PSN);
        person.setDeterminerCode(EntityDeterminer.INSTANCE);
        person.addName(buildPersonName(patient.getFirstName(), patient.getLastName(), null));
        if (otherIds != null && !otherIds.isEmpty()) {
            person.setAsOtherIDs(otherIds);
        }
        return person;
    }

    /**
     * <p>buildPRPAIN2010UV02MessageErrorCase.</p>
     *
     * @param id               a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @param sutOid           a {@link java.lang.String} object.
     * @param errorMessage     a {@link java.lang.String} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02QueryByParameter} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type} object.
     */
    public static PRPAIN201310UV02Type buildPRPAIN2010UV02MessageErrorCase(II id, String sutOid, String errorMessage,
                                                                           PRPAMT201307UV02QueryByParameter queryByParameter) {
        PRPAIN201310UV02Type response = new PRPAIN201310UV02Type();
        fillResponseHeader(id, sutOid, response);
        MCCIMT000300UV01Acknowledgement ack = MCCIMT00300UV01PartBuilder.populateAcknowledgement(id, HL7V3AcknowledgmentCode.AR.getValue());
        ack.addAcknowledgementDetail(MCCIMT00300UV01PartBuilder
                .populateAcknowledgementDetail(AcknowledgementDetailType.E, "101", errorMessage));
        response.addAcknowledgement(ack);
        PRPAIN201310UV02MFMIMT700711UV01ControlActProcess controlActProcess = new PRPAIN201310UV02MFMIMT700711UV01ControlActProcess();
        controlActProcess.setQueryByParameter(queryByParameter);
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201310UV02", INTERACTION_ID_NAMESPACE, null));
        controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(null, null, HL7V3ResponseCode.QE.getValue(), "aborted", null));
        response.setControlActProcess(controlActProcess);
        return response;
    }

    private static void fillResponseHeader(II id, String sutOid, PRPAIN201310UV02Type response) {
        SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
        response.setITSVersion(VERSION);
        response.addReceiver(MCCIMT00300UV01PartBuilder.buildReceiver(sutOid));
        response.setCreationTime(new TS(sdfDateTime.format(new Date())));
        response.setInteractionId(new II(INTERACTION_ID_NAMESPACE, "PRPA_IN201310UV02"));
        response.setProcessingCode(new CS("T", null, null));
        response.setProcessingModeCode(new CS("T", null, null));
        response.setAcceptAckCode(new CS("NE", null, null));
        MessageIdGenerator generator = MessageIdGenerator.getGeneratorForActor("PAT_IDENTITY_X_REF_MGR");
        if (generator != null) {
            response.setId(new II(generator.getOidForMessage(), generator.getNextId()));
        } else {
            log.error("No MessageIdGenerator instance found for actor PAT_IDENTITY_X_REF_MGR");
        }
        response.setSender(MCCIMT00300UV01PartBuilder.buildSender(
                PreferenceService.getString("hl7v3_pix_mgr_device_id"), PreferenceService.getString("pixv3_mgr_url")));
    }
}
