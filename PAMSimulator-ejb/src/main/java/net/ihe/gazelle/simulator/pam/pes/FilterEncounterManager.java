package net.ihe.gazelle.simulator.pam.pes;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.EncounterQuery;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * <b>Class Description : </b>FilterEncounterManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 21/09/15
 */
public class FilterEncounterManager implements QueryModifier<Encounter> {

    private Actor simulatedActor;
    private Patient fixedPatient;
    private String classCode;
    private PatientStatus status;

    private String visitNumber;
    private String patientId;

    /**
     * <p>Constructor for FilterEncounterManager.</p>
     *
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public FilterEncounterManager(Actor actor) {
        this.simulatedActor = actor;
    }


    private Filter<Encounter> filter;

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Encounter> getFilter() {
        if (this.filter == null) {
            this.filter = new Filter<Encounter>(getHQLCriteriaForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Encounter> getHQLCriteriaForFilter() {
        EncounterQuery query = new EncounterQuery();
        HQLCriterionsForFilter<Encounter> criterionsForFilter = query.getHQLCriterionsForFilter();
        if (fixedPatient == null) {
            criterionsForFilter.addPath("firstName", query.patient().firstName());
            criterionsForFilter.addPath("lastName", query.patient().lastName());
            criterionsForFilter.addPath("gender", query.patient().genderCode());
        }
        if (Identity.instance().isLoggedIn()){
            criterionsForFilter.addPath("creator", query.creator());
        }
        criterionsForFilter.addPath("timestamp", query.creationDate());
        criterionsForFilter.addQueryModifier(this);
        return criterionsForFilter;
    }

    /**
     * <p>getEncounters.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Encounter> getEncounters() {
        return new FilterDataModel<Encounter>(getFilter()) {
            @Override
            protected Object getId(Encounter encounter) {
                return encounter.getId();
            }
        };
    }

    /**
     * <p>reset.</p>
     */
    public void reset(){
        getFilter().clear();
        patientId = null;
        visitNumber = null;
        classCode = null;
        status = null;
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Encounter> hqlQueryBuilder, Map<String, Object> map) {
        EncounterQuery query = new EncounterQuery();
        hqlQueryBuilder.addRestriction(query.open().eqRestriction(true));
        if (simulatedActor != null) {
            hqlQueryBuilder.addRestriction(query.simulatedActor().eqRestriction(simulatedActor));
        }
        if (fixedPatient != null) {
            hqlQueryBuilder.addRestriction(query.patient().eqRestriction(fixedPatient));
        }
        if (fixedPatient == null && patientId != null){
            hqlQueryBuilder.addRestriction(query.patient().patientIdentifiers().fullPatientId().likeRestriction(patientId, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (visitNumber != null){
            hqlQueryBuilder.addRestriction(query.visitNumber().likeRestriction(visitNumber, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (classCode != null){
            hqlQueryBuilder.addRestriction(query.patientClassCode().eqRestriction(classCode));
        }
        if (status != null){
            hqlQueryBuilder.addRestriction(query.patientStatus().eqRestriction(status));
        }
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>fixedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getFixedPatient() {
        return fixedPatient;
    }

    /**
     * <p>Setter for the field <code>fixedPatient</code>.</p>
     *
     * @param fixedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setFixedPatient(Patient fixedPatient) {
        this.fixedPatient = fixedPatient;
    }

    /**
     * <p>Getter for the field <code>classCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * <p>Setter for the field <code>classCode</code>.</p>
     *
     * @param classCode a {@link java.lang.String} object.
     */
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
     */
    public PatientStatus getStatus() {
        return status;
    }

    /**
     * <p>Setter for the field <code>status</code>.</p>
     *
     * @param status a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
     */
    public void setStatus(PatientStatus status) {
        this.status = status;
    }

    /**
     * <p>Getter for the field <code>visitNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVisitNumber() {
        return visitNumber;
    }

    /**
     * <p>Setter for the field <code>visitNumber</code>.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     */
    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    /**
     * <p>Getter for the field <code>patientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * <p>Setter for the field <code>patientId</code>.</p>
     *
     * @param patientId a {@link java.lang.String} object.
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
}
