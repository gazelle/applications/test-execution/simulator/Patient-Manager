package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * <p>MCCIIN000002UV01Parser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MCCIIN000002UV01Parser extends HL7v3MessageParser{

    private static final Logger LOG = LoggerFactory.getLogger(MCCIIN000002UV01Parser.class);

    /**
     * <p>parseXCPDDeferredAcknowledgement.</p>
     *
     * @param response an array of byte.
     * @return a {@link java.lang.String} object.
     */
    public static String parseXCPDDeferredAcknowledgement(byte[] response) {
        if (response == null) {
            return null;
        } else {
            String messageString = new String(response, StandardCharsets.UTF_8);
            messageString = messageString.replace("UTF8", "UTF-8");
            try {
                MCCIIN000002UV01Type ackMessage = HL7V3Transformer.unmarshallMessage(MCCIIN000002UV01Type.class,
                        new ByteArrayInputStream(messageString.getBytes(StandardCharsets.UTF_8)));
                if (!ackMessage.getAcknowledgement().isEmpty()
                        && ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode() != null
                        && ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode().getCode() != null && ackMessage
                        .getAcknowledgement().get(FIRST_OCCURRENCE).getTypeCode().getCode().equals(HL7V3AcknowledgmentCode.AE.getMessage())) {
                    if (ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getAcknowledgementDetail() != null
                            && !ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getAcknowledgementDetail().isEmpty()
                            && ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getAcknowledgementDetail().get(FIRST_OCCURRENCE).getCode() != null) {
                        return ackMessage.getAcknowledgement().get(FIRST_OCCURRENCE).getAcknowledgementDetail().get(FIRST_OCCURRENCE).getCode().getCode();
                    } else {
                        return HL7V3AcknowledgmentCode.AE.getValue();
                    }
                }
            } catch (JAXBException e) {
                LOG.error(e.getMessage());
                return null;
            }
        }
        return null;
    }
}
