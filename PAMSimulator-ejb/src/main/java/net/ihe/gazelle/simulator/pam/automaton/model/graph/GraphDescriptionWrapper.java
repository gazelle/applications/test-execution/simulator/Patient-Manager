package net.ihe.gazelle.simulator.pam.automaton.model.graph;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <p>GraphDescriptionWrapper class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "GraphDescriptions")
@XmlAccessorType(XmlAccessType.FIELD)
public class GraphDescriptionWrapper {

    @XmlElementRefs(@XmlElementRef(name = "GraphDescription", type = GraphDescription.class))
    private List<GraphDescription> graphDescriptions;

    /**
     * <p>Constructor for GraphDescriptionWrapper.</p>
     */
    public GraphDescriptionWrapper(){

    }

    /**
     * <p>Constructor for GraphDescriptionWrapper.</p>
     *
     * @param graphs a {@link List} object.
     */
    public GraphDescriptionWrapper(List<GraphDescription> graphs) {
        this.graphDescriptions = graphs;
    }


    public List<GraphDescription> getGraphDescriptions() {
        return graphDescriptions;
    }

    public void setGraphDescriptions(List<GraphDescription> graphDescriptions) {
        this.graphDescriptions = graphDescriptions;
    }
}
