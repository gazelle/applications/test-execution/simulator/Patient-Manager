package net.ihe.gazelle.simulator.pam.pec;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ACK;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.utils.ADTDecoder;
import net.ihe.gazelle.simulator.pam.utils.Consumer;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;

import java.util.Arrays;

/**
 * <p>PEC class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PEC extends Consumer {

    private static final String[] ACCEPTED_EVENTS = {"A01", "A04", "A03", "A11", "A13", "A08", "A40", "A02", "A05",
            "A06", "A07", "A12", "A38", "A21", "A22", "Z99",
            "A44", "A54", "A55", "A52", "A53"};
    private static final String[] ACCEPTED_MESSAGE_TYPES = {"ADT"};

    /**
     * {@inheritDoc}
     */
    @Override
    public IHEDefaultHandler newInstance() {
        PEC newHandler = new PEC();
        return newHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Message processMessage(Message receivedMessage) throws HL7Exception {
        if (receivedMessage == null) {
            return null;
        }
        // start hibernate transaction
        ACKBuilder ackBuilder = new ACKBuilder(serverApplication, serverFacility, Arrays.asList(ACCEPTED_MESSAGE_TYPES), Arrays.asList
                (ACCEPTED_EVENTS));
        Message response = null;
        try {
            ackBuilder.isMessageAccepted(receivedMessage);
        } catch (HL7v2ParsingException e) {
            response = ackBuilder.buildNack(receivedMessage, e);
        }

        // if no NACK is generated, process the message
        if (response == null) {
            ADTDecoder decoder = new ADTDecoder(domain, simulatedTransaction, simulatedActor, ackBuilder,
                    entityManager, this);
            String triggerEvent = terser.get("/.MSH-9-2");
            if (triggerEvent != null) {
                response = decoder.decode(receivedMessage, triggerEvent);
            }
        }

        if (response != null) {
            if (domain != null && domain.getKeyword().equals("ITI-FR")) {
                ((ACK) response).getMSH().getVersionID().getVid2_InternationalizationCode().getCe1_Identifier().setValue
                        (PatientManagerConstants.FRA);
                ((ACK) response).getMSH().getVersionID().getVid3_InternationalVersionID().getCe1_Identifier().setValue(PatientManagerConstants
                        .PAM_FR_VERSION);
            }
            logIfError(response, entityManager);
        }
        setSutActor(Actor.findActorWithKeyword(getSutActorKeyword(), entityManager));
        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        return response;
    }

    @Override
    public String getSutActorKeyword() {
        return "PES";
    }
}