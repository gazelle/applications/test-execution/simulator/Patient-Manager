package net.ihe.gazelle.simulator.pdqv3.pds;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.simulator.common.ihewsresp.IHESoapConstant;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.hl7v3.responder.PDQV3QueryHandler;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.Addressing;

@Stateless
@Name("PDQSupplierService")
@WebService(portName = "PDQSupplier_Port_Soap12", name = "PDQSupplier_PortType", targetNamespace = "urn:ihe:iti:pdqv3:2007", serviceName = "PDQSupplier_Service")
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
@Addressing(required = true)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding
@GenerateInterface(value = "PDQSupplierServiceRemote", isLocal = false, isRemote = true)
@HandlerChain(file = "soap-handler.xml")
public class PDQSupplierService implements PDQSupplierServiceRemote {

    private static Logger log = LoggerFactory.getLogger(PDQSupplierService.class);

    @Resource
    private WebServiceContext context;

    @WebMethod(operationName = "PDQSupplier_PRPA_IN201305UV02", action = "urn:hl7-org:v3:PRPA_IN201305UV02")
    @WebResult(name = "PRPA_IN201306UV02", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS)
    @Action(input = "urn:hl7-org:v3:PRPA_IN201305UV02", output = "urn:hl7-org:v3:PRPA_IN201306UV02")
    public PRPAIN201306UV02Type findCandidatesQuery(
            @WebParam(name = "PRPA_IN201305UV02", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS) PRPAIN201305UV02Type request) {
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
        PDQV3QueryHandler queryHandler = new PDQV3QueryHandler(domain,
                Actor.findActorWithKeyword(PatientManagerConstants.PDS), Actor.findActorWithKeyword(PatientManagerConstants.PDC),
                Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_47), getServletRequest(), context.getMessageContext());
        try {
            return queryHandler.handlePRPAIN201305UV02(request);
        } catch (HL7V3ParserException e) {
            log.error(e.getMessage());
            return null;
        }
    }


    @WebMethod(operationName = "PDQSupplier_QUQI_IN000003UV01_Continue", action = "urn:hl7-org:v3:QUQI_IN000003UV01_Continue")
    @WebResult(name = "PRPA_IN201306UV02", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS)
    @Action(input = "urn:hl7-org:v3:QUQI_IN000003UV01_Continue", output = "urn:hl7-org:v3:PRPA_IN201306UV02")
    public PRPAIN201306UV02Type findCandidatesContinue(
            @WebParam(name = "QUQI_IN000003UV01", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS) QUQIIN000003UV01Type request) {
        Domain domain = HL7V3ResponderUtils.getDefaultDomain();
        PDQV3QueryHandler queryHandler = new PDQV3QueryHandler(domain,
                Actor.findActorWithKeyword(PatientManagerConstants.PDS), Actor.findActorWithKeyword(PatientManagerConstants.PDC),
                Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_47), getServletRequest(), context.getMessageContext());
        try {
            return queryHandler.handleFindCandidatesContinue(request);
        } catch (HL7V3ParserException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    @WebMethod(operationName = "PDQSupplier_QUQI_IN000003UV01_Cancel", action = "urn:hl7-org:v3:QUQI_IN000003UV01_Cancel")
    @WebResult(name = "MCCI_IN000002UV01", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS)
    @Action(input = "urn:hl7-org:v3:QUQI_IN000003UV01_Cancel", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
    public MCCIIN000002UV01Type findCandidatesCancel(
            @WebParam(name = "QUQI_IN000003UV01_Cancel", partName = IHESoapConstant.BODY, targetNamespace = IHESoapConstant.HL7V3_NS) QUQIIN000003UV01CancelType request) {
        try {
            Domain domain = HL7V3ResponderUtils.getDefaultDomain();
            PDQV3QueryHandler queryHandler = new PDQV3QueryHandler(domain,
                    Actor.findActorWithKeyword(PatientManagerConstants.PDS), Actor.findActorWithKeyword(PatientManagerConstants.PDC),
                    Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_47), getServletRequest(), context.getMessageContext());
            return queryHandler.handleFindCandidatesCancel(request);
        } catch (HL7V3ParserException e) {
            log.error(e.getMessage());
            return null;
        }
    }


    private HttpServletRequest getServletRequest() {
        return HL7V3Utils.getServletRequestFromContext(context);
    }
}
