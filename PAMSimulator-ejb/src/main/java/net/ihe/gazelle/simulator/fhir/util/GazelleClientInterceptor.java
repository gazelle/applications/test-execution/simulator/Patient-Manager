package net.ihe.gazelle.simulator.fhir.util;

import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.api.IHttpResponse;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;

import java.io.IOException;

/**
 * <p>GazelleClientInterceptor class.</p>
 *
 * @author aberge
 * @version 1.0: 30/11/17
 */

public class GazelleClientInterceptor extends LoggingInterceptor {

    private String requestContent;
    private int httpCode;

    public GazelleClientInterceptor() {
        super();
    }

    @Override
    public void interceptRequest(IHttpRequest theRequest) {
        requestContent = theRequest.getUri();
    }

    public String getRequestContent() {
        return this.requestContent;
    }

    @Override
    public void interceptResponse(IHttpResponse theResponse) throws IOException{
        this.httpCode = theResponse.getStatus();
    }

    public int getHttpCode() {
        return httpCode;
    }
}
