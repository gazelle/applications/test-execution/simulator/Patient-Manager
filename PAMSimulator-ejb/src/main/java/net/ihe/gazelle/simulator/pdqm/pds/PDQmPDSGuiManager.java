package net.ihe.gazelle.simulator.pdqm.pds;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PDQmPDSGuiManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqmPdsGuiManager")
@Scope(ScopeType.PAGE)
public class PDQmPDSGuiManager extends AbstractPDQPDSGuiManager {

    /**
     *
     */
    private static final long serialVersionUID = -2657551111877383278L;
    private String endpointUrl;

    /**
     * <p>getUrlForHL7Messages.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrlForHL7Messages() {
        Actor actor = Actor.findActorWithKeyword(PatientManagerConstants.PDS);
        Transaction transaction = Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_78);
        return "/messages/browser.seam?simulatedActor=" + actor.getId() + "&transaction=" + transaction.getId();
    }

    /** {@inheritDoc} */
    @Override
    @Create
    public void getSimulatorResponderConfiguration() {
        this.endpointUrl = PreferenceService.getString("pdqm_pds_server_url");
    }

    /**
     * <p>Getter for the field <code>endpointUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEndpointUrl() {
        return endpointUrl;
    }

}
