package net.ihe.gazelle.simulator.pam.automaton.model.graph;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;

import java.util.Date;

/**
 * <p>GraphExecutionResultsDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

public class GraphExecutionResultsDAO {

    public static GraphExecutionResults initializeResults(boolean fullmovementcoverage, boolean validatetransactioninstance, boolean bp6Mode,
                                                          GraphDescription graph, HL7V2ResponderSUTConfiguration sut) {
        GraphExecutionResults graphExecutionResults = new GraphExecutionResults();
        graphExecutionResults.setTimestamp(new Date());
        GraphExecution graphExecution = new GraphExecution();
        graphExecution.setFullEdgeCoverage(fullmovementcoverage);
        graphExecution.setValidateTransactionInstance(validatetransactioninstance);
        graphExecution.setBp6mode(bp6Mode);
        graphExecution.setGraph(graph);
        graphExecution.setSut(sut);
        graphExecutionResults.setGraphExecution(graphExecution);
        return graphExecutionResults;
    }

    public static GraphExecutionResults getGraphExecutionResultsById(Integer dbIdentifier){
        GraphExecutionResultsQuery query = new GraphExecutionResultsQuery();
        query.id().eq(dbIdentifier);
        return query.getUniqueResult();
    }
}
