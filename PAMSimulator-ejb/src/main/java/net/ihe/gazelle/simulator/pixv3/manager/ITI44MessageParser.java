package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson;
import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.EN;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.voc.PersonalRelationshipRoleType;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageParser;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.Person;

import java.util.List;

/**
 * <p>ITI44MessageParser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ITI44MessageParser extends HL7v3MessageParser {

    /** Constant <code>PATIENT_XPATH="/PRPA_IN201301UV02/controlActProcess/su"{trunked}</code> */
    protected static final String PATIENT_XPATH = "/PRPA_IN201301UV02/controlActProcess/subject/registrationEvent/subject1/Patient";
    /** Constant <code>PIX_MANAGER_ACTOR_KEYWORD="PAT_IDENTITY_X_REF_MGR"</code> */
    protected static final String PIX_MANAGER_ACTOR_KEYWORD = "PAT_IDENTITY_X_REF_MGR";


    static void setRelationshipName(List<EN> name, Person person) {
        // we hold only one name for person
        EN personName = name.get(FIRST_OCCURRENCE);
        setPersonName(personName, person);
    }


    static void setRelationshipCode(CE code, Person person) {
        if (code != null) {
            person.setRelationshipCode(code.getCode());
        }
    }

    static void parseRelationshipHolder1(Patient patient, int index, Person person, COCTMT030007UVPerson relationshipHolder1) throws
            HL7V3ParserException {
        if (relationshipHolder1 == null) {
            throw new HL7V3ParserException(PATIENT_XPATH + "/personalRelationship[" + index + "]", "PersonalRelationship shall be a person");
        } else if (!relationshipHolder1.getName().isEmpty()) {
            setRelationshipName(relationshipHolder1.getName(), person);
            // in other domains than EPD, the mother's maiden name is stored as a attribute of the patient, not a relationship
            if (PersonalRelationshipRoleType.MTH.value().equals(person.getRelationshipCode())
                    && !SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
                patient.setMotherMaidenName(person.getLastName());
            } else {
                patient.addPersonalRelationship(person);
            }
        } else {
            throw new HL7V3ParserException(PATIENT_XPATH + "/personalRelationship[" + index + "]/relationshipHolder1", "The relationship " +
                    "holder shall have a name");
        }
    }


    /**
     * <p>setPatientsCreator.</p>
     *
     * @param providerOrganization a {@link net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization} object.
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    protected static void setPatientsCreator(COCTMT150003UV03Organization providerOrganization, Patient patient) throws HL7V3ParserException {
        // use the id as it is the only required attribute
        if (providerOrganization != null) {
            if (!providerOrganization.getId().isEmpty()) {
                II ii = providerOrganization.getId().get(FIRST_OCCURRENCE);
                // only the root is allowed here
                patient.setCreator(ii.getRoot());
            }
        }else {
            throw new HL7V3ParserException(PATIENT_XPATH, "providerOrganization is missing");
        }
    }

    static void setPersonIdentifier(Person person, II firstId) {
        StringBuilder identifier = new StringBuilder(firstId.getExtension());
        identifier.append("^^^");
        if (firstId.getAssigningAuthorityName() != null) {
            identifier.append(firstId.getAssigningAuthorityName());
        }
        identifier.append('&');
        identifier.append(firstId.getRoot());
        identifier.append("&ISO");
        person.setIdentifier(identifier.toString());
    }
}
