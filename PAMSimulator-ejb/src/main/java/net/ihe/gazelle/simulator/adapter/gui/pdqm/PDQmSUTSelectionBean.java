package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.simulator.adapter.gui.SUTSelectionBean;
import net.ihe.gazelle.simulator.application.FHIRResponderSUTConfigurationFinder;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

/**
 * Page Bean for SUT Selection for PDQm PDC simulator.
 */
@Name("pdqmSUTSelectionBean")
@Scope(ScopeType.PAGE)
public class PDQmSUTSelectionBean implements SUTSelectionBean<FHIRResponderSUTConfiguration> {

    @In(create = true, value = "fhirResponderSUTConfigurationFinder")
    private FHIRResponderSUTConfigurationFinder sutFinder;

    private FHIRResponderSUTConfiguration selectedSUT;
    private List<FHIRResponderSUTConfiguration> availableSystems;

    /**
     * Empty constructor for injection.
     */
    public PDQmSUTSelectionBean() {
        //Empty
    }

    /**
     * Initiate the list of available systems to act as PDS.
     */
    @Create
    public void initializeAvailableSystems() {
        if (availableSystems == null || availableSystems.isEmpty()) {
            availableSystems = sutFinder.retrieveSUTByTransactionKeyword(PatientManagerConstants.ITI_78);
        }
    }

    /**
     * Getter for the sutFinder property.
     *
     * @return the value of the property.
     */
    public FHIRResponderSUTConfigurationFinder getSutFinder() {
        return sutFinder;
    }

    /**
     * Setter for the sutFinder property.
     *
     * @param sutFinder value to set to the property.
     */
    public void setSutFinder(FHIRResponderSUTConfigurationFinder sutFinder) {
        this.sutFinder = sutFinder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FHIRResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedSUT(FHIRResponderSUTConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FHIRResponderSUTConfiguration> getAvailableSystems() {
        return this.availableSystems;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDiagram() {
        return "diag/pdqmConsumer.png";
    }
}
