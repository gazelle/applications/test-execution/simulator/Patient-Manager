package net.ihe.gazelle.simulator.cat.action;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.exception.HL7MessageBuilderException;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.cat.model.SharingResult;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.EncounterDAO;
import net.ihe.gazelle.simulator.pam.dao.MovementDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.util.IHEPESMessageGenerator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;
import net.ihe.gazelle.simulator.pixv3.source.ITI44MessageBuilder;
import net.ihe.gazelle.simulator.pixv3.source.PatientIdentitySourceSender;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.hibernate.Hibernate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import java.util.Date;

/**
 * <p>CATMessageSender class.</p>
 *
 * @author abe
 * @version 1.0: 06/12/17
 */
@Name("catMessageSender")
@Stateless
public class CATMessageSender implements CATMessageSenderLocal {

    private static final String SENDING_APP = "PatientManager";
    private static final String SENDING_FACILITY = "IHE";

    @In(create = true)
    private PatientService patientService;

    private boolean bp6Mode;

    public boolean isBp6Mode() {
        return bp6Mode;
    }

    public void setBp6Mode(boolean bp6Mode) {
        this.bp6Mode = bp6Mode;
    }

    @Override
    public void sendMessages(SharingResult sharingResult) {
        for (SystemUnderTestPreferences systemUnderTestPreference : sharingResult.getSelectedSystems()) {
            sendMessages(systemUnderTestPreference, sharingResult);
        }
    }

    private void sendMessages(SystemUnderTestPreferences preferences, SharingResult sharingResult) {
        sharingResult.setProcessEnded(false);
        for (Patient patient : sharingResult.getSelectedPatients()) {
            try {
                TransactionInstance transactionInstance = sendMessage(preferences, patient);
                if (transactionInstance != null) {
                    sharingResult.addTransactionInstance(transactionInstance);
                }
            } catch (HL7MessageBuilderException|HL7Exception|SoapSendException e) {
                StringBuilder builder = new StringBuilder("Error when sending patient '");
                builder.append(patient.getFirstName());
                builder.append(' ');
                builder.append(patient.getLastName());
                builder.append("' to system ");
                builder.append(preferences.getSystemUnderTest().getName());
                builder.append(": ");
                builder.append(e.getMessage());
                sharingResult.addIssue(builder.toString());
            }
        }
        sharingResult.setProcessEnded(true);
    }


    private TransactionInstance sendMessage(SystemUnderTestPreferences preferences, Patient patient) throws HL7Exception, HL7MessageBuilderException, SoapSendException {
        SystemConfiguration systemConfiguration = preferences.getSystemUnderTest();
        TransactionInstance transactionInstance = null;
        Hibernate.initialize(patient.getLegalCareModes());
        Hibernate.initialize(patient.getEncounters());
        switch (preferences.getPreferredMessage()) {
            case PIXV3_ADD_RECORD:
                transactionInstance = sendPIXV3Feed((HL7v3ResponderSUTConfiguration) systemConfiguration, patient);
                break;
            case A01_ADT_ADMIT_PATIENT:
                transactionInstance = sendRADAdmissionMessage((HL7V2ResponderSUTConfiguration) systemConfiguration,
                        patient);
                break;
            case A28_PAM_CREATE_PATIENT:
                transactionInstance = sendCreatePatientMessage((HL7V2ResponderSUTConfiguration) systemConfiguration, patient);
                break;
            case A01_PAM_ADMIT_PATIENT:
                transactionInstance = sendPAMAdmissionMessage((HL7V2ResponderSUTConfiguration) systemConfiguration, patient);
                break;
            case A01_PIX_ADMIT_PATIENT:
                transactionInstance = sendPIXFeed((HL7V2ResponderSUTConfiguration) systemConfiguration,
                        patient);
                break;
        }
        return transactionInstance;
    }

    private TransactionInstance sendPAMAdmissionMessage(HL7V2ResponderSUTConfiguration systemConfiguration,
                                                        Patient patient) throws HL7Exception, HL7MessageBuilderException {
        return sendA01Message(systemConfiguration, patient, "ITI-31", "PES", "PEC", "ITI");
    }

    private TransactionInstance sendCreatePatientMessage(HL7V2ResponderSUTConfiguration systemConfiguration,
                                                         Patient patient) throws HL7Exception {
        return patientService.sendADTMessage(
                new PatientService.Exchange(
                        systemConfiguration,
                        SENDING_APP,
                        SENDING_FACILITY,
                        Actor.findActorWithKeyword(PatientManagerConstants.PDS),
                        Actor.findActorWithKeyword(PatientManagerConstants.PDC),
                        Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_30),
                        getHL7Domain(),
                        PatientService.ADT.A28,
                        patient
                )
        ).iterator().next();
    }

    private TransactionInstance sendPIXFeed(HL7V2ResponderSUTConfiguration systemConfiguration,
                                            Patient patient) throws HL7Exception, HL7MessageBuilderException {
        return sendA01Message(systemConfiguration, patient, "ITI-8", "PAT_IDENTITY_SRC", "PAT_IDENTITY_X_REF_MGR", "ITI");
    }

    private TransactionInstance sendRADAdmissionMessage(HL7V2ResponderSUTConfiguration systemConfiguration, Patient patient) throws HL7Exception, HL7MessageBuilderException {
        return sendA01Message(systemConfiguration, patient, "RAD-1", "ADT", "MPI", "RAD");
    }

    private TransactionInstance sendA01Message(HL7V2ResponderSUTConfiguration sutConfiguration, Patient patient, String transactionKeyword, String
            sendingActorKeyword, String sutActorKeyword, String domainKeyword) throws HL7Exception, HL7MessageBuilderException {
        IHEPESMessageGenerator builder = new IHEPESMessageGenerator(false);
        final Actor simulatedActor = Actor.findActorWithKeyword(sendingActorKeyword);
        final Actor sutActor = Actor.findActorWithKeyword(sutActorKeyword);
        final Transaction transaction = Transaction.GetTransactionByKeyword(transactionKeyword);
        final Encounter encounter = getPatientEncounter(patient);
        builder.setAttributes(encounter, getMovement(encounter), patient, "A01",
                false, null, sutConfiguration, SENDING_APP, SENDING_FACILITY, transactionKeyword);
        String messageToSend = builder.build();
        return sendHL7v2Message(sutConfiguration, domainKeyword, builder.getMessageType(), simulatedActor, sutActor, transaction, messageToSend);
    }

    private TransactionInstance sendHL7v2Message(HL7V2ResponderSUTConfiguration sutConfiguration, String domainKeyword, String messageType, Actor
            simulatedActor, Actor sutActor, Transaction transaction, String messageToSend) throws HL7Exception {
        Initiator initiator = new Initiator(sutConfiguration, SENDING_FACILITY, SENDING_APP, simulatedActor,
                transaction, messageToSend, messageType, domainKeyword, sutActor);
        return initiator.sendMessageAndGetTheHL7Message();
    }

    private Movement getMovement(Encounter encounter) {
        if (encounter.getMovements() != null && !encounter.getMovements().isEmpty()) {
            return encounter.getMovements().get(0);
        } else {
            Movement movement = new Movement(encounter);
            movement.setActionType(ActionType.INSERT);
            movement.setTriggerEvent("A01");
            movement.setCurrentMovement(true);
            movement.setPendingMovement(false);
            movement.setLastChanged(new Date());
            movement.setMovementDate(new Date());
            movement.setPatientClassCode("I");
            MovementDAO.randomlyFillMovement(movement, false);
            return movement.save(EntityManagerService.provideEntityManager());
        }
    }

    private Encounter getPatientEncounter(Patient patient) {
        if (patient.getEncounters() != null && !patient.getEncounters().isEmpty()) {
            return patient.getEncounters().get(0);
        } else {
            // we create the encounter
            Encounter encounter = new Encounter(patient);
            encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            encounter.setPatientClassCode("I");
            EncounterDAO.randomlyFillEncounter(encounter);
            return encounter.save(EntityManagerService.provideEntityManager());
        }
    }


    private TransactionInstance sendPIXV3Feed(HL7v3ResponderSUTConfiguration systemConfiguration, Patient patient) throws SoapSendException {
        PatientIdentitySourceSender sender = new PatientIdentitySourceSender(systemConfiguration);
        ITI44MessageBuilder builder = new ITI44MessageBuilder(systemConfiguration, patient, null);
        PRPAIN201301UV02Type request = builder.buildAddPatientRecordMessage();
        sender.sendAddPatientRecord(request);
        return sender.getTransactionInstance();
    }

    private HL7Domain<?> getHL7Domain() {
        try {
            return isBp6Mode()
                    ? BP6HL7Domain.class.newInstance()
                    : IHEHL7Domain.class.newInstance();
        } catch (InstantiationException|IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


}
