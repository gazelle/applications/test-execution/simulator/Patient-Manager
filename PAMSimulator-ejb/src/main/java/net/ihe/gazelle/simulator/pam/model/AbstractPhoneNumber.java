package net.ihe.gazelle.simulator.pam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * Created by aberge on 03/07/17.
 */

@Entity
@Table(name = "pam_phone_number", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@SequenceGenerator(name = "pam_phone_number_sequence", sequenceName = "pam_phone_number_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AbstractPhoneNumber implements Serializable {

    @Id
    @GeneratedValue(generator = "pam_phone_number_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "value")
    private String value;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private PhoneNumberType type;


    public AbstractPhoneNumber() {
        this.type = PhoneNumberType.HOME;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PhoneNumberType getType() {
        return type;
    }

    public void setType(PhoneNumberType type) {
        this.type = type;
    }



    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractPhoneNumber)) {
            return false;
        }

        AbstractPhoneNumber that = (AbstractPhoneNumber) o;

        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
