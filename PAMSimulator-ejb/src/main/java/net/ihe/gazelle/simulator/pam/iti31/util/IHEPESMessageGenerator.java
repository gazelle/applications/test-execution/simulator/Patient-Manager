package net.ihe.gazelle.simulator.pam.iti31.util;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.datatype.XCN;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.message.ADT_A39;
import ca.uhn.hl7v2.model.v25.message.ADT_A43;
import ca.uhn.hl7v2.model.v25.segment.MRG;
import ca.uhn.hl7v2.parser.PipeParser;
import com.google.common.base.Strings;
import net.ihe.gazelle.HL7Common.exception.HL7MessageBuilderException;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.*;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.*;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.ACC;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.EVN;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.MSH;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.PID;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV1;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.ROL;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.ZBE;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.*;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.commons.lang.time.DateUtils;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>IHEPESMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class IHEPESMessageGenerator extends AbstractHL7MessageGenerator implements PESMessageGenerator {

    /**
     * Constant <code>ITI_31="ITI-31"</code>
     */
    private static final String ITI_31 = "ITI-31";
    private static final String ADT_A_01_STRUC = "ADT_A01";
    private static final String ITI_8 = "ITI-8";
    private static final String RAD_1 = "RAD-1";
    private static final String RAD_12 = "RAD-12";
    private static Log log = Logging.getLog(IHEPESMessageGenerator.class);


    private Encounter aEncounter;
    private Movement aMovement;
    private Patient patient;
    private String triggerEvent;
    private boolean enableHistoric;
    private String messageType;
    private Patient incorrectPatient;
    private SimpleDateFormat sdf;
    private HL7V2ResponderSUTConfiguration selectedSUT;
    private String sendingFacility;
    private String sendingApplication;
    private String transactionKeyword;
    private boolean bp6Mode;
    private TriggerEventEnum eventEnum;

    private HL7Domain hl7Domain = null;

    /**
     * <p>Constructor for IHEPESMessageBuilder.</p>
     *
     * @param bp6 a boolean.
     */
    public IHEPESMessageGenerator(boolean bp6) {
        super(bp6?new BP6PID():new IHEPID());
        this.bp6Mode = bp6;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttributes(Encounter selectedEncounter, Movement selectedMovement, Patient selectedPatient,
                              String triggerEvent, boolean enableHistoric, Patient incorrePatient, HL7V2ResponderSUTConfiguration systemConfiguration,
                              String sendingApplication, String sendingFacility, String transactionKeyword) {
        this.aEncounter = selectedEncounter;
        this.aMovement = selectedMovement;
        this.patient = selectedPatient;
        this.triggerEvent = triggerEvent;
        this.enableHistoric = enableHistoric || bp6Mode;
        this.incorrectPatient = incorrePatient;
        this.sendingApplication = sendingApplication;
        this.sendingFacility = sendingFacility;
        this.selectedSUT = systemConfiguration;
        this.transactionKeyword = transactionKeyword;
        this.restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(systemConfiguration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String build() throws HL7MessageBuilderException {
        eventEnum = TriggerEventEnum.getEventForTrigger(triggerEvent);
        if (eventEnum == null) {
            throw new HL7MessageBuilderException("Event shall not be null at this point");
        }
        sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        messageType = eventEnum.getMessageType();
        Message message;
        switch (eventEnum) {
            case A01:
                // A01: admit
                message = createA01Message();
                break;
            case A04:
                // A04: register
                message = createA04Message();
                break;
            case A11:
                // A11: cancel admit/register
                message = createA11Message();
                break;
            case A03:
                // A03: discharge
                message = createA03Message();
                break;
            case A13:
                // A13: cancel discharge
                message = createADTA01Message(ActionType.CANCEL);
                break;
            case A08:
                // A08: update patient data
                message = createA08Message();
                break;
            case A40:
                // A40: merge patient identifiers
                message = createA40Message();
                break;
            case Z99:
                // Z99: update
                message = createADTA01Message(ActionType.UPDATE);
                break;
            case A05:
                // A05: pre-admit
                message = createA05Message();
                break;
            case A06:
                // A06: change Outpatient to inpatient
                message = createA06Message();
                break;
            case A07:
                // A07: change inpatient to outpatient
                message = createA07Message();
                break;
            case A02:
                // A02: transfer
                message = createA02Message();
                break;
            case A38:
                // A38: cancel pre-admission
                message = createA38Message();
                break;
            case A12:
                // A12: cancel transfer
                message = createA12Message();
                break;
            case A21:
                message = createA21Message();
                break;
            case A22:
                message = createA22Message();
                break;
            case A44:
                message = createA44Message();
                break;
            case A52:
                message = createA52Message();
                break;
            case A53:
                message = createA53Message();
                break;
            case A54:
                message = createA54Message();
                break;
            case A55:
                message = createA55Message();
                break;
            default:
                throw new HL7MessageBuilderException("event " + eventEnum.getTriggerEvent() + " is not yet supported");

        }
        PipeParser parser = new PipeParser();
        try {
            String messageString = parser.encode(message);
            if (transactionKeyword.equals(ITI_8)) // thus 2.3.1 must be used
            {
                messageString = messageString.replace("|2.5|", "|2.3.1|");
            } else if (transactionKeyword.equals(RAD_12) || transactionKeyword.equals(RAD_1)) { //[PAM-469] SWF.b uses HL7v2.5.1
                messageString = messageString.replace("|2.5|", "|2.5.1|");
            }
            return messageString;
        } catch (HL7Exception e) {
            throw new HL7MessageBuilderException(e);
        }
    }

    private Message createA52Message() throws HL7MessageBuilderException {
        ADT_A52 adtMessage = new ADT_A52();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.CANCEL, null, null);
        return adtMessage;
    }

    private Message createA53Message() throws HL7MessageBuilderException {
        ADT_A52 adtMessage = new ADT_A52();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.CANCEL, null, null);
        setExpectedLOAReturnDate(adtMessage.getPV2());
        return adtMessage;
    }

    private void setExpectedLOAReturnDate(net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV2 pv2) {
        try {
            if (aEncounter.getExpectedLOAReturn() == null) {
                Date dayafter = DateUtils.addDays(new Date(), 1);
                aEncounter.setExpectedLOAReturn(dayafter);
            }
            pv2.getExpectedLOAReturnDateTime().getTime().setValue(sdf.format(aEncounter.getExpectedLOAReturn()));
        } catch (DataTypeException e) {
            log.error(e.getMessage(), e);
        }
    }

    private Message createA55Message() throws HL7MessageBuilderException {
        ADT_A52 adtMessage = new ADT_A52();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, null);
        return adtMessage;
    }

    private Message createA54Message() throws HL7MessageBuilderException {
        ADT_A54 adtMessage = new ADT_A54();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, rolList);
        return adtMessage;
    }

    private Message createA44Message() throws HL7MessageBuilderException {
        ADT_A43 adtMessage = new ADT_A43();
        AbstractHL7MessageGenerator generator = bp6Mode ? new BP6HL7MessageGenerator() : this;
        try {
            ca.uhn.hl7v2.model.v25.segment.MSH mshSegment = adtMessage.getMSH();
            ca.uhn.hl7v2.model.v25.segment.EVN evnSegment = adtMessage.getEVN();
            fillMSHSegment(mshSegment, selectedSUT.getApplication(), selectedSUT.getFacility(),
                    sendingApplication, sendingFacility, "ADT", "A44", "ADT_A43", null);
            fillEVNSegment(evnSegment);
            // PID-3: patient to link to the account in MRG-3
            setRestrictedDomain(restrictedDomain);
            generator.fillPIDSegment(adtMessage.getPATIENT().getPID(), patient, false, false);
            MRG mrgSegment = adtMessage.getPATIENT().getMRG();
            // MRG-3 prior patient account number to be associated with PID-3
            mrgSegment.getPriorPatientAccountNumber().parse(incorrectPatient.getAccountNumber());
            int index = 0;
            // MRG-1 prior patient identifier list => association to be removed
            for (PatientIdentifier identifier : incorrectPatient.getPatientIdentifiers()) {
                setCX(mrgSegment.getPriorPatientIdentifierList(index), identifier.splitPatientId(), identifier
                        .getIdentifierTypeCode());
                index++;
            }
            if (selectedSUT.getCharset() != null) {
                mshSegment.getCharacterSet(0).setValue(selectedSUT.getCharset().getHl7Code());
            }

        } catch (HL7Exception e) {
            throw new HL7MessageBuilderException(e);
        }
        return adtMessage;
    }

    private Message createA22Message() throws HL7MessageBuilderException {
        Message msg = createA21Message();
        if (msg instanceof ADT_A21) {
            ADT_A21 adtMessage = (ADT_A21) msg;
            try {
                if (aEncounter.getLoaReturnDate() == null) {
                    Date dayplusone = DateUtils.addDays(new Date(), 1);
                    aEncounter.setLoaReturnDate(dayplusone);
                }
                adtMessage.getEVN().getEventOccurred().getTime().setValue(sdf.format(aEncounter.getLoaReturnDate()));
            } catch (DataTypeException e) {
                throw new HL7MessageBuilderException("Cannot set time", e);
            }
            return adtMessage;
        } else {
            throw new HL7MessageBuilderException("Unexpected message type");
        }
    }

    private Message createA21Message() throws HL7MessageBuilderException {
        ADT_A21 adtMessage = new ADT_A21();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, null);
        setExpectedLOAReturnDate(adtMessage.getPV2());
        return adtMessage;
    }

    private void fillDefaultMessage(MSH msh, EVN evn, PID pid, PV1 pv1, ZBE zbe, ActionType action, ACC acc, List<ROL> rolList) throws
            HL7MessageBuilderException {
        try {
            fillMSHSegment(msh, eventEnum.getTriggerEvent(), eventEnum.getMessageStructure());
            fillEVNSegment(evn, null, aMovement.getMovementDate());
            fillPIDSegment(pid);
            fillPV1Segment(pv1);
            // TODO first fix the validation before adding those segments in the message
           /* if (rolList != null) {
                int rolIndex = 0;
                if (aEncounter.getAttendingDoctorCode() != null) {
                    fillROLSegment(rolList.get(rolIndex), aEncounter.getAttendingDoctorCode(), "AT");
                    rolIndex++;
                }
                if (aEncounter.getAdmittingDoctorCode() != null) {
                    fillROLSegment(rolList.get(rolIndex), aEncounter.getAdmittingDoctorCode(), "AD");
                    rolIndex++;
                }
                if (aEncounter.getReferringDoctorCode() != null) {
                    fillROLSegment(rolList.get(rolIndex), aEncounter.getReferringDoctorCode(), "RP");
                }
            }*/
            if (enableHistoric) {
                fillZBESegment(zbe, action);
            }
            if (acc != null) {
                fillACCSegment(acc);
            }
        } catch (HL7Exception e) {
            throw new HL7MessageBuilderException("Cannot build message for event " + eventEnum.getTriggerEvent() + ": " + e.getMessage(), e);
        }
    }

    private void fillROLSegment(ROL rol, String doctorCode, String actionCode) throws HL7Exception {
        rol.getActionCode().setValue(actionCode);
        setXCNForDoctor(rol.getRolePerson(0), doctorCode);
        if (eventEnum.equals(TriggerEventEnum.A54)) {
            rol.getRoleBeginDateTime().getTime().setValue(sdf.format(new Date()));
            rol.getActionCode().setValue("UP");
        }
    }

    /**
     * 3.31.7.12 Cancel transfer (ADT^A12^ADT_A12) Rev8.0 p63
     *
     * @return
     */
    private Message createA12Message() throws HL7MessageBuilderException {
        ADT_A12 adtMessage = new ADT_A12();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.CANCEL, null, null);
        return adtMessage;
    }

    /**
     * 3.31.7.8 Cancel Pre-Admit (ADT^A38^ADT_A38) Rev8.0 p58
     *
     * @return
     */
    private Message createA38Message() throws HL7MessageBuilderException {
        ADT_A38 adtMessage = new ADT_A38();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.CANCEL, null, null);
        return adtMessage;
    }

    /**
     * 3.31.7.11 Transfer a patient (ADT^A02^ADT_A02) Rev8.0 p62
     *
     * @return
     */
    private Message createA02Message() throws HL7MessageBuilderException {
        ADT_A02 adtMessage = new ADT_A02();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, rolList);
        return adtMessage;
    }

    /**
     * 3.31.7.10 Change Inpatient to Outpatient (ADT^A07^ADT_A06) Rev8.0 p60
     *
     * @return
     */
    private Message createA07Message() throws HL7MessageBuilderException {
        return createA06Message();
    }

    /**
     * 3.31.7.9 Change outpatient to Inpatient (ADT^A06^ADT_A06) Rev8.0 p59
     *
     * @return
     */
    private Message createA06Message() throws HL7MessageBuilderException {
        ADT_A06 adtMessage = new ADT_A06();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, rolList);
        return adtMessage;
    }

    /**
     * 3.31.7.7 Pre-Admit (ADT^A05^ADT_A05) Rev8.0 p56
     *
     * @return
     */
    private Message createA05Message() throws HL7MessageBuilderException {
        ADT_A05 adtMessage = new ADT_A05();
        //[PAM-469] SWF.b uses ADT_A05
        if (transactionKeyword.equals(ITI_8)) {
            this.messageType = "ADT^A05^ADT_A01";
            try {
                fillMSHSegment(adtMessage.getMSH(), "A05", ADT_A_01_STRUC);
                fillEVNSegment(adtMessage.getEVN(), null, aMovement.getMovementDate());
                fillPIDSegment(adtMessage.getPID());
                fillPV1Segment(adtMessage.getPV1());
                return adtMessage;
            } catch (HL7Exception e) {
                throw new HL7MessageBuilderException(e);
            }
        } else {
            try {
                List<ROL> rolList = new ArrayList<ROL>();
                rolList.add(adtMessage.getROL2(0));
                rolList.add(adtMessage.getROL2(1));
                rolList.add(adtMessage.getROL2(2));
                fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                        adtMessage.getZBE(), ActionType.INSERT, null, rolList);
                if (aEncounter.getExpectedAdmitDate() != null) {
                    adtMessage.getPV2().getExpectedAdmitDateTime().getTime()
                            .setValue(sdf.format(aEncounter.getExpectedAdmitDate()));
                }
                return adtMessage;
            } catch (HL7Exception | HL7MessageBuilderException e) {
                throw new HL7MessageBuilderException(e);
            }
        }
    }

    /**
     * 3.31.7.1 Admit inpatient (ADT^A01^ADT_A01) Rev8.0 p48
     *
     * @return
     */
    private Message createA01Message() throws HL7MessageBuilderException {
        return populateADTA01MessageStructure();
    }

    private Message populateADTA01MessageStructure() throws HL7MessageBuilderException {
        ADT_A01 adtMessage = new ADT_A01();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, adtMessage.getACC(), rolList);
        return adtMessage;
    }

    /**
     * 3.31.7.3 Register an outpatient (ADT^A04^ADT_A01) Rev8.0 p50
     *
     * @return
     */
    private Message createA04Message() throws HL7MessageBuilderException {
        return populateADTA01MessageStructure();
    }

    /**
     * 3.31.7.2 Cancel Admit/Visit Notification (ADT^A11^ADT_A09) Rev8.0 p45
     *
     * @return
     */
    private Message createA11Message() throws HL7MessageBuilderException {
        ADT_A09 adtMessage = new ADT_A09();
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.CANCEL, null, null);
        return adtMessage;
    }

    /**
     * 3.31.7.6 Update Patient information (ADT^A08^ADT_A01) Rev8.0 p54
     *
     * @return
     */
    private Message createA08Message() throws HL7MessageBuilderException {

        ca.uhn.hl7v2.model.v25.message.ADT_A01 adtMessage = initADTA01();
        AbstractHL7MessageGenerator generator = bp6Mode ? new BP6HL7MessageGenerator() : this;
        try {
            fillMSHSegment(adtMessage.getMSH(), selectedSUT.getApplication(),
                    selectedSUT.getFacility(), sendingApplication, sendingFacility, "ADT", "A08", ADT_A_01_STRUC,
                    selectedSUT.getCharset().getHl7Code());
            SegmentBuilder.fillEVNSegment(adtMessage.getEVN(), sdf);
            setRestrictedDomain(restrictedDomain);
            generator.fillPIDSegment(adtMessage.getPID(), patient, false, transactionKeyword.equals(ITI_31));
            if (aEncounter != null) {
                adtMessage.getPV1().getPatientClass().setValue(aEncounter.getPatientClassCode());
                adtMessage.getPV1().getHospitalService().setValue(aEncounter.getHospitalServiceCode());
                if (aEncounter.getVisitNumber() != null) {
                    adtMessage.getPV1().getVisitNumber().parse(aEncounter.getVisitNumber());
                    adtMessage.getPV1().getVisitIndicator().setValue("V");
                }
                if (aEncounter.getAdmitDate() != null) {
                    adtMessage.getPV1().getAdmitDateTime().getTime().setValue(sdf.format(aEncounter.getAdmitDate()));
                }
                if (aEncounter.getAttendingDoctorCode() != null) {
                    setXCNForDoctor(adtMessage.getPV1().getAttendingDoctor(0), aEncounter.getAttendingDoctorCode());
                }
            }
            return adtMessage;
        } catch (HL7Exception e) {
            throw new HL7MessageBuilderException(e);
        }
    }

    /**
     * 3.31.7.31 Merge two patients (ADT^A40^ADT_A39) Rev8.0 p83
     *
     * @return
     */
    private Message createA40Message() throws HL7MessageBuilderException {
        ADT_A39 adtMessage = initADTA39();
        AbstractHL7MessageGenerator generator = bp6Mode ? new BP6HL7MessageGenerator() : this;
        try {
            fillMSHSegment(adtMessage.getMSH(), selectedSUT.getApplication(),
                    selectedSUT.getFacility(), sendingApplication, sendingFacility, "ADT", "A40", "ADT_A39",
                    selectedSUT.getCharset().getHl7Code());
            SegmentBuilder.fillEVNSegment(adtMessage.getEVN(), sdf);
            generator.fillPIDSegment(adtMessage.getPATIENT().getPID(), patient, false, transactionKeyword.equals(ITI_31));
            fillMRGSegment(adtMessage.getPATIENT().getMRG(),
                    incorrectPatient.getPatientIdentifiers(), incorrectPatient);
            return adtMessage;
        } catch (HL7Exception e) {
            throw new HL7MessageBuilderException(e);
        }
    }

    /**
     * 3.31.7.4 Discharged/End Visit (ADT^A03^ADT_A03) Rev8.0 p51
     *
     * @return
     */
    private Message createA03Message() throws HL7MessageBuilderException {
        ADT_A03 adtMessage = new ADT_A03();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), ActionType.INSERT, null, rolList);
        return adtMessage;
    }

    /**
     * 3.31.7.5 Cancel Discharge/End visit (ADT^A13^ADT_A01) v8.0 p53
     *
     * @return
     */
    private Message createADTA01Message(ActionType action) throws HL7MessageBuilderException {
        ADT_A01 adtMessage = new ADT_A01();
        List<ROL> rolList = new ArrayList<ROL>();
        rolList.add(adtMessage.getROL2(0));
        rolList.add(adtMessage.getROL2(1));
        rolList.add(adtMessage.getROL2(2));
        fillDefaultMessage(adtMessage.getMSH(), adtMessage.getEVN(), adtMessage.getPID(), adtMessage.getPV1(),
                adtMessage.getZBE(), action, null, rolList);
        return adtMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessageType() {
        return this.messageType;
    }

    /**
     * 3.31.6.1 Movement Action Segment Rev 8.0 p46
     *
     * @param zbeSegment
     * @param action
     *
     * @throws HL7Exception
     */
    private void fillZBESegment(ZBE zbeSegment, ActionType action) throws HL7Exception {
        zbeSegment.getMovementAction().setValue(action.getName());
        zbeSegment.getMovementID(0).parse(aMovement.getMovementUniqueId());
        if (!aMovement.getCurrentMovement() && !aMovement.getPendingMovement()) {
            zbeSegment.getHistoricalMovementIndicator().setValue("Y");
        } else {
            zbeSegment.getHistoricalMovementIndicator().setValue("N");
        }
        if (aMovement.getMovementDate() != null) {
            zbeSegment.getStartMovementDateTime().getTime().setValue(sdf.format(aMovement.getMovementDate()));
        }
        if (!action.equals(ActionType.INSERT)) {
            zbeSegment.getOriginalTriggerEventCode().setValue(aMovement.getTriggerEvent());
        }
        if (aMovement.getWardCode() != null) {
            zbeSegment.getResponsibleMedicalWard().getXon1_OrganizationName().setValue(ValueSet.getDisplayNameForCode
                    ("MEDICAL_WARD", aMovement
                            .getWardCode()));
            zbeSegment.getResponsibleMedicalWard().getXon10_OrganizationIdentifier().setValue(aMovement.getWardCode());
        }
        if (aMovement.getNursingCareWard() != null) {
            zbeSegment.getResponsibleNursingWard().getXon1_OrganizationName().setValue(ValueSet.getDisplayNameForCode
                    ("NURSING_WARD", aMovement
                            .getNursingCareWard()));
            zbeSegment.getResponsibleNursingWard().getXon10_OrganizationIdentifier().setValue(aMovement.getNursingCareWard());
        }
        // this field comes from IHE FRA and was initially named nature of movement
        if (aMovement.getNatureOfMovement() != null) {
            zbeSegment.getMovementScope().getText().setValue(ValueSet.getDisplayNameForCode("HL79106", aMovement
                    .getNatureOfMovement()));
            zbeSegment.getMovementScope().getIdentifier().setValue(aMovement.getNatureOfMovement());
        }
    }

    /**
     * 3.30.5.1 MSH - Message Header Segment Rev8.0 p13
     *
     * @param mshSegment
     * @param triggerEvent
     * @param messageStructure
     *
     * @throws DataTypeException
     */
    private void fillMSHSegment(MSH mshSegment, String triggerEvent, String messageStructure) throws DataTypeException {
        mshSegment.getFieldSeparator().setValue("|");
        mshSegment.getEncodingCharacters().setValue("^~\\&");
        mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
        mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
        mshSegment.getReceivingApplication().getNamespaceID().setValue(selectedSUT.getApplication());
        mshSegment.getReceivingFacility().getNamespaceID().setValue(selectedSUT.getFacility());
        mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
        mshSegment.getMessageType().getMessageCode().setValue("ADT");
        mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
        mshSegment.getMessageType().getMessageStructure().setValue(messageStructure);
        mshSegment.getProcessingID().getProcessingID().setValue("P");
        mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
        mshSegment.getVersionID().getVersionID().setValue("2.5");
        if (bp6Mode) {
            mshSegment.getVersionID().getInternationalizationCode().getIdentifier().setValue(PatientManagerConstants.FRA);
            mshSegment.getVersionID().getInternationalVersionID().getIdentifier().setValue(PatientManagerConstants.PAM_FR_VERSION);
        }
        if (selectedSUT.getCharset() != null) {
            mshSegment.getCharacterSet(0).setValue(selectedSUT.getCharset().getHl7Code());
        }
    }

    /**
     * 3.30.5.2 EVN - Event Type segment Rev8.0 p16 !!! EVN-3 must be populated for event A26 and A15, for A27, A14, A25 and
     * A16 EVN-3 = PV2-8 ou
     * PV2-9
     *
     * @param evnSegment
     *
     * @throws DataTypeException
     */
    private void fillEVNSegment(EVN evnSegment, Date plannedDate, Date occurredDate) throws DataTypeException {
        String currentDateString = sdf.format(Calendar.getInstance().getTime());
        evnSegment.getRecordedDateTime().getTime().setValue(currentDateString);
        // for INSERT action we must have the same time in EVN
        if (eventEnum.equals(TriggerEventEnum.A21) && aEncounter.getLeaveOfAbsenceDate() != null) {
            evnSegment.getEventOccurred().getTime().setValue(sdf.format(aEncounter.getLeaveOfAbsenceDate()));
        } else if (eventEnum.equals(TriggerEventEnum.A22) && aEncounter.getLoaReturnDate() != null) {
            evnSegment.getEventOccurred().getTime().setValue(sdf.format(aEncounter.getLoaReturnDate()));
        } else if (occurredDate != null) {
            evnSegment.getEventOccurred().getTime().setValue(sdf.format(occurredDate));
        }
        if (plannedDate != null) {
            evnSegment.getDateTimePlannedEvent().getTime().setValue(sdf.format(plannedDate));
        }
    }

    /**
     * 3.30.5.3 PID - Patient Identification Segment Rev8.0 p17
     *
     * @param pidSegment
     *
     * @throws HL7Exception
     */
    private void fillPIDSegment(PID pidSegment) throws HL7Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        setPatientIdentifiersInPID(pidSegment, PatientIdentifierDAO.getPatientIdentifiersForRestrictedDomains(patient, restrictedDomain));
        if (bp6Mode) {
            fillBp6Names(pidSegment, patient);
        } else {
            fillNames(pidSegment, patient);
        }
        if (patient.getDateOfBirth() != null) {
            pidSegment.getDateTimeOfBirth().getTime().setValue(sdf.format(patient.getDateOfBirth()));
        }
        pidSegment.getAdministrativeSex().setValue(patient.getGenderCode());
        if (pidConfig.isFillRace()) {
            pidSegment.getRace(0).getIdentifier().setValue(patient.getRaceCode());
        }
        if (pidConfig.isFillReligion()) {
            pidSegment.getReligion().getIdentifier().setValue(patient.getReligionCode());
        }
        List<PatientAddress> patientAddresses = patient.getAddressList();
        for (int i = 0; i < patientAddresses.size(); i++) {
            if(!isAddressEmpty(patientAddresses.get(i))) {
                setXAD(pidSegment.getPatientAddress(i),
                        patientAddresses.get(i).getAddressLine(),
                        patientAddresses.get(i).getCity(),
                        patientAddresses.get(i).getCountryCode(),
                        patientAddresses.get(i).getZipCode(),
                        patientAddresses.get(i).getState(),
                        patientAddresses.get(i).getAddressType());
            }
        }
        if (bp6Mode) {
            fillBp6PIDXAD(pidSegment, patient);
        }
        int xtnIndex = 0;
        PatientPhoneNumber phoneNumber = patient.getPrincipalPhoneNumber();
        if (phoneNumber != null) {
            setPhoneNumber(pidSegment.getPhoneNumberHome(xtnIndex), phoneNumber.getValue());
            xtnIndex++;
        }
        if (patient.getEmail() != null && !patient.getEmail().isEmpty()) {
            setEmail(pidSegment.getPhoneNumberHome(xtnIndex), patient.getEmail());
        }
        if (transactionKeyword.equals(ITI_31)) {
            pidSegment.getIdentityUnknownIndicator().setValue("N");
        }
        if (aEncounter.getPatientAccountNumber() != null) {
            pidSegment.getPatientAccountNumber().parse(aEncounter.getPatientAccountNumber());
        }
        if (patient.getMaritalStatus() != null) {
            pidSegment.getMaritalStatus().getIdentifier().setValue(patient.getMaritalStatus());
        }
        if (patient.getIdentityReliabilityCode() != null) {
            pidSegment.getIdentityReliabilityCode(0).setValue(patient.getIdentityReliabilityCode());
        }
        if (patient.isPatientDead()) {
            pidSegment.getPatientDeathIndicator().setValue("Y");
        }
        if (patient.isPatientDead() && patient.getPatientDeathTime() != null) {
            pidSegment.getPatientDeathDateAndTime().getTime().parse(sdf.format(patient.getPatientDeathTime()));
        }
    }
    private void setPatientIdentifiersInPID(PID pidSegment, List<PatientIdentifier> patientIdentifiers) throws DataTypeException {
        if (patientIdentifiers != null) {
            int index = 0;
            for (PatientIdentifier pid : patientIdentifiers) {
                if (pid != null) {
                    Map<String, String> splitPid = pid.splitPatientId();
                    if (splitPid != null) {
                        setCX(pidSegment.getPatientIdentifierList(index), splitPid, pid.getIdentifierTypeCode());
                        index++;
                    }
                }
            }
        }
    }
    private void fillBp6PIDXAD(PID pidSegment, Patient patient) {
        List<PatientAddress> patientAddresses = patient.getAddressList();
        for (int i = 0; i < patientAddresses.size(); i++) {
            PatientAddress pa = patientAddresses.get(i);
            if (pidConfig.isFillCountyParish()&& AddressType.BDL.equals(pa.getAddressType())) {
                try {
                    XAD xad = (XAD)pidSegment.getField(11, i);
                    xad.getCountry().setValue(patientAddresses.get(i).getCountryCode());
                    xad.getOtherGeographicDesignation().setValue(AddressType.BDL.getHl7Code());
                    xad.getCountyParishCode().setValue(getInseeCode(pa));
                } catch (ClassCastException cce) {
                    log.error("Unexpected problem obtaining field value.  This is a bug.", cce);
                    throw new RuntimeException(cce);
                } catch (HL7Exception hl7e) {
                    log.error("Unexpected problem obtaining field value.  This is a bug.", hl7e);
                    throw new RuntimeException(hl7e);
                }
            }
        }
    }
    private String getInseeCode(PatientAddress pa) {
        return pa.getInseeCode();
    }
    private void fillNames(PID pidSegment, Patient patient) throws HL7Exception {
        setPatientName(pidSegment.getPatientName(0), patient);
        setMotherMaidenName(pidSegment.getMotherSMaidenName(0), patient);
    }

    private void fillBp6Names(PID pidSegment, Patient patient) throws HL7Exception {
        if(patient.getIdentityReliabilityCode() != null && patient.getIdentityReliabilityCode().equals("VALI")) {
            XPN legal = pidSegment.getPatientName(0);
            legal.getGivenName().setValue(patient.getAlternateFirstName());
            legal.getSecondAndFurtherGivenNamesOrInitialsThereof().setValue(buildFirstAndFurtherGivenNamesOrInitialsThereof(patient.getFirstName(), patient.getSecondName(), patient.getThirdName()));
            legal.getFamilyName().getSurname().setValue(patient.getLastName());
            legal.getNameTypeCode().setValue(PatientManagerConstants.LEGAL_NAME_CODE);
            setBp6MotherMaidenName(pidSegment.getMotherSMaidenName(0), patient);
        } else {
            fillNames(pidSegment, patient);
        }
    }

    private String buildFirstAndFurtherGivenNamesOrInitialsThereof(String firstName, String secondName, String thirdName) {
        StringBuilder sb = new StringBuilder();
        if (firstName != null) {
            sb.append(firstName);
        }
        if (secondName != null && !secondName.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(secondName);
        }
        if (thirdName != null && !thirdName.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(thirdName);
        }
        return sb.toString();
    }

    private void setBp6MotherMaidenName(XPN xpn, Patient patient) throws DataTypeException {
        if (patient.getMotherMaidenName() != null) {
            xpn.getFamilyName().getSurname().setValue(patient.getMotherMaidenName());
            xpn.getNameTypeCode().setValue(PatientManagerConstants.LEGAL_NAME_CODE);
        }
    }

    /**
     * 3.30.5.4 PV1 Patient Visit Segment Rev8.0 p22
     *
     * @param pv1Segment
     *
     * @throws HL7Exception
     */
    private void fillPV1Segment(PV1 pv1Segment) throws HL7Exception {
        pv1Segment.getPatientClass().setValue(aEncounter.getPatientClassCode());
        pv1Segment.getHospitalService().setValue(aEncounter.getHospitalServiceCode());
        if (aEncounter.getVisitNumber() != null) {
            pv1Segment.getVisitNumber().parse(aEncounter.getVisitNumber());
            pv1Segment.getVisitIndicator().setValue("V");
        }
        if (aMovement.getAssignedPatientLocation() != null) {
            pv1Segment.getAssignedPatientLocation().parse(aMovement.getAssignedPatientLocation());
        }
        if (aEncounter.getAdmitDate() != null) {
            pv1Segment.getAdmitDateTime().getTime().setValue(sdf.format(aEncounter.getAdmitDate()));
        }
        setXCNForDoctor(pv1Segment.getAttendingDoctor(0), aEncounter.getAttendingDoctorCode());
        setXCNForDoctor(pv1Segment.getAdmittingDoctor(0), aEncounter.getAdmittingDoctorCode());
        if (aEncounter.getDischargeDate() != null) {
            pv1Segment.getDischargeDateTime(0).getTime().setValue(sdf.format(aEncounter.getDischargeDate()));
        }
        if (aMovement.getPendingPatientLocation() != null) {
            pv1Segment.getPendingLocation().parse(aMovement.getPendingPatientLocation());
        }
        if (aEncounter.getVisitNumber() != null && eventEnum.equals(TriggerEventEnum.A05)) {
            pv1Segment.getPreadmitNumber().parse(aEncounter.getVisitNumber());
        }
        if (aMovement.getPriorPatientLocation() != null) {
            pv1Segment.getPriorPatientLocation().parse(aMovement.getPriorPatientLocation());
        }
        if (aMovement.getPriorTemporaryLocation() != null) {
            pv1Segment.getPriorTemporaryLocation().parse(aMovement.getPriorTemporaryLocation());
        }
        setXCNForDoctor(pv1Segment.getReferringDoctor(0), aEncounter.getReferringDoctorCode());
        if (aMovement.getTemporaryPatientLocation() != null && (eventEnum.equals(TriggerEventEnum.A09)
                || eventEnum.equals(TriggerEventEnum.A10) || eventEnum.equals(TriggerEventEnum.A32) || eventEnum.equals
                (TriggerEventEnum.A33))) {
            pv1Segment.getTemporaryLocation().parse(aMovement.getTemporaryPatientLocation());
        }
        pv1Segment.getAdmissionType().setValue(aEncounter.getAdmissionType());
        if (patient.getVipIndicator() != null && !patient.getVipIndicator().isEmpty()) {
            pv1Segment.getVIPIndicator().setValue(patient.getVipIndicator());
        }
    }

    private void setXCNForDoctor(XCN xcn, String doctorId) throws HL7Exception {
        if (doctorId != null) {
            Concept doctor = ValueSet.getConceptForCode("DOCTOR", doctorId);
            if (doctor != null) {
                String name = doctorId + "^" + doctor.getDisplayName();
                xcn.parse(name);
                if (bp6Mode) {
                    xcn.getAssigningAuthority().getNamespaceID().setValue("ASIP-SANTE-PS");
                    xcn.getAssigningAuthority().getUniversalID().setValue("1.2.250.1.71.4.2.1 I");
                    xcn.getAssigningAuthority().getUniversalIDType().setValue("ISO");
                    xcn.getIdentifierTypeCode().setValue(doctor.getCodeSystemName());
                    xcn.getXcn10_NameTypeCode().setValue("D");
                }
            }
        }
    }

    /**
     * <p>fillACCSegment.</p>
     *
     * @param accSegment a {@link net.ihe.gazelle.pam.hl7v2.model.v25.segment.ACC} object.
     *
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public void fillACCSegment(ACC accSegment) throws DataTypeException {
        if (aEncounter.getAccidentCode() != null) {
            accSegment.getAccidentCode().getIdentifier().setValue(aEncounter.getAccidentCode());
        }
        if (aEncounter.getAccidentDate() != null) {
            accSegment.getAcc1_AccidentDateTime().getTime().setValue(sdf.format(aEncounter.getAccidentDate()));
        }
    }

    /**
     * <p>convertMessageToBP6.</p>
     *
     * @param messageAsString a {@link java.lang.String} object.
     *
     * @return a {@link java.lang.String} object.
     *
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public String convertMessageToBP6(String messageAsString) throws HL7Exception {
        TriggerEventEnum event = TriggerEventEnum.getEventForTrigger(triggerEvent);
        Message convertedMessage = null;
        if (event == null) {
            log.error("Event is null or not event match " + triggerEvent);
            return null;
        } else if (event.equals(TriggerEventEnum.A11)) {
            convertedMessage = convertA11ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals(ADT_A_01_STRUC)) {
            convertedMessage = convertADTA01ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A02")) {
            convertedMessage = convertADTA02ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A03")) {
            convertedMessage = convertADTA03ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A05")) {
            convertedMessage = convertADTA05ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A12")) {
            convertedMessage = convertADTA12ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A15")) {
            convertedMessage = convertADTA15ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A16")) {
            convertedMessage = convertADTA16ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A21")
                && !(event.equals(TriggerEventEnum.A32) || event.equals(TriggerEventEnum.A33))) {
            convertedMessage = convertADTA21ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A38")) {
            convertedMessage = convertADTA38ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A52")) {
            convertedMessage = convertADTA52ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A54")) {
            convertedMessage = convertADTA54ToBP6Message(messageAsString);
        } else if (event.getMessageStructure().equals("ADT_A06")) {
            convertedMessage = convertADTA06ToBP6Message(messageAsString);
        }
        if (convertedMessage != null) {
            PipeParser parser = PipeParser.getInstanceWithNoValidation();
            parser.getParserConfiguration().setEncodeEmptyMandatoryFirstSegments(true);
            return parser.encode(convertedMessage);
        }
        return null;
    }

    private Message convertADTA52ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A52 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A52();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA54ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A54 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A54();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA38ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A38 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A38();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA21ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A21 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A21();
        adt.parse(messageAsString);
        fillZFSegments(adt.getZFA(), adt.getZFV(), adt.getZFD(), adt.getZFM(), adt.getZFP(), adt.getZBE());
        return adt;
    }

    private Message convertADTA16ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A16 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A16();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA15ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A15 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A15();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA12ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A12 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A12();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertA11ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A09 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A09();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private Message convertADTA01ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A01 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A01();
        adt.parse(messageAsString);
        fillZFSegments(adt.getZFA(), adt.getZFV(), adt.getZFD(), adt.getZFM(), adt.getZFP(), adt.getZBE());
        return adt;
    }

    private Message convertADTA02ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A02 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A02();
        adt.parse(messageAsString);
        fillZFSegments(adt.getZFA(), adt.getZFV(), adt.getZFD(), adt.getZFM(), adt.getZFP(), adt.getZBE());
        return adt;
    }

    private Message convertADTA03ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A03 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A03();
        adt.parse(messageAsString);
        fillZFSegments(adt.getZFA(), adt.getZFV(), adt.getZFD(), adt.getZFM(), adt.getZFP(), adt.getZBE());
        return adt;
    }

    private Message convertADTA05ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05();
        adt.parse(messageAsString);
        fillZFSegments(adt.getZFA(), adt.getZFV(), adt.getZFD(), adt.getZFM(), adt.getZFP(), adt.getZBE());
        return adt;
    }

    private Message convertADTA06ToBP6Message(String messageAsString) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A06 adt = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A06();
        adt.parse(messageAsString);
        BP6SegmentBuilder.fillZBE(adt.getZBE(), aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        return adt;
    }

    private void fillZFSegments(ZFA zfa, ZFV zfv, ZFD zfd, ZFM zfm, ZFP zfp, net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZBE
            zbe) throws
            HL7Exception {
        if (zfa != null && aEncounter != null && aEncounter.getPatientsPHRInfo() != null) {
            BP6SegmentBuilder.fillZFASegment(zfa, aEncounter.getPatientsPHRInfo(), sdf);
        }
        if (zfv != null && aEncounter != null && aEncounter.getEncounterAdditionalInfo() != null) {
            BP6SegmentBuilder.fillZFVSegment(zfv, aEncounter.getEncounterAdditionalInfo(), sdf);
        }
        if (patient.getAdditionalDemographics() != null) {
            if (zfd != null) {
                BP6SegmentBuilder.fillZFDSegment(zfd, patient.getAdditionalDemographics());
            }
            if (zfp != null) {
                BP6SegmentBuilder.fillZFPSegment(zfp, patient.getAdditionalDemographics());
            }
        }
        if (zfm != null && aEncounter != null && aEncounter.getDrgMovement() != null) {
            BP6SegmentBuilder.fillZFMSegment(zfm, aEncounter.getDrgMovement());
        }
        if (zbe != null && aMovement != null) {
            BP6SegmentBuilder.fillZBE(zbe, aMovement.getNursingCareWard(), aMovement.getNatureOfMovement(), aMovement.getFacilityCode());
        }
    }

    private HL7Domain<?> getHL7Domain() {
        if (bp6Mode) {
            hl7Domain = hl7Domain instanceof BP6HL7Domain ? hl7Domain : new BP6HL7Domain();
        } else {
            hl7Domain = hl7Domain instanceof IHEHL7Domain ? hl7Domain : new IHEHL7Domain();
        }
        return hl7Domain;
    }

}
