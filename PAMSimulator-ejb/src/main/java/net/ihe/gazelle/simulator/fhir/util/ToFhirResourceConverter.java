package net.ihe.gazelle.simulator.fhir.util;

import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>ToFhirConverter class.</p>
 *
 * @author aberge
 * @version 1.0: 10/11/17
 */

public class ToFhirResourceConverter {

    private static final Logger LOG = LoggerFactory.getLogger(ToFhirResourceConverter.class);
    public static final String MTH = "MTH";

    public static PatientFhirIHE patientToFhirResource(Patient patient) {
        PatientFhirIHE fhirPatient = new PatientFhirIHE();

        //ID
        fhirPatient.setId(patient.getId().toString());

        //Name
        HumanName name = new HumanName();
        name.setFamily(patient.getLastName());
        name.addGiven(patient.getFirstName());
        if (patient.getSecondName() != null) {
            name.addGiven(patient.getSecondName());
        }
        if (patient.getThirdName() != null) {
            name.addGiven(patient.getThirdName());
        }
        name.setUse(HumanName.NameUse.OFFICIAL);
        fhirPatient.addName(name);

        HumanName alternate = new HumanName();
        if (patient.getAlternateFirstName() != null && !patient.getAlternateFirstName().isEmpty()){
            alternate.addGiven(patient.getAlternateFirstName());
        }
        if (patient.getAlternateSecondName() != null && !patient.getAlternateSecondName().isEmpty()){
            alternate.addGiven(patient.getAlternateSecondName());
        }
        if (patient.getAlternateThirdName() != null && !patient.getAlternateThirdName().isEmpty()){
            alternate.addGiven(patient.getAlternateThirdName());
        }
        if (patient.getAlternateLastName() != null && !patient.getAlternateLastName().isEmpty()){
            alternate.setFamily(patient.getAlternateLastName());
        }
        if (!alternate.isEmpty()){
            alternate.setUse(HumanName.NameUse.USUAL);
            fhirPatient.addName(alternate);
        }

        //Mother Maiden Name extension
        if (!patient.getPersonalRelationships().isEmpty()){
            for (Person person: patient.getPersonalRelationships()){
                if (MTH.equals(person.getRelationshipCode())){
                    Extension mothersMaidenNameExt = buildMothersMaidenNameExtension(person.getLastName());
                    fhirPatient.addExtension(mothersMaidenNameExt);
                    break;
                }
            }
        } else if (patient.getMotherMaidenName() != null && !patient.getMotherMaidenName().isEmpty()){
            Extension mothersMaidenNameExt = buildMothersMaidenNameExtension(patient.getMotherMaidenName());
            fhirPatient.addExtension(mothersMaidenNameExt);
        }


        //Gender
        fhirPatient.setGender(getFhirGenderCodeFromPatientDb(patient.getGenderCode()));

        //Cross-Identifiers
        // TODO consider the what domains returned here
        for (PatientIdentifier currentPatientIdentifier : patient.getPatientIdentifiers()) {
            if (currentPatientIdentifier.getDomain() != null) {
                String fhirSystem = currentPatientIdentifier.getDomain().getUniversalIDAsUrn();
                fhirPatient.addIdentifier().setSystem(fhirSystem).setValue(currentPatientIdentifier.getIdNumber());
            }
        }

        //Birthdate
        try {
            fhirPatient.setBirthDate(patient.getDateOfBirth());
        } catch (StringIndexOutOfBoundsException e) {
            LOG.warn(patient.getDateOfBirth() + " is not a valid date", e.getMessage());
        }


        // Address
        List<PatientAddress> addressList = patient.getAddressList();
        if (!addressList.isEmpty()) {
            for (PatientAddress pAddress : addressList) {
                Address address = new Address();
                address.addLine(pAddress.getAddressLine());
                address.setCity(pAddress.getCity());
                address.setCountry(pAddress.getCountryCode());
                address.setPostalCode(pAddress.getZipCode());
                address.setState(pAddress.getState());
                address.setUse(getAddressUse(pAddress.getAddressType()));
                fhirPatient.addAddress(address);
            }
        }
        //Active
        fhirPatient.setActive(patient.getStillActive());

        //Telecom
        List<PatientPhoneNumber> phoneNumbers = patient.getPhoneNumbers();
        if (!phoneNumbers.isEmpty()) {
            for (PatientPhoneNumber pPhone : phoneNumbers) {
                ContactPoint contact = new ContactPoint();
                if (PhoneNumberType.BEEPER.equals(pPhone.getType())){
                    contact.setSystem(ContactPoint.ContactPointSystem.PAGER);
                } else {
                    contact.setSystem(ContactPoint.ContactPointSystem.PHONE);
                }
                contact.setValue(pPhone.getValue());
                contact.setUse(getContactPointUse(pPhone.getType()));
                fhirPatient.addTelecom(contact);
            }
        }
        String email = patient.getEmail();
        if (email != null && !email.isEmpty()) {
            ContactPoint contactEmail = new ContactPoint();
            contactEmail.setSystem(ContactPoint.ContactPointSystem.EMAIL);
            contactEmail.setValue(email);
            fhirPatient.addTelecom(contactEmail);
        }
        return fhirPatient;
    }

    private static Extension buildMothersMaidenNameExtension(String family) {
        StringType mothersMaidenNameValue = new StringType();
        if (family != null && !family.isEmpty()) {
            mothersMaidenNameValue.setValue(family);
        }
        Extension mothersMaidenNameExt = new Extension(FhirConstants.MOTHERS_MAIDEN_NAME_EXTENSION_URL);
        mothersMaidenNameExt.setValue(mothersMaidenNameValue);
        return mothersMaidenNameExt;
    }

    private static ContactPoint.ContactPointUse getContactPointUse(PhoneNumberType type) {
        if (type != null) {
            switch (type) {
                case HOME:
                    return ContactPoint.ContactPointUse.HOME;
                case MOBILE:
                    return ContactPoint.ContactPointUse.MOBILE;
                case WORK:
                    return ContactPoint.ContactPointUse.WORK;
                case VACATION:
                    return ContactPoint.ContactPointUse.TEMP;
                default:
                    return ContactPoint.ContactPointUse.NULL;
            }
        }
        return null;
    }

    private static Address.AddressUse getAddressUse(AddressType addressType) {
        if (addressType != null) {
            switch (addressType) {
                case HOME:
                    return Address.AddressUse.HOME;
                case OFFICE:
                case BUSINESS:
                    return Address.AddressUse.WORK;
                case CURRENT:
                    return Address.AddressUse.TEMP;
                case BAD_ADDRESS:
                    return Address.AddressUse.OLD;
                default:
                    return Address.AddressUse.NULL;
            }
        }
        return null;
    }

    private static Enumerations.AdministrativeGender getFhirGenderCodeFromPatientDb(String genderCode) {
        if (genderCode == null) {
            return Enumerations.AdministrativeGender.OTHER;
        } else if (genderCode.equals("M")) {
            return Enumerations.AdministrativeGender.MALE;
        } else if (genderCode.equals("F")) {
            return Enumerations.AdministrativeGender.FEMALE;
        } else {
            return Enumerations.AdministrativeGender.UNKNOWN;
        }
    }

    public static Bundle patientListToFhirBundle(List<Patient> patients) {
        List<Bundle.BundleEntryComponent> fhirPatients = new ArrayList<Bundle.BundleEntryComponent>();
        for (Patient patient: patients){
            PatientFhirIHE fhirPatient = patientToFhirResource(patient);
            Bundle.BundleEntryComponent component = new Bundle.BundleEntryComponent();
            component.setResource(fhirPatient);
            component.setFullUrl(getPatientResouceUrl(patient));
            fhirPatients.add(component);
        }
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);
        bundle.setEntry(fhirPatients);
        return bundle;
    }

    private static String getPatientResouceUrl(Patient patient) {
        String application_url = PreferenceService.getString(PatientManagerConstants.APPLICATION_URL_PREFERENCE);
        StringBuilder resourceUrl = new StringBuilder(application_url);
        resourceUrl.append("/fhir/Patient/");
        resourceUrl.append(patient.getId());
        return resourceUrl.toString();
    }
}
