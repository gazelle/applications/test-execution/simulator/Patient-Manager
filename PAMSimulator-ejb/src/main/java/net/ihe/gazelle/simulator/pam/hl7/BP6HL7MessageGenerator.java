package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFS;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>PAMPDSMessageGenerator<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 02/11/15
 */
public class BP6HL7MessageGenerator extends AbstractHL7MessageGenerator {
    private static Log log = Logging.getLog(BP6HL7MessageGenerator.class);

    /**
     * <p>Constructor for BP6MessageGenerator.</p>
     */
    public BP6HL7MessageGenerator() {
        super(new BP6PID());
    }

    @Override
    public void fillPIDSegment(PID pidSegment, Patient patient, boolean onlyFillPID3, boolean fillPID31) throws HL7Exception {
        super.fillPIDSegment(pidSegment, patient, onlyFillPID3,
                false // PID31 not filled in BP6
        );
    }

    @Override
    public void fillAddress(PID pidSegment, List<PatientAddress> patientAddresses) throws HL7Exception {
        for (PatientAddress address : patientAddresses) {
            if (pidConfig.isFillCountyParish() && AddressType.BDL.equals(address.getAddressType())) {
                try {
                    XAD xad = (XAD) pidSegment.getField(11, pidSegment.getPatientAddress().length);
                    xad.getCountry().setValue(address.getCountryCode());
                    xad.getAddressType().setValue(AddressType.BDL.getHl7Code());
                    xad.getCountyParishCode().setValue(getInseeCode(address));
                    xad.getCity().setValue(address.getCity());
                    xad.getZipOrPostalCode().setValue(address.getZipCode());
                } catch (ClassCastException cce) {
                    log.error("Unexpected problem obtaining field value.  This is a bug.", cce);
                    throw new RuntimeException(cce);
                } catch (HL7Exception hl7e) {
                    log.error("Unexpected problem obtaining field value.  This is a bug.", hl7e);
                    throw new RuntimeException(hl7e);
                }
            } else {
                if(!isAddressEmpty(address)) {
                    setXAD((XAD) pidSegment.getField(11, pidSegment.getPatientAddress().length),
                            address.getAddressLine(),
                            address.getCity(),
                            address.getCountryCode(),
                            address.getZipCode(),
                            address.getState(),
                            address.getAddressType());
                }
            }
        }
    }

    private String getInseeCode(PatientAddress pa) {
        return pa.getInseeCode();
    }

    /**
     * <p>convertADTA05ToBP6Message.</p>
     *
     * @param messageAsString a {@link String} object.
     * @param selectedPatient a {@link Patient} object.
     * @return a {@link String} object.
     * @throws HL7Exception if any.
     */
    public String convertADTA05ToBP6Message(String messageAsString, Patient selectedPatient) throws HL7Exception {
        net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05 bp6Message = new net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05();
        bp6Message.parse(messageAsString);
        if (selectedPatient.getAdditionalDemographics() != null) {
            BP6SegmentBuilder.fillZFDSegment(bp6Message.getZFD(), selectedPatient.getAdditionalDemographics());
        }
        // legal care mode segment is used for A28 and A31 events
        int zfsRepIndex = 0;
        for (LegalCareMode careMode : selectedPatient.getLegalCareModes()) {
            ZFS zfsRep = bp6Message.getZFS(zfsRepIndex);
            // TODO : do we always send all the repetitions ?
            BP6SegmentBuilder.fillZFSSegment(zfsRep, careMode);
            zfsRepIndex++;
        }
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        return parser.encode(bp6Message);
    }

    @Override
    public String buildADTA28Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient patient) throws HL7Exception {
        return convertADTA05ToBP6Message(super.buildADTA28Frame(sut, sendingApplication, sendingFacility, patient), patient);
    }

    @Override
    public String buildADTA31Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient patient, boolean onlyFillPID3) throws HL7Exception {
        return convertADTA05ToBP6Message(super.buildADTA31Frame(sut, sendingApplication, sendingFacility, patient, onlyFillPID3), patient);
    }

    @Override
    protected void fillMSHSegment(MSH mshSegment, String receivingApplication, String receivingFacility, String sendingApplication, String sendingFacility, String messageCode, String triggerEvent, String msgStructure, String characterSet) throws DataTypeException {
        super.fillMSHSegment(mshSegment, receivingApplication, receivingFacility, sendingApplication, sendingFacility, messageCode, triggerEvent, msgStructure, characterSet);
        mshSegment.getVersionID().getInternationalizationCode().getIdentifier().setValue(PatientManagerConstants.FRA);
        mshSegment.getVersionID().getInternationalVersionID().getIdentifier().setValue(PatientManagerConstants.PAM_FR_VERSION);
    }

    @Override
    public void fillNames(PID pidSegment, Patient patient) throws HL7Exception {
        if (patient.getIdentityReliabilityCode() != null && patient.getIdentityReliabilityCode().equals("VALI")) {
            XPN legal = pidSegment.getPatientName(0);
            legal.getGivenName().setValue(patient.getAlternateFirstName());
            legal.getSecondAndFurtherGivenNamesOrInitialsThereof().setValue(buildSecondAndFurtherGivenNamesOrInitialsThereof(patient.getFirstName(), patient.getSecondName(), patient.getThirdName()));
            legal.getFamilyName().getSurname().setValue(patient.getLastName());
            legal.getNameTypeCode().setValue(PatientManagerConstants.LEGAL_NAME_CODE);
            setMotherMaidenName(pidSegment.getMotherSMaidenName(0), patient);
        } else {
            super.setPatientName(pidSegment.getPatientName(0), patient);
            setMotherMaidenName(pidSegment.getMotherSMaidenName(0), patient);
        }
    }

    @Override
    public void setMotherMaidenName(XPN xpn, Patient patient) throws DataTypeException {
        if (patient.getMotherMaidenName() != null && !patient.getMotherMaidenName().isEmpty()) {
            xpn.getFamilyName().getSurname().setValue(patient.getMotherMaidenName());
            xpn.getNameTypeCode().setValue(PatientManagerConstants.LEGAL_NAME_CODE);
        }
    }

    private String buildSecondAndFurtherGivenNamesOrInitialsThereof(String firstName, String secondName, String thirdName) {
        StringBuilder sb = new StringBuilder();
        if (firstName != null) {
            sb.append(firstName);
        }
        if (secondName != null && !secondName.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(secondName);
        }
        if (thirdName != null && !thirdName.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(thirdName);
        }
        return sb.toString();
    }

    /*
     * il faut transmettre tous les PatientIdentifier du patient qui ne sont pas des identifier INS
     * en plus du correctPatientIdentifier passé en paramètre
     */
    @Override
    protected List<PatientIdentifier> createA30CorrectPatientIdentifierList(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, PatientIdentifier
            correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier, Patient selectedPatient, Patient secondaryPatient) throws
            HL7Exception {
        List<PatientIdentifier> correctPatientIdentifierList = new ArrayList<>();
        correctPatientIdentifierList.add(correctPatientIdentifier);
        for (PatientIdentifier pid : selectedPatient.getPatientIdentifiers()) {
            if (!BP6HL7Domain.INS.getAuthorities().contains(pid.splitPatientId().get("CX-4-2"))) {
                correctPatientIdentifierList.add(pid);
            }
        }
        return correctPatientIdentifierList;
    }

}
