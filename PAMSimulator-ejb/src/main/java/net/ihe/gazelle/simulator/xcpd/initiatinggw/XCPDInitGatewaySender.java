package net.ihe.gazelle.simulator.xcpd.initiatinggw;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.sts.constants.AssertionProfile;
import net.ihe.gazelle.xua.model.PicketLinkCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.io.Serializable;

/**
 * <p>XCPDInitGatewaySender class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XCPDInitGatewaySender extends SoapHL7V3WebServiceClient implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5525858837170285232L;
	private static final String ACTION_QUERY = "urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery";
    private static final String ACTION_QUERY_DEFERRED = "urn:hl7-org:v3:PRPA_IN201305UV02:Deferred:CrossGatewayPatientDiscovery";
	private static final String SERVICE_NAME = "RespondingGateway_Service";
	private static final String SERVICE_PORT_TYPE = "RespondingGateway_Port_Soap12";

	private static Logger log = LoggerFactory.getLogger(XCPDInitGatewaySender.class);

	/**
	 * <p>Constructor for XCPDInitGatewaySender.</p>
	 *
	 * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public XCPDInitGatewaySender(HL7v3ResponderSUTConfiguration inSUT) {
		super(inSUT);
		this.transactionInstance = null;
	}

	/**
	 * <p>Constructor for XCPDInitGatewaySender.</p>
	 */
	public XCPDInitGatewaySender(){
		super();
	}

	/** {@inheritDoc} */
	@Override
	protected QName getServiceQName() {
		return new QName(XCPD_TNS, SERVICE_NAME);
	}

	/** {@inheritDoc} */
	@Override
	protected QName getPortQName() {
		return new QName(XCPD_TNS, SERVICE_PORT_TYPE);
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSimulatedActor() {
		return Actor.findActorWithKeyword("INIT_GW");
	}

	/** {@inheritDoc} */
	@Override
	protected Transaction getSimulatedTransaction() {
		return Transaction.GetTransactionByKeyword("ITI-55");
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSutActor() {
		return Actor.findActorWithKeyword("RESP_GW");
	}

    /**
     * <p>sendCrossGatewayPatientDiscoveryRequest.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param credentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type sendCrossGatewayPatientDiscoveryRequest(PRPAIN201305UV02Type request, PicketLinkCredentials credentials)
			throws SoapSendException {
        return sendPRPAIN201305UV02Type(request, ACTION_QUERY, credentials);
    }

    /**
     * <p>sendDeferredCrossGatewayPatientDiscoveryRequest.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param credentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type sendDeferredCrossGatewayPatientDiscoveryRequest(PRPAIN201305UV02Type request, PicketLinkCredentials credentials)
			throws SoapSendException{
        transactionInstance = null;
		MCCIIN000002UV01Type response;
		if (credentials != null) {
			AssertionProfile profile = credentials.getAssertionProfile();
			response = sendWithXua(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), ACTION_QUERY_DEFERRED, profile.getName(), profile .getPassword());
			logTransactionDetailsForXUA();
		}
		else {
			response = send(MCCIIN000002UV01Type.class, request.get_xmlNodePresentation(), ACTION_QUERY_DEFERRED);
		}
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
        }
        transactionInstance = saveTransactionInstance(findCandidatesQueryToByteArray(request),
                acknowledgementToByteArray(response), request.get_xmlNodePresentation().getLocalName(),
                responseType);
        transactionInstance.getRequest().setType("PRPA_IN201305UV02 (deferred)");
		HL7V3Utils.saveMessageId(request.getId(), transactionInstance.getRequest());
		if (response != null){
			HL7V3Utils.saveMessageId(response.getId(), transactionInstance.getResponse());
		}
        return response;
    }


}
