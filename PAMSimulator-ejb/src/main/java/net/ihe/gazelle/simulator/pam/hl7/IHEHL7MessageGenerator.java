package net.ihe.gazelle.simulator.pam.hl7;

/**
 * <b>Class Description : </b>PAMPDSMessageGenerator<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 02/11/15
 */
public class IHEHL7MessageGenerator extends AbstractHL7MessageGenerator {

    public IHEHL7MessageGenerator() {
        super(new IHEPID());
    }

}
