package net.ihe.gazelle.simulator.pix.xmgr;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.message.ADT_A01;
import ca.uhn.hl7v2.model.v231.message.ADT_A39;
import ca.uhn.hl7v2.model.v25.message.ACK;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.exception.UnexpectedMessageStructureException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.pix.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.utils.ADTDecoder;
import net.ihe.gazelle.simulator.pam.utils.Consumer;
import net.ihe.gazelle.simulator.pix.hl7.QBPMessageDecoder;
import net.ihe.gazelle.simulator.pix.hl7.QueryResponseStatus;
import net.ihe.gazelle.simulator.pix.hl7.RSPMessageBuilder;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>PIXManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PIXManager extends Consumer {

    private static final String[] ACCEPTED_EVENTS = {"A01", "A04", "A05", "A08", "A40", "Q23", "A40", "A28", "A31",
            "A47", "A24", "A37"};
    private static final String[] ACCEPTED_MESSAGE_TYPES = {"ADT", "QBP"};

    private static final Logger LOG = LoggerFactory.getLogger(PIXManager.class);

    /**
     * <p>Constructor for PIXManager.</p>
     */
    public PIXManager() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IHEDefaultHandler newInstance() {
        PIXManager handler = new PIXManager();
        return handler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Message processMessage(Message receivedMessage) throws HL7Exception {
        if (receivedMessage == null) {
            return null;
        }

        // start hibernate transaction
        Message response = null;
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        ACKBuilder ackBuilder = new ACKBuilder(serverApplication, serverFacility, Arrays.asList(ACCEPTED_MESSAGE_TYPES), Arrays.asList
                (ACCEPTED_EVENTS));
        // check if we must accept the message
        try {
            ackBuilder.isMessageAccepted(receivedMessage);
        } catch (HL7v2ParsingException e) {
            return ackBuilder.buildNack(receivedMessage, e);
        }
        // if no NACK is generated, process the message
        simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-30", entityManager);

        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        String messageType = terser.get("/.MSH-9-1");
        if (messageType.equals("ADT")) {
            setSutActor(Actor.findActorWithKeyword(getSutActorKeyword(), entityManager));
            ADTDecoder decoder = new ADTDecoder(domain, simulatedTransaction, simulatedActor, ackBuilder, entityManager, this);
            String triggerEvent = terser.get("/.MSH-9-2");
            if (triggerEvent != null) {
                if (hl7Version.equals("2.3.1")) {
                    simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-8", entityManager);
                    try {
                        response = processITI8Message(receivedMessage, triggerEvent, decoder);
                    } catch (DataTypeException e) {
                        response = ackBuilder.buildNack(receivedMessage, new HL7v2ParsingException(e));
                        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
                        return response;
                    }
                } else {
                    response = decoder.decode(receivedMessage, triggerEvent);
                }
                if (response != null) {
                    try {
                        crossReferencePatients(decoder);
                    } catch (Exception e) {
                        LOG.warn("An error occurred when automatically cross-referencing the patients", e);
                    }
                }
            }
        } else { // QBP
            setSutActor(Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER", entityManager));
            simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-9", entityManager);
            if (receivedMessage instanceof ca.uhn.hl7v2.model.v25.message.QBP_Q21) {
                QBP_Q21 qbpMessage = (QBP_Q21) parser.parseForSpecificPackage(receivedMessage.encode(),
                        "net.ihe.gazelle.pix.hl7v2.model.v25.message");
                try {
                    response = processITI9Message(qbpMessage);
                } catch (HL7Exception e) {
                    response = ackBuilder.buildNack(qbpMessage, new HL7v2ParsingException(e));
                }
            } else {
                UnexpectedMessageStructureException unsupportedMessageTypeException = new UnexpectedMessageStructureException();
                response = ackBuilder.buildNack(receivedMessage, unsupportedMessageTypeException);
            }
        }
        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        return response;
    }

    private void crossReferencePatients(ADTDecoder decoder) {
        Patient deactivatedPatient = decoder.getDeactivatedPatient();
        Patient newPatient = decoder.getNewPatient();
        if ((deactivatedPatient == null) && (newPatient == null)) {
            LOG.debug("no patient updated nor created");
        }
        if (newPatient != null) {
            findMatchesForNewPatient(newPatient);
        }
        if ((deactivatedPatient != null) && (deactivatedPatient.getPixReference() != null)) {
            unreferencePatient(deactivatedPatient);
        }
    }

    private void unreferencePatient(Patient deactivatedPatient) {
        CrossReference foundReference = deactivatedPatient.getPixReference();
        deactivatedPatient.setPixReference(null);
        deactivatedPatient.savePatient(entityManager);
        if (foundReference.getPatients().size() <= 1) {
            for (Patient patient : foundReference.getPatients()) {
                patient.setPixReference(null);
                patient.savePatient(entityManager);
            }
            entityManager.remove(foundReference);
            entityManager.flush();
        }
    }

    private void findMatchesForNewPatient(Patient newPatient) {
        newPatient = newPatient.computeDoubleMetaphones(entityManager);
        List<Patient> similarPatients = newPatient.searchForReferences(entityManager);
        if ((similarPatients != null) && !similarPatients.isEmpty()) {
            CrossReference reference = null;
            for (Patient patient : similarPatients) {
                LOG.debug("patient matches: " + patient.getFirstName() + ", "
                        + patient.getLastName() + ", " + patient.getMotherMaidenName());
                if (patient.getPixReference() == null) {
                    reference = new CrossReference();
                    reference.setPatients(new ArrayList<Patient>());
                    reference.getPatients().add(newPatient);
                    reference.getPatients().add(patient);
                    patient.setPixReference(reference);
                    newPatient.setPixReference(reference);
                    reference.updateReference(entityManager, sendingApplication + "_"
                            + sendingFacility);
                } else {
                    reference = patient.getPixReference();
                    reference.getPatients().add(newPatient);
                    reference.updateReference(entityManager, sendingApplication + "_"
                            + sendingFacility);
                }
            }
        }
    }

    private Message processITI9Message(QBP_Q21 qbpMessage) throws HL7Exception {

        QBPMessageDecoder qbpDecoder = new QBPMessageDecoder(qbpMessage);
        List<Integer> unsupportedDomains = qbpDecoder.checkWhatDomainsReturned(entityManager);
        List<PatientIdentifier> identifiers = null;
        QueryResponseStatus responseStatus = qbpDecoder.getQueryResponseStatus();
        if (responseStatus == null) {
            // search the list of matching patient identifiers
            identifiers = qbpDecoder.getListOfPatientIdentifiers(entityManager);
            responseStatus = qbpDecoder.getQueryResponseStatus();
        }
        RSPMessageBuilder messageBuilder = new RSPMessageBuilder(identifiers, unsupportedDomains,
                qbpMessage);
        return messageBuilder.build(responseStatus, serverApplication, serverFacility);
    }

    private Message processITI8Message(Message receivedMessage, String triggerEvent, ADTDecoder decoder) throws DataTypeException {
        // lie on hl7 version declared in the message in order to parse it as a 2.5 message
        Message response;
        if (triggerEvent.equals("A40")) {
            ADT_A39 adtMessage = (ADT_A39) receivedMessage;
            adtMessage.getMSH().getVersionID().getVersionID().setValue("2.5");
            response = decoder.decode(adtMessage, triggerEvent);
        } else {
            ADT_A01 adtMessage = (ADT_A01) receivedMessage;
            adtMessage.getMSH().getVersionID().getVersionID().setValue("2.5");
            if (triggerEvent.equals("A05")) {
                adtMessage.getMSH().getMessageType().getMessageStructure().setValue("ADT_A05");
            }
            response = decoder.decode(adtMessage, triggerEvent);
        }
        if (response != null) {
            ACK ack = (ACK) response;
            ack.getMSH().getVersionID().getVersionID().setValue("2.3.1");
            if (ack.getERRReps() > 0) {
                // ERR segment exists
                String errorCode = ack.getERR().getHL7ErrorCode().getIdentifier().getValue();
                ack.getERR().clear();
                ack.getERR().getErrorCodeAndLocation(0).getEld4_CodeIdentifyingError()
                        .getIdentifier().setValue(errorCode);
            }
            response = ack;
        }
        return response;
    }

    @Override
    public String getSutActorKeyword() {
        return "PAT_IDENTITY_SRC";
    }
}
