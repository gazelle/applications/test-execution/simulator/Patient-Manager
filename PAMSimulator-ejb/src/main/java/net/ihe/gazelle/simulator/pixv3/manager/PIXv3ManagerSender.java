package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.SoapHL7V3WebServiceClient;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;

import javax.xml.namespace.QName;
import java.io.Serializable;

/**
 * Created by gthomazon on 24/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PIXv3ManagerSender extends SoapHL7V3WebServiceClient implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4263239374893323024L;

	private static final String ACTION_REVISE = "urn:hl7-org:v3:PRPA_IN201302UV02";
	private static final String SERVICE_NAME = "PIXConsumer_Service";
	private static final String SERVICE_PORT_TYPE = "PIXConsumer_Port_Soap12";

	/**
	 * <p>Constructor for PIXv3ManagerSender.</p>
	 *
	 * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public PIXv3ManagerSender(HL7v3ResponderSUTConfiguration inSUT) {
		super(inSUT);
	}

	/** {@inheritDoc} */
	@Override
	protected QName getServiceQName() {
		return new QName(PIXV3_TNS, SERVICE_NAME);
	}

	/** {@inheritDoc} */
	@Override
	protected QName getPortQName() {
		return new QName(PIXV3_TNS, SERVICE_PORT_TYPE);
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSimulatedActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
	}

	/** {@inheritDoc} */
	@Override
	protected Transaction getSimulatedTransaction() {
		return Transaction.GetTransactionByKeyword("ITI-46");
	}

	/** {@inheritDoc} */
	@Override
	protected Actor getSutActor() {
		return Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER");
	}

	/**
	 * <p>sendRevisePatientRecord.</p>
	 *
	 * @param request a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
	 */
	public MCCIIN000002UV01Type sendRevisePatientRecord(PRPAIN201302UV02Type request) throws SoapSendException {
        return sendPRPAIN201302UV02(request, ACTION_REVISE, null);
	}
}
