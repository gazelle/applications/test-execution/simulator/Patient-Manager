package net.ihe.gazelle.simulator.pam.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by aberge on 09/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Name("patientPhoneNumber")
@DiscriminatorValue("patient_phone_number")
public class PatientPhoneNumber extends AbstractPhoneNumber implements Serializable {



    @ManyToOne(targetEntity = Patient.class)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "is_principal")
    private boolean principal;

    /**
     * <p>Constructor for PatientPhoneNumber.</p>
     */
    public PatientPhoneNumber(){
        super();
        this.principal = true;
    }

    /**
     * <p>Constructor for PatientPhoneNumber.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public PatientPhoneNumber(Patient inPatient) {
        super();
        this.principal = true;
        this.patient = inPatient;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * <p>isPrincipal.</p>
     *
     * @return a boolean.
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * <p>Setter for the field <code>principal</code>.</p>
     *
     * @param principal a boolean.
     */
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientPhoneNumber)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientPhoneNumber that = (PatientPhoneNumber) o;

        return principal == that.principal;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (principal ? 1 : 0);
        return result;
    }
}
