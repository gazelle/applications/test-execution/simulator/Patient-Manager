package net.ihe.gazelle.simulator.pam.iti31.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.pds.ITI30ManagerLocal;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>EncounterManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("encounterManager")
@Scope(ScopeType.PAGE)
public class EncounterManager implements Serializable, UserAttributeCommon {

    private static Logger log = LoggerFactory.getLogger(EncounterManager.class);

    private static final long serialVersionUID = 1L;

    private Encounter encounter;
    private Movement selectedMovement;

    @In(value="gumUserService")
    private UserService userService;

    /**
     * <p>Getter for the field <code>selectedMovement</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public Movement getSelectedMovement() {
        return selectedMovement;
    }

    /**
     * <p>Setter for the field <code>selectedMovement</code>.</p>
     *
     * @param selectedMovement a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public void setSelectedMovement(Movement selectedMovement) {
        this.selectedMovement = selectedMovement;
    }

    /**
     * <p>Getter for the field <code>encounter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getEncounter() {
        return encounter;
    }

    /**
     * <p>displayBP6Attributes.</p>
     *
     * @return a boolean.
     */
    public boolean displayBP6Attributes() {
        if (encounter != null && encounter.getSimulatedActor() != null && (encounter.getSimulatedActor().getKeyword().equals("PES")
                || encounter.getSimulatedActor().getKeyword().equals("PEC"))) {
            ITI30ManagerLocal local = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
            return local.isBp6Authorized();
        } else {
            return false;
        }
    }

    /**
     * <p>getEncounterById.</p>
     */
    @Create
    public void getEncounterById() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if ((params != null) && params.containsKey("id")) {
            try {
                Integer encounterId = Integer.decode(params.get("id"));
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                encounter = entityManager.find(Encounter.class, encounterId);
                if ((encounter != null) && !encounter.isOpen()) {
                    FacesMessages.instance().addFromResourceBundle("gazelle.simulator.pam.encounterClosed");
                }
            } catch (NumberFormatException e) {
                encounter = null;
                log.error(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id is not an Integer");
            }
        }
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public String redirectToWorklistPage(){
        return "/patient/createWorklist.seam?eid=" + encounter.getId();
    }

    /**
     * <p>isWorklistEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isWorklistEnabled(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.MOD_WORKLIST);
    }

}
