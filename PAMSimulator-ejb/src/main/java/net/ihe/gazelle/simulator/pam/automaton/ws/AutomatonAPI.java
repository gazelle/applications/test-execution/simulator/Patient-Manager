package net.ihe.gazelle.simulator.pam.automaton.ws;


import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Local
@Path("/automaton")
@Api("/automaton")
public interface AutomatonAPI {

    String TEXT_PLAIN = "text/plain";
    String INTEGER_CLASS = "java.lang.Integer";
    String XML = "application/xml";

    @GET
    @Path("/listGraphs")
    @Produces(XML)
    @ApiOperation(value = "List the available graphs", notes = "List the available graphs")
    @ApiResponses(value = {@ApiResponse(value = "The identifier of the current execution")})
    Response listGraphs();

    @GET
    @Path("/RunAutomaton")
    @Produces(TEXT_PLAIN)
    @ApiOperation(value = "Execute the automaton remotely", notes = "Execute a test automaton with a patient randomly generated", responseClass =
            INTEGER_CLASS)
    @ApiResponses(value = {@ApiResponse(errors = {
            @ApiError(code = 400, reason = "Given parameters are invalid"),
            @ApiError(code = 404, reason = "Configuration of the automaton failed"),
            @ApiError(code = 503, reason = "Internal error, details given in HTTP error")}, value = "The identifier of the current execution")})
    Response runAutomaton(@ApiParam(value = "Identifier of the graph to execute", required = true) @QueryParam("graphmlID") Integer graphmlID,
                          @ApiParam(value = "Indicates whether or not you want the graph to cover all the movements (default: only covers all " +
                                  "statuses)", required = false) @QueryParam("fullmovementcoverage") boolean fullmovementcoverage,
                          @ApiParam(value = "Indicates whether or not messages are validated on the fly (default is false)", required = false)
                          @QueryParam("validatetransactioninstance") boolean validatetransactioninstance,
                          @ApiParam(value = "Indicates whether or not the tool first sends a A28 message (default is false)", required = false)
                          @QueryParam("startwithanA28") boolean startwithanA28,
                          @ApiParam(value = "Name of the system under test registered in the tool to which send messages", required = true)
                          @QueryParam("systemundertest") String systemundertest,
                          @ApiParam(value = "Country code for the generated patient (default: random)", required = false) @QueryParam("country")
                                  String
                                  patientsCountryCode,
                          @ApiParam(value = "Use true to send messages compliant with PAM-FR (default is false)", required = false) @QueryParam
                                  ("BP6Mode") boolean BP6Mode);


    @GET
    @Path("/RunAutomatonWithExistingPatient")
    @Produces(TEXT_PLAIN)
    @ApiOperation(value = "Execute the automaton for an existing patient", notes = "Execute the test automaton for a given patient", responseClass
            = INTEGER_CLASS)
    @ApiResponses(value = {@ApiResponse(errors = {
            @ApiError(code = 400, reason = "Given parameters are invalid"),
            @ApiError(code = 404, reason = "Configuration of the automaton failed"),
            @ApiError(code = 503, reason = "Internal error, details given in HTTP error")}, value = "The identifier of the current execution")})
    Response runAutomatonWithExistingPatient(@ApiParam(value = "Identifier of the graph to execute") @QueryParam("graphmlID") Integer graphmlID,
                                             @ApiParam(value = "Indicates whether or not you want the graph to cover all the movements (default: " +
                                                     "only covers all statuses)", required = false) @QueryParam("fullmovementcoverage") boolean
                                                     fullmovementcoverage,
                                             @ApiParam(value = "Indicates whether or not messages are validated on the fly (default is false)",
                                                     required = false) @QueryParam("validatetransactioninstance") boolean validatetransactioninstance,
                                             @ApiParam(value = "Indicates whether or not the tool first sends a A28 message (default is false)",
                                                     required = false) @QueryParam("startwithanA28") boolean startwithanA28,
                                             @ApiParam(value = "Name of the system under test registered in the tool to which send messages",
                                                     required = true) @QueryParam("systemundertest") String systemundertest,
                                             @ApiParam(value = "DB identifier of the patient to use") @QueryParam("patientID") Integer patientID,
                                             @ApiParam(value = "Use true to send messages compliant with PAM-FR (default is false)", required =
                                                     false) @QueryParam("BP6Mode") boolean BP6Mode);


    @GET
    @Path("/StatusExecution")
    @Produces(TEXT_PLAIN)
    @ApiOperation(value = "Gives the status of the given execution", notes = "Gives the status of the given execution", responseClass = INTEGER_CLASS)
    @ApiResponses(value = {@ApiResponse(errors = {
            @ApiError(code = 400, reason = "Missing executionID parameter")})})
    Response statusExecution(@ApiParam(value = "Identifier of the automaton execution for which you want to get the status", required = true)
                             @QueryParam("executionID") Integer executionID);


}
