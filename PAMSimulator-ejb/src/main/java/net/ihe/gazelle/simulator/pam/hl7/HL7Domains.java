package net.ihe.gazelle.simulator.pam.hl7;

import gnu.trove.set.hash.THashSet;

import java.io.Serializable;

public class HL7Domains extends THashSet<Class<? extends HL7Domain<?>>> implements Serializable {
    public HL7Domains() {
        super(2);
        this.add(BP6HL7Domain.class);
        this.add(IHEHL7Domain.class);
    }
}
