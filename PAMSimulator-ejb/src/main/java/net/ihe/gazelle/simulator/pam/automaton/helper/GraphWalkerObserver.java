package net.ihe.gazelle.simulator.pam.automaton.helper;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DependsOn;

/**
 * <p>GraphWalkerObserver class.</p>
 *
 * @author aberge
 * @version 1.0: 30/10/17
 */
@Name("graphWalkerObserver")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
@GenerateInterface("IGraphWalkerObserver")
public class GraphWalkerObserver implements IGraphWalkerObserver{

    private static final Logger LOG = LoggerFactory.getLogger(GraphWalkerObserver.class);
    private static final String OBSERVER_NAME = "executeGraphWalkerAsync";

    @Observer(OBSERVER_NAME)
    @Transactional
    public void runGraphWalker(GraphExecutionResults selectedGraphExecutionResults, Encounter encounter) {
        GraphWalkerExecution.run(selectedGraphExecutionResults, encounter);
    }

    public static String getObserverName(){
        return OBSERVER_NAME;
    }
}
