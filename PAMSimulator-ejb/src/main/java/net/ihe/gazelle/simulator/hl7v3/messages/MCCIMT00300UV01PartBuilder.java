package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.*;
import net.ihe.gazelle.hl7v3.voc.AcknowledgementDetailType;
import net.ihe.gazelle.hl7v3.voc.CommunicationFunctionType;
import net.ihe.gazelle.hl7v3.voc.EntityClassDevice;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MCCIMT00300UV01PartBuilder extends HL7v3MessageBuilder{

	/**
	 * <p>buildReceiver.</p>
	 *
	 * @param deviceOid a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Receiver} object.
	 */
	public static MCCIMT000300UV01Receiver buildReceiver(String deviceOid) {
		MCCIMT000300UV01Receiver receiver = new MCCIMT000300UV01Receiver();
		receiver.setTypeCode(CommunicationFunctionType.RCV);
		receiver.setDevice(buildDevice(deviceOid, null));
		return receiver;
	}

	/**
	 * <p>buildReceiver.</p>
	 *
	 * @param sut a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Receiver} object.
	 */
	public static MCCIMT000300UV01Receiver buildReceiver(HL7v3ResponderSUTConfiguration sut) {
		MCCIMT000300UV01Receiver receiver = new MCCIMT000300UV01Receiver();
		receiver.setTypeCode(CommunicationFunctionType.RCV);
		receiver.setDevice(buildDevice(sut.getDeviceOID(), sut.getUrl()));
		return receiver;
	}

	/**
	 * <p>buildSender.</p>
	 *
	 * @param senderOid a {@link java.lang.String} object.
	 * @param url a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Sender} object.
	 */
	public static MCCIMT000300UV01Sender buildSender(String senderOid, String url) {
		MCCIMT000300UV01Sender sender = new MCCIMT000300UV01Sender();
		sender.setTypeCode(CommunicationFunctionType.SND);
		sender.setDevice(buildDevice(senderOid, url));
		return sender;
	}

	/**
	 * <p>buildDevice.</p>
	 *
	 * @param deviceOID a {@link java.lang.String} object.
	 * @param value a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Device} object.
	 */
	public static MCCIMT000300UV01Device buildDevice(String deviceOID, String value) {
		MCCIMT000300UV01Device device = new MCCIMT000300UV01Device();
		device.setClassCode(EntityClassDevice.DEV);
		device.setDeterminerCode(EntityDeterminer.INSTANCE);
		device.addId(new II(deviceOID, null));
		if (value != null && !value.isEmpty()) {
			device.addTelecom(populateTelecom(value));
		}
		return device;
	}

	/**
	 * <p>populateAcknowledgement.</p>
	 *
	 * @param incomingMessageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 * @param typeCode a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Acknowledgement} object.
	 */
	public static MCCIMT000300UV01Acknowledgement populateAcknowledgement(II incomingMessageId, String typeCode) {
		MCCIMT000300UV01Acknowledgement acknowledgement = new MCCIMT000300UV01Acknowledgement();
		acknowledgement.setTypeCode(new CS(typeCode, null, null));
		acknowledgement.setTargetMessage(populateTargetMessage(incomingMessageId));
		return acknowledgement;
	}

	/**
	 * <p>populateAcknowledgementDetail.</p>
	 *
	 * @param type a {@link net.ihe.gazelle.hl7v3.voc.AcknowledgementDetailType} object.
	 * @param code a {@link java.lang.String} object.
	 * @param location a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01AcknowledgementDetail} object.
	 */
	public static MCCIMT000300UV01AcknowledgementDetail populateAcknowledgementDetail(AcknowledgementDetailType type,
			String code, String location){
		MCCIMT000300UV01AcknowledgementDetail detail = new MCCIMT000300UV01AcknowledgementDetail();
		if (type != null) {
			detail.setTypeCode(AcknowledgementDetailType.E);
		}
		if (code != null){
			detail.setCode(new CE(code, null, null));
		}
		if (location != null) {
			detail.getLocation().add(buildST(location));
		}
		return detail;
	}

	/**
	 * <p>populateTargetMessage.</p>
	 *
	 * @param targetMessageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01TargetMessage} object.
	 */
	public static MCCIMT000300UV01TargetMessage populateTargetMessage(II targetMessageId) {
		MCCIMT000300UV01TargetMessage targetMessage = new MCCIMT000300UV01TargetMessage();
		targetMessage.setId(new II(targetMessageId.getRoot(), targetMessageId.getExtension()));
		return targetMessage;
	}
}
