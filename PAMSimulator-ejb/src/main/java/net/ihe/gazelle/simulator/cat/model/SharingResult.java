package net.ihe.gazelle.simulator.cat.model;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.Patient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>SharingResult class.</p>
 *
 * @author abe
 * @version 1.0: 08/12/17
 */

public class SharingResult implements Serializable{

    private List<TransactionInstance> transactionInstances;
    private List<String> issues;
    private boolean processEnded;
    private List<SystemUnderTestPreferences> selectedSystems;
    private List<Patient> selectedPatients;

    public SharingResult(){
        this.transactionInstances = new ArrayList<TransactionInstance>();
        this.issues = new ArrayList<String>();
        this.processEnded = false;
    }

    public List<TransactionInstance> getTransactionInstances() {
        return transactionInstances;
    }

    public List<String> getIssues() {
        return issues;
    }

    public boolean isProcessEnded() {
        return processEnded;
    }

    public void setProcessEnded(boolean processEnded) {
        this.processEnded = processEnded;
    }

    public void addTransactionInstance(TransactionInstance instance){
        this.transactionInstances.add(instance);
    }

    public void addIssue(String issueMessage){
        this.issues.add(issueMessage);
    }

    public List<SystemUnderTestPreferences> getSelectedSystems() {
        return selectedSystems;
    }

    public void setSelectedSystems(List<SystemUnderTestPreferences> selectedSystems) {
        this.selectedSystems = selectedSystems;
    }

    public List<Patient> getSelectedPatients() {
        return selectedPatients;
    }

    public void setSelectedPatients(List<Patient> selectedPatients) {
        this.selectedPatients = selectedPatients;
    }

    public void setSingleSystem(SystemUnderTestPreferences preferences){
        this.selectedSystems = new ArrayList<SystemUnderTestPreferences>();
        selectedSystems.add(preferences);
    }

    public void clearMessageList(){
        this.transactionInstances = new ArrayList<TransactionInstance>();
    }
}
