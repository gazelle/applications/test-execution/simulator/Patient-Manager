package net.ihe.gazelle.simulator.pam.pds;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.utils.Pair;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ChangePatientIdManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("changePatientIdManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class ChangePatientIdManager extends AbstractPatientIdManager implements Serializable {

    /**
     *
     */
    private static org.slf4j.Logger log = LoggerFactory.getLogger(ChangePatientIdManager.class);

    private static final long serialVersionUID = 1L;

    private List<Pair<Integer, String>> patientIdentifiers;
    private boolean changeEnable = false;

    // not used in xhtml
    private Integer idForUpdate;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateSelectedEvent() {
        log.info("updating selected event on bean creation");
        this.canSendMessage = false;
        ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
        bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A47);
    }

    /**
     * <p>Getter for the field <code>idForUpdate</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getIdForUpdate() {
        return idForUpdate;
    }

    /**
     * <p>Setter for the field <code>idForUpdate</code>.</p>
     *
     * @param idForUpdate a {@link java.lang.Integer} object.
     */
    public void setIdForUpdate(Integer idForUpdate) {
        this.idForUpdate = idForUpdate;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Pair<Integer, String>> getPatientIdentifiers() {
        return patientIdentifiers;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<Pair<Integer, String>> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Setter for the field <code>changeEnable</code>.</p>
     *
     * @param changeEnable a boolean.
     */
    public void setChangeEnable(boolean changeEnable) {
        this.changeEnable = changeEnable;
    }

    /**
     * <p>isChangeEnable.</p>
     *
     * @return a boolean.
     */
    public boolean isChangeEnable() {
        return changeEnable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage() {
        if (changeEnable) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "The patient's identifiers has not been changed. Please modify "
                            + "one of the patient's identifiers in order to tell the tool which "
                            + "identifier use to populate the MRG segment");
        }
        super.sendMessage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionOnSelectedPatient(Patient inPatient) {
        upgradePatientVersion(inPatient);
        buildPatientIdentifiers(); // confectionner la liste des identifiants du patient sélectionné
        changeEnable = true; // autoriser la modification d'un identifiant
        displayPatientsList = false; // lorsque un patient est choisi, cacher la liste des patients
    }

    private void buildPatientIdentifiers() {
        if (selectedPatient.getPatientIdentifiers() != null) {
            patientIdentifiers = new ArrayList<>();
            for (PatientIdentifier pid : selectedPatient.getPatientIdentifiers()) {
                String pidValue = pid.getFullPatientId();
                if (pid.getIdentifierTypeCode() != null) {
                    pidValue = pidValue.concat("^").concat(pid.getIdentifierTypeCode());
                }
                patientIdentifiers.add(new Pair<>(pid.getId(), pidValue));
            }
        } else {
            patientIdentifiers = null;
        }
    }

    /**
     * <p>generateAPatient.</p>
     */
    @Override
    public void generateAPatient() {
        super.generateAPatient();
        if (selectedPatient != null) {
            buildPatientIdentifiers();
        }
    }

    /**
     * <p>changeIdentifier.</p>
     *
     * @param pid         a {@link java.lang.Integer} object.
     * @param newIdString a {@link java.lang.String} object.
     */
    public void changeIdentifier(Integer pid, String newIdString) {
        try {
            correctPatientIdentifier = null;
            incorrectPatientIdentifier = null;
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            PatientIdentifier modifiedId = entityManager.find(PatientIdentifier.class, pid);
            Patient x = patientService.changeIdentifier(selectedPatient, modifiedId, newIdString);
            selectedPatient = x;
            incorrectPatientIdentifier = modifiedId;
            for (PatientIdentifier id:x.getPatientIdentifiers()) {
                if (id.getFullPatientId().equals(newIdString.replaceFirst("\\^[^\\^]+$",""))) {
                    correctPatientIdentifier = id;
                    break;
                }
            }
            buildPatientIdentifiers();
            canSendMessage = true;
            changeEnable = false;
        } catch (Exception e) {
            changeEnable = true;
        }
        idForUpdate = null;
    }
}
