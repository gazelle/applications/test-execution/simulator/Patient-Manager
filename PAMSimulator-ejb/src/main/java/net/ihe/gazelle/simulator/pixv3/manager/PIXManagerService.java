package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.hl7v3.responder.HL7V3ResponderUtils;
import net.ihe.gazelle.simulator.hl7v3.responder.PIXV3QueryHandler;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.Addressing;

/**
 * Created by aberge on 11/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Stateless
@Name("PIXManagerService")
@WebService(portName = "PIXManager_Port_Soap12", name = "PIXManager_PortType", targetNamespace = "urn:ihe:iti:pixv3:2007", serviceName = "PIXManager_Service")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@Addressing(enabled = true, required = true)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@HandlerChain(file = "soap-handler.xml")
@GenerateInterface(value = "PIXManagerServiceRemote", isLocal = false, isRemote = true)
public class PIXManagerService implements PIXManagerServiceRemote{

	private static final String PIX_MGR_ACTOR_KEYWORD = "PAT_IDENTITY_X_REF_MGR";
	private static final String PIX_SRC_ACTOR_KEYWORD = "PAT_IDENTITY_SRC";
	private static final String ITI_44_KEYWORD = "ITI-44";
	private static final String ITI_45_KEYWORD = "ITI-45";
	private static final String PIX_CONS_ACTOR_KEYWORD = "PAT_IDENTITY_CONSUMER";
	private static final Logger LOGGER = LoggerFactory.getLogger(PIXManagerService.class);
	private static final String HL7_NS = "urn:hl7-org:v3";

	@Resource
	private WebServiceContext context;


	@WebMethod(operationName = "PIXManager_PRPA_IN201301UV02", action = "urn:hl7-org:v3:PRPA_IN201301UV02")
	@WebResult(name = "MCCI_IN000002UV01", partName = "Body", targetNamespace = HL7_NS)
	@Action(input = "urn:hl7-org:v3:PRPA_IN201301UV02", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
	public MCCIIN000002UV01Type addPatientRecord(
			@WebParam(name = "PRPA_IN201301UV02", partName = "Body", targetNamespace = HL7_NS) PRPAIN201301UV02Type request) throws HL7V3ParserException {
		Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		PIXV3QueryHandler queryHandler = new PIXV3QueryHandler(domain,
				Actor.findActorWithKeyword(PIX_MGR_ACTOR_KEYWORD), Actor.findActorWithKeyword(PIX_SRC_ACTOR_KEYWORD),
				Transaction.GetTransactionByKeyword(ITI_44_KEYWORD), getServletRequest(), context.getMessageContext());
		try {
			return queryHandler.handleAddRecordMessage(request);
		} catch (HL7V3ParserException e) {
            throw new HL7V3ParserException(e.getMessage(),e.getMessage(),e);
		}

	}


	@WebMethod(operationName = "PIXManager_PRPA_IN201302UV02", action = "urn:hl7-org:v3:PRPA_IN201302UV02")
	@WebResult(name = "MCCI_IN000002UV01", partName = "Body", targetNamespace = HL7_NS)
	@Action(input = "urn:hl7-org:v3:PRPA_IN201302UV02", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
	public MCCIIN000002UV01Type revisePatientRecord(
			@WebParam(name = "PRPA_IN201302UV02", partName = "Body", targetNamespace = HL7_NS) PRPAIN201302UV02Type request) throws HL7V3ParserException {
		Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		PIXV3QueryHandler queryHandler = new PIXV3QueryHandler(domain,
				Actor.findActorWithKeyword(PIX_MGR_ACTOR_KEYWORD), Actor.findActorWithKeyword(PIX_SRC_ACTOR_KEYWORD),
				Transaction.GetTransactionByKeyword(ITI_44_KEYWORD), getServletRequest(), context.getMessageContext());
		try {
			return queryHandler.handleReviseRecordMessage(request);
		} catch (HL7V3ParserException e) {
            throw new HL7V3ParserException(e.getMessage(),e.getMessage(),e);
		}
	}


	@WebMethod(operationName = "PIXManager_PRPA_IN201304UV02", action = "urn:hl7-org:v3:PRPA_IN201304UV02")
	@WebResult(name = "MCCI_IN000002UV01", partName = "Body", targetNamespace = HL7_NS)
	@Action(input = "urn:hl7-org:v3:PRPA_IN201304UV02", output = "urn:hl7-org:v3:MCCI_IN000002UV01")
	public MCCIIN000002UV01Type patientIdentityMerge(
			@WebParam(name = "PRPA_IN201304UV02", partName = "Body", targetNamespace = HL7_NS) PRPAIN201304UV02Type request) throws HL7V3ParserException {
		Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		PIXV3QueryHandler queryHandler = new PIXV3QueryHandler(domain,
				Actor.findActorWithKeyword(PIX_MGR_ACTOR_KEYWORD), Actor.findActorWithKeyword(PIX_SRC_ACTOR_KEYWORD),
				Transaction.GetTransactionByKeyword(ITI_44_KEYWORD), getServletRequest(), context.getMessageContext());
		try {
			return queryHandler.handlePatientIdentityMergeMessage(request);
		} catch (HL7V3ParserException e) {
            throw new HL7V3ParserException(e.getMessage(),e.getMessage(),e);
		}
	}


	@WebMethod(operationName = "PIXManager_PRPA_IN201309UV02", action = "urn:hl7-org:v3:PRPA_IN201309UV02")
	@WebResult(name = "PRPA_IN201310UV02", partName = "Body", targetNamespace = HL7_NS)
	@Action(input = "urn:hl7-org:v3:PRPA_IN201309UV02", output = "urn:hl7-org:v3:PRPA_IN201310UV02")
	public PRPAIN201310UV02Type getCorrespondingIdentifiers(
			@WebParam(name = "PRPA_IN201309UV02", partName = "Body", targetNamespace = HL7_NS) PRPAIN201309UV02Type request) throws HL7V3ParserException {
		Domain domain = HL7V3ResponderUtils.getDefaultDomain();
		PIXV3QueryHandler queryHandler = new PIXV3QueryHandler(domain,
				Actor.findActorWithKeyword(PIX_MGR_ACTOR_KEYWORD), Actor.findActorWithKeyword(PIX_CONS_ACTOR_KEYWORD),
				Transaction.GetTransactionByKeyword(ITI_45_KEYWORD), getServletRequest(), context.getMessageContext());
		try {
			return queryHandler.handleGetCorrespondingIdentifiersQuery(request);
		} catch (HL7V3ParserException e) {
			throw new HL7V3ParserException(e.getMessage(),e.getMessage(),e);
		}
	}

	private HttpServletRequest getServletRequest() {
		return HL7V3Utils.getServletRequestFromContext(context);
	}
}
