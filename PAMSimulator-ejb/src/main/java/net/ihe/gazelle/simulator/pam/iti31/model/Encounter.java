package net.ihe.gazelle.simulator.pam.iti31.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.datamodel.EncounterDataModel;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Encounter class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "encounter")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("encounter")
@Table(name = "pam_encounter", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_encounter_sequence", sequenceName = "pam_encounter_id_seq", allocationSize = 1)
// @Inheritance(strategy=InheritanceType.JOINED)
public class Encounter implements Serializable{

	private static org.slf4j.Logger log = LoggerFactory.getLogger(Encounter.class);

	/**
	 *
	 */
	private static final long serialVersionUID = 5020862027352205040L;



	@Id
	@GeneratedValue(generator = "pam_encounter_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "simulated_actor_id")
	private Actor simulatedActor;

	@ManyToOne
	@JoinColumn(name = "patient_id")
	@XmlElement(name = "patient")
	private Patient patient;

	@Column(name = "status")
	private PatientStatus patientStatus;

	@Column(name = "prior_status")
	private PatientStatus priorPatientStatus;

	@Column(name = "patient_class_code")
	@XmlElement(name = "patientClass")
	private String patientClassCode;

	@Column(name = "prior_patient_class_code")
	private String priorPatientClassCode;

	@Column(name = "admission_type")
	private String admissionType;

	@Transient
	private boolean accident;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "accident_date")
	private Date accidentDate;

	@Column(name = "accident_code")
	private String accidentCode;
	/**
	 * null if no absence planned or past true if patient goes for a leave or absence false if the patient is returned form a leave of absence
	 */
	@Column(name = "leave_of_absence")
	private Boolean leaveOfAbsence;

	@Column(name = "visit_number")
	@XmlElement(name = "visitNumber")
	private String visitNumber;

	@Column(name = "visit_number_id")
	private String visitNumberId;

	@ManyToOne
	@JoinColumn(name = "visit_designator_id")
	private HierarchicDesignator visitDesignator;

	@Column(name = "attending_doctor_code")
	private String attendingDoctorCode;

	/**
	 * used in A55: cancel change of attending doctor
	 */
	@Column(name = "prior_attending_doctor_code")
	private String priorAttendingDoctorCode;

	@XmlElement(name = "referringDoctor")
	@Column(name = "referring_doctor_code")
	private String referringDoctorCode;

	@Column(name = "expected_admit_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expectedAdmitDate;

	@Column(name = "admit_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date admitDate;

	@Column(name = "discharge_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dischargeDate;

	@Column(name = "leave_of_absence_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date leaveOfAbsenceDate;

	@Column(name = "expected_loa_return")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expectedLOAReturn;

	@Column(name = "loa_return_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date loaReturnDate;

	@Column(name = "expected_discharge_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expectedDischargeDate;

	@Column(name = "transfer_planned_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transferPlannedDate;

	@Column(name = "creator")
	private String creator;

	@Column(name = "creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "is_open")
	private boolean open;

	@Column(name = "hospital_service_code")
	private String hospitalServiceCode;

	@Column(name = "admitting_doctor_code")
	private String admittingDoctorCode;

	@OneToMany(mappedBy = "encounter")
	@OrderBy("movementDate asc")
	@XmlElement(name = "movements")
	private List<Movement> movements;

	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "encounter_additional_info_id")
	private EncounterAdditionalInfo encounterAdditionalInfo;

	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "drg_movement_id")
	private DRGMovement drgMovement;

	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "patients_phr_info_id")
	private PatientsPHRInfo patientsPHRInfo;

	@Column(name = "patient_account_number")
	private String patientAccountNumber;

	/**
	 * <p>Constructor for Encounter.</p>
	 */
	public Encounter() {
		this.accident = false;
		this.creationDate = new Date();
		this.patient = null;
	}

	/**
	 * <p>Constructor for Encounter.</p>
	 *
	 * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public Encounter(Patient patient) {
		this.creationDate = new Date();
		this.patient = patient;
		this.accident = false;
		this.patientAccountNumber = patient.getAccountNumber();
	}

	/**
	 * <p>Getter for the field <code>admissionType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAdmissionType() {
		return admissionType;
	}

	/**
	 * <p>Setter for the field <code>admissionType</code>.</p>
	 *
	 * @param admissionType a {@link java.lang.String} object.
	 */
	public void setAdmissionType(String admissionType) {
		this.admissionType = admissionType;
	}

	/**
	 * <p>getPatientClass.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPatientClass() {
		return this.patientClassCode;
	}

	/**
	 * <p>getPermanentLink.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPermanentLink() {
		if (id != null) {
			String baseUrl = PreferenceService.getString("application_url");
			return baseUrl + "/encounter.seam?id=" + getId();
		} else {
			return null;
		}
	}

	/**
	 * <p>isAccident.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isAccident() {
		return accident;
	}

	/**
	 * <p>Setter for the field <code>accident</code>.</p>
	 *
	 * @param accident a boolean.
	 */
	public void setAccident(boolean accident) {
		this.accident = accident;
	}

	/**
	 * <p>Getter for the field <code>accidentDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getAccidentDate() {
		return accidentDate;
	}

	/**
	 * <p>Setter for the field <code>accidentDate</code>.</p>
	 *
	 * @param accidentDate a {@link java.util.Date} object.
	 */
	public void setAccidentDate(Date accidentDate) {
		if (accidentDate != null) {
			this.accidentDate = (Date) accidentDate.clone();
		} else {
			this.accidentDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>accidentCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAccidentCode() {
		return accidentCode;
	}

	/**
	 * <p>Setter for the field <code>accidentCode</code>.</p>
	 *
	 * @param accidentCode a {@link java.lang.String} object.
	 */
	public void setAccidentCode(String accidentCode) {
		this.accidentCode = accidentCode;
	}

	/**
	 * <p>Setter for the field <code>open</code>.</p>
	 *
	 * @param open
	 *            the open to set
	 */
	public void setOpen(boolean open) {
		this.open = open;
	}

	/**
	 * <p>isOpen.</p>
	 *
	 * @return the open
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * <p>Setter for the field <code>hospitalServiceCode</code>.</p>
	 *
	 * @param hospitalService a {@link java.lang.String} object.
	 */
	public void setHospitalServiceCode(String hospitalService) {
		this.hospitalServiceCode = hospitalService;
	}

	/**
	 * <p>Getter for the field <code>hospitalServiceCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHospitalServiceCode() {
		return hospitalServiceCode;
	}

	/**
	 * <p>getHospitalService.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHospitalService(){
		if (hospitalServiceCode != null){
			return ValueSetProvider.getInstance().getDisplayNameForCode("HOSPITAL_SERVICE", hospitalServiceCode);
		} else {
			return null;
		}
	}

	/**
	 * <p>Setter for the field <code>admittingDoctorCode</code>.</p>
	 *
	 * @param admittingDoctorCode a {@link java.lang.String} object.
	 */
	public void setAdmittingDoctorCode(String admittingDoctorCode) {
		this.admittingDoctorCode = admittingDoctorCode;
	}

	/**
	 * <p>Getter for the field <code>admittingDoctorCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAdmittingDoctorCode() {
		return admittingDoctorCode;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>movements</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Movement> getMovements() {
		if (id != null){
			this.movements = HibernateHelper.getLazyValue(this, "movements", movements);
		}
		return this.movements;
	}

	/**
	 * <p>Setter for the field <code>movements</code>.</p>
	 *
	 * @param movements a {@link java.util.List} object.
	 */
	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}

	/**
	 * <p>Getter for the field <code>patient</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * <p>Setter for the field <code>patient</code>.</p>
	 *
	 * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * <p>Getter for the field <code>patientStatus</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
	 */
	public PatientStatus getPatientStatus() {
		return patientStatus;
	}

	/**
	 * <p>Setter for the field <code>patientStatus</code>.</p>
	 *
	 * @param patientStatus a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
	 */
	public void setPatientStatus(PatientStatus patientStatus) {
		this.patientStatus = patientStatus;
	}

	/**
	 * <p>Getter for the field <code>patientClassCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPatientClassCode() {
		return patientClassCode;
	}

	/**
	 * <p>Setter for the field <code>patientClassCode</code>.</p>
	 *
	 * @param patientClass a {@link java.lang.String} object.
	 */
	public void setPatientClassCode(String patientClass) {
		this.patientClassCode = patientClass;
	}

	/**
	 * <p>Getter for the field <code>leaveOfAbsence</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getLeaveOfAbsence() {
		return leaveOfAbsence;
	}

	/**
	 * <p>Setter for the field <code>leaveOfAbsence</code>.</p>
	 *
	 * @param leaveOfAbsence a {@link java.lang.Boolean} object.
	 */
	public void setLeaveOfAbsence(Boolean leaveOfAbsence) {
		this.leaveOfAbsence = leaveOfAbsence;
	}

	/**
	 * <p>Getter for the field <code>visitNumber</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getVisitNumber() {
		return visitNumber;
	}

	/**
	 * <p>Setter for the field <code>visitNumber</code>.</p>
	 *
	 * @param visitNumber a {@link java.lang.String} object.
	 */
	public void setVisitNumber(String visitNumber) {
		this.visitNumber = visitNumber;
	}

	/**
	 * <p>Getter for the field <code>attendingDoctorCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAttendingDoctorCode() {
		return attendingDoctorCode;
	}

	/**
	 * <p>Setter for the field <code>attendingDoctorCode</code>.</p>
	 *
	 * @param attendingDoctor a {@link java.lang.String} object.
	 */
	public void setAttendingDoctorCode(String attendingDoctor) {
		this.attendingDoctorCode = attendingDoctor;
	}

	/**
	 * <p>Getter for the field <code>leaveOfAbsenceDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getLeaveOfAbsenceDate() {
		return leaveOfAbsenceDate;
	}

	/**
	 * <p>Setter for the field <code>leaveOfAbsenceDate</code>.</p>
	 *
	 * @param leaveOfAbsenceDate a {@link java.util.Date} object.
	 */
	public void setLeaveOfAbsenceDate(Date leaveOfAbsenceDate) {
		if (leaveOfAbsenceDate != null) {
			this.leaveOfAbsenceDate = (Date) leaveOfAbsenceDate.clone();
		} else {
			this.leaveOfAbsenceDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>expectedDischargeDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getExpectedDischargeDate() {
		return expectedDischargeDate;
	}

	/**
	 * <p>Setter for the field <code>expectedDischargeDate</code>.</p>
	 *
	 * @param expectedDischargeDate a {@link java.util.Date} object.
	 */
	public void setExpectedDischargeDate(Date expectedDischargeDate) {
		if (expectedDischargeDate != null) {
			this.expectedDischargeDate = (Date) expectedDischargeDate.clone();
		} else {
			this.expectedDischargeDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>transferPlannedDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getTransferPlannedDate() {
		return transferPlannedDate;
	}

	/**
	 * <p>Setter for the field <code>transferPlannedDate</code>.</p>
	 *
	 * @param transferPlannedDate a {@link java.util.Date} object.
	 */
	public void setTransferPlannedDate(Date transferPlannedDate) {
		if (transferPlannedDate != null) {
			this.transferPlannedDate = (Date) transferPlannedDate.clone();
		} else {
			this.transferPlannedDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>referringDoctorCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReferringDoctorCode() {
		return referringDoctorCode;
	}

	/**
	 * <p>Setter for the field <code>referringDoctorCode</code>.</p>
	 *
	 * @param referringDoctor a {@link java.lang.String} object.
	 */
	public void setReferringDoctorCode(String referringDoctor) {
		this.referringDoctorCode = referringDoctor;
	}

	/**
	 * <p>Setter for the field <code>expectedAdmitDate</code>.</p>
	 *
	 * @param expectedAdmitDate a {@link java.util.Date} object.
	 */
	public void setExpectedAdmitDate(Date expectedAdmitDate) {
		if (expectedAdmitDate != null) {
			this.expectedAdmitDate = (Date) expectedAdmitDate.clone();
		} else {
			this.expectedAdmitDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>expectedAdmitDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getExpectedAdmitDate() {
		return expectedAdmitDate;
	}

	/**
	 * <p>Getter for the field <code>priorPatientStatus</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
	 */
	public PatientStatus getPriorPatientStatus() {
		return priorPatientStatus;
	}

	/**
	 * <p>Setter for the field <code>priorPatientStatus</code>.</p>
	 *
	 * @param priorPatientStatus a {@link net.ihe.gazelle.simulator.pam.model.PatientStatus} object.
	 */
	public void setPriorPatientStatus(PatientStatus priorPatientStatus) {
		this.priorPatientStatus = priorPatientStatus;
	}

	/**
	 * <p>Getter for the field <code>priorPatientClassCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPriorPatientClassCode() {
		return priorPatientClassCode;
	}

	/**
	 * <p>Setter for the field <code>priorPatientClassCode</code>.</p>
	 *
	 * @param priorPatientClass a {@link java.lang.String} object.
	 */
	public void setPriorPatientClassCode(String priorPatientClass) {
		this.priorPatientClassCode = priorPatientClass;
	}

	/**
	 * <p>Getter for the field <code>admitDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getAdmitDate() {
		return admitDate;
	}

	/**
	 * <p>Setter for the field <code>admitDate</code>.</p>
	 *
	 * @param admitDate a {@link java.util.Date} object.
	 */
	public void setAdmitDate(Date admitDate) {
		if (admitDate != null) {
			this.admitDate = (Date) admitDate.clone();
		} else {
			this.admitDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>dischargeDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}

	/**
	 * <p>Setter for the field <code>dischargeDate</code>.</p>
	 *
	 * @param dischargeDate a {@link java.util.Date} object.
	 */
	public void setDischargeDate(Date dischargeDate) {
		if (dischargeDate != null) {
			this.dischargeDate = (Date) dischargeDate.clone();
		} else {
			this.dischargeDate = null;
		}
	}

	/**
	 * <p>Setter for the field <code>simulatedActor</code>.</p>
	 *
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * <p>Getter for the field <code>simulatedActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	/**
	 * <p>Setter for the field <code>creator</code>.</p>
	 *
	 * @param creator a {@link java.lang.String} object.
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * <p>Getter for the field <code>creator</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * <p>Setter for the field <code>creationDate</code>.</p>
	 *
	 * @param creationDate a {@link java.util.Date} object.
	 */
	public void setCreationDate(Date creationDate) {
		if (creationDate != null) {
			this.creationDate = (Date) creationDate.clone();
		} else {
			this.creationDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>creationDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * <p>Getter for the field <code>patientsPHRInfo</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.PatientsPHRInfo} object.
	 */
	public PatientsPHRInfo getPatientsPHRInfo() {
		if (patientsPHRInfo == null){
			patientsPHRInfo = new PatientsPHRInfo();
		}
		return patientsPHRInfo;
	}

	/**
	 * <p>Setter for the field <code>patientsPHRInfo</code>.</p>
	 *
	 * @param patientsPHRInfo a {@link net.ihe.gazelle.simulator.pam.iti31.model.PatientsPHRInfo} object.
	 */
	public void setPatientsPHRInfo(PatientsPHRInfo patientsPHRInfo) {
		this.patientsPHRInfo = patientsPHRInfo;
	}

	/**
	 * <p>Setter for the field <code>loaReturnDate</code>.</p>
	 *
	 * @param loaReturnDate
	 *            the loaReturnDate to set
	 */
	public void setLoaReturnDate(Date loaReturnDate) {
		if (loaReturnDate != null) {
			this.loaReturnDate = (Date) loaReturnDate.clone();
		} else {
			this.loaReturnDate = null;
		}
	}

	/**
	 * <p>Getter for the field <code>loaReturnDate</code>.</p>
	 *
	 * @return the loaReturnDate
	 */
	public Date getLoaReturnDate() {
		return loaReturnDate;
	}

	/**
	 * <p>Setter for the field <code>priorAttendingDoctorCode</code>.</p>
	 *
	 * @param priorAttendingDoctorCode
	 *            the priorAttendingDoctorCode to set
	 */
	public void setPriorAttendingDoctorCode(String priorAttendingDoctorCode) {
		this.priorAttendingDoctorCode = priorAttendingDoctorCode;
	}

	/**
	 * <p>Getter for the field <code>priorAttendingDoctorCode</code>.</p>
	 *
	 * @return the priorAttendingDoctorCode
	 */
	public String getPriorAttendingDoctorCode() {
		return priorAttendingDoctorCode;
	}

	/**
	 * <p>Setter for the field <code>expectedLOAReturn</code>.</p>
	 *
	 * @param expectedLOAReturn
	 *            the expectedLOAReturn to set
	 */
	public void setExpectedLOAReturn(Date expectedLOAReturn) {
		if (expectedLOAReturn != null) {
			this.expectedLOAReturn = (Date) expectedLOAReturn.clone();
		} else {
			this.expectedLOAReturn = null;
		}
	}

	/**
	 * <p>Getter for the field <code>expectedLOAReturn</code>.</p>
	 *
	 * @return the expectedLOAReturn
	 */
	public Date getExpectedLOAReturn() {
		return expectedLOAReturn;
	}

	/**
	 * <p>Getter for the field <code>drgMovement</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.DRGMovement} object.
	 */
	public DRGMovement getDrgMovement() {
		if (drgMovement == null){
			drgMovement = new DRGMovement();
		}
		return drgMovement;
	}

	/**
	 * <p>Setter for the field <code>drgMovement</code>.</p>
	 *
	 * @param drgMovement a {@link net.ihe.gazelle.simulator.pam.iti31.model.DRGMovement} object.
	 */
	public void setDrgMovement(DRGMovement drgMovement) {
		this.drgMovement = drgMovement;
	}

	/**
	 * <p>save.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public Encounter save(EntityManager entityManager) {
		if ((this.visitNumber != null) && (this.visitDesignator == null)) {
			Map<String, String> visitComponents = splitVisitNumber();
			HierarchicDesignator designator = HierarchicDesignator.getDomainAndCreateItIfMissing(
					visitComponents.get("CX-4-1"), visitComponents.get("CX-4-2"), visitComponents.get("CX-4-3"),
					entityManager, DesignatorType.VISIT_NUMBER, null);
			this.setVisitDesignator(designator);
			this.setVisitNumberId(visitNumberId);
		}
		Encounter encounter = entityManager.merge(this);
		entityManager.flush();
		return encounter;
	}

	/**
	 * For a given encounter, returns the current/inserted movement with the specified trigger event
	 *
	 * @param triggerEventToCancel a {@link java.lang.String} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 */
	public Movement getMovementToCancel(String triggerEventToCancel, EntityManager entityManager) {
		HQLQueryBuilder<Movement> builder = new HQLQueryBuilder<Movement>(entityManager, Movement.class);
		builder.addEq("encounter", this);
		// A11 trigger event is used to cancel both A01 and A04 movements
		if (triggerEventToCancel.equals("A01") || triggerEventToCancel.equals("A04")) {
			builder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("triggerEvent", "A01"),
					HQLRestrictions.eq("triggerEvent", "A04")));
		} else {
			builder.addEq("triggerEvent", triggerEventToCancel);
		}
		builder.addEq("currentMovement", true);
		builder.addRestriction(HQLRestrictions.neq("actionType", ActionType.CANCEL));
		List<Movement> movements = builder.getList();
		if ((movements != null) && !movements.isEmpty()) {
			return movements.get(0);
		} else {
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((creator == null) ? 0 : creator.hashCode());
		result = (prime * result) + ((visitNumber == null) ? 0 : visitNumber.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Encounter other = (Encounter) obj;
		if (creator == null) {
			if (other.creator != null) {
				return false;
			}
		} else if (!creator.equals(other.creator)) {
			return false;
		}
		if (visitNumber == null) {
			if (other.visitNumber != null) {
				return false;
			}
		} else if (!visitNumber.equals(other.visitNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>Getter for the field <code>visitNumberId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getVisitNumberId() {
		return visitNumberId;
	}

	/**
	 * <p>Setter for the field <code>visitNumberId</code>.</p>
	 *
	 * @param visitNumberId a {@link java.lang.String} object.
	 */
	public void setVisitNumberId(String visitNumberId) {
		this.visitNumberId = visitNumberId;
	}

	/**
	 * <p>Getter for the field <code>visitDesignator</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
	 */
	public HierarchicDesignator getVisitDesignator() {
		return visitDesignator;
	}

	/**
	 * <p>Setter for the field <code>visitDesignator</code>.</p>
	 *
	 * @param visitDesignator a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
	 */
	public void setVisitDesignator(HierarchicDesignator visitDesignator) {
		this.visitDesignator = visitDesignator;
	}

	/**
	 * <p>Getter for the field <code>encounterAdditionalInfo</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.EncounterAdditionalInfo} object.
	 */
	public EncounterAdditionalInfo getEncounterAdditionalInfo() {
		if (encounterAdditionalInfo == null){
			encounterAdditionalInfo = new EncounterAdditionalInfo();
		}
		return encounterAdditionalInfo;
	}

	/**
	 * <p>Setter for the field <code>encounterAdditionalInfo</code>.</p>
	 *
	 * @param encounterAdditionalInfo a {@link net.ihe.gazelle.simulator.pam.iti31.model.EncounterAdditionalInfo} object.
	 */
	public void setEncounterAdditionalInfo(EncounterAdditionalInfo encounterAdditionalInfo) {
		this.encounterAdditionalInfo = encounterAdditionalInfo;
	}

	/**
	 * <p>Getter for the field <code>patientAccountNumber</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPatientAccountNumber() {
		return patientAccountNumber;
	}

	/**
	 * <p>Setter for the field <code>patientAccountNumber</code>.</p>
	 *
	 * @param patientAccountNumber a {@link java.lang.String} object.
	 */
	public void setPatientAccountNumber(String patientAccountNumber) {
		this.patientAccountNumber = patientAccountNumber;
	}

	/**
	 * <p>splitVisitNumber.</p>
	 *
	 * @return a {@link java.util.Map} object.
	 */
	public Map<String, String> splitVisitNumber() {
		return PatientIdentifierDAO.splitCXField(this.visitNumber);
	}
}
