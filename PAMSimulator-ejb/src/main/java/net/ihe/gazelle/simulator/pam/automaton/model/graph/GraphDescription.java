package net.ihe.gazelle.simulator.pam.automaton.model.graph;

/**
 * Created by xfs on 26/10/15.
 */

import net.ihe.gazelle.hql.FilterLabel;
import org.apache.commons.io.FilenameUtils;
import org.jboss.resteasy.util.Base64;
import org.jboss.seam.annotations.Name;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Description of a graphML used in the automaton.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("graphDescription")
@Table(name = "pam_graph_description", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "pam_graph_description_sequence", sequenceName = "pam_graph_description_id_seq", allocationSize = 1)
@XmlRootElement(name = "GraphDescription")
@XmlAccessorType(XmlAccessType.FIELD)
public class GraphDescription implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_graph_description_sequence")
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @XmlElement(name = "id")
    private Integer id;

    @Column(name = "name")
    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "description")
    @Column(name = "description")
    private String description;

    @XmlTransient
    @Column(name = "imagelink")
    private String imageLink;

    @XmlTransient
    @Column(name = "edges")
    private int edges;

    @XmlTransient
    @Column(name = "vertices")
    private int vertices;

    @XmlTransient
    @Column(name = "graphmllink")
    private String graphmlLink;

    @XmlTransient
    @Column(name = "active")
    private Boolean active;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @FilterLabel
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>edges</code>.</p>
     *
     * @return a int.
     */
    public int getEdges() {
        return edges;
    }

    /**
     * <p>Setter for the field <code>edges</code>.</p>
     *
     * @param edges a int.
     */
    public void setEdges(int edges) {
        this.edges = edges;
    }

    /**
     * <p>Getter for the field <code>vertices</code>.</p>
     *
     * @return a int.
     */
    public int getVertices() {
        return vertices;
    }

    /**
     * <p>Setter for the field <code>vertices</code>.</p>
     *
     * @param vertices a int.
     */
    public void setVertices(int vertices) {
        this.vertices = vertices;
    }

    /**
     * <p>Getter for the field <code>imageLink</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getImageLink() {
        return imageLink;
    }

    /**
     * <p>Setter for the field <code>imageLink</code>.</p>
     *
     * @param imageLink a {@link java.lang.String} object.
     */
    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    /**
     * <p>Getter for the field <code>graphmlLink</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getGraphmlLink() {
        return graphmlLink;
    }

    /**
     * <p>Setter for the field <code>graphmlLink</code>.</p>
     *
     * @param graphmlLink a {@link java.lang.String} object.
     */
    public void setGraphmlLink(String graphmlLink) {
        this.graphmlLink = graphmlLink;
    }

    /**
     * <p>Getter for the field <code>active</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * <p>Setter for the field <code>active</code>.</p>
     *
     * @param active a {@link java.lang.Boolean} object.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * <p>displaySelectedImage.</p>
     *
     * @return a {@link java.lang.String} object.
     * @throws java.io.IOException if any.
     */
    public String displaySelectedImage() throws IOException {

        Path path = Paths.get(this.getImageLink());
        if (Files.exists(path)) {
            byte[] data = Files.readAllBytes(path);
            return Base64.encodeBytes(data);
        }
        return "";
    }

    /**
     * <p>downloadSelectedGraph.</p>
     */
    public void downloadSelectedGraph() {

        Path path = Paths.get(this.getGraphmlLink());
        try {
            byte[] data = Files.readAllBytes(path);

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("application/xml");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + FilenameUtils.getName(this.getGraphmlLink()));


            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(data);
            servletOutputStream.flush();
            servletOutputStream.close();
            context.responseComplete();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
