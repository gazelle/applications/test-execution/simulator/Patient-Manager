package net.ihe.gazelle.simulator.pam.automaton.helper;

import net.ihe.gazelle.HL7Common.messages.HL7Validator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.automaton.Manager.ContextExecutor;
import net.ihe.gazelle.simulator.pam.automaton.Manager.DataObserver;
import net.ihe.gazelle.simulator.pam.automaton.Manager.GraphExecutionStatus;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecution;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.modelImplementations.BP6Graph;
import net.ihe.gazelle.simulator.pam.automaton.report.TestStep;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import org.apache.commons.io.IOUtils;
import org.graphwalker.core.event.Observer;
import org.graphwalker.core.machine.Context;
import org.graphwalker.java.test.Executor;
import org.graphwalker.java.test.TestExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Date;

/**
 * <p>GraphWalkerExecution class.</p>
 *
 * @author aberge
 * @version 1.0: 30/10/17
 */

public class GraphWalkerExecution {

    private static final Logger LOG = LoggerFactory.getLogger(GraphWalkerExecution.class);
    private final static int TIMEOUT = 5000;

    public static void run(GraphExecutionResults graphExecutionResults, Encounter encounter) {

        EntityManager em = EntityManagerService.provideEntityManager();

        try {

            HL7V2ResponderSUTConfiguration sut = graphExecutionResults.getGraphExecution().getSut();
            boolean testConnection = testConnection(sut);
            if (testConnection) {

                GraphExecution currentGraphExecution = em.merge(graphExecutionResults.getGraphExecution());
                em.flush();

                Context context = ContextExecutor.create(BP6Graph.class, currentGraphExecution.isFullEdgeCoverage(), currentGraphExecution.getGraph
                        ().getGraphmlLink());
                Executor executor = new TestExecutor(context);

                Observer o = new DataObserver(graphExecutionResults, encounter);
                executor.getMachine().addObserver(o);
                executor.execute();


                if (graphExecutionResults.getGraphExecution().isValidateTransactionInstance()) {
                    replay(graphExecutionResults);
                } else {
                    graphExecutionResults.setExecutionStatus(GraphExecutionStatus.COMPLETED);
                }

            } else {
                graphExecutionResults.setExecutionStatus(GraphExecutionStatus.FAILED);
            }

            em.merge(graphExecutionResults);
            em.flush();

        } catch (IllegalAccessException | GraphWalkerInitialisationException e) {
            LOG.error(e.getMessage());
        }
    }

    public static void replay(GraphExecutionResults graphExecutionResults) {

        EntityManager em = EntityManagerService.provideEntityManager();

        HL7V2ResponderSUTConfiguration sut = graphExecutionResults.getGraphExecution().getSut();
        boolean testConnection = testConnection(sut);
        if (testConnection) {

            em.merge(graphExecutionResults.getGraphExecution());
            em.flush();

            for (TestStep currentTestStep : graphExecutionResults.getMessagesList()) {
                if (currentTestStep.getRequestAndResponse() != null) {
                    currentTestStep.getRequestAndResponse().setTimestamp(new Date());
                }
                HL7Validator.validateTransactionInstance(currentTestStep.getRequestAndResponse());
                currentTestStep.setAckAndErrorMessage();
            }

            graphExecutionResults.setExecutionStatus(GraphExecutionStatus.VALIDATED);

        } else {
            graphExecutionResults.setExecutionStatus(GraphExecutionStatus.FAILED);
        }

        em.merge(graphExecutionResults);
        em.flush();


    }

    private static boolean testConnection(HL7V2ResponderSUTConfiguration sutConfiguration) {
        Socket socket = new Socket();
        boolean success;
        try {
            socket.connect(new InetSocketAddress(sutConfiguration.getIpAddress(), sutConfiguration.getPort()), TIMEOUT);
            success = true;
        } catch (IOException e) {
            success = false;
        } finally {
            IOUtils.closeQuietly(socket);
        }
        return success;
    }
}
