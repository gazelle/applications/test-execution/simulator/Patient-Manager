package net.ihe.gazelle.simulator.pdqv3.pdc;

import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.messages.PRPAIN201306UV02Parser;
import net.ihe.gazelle.simulator.hl7v3.sut.action.HL7v3ResponderSUTConfigurationDAO;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDC;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PDQv3PDC class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqv3pdc")
@Scope(ScopeType.PAGE)
public class PDQv3PDC extends AbstractPDQPDC implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3989360713596354135L;
    private static final String ITI_47_KEYWORD = "ITI-47";
    private static Logger log = LoggerFactory.getLogger(PDQv3PDC.class);

    private List<TransactionInstance> messages;
    private HL7v3ResponderSUTConfiguration selectedSUT;
    private List<HL7v3ResponderSUTConfiguration> availableSystems;
    private String senderDeviceId;
    private String organizationOid;
    private PDQv3MessageBuilder messageBuilder;
    private PDQv3PDCSender sender;

    /** {@inheritDoc} */
    @Override
    protected void initializePatientCriteria() {
        super.initializePatientCriteria();
    }

    /** {@inheritDoc} */
    @Override
    public void sendMessage() {
        if (selectedSUT != null) {
            if (!limit) {
                limitValue = null;
            }
            messageBuilder = new PDQv3MessageBuilder(patientCriteria, limitValue, selectedSUT, domainsReturned);
            PRPAIN201305UV02Type request = messageBuilder.buildPatientRegistryFindCandidatesQuery();
            sender = new PDQv3PDCSender(selectedSUT);
            try {
                sender.sendPatientRegistryFindCandidatesQuery(request);
            } catch (SoapSendException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
                return;
            }
            messages = new ArrayList<TransactionInstance>();
            messages.add(sender.getTransactionInstance());
            pageNumber = 1;
            // at the end
            parserResponse();
            displayCriteriaForm = false;
        } else {
            FacesMessages.instance().add(
                    "Please, first select the system under test to which you want to send the message");
        }
    }

    private void parserResponse() {
        try {
            parseResponse(sender.getTransactionInstance().getResponse().getContent(), sender
                    .getTransactionInstance().getResponse().getType());
        } catch (NullPointerException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the supplier");
        } catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to parse the response");
            log.error(e.getMessage(), e);
        }
    }

    private void parseResponse(byte[] response, String responseType) throws JAXBException {
        if (response != null) {
            if (responseType.equals("PRPA_IN201306UV02")) {
                PRPAIN201306UV02Parser parser = new PRPAIN201306UV02Parser(FacesMessages.instance());
                List<Patient> patients = parser.extractPatients(response);
                messageBuilder.setTargetMessageId(parser.getMessageId());
                listReceivedPatients(parser, patients);
            } else if (responseType.equals("MCCI_IN000002UV01")) {
                PRPAIN201306UV02Parser parser = new PRPAIN201306UV02Parser(FacesMessages.instance());
                cancellation = parser.isCancelAccept(response);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Received " + responseType + ": cannot be parsed");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the supplier");
        }
    }


    /** {@inheritDoc} */
    @Override
    public void getNextResults() {
        messageBuilder.setLimitValue(limitValue);
        QUQIIN000003UV01Type query = messageBuilder.buildGeneralQueryActivateQueryContinue();
        try {
            sender.sendGeneralQueryActivateQueryContinue(query);
        } catch (SoapSendException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
            return;
        }
        messages.add(sender.getTransactionInstance());
        nextPage();
        // at the end
        parserResponse();
        displayCriteriaForm = false;
    }

    /** {@inheritDoc} */
    @Override
    public void cancelQuery() {
        receivedPatients = null;
        moreResultsAvailable = false;
        messageBuilder.setLimitValue(0);
        QUQIIN000003UV01CancelType query = messageBuilder.buildGeneralQueryActivateQueryCancel();
        try {
            sender.sendGeneralQueryActiveQueryCancel(query);
        } catch (SoapSendException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
            return;
        }
        messages.add(sender.getTransactionInstance());
        try {
            parseResponse(sender.getTransactionInstance().getResponse().getContent(), sender.getTransactionInstance()
                    .getResponse().getType());
        } catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to parse the response: " + e.getMessage());
            log.error(e.getMessage());
        }
        displayCriteriaForm = false;
    }

    /**
     * <p>initializeRequest.</p>
     */
    @Create
    public void initializeRequest() {
        super.initPage();
        if (selectedTransaction == null) {
            selectedTransaction = Transaction.GetTransactionByKeyword(ITI_47_KEYWORD);
        }
        if (availableSystems == null || availableSystems.isEmpty()) {
            availableSystems = HL7v3ResponderSUTConfigurationDAO.getSUTForTransactionDomain(ITI_47_KEYWORD, null);
        }
        initializePatientCriteria();
        currentDomainReturned = new AssigningAuthority();
        messages = null;
        receivedPatients = null;
        displayCriteriaForm = true;
        cancellation = null;
    }


    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7v3ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>Getter for the field <code>messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getMessages() {
        return messages;
    }

    /**
     * <p>Getter for the field <code>selectedSUT</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public HL7v3ResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     * <p>Setter for the field <code>selectedSUT</code>.</p>
     *
     * @param selectedSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public void setSelectedSUT(HL7v3ResponderSUTConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    /**
     * <p>Getter for the field <code>senderDeviceId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSenderDeviceId() {
        if (senderDeviceId == null) {
            senderDeviceId = PreferenceService.getString("hl7v3_pdq_pdc_device_id");
        }
        return senderDeviceId;
    }

    /**
     * <p>Getter for the field <code>organizationOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrganizationOid() {
        if (organizationOid == null) {
            organizationOid = PreferenceService.getString("hl7v3_organization_oid");
        }
        return organizationOid;
    }
}
