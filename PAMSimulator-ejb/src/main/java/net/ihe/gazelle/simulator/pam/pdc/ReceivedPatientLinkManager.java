package net.ihe.gazelle.simulator.pam.pdc;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientLink;
import net.ihe.gazelle.simulator.pam.model.PatientLinkQuery;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>ReceivedPatientLinkManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("receivedPatientLinkManager")
@Scope(ScopeType.PAGE)
public class ReceivedPatientLinkManager implements Serializable, QueryModifier<PatientLink> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Filter<PatientLink> filter;
    private String searchedIdentifier = null;

    /**
     * <p>Getter for the field <code>searchedIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSearchedIdentifier() {
        return searchedIdentifier;
    }

    /**
     * <p>Setter for the field <code>searchedIdentifier</code>.</p>
     *
     * @param searchedIdentifier a {@link java.lang.String} object.
     */
    public void setSearchedIdentifier(String searchedIdentifier) {
        this.searchedIdentifier = searchedIdentifier;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<PatientLink> getFilter() {
        if (filter == null) {
            filter = new Filter<PatientLink>(getCriteriaForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<PatientLink> getCriteriaForFilter() {
        PatientLinkQuery query = new PatientLinkQuery();
        HQLCriterionsForFilter<PatientLink> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("timestamp", query.creationDate());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /**
     * <p>getPatientLinksFiltered.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<PatientLink> getPatientLinksFiltered() {
        return new FilterDataModel<PatientLink>(getFilter()) {
            @Override
            protected Object getId(PatientLink patientLink) {
                return patientLink.getId();
            }
        };
    }

    /**
     * <p>resetFilter.</p>
     */
    public void resetFilter() {
        getFilter().clear();
        searchedIdentifier = null;
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<PatientLink> hqlQueryBuilder, Map<String, Object> map) {
        PatientLinkQuery query = new PatientLinkQuery();

        if ((searchedIdentifier != null) && !searchedIdentifier.isEmpty()) {
         hqlQueryBuilder.addRestriction(HQLRestrictions.or(
                 query.patientIdentifierOne().fullPatientId().likeRestriction(searchedIdentifier, HQLRestrictionLikeMatchMode.ANYWHERE),
                 query.patientIdentifierTwo().fullPatientId().likeRestriction(searchedIdentifier, HQLRestrictionLikeMatchMode.ANYWHERE)));
        }

    }
}
