package net.ihe.gazelle.simulator.utils;

import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferencesQuery;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerPages;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.List;

/**
 * <p>SUTPreferencesProvider class.</p>
 *
 * @author abe
 * @version 1.0: 14/12/17
 */

@Name("sutPreferencesProvider")
@Scope(ScopeType.STATELESS)
public class SUTPreferencesProvider implements Serializable {

    public boolean hasPreferredAssigningAuthorities(SystemConfiguration sut) {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        query.systemUnderTest().id().eq(sut.getId());
        query.assigningAuthorities().isNotEmpty();
        return query.getCount() > 0;
    }

    public List<HierarchicDesignator> getAssigningAuthoritiesForSystem(SystemConfiguration sut) {
        if (sut != null) {
            return SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(sut);
        } else {
            return null;
        }
    }

    public boolean hasPreferences(SystemConfiguration sut) {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        query.systemUnderTest().id().eq(sut.getId());
        return (query.getUniqueResult() != null);
    }

    public String getEditionLink(SystemConfiguration sut) {
        SystemUnderTestPreferences sutPref;
        if (!hasPreferences(sut)) {
            sutPref = SystemUnderTestPreferencesDAO.createPreferencesForSUT(sut);
        } else {
            sutPref = SystemUnderTestPreferencesDAO.getPreferencesForSUT(sut);
        }
        return PatientManagerPages.CAT_EDIT_SUT_PREF.getLink() + "?sut=" + sutPref.getId();
    }

}
