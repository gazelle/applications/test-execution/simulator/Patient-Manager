package net.ihe.gazelle.simulator.pixv3.consumer;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PIXV3ConsumerGUIManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixv3consumerGuiManager")
@Scope(ScopeType.PAGE)
public class PIXV3ConsumerGUIManager extends AbstractPDQPDSGuiManager {

    /**
     *
     */
    private static final long serialVersionUID = -2678951111877383278L;
    private String endpointUrl;
    private String deviceId;
    private String organizationOid;

    /**
     * <p>getUrlForHL7Messages.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrlForHL7Messages() {
        Transaction iti46 = Transaction.GetTransactionByKeyword("ITI-46");
        return "/messages/browser.seam?simulatedActor=" + getPIXV3ConsumerActor().getId() + "&transaction=" + iti46.getId();
    }

    private Actor getPIXV3ConsumerActor() {
        return Actor.findActorWithKeyword("PAT_IDENTITY_CONSUMER");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Create
    public void getSimulatorResponderConfiguration() {
        this.endpointUrl = PreferenceService.getString("pixv3_cons_url");
        this.deviceId = PreferenceService.getString("hl7v3_pix_consumer_device_id");
        this.organizationOid = PreferenceService.getString("hl7v3_pix_organization_oid");
    }

    /**
     * <p>Getter for the field <code>endpointUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEndpointUrl() {
        return endpointUrl;
    }

    /**
     * <p>Getter for the field <code>deviceId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * <p>Getter for the field <code>organizationOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrganizationOid() {
        return organizationOid;
    }

    @Override
    public String linkToPatients() {
        return "/patient/allPatients.seam?actor=" + getPIXV3ConsumerActor().getId();
    }

}
