package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.hl7v3.datatypes.AD;
import net.ihe.gazelle.hl7v3.datatypes.BL;
import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02MFMIMT700701UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientPatientPerson;
import net.ihe.gazelle.hl7v3.prpamt201303UV02.PRPAMT201303UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201303UV02.PRPAMT201303UV02Person;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.EntityClass;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.hl7v3.voc.ParticipationTargetSubject;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.hl7v3.initiator.MCCIMT000100UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.initiator.MFMIMT700701UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.initiator.RevisePatientMessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3Constants;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.hl7v3.responder.COCTMT150003UV03PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by aberge on 17/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ITI44MessageBuilder extends RevisePatientMessageBuilder {

    private static final Logger log = Logger.getLogger(ITI44MessageBuilder.class);
    private static final String SUBJECT_TYPE_CODE = "SUBJ";
    private static final String PATIENT_CLASS_CODE = "PAT";
    private List<HierarchicDesignator> restrictedDomains;

    private Patient obsoletePatient;

    /**
     * <p>Constructor for ITI44MessageBuilder.</p>
     *
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     * @param active a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param obsolete a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public ITI44MessageBuilder(HL7v3ResponderSUTConfiguration inSUT, Patient active, Patient obsolete) {
        super(inSUT, active);
        this.obsoletePatient = obsolete;
        restrictedDomains = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(inSUT);
    }

    /**
     * <p>buildAddPatientRecordMessage.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type} object.
     */
    public PRPAIN201301UV02Type buildAddPatientRecordMessage() {
        PRPAIN201301UV02Type request = new PRPAIN201301UV02Type();
        request.setITSVersion(VERSION);
        PRPAIN201301UV02MFMIMT700701UV01ControlActProcess controlActProcess = new PRPAIN201301UV02MFMIMT700701UV01ControlActProcess();
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201301UV02", INTERACTION_ID_NAMESPACE, null));

        generator = MessageIdGenerator.getGeneratorForActor(getActorKeyword());
        if (generator != null) {
            request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
            request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid()));
        } else {
            log.error("No MessageIdGenerator instance found for actor PAT_IDENTITY_SRC");
            request.setSender(MCCIMT000100UV01PartBuilder
                    .buildSender((PreferenceService.getString(getOIDPreferenceName()))));
        }
        request.setCreationTime(new TS(sdfDateTime.format(new Date())));
        request.setInteractionId(new II(INTERACTION_ID_NAMESPACE, "PRPA_IN201301UV02"));
        request.setProcessingCode(new CS("T", null, null));
        request.setProcessingModeCode(new CS("T", null, null));
        request.setAcceptAckCode(new CS("AL", null, null));
        request.addReceiver(MCCIMT000100UV01PartBuilder.buildReceiver(sut));
        populateControlActProcess(controlActProcess);
        request.setControlActProcess(controlActProcess);
        return request;
    }

    private void populateControlActProcess(PRPAIN201301UV02MFMIMT700701UV01ControlActProcess controlActProcess) {
        PRPAIN201301UV02MFMIMT700701UV01Subject1 subject1 = new PRPAIN201301UV02MFMIMT700701UV01Subject1();
        subject1.setTypeCode(SUBJECT_TYPE_CODE);
        subject1.setContextConductionInd(false);
        subject1.setRegistrationEvent(populateRegistrationEventForAddPatientRecord());
        controlActProcess.addSubject(subject1);
    }

    private PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent populateRegistrationEventForAddPatientRecord() {
        PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent event = new PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent();
        event.setClassCode(ActClass.REG);
        event.setMoodCode(ActMood.EVN);
        event.setStatusCode(new CS("active", null, null));
        event.setSubject1(populateSubject1ForAddPatientRecord());
        event.setCustodian(MFMIMT700701UV01PartBuilder.populateCustodian());

        return event;
    }

    private PRPAIN201301UV02MFMIMT700701UV01Subject2 populateSubject1ForAddPatientRecord() {
        PRPAIN201301UV02MFMIMT700701UV01Subject2 subject2 = new PRPAIN201301UV02MFMIMT700701UV01Subject2();
        subject2.setTypeCode(ParticipationTargetSubject.SBJ);
        subject2.setPatient(populatePatientForAddPatientRecord());
        return subject2;
    }

    private PRPAMT201301UV02Patient populatePatientForAddPatientRecord() {
        PRPAMT201301UV02Patient patient = new PRPAMT201301UV02Patient();
        patient.setStatusCode(new CS("active", null, null));
        patient.setClassCode(PATIENT_CLASS_CODE);
        patient.setProviderOrganization(
                COCTMT150003UV03PartBuilder.populateProviderOrganization("http://gazelle.ihe.net", false));
        addIdentifiersForAddRecord(patient);
        patient.setPatientPerson(populatePatientPersonForAddPatientRecord());
        return patient;
    }

    private void addIdentifiersForAddRecord(PRPAMT201301UV02Patient patient) {
        for (PatientIdentifier pid : PatientIdentifierDAO.getPatientIdentifiersForRestrictedDomains(activePatient, restrictedDomains)) {
            patient.addId(new II(pid.getDomain().getUniversalID(), pid.getIdNumber(), pid.getDomain().getNamespaceID()));
            II ii = new II(pid.getDomain().getUniversalID(), null);
            if (!patient.getProviderOrganization().getId().contains(ii)) {
                patient.getProviderOrganization().addId(ii);
            }
        }
    }

    private PRPAMT201301UV02Person populatePatientPersonForAddPatientRecord() {
        PRPAMT201301UV02Person person = new PRPAMT201301UV02Person();
        person.setClassCode(EntityClass.PSN);
        person.setDeterminerCode(EntityDeterminer.INSTANCE);
        // In EPD case, the mother is stored as a personal relationship
        if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
            String motherMaidenName = activePatient.getMotherMaidenName();
            if (motherMaidenName != null && !motherMaidenName.isEmpty()) {
                person.addPersonalRelationship(builPersonRelationshipForMothersMaidenNameInAddRecord(motherMaidenName));
            }
        }
        if (!activePatient.getFirstName().isEmpty() || !activePatient.getLastName().isEmpty()) {
            person.addName(buildPersonName(activePatient, null));
        }
        if (activePatient.getAlternateFirstName() != null || activePatient.getAlternateLastName() != null) {
            person.addName(buildAlternatePersonName(activePatient));
        }
        if (activePatient.getGenderCode() != null && !activePatient.getGenderCode().isEmpty()) {
            person.setAdministrativeGenderCode(populateCEFromSVS(activePatient.getGenderCode(), "HL70001"));
        }
        if (activePatient.getDateOfBirth() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            person.setBirthTime(new TS(sdf.format(activePatient.getDateOfBirth())));
        }
        AD addr = populateAddr(activePatient.getAddressLine(), activePatient.getCity(), activePatient.getState(),
                activePatient.getCountryCode(), activePatient.getZipCode());
        if (addr != null) {
            person.addAddr(addr);
        }
        for (PatientPhoneNumber phoneNumber : activePatient.getPhoneNumbers()) {
            if (phoneNumber.getValue() != null && !phoneNumber.getValue().isEmpty()) {
                person.addTelecom(populateTelecom(phoneNumber));
            }
        }
        if (activePatient.getMultipleBirthIndicator() != null) {
            person.setMultipleBirthInd(new BL(activePatient.getMultipleBirthIndicator()));
            if (activePatient.getBirthOrder() != null) {
                person.setMultipleBirthOrderNumber(new INT(activePatient.getBirthOrder()));
            }
        }
        for (Person relative : activePatient.getPersonalRelationships()) {
            person.addPersonalRelationship(builderPersonRelationshipForAddRecordMessage(relative));
        }
        return person;
    }

    /** {@inheritDoc} */
    @Override
    protected PRPAMT201302UV02PatientPatientPerson populatePatientPersonForRevisePatientRecord() {
        PRPAMT201302UV02PatientPatientPerson person = new PRPAMT201302UV02PatientPatientPerson();
        person.setClassCode(EntityClass.PSN);
        person.setDeterminerCode(EntityDeterminer.INSTANCE);
        // In EPD case, the mother is stored as a personal relationship
        if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
            if (activePatient.getMotherMaidenName() != null && !activePatient.getMotherMaidenName().isEmpty()) {
                person.addPersonalRelationship(builPersonRelationshipForMothersMaidenNameInRevise(activePatient.getMotherMaidenName()));
            }
        }
        if (!activePatient.getFirstName().isEmpty() || !activePatient.getLastName().isEmpty()) {
            person.addName(buildPersonName(activePatient, null));
        }
        if (activePatient.getAlternateFirstName() != null || activePatient.getAlternateLastName() != null) {
            person.addName(buildAlternatePersonName(activePatient));
        }
        if (activePatient.getGenderCode() != null && !activePatient.getGenderCode().isEmpty()) {
            person.setAdministrativeGenderCode(populateCEFromSVS(activePatient.getGenderCode(), "HL70001"));
        }
        if (activePatient.getDateOfBirth() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            person.setBirthTime(new TS(sdf.format(activePatient.getDateOfBirth())));
        }
        AD addr = populateAddr(activePatient.getAddressLine(), activePatient.getCity(), activePatient.getState(),
                activePatient.getCountryCode(), activePatient.getZipCode());
        if (addr != null) {
            person.addAddr(addr);
        }
        for (PatientPhoneNumber phoneNumber : activePatient.getPhoneNumbers()) {
            if (phoneNumber.getValue() != null && !phoneNumber.getValue().isEmpty()) {
                person.addTelecom(populateTelecom(phoneNumber));
            }
        }
        if (activePatient.getMultipleBirthIndicator() != null) {
            person.setMultipleBirthInd(new BL(activePatient.getMultipleBirthIndicator()));
            if (activePatient.getBirthOrder() != null) {
                person.setMultipleBirthOrderNumber(new INT(activePatient.getBirthOrder()));
            }
        }
        // CH-PIX-003, CH-PIX-004
        if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
            String religionCode = activePatient.getReligionCode();
            if (religionCode != null && !religionCode.isEmpty()) {
                person.setReligiousAffiliationCode(populateCEFromSVS(religionCode, "RELIGION"));
            }
            String raceCode = activePatient.getRaceCode();
            if (raceCode != null && !raceCode.isEmpty()) {
                person.addRaceCode(populateCEFromSVS(raceCode, "RACE"));
            }
        }
        for (Person relative : activePatient.getPersonalRelationships()) {
            person.addPersonalRelationship(buildPersonRelationshipForReviseMessage(relative));
        }
        return person;
    }

    /** {@inheritDoc} */
    @Override
    protected void addPatientIdToList(PRPAMT201302UV02Patient patient, PatientIdentifier pid) {
        patient.addId(new PRPAMT201302UV02PatientId(pid.getDomain().getUniversalID(), pid.getIdNumber(), pid.getDomain().getNamespaceID()));
        II ii = new II(pid.getDomain().getUniversalID(), null);
        if (!patient.getProviderOrganization().getId().contains(ii)) {
            patient.getProviderOrganization().addId(ii);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected String getActorKeyword() {
        return "PAT_IDENTITY_SRC";
    }

    /** {@inheritDoc} */
    @Override
    protected String getOIDPreferenceName() {
        return "hl7v3_pix_source_device_id";
    }

    /**
     * <p>buildPatientIdentityMergeMessage.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type} object.
     */
    public PRPAIN201304UV02Type buildPatientIdentityMergeMessage() {
        PRPAIN201304UV02Type request = new PRPAIN201304UV02Type();
        request.setITSVersion(VERSION);
        PRPAIN201304UV02MFMIMT700701UV01ControlActProcess controlActProcess = new PRPAIN201304UV02MFMIMT700701UV01ControlActProcess();
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201304UV02", INTERACTION_ID_NAMESPACE, null));

        generator = MessageIdGenerator.getGeneratorForActor("PAT_IDENTITY_SRC");
        if (generator != null) {
            request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
            request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid()));
        } else {
            log.error("No MessageIdGenerator instance found for actor PAT_IDENTITY_SRC");
            request.setSender(MCCIMT000100UV01PartBuilder
                    .buildSender((PreferenceService.getString("hl7v3_pix_source_device_id"))));
        }
        request.setCreationTime(new TS(sdfDateTime.format(new Date())));
        request.setInteractionId(new II(INTERACTION_ID_NAMESPACE, "PRPA_IN201304UV02"));
        request.setProcessingCode(new CS("T", null, null));
        request.setProcessingModeCode(new CS("T", null, null));
        request.setAcceptAckCode(new CS("AL", null, null));
        request.addReceiver(MCCIMT000100UV01PartBuilder.buildReceiver(sut));
        populateControlActProcess(controlActProcess);
        request.setControlActProcess(controlActProcess);
        return request;
    }

    private void populateControlActProcess(PRPAIN201304UV02MFMIMT700701UV01ControlActProcess controlActProcess) {
        PRPAIN201304UV02MFMIMT700701UV01Subject1 subject1 = new PRPAIN201304UV02MFMIMT700701UV01Subject1();
        subject1.setTypeCode(SUBJECT_TYPE_CODE);
        subject1.setContextConductionInd(false);
        subject1.setRegistrationEvent(populateRegistrationEventForPatientIdentityMerge());
        controlActProcess.addSubject(subject1);
    }

    private PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent populateRegistrationEventForPatientIdentityMerge() {
        PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent event = new PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent();
        event.setClassCode(ActClass.REG);
        event.setMoodCode(ActMood.EVN);
        event.setStatusCode(new CS("active", null, null));
        event.setSubject1(populateSubject1ForPatientIdentityMerge());
        event.setCustodian(MFMIMT700701UV01PartBuilder.populateCustodian());
        event.addReplacementOf(MFMIMT700701UV01PartBuilder.populateReplaceOf(obsoletePatient));
        return event;
    }


    private PRPAIN201304UV02MFMIMT700701UV01Subject2 populateSubject1ForPatientIdentityMerge() {
        PRPAIN201304UV02MFMIMT700701UV01Subject2 subject2 = new PRPAIN201304UV02MFMIMT700701UV01Subject2();
        subject2.setTypeCode(ParticipationTargetSubject.SBJ);
        subject2.setPatient(populatePatientForPatientIdentityMerge());
        return subject2;
    }

    private PRPAMT201303UV02Patient populatePatientForPatientIdentityMerge() {
        PRPAMT201303UV02Patient patient = new PRPAMT201303UV02Patient();
        patient.setStatusCode(new CS("active", null, null));
        patient.setClassCode(PATIENT_CLASS_CODE);
        patient.setProviderOrganization(
                COCTMT150003UV03PartBuilder.populateProviderOrganization("http://gazelle.ihe.net", false));
        addIdentifiersForIdentityMerge(patient);
        patient.setPatientPerson(populatePatientPersonForPatientIdentityFeed());
        return patient;
    }

    private void addIdentifiersForIdentityMerge(PRPAMT201303UV02Patient patient) {
        for (PatientIdentifier pid : PatientIdentifierDAO.getPatientIdentifiers(activePatient)) {
            patient.addId(new II(pid.getDomain().getUniversalID(), pid.getIdNumber(), pid.getDomain().getNamespaceID()));
            II ii = new II(pid.getDomain().getUniversalID(), null);
            if (!patient.getProviderOrganization().getId().contains(ii)) {
                patient.getProviderOrganization().addId(ii);
            }
        }
    }

    private PRPAMT201303UV02Person populatePatientPersonForPatientIdentityFeed() {
        PRPAMT201303UV02Person person = new PRPAMT201303UV02Person();
        person.setClassCode(EntityClass.PSN);
        person.setDeterminerCode(EntityDeterminer.INSTANCE);
        if (!activePatient.getFirstName().isEmpty() || !activePatient.getLastName().isEmpty()) {
            person.addName(buildPersonName(activePatient, null));
        }
        return person;
    }
}
