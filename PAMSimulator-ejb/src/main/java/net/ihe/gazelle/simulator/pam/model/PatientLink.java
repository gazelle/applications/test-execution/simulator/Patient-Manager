package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>PatientLink</b> This class is used as a join table to link/unlink patients.
 * 
 * @author Anne-Gaëlle Bergé / INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - 2011, July 6th
 * 
 *          This class possesses the following attributes:
 *          <ul>
 *          <li><b>Id</b> unique identifier in the database</li>
 *          <li><b>patientIdentifierOne</b> one of the two patient identifiers to be linked</li>
 *          <li><b>patientIdentifierTwo</b> the second patient identifier to link
 *          <li><b>simulatedActor</b> for which simulated actor the link is done</li>
 *          </ul>
 */

@Entity
@Name("patientLink")
@Table(name = "pam_patient_link", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_patient_link_sequence", sequenceName = "pam_patient_link_id_seq", allocationSize = 1)
public class PatientLink implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pam_patient_link_sequence")
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "patient_identifier_one_id")
	private PatientIdentifier patientIdentifierOne;

	@ManyToOne
	@JoinColumn(name = "patient_identifier_two_id")
	private PatientIdentifier patientIdentifierTwo;

	@ManyToOne
	@JoinColumn(name = "simulated_actor_id")
	private Actor simulatedActor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Date creationDate;

	public PatientLink() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPatientIdentifierOne(PatientIdentifier patientIdentifierOne) {
		this.patientIdentifierOne = patientIdentifierOne;
	}

	public PatientIdentifier getPatientIdentifierOne() {
		return patientIdentifierOne;
	}

	public PatientIdentifier getPatientIdentifierTwo() {
		return patientIdentifierTwo;
	}

	public void setPatientIdentifierTwo(PatientIdentifier patientIdentifierTwo) {
		this.patientIdentifierTwo = patientIdentifierTwo;
	}

	public void setCreationDate(Date creationDate) {
		if (creationDate != null) {
			this.creationDate = (Date) creationDate.clone();
		} else {
			this.creationDate = null;
		}
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	public static void linkPatientIdentifiers(List<PatientIdentifier> firstList, List<PatientIdentifier> secondList,
			Actor simulatedActor) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if ((firstList != null) && (secondList != null)) {
			for (PatientIdentifier id1 : firstList) {
				for (PatientIdentifier id2 : secondList) {
					PatientLink link = getLink(id1, id2, simulatedActor, entityManager);
					if (link == null) {
						link = new PatientLink();
						link.setPatientIdentifierOne(id1);
						link.setPatientIdentifierTwo(id2);
						link.setSimulatedActor(simulatedActor);
						link.setCreationDate(new Date());
						entityManager.merge(link);
					}
				}
			}
			entityManager.flush();
		}
	}

	public static void unlinkPatientIdentifiers(List<PatientIdentifier> firstList, List<PatientIdentifier> secondList,
			Actor simulatedActor) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if ((firstList != null) && (secondList != null)) {
			for (PatientIdentifier id1 : firstList) {
				for (PatientIdentifier id2 : secondList) {
					PatientLink link = getLink(id1, id2, simulatedActor, entityManager);
					if (link != null) {
						entityManager.remove(link);
					}
				}
			}
			entityManager.flush();
		}
	}

	public static void linkPatients(Patient patientOne, Patient patientTwo, Actor simulatedActor) {
		if ((patientOne != null) && (patientOne.getPatientIdentifiers() != null) && (patientTwo != null)
				&& (patientTwo.getPatientIdentifiers() != null)) {
			linkPatientIdentifiers(patientOne.getPatientIdentifiers(), patientTwo.getPatientIdentifiers(),
					simulatedActor);
		}
	}

	public static void unlinkPatients(Patient patientOne, Patient patientTwo, Actor simulatedActor) {
		if ((patientOne != null) && (patientOne.getPatientIdentifiers() != null) && (patientTwo != null)
				&& (patientTwo.getPatientIdentifiers() != null)) {
			unlinkPatientIdentifiers(patientOne.getPatientIdentifiers(), patientTwo.getPatientIdentifiers(),
					simulatedActor);
		}
	}

	private static PatientLink getLink(PatientIdentifier firstIdentifier, PatientIdentifier secondIdentifier,
			Actor simulatedActor, EntityManager entityManager) {
        PatientLinkQuery query = new PatientLinkQuery(entityManager);
        query.simulatedActor().eq(simulatedActor);
        query.addRestriction(HQLRestrictions.or(query.patientIdentifierOne().eqRestriction(firstIdentifier),
                query.patientIdentifierTwo().eqRestriction(secondIdentifier)));
		return query.getUniqueResult();
	}

	/**
	 * Returns the list of all PatientLink in which the given patientIdentifier is involved
	 * 
	 * @param searchedPid
	 * @param simulatedActor
	 * @return the list of PatientLink for the given patient identifier
	 */
	public static List<PatientLink> getLinksForPid(PatientIdentifier searchedPid, Actor simulatedActor) {
        PatientLinkQuery query = new PatientLinkQuery();
        query.simulatedActor().eq(simulatedActor);
        query.addRestriction(HQLRestrictions.or(query.patientIdentifierOne().eqRestriction(searchedPid),
                query.patientIdentifierTwo().eqRestriction(searchedPid)));
        return query.getListNullIfEmpty();
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientLink)) return false;

        PatientLink that = (PatientLink) o;

        if (patientIdentifierOne != null ? !patientIdentifierOne.equals(that.patientIdentifierOne) : that.patientIdentifierOne != null)
            return false;
        if (patientIdentifierTwo != null ? !patientIdentifierTwo.equals(that.patientIdentifierTwo) : that.patientIdentifierTwo != null)
            return false;
        if (simulatedActor != null ? !simulatedActor.equals(that.simulatedActor) : that.simulatedActor != null)
            return false;
        return !(creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null);

    }

    @Override
    public int hashCode() {
        int result = patientIdentifierOne != null ? patientIdentifierOne.hashCode() : 0;
        result = 31 * result + (patientIdentifierTwo != null ? patientIdentifierTwo.hashCode() : 0);
        result = 31 * result + (simulatedActor != null ? simulatedActor.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }
}
