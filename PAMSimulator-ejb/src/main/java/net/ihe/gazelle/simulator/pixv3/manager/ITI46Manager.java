package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.sut.action.HL7v3ResponderSUTConfigurationDAO;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;
import java.util.List;

@Stateful
@Name("iti46Manager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ITI46ManagerLocal")
public class ITI46Manager implements Serializable, ITI46ManagerLocal {

	/**
	 * 
	 */

	private static final long serialVersionUID = -6844674348612437538L;
	private HL7v3ResponderSUTConfiguration selectedSystem;
	private List<HL7v3ResponderSUTConfiguration> availableSystems;
	private String senderDeviceId;
	private String organizationOid;

	@Create
	public void init() {
		refreshListOfSystems();
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {

	}

	public void refreshListOfSystems() {
		String domain = PreferenceService.getString("default_pdq_domain");
		if (domain == null || domain.equals("ITI")){
			domain = "IHE";
		}
		availableSystems= HL7v3ResponderSUTConfigurationDAO.getSUTForTransactionDomain("ITI-46", domain);
	}

	public List<HL7v3ResponderSUTConfiguration> getAvailableSystems() {
		return availableSystems;
	}

	public void setAvailableSystems(List<HL7v3ResponderSUTConfiguration> availableSystems) {
		this.availableSystems = availableSystems;
	}

	public HL7v3ResponderSUTConfiguration getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(HL7v3ResponderSUTConfiguration selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public String getSenderDeviceId() {
		if (senderDeviceId == null) {
			senderDeviceId = PreferenceService.getString("hl7v3_pix_mgr_device_id");
		}
		return senderDeviceId;
	}

	public String getOrganizationOid() {
		if (organizationOid == null) {
			organizationOid = PreferenceService.getString("hl7v3_pix_organization_oid");
		}
		return organizationOid;
	}
}
