package net.ihe.gazelle.simulator.pam.automaton.modelImplementations;

import org.graphwalker.java.annotation.Model;
import org.graphwalker.java.annotation.Vertex;
import org.graphwalker.java.annotation.Edge;

/**
 * <p>Ibp6gwTest interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Model(file = "bp6gw.graphml")
public interface Ibp6gwTest {

    /**
     * <p>e_send_A03.</p>
     */
    @Edge()
    void e_send_A03();

    /**
     * <p>e_send_A01.</p>
     */
    @Edge()
    void e_send_A01();

    /**
     * <p>v_Consultant_Externe.</p>
     */
    @Vertex()
    void v_Consultant_Externe();

    /**
     * <p>e_send_A07.</p>
     */
    @Edge()
    void e_send_A07();

    /**
     * <p>e_send_A06.</p>
     */
    @Edge()
    void e_send_A06();

    /**
     * <p>e_send_A53.</p>
     */
    @Edge()
    void e_send_A53();

    /**
     * <p>e_send_A05.</p>
     */
    @Edge()
    void e_send_A05();

    /**
     * <p>e_send_A22.</p>
     */
    @Edge()
    void e_send_A22();

    /**
     * <p>e_send_A52.</p>
     */
    @Edge()
    void e_send_A52();

    /**
     * <p>e_send_A07_classe_O.</p>
     */
    @Edge()
    void e_send_A07_classe_O();

    /**
     * <p>e_send_A04.</p>
     */
    @Edge()
    void e_send_A04();

    /**
     * <p>e_send_A21.</p>
     */
    @Edge()
    void e_send_A21();

    /**
     * <p>e_send_A04_classe_O.</p>
     */
    @Edge()
    void e_send_A04_classe_O();

    /**
     * <p>e_send_Z99.</p>
     */
    @Edge()
    void e_send_Z99();

    /**
     * <p>v_Absence_Temporaire.</p>
     */
    @Vertex()
    void v_Absence_Temporaire();

    /**
     * <p>e_send_A11_pas_pre_admis.</p>
     */
    @Edge()
    void e_send_A11_pas_pre_admis();

    /**
     * <p>v_Pas_de_venue_courante.</p>
     */
    @Vertex()
    void v_Pas_de_venue_courante();

    /**
     * <p>e_initialize.</p>
     */
    @Edge()
    void e_initialize();

    /**
     * <p>e_send_A07_classe_E.</p>
     */
    @Edge()
    void e_send_A07_classe_E();

    /**
     * <p>v_Pre_Admis_Consultation_Externe.</p>
     */
    @Vertex()
    void v_Pre_Admis_Consultation_Externe();

    /**
     * <p>v_Pre_Admis_Sceance.</p>
     */
    @Vertex()
    void v_Pre_Admis_Sceance();

    /**
     * <p>e_send_A38.</p>
     */
    @Edge()
    void e_send_A38();

    /**
     * <p>v_Pre_Admis_Hospitalisation.</p>
     */
    @Vertex()
    void v_Pre_Admis_Hospitalisation();

    /**
     * <p>e_send_A06_classe_I_R.</p>
     */
    @Edge()
    void e_send_A06_classe_I_R();

    /**
     * <p>e_send_A11_pre_admis_ext.</p>
     */
    @Edge()
    void e_send_A11_pre_admis_ext();

    /**
     * <p>e_send_A11.</p>
     */
    @Edge()
    void e_send_A11();

    /**
     * <p>v_Consultant_Urgences.</p>
     */
    @Vertex()
    void v_Consultant_Urgences();

    /**
     * <p>e_send_A13.</p>
     */
    @Edge()
    void e_send_A13();

    /**
     * <p>v_Hospitalise.</p>
     */
    @Vertex()
    void v_Hospitalise();
}
