package net.ihe.gazelle.simulator.application;

/**
 * Interface for Application Preferences DAO.
 */
public interface PreferenceServiceDAO {

    /**
     * Retrieve the value of the Preference
     *
     * @param preferenceName The name of the preference
     * @return The value of the preference
     */
    String retrievePreferenceServiceValue(String preferenceName);
}
