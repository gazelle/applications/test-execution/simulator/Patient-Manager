package net.ihe.gazelle.simulator.pdq.hl7;

import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.QIP;
import ca.uhn.hl7v2.model.v25.message.QCN_J01;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.model.MovementQuery;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>QBPMessageDecoder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class QBPMessageDecoder {

    private QBP_Q21 queryMessage;
    private static Logger log = LoggerFactory.getLogger(QBPMessageDecoder.class);
    private List<HierarchicDesignator> domainsToReturn;

    /**
     * <p>Constructor for QBPMessageDecoder.</p>
     *
     * @param incomingMessage a {@link net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21} object.
     */
    public QBPMessageDecoder(QBP_Q21 incomingMessage) {
        this.queryMessage = incomingMessage;
        this.domainsToReturn = new ArrayList<HierarchicDesignator>();
    }

    /**
     * <p>generateContinuationPointer.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String generateContinuationPointer() {
        String queryID = queryMessage.getQPD().getQueryTag().getValue();
        queryID = queryID.concat("_" + queryMessage.getMSH().getSendingApplication().getHd1_NamespaceID().getValue());
        return queryID;
    }

    /**
     * <p>getNbOfHitsToReturnFromQuery.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getNbOfHitsToReturnFromQuery() {
        try {
            return Integer.parseInt(queryMessage.getRCP().getQuantityLimitedRequest().getQuantity().getValue());
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * <p>getContinuationPointerFromMessage.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getContinuationPointerFromMessage() {
        return queryMessage.getDSC().getContinuationPointer().getValue();
    }

    /**
     * <p>getPatientQueryFromMessage.</p>
     *
     * @param queryBuilder a {@link net.ihe.gazelle.hql.HQLQueryBuilder} object.
     * @return a PatientQuery object initialised based on the content of the QBP message
     */
    public PatientQuery getPatientQueryFromMessage(HQLQueryBuilder<Patient> queryBuilder) {
        PatientQuery query = new PatientQuery(queryBuilder);
        if (!domainsToReturn.isEmpty()) {
            query.patientIdentifiers().domain().in(domainsToReturn);
        }
        // default criteria
        query.simulatedActor().keyword().in(getActorKeywords());
        query.stillActive().eq(true);
        // criteria from query
        QIP[] parameters = queryMessage.getQPD().getDemographicsFields();
        if ((parameters != null) && (parameters.length > 0)) {
            for (QIP param : parameters) {
                String paramString = valuesAsString(param);
                if (paramString != null && param.getSegmentFieldName() != null) {
                    if (param.getSegmentFieldName().getValue().equals("@PID.5.2")) {
                        query.firstName().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.5.1.1")) {
                        query.lastName().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.6.1.1")) {
                        query.motherMaidenName().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.7.1")
                            && (param.getValues().getValue() != null)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        try {
                            Date date = sdf.parse(param.getValues().getValue());
                            Calendar begin = Calendar.getInstance();
                            begin.setTime(date);
                            begin.set(Calendar.HOUR_OF_DAY, 0);
                            begin.set(Calendar.MINUTE, 0);
                            begin.set(Calendar.SECOND, 0);
                            Calendar end = Calendar.getInstance();
                            end.setTime(date);
                            end.set(Calendar.HOUR_OF_DAY, 23);
                            end.set(Calendar.MINUTE, 59);
                            end.set(Calendar.SECOND, 59);
                            query.dateOfBirth().ge(begin.getTime());
                            query.dateOfBirth().le(end.getTime());
                        } catch (Exception e) {
                            log.error(param.getValues().getValue() + " has not the expected date format yyyyMMddHHmmss");
                        }
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.8")) {
                        query.genderCode().eq(param.getValues().getValue());
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.13.9")) {
                        query.phoneNumbers().value().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.5")) {
                        query.patientIdentifiers().identifierTypeCode()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.1")) {
                        query.addressList().street().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.3")) {
                        query.addressList().city().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.4")) {
                        query.addressList().state().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.5")) {
                        query.addressList().zipCode().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.6")) {
                        query.countryCode().eq(param.getValues().getValue());
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.1")) {
                        query.patientIdentifiers().idNumber()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.1")) {
                        query.patientIdentifiers().domain().namespaceID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.2")) {
                        query.patientIdentifiers().domain().universalID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.3")) {
                        query.patientIdentifiers().domain().universalIDType()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    }// filter on patient account number
                    else if (param.getSegmentFieldName().getValue().equals("@PID.18")) {
                        query.accountNumber().like(paramString);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.1")) {
                        query.accountNumber().like(paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.1")) {
                        query.accountNumber().like("%^%^%^" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.2")) {
                        query.accountNumber().like("%^%^%^%&" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.3")) {
                        query.accountNumber().like("%^%^%^%&%&" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.13.4")) {
                        query.email().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else {
                        log.warn(param.getSegmentFieldName().getValue() + " is not taken into account in patient query !");
                    }
                }
            }
        } else {
            log.warn("No repetition of QPD-3 field");
            query = null;
        }
        return query;
    }

    /**
     * PAM-509: We want the PDS actor to look into Connectathon demographics as well
     *
     * @return the list of actor's keywords for which we want to return patients
     */
    private List<String> getActorKeywords() {
        return Arrays.asList(PatientManagerConstants.PDS, PatientManagerConstants.CONNECTATHON);
    }

    /**
     * parses the QPD-8 field and for each defined domain, check if the PDS knows it
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return the list reps which contain an unknown domain
     */
    public List<Integer> checkWhatDomainsReturned(EntityManager entityManager) {
        List<Integer> unknownDomains = new ArrayList<Integer>();
        CX[] domains = queryMessage.getQPD().getWhatDomainsReturned();
        Integer index = 0;
        if ((domains != null) && (domains.length > 0)) {
            for (CX domain : domains) {
                List<HierarchicDesignator> designators = HierarchicDesignator.getDomainFiltered(domain.getAssigningAuthority()
                                .getNamespaceID().getValue(), domain.getAssigningAuthority().getUniversalID().getValue(),
                        domain.getAssigningAuthority().getUniversalIDType().getValue(), entityManager,
                        DesignatorType.PATIENT_ID);
                for (HierarchicDesignator designator : designators) {
                    if (designator == null) {
                        unknownDomains.add(index);
                    } else {
                        domainsToReturn.add(designator);
                    }
                }
                index++;
            }
        }
        return unknownDomains;
    }

    /**
     * <p>getMovementQueryFromMessage.</p>
     *
     * @param queryBuilder a {@link net.ihe.gazelle.hql.HQLQueryBuilder} object.
     * @return a MovementQuery object initialised based on the content of the QBP message.
     */
    public MovementQuery getMovementQueryFromMessage(HQLQueryBuilder<Movement> queryBuilder) {
        MovementQuery query = new MovementQuery(queryBuilder);
        if (!domainsToReturn.isEmpty()) {
            query.encounter().patient().patientIdentifiers().domain().in(domainsToReturn);
        }
        // default criteria
        query.currentMovement().eq(true);
        query.simulatedActor().keyword().eq(PatientManagerConstants.PES);
        query.encounter().open().eq(true);
        // criteria from query
        QIP[] parameters = queryMessage.getQPD().getDemographicsFields();
        if ((parameters != null) && (parameters.length > 0)) {
            for (QIP param : parameters) {
                String paramString = valuesAsString(param);
                if (paramString != null && param.getSegmentFieldName() != null) {
                    if (param.getSegmentFieldName().getValue().equals("@PID.5.2")) {
                        query.encounter().patient().firstName()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.5.1.1")) {
                        query.encounter().patient().lastName()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.6.1.1")) {
                        query.encounter().patient().motherMaidenName()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.7.1")
                            && (param.getValues().getValue() != null)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                        try {
                            Date date = sdf.parse(param.getValues().getValue());
                            Calendar begin = Calendar.getInstance();
                            begin.setTime(date);
                            begin.set(Calendar.HOUR_OF_DAY, 0);
                            begin.set(Calendar.MINUTE, 0);
                            begin.set(Calendar.SECOND, 0);
                            Calendar end = Calendar.getInstance();
                            end.setTime(date);
                            end.set(Calendar.HOUR_OF_DAY, 23);
                            end.set(Calendar.MINUTE, 59);
                            end.set(Calendar.SECOND, 59);
                            query.encounter().patient().dateOfBirth().ge(begin.getTime());
                            query.encounter().patient().dateOfBirth().le(end.getTime());
                        } catch (Exception e) {
                            log.error(param.getValues().getValue() + " has not the expected date format yyyyMMddHHmmss");
                        }
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.8")) {
                        query.encounter().patient().genderCode().eq(param.getValues().getValue());
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.13.9")) {
                        query.encounter().patient().phoneNumbers().value()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.1")) {
                        query.encounter().patient().addressList().street().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.3")) {
                        query.encounter().patient().addressList().city().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.4")) {
                        query.encounter().patient().addressList().state().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.5")) {
                        query.encounter().patient().addressList().zipCode()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.11.6")) {
                        query.encounter().patient().countryCode().eq(param.getValues().getValue());
                    }
                    // patient identifier
                    else if (param.getSegmentFieldName().getValue().equals("@PID.3.1")) {
                        query.encounter().patient().patientIdentifiers().idNumber()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.1")) {
                        query.encounter().patient().patientIdentifiers().domain().namespaceID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.2")) {
                        query.encounter().patient().patientIdentifiers().domain().universalID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.3.4.3")) {
                        query.encounter().patient().patientIdentifiers().domain().universalIDType()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    }
                    // visit number
                    else if (param.getSegmentFieldName().getValue().equals("@PV1.19.1")) {
                        query.encounter().visitNumberId().like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.19.4.1")) {
                        query.encounter().visitDesignator().namespaceID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.19.4.2")) {
                        query.encounter().visitDesignator().universalID()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.19.4.3")) {
                        query.encounter().visitDesignator().universalIDType()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    }
                    // filter on patient account number
                    else if (param.getSegmentFieldName().getValue().equals("@PID.18")) {
                        query.encounter().patient().accountNumber().like(paramString);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.1")) {
                        query.encounter().patient().accountNumber()
                                .like(paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.1")) {
                        query.encounter().patient().accountNumber()
                                .like("%^%^%^" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.2")) {
                        query.encounter().patient().accountNumber()
                                .like("%^%^%^%&" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PID.18.4.3")) {
                        query.encounter().patient().accountNumber()
                                .like("%^%^%^%&%&" + paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.2")) {
                        query.encounter().patientClassCode().eq(param.getValues().getValue());
                    }
                    // filter on patient location
                    else if (param.getSegmentFieldName().getValue().equals("@PV1.3.1")) {
                        query.assignedPatientLocation().like(paramString, HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.3.2")) {
                        query.assignedPatientLocation().like("%^" + paramString,
                                HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.3.3")) {
                        query.assignedPatientLocation().like("%^%^" + paramString,
                                HQLRestrictionLikeMatchMode.START);
                    } else if (param.getSegmentFieldName().getValue().equals("@PV1.3.4")) {
                        query.assignedPatientLocation().like("%^%^%^" + paramString,
                                HQLRestrictionLikeMatchMode.START);
                    }
                    // filter on hospital service
                    else if (param.getSegmentFieldName().getValue().equals("@PV1.10")) {
                        query.encounter().hospitalServiceCode()
                                .like(paramString, HQLRestrictionLikeMatchMode.EXACT);
                    }
                    // filter on admitting doctor code @PV1.17.2.1
                    else if (param.getSegmentFieldName().getValue().startsWith("@PV1.17")) {
                        query.addRestriction(HQLRestrictions.or(
                                query.encounter().admittingDoctorCode().likeRestriction(paramString),
                                query.encounter().attendingDoctorCode().likeRestriction(paramString)));
                    } else if (param.getSegmentFieldName().getValue().startsWith("@PV1.8")) {
                        query.encounter().referringDoctorCode().like(paramString);
                    } else {
                        log.warn(param.getSegmentFieldName().getValue() + " is not used as query parameter !!");
                    }
                }
            }
        } else {
            log.warn("No repetition of QPD-3 field");
            query = null;
        }
        return query;
    }

    private String valuesAsString(QIP param) {
        String value = param.getValues().getValue();
        if (value != null && !value.isEmpty()) {
            value = value.replace("*", "%");
        } else {
            return null;
        }
        return value;
    }

    /**
     * <p>getPointerFromCancellationRequest.</p>
     *
     * @param request a {@link ca.uhn.hl7v2.model.v25.message.QCN_J01} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getPointerFromCancellationRequest(QCN_J01 request) {
        return request.getQID().getQueryTag().getValue();
    }

    /**
     * <p>Getter for the field <code>domainsToReturn</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getDomainsToReturn() {
        return domainsToReturn;
    }
}
