package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.coctmt030007UV.COCTMT030007UVPerson;
import net.ihe.gazelle.hl7v3.datatypes.AD;
import net.ihe.gazelle.hl7v3.datatypes.AdxpCity;
import net.ihe.gazelle.hl7v3.datatypes.AdxpCountry;
import net.ihe.gazelle.hl7v3.datatypes.AdxpPostalCode;
import net.ihe.gazelle.hl7v3.datatypes.AdxpState;
import net.ihe.gazelle.hl7v3.datatypes.AdxpStreetAddressLine;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.ENXP;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.EnGiven;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.ON;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.datatypes.ST;
import net.ihe.gazelle.hl7v3.datatypes.TEL;
import net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationshipId;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship;
import net.ihe.gazelle.hl7v3.voc.PersonalRelationshipRoleType;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.hl7v3.initiator.COCTMT030007UVPersonBuilder;
import net.ihe.gazelle.simulator.pam.dao.PersonDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * <p>HL7v3MessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7v3MessageBuilder {

    /** Constant <code>STAR="*"</code> */
    public static final String STAR = "*";
    /** Constant <code>EMPTYCHAR=""</code> */
    public static final String EMPTYCHAR = "";
    /** Constant <code>SRCH_USAGE="SRCH"</code> */
    public static final String SRCH_USAGE = "SRCH";
    private static Logger log = LoggerFactory.getLogger(HL7v3MessageBuilder.class);
    /** Constant <code>INTERACTION_ID_NAMESPACE="2.16.840.1.113883.1.18"</code> */
    protected static final String INTERACTION_ID_NAMESPACE = "2.16.840.1.113883.1.18";
    /** Constant <code>VERSION="XML_1.0"</code> */
    public static final String VERSION = "XML_1.0";
    private PRPAMT201302UV02PersonalRelationship relationship;

    /**
     * <p>buildPersonName.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     * @param lastName a {@link java.lang.String} object.
     * @param lastNameQualifier a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.PN} object.
     */
    protected static PN buildPersonName(String firstName, String lastName, String lastNameQualifier) {
        PN personName = new PN();
        boolean search = false;
        if (firstName != null && !firstName.isEmpty()) {
            EnGiven given = new EnGiven();
            search = populateENXP(given, firstName);
            personName.addGiven(given);
        }
        if (lastName != null && !lastName.isEmpty()) {
            EnFamily family = new EnFamily();
            search = search || populateENXP(family, lastName);
            if (lastNameQualifier != null && !lastNameQualifier.isEmpty()) {
                family.setQualifier(lastNameQualifier);
            }
            personName.addFamily(family);
        }
        if (search) {
            personName.setUse(SRCH_USAGE);
        }
        return personName;
    }

    /**
     * <p>populateENXP.</p>
     *
     * @param given a {@link net.ihe.gazelle.hl7v3.datatypes.ENXP} object.
     * @param value a {@link java.lang.String} object.
     * @return a boolean.
     */
    protected static boolean populateENXP(ENXP given, String value) {
        if (value != null && value.contains(STAR)) {
            given.getMixed().add(canonicalize(value));
            return true;
        } else {
            given.getMixed().add(value);
            return false;
        }
    }

    private static String canonicalize(String value) {
        return value.replace(STAR, EMPTYCHAR);
    }

    /**
     * <p>buildPersonName.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param lastNameQualifier a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.PN} object.
     */
    protected static PN buildPersonName(Patient patient, String lastNameQualifier) {
        PN patientName = buildPersonName(patient.getFirstName(), patient.getLastName(), lastNameQualifier);
        addSecondThirdNames(patientName, patient.getSecondName(), patient.getThirdName());
        return patientName;
    }

    /**
     * <p>addSecondThirdNames.</p>
     *
     * @param patientName a {@link net.ihe.gazelle.hl7v3.datatypes.PN} object.
     * @param secondName a {@link java.lang.String} object.
     * @param thirdName a {@link java.lang.String} object.
     */
    protected static void addSecondThirdNames(PN patientName, String secondName, String thirdName) {
        if (secondName != null && !secondName.isEmpty()) {
            EnGiven second = new EnGiven();
            populateENXP(second, secondName);
            patientName.addGiven(second);
        }
        if (thirdName != null && !thirdName.isEmpty()) {
            EnGiven third = new EnGiven();
            populateENXP(third, thirdName);
            patientName.addGiven(third);
        }
    }

    /**
     * <p>buildAlternatePersonName.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.PN} object.
     */
    protected static PN buildAlternatePersonName(Patient patient) {
        PN alternatePatientName = buildPersonName(patient.getAlternateFirstName(), patient.getAlternateLastName(), null);
        addSecondThirdNames(alternatePatientName, patient.getAlternateSecondName(), patient.getAlternateThirdName());
        return alternatePatientName;
    }

    /**
     * <p>buildST.</p>
     *
     * @param inString a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.ST} object.
     */
    protected static ST buildST(String inString) {
        ST text = new ST();
        text.getMixed().add(inString);
        return text;
    }

    /**
     * <p>populateTelecom.</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.TEL} object.
     */
    protected static TEL populateTelecom(String value) {
        TEL telecom = new TEL();
        telecom.setValue(value);
        return telecom;
    }

    /**
     * <p>populateTelecom.</p>
     *
     * @param phoneNumber a {@link net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.TEL} object.
     */
    protected static TEL populateTelecom(PatientPhoneNumber phoneNumber) {
        TEL telecom = new TEL();
        if (phoneNumber.getValue() != null && !phoneNumber.getValue().isEmpty()) {
            String value = "tel:" + phoneNumber.getValue();
            telecom.setValue(value);
            if (phoneNumber.getType() != null) {
                telecom.setUse(phoneNumber.getType().getHl7v3Code());
            }
        }
        return telecom;
    }

    /**
     * <p>populateAddr.</p>
     *
     * @param street a {@link java.lang.String} object.
     * @param city a {@link java.lang.String} object.
     * @param state a {@link java.lang.String} object.
     * @param country a {@link java.lang.String} object.
     * @param zipCode TODO
     * @return a null object if none of the given parameter is not null
     */
    protected static AD populateAddr(String street, String city, String state, String country, String zipCode) {
        AD addr = new AD();
        boolean notEmpty = false;
        if (street != null && !street.isEmpty()) {
            notEmpty = true;
            AdxpStreetAddressLine adxpStreet = new AdxpStreetAddressLine();
            adxpStreet.getMixed().add(street);
            addr.addStreetAddressLine(adxpStreet);
        }
        if (city != null && !city.isEmpty()) {
            notEmpty = true;
            AdxpCity adxpCity = new AdxpCity();
            adxpCity.getMixed().add(city);
            addr.addCity(adxpCity);
        }
        if (state != null && !state.isEmpty()) {
            notEmpty = true;
            AdxpState adxpState = new AdxpState();
            adxpState.getMixed().add(state);
            addr.addState(adxpState);
        }
        if (country != null && !country.isEmpty()) {
            notEmpty = true;
            AdxpCountry adxpCountry = new AdxpCountry();
            adxpCountry.getMixed().add(country);
            addr.addCountry(adxpCountry);
        }
        if (zipCode != null && !zipCode.isEmpty()) {
            notEmpty = true;
            AdxpPostalCode adxpPostalCode = new AdxpPostalCode();
            adxpPostalCode.getMixed().add(zipCode);
            addr.addPostalCode(adxpPostalCode);
        }
        if (notEmpty) {
            return addr;
        } else {
            return null;
        }
    }

    /**
     * <p>populateOn.</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.ON} object.
     */
    protected static ON populateOn(String value) {
        ON on = new ON();
        on.getMixed().add(value);
        return on;
    }

    /**
     * <p>messageToString.</p>
     *
     * @param messageClass a {@link java.lang.Class} object.
     * @param message a T object.
     * @param <T> a T object.
     * @return an array of byte.
     */
    public static <T> byte[] messageToString(Class<T> messageClass, T message) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(messageClass, baos, message);
            String requestString = new String(baos.toByteArray(), StandardCharsets.UTF_8);
            requestString = requestString.replaceFirst("UTF", "UTF-");
            return requestString.getBytes(StandardCharsets.UTF_8);
        } catch (JAXBException e) {
            log.error("Cannot convert message to string");
            return null;
        }
    }

    /**
     * <p>populateCEFromSVS.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param valueSetKeyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.datatypes.CE} object.
     */
    protected static CE populateCEFromSVS(String code, String valueSetKeyword) {
        Concept svsCode = ValueSetProvider.getInstance().getConceptForCode(valueSetKeyword, code, null);
        if (svsCode != null) {
            return new CE(code, svsCode.getDisplayName(), svsCode.getCodeSystem());
        } else {
            return new CE(code, null, null);
        }

    }

    /**
     * <p>buildPersonRelationshipForFindCandidatesResponse.</p>
     *
     * @param relative a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship} object.
     */
    protected PRPAMT201310UV02PersonalRelationship buildPersonRelationshipForFindCandidatesResponse(Person relative) {
        PRPAMT201310UV02PersonalRelationship relationship = new PRPAMT201310UV02PersonalRelationship();
        relationship.setClassCode(HL7V3Constants.PERSON_CLASS_CODE); // see figure  3.47.4.2.2-1 (ITI-2b)
        COCTMT030007UVPerson personHolder = COCTMT030007UVPersonBuilder.buildHolderForRelationship(relative);
        relationship.setRelationshipHolder1(personHolder);
        relationship.setCode(populateCEFromSVS(relative.getRelationshipCode(), "RELATIONSHIP"));
        if (relative.getIdentifier() != null && !relative.getIdentifier().isEmpty()) {
            II personId = buildIdentifierForRelative(relative);
            relationship.addId(personId);
        }
        // FIXME add the address
        // FIXME add the phone number
        return relationship;
    }

    /**
     * <p>buildPersonRelationshipForReviseMessage.</p>
     *
     * @param relative a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship} object.
     */
    protected PRPAMT201302UV02PersonalRelationship buildPersonRelationshipForReviseMessage(Person relative) {
        PRPAMT201302UV02PersonalRelationship relationship = new PRPAMT201302UV02PersonalRelationship();
        relationship.setClassCode(HL7V3Constants.PERSON_CLASS_CODE); // see figure  3.47.4.2.2-1 (ITI-2b)
        COCTMT030007UVPerson personHolder = COCTMT030007UVPersonBuilder.buildHolderForRelationship(relative);
        relationship.setRelationshipHolder1(personHolder);
        relationship.setCode(populateCEFromSVS(relative.getRelationshipCode(), "RELATIONSHIP"));
        if (relative.getIdentifier() != null && !relative.getIdentifier().isEmpty()) {
            II personId = buildIdentifierForRelative(relative);
            relationship.addId((PRPAMT201302UV02PersonalRelationshipId) personId);
        }
        // FIXME add the address
        // FIXME add the phone number
        return relationship;
    }

    /**
     * <p>builderPersonRelationshipForAddRecordMessage.</p>
     *
     * @param relative a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship} object.
     */
    protected PRPAMT201301UV02PersonalRelationship builderPersonRelationshipForAddRecordMessage(Person relative) {
        PRPAMT201301UV02PersonalRelationship relationship = new PRPAMT201301UV02PersonalRelationship();
        relationship.setClassCode(HL7V3Constants.PERSON_CLASS_CODE); // see figure  3.47.4.2.2-1 (ITI-2b)
        COCTMT030007UVPerson personHolder = COCTMT030007UVPersonBuilder.buildHolderForRelationship(relative);
        relationship.setRelationshipHolder1(personHolder);
        relationship.setCode(populateCEFromSVS(relative.getRelationshipCode(), "RELATIONSHIP"));
        if (relative.getIdentifier() != null && !relative.getIdentifier().isEmpty()) {
            PRPAMT201302UV02PersonalRelationshipId personId = buildIdentifierForRelative(relative);
            relationship.addId(personId);
        }
        // FIXME add the address
        // FIXME add the phone number
        return relationship;
    }

    private PRPAMT201302UV02PersonalRelationshipId buildIdentifierForRelative(Person relative) {
        return new PRPAMT201302UV02PersonalRelationshipId(PersonDAO.getIdentifierRoot(relative), PersonDAO.getIdentifierExtension(relative));
    }


    /**
     * <p>builPersonRelationshipForMothersMaidenNameInRevise.</p>
     *
     * @param mothersMaidenName a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship} object.
     */
    protected PRPAMT201302UV02PersonalRelationship builPersonRelationshipForMothersMaidenNameInRevise(String mothersMaidenName) {
        Person tempPerson = new Person(PersonalRelationshipRoleType.MTH);
        tempPerson.setLastName(mothersMaidenName);
        return buildPersonRelationshipForReviseMessage(tempPerson);
    }

    /**
     * <p>builPersonRelationshipForMothersMaidenNameInAddRecord.</p>
     *
     * @param mothersMaidenName a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship} object.
     */
    protected PRPAMT201301UV02PersonalRelationship builPersonRelationshipForMothersMaidenNameInAddRecord(String mothersMaidenName) {
        Person tempPerson = new Person(PersonalRelationshipRoleType.MTH);
        tempPerson.setLastName(mothersMaidenName);
        return builderPersonRelationshipForAddRecordMessage(tempPerson);
    }
}
