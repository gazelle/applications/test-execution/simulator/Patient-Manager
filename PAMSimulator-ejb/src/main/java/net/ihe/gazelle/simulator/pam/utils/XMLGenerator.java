package net.ihe.gazelle.simulator.pam.utils;

import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

/**
 * <p>XMLGenerator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XMLGenerator {

	
	private static Logger log = LoggerFactory.getLogger(XMLGenerator.class);
	
	/**
	 * <p>Encounter2XML4Rest.</p>
	 *
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String Encounter2XML4Rest(Encounter encounter) {
		try {
			Element xmlEncounter = new Element("Encounter");
			Document document = new Document(xmlEncounter);
			// encounter
			if (encounter.getVisitNumber() != null) {
				xmlEncounter.setAttribute("visitNumber", encounter.getVisitNumber());
			} else {
				xmlEncounter.setAttribute("visitNumber", "");
			}
			if (encounter.getPatientClassCode() != null) {
				xmlEncounter.setAttribute("patientClassCode", encounter.getPatientClassCode());
			} else {
				xmlEncounter.setAttribute("patientClassCode", "");
			}
			if (encounter.getReferringDoctorCode() != null) {
				xmlEncounter.setAttribute("referingDoctorCode", encounter.getReferringDoctorCode());
			} else {
				xmlEncounter.setAttribute("referingDoctorCode", "");
			}
			// patient
			Patient patient = encounter.getPatient();
			if (patient != null) {
				Element xmlPatient = new Element("Patient");
				xmlPatient.setAttribute("firstName", patient.getFirstName());
				xmlPatient.setAttribute("lastName", patient.getLastName());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				String dobString = sdf.format(patient.getDateOfBirth());
				xmlPatient.setAttribute("birthdate", dobString);
				xmlPatient.setAttribute("motherMaidenName", patient.getMotherMaidenName());
				xmlPatient.setAttribute("genderCode", patient.getGenderCode());
				// patient address
				Element patientAddress = new Element("Address");
				Element street = new Element("Street");
				street.addContent(patient.getAddressLine());
				patientAddress.addContent(street);
				Element zipCode = new Element("ZipCode");
				zipCode.addContent(patient.getZipCode());
				patientAddress.addContent(zipCode);
				Element city = new Element("City");
				city.addContent(patient.getCity());
				patientAddress.addContent(city);
				Element countryCode = new Element("CountryCode");
				countryCode.addContent(patient.getCountryCode());
				patientAddress.addContent(countryCode);
				Element state = new Element("State");
				state.addContent(patient.getState());
				patientAddress.addContent(state);
				// link patient address to patient
				xmlPatient.addContent(patientAddress);
				// patient's identifiers
				Element identifiers = new Element("PatientsIdentifiers");
				Element identifier;
				for (PatientIdentifier id : patient.getPatientIdentifiers()) {
					identifier = new Element("Identifier");
					identifier.setAttribute("id", id.getFullPatientId());
					identifier.setAttribute("identifierTypeCode", id.getIdentifierTypeCode());
					identifiers.addContent(identifier);
				}
				xmlPatient.addContent(identifiers);
				// link patient to encounter
				xmlEncounter.addContent(xmlPatient);
			}
			// movement
			Movement movement = encounter.getMovements().get(0);
			Element xmlMovements = new Element("Movements");
			Element xmlMovement = new Element("Movement");
			Element assignedLocation = new Element("AssignedPatientLocation");
			assignedLocation.addContent(movement.getAssignedPatientLocation());
			xmlMovement.addContent(assignedLocation);
			xmlMovements.addContent(xmlMovement);
			xmlEncounter.addContent(xmlMovements);

			// Document to String
			XMLOutputter outputter = new XMLOutputter();
			return outputter.outputString(document);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return "exception when creating the XML !!!";
		}
	}
}
