package net.ihe.gazelle.simulator.pdq.util;

import net.ihe.gazelle.HL7Common.action.SimulatorResponderConfigurationDisplay;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Abstract AbstractPDQPDSGuiManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractPDQPDSGuiManager extends SimulatorResponderConfigurationDisplay {

	/**
	 * 
	 */
	private static final long serialVersionUID = -725742593564824872L;

	/**
	 * <p>availableDomainsForPID.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public GazelleListDataModel<HierarchicDesignator> availableDomainsForPID() {
		PatientQuery query = new PatientQuery();
		List<String> actorKeywords = new ArrayList<String>();
		actorKeywords.add(PatientManagerConstants.PDS);
		actorKeywords.add(PatientManagerConstants.PES);
		query.simulatedActor().keyword().in(actorKeywords);
		query.stillActive().eq(true);
		List<HierarchicDesignator> hdList = query.patientIdentifiers().domain().getListDistinct();
		return new GazelleListDataModel<HierarchicDesignator>(hdList);
	}

    /** {@inheritDoc} */
    public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
        return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&transaction=" + conf.getTransaction().getId()
                + "&domain=" + conf.getDomain().getId();
    }

    public String linkToPatients(){
    	Actor pds = Actor.findActorWithKeyword(PatientManagerConstants.PDS);
    	return "/patient/allPatients.seam?actor=" + pds.getId();
	}

	public boolean displayLinkToPatients(){
    	return true;
	}
}
