package net.ihe.gazelle.simulator.pam.pes;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.exception.HL7MessageBuilderException;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.PatientSupplierManager;
import net.ihe.gazelle.simulator.pam.dao.EncounterDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.*;
import net.ihe.gazelle.simulator.pam.iti31.util.IHEPESEventHandler;
import net.ihe.gazelle.simulator.pam.iti31.util.IHEPESMessageGenerator;
import net.ihe.gazelle.simulator.pam.iti31.util.PESEventHandler;
import net.ihe.gazelle.simulator.pam.iti31.util.TriggerEventEnum;
import net.ihe.gazelle.simulator.pam.model.*;
import net.ihe.gazelle.simulator.pam.pds.ITI30ManagerLocal;
import net.ihe.gazelle.simulator.pam.utils.Pair;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.hibernate.Hibernate;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>PES class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pesManager")
@Scope(ScopeType.PAGE)
public class PES extends PatientSupplierManager implements Serializable, UserAttributeCommon {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String AFFINITY_DOMAIN_IHE = "IHE";
    private static final String AFFINITY_DOMAIN_PIX = "IHE_PIX";
    private static final String AFFINITY_DOMAIN_ADT = "IHE_RAD";
    private static final String SENDING_APPLICATION_KEY = "sending_application";
    private static final String SENDING_FACILITY_KEY = "sending_facility";

    // used for A40 not to interfer with the patient selected for the other
    // events
    private List<SelectItem> availableActions;
    private List<EncounterManagementEvent> availableEvents;
    private boolean confirmPreadmission = false;
    private boolean historicMovementOption;
    private ActionType selectedAction;
    private Encounter selectedEncounter;
    private EncounterManagementEvent selectedEvent;
    private Movement selectedMovement;

    private String selectedTriggerEvent;

    private Selection selection;
    private FilterEncounterManager encounterFilter;
    private FilterMovementManager movementFilter;
    private boolean displayEditPatientPanel;

    //patient identifiers management
    private String newPatientIdentifier;
    private List<Pair<Integer, String>> patientIdentifiers;
    private Integer idForUpdate;

    @In(value="gumUserService")
    private UserService userService;


    /**
     * {@inheritDoc}
     */
    @Override
    public Actor getSendingActor() {
        return sendingActor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createNewRelationship() {
        setCurrentRelationship(new Person(selectedPatient));
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRelationship() {
        selectedPatient.addPersonalRelationship(getCurrentRelationship());
        setCurrentRelationship(null);
    }

    private static Logger log = LoggerFactory.getLogger(PES.class);

    /**
     * <p>Setter for the field <code>selectedMovement</code>.</p>
     *
     * @param selectedMovement a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public void setSelectedMovement(Movement selectedMovement) {
        Hibernate.initialize(selectedMovement.getEncounter().getPatient().getAddressList());
        this.selectedMovement = selectedMovement;
        if ((this.selectedMovement != null) && this.selectedAction.equals(ActionType.UPDATE)) {
            if (this.selectedMovement.getEncounter() != null) {
                this.selectedEncounter = selectedMovement.getEncounter();
                this.selectedPatient = selectedEncounter.getPatient();
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This movement is not appropriate for the selected affinity domain");
            }
        }
    }

    /**
     * <p>randomlyFillEncounter.</p>
     */
    public void randomlyFillEncounter() {
        if (selectedEncounter == null) {
            selectedEncounter = new Encounter(selectedPatient);
        }
        if ((selectedEncounter.getVisitNumber() == null) || selectedEncounter.getVisitNumber().isEmpty()) {
            selectedEncounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        }
        if (selectedEncounter.getPatientClassCode() == null) {
            if (selectedTriggerEvent.equals("A04") || selectedTriggerEvent.equals("A07")) {
                selectedEncounter.setPatientClassCode(PatientManagerConstants.OUTPATIENT);
            } else {
                selectedEncounter.setPatientClassCode(PatientManagerConstants.INPATIENT);
            }
        }
        String concept;
        ValueSetProvider provider = ValueSetProvider.getInstance();
        if (!(selectedTriggerEvent.equals("A04") || selectedTriggerEvent.equals("A07") || selectedTriggerEvent
                .equals("A05"))) {
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.BED);
            if (concept != null) {
                selectedMovement.setBedCode(concept);
            }
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.FACILITY);
            if (concept != null) {
                selectedMovement.setFacilityCode(concept);
            }
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.ROOM);
            if (concept != null) {
                selectedMovement.setRoomCode(concept);
            }
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.POINT_OF_CARE);
            if (concept != null) {
                selectedMovement.setPointOfCareCode(concept);
            }
            if (!(selectedTriggerEvent.equals("A04") || selectedTriggerEvent.equals("A05"))) {
                concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.HOSPITAL_SERVICE);
                if (concept != null) {
                    selectedEncounter.setHospitalServiceCode(concept);
                }
            }
        }
        if (!(selectedTriggerEvent.equals("A04") || selectedTriggerEvent.equals("A05"))) {
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.DOCTOR);
            selectedEncounter.setAttendingDoctorCode(concept);
            concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.DOCTOR);
            selectedEncounter.setAdmittingDoctorCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.DOCTOR);
        selectedEncounter.setReferringDoctorCode(concept);
        if (selectedTriggerEvent.equals("A05") || selectedTriggerEvent.equals("A14")) {
            selectedEncounter.setExpectedAdmitDate(new Date());
        } else {
            selectedEncounter.setAdmitDate(new Date());
        }
        concept = provider.getRandomCodeFromValueSet(PatientManagerConstants.ADMISSION_TYPE_TABLE);
        if (concept != null) {
            selectedEncounter.setAdmissionType(concept);
        }
        if (isBP6Mode()) {
            selectedMovement.setNatureOfMovement("HMS");
            selectedMovement.setNursingCareWard(provider.getRandomCodeFromValueSet(PatientManagerConstants.NURSING_WARD));
            selectedMovement.setWardCode(provider.getRandomCodeFromValueSet(PatientManagerConstants.MEDICAL_WARD));
            selectedEncounter.getPatientsPHRInfo().setStatus(PatientManagerConstants.INEXISTANT);
            selectedEncounter.getDrgMovement().setPmsiAdmissionMode("8");
        }
    }

    /**
     * <p>initialize.</p>
     */
    @Create
    public void initialize() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        sendingActor = Actor.findActorWithKeyword(urlParams.get("initiator"));
        simulatedTransaction = Transaction.GetTransactionByKeyword(urlParams.get("transaction"));
        String affinityDomain = AFFINITY_DOMAIN_IHE;
        receivingActor = Actor.findActorWithKeyword("PEC");
        if ((sendingActor == null) && (simulatedTransaction == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No initiator given in URL parameter, take PES as default");
            sendingActor = Actor.findActorWithKeyword("PES");
            simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-31");
        } else if ((sendingActor != null) && sendingActor.getKeyword().equals("PAT_IDENTITY_SRC")) {
            simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-8");
            affinityDomain = AFFINITY_DOMAIN_PIX;
            receivingActor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
        } else if ((simulatedTransaction != null) && simulatedTransaction.getKeyword().equals("RAD-1")) {
            sendingActor = Actor.findActorWithKeyword("ADT");
            affinityDomain = AFFINITY_DOMAIN_ADT + "1";
            receivingActor = Actor.findActorWithKeyword("MPI");
        } else if ((simulatedTransaction != null) && simulatedTransaction.getKeyword().equals("RAD-12")) {
            sendingActor = Actor.findActorWithKeyword("ADT");
            affinityDomain = AFFINITY_DOMAIN_ADT + "12";
            receivingActor = Actor.findActorWithKeyword("MPI");
        }
        super.initialize();
        sendingFacility = ApplicationConfiguration.getValueOfVariable(SENDING_FACILITY_KEY);
        sendingApplication = ApplicationConfiguration.getValueOfVariable(SENDING_APPLICATION_KEY);
        getAllAvailableEventsForPage(affinityDomain);
        getAllAvailableActions();
        setHistoricMovementOption(false);
        if (simulatedTransaction != null) {
            availableSystems = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(simulatedTransaction.getKeyword(), HL7Protocol.MLLP);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No transaction is set, cannot list the available systems under test");
        }
    }

    /**
     * Generate a new patient randomly
     */
    @SuppressWarnings("unchecked")
    private Patient generateAPatient() {
        Patient generatedPatient = generateNewPatient(getSendingActor());
        if (generatedPatient != null) {
            // save identifiers before saving the patient
            List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(generatedPatient
                    .getPatientIdentifiers());
            generatedPatient.setPatientIdentifiers(mergedIdentifiers);
            generatedPatient = generatedPatient.savePatient(EntityManagerService.provideEntityManager());
            displayDDSPanel = false;
            return generatedPatient;
        } else {
            return null;
        }
    }

    /**
     * <p>generateSelectedPatient.</p>
     */
    public void generateSelectedPatient() {
        selectedPatient = generateAPatient();
        if (!(selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A08") || selectedTriggerEvent.equals("A44"))) {
            selectedEncounter = new Encounter(selectedPatient);
            selectedMovement = new Movement(selectedEncounter);
        }
        generatePatientIdentifiersList(selectedPatient);
    }

    /**
     * <p>generateIncorrectPatient.</p>
     */
    public void generateIncorrectPatient() {
        secondaryPatient = generateAPatient();
    }

    /**
     * List the action that are available for the selected event
     *
     * @param event a {@link javax.faces.event.ValueChangeEvent} object.
     */
    public void eventChangeListener(ValueChangeEvent event) {
        selectedEvent = (EncounterManagementEvent) event.getNewValue();
        getAllAvailableActions();
    }

    private void getAllAvailableActions() {
        if (selectedEvent != null) {
            availableActions = new ArrayList<>();
            availableActions.add(new SelectItem(ActionType.INSERT, ActionType.INSERT.getName().concat(" ("
                    + selectedEvent.getInsertTriggerEvent() + ")")));
            if (selectedEvent.getCancelTriggerEvent() != null) {
                availableActions.add(new SelectItem(ActionType.CANCEL, ActionType.CANCEL.getName().concat(" ("
                        + selectedEvent.getCancelTriggerEvent() + ")")));
            }
            if (selectedEvent.getUpdateTriggerEvent() != null) {
                availableActions.add(new SelectItem(ActionType.UPDATE, ActionType.UPDATE.getName().concat(" ("
                        + selectedEvent.getUpdateTriggerEvent() + ")")));
            } else {
                historicMovementOption = false;
            }
            selectedAction = ActionType.INSERT;
            resetPatientChoice();
            setChosenTriggerEvent();
        } else {
            availableActions = null;
            selectedAction = null;
        }
    }

    /**
     * <p>actionChangeListener.</p>
     *
     * @param event a {@link javax.faces.event.ValueChangeEvent} object.
     */
    public void actionChangeListener(ValueChangeEvent event) {
        selectedAction = (ActionType) event.getNewValue();
        resetPatientChoice();
        setChosenTriggerEvent();
    }

    private void getAllAvailableEventsForPage(String affinityDomainKeyword) {
        availableEvents = EncounterManagementEvent.listEventsForPage(affinityDomainKeyword);
        if (availableEvents != null) {
            selectedEvent = availableEvents.get(0);
            selectedTriggerEvent = selectedEvent.getInsertTriggerEvent();
        } else {
            selectedEvent = null;
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "No event are available");
        }
    }

    /**
     * <p>getNewVisitNumber.</p>
     */
    public void getNewVisitNumber() {
        if (selectedEncounter != null) {
            selectedEncounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        }
    }


    /*
     * <p>performAnotherTest.</p>
     */
    public void performAnotherTest() {
        resetPatientChoice();
        hl7Messages = null;

    }

    /**
     * <p>confirmPreadmissionListener.</p>
     *
     * @param event a {@link javax.faces.event.ValueChangeEvent} object.
     */
    public void confirmPreadmissionListener(ValueChangeEvent event) {
        confirmPreadmission = (Boolean) event.getNewValue();
        setChosenTriggerEvent();
    }

    /**
     * <p>resetPatientChoice.</p>
     */
    public void resetPatientChoice() {
        selectedPatient = null;
        selectedEncounter = null;
        selectedMovement = null;
        secondaryPatient = null;
        displayPatientsList = true;
        displayDDSPanel = false;
        confirmPreadmission = false;
    }

    /**
     * <p>selectEncounterForInsert.</p>
     *
     * @param inEncounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void selectEncounterForInsert(Encounter inEncounter) {
        selectedEncounter = inEncounter;
        if (selectedEncounter != null) {
            selectedMovement = new Movement(selectedEncounter);
            selectedMovement.setSimulatedActor(sendingActor);
            selectedMovement.setMovementUniqueId(NumberGenerator.generate(DesignatorType.MOVEMENT_ID));
            selectedMovement.setActionType(ActionType.INSERT);
            selectedMovement.setLastChanged(new Date());
            selectedMovement.setTriggerEvent(selectedTriggerEvent);
            selectedPatient = selectedEncounter.getPatient();
            if (selectedTriggerEvent.equals("A54")) {
                selectedEncounter.setPriorAttendingDoctorCode(inEncounter.getAttendingDoctorCode());
                selectedEncounter.setAttendingDoctorCode(null);
            } else if (selectedTriggerEvent.equals("A02") || selectedTriggerEvent.equals("A15")) {
                Movement previousMovement = selectedMovement.getPreviousValidMovement(EntityManagerService.provideEntityManager(), true);
                if (previousMovement != null) {
                    selectedMovement.setPriorPatientLocation(previousMovement.getAssignedPatientLocation());
                }
            }
            // PAM-566, ZBE fields are now used in IHE Intl (not only IHE France/BP6)
            if (selectedEvent.getUpdateTriggerEvent() != null && historicMovementOption) {
                Movement previousMovement = selectedMovement.getPreviousValidMovement(EntityManagerService.provideEntityManager(), true);
                if (previousMovement != null) {
                    selectedMovement.setWardCode(previousMovement.getWardCode());
                    selectedMovement.setNursingCareWard(previousMovement.getNursingCareWard());
                }
            }
        } else {
            log.error("selectedEncounter is null");
        }
        displayPatientsList = false;
    }


    /**
     * <p>selectPatient.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void selectPatient(Patient inPatient) {
        Hibernate.initialize(inPatient.getLegalCareModes());
        selectedPatient = inPatient;
        generatePatientIdentifiersList(selectedPatient);
        if (selectedPatient != null &&
                (selectedPatient.getAccountNumber() == null || selectedPatient.getAccountNumber().isEmpty())) {
            EntityManager em = EntityManagerService.provideEntityManager();
            String an = NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER);
            selectedPatient.setAccountNumber(an);
            selectedPatient.savePatient(em);
        }
        // for A01 we check that the patient has not already an opened admission
        if (!confirmPreadmission && selectedTriggerEvent.equals("A01")) {
            if (EncounterDAO.isPatientAlreadyAdmitted(selectedPatient, sendingActor)) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "gazelle.simulator.pam.AnAdmissionIsAlreadyOpenedForThisPatient");
                selectedPatient = null;
                selectedEncounter = null;
                return;
            }
        }
        if (confirmPreadmission && !EncounterDAO.isPatientPreAdmitted(selectedPatient, sendingActor)) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.simulator.pam.noPreAdmissionForThisPatient");
            selectedPatient = null;
            selectedEncounter = null;
            return;
        }
        displayPatientsList = false;
        displayDDSPanel = false;
        selectedEncounter = new Encounter(selectedPatient);
        selectedMovement = new Movement(selectedEncounter);
    }

    /**
     * <p>selectPatientForUpdate.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    @SuppressWarnings("unchecked")
    public void selectPatientForUpdate(Patient inPatient) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        // create a new patient from the first one
        Patient oldPatient = inPatient;
        secondaryPatient = newInstanceOfPatient(oldPatient, sendingActor);
        oldPatient.setStillActive(false);
        oldPatient = oldPatient.savePatient(entityManager);

        // fill the new patient
        if (Identity.instance().isLoggedIn()) {
            secondaryPatient.setCreator(Identity.instance().getCredentials().getUsername());
        }
        secondaryPatient = secondaryPatient.savePatient(entityManager);

        // patient's history
        PatientHistory history = new PatientHistory(oldPatient, secondaryPatient, PatientHistory.ActionPerformed.UPDATE, null, null,
                null, null);
        history.save(entityManager);

        // move encounters from oldPatient to the newly created patient
        List<Encounter> encounters = oldPatient.getAllEncountersForPatient(entityManager);
        if (encounters != null) {
            for (Encounter encounter : encounters) {
                encounter.setPatient(secondaryPatient);
                encounter.save(entityManager);
            }
        }
        generatePatientIdentifiersList(secondaryPatient);
        displayDDSPanel = false;
        displayPatientsList = false;
    }

    /**
     * <p>sendCancelMessage.</p>
     */
    @SuppressWarnings("unchecked")
    public void sendCancelMessage() {
        if (selectedMovement != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedEncounter = selectedMovement.getEncounter();
            selectedPatient = selectedEncounter.getPatient();
            PESEventHandler eventHandler = new IHEPESEventHandler();
            eventHandler
                    .setAttributes(selectedEncounter, selectedMovement, selectedTriggerEvent, sendingActor, entityManager);
            selectedMovement = eventHandler.processCancel();

            // send the message
            sendMessage();
        } else {
            log.error("no movement selected !");
        }
    }

    /**
     * <p>sendMessage.</p>
     */
    @SuppressWarnings("unchecked")
    @Override
    public void sendMessage() {
        if (selectedSUT == null) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.WARN, "gazelle.simulator.pam.noSUTSelected");
            return;
        } else if (selectedTriggerEvent.equals("A08") && (secondaryPatient == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please, first select a patient");
            return;
        } else if ((selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44")) && (secondaryPatient == null || selectedPatient ==
                null)) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, select the incorrect AND the target patients");
            return;
        } else if (!(selectedTriggerEvent.equals("A08") || selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44"))
                && (selectedEncounter == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select an encounter");
            return;
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        String username;
        if (Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        } else {
            username = null;
        }
        //
        if (selectedAction.equals(ActionType.INSERT)) {
            if (selectedEncounter != null) {
                setEncounterMetadata(entityManager, username);
                PESEventHandler eventHandler = new IHEPESEventHandler();
                eventHandler
                        .setAttributes(selectedEncounter, selectedMovement, selectedTriggerEvent, sendingActor, entityManager);
                selectedMovement = eventHandler.processInsert();
                selectedMovement.save(entityManager);
            }
            // process merge
            if (selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44")) {
                processMerge(entityManager);
            } else if (selectedTriggerEvent.equals("A08")) {
                processUpdatePatientInfo(entityManager);
            } else {
                secondaryPatient = null;
            }
        } else if (selectedAction.equals(ActionType.UPDATE)) {
            setMovementMetadataOnUpdate(entityManager);
        }
        // message creation
        IHEPESMessageGenerator builder = new IHEPESMessageGenerator(isBP6Mode());
        if (selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44")) {
            builder.setAttributes(selectedEncounter, selectedMovement, selectedPatient, selectedTriggerEvent,
                    historicMovementOption, secondaryPatient, selectedSUT, sendingApplication, sendingFacility, simulatedTransaction.getKeyword());
        } else {
            builder.setAttributes(selectedEncounter, selectedMovement, selectedPatient, selectedTriggerEvent,
                    historicMovementOption, null, selectedSUT, sendingApplication, sendingFacility, simulatedTransaction.getKeyword());
        }
        try {
            String messageToSend = builder.build();
            messageToSend = convertMessageForFrance(builder, messageToSend);
            sendMessageAndStore(builder, messageToSend);
        } catch (HL7MessageBuilderException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot build message: " + e.getMessage());
        } catch (HL7Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot send message: " + e.getMessage());
        }

        selectedEncounter = null;
    }

    private void sendMessageAndStore(IHEPESMessageGenerator builder, String messageToSend) throws HL7Exception {
        Initiator initiator = new Initiator(selectedSUT, sendingFacility, sendingApplication, sendingActor,
                simulatedTransaction, messageToSend, builder.getMessageType(), getDomain(), receivingActor);
        TransactionInstance instance = initiator.sendMessageAndGetTheHL7Message();
        hl7Messages = new ArrayList<>();
        hl7Messages.add(instance);
    }

    private String convertMessageForFrance(IHEPESMessageGenerator builder, String messageToSend) throws HL7Exception {
        if (isBP6Mode() && !(selectedTriggerEvent.equals("A44") || selectedTriggerEvent.equals("A40"))) {
            messageToSend = builder.convertMessageToBP6(messageToSend);
        }
        return messageToSend;
    }

    private void setMovementMetadataOnUpdate(EntityManager entityManager) {
        selectedMovement.setActionType(ActionType.UPDATE);
        selectedMovement.setLastChanged(new Date());
        selectedMovement.save(entityManager);
    }

    private void processUpdatePatientInfo(EntityManager entityManager) {
        EncounterQuery query = new EncounterQuery();
        query.patient().id().eq(secondaryPatient.getId());
        query.open().eq(true);
        query.simulatedActor().eq(sendingActor);
        query.admitDate().order(false);
        selectedEncounter = query.getUniqueResult();
        selectedPatient = secondaryPatient.savePatient(entityManager);
    }

    private void setEncounterMetadata(EntityManager entityManager, String username) {
        // common to all INSERTED/UPDATED encounters
        if (selectedEncounter.getCreator() != null) {
            selectedEncounter.setCreator(username);
        }
        if (selectedMovement.getCreator() != null) {
            selectedMovement.setCreator(username);
        }
        selectedMovement.setLastChanged(new Date());
        selectedMovement.setMovementDate(new Date());
        selectedMovement.setSimulatedActor(sendingActor);
        selectedEncounter.setSimulatedActor(sendingActor);
        selectedEncounter.setCreationDate(new Date());
        selectedEncounter = selectedEncounter.save(entityManager);
    }

    /**
     * Select the appropriate trigger event according the selected action
     */
    public void setChosenTriggerEvent() {
        if (selectedAction != null) {
            if (selectedAction.equals(ActionType.INSERT)) {
                selectedTriggerEvent = selectedEvent.getInsertTriggerEvent();
            } else if (selectedAction.equals(ActionType.CANCEL)) {
                selectedTriggerEvent = selectedEvent.getCancelTriggerEvent();
            } else {
                selectedTriggerEvent = selectedEvent.getUpdateTriggerEvent();
            }
        }
        displayDDSPanel = false;
        displayPatientsList = false;
        selection = selectedEvent.getSelection();
        if (confirmPreadmission
                && (selectedTriggerEvent.equals(TriggerEventEnum.A01.getTriggerEvent())
                || selectedTriggerEvent.equals(TriggerEventEnum.A04.getTriggerEvent()))) {
            selection = Selection.ENCOUNTERS_PRE_ADMIT;
            encounterFilter = new FilterEncounterManager(sendingActor);
            encounterFilter.setStatus(PatientStatus.STATUS_PRE_ADMITTED);
        } else if (!selectedAction.equals(ActionType.INSERT)) {
            selection = Selection.MOVEMENTS;
            movementFilter = new FilterMovementManager(sendingActor, selectedEvent.getInsertTriggerEvent(), selectedAction);
        } else if (selection.displayEncounters()) {
            encounterFilter = new FilterEncounterManager(sendingActor);
            if (selectedTriggerEvent.equals(TriggerEventEnum.A06.getTriggerEvent())) {
                encounterFilter.setStatus(PatientStatus.STATUS_REGISTERED);
            } else if (selectedTriggerEvent.equals(TriggerEventEnum.A07.getTriggerEvent())) {
                encounterFilter.setStatus(PatientStatus.STATUS_ADMITTED);
            } else if (selectedTriggerEvent.equals(TriggerEventEnum.A22.getTriggerEvent())) {
                encounterFilter.setStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
            } else if (selectedTriggerEvent.equals(TriggerEventEnum.A21.getTriggerEvent())) {
                encounterFilter.setStatus(PatientStatus.STATUS_ADMITTED);
            }
        } else if (selection.displayPatients()) {
            displayPatientsList = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayPatientsList() {
        return displayPatientsList && (selection != null && selection.displayPatients());
    }

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>Setter for the field <code>confirmPreadmission</code>.</p>
     *
     * @param confirmPreadmission a boolean.
     */
    public void setConfirmPreadmission(boolean confirmPreadmission) {
        this.confirmPreadmission = confirmPreadmission;
    }

    /**
     * <p>Setter for the field <code>historicMovementOption</code>.</p>
     *
     * @param historicMovementOption a boolean.
     */
    public void setHistoricMovementOption(boolean historicMovementOption) {
        this.historicMovementOption = historicMovementOption;
    }

    /**
     * <p>Setter for the field <code>secondaryPatient</code>.</p>
     *
     * @param secondaryPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSecondaryPatient(Patient secondaryPatient) {
        this.secondaryPatient = secondaryPatient;
        if (selectedTriggerEvent != null && (selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44"))) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Incorrect patient has been selected");
        }
    }

    /**
     * <p>Setter for the field <code>selectedAction</code>.</p>
     *
     * @param selectedAction a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
     */
    public void setSelectedAction(ActionType selectedAction) {
        this.selectedAction = selectedAction;
    }

    /**
     * <p>Setter for the field <code>selectedEncounter</code>.</p>
     *
     * @param selectedEncounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setSelectedEncounter(Encounter selectedEncounter) {
        this.selectedEncounter = selectedEncounter;
    }

    /**
     * <p>Setter for the field <code>selectedEvent</code>.</p>
     *
     * @param selectedEvent a {@link net.ihe.gazelle.simulator.pam.iti31.model.EncounterManagementEvent} object.
     */
    public void setSelectedEvent(EncounterManagementEvent selectedEvent) {
        this.selectedEvent = selectedEvent;
    }


    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    @Override
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
        if (selectedTriggerEvent != null && (selectedTriggerEvent.equals("A40") || selectedTriggerEvent.equals("A44"))) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Correct patient has been selected");
        }
    }


    private Patient getUniquePatient() {
        if (selectedPatient != null) {
            return selectedPatient;
        } else if (secondaryPatient != null) {
            return secondaryPatient;
        } else {
            return null;
        }
    }

    /**
     *
     */
    private void processMerge(EntityManager entityManager) {
        if ((secondaryPatient == null) || (selectedPatient == null)) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.simulator.pam.SelectTwoPatientsToMergeAndRetry");
        } else {
            String currentUser = null;
            if (Identity.instance().isLoggedIn()) {
                currentUser = Identity.instance().getCredentials().getUsername();
            }
            secondaryPatient.setStillActive(selectedTriggerEvent.equals("A44"));
            String action = (selectedTriggerEvent.equals("A40")) ? PatientHistory.ActionPerformed.MERGE : PatientHistory.ActionPerformed.ACCOUNT_MOVE;
            String messageType = (selectedTriggerEvent.equals("A40")) ? "ADT^A40^ADT_A39" : "ADT^A44^ADT_A43";
            PatientHistory history = new PatientHistory(secondaryPatient, selectedPatient, action,
                    messageType, null, selectedSUT.getFacility(), selectedSUT.getApplication());
            history.save(entityManager);
            history = new PatientHistory(selectedPatient, secondaryPatient, action,
                    messageType, currentUser, selectedSUT.getFacility(), selectedSUT.getApplication());
            history.save(entityManager);
            secondaryPatient = secondaryPatient.savePatient(entityManager);
            selectedPatient = selectedPatient.savePatient(entityManager);
            List<Encounter> encounters;
            if (selectedTriggerEvent.equals("A40")) {
                encounters = secondaryPatient.getAllEncountersForPatient(entityManager);
            } else {
                // retrieve the encounters with the same account number
                encounters = EncounterDAO.getEncountersByAccountNumber(secondaryPatient);
                // we generate a new account number for this patient since the previous one is not to be used anymore with this patient
                secondaryPatient.setAccountNumber(NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER));
                secondaryPatient.savePatient(entityManager);
            }
            if (encounters != null) {
                for (Encounter encounter : encounters) {
                    encounter.setPatient(selectedPatient);
                    encounter.save(entityManager);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> params) {
        super.modifyQuery(queryBuilder, params);
        PatientQuery query = new PatientQuery();
        if (selectedTriggerEvent != null && selectedTriggerEvent.equals("A01")) {
            queryBuilder.addRestriction(HQLRestrictions.or(query.encounters().isEmptyRestriction(), query.encounters().open().eqRestriction(false)));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void switchToGenerationMode() {
        selection = Selection.PATIENTS;
        super.switchToGenerationMode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void switchToSelectionMode() {
        selection = Selection.PATIENTS;
        super.switchToSelectionMode();
    }

    /**
     * <p>switchToEncounterSelectionMode.</p>
     */
    public void switchToEncounterSelectionMode() {
        selection = Selection.ENCOUNTERS_OR_PATIENTS;
        displayDDSPanel = false;
        displayPatientsList = false;
    }

    /**
     * <p>Getter for the field <code>availableEvents</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<EncounterManagementEvent> getAvailableEvents() {
        return availableEvents;
    }


    /**
     * <p>newInstanceOfPatient.</p>
     *
     * @param oldPatient     a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient newInstanceOfPatient(Patient oldPatient, Actor simulatedActor) {
        return new Patient(oldPatient, simulatedActor);
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getsimulatedActor() {
        return sendingActor;
    }

    /**
     * <p>isConfirmPreadmission.</p>
     *
     * @return a boolean.
     */
    public boolean isConfirmPreadmission() {
        return confirmPreadmission;
    }

    /**
     * <p>isHistoricMovementOption.</p>
     *
     * @return a boolean.
     */
    public boolean isHistoricMovementOption() {
        if (isBP6Mode()) {
            historicMovementOption = true;
        }
        return historicMovementOption;
    }

    /**
     * <p>Getter for the field <code>selection</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.pes.Selection} object.
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * <p>Getter for the field <code>availableActions</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getAvailableActions() {
        return availableActions;
    }


    /**
     * <p>Getter for the field <code>encounterFilter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.pes.FilterEncounterManager} object.
     */
    public FilterEncounterManager getEncounterFilter() {
        return encounterFilter;
    }

    /**
     * <p>Getter for the field <code>movementFilter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.pes.FilterMovementManager} object.
     */
    public FilterMovementManager getMovementFilter() {
        return movementFilter;
    }

    /**
     * <p>Getter for the field <code>selectedAction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
     */
    public ActionType getSelectedAction() {
        return selectedAction;
    }

    /**
     * <p>Getter for the field <code>selectedEncounter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getSelectedEncounter() {
        return selectedEncounter;
    }

    /**
     * <p>Getter for the field <code>selectedEvent</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.EncounterManagementEvent} object.
     */
    public EncounterManagementEvent getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * <p>Getter for the field <code>selectedMovement</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public Movement getSelectedMovement() {
        return selectedMovement;
    }


    /**
     * <p>Getter for the field <code>selectedTriggerEvent</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectedTriggerEvent() {
        return selectedTriggerEvent;
    }

    /**
     * <p>isDisplayEditPatientPanel.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayEditPatientPanel() {
        return displayEditPatientPanel;
    }

    /**
     * <p>Setter for the field <code>displayEditPatientPanel</code>.</p>
     *
     * @param displayEditPatientPanel a boolean.
     */
    public void setDisplayEditPatientPanel(boolean displayEditPatientPanel) {
        this.displayEditPatientPanel = displayEditPatientPanel;
    }

    /**
     * <p>savePatient.</p>
     */
    public void savePatient() {
        log.info("saving patient");
        this.selectedPatient = selectedPatient.savePatient(EntityManagerService.provideEntityManager());
        setDisplayEditPatientPanel(false);
    }

    /**
     * <p>isBP6Mode.</p>
     *
     * @return a boolean.
     */
    public boolean isBP6Mode() {
        if (sendingActor.getKeyword().equals("PES")) {
            ITI30ManagerLocal local = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
            return local.isBp6Mode();
        } else {
            return false;
        }
    }

    /**
     * <p>getDomain.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDomain() {
        if (sendingActor.getKeyword().equals("PES") && isBP6Mode()) {
            return "ITI-FR";
        } else if (sendingActor.getKeyword().equals("ADT")) {
            return "RAD";
        } else {
            return "ITI";
        }
    }

    /**
     * Used to generate additional identifiers on A08 event -- secondaryPatient is populated in that case
     */
    public void generateAdditionalIdentifiers() {
        patientBuilderFactory.make(getHL7Domain()).buildIdentifiers(secondaryPatient,selectedAuthorities);
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Pair<Integer, String>> getPatientIdentifiers() {
        return patientIdentifiers;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<Pair<Integer, String>> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>idForUpdate</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getIdForUpdate() {
        return idForUpdate;
    }

    /**
     * <p>Setter for the field <code>idForUpdate</code>.</p>
     *
     * @param idForUpdate a {@link java.lang.Integer} object.
     */
    public void setIdForUpdate(Integer idForUpdate) {
        this.idForUpdate = idForUpdate;
    }

    /**
     * <p>Getter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNewPatientIdentifier() {
        return newPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @param newPatientIdentifier a {@link java.lang.String} object.
     */
    public void setNewPatientIdentifier(String newPatientIdentifier) {
        this.newPatientIdentifier = newPatientIdentifier;
    }

    // identifiers management
    private void generatePatientIdentifiersList(Patient patient) {
        if (patient.getPatientIdentifiers() != null) {
            patientIdentifiers = new ArrayList<>();
            for (int index = 0; index < patient.getPatientIdentifiers().size(); index++) {
                String fullPatientId = patient.getPatientIdentifiers().get(index).getFullPatientId();
                if (patient.getPatientIdentifiers().get(index).getIdentifierTypeCode() != null) {
                    fullPatientId = fullPatientId.concat("^").concat(
                            patient.getPatientIdentifiers().get(index).getIdentifierTypeCode());
                }
                patientIdentifiers.add(new Pair<>(index, fullPatientId));
            }
        } else {
            patientIdentifiers = null;
        }
    }

    /**
     * <p>changeIdentifier.</p>
     *
     * @param pidIndex    a {@link java.lang.Integer} object.
     * @param newIdString a {@link java.lang.String} object.
     */
    public void changeIdentifier(Integer pidIndex, String newIdString) {
        String[] pidComponents = newIdString.split("\\^");
        String fullId;
        String newIdentifierTypeCode = null;
        if (pidComponents.length > 4) {
            int cx5Index = newIdString.lastIndexOf("^");
            fullId = newIdString.substring(0, cx5Index);
            newIdentifierTypeCode = newIdString.substring(cx5Index + 1);
        } else {
            fullId = newIdString;
        }
        PatientIdentifier newIdentifier = new PatientIdentifier(fullId, newIdentifierTypeCode);
        PatientIdentifier modifiedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        if (!newIdentifier.equals(modifiedPid)) {
            removeIdentifier(modifiedPid);
            selectedPatient.getPatientIdentifiers().add(newIdentifier);
            generatePatientIdentifiersList(selectedPatient);
        }
        idForUpdate = null;
    }

    /**
     * <p>addIdentifier.</p>
     */
    public void addIdentifier() {
        if ((newPatientIdentifier != null) && !newPatientIdentifier.isEmpty()) {
            String[] pidComponents = newPatientIdentifier.split("\\^");
            String newIdentifierTypeCode = null;
            if (pidComponents.length > 4) {
                int cx5Index = newPatientIdentifier.lastIndexOf("^");
                newIdentifierTypeCode = newPatientIdentifier.substring(cx5Index + 1);
            }
            PatientIdentifier id = new PatientIdentifier(newPatientIdentifier, newIdentifierTypeCode);
            if (selectedPatient.getPatientIdentifiers() == null) {
                List<PatientIdentifier> identifiers = new ArrayList<>();
                identifiers.add(id);
                selectedPatient.setPatientIdentifiers(identifiers);
            } else {
                selectedPatient.getPatientIdentifiers().add(id);
            }
            newPatientIdentifier = null;
        }
        generatePatientIdentifiersList(selectedPatient);
    }

    /**
     * <p>removeIdentifier.</p>
     *
     * @param pidIndex a {@link java.lang.Integer} object.
     */
    public void removeIdentifier(Integer pidIndex) {
        PatientIdentifier removedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        removeIdentifier(removedPid);
        generatePatientIdentifiersList(selectedPatient);
    }

    private void removeIdentifier(PatientIdentifier identifier) {
        if (selectedPatient.getPatientIdentifiers().contains(identifier)) {
            selectedPatient.getPatientIdentifiers().remove(identifier);
        } else {
            log.error("this identifier is not linked to the current patient");
        }
    }

    /**
     * <p>copyAccountNumberToVisitNumber.</p>
     */
    public void copyAccountNumberToVisitNumber() {
        if (selectedEncounter != null && selectedEncounter.getPatient() != null && selectedEncounter.getPatient().getAccountNumber() != null) {
            selectedEncounter.setVisitNumber(selectedEncounter.getPatient().getAccountNumber());
        }
    }

    /**
     * <p>createNewPhoneNumber.</p>
     */
    @Override
    public void createNewPhoneNumber() {
        this.newPhoneNumber = new PatientPhoneNumber();
        if (getUniquePatient() != null) {
            newPhoneNumber.setPatient(getUniquePatient());
        }
    }

    /**
     * <p>addNewPhoneNumber.</p>
     */
    @Override
    public void addNewPhoneNumber() {
        if (this.newPhoneNumber.getValue() == null || this.newPhoneNumber.getValue().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Phone number shall have a value");
        } else {
            if (getUniquePatient() != null) {
                getUniquePatient().addPhoneNumber(newPhoneNumber);
            }
            newPhoneNumber = null;
        }
    }

    protected Class<? extends HL7Domain<?>> getHL7DomainClass() {
        return getSendingActor() != null && PatientManagerConstants.PES.equals(getSendingActor().getKeyword()) && isBP6Mode()
                ? BP6HL7Domain.class
                : IHEHL7Domain.class;
    }

}
