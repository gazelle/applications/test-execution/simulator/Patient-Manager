package net.ihe.gazelle.simulator.fhir.context;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.server.FifoMemoryPagingProvider;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.IncomingRequestAddressStrategy;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import net.ihe.gazelle.simulator.fhir.resources.RestfulPatientResourceProvider;
import net.ihe.gazelle.simulator.fhir.util.GazelleServerInterceptor;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xfs on 20/04/16.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PatientManagerFhirServer extends RestfulServer {

    private static final long serialVersionUID = 1L;
    /**
     * Constant <code>DEFAULT_PAGE_SIZE=10</code>
     */
    public static final int DEFAULT_PAGE_SIZE = 10;
    /**
     * Constant <code>MAXIMUM_PAGE_SIZE=100</code>
     */
    public static final int MAXIMUM_PAGE_SIZE = 100;


    /**
     * {@inheritDoc}
     * The initialize method is automatically called when the servlet is starting up, so it can
     * be used to configure the servlet to define resource providers, or set up
     * configuration, interceptors, etc.
     */
    @Override
    protected void initialize() throws ServletException {
        // Address
        setServerAddressStrategy(new IncomingRequestAddressStrategy());

        // Encoding
        setDefaultResponseEncoding(EncodingEnum.XML);

        // Description
        setImplementationDescription("IHE PDQm Patient Demographics Supplier and IHE PIXm Patient Identifier Cross-Reference Manager");

        // Resources
        List<IResourceProvider> resourceProviders = new ArrayList<IResourceProvider>();
        resourceProviders.add(new RestfulPatientResourceProvider());
        setResourceProviders(resourceProviders);
        ResponseHighlighterInterceptor interceptor = new ResponseHighlighterInterceptor();
        registerInterceptor(interceptor);
        registerInterceptor(new GazelleServerInterceptor());
    }

    /**
     * <p>Constructor for PatientManagerFhirServer.</p>
     */
    public PatientManagerFhirServer() {
        super(FhirContext.forDstu3());

        // Paging
        FifoMemoryPagingProvider pp = new FifoMemoryPagingProvider(DEFAULT_PAGE_SIZE);
        pp.setDefaultPageSize(DEFAULT_PAGE_SIZE);
        pp.setMaximumPageSize(MAXIMUM_PAGE_SIZE);
        setPagingProvider(pp);

    }

}
