package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareModeQuery;
import net.ihe.gazelle.simulator.pam.model.Patient;

import javax.persistence.EntityManager;

/**
 * <p>LegalCareModeDAO class.</p>
 *
 * @author abe
 * @version 1.0: 28/03/19
 */

public class LegalCareModeDAO {

    public static void saveEntity(LegalCareMode mode){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(mode);
        entityManager.flush();
    }

    public static LegalCareMode getCareModeForPatientByIdentifier(String legalCareModeIdentifier, Patient patient) {
        LegalCareModeQuery query = new LegalCareModeQuery();
        query.identifier().eq(legalCareModeIdentifier);
        query.patient().id().eq(patient.getId());
        return query.getUniqueResult();
    }
}
