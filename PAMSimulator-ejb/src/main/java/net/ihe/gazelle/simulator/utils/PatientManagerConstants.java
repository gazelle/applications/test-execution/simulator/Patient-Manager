package net.ihe.gazelle.simulator.utils;

/**
 * <p>PatientManagerConstants class.</p>
 *
 * @author abe
 * @version 1.0: 07/12/17
 */

public final class PatientManagerConstants {

    public static final String PAT_IDENTITY_X_REF_MGR = "PAT_IDENTITY_X_REF_MGR";
    public static final String CONNECTATHON = "CONNECTATHON";
    public static final String PRPA_IN_201304_UV_02 = "PRPA_IN201304UV02";
    public static final String PRPA_IN_201302_UV_02 = "PRPA_IN201302UV02";
    public static final String PRPA_IN_201310_UV_02 = "PRPA_IN201310UV02";
    public static final String ACCEPT_ACK_MESSAGE_TYPE = "MCCI_IN000002UV01";
    public static final String PRPA_IN_201301_UV_02 = "PRPA_IN201301UV02";
    public static final String PRPA_IN_201309_UV_02 = "PRPA_IN201309UV02";
    public static final String PDS = "PDS";
    public static final String PES = "PES";
    public static final String ISSUER_DEFAULT_NAME = "PatientManager";
    public static final String ITI = "ITI";
    public static final String ITI_83 = "ITI_83";
    public static final String PAT_IDENTITY_CONSUMER = "PAT_IDENTITY_CONSUMER";
    public static final String APPLICATION_URL_PREFERENCE = "application_url";
    public static final String PDC = "PDC";
    public static final String ITI_30 = "ITI-30";
    public static final String MCCI_IN_000002_UV_01 = "MCCI_IN000002UV01";
    public static final String QUQI_IN_000003_UV_01 = "QUQI_IN000003UV01";
    public static final String PRPA_IN_201306_UV_02 = "PRPA_IN201306UV02";
    public static final String QUQI_IN_000003_UV_01_CANCEL = "QUQI_IN000003UV01_Cancel";
    public static final String MCCI_IN_00002_UV_01 = "MCCI_IN00002UV01";
    public static final String ITI_FR = "ITI-FR";
    public static final String FRA = "FRA";
    public static final String PAM_FR_VERSION = "2.10";
    public static final String PAT_IDENTITY_SRC = "PAT_IDENTITY_SRC";
    public static final String SENDING_APPLICATION_PREF = "sending_application";
    public static final String SENDING_FACILITY_PREF = "sending_facility";
    public static final String ITI21 = "ITI-21";
    public static final String ITI22 = "ITI-22";
    public static final String ITI_78 = "ITI-78";
    public static final String ITI_47 = "ITI-47";
    public static final String PDQV_3_PDS_URL_PREF = "pdqv3_pds_url";
    public static final String HL_7_V_3_PDQ_PDS_DEVICE_ID_PREF = "hl7v3_pdq_pds_device_id";
    public static final String HL7V3_ORGANIZATION_OID_PREF = "hl7v3_organization_oid";
    public static final String INIT_GW = "INIT_GW";
    public static final String DOCTOR = "DOCTOR";
    public static final String BED = "BED";
    public static final String FACILITY = "FACILITY";
    public static final String ROOM = "ROOM";
    public static final String POINT_OF_CARE = "POINT_OF_CARE";
    public static final String HOSPITAL_SERVICE = "HOSPITAL_SERVICE";
    public static final String OUTPATIENT = "O";
    public static final String INPATIENT = "I";
    public static final String ADMISSION_TYPE_TABLE = "HL70007";
    public static final String NURSING_WARD = "NURSING_WARD";
    public static final String MEDICAL_WARD = "MEDICAL_WARD";
    public static final String INEXISTANT = "INEXISTANT";
    public static final String SOCIO_PRO_GROUP_TABLE = "HL79108";
    public static final String SOCIO_PRO_OCCUP_TABLE = "HL79107";
    public static final String XTN_USE_PHONE = "PRN";
    public static final String XTN_EQUIPMENT_PHONE = "PH";
    public static final String XTN_USE_EMAIL = "NET";
    public static final String XTN_EQUIPMENT_INTERNET = "Internet";
    public static final String LEGAL_NAME_CODE = "L";
    public static final String MOTHER_MAIDEN_NAME_CODE = "M";
    public static final String TRIGGER_CREATE = "A28";
    public static final String DISPLAY_NAME_CODE = "D";
    public static final String SURNAME_CODE = "S";

    private PatientManagerConstants(){

    }
}
