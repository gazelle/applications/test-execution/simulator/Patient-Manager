package net.ihe.gazelle.simulator.pam.iti31.model;

import net.ihe.gazelle.simulator.pam.model.Patient;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>LegalCareMode class.</p>
 * This class is used to represent the content of the ZFS segment "Mode légal de soin en psychiatrie" defined by PAM FR extension.
 * @author abe
 * @version 1.0: 26/03/19
 */

@Entity
@Name("legalCareModel")
@Table(name = "pam_fr_legal_care_mode", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_fr_legal_care_mode_sequence", sequenceName = "pam_fr_legal_care_mode_id_seq", allocationSize = 1)
public class LegalCareMode implements Serializable {

    @Id
    @GeneratedValue(generator = "pam_fr_legal_care_mode_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * ZFS-1 Set ID
     */
    @Column(name = "set_id", nullable = false)
    private Integer setId;

    /**
     * ZFS-2 Identifier of the legal care mode - cardinality [1..1]
     */
    @Column(name = "identifier", nullable = false)
    private String identifier;

    /**
     * ZFS-3 Date and time of the beginning of the care mode -  cardinality [1..1]
     */
    @Column(name = "care_start_date_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date careStartDateTime;

    /**
     * ZFS-4 Date and time of the end of the care mode - cardinality [0..1]
     */
    @Column(name = "care_end_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date careEndDateTime;

    /**
     * ZFS-5 Action of the legal care mode (INSERT, CANCEL, UPDATE) -  cardinality [1..1]
     * 3 actions are available: UPDATE, INSERT, CANCEL
     */
    @Column(name = "action", nullable = false)
    @Enumerated(EnumType.STRING)
    private ActionType action;

    /**
     * ZFS-6 Legal care mode (code only, ZFS-6-1) -  cardinality [1..1]
     */
    @Column(name = "mode")
    private String mode;

    /**
     * ZFS-7 RIM-P code for the legal care mode (code only, ZFS-7-1) -  cardinality [0..1]
     */
    @Column(name = "rimp_code")
    private String rimpCode;

    /**
     * ZFS-8 Comment -  cardinality [0..1]
     */
    @Column(name = "comment")
    @Lob
    @Type(type = "text")
    private String comment;

    /**
     * Subject of the legal care mode
     */
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    /**
     * since statuses are INSERT, UPDATE, CANCEL, an entry in the list of ZFS segments
     * shall only be sent if it is new or its status changes
     */
    @Transient
    private boolean includeInMessage = false;

    public LegalCareMode(){
        this.includeInMessage = false;
    }

    public LegalCareMode(Patient inPatient){
        this.patient = inPatient;
    }

    /**
     * Copy constructor
     * @param oldCareMode
     * @param inPatient
     */
    public LegalCareMode(LegalCareMode oldCareMode, Patient inPatient){
        this.setSetId(oldCareMode.getSetId());
        this.setPatient(inPatient);
        this.setIncludeInMessage(false);
        this.setAction(oldCareMode.getAction());
        this.setComment(oldCareMode.getComment());
        this.setIdentifier(oldCareMode.getIdentifier());
        this.setRimpCode(oldCareMode.getRimpCode());
        if (oldCareMode.getCareEndDateTime() != null) {
            this.setCareEndDateTime((Date) oldCareMode.getCareEndDateTime().clone());
        }
        this.setCareStartDateTime((Date) oldCareMode.getCareStartDateTime().clone());
        this.setMode(oldCareMode.getMode());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSetId() {
        return setId;
    }

    public void setSetId(Integer setId) {
        this.setId = setId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Date getCareStartDateTime() {
        return careStartDateTime;
    }

    public void setCareStartDateTime(Date careStartDateTime) {
        this.careStartDateTime = careStartDateTime;
    }

    public Date getCareEndDateTime() {
        return careEndDateTime;
    }

    public void setCareEndDateTime(Date careEndDateTime) {
        this.careEndDateTime = careEndDateTime;
    }

    public ActionType getAction() {
        return action;
    }

    public void setAction(ActionType action) {
        this.action = action;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getRimpCode() {
        return rimpCode;
    }

    public void setRimpCode(String rimpCode) {
        this.rimpCode = rimpCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public boolean isIncludeInMessage() {
        return includeInMessage;
    }

    public void setIncludeInMessage(boolean includeInMessage) {
        this.includeInMessage = includeInMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LegalCareMode)) {
            return false;
        }

        LegalCareMode that = (LegalCareMode) o;

        if (setId != null ? !setId.equals(that.setId) : that.setId != null) {
            return false;
        }
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) {
            return false;
        }
        if (careStartDateTime != null ? !careStartDateTime.equals(that.careStartDateTime) : that.careStartDateTime != null) {
            return false;
        }
        if (careEndDateTime != null ? !careEndDateTime.equals(that.careEndDateTime) : that.careEndDateTime != null) {
            return false;
        }
        if (action != null ? !action.equals(that.action) : that.action != null) {
            return false;
        }
        if (mode != null ? !mode.equals(that.mode) : that.mode != null) {
            return false;
        }
        if (rimpCode != null ? !rimpCode.equals(that.rimpCode) : that.rimpCode != null) {
            return false;
        }
        return comment != null ? comment.equals(that.comment) : that.comment == null;
    }

    @Override
    public int hashCode() {
        int result = setId != null ? setId.hashCode() : 0;
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        result = 31 * result + (careStartDateTime != null ? careStartDateTime.hashCode() : 0);
        result = 31 * result + (careEndDateTime != null ? careEndDateTime.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (mode != null ? mode.hashCode() : 0);
        result = 31 * result + (rimpCode != null ? rimpCode.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }
}
