package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import java.io.Serializable;

/**
 * Class description <b>ReceiverManager</b>
 *
 * This class is used as a base for the management of the GUI of the actors acting as receiver as PES and PEC
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - 2011, July 5th
 */
public abstract class ReceiverManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <p>getReceivingApplication.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReceivingApplication() {
		return ApplicationConfiguration.getValueOfVariable("receiving_application");
	}

	/**
	 * <p>getReceivingFacility.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReceivingFacility() {
		return ApplicationConfiguration.getValueOfVariable("receiving_facility");
	}

	/**
	 * <p>getSimulatorIpAddress.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSimulatorIpAddress() {
		return ApplicationConfiguration.getValueOfVariable("ip_address");
	}

	/**
	 * <p>getSimulatorPort.</p>
	 *
	 * @param applicationPreferenceName a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getSimulatorPort(String applicationPreferenceName) {
		return ApplicationConfiguration.getValueOfVariable(applicationPreferenceName);
	}

	/**
	 * Calls getSimulatorPort(String) with the appropriate property name (eg. pdc_port) and returns the response
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public abstract String getSimulatorPort();

}
