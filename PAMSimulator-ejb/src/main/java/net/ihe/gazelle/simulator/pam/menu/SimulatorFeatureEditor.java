package net.ihe.gazelle.simulator.pam.menu;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import java.io.Serializable;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("simulatorFeatureEditor")
@Scope(ScopeType.PAGE)
public class SimulatorFeatureEditor implements Serializable{

    /**
     * <p>getSimulatorFeatures.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<SimulatorFeature> getSimulatorFeatures(){
        return new FilterDataModel<SimulatorFeature>(getFilter()) {
            @Override
            protected Object getId(SimulatorFeature simulatorFeature) {
                return simulatorFeature.getId();
            }
        };
    }

    private Filter<SimulatorFeature> getFilter() {
        SimulatorFeatureQuery query = new SimulatorFeatureQuery();
        return new Filter<SimulatorFeature>(query.getHQLCriterionsForFilter());
    }

    /**
     * <p>saveFeature.</p>
     *
     * @param inFeature a {@link net.ihe.gazelle.simulator.pam.menu.SimulatorFeature} object.
     */
    public void saveFeature(SimulatorFeature inFeature){
        SimulatorFeatureDAO.save(inFeature);
        String newValue = (inFeature.isEnabled() ? "enabled" : "disabled");
        FacesMessages.instance().add(inFeature.getLabel() + " is now " + newValue);
        MainMenu.refreshMenu();
    }
}
