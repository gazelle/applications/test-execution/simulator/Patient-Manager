package net.ihe.gazelle.simulator.pdqm.pdc;

import net.ihe.gazelle.simulator.pdqm.pdc.exception.ConnectionException;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;

import java.net.MalformedURLException;

/**
 * PDC Messaging Client Interface. This defines methods for the simulation of the PDC actor of the PDQm profile.
 */
public interface PDCMessagingClient {

    /**
     * Send an ITI-78 query request to an SUT.
     *
     * @param id          id of the Patient to query.
     * @param sutEndpoint endpoint of the system under test.
     * @return the literal representation of the record identifier corresponding to the simulated transaction.
     * @throws SimulationException if the simulation has encountered an error.
     * @throws ConnectionException if the Connector for the simulation could not be reached.
     */
    String query(String id, String sutEndpoint) throws SimulationException, ConnectionException, MalformedURLException;

    /**
     * Send an ITI-78 retrieve request to an SUT.
     *
     * @param id          id of the Patient to retrieve.
     * @param sutEndpoint endpoint of the system under test.
     * @return the literal representation of the record identifier corresponding to the simulated transaction.
     * @throws SimulationException if the simulation has encountered an error.
     * @throws ConnectionException if the Connector for the simulation could not be reached.
     */
    String retrieve(String id, String sutEndpoint) throws SimulationException, ConnectionException, MalformedURLException;
}
