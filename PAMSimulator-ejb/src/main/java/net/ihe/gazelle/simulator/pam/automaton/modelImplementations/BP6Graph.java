package net.ihe.gazelle.simulator.pam.automaton.modelImplementations;


import com.google.code.scriptengines.js.javascript.RhinoScriptEngineFactory;
import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.GraphWalker;

import javax.script.ScriptEngineManager;

/**
 * Created by xfs on 19/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */

@GraphWalker
public class BP6Graph extends ExecutionContext implements IBP6Graph{

    static {
        ScriptEngineManager sem = new ScriptEngineManager(null);
        RhinoScriptEngineFactory rhinoScriptEngineFactory = new RhinoScriptEngineFactory();
        sem.registerEngineExtension("JavaScript", rhinoScriptEngineFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void v_Pre_Admis_Sceance() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Pre_Admis_Hospitalisation() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Consultant_Externe() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Consultant_Urgences() {

    }

    /** {@inheritDoc} */
    @Override
    public void A53() {

    }

    /** {@inheritDoc} */
    @Override
    public void A52() {

    }

    /** {@inheritDoc} */
    @Override
    public void Z99() {

    }

    /** {@inheritDoc} */
    @Override
    public void A07() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Hospitalise() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Absence_Temporaire() {

    }

    /** {@inheritDoc} */
    @Override
    public void A11() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Pas_de_venue_courante() {

    }

    /** {@inheritDoc} */
    @Override
    public void A01() {

    }

    /** {@inheritDoc} */
    @Override
    public void ini() {

    }

    /** {@inheritDoc} */
    @Override
    public void A13() {

    }

    /** {@inheritDoc} */
    @Override
    public void A38() {

    }

    /** {@inheritDoc} */
    @Override
    public void A06() {

    }

    /** {@inheritDoc} */
    @Override
    public void A05() {

    }

    /** {@inheritDoc} */
    @Override
    public void A04() {

    }

    /** {@inheritDoc} */
    @Override
    public void A22() {

    }

    /** {@inheritDoc} */
    @Override
    public void A21() {

    }

    /** {@inheritDoc} */
    @Override
    public void A03() {

    }

    /** {@inheritDoc} */
    @Override
    public void v_Pre_Admis_Consultation_Externe() {

    }
}

