package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.utils.Pair;
import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.springframework.remoting.soap.SoapFaultException;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 16/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("addPatientRecordBean")
@Scope(ScopeType.PAGE)
public class AddPatientRecord extends PatientIdentitySource {

	private static Logger log = Logger.getLogger(AddPatientRecord.class);

	private boolean displayEditPatientPanel;
	private Integer idForUpdate;

	// id management
	private String newPatientIdentifier;
	private List<Pair<Integer, String>> patientIdentifiers;

	/** {@inheritDoc} */
	@Override
	public void createNewRelationship() {
		setCurrentRelationship(new Person(selectedPatient));
	}

	/** {@inheritDoc} */
	@Override
	public void saveRelationship() {
		selectedPatient.addPersonalRelationship(getCurrentRelationship());
		setCurrentRelationship(null);
	}

	/** {@inheritDoc} */
	@Override
	public void setDisplayDDSPanel(boolean displayDDSPanel) {
		this.displayDDSPanel = displayDDSPanel;
	}

	/**
	 * <p>getDisplayDDSPanel.</p>
	 *
	 * @return a boolean.
	 */
	public boolean getDisplayDDSPanel() {
		return displayDDSPanel;
	}

	/**
	 * <p>Setter for the field <code>displayEditPatientPanel</code>.</p>
	 *
	 * @param displayEditPatientPanel a boolean.
	 */
	public void setDisplayEditPatientPanel(boolean displayEditPatientPanel) {
		this.displayEditPatientPanel = displayEditPatientPanel;
		if (displayEditPatientPanel == true) {
			generatePatientIdentifiersList();
		}
	}

	/**
	 * <p>Getter for the field <code>displayEditPatientPanel</code>.</p>
	 *
	 * @return a boolean.
	 */
	public boolean getDisplayEditPatientPanel() {
		return displayEditPatientPanel;
	}


	/**
	 * <p>Getter for the field <code>newPatientIdentifier</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getNewPatientIdentifier() {
		return newPatientIdentifier;
	}

	/**
	 * <p>Setter for the field <code>newPatientIdentifier</code>.</p>
	 *
	 * @param newPatientIdentifier a {@link java.lang.String} object.
	 */
	public void setNewPatientIdentifier(String newPatientIdentifier) {
		this.newPatientIdentifier = newPatientIdentifier;
	}

	/**
	 * <p>Setter for the field <code>patientIdentifiers</code>.</p>
	 *
	 * @param patientIdentifiers a {@link java.util.List} object.
	 */
	public void setPatientIdentifiers(List<Pair<Integer, String>> patientIdentifiers) {
		this.patientIdentifiers = patientIdentifiers;
	}

	/**
	 * <p>Getter for the field <code>patientIdentifiers</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Pair<Integer, String>> getPatientIdentifiers() {
		return patientIdentifiers;
	}

	/** {@inheritDoc} */
	@Override public void sendMessage() {
		ITI44ManagerLocal manager = (ITI44ManagerLocal) Component.getInstance("iti44Manager");
		HL7v3ResponderSUTConfiguration sut = manager.getSelectedSystem();
		if (sut == null) {
			FacesMessages.instance().addFromResourceBundle("gazelle.pam.selectSUT");
			return;
		}
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		String currentUser = null;
		if (Identity.instance().isLoggedIn()) {
			currentUser = Identity.instance().getCredentials().getUsername();
		}
		if (selectedPatient.getId() == null) {
			if (selectedPatient.getPatientIdentifiers() != null) {
				List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(selectedPatient
						.getPatientIdentifiers());
				selectedPatient.setPatientIdentifiers(pidList);
			}
			selectedPatient = selectedPatient.savePatient(entityManager);
			PatientHistory history = new PatientHistory(selectedPatient, null, PatientHistory.ActionPerformed.CREATED, null,
					currentUser, null, null);
			history.save(entityManager);
		}
		ITI44MessageBuilder builder = new ITI44MessageBuilder(sut, selectedPatient, null);
		PRPAIN201301UV02Type message = builder.buildAddPatientRecordMessage();
		PatientIdentitySourceSender sender = new PatientIdentitySourceSender(sut);
		try {
			sender.sendAddPatientRecord(message);
		} catch (SoapSendException e){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
			return;
		}

		messages = new ArrayList<TransactionInstance>();
		messages.add(sender.getTransactionInstance());
		PatientHistory history = new PatientHistory(selectedPatient, null, PatientHistory.ActionPerformed.SENT, "PRPA_IN201301UV02",
				currentUser, sut.getOrganizationOID(), sut.getDeviceOID());
		history.save(entityManager);
		selectedPatient = null;
		displayPatientsList = false;
	}

	/** {@inheritDoc} */
	@Override public void actionOnSelectedPatient(Patient patient) {
		selectedPatient = patient;
		sendMessage();
	}

	/**
	 * <p>generateAndSavePatient.</p>
	 */
	public void generateAndSavePatient() {
		selectedPatient = generateNewPatient(sendingActor);
		if (selectedPatient != null) {
			displayEditPatientPanel = true;
			displayDDSPanel = false;
			generatePatientIdentifiersList();
		}

	}

	/**
	 * <p>displayDDSPanel.</p>
	 */
	public void displayDDSPanel() {
		displayDDSPanel = true;
		selectedPatient = null;
	}

	private void generatePatientIdentifiersList() {
		if (selectedPatient.getPatientIdentifiers() != null) {
			patientIdentifiers = new ArrayList<Pair<Integer, String>>();
			for (int index = 0; index < selectedPatient.getPatientIdentifiers().size(); index++) {
				String fullPatientId = selectedPatient.getPatientIdentifiers().get(index).getFullPatientId();
				if (selectedPatient.getPatientIdentifiers().get(index).getIdentifierTypeCode() != null) {
					fullPatientId = fullPatientId.concat("^").concat(
							selectedPatient.getPatientIdentifiers().get(index).getIdentifierTypeCode());
				}
				patientIdentifiers.add(new Pair<Integer, String>(index, fullPatientId));
			}
		} else {
			patientIdentifiers = null;
		}
	}

	/**
	 * <p>changeIdentifier.</p>
	 *
	 * @param pidIndex a {@link java.lang.Integer} object.
	 * @param newIdString a {@link java.lang.String} object.
	 */
	public void changeIdentifier(Integer pidIndex, String newIdString) {
		String[] pidComponents = newIdString.split("\\^");
		String fullId = null;
		String newIdentifierTypeCode = null;
		if (pidComponents.length > 4) {
			int cx5Index = newIdString.lastIndexOf("^");
			fullId = newIdString.substring(0, cx5Index);
			newIdentifierTypeCode = newIdString.substring(cx5Index + 1, newIdString.length());
		} else {
			fullId = newIdString;
			newIdentifierTypeCode = null;
		}
		PatientIdentifier newIdentifier = new PatientIdentifier(fullId, newIdentifierTypeCode);
		PatientIdentifier modifiedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
		if (!newIdentifier.equals(modifiedPid)) {
			removeIdentifier(modifiedPid);
			selectedPatient.getPatientIdentifiers().add(newIdentifier);
			generatePatientIdentifiersList();
		}
	}

	/**
	 * <p>addIdentifier.</p>
	 */
	public void addIdentifier() {
		if ((newPatientIdentifier != null) && !newPatientIdentifier.isEmpty()) {
			String[] pidComponents = newPatientIdentifier.split("\\^");
			String newIdentifierTypeCode = null;
			if (pidComponents.length > 4) {
				int cx5Index = newPatientIdentifier.lastIndexOf("^");
				newIdentifierTypeCode = newPatientIdentifier.substring(cx5Index + 1, newPatientIdentifier.length());
			} else {
				newIdentifierTypeCode = null;
			}
			PatientIdentifier id = new PatientIdentifier(newPatientIdentifier, newIdentifierTypeCode);
			if (selectedPatient.getPatientIdentifiers() == null) {
				List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
				identifiers.add(id);
				selectedPatient.setPatientIdentifiers(identifiers);
			} else {
				selectedPatient.getPatientIdentifiers().add(id);
			}
			newPatientIdentifier = null;
		}
		generatePatientIdentifiersList();
	}

	/**
	 * <p>removeIdentifier.</p>
	 *
	 * @param pidIndex a {@link java.lang.Integer} object.
	 */
	public void removeIdentifier(Integer pidIndex) {
		PatientIdentifier removedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
		removeIdentifier(removedPid);
		generatePatientIdentifiersList();
	}

	private void removeIdentifier(PatientIdentifier identifier) {
		if (selectedPatient.getPatientIdentifiers().contains(identifier)) {
			selectedPatient.getPatientIdentifiers().remove(identifier);
		} else {
			log.error("this identifier is not linked to the current patient");
		}
	}

	/**
	 * <p>Getter for the field <code>idForUpdate</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getIdForUpdate() {
		return idForUpdate;
	}

	/**
	 * <p>Setter for the field <code>idForUpdate</code>.</p>
	 *
	 * @param idForUpdate a {@link java.lang.Integer} object.
	 */
	public void setIdForUpdate(Integer idForUpdate) {
		this.idForUpdate = idForUpdate;
	}
}
