package net.ihe.gazelle.simulator.pam.iti31.util;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;

import javax.persistence.EntityManager;

/**
 * <p>PESEventHandler interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public interface PESEventHandler {

	/**
	 * <p>setAttributes.</p>
	 *
	 * @param inEncounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 * @param inMovement a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 * @param triggerEvent a {@link java.lang.String} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 */
	public void setAttributes(Encounter inEncounter, Movement inMovement, String triggerEvent, Actor simulatedActor,
                              EntityManager entityManager);

	/**
	 * <p>processInsert.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 */
	public Movement processInsert();

	/**
	 * <p>processCancel.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 */
	public Movement processCancel();

}
