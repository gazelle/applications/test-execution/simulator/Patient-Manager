package net.ihe.gazelle.simulator.pam.pds;

import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.PatientSupplierManager;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

/**
 * <p>Abstract PDS class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class PDS extends PatientSupplierManager {

	protected String affinityDomainKeyword;
	protected boolean canSendMessage = true;

	/**
	 * <p>getInstanceOfITI30Manager.</p>
	 *
	 * @return the session scoped instance of ITI30Manager
	 */
	public ITI30ManagerLocal getInstanceOfITI30Manager(){
		return (ITI30ManagerLocal) Component.getInstance("iti30Manager");
	}


	/**
	 * Called at bean creation
	 */
	@Create
	public void initializePage() {
		super.initialize();
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		affinityDomainKeyword = urlParams.get("afdomain");
		sendingActor = Actor.findActorWithKeyword(urlParams.get("initiator"));
		simulatedTransaction = Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_30);
		availableSystems = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(PatientManagerConstants.ITI_30, HL7Protocol.MLLP);
		if (sendingActor == null) {
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"initiator parameter is missing from the URL, default behaviour is PAM/PDS");
			sendingActor = Actor.findActorWithKeyword(PatientManagerConstants.PDS);
			receivingActor = Actor.findActorWithKeyword(PatientManagerConstants.PDC);
		} else if (sendingActor.getKeyword().equals(PatientManagerConstants.PDS)) {
			receivingActor = Actor.findActorWithKeyword(PatientManagerConstants.PDC);
		} else if (sendingActor.getKeyword().equals(PatientManagerConstants.PAT_IDENTITY_SRC)) {
			receivingActor = Actor.findActorWithKeyword(PatientManagerConstants.PAT_IDENTITY_X_REF_MGR);
		}
		sendingApplication = ApplicationConfiguration.getValueOfVariable(PatientManagerConstants.SENDING_APPLICATION_PREF);
		sendingFacility = ApplicationConfiguration.getValueOfVariable(PatientManagerConstants.SENDING_FACILITY_PREF);
		selectedPatient = null;
		secondaryPatient = null;
		hl7Messages = null;
		updateSelectedEvent();
	}

	/**
	 * <p>updateSelectedEvent.</p>
	 */
	protected abstract void updateSelectedEvent();

	@Override
	public void sendMessage() {
		try {
			if (selectedSUT == null) {
				throw new SendindException("Please select a system under test and retry");
			}
			hl7Messages = send();
		} catch (Exception e) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
		}
	}

	protected abstract List<TransactionInstance> send();


	/**
	 * <p>performAnotherTest.</p>
	 */
	public void performAnotherTest(){
		switchPatient();
		displayDDSPanel = false;
	}


	/**
	 * <p>switchPatient.</p>
	 */
	public void switchPatient() {
		displayPatientsList = true;
		hl7Messages = null;
		selectedPatient = null;
		secondaryPatient = null;
	}

	/**
	 * <p>actionOnSelectedPatient.</p>
	 *
	 * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public abstract void actionOnSelectedPatient(Patient patient);



	public boolean isCanSendMessage() {
		return canSendMessage;
	}

	public void setCanSendMessage(boolean canSendMessage) {
		this.canSendMessage = canSendMessage;
	}

	public boolean isProfileDedicated() {
		return false;
	}

	protected Class<? extends HL7Domain<?>> getHL7DomainClass() {
		return getSendingActor() != null && PatientManagerConstants.PDS.equals(getSendingActor().getKeyword()) && getInstanceOfITI30Manager().isBp6Mode()
				? BP6HL7Domain.class
				: IHEHL7Domain.class;
	}


}
