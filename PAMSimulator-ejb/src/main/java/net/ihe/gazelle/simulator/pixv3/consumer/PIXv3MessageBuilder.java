package net.ihe.gazelle.simulator.pixv3.consumer;

import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02QUQIMT021001UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02DataSource;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02PatientIdentifier;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.initiator.MCCIMT000100UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>PIXv3MessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PIXv3MessageBuilder extends HL7v3MessageBuilder {

	private static Logger log = LoggerFactory.getLogger(PIXv3MessageBuilder.class);

	private HL7v3ResponderSUTConfiguration sut;
	private List<AssigningAuthority> otherIDsScopingOrganizations;
	private MessageIdGenerator generator = null;
	private II queryId = null;
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
	private PatientIdentifier patientId;
	private II targetMessageId = null;

	/**
	 * <p>Getter for the field <code>sut</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public HL7v3ResponderSUTConfiguration getSut() {
		return sut;
	}

	/**
	 * <p>Setter for the field <code>sut</code>.</p>
	 *
	 * @param sut a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public void setSut(HL7v3ResponderSUTConfiguration sut) {
		this.sut = sut;
	}

	/**
	 * <p>Getter for the field <code>otherIDsScopingOrganizations</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<AssigningAuthority> getOtherIDsScopingOrganizations() {
		return otherIDsScopingOrganizations;
	}

	/**
	 * <p>Setter for the field <code>otherIDsScopingOrganizations</code>.</p>
	 *
	 * @param otherIDsScopingOrganizations a {@link java.util.List} object.
	 */
	public void setOtherIDsScopingOrganizations(List<AssigningAuthority> otherIDsScopingOrganizations) {
		this.otherIDsScopingOrganizations = otherIDsScopingOrganizations;
	}

	/**
	 * <p>Getter for the field <code>generator</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator} object.
	 */
	public MessageIdGenerator getGenerator() {
		return generator;
	}

	/**
	 * <p>Setter for the field <code>generator</code>.</p>
	 *
	 * @param generator a {@link net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator} object.
	 */
	public void setGenerator(MessageIdGenerator generator) {
		this.generator = generator;
	}

	/**
	 * <p>Getter for the field <code>queryId</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 */
	public II getQueryId() {
		return queryId;
	}

	/**
	 * <p>Setter for the field <code>queryId</code>.</p>
	 *
	 * @param queryId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 */
	public void setQueryId(II queryId) {
		this.queryId = queryId;
	}

	/**
	 * <p>Getter for the field <code>sdfDateTime</code>.</p>
	 *
	 * @return a {@link java.text.SimpleDateFormat} object.
	 */
	public SimpleDateFormat getSdfDateTime() {
		return sdfDateTime;
	}

	/**
	 * <p>Setter for the field <code>sdfDateTime</code>.</p>
	 *
	 * @param sdfDateTime a {@link java.text.SimpleDateFormat} object.
	 */
	public void setSdfDateTime(SimpleDateFormat sdfDateTime) {
		this.sdfDateTime = sdfDateTime;
	}

	/**
	 * <p>Getter for the field <code>patientId</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public PatientIdentifier getPatientId() {
		return patientId;
	}

	/**
	 * <p>Setter for the field <code>patientId</code>.</p>
	 *
	 * @param patientId a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public void setPatientId(PatientIdentifier patientId) {
		this.patientId = patientId;
	}

	/**
	 * <p>Getter for the field <code>targetMessageId</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 */
	public II getTargetMessageId() {
		return targetMessageId;
	}

	/**
	 * <p>Setter for the field <code>targetMessageId</code>.</p>
	 *
	 * @param targetMessageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
	 */
	public void setTargetMessageId(II targetMessageId) {
		this.targetMessageId = targetMessageId;
	}

	/**
	 * <p>Constructor for PIXv3MessageBuilder.</p>
	 *
	 * @param searchedPid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 * @param selectedSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 * @param whatDomainsReturned a {@link java.util.List} object.
	 */
	public PIXv3MessageBuilder(PatientIdentifier searchedPid, HL7v3ResponderSUTConfiguration selectedSUT,
			List<AssigningAuthority> whatDomainsReturned) {
		this.patientId = searchedPid;
		this.sut = selectedSUT;
		this.otherIDsScopingOrganizations = whatDomainsReturned;
	}

	/**
	 * <p>buildPatientRegistryGetIdentifierQuery.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type} object.
	 */
	public PRPAIN201309UV02Type buildPatientRegistryGetIdentifierQuery() {

		PRPAIN201309UV02QUQIMT021001UV01ControlActProcess controlActProcess = new PRPAIN201309UV02QUQIMT021001UV01ControlActProcess();
		controlActProcess.setClassCode(ActClassControlAct.CACT);
		controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
		controlActProcess.setCode(new CD("PRPA_TE201309UV02", INTERACTION_ID_NAMESPACE, null));

		PRPAIN201309UV02Type request = new PRPAIN201309UV02Type();
		request.setITSVersion(VERSION);

		generator = MessageIdGenerator.getGeneratorForActor("PAT_IDENTITY_CONSUMER");
		if (generator != null) {
			controlActProcess.setQueryByParameter(buildQueryByParameter(generator));
			request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
			request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid()));
		} else {
			log.error("No MessageIdGenerator instance found for actor PAT_IDENTITY_CONSUMER");
			controlActProcess.setQueryByParameter(buildQueryByParameter(null));
			request.setSender(MCCIMT000100UV01PartBuilder
					.buildSender((PreferenceService.getString("hl7v3_pix_consumer_device_id"))));
		}
		request.setCreationTime(new TS(sdfDateTime.format(new Date())));
		request.setInteractionId(new II(INTERACTION_ID_NAMESPACE, "PRPA_IN201309UV02"));
		request.setProcessingCode(new CS("T", null, null));
		request.setProcessingModeCode(new CS("T", null, null));
		request.setAcceptAckCode(new CS("AL", null, null));
		request.addReceiver(MCCIMT000100UV01PartBuilder.buildReceiver(sut));

		request.setControlActProcess(controlActProcess);
		return request;
	}

	private PRPAMT201307UV02QueryByParameter buildQueryByParameter(MessageIdGenerator generator) {
		PRPAMT201307UV02QueryByParameter queryByParameter = new PRPAMT201307UV02QueryByParameter();
		if (generator != null) {
			queryId = new II(generator.getOidForQuery(), generator.getNextId());
			queryByParameter.setQueryId(queryId);
		} else {
			log.error("queryId cannot be generated because no generator has been found");
		}
		queryByParameter.setStatusCode(new CS("new", null, null));
		queryByParameter.setResponsePriorityCode(new CS("I", null, null));
		queryByParameter.setParameterList(buildParameterList());
		return queryByParameter;
	}

	private PRPAMT201307UV02ParameterList buildParameterList() {
		PRPAMT201307UV02ParameterList parameterList = new PRPAMT201307UV02ParameterList();
		if (patientId != null) {
			PRPAMT201307UV02PatientIdentifier patientIdentifier = new PRPAMT201307UV02PatientIdentifier();
			String extension = patientId.getFullPatientId();
			String root = patientId.getDomain().getUniversalID();
			II ii = new II();
			ii.setRoot(root);
			ii.setExtension(extension);
			patientIdentifier.addValue(ii);
			patientIdentifier.setSemanticsText(buildST("Patient.id"));
			parameterList.addPatientIdentifier(patientIdentifier);

		}
		if (otherIDsScopingOrganizations != null && !otherIDsScopingOrganizations.isEmpty()) {
			for (AssigningAuthority authority : otherIDsScopingOrganizations) {
				PRPAMT201307UV02DataSource dataSource = new PRPAMT201307UV02DataSource();
				dataSource.addValue(new II(authority.getUniversalId(), null));
				dataSource.setSemanticsText(buildST("DataSource.id"));
				parameterList.addDataSource(dataSource);
			}
		}
		return parameterList;
	}
}
