package net.ihe.gazelle.simulator.pixv3.source;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.PatientSelectorManager;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.List;

/**
 * Created by aberge on 16/03/15.
 */
public abstract class PatientIdentitySource extends PatientSelectorManager {

	protected Actor sendingActor;
	protected Actor receivingActor;
	protected Transaction simulatedTransaction;
	protected String senderDeviceId;
	protected String organizationOid;
	protected Patient selectedPatient; // correctPatient for merge
	protected Patient secondaryPatient; // incorrect Patient for merge and
	protected transient List<TransactionInstance> messages;

	@Create
	public void initializePage() {
		sendingActor = Actor.findActorWithKeyword("PAT_IDENTITY_SRC");
		simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-44");
		receivingActor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
		super.initialize();
		senderDeviceId = ApplicationConfiguration.getValueOfVariable("hl7v3_pix_src_device_id");
		organizationOid = ApplicationConfiguration.getValueOfVariable("hl7v3_organization_oid");
		selectedPatient = null;
		secondaryPatient = null;
		messages = null;
	}

	public void performAnotherTest(){
		selectedPatient = null;
		secondaryPatient = null;
		messages = null;
		switchToSelectionMode();
	}

	/**
	 * Builds and sends the message
	 */
	public abstract void sendMessage();

	/**
	 *
	 */
	public void switchPatient() {
		messages = null;
		selectedPatient = null;
		secondaryPatient = null;
	}

	/**
	 *
	 */
	public abstract void actionOnSelectedPatient(Patient patient);

	public List<TransactionInstance> getMessages() {
		return messages;
	}

	public String getOrganizationOid() {
		return organizationOid;
	}

	public Actor getReceivingActor() {
		return receivingActor;
	}

	public String getSenderDeviceId() {
		return senderDeviceId;
	}

	public Transaction getSimulatedTransaction() {
		return simulatedTransaction;
	}

	public Actor getSendingActor() {
		return sendingActor;
	}

	public Patient getSelectedPatient() {
		return selectedPatient;
	}

	public void setSelectedPatient(Patient selectedPatient) {
		this.selectedPatient = selectedPatient;
	}

	public Patient getSecondaryPatient() {
		return secondaryPatient;
	}

	public void setSecondaryPatient(Patient secondaryPatient) {
		this.secondaryPatient = secondaryPatient;
	}

	public  void createNewPhoneNumber(){
		this.newPhoneNumber = new PatientPhoneNumber();
		newPhoneNumber.setPatient(selectedPatient);
	}

	public void addNewPhoneNumber(){
		if (this.newPhoneNumber.getValue() == null || this.newPhoneNumber.getValue().isEmpty()){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Phone number shall have a value");
		} else {
			selectedPatient.addPhoneNumber(newPhoneNumber);
			newPhoneNumber = null;
		}
	}

	protected Class<? extends HL7Domain<?>> getHL7DomainClass() {
		return IHEHL7Domain.class;
	}

}
