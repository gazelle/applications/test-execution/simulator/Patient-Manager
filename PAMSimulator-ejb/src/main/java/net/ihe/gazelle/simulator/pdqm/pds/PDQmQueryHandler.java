package net.ihe.gazelle.simulator.pdqm.pds;

import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.StringOrListParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.responder.IHEQueryParserCode;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.exceptions.FHIRException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>PDQmQueryHandler class.</p>
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */

public class PDQmQueryHandler {


    public PDQmQueryHandler() {
        this.unknownDomains = new ArrayList<String>();
    }

    private List<Patient> returnedPatients;

    public List<Patient> getReturnedPatients() {
        return returnedPatients;
    }

    public List<String> getUnknownDomains() {
        return unknownDomains;
    }

    private List<String> unknownDomains;



    public IHEQueryParserCode executeQuery(PDQmQueryParameters queryParameters) {
        // extract what domains returned list
        List<String> domainsOid = new ArrayList<String>();
        // extract queried patient identifiers
        List<TokenParam> flatList = new ArrayList<TokenParam>();
        TokenAndListParam identifierList = queryParameters.getIdentifierList();
        if (identifierList != null && identifierList.getValuesAsQueryTokens() != null) {
            for (TokenOrListParam orListParams : queryParameters.getIdentifierList().getValuesAsQueryTokens()) {
                flatList.addAll(orListParams.getValuesAsQueryTokens());
            }
        }
        List<TokenParam> identifiersToQueryOn = new ArrayList<TokenParam>();
        if (!flatList.isEmpty()) {
            for (TokenParam tokenParam : flatList) {
                String identifierValue = tokenParam.getValue();
                if (identifierValue == null || identifierValue.isEmpty()) {
                    domainsOid.add(tokenParam.getSystem());
                } else if (identifierValue.contains("urn:oid")) {
                    String[] urns = identifierValue.split(",");
                    for (String urn : urns) {
                        domainsOid.add(StringUtils.removeEnd(urn, "|"));
                    }
                } else {
                    // this is a identifier or system|identifier value and we need to include it in the query
                    identifiersToQueryOn.add(tokenParam);
                }
            }
        }

        // check what domains returned are known by the PDS
        if (!domainsOid.isEmpty()) {
            for (String universalId : domainsOid) {
                if (HierarchicDesignatorDAO.domainExists(PatientIdentifierDAO.urnToOid(universalId), DesignatorType.PATIENT_ID)) {
                    continue;
                } else {
                    unknownDomains.add(universalId);
                }
            }
        }

        if (!unknownDomains.isEmpty()) {
            return IHEQueryParserCode.UNKNOWN_DOMAINS;
        } else {
            PatientQuery query = getPatientQueryBuild(queryParameters, identifiersToQueryOn, domainsOid);
            if (query == null){
                return IHEQueryParserCode.NO_PATIENT_FOUND;
            } else {
                query.id().order(true);
                returnedPatients = query.getList();
                if (returnedPatients.isEmpty()) {
                    return IHEQueryParserCode.NO_PATIENT_FOUND;
                } else {
                    return IHEQueryParserCode.RETURN_ALL_PATIENTS;
                }
            }
        }
    }

    /**
     * PAM-509: We want the PDS actor to look into Connectathon demographics as well
     * @return the list of actor's keywords for which we want to return patients
     */
    private static List<String> getActorKeywords() {
        return Arrays.asList(PatientManagerConstants.PDS, PatientManagerConstants.CONNECTATHON);
    }

    private static PatientQuery getPatientQueryBuild(PDQmQueryParameters queryParameters,
                                                     List<TokenParam> patientIdentifiers,
                                                     List<String> restrictionsOnDomain) {

        PatientQuery patientQuery = new PatientQuery();
        patientQuery.simulatedActor().keyword().in(getActorKeywords());

        queryOnResourceId(queryParameters, patientQuery);

        queryOnStillActive(queryParameters, patientQuery);

        queryOnFamilyNames(queryParameters, patientQuery);

        queryOnGivenNames(queryParameters, patientQuery);

        queryOnBirthdate(queryParameters, patientQuery);

        queryOnCity(queryParameters, patientQuery);

        queryOnCountry(queryParameters, patientQuery);

        queryOnPostalCode(queryParameters, patientQuery);

        queryOnState(queryParameters, patientQuery);

        try {
            queryOnGender(queryParameters, patientQuery);
        }catch (FHIRException e){

            return null;
        }

        queryOnTelecom(queryParameters, patientQuery);

        queryOnAddressPart(queryParameters, patientQuery);

        queryOnPatientIdentifiers(patientIdentifiers, patientQuery, restrictionsOnDomain);

        queryOnMotherMaidenFamilyName(queryParameters, patientQuery);

        return patientQuery;
    }

    private static void queryOnMotherMaidenFamilyName(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam mothersMaidenName = queryParameters.getMotherMaidenFamilyName();
        if (mothersMaidenName != null && !mothersMaidenName.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(mothersMaidenName);
            patientQuery.motherMaidenName().like(mothersMaidenName.getValue(), likeMatchMode);
        }
    }

    private static void queryOnResourceId(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam id = queryParameters.getId();
        if (id != null && !id.isEmpty()) {
            patientQuery.id().eq(Integer.parseInt(id.getValue()));
        }
    }

    private static void queryOnStillActive(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        TokenParam active = queryParameters.getActive();
        if (active != null && !active.isEmpty()) {
            boolean filterOnActive = Boolean.parseBoolean(active.getValue());
            patientQuery.stillActive().eq(filterOnActive);
        } else {
            patientQuery.stillActive().eq(true);
        }
    }

    private static void queryOnPatientIdentifiers(List<TokenParam> patientIdentifiers, PatientQuery patientQuery, List<String> restrictionsOnDomain) {
        if (!patientIdentifiers.isEmpty()) {
            for (TokenParam entry : patientIdentifiers) {
                String universalId = PatientIdentifierDAO.urnToOid(entry.getSystem());
                String idNumber = entry.getValue();
                if (universalId != null && idNumber != null) {
                    PatientQuery intermediateQuery = new PatientQuery();
                    patientQuery.addRestriction(HQLRestrictions.and(
                            intermediateQuery.patientIdentifiers().idNumber().eqRestriction(idNumber),
                            intermediateQuery.patientIdentifiers().domain().universalID().eqRestriction(universalId)));
                } else {
                    patientQuery.patientIdentifiers().idNumber().eq(idNumber);
                }
            }
        }
        if (!restrictionsOnDomain.isEmpty()) {
            for (String domain : restrictionsOnDomain) {
                String universalId = PatientIdentifierDAO.urnToOid(domain);
                patientQuery.patientIdentifiers().domain().universalID().eq(universalId);
            }
        }
    }

    private static void queryOnAddressPart(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam address = queryParameters.getAddress();
        if (address != null && !address.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(address);
            patientQuery.addRestriction(HQLRestrictions.or(
                    patientQuery.addressList().street().likeRestriction(address.getValue(), likeMatchMode),
                    patientQuery.addressList().city().likeRestriction(address.getValue(), likeMatchMode),
                    patientQuery.addressList().countryCode().likeRestriction(address.getValue(), likeMatchMode),
                    patientQuery.addressList().zipCode().likeRestriction(address.getValue(), likeMatchMode),
                    patientQuery.addressList().state().likeRestriction(address.getValue(), likeMatchMode)
            ));
        }
    }

    private static void queryOnTelecom(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        TokenParam telecom = queryParameters.getTelecom();
        if (telecom != null && !telecom.isEmpty()) {
            patientQuery.phoneNumbers().value().eq(telecom.getValue());
        }
    }

    private static void queryOnGender(PDQmQueryParameters queryParameters, PatientQuery patientQuery) throws FHIRException {
        TokenParam gender = queryParameters.getGender();
        if (gender != null && !gender.isEmpty()) {
            Enumerations.AdministrativeGender genderCode = Enumerations.AdministrativeGender.fromCode(gender.getValue());
            String hl7Code = getGenderAsHL7Code(genderCode);
            if (hl7Code != null) {
                patientQuery.genderCode().eq(hl7Code);
            } else {
                throw new FHIRException(gender.getValue() + " is not a valid Administrative Gender code");
            }
        }
    }

    private static String getGenderAsHL7Code(Enumerations.AdministrativeGender gender) {
        switch (gender) {
            case MALE:
                return "M";
            case FEMALE:
                return "F";
            case OTHER:
                return "O";
            case UNKNOWN:
                return "U";
            default:
                return null;
        }
    }

    private static void queryOnState(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam addressState = queryParameters.getAddressState();
        if (addressState != null && !addressState.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(addressState);
            patientQuery.addressList().state().like(addressState.getValue(), likeMatchMode);
        }
    }

    private static void queryOnPostalCode(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam addressPostalcode = queryParameters.getAddressPostalcode();
        if (addressPostalcode != null && !addressPostalcode.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(addressPostalcode);
            patientQuery.addressList().zipCode().like(addressPostalcode.getValue(), likeMatchMode);
        }
    }

    private static void queryOnCountry(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam addressCountry = queryParameters.getAddressCountry();
        if (addressCountry != null && !addressCountry.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(addressCountry);
            patientQuery.addressList().countryCode().like(addressCountry.getValue(), likeMatchMode);
        }
    }

    private static void queryOnCity(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        StringParam addressCity = queryParameters.getAddressCity();
        if (addressCity != null && !addressCity.isEmpty()) {
            HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(addressCity);
            patientQuery.addressList().city().like(addressCity.getValue(), likeMatchMode);
        }
    }

    private static void queryOnBirthdate(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        DateParam birthdate = queryParameters.getBirthdate();
        if (birthdate != null && !birthdate.isEmpty()) {
            Date birthTimeValue = birthdate.getValue();
            Calendar begin = Calendar.getInstance();
            begin.setTime(birthTimeValue);
            begin.set(Calendar.HOUR_OF_DAY, 0);
            begin.set(Calendar.MINUTE, 0);
            begin.set(Calendar.SECOND, 0);
            Calendar end = Calendar.getInstance();
            end.setTime(birthTimeValue);
            end.set(Calendar.HOUR_OF_DAY, 23);
            end.set(Calendar.MINUTE, 59);
            end.set(Calendar.SECOND, 59);
            patientQuery.dateOfBirth().ge(begin.getTime());
            patientQuery.dateOfBirth().le(end.getTime());
        }
    }

    private static void queryOnGivenNames(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        List<StringParam> givenNames = toListOfStringParam(queryParameters.getGivenNames());
        if (givenNames != null && !givenNames.isEmpty()) {
            // 3.78.4.1.2.1: repetition of each of the given parameters are interpreted to mean multiple parts of the same name
            for (StringParam given : givenNames) {
                HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(given);
                if (likeMatchMode == null) {
                    continue;
                } else {
                    String queryValue = given.getValue();
                    PatientQuery intermediateQuery = new PatientQuery();
                    patientQuery.addRestriction(HQLRestrictions.or(intermediateQuery.firstName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.alternateFirstName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.secondName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.thirdName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.alternateSecondName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.alternateThirdName().likeRestriction(queryValue, likeMatchMode)));
                }
            }
        }
    }

    private static void queryOnFamilyNames(PDQmQueryParameters queryParameters, PatientQuery patientQuery) {
        List<StringParam> familyNames = toListOfStringParam(queryParameters.getFamilyNames());
        if (familyNames != null && !familyNames.isEmpty()) {
            // 3.78.4.1.2.1: repetition of each of the family parameters are interpreted to mean multiple parts of the same name
            for (StringParam family : familyNames) {
                HQLRestrictionLikeMatchMode likeMatchMode = getLikeMatchMode(family);
                if (likeMatchMode == null) {
                    continue;
                } else {
                    String queryValue = family.getValue();
                    PatientQuery intermediateQuery = new PatientQuery();
                    patientQuery.addRestriction(HQLRestrictions.or(intermediateQuery.lastName().likeRestriction(queryValue, likeMatchMode),
                            intermediateQuery.alternateLastName().likeRestriction(queryValue, likeMatchMode)));
                }
            }
        }
    }

    private static List<StringParam> toListOfStringParam(StringAndListParam listParam) {
        if (listParam == null) {
            return null;
        } else {
            List<StringParam> params = new ArrayList<StringParam>();
            for (StringOrListParam orListParam : listParam.getValuesAsQueryTokens()) {
                params.addAll(orListParam.getValuesAsQueryTokens());
            }
            return params;
        }
    }

    private static HQLRestrictionLikeMatchMode getLikeMatchMode(StringParam queryParam) {
        if (queryParam.isEmpty()) {
            return null;
        } else if (queryParam.isExact()) {
            // use like with HQLRestrictionLikeMathMode.EXACT to compare ignoring the case
            return HQLRestrictionLikeMatchMode.EXACT;
        } else {
            return HQLRestrictionLikeMatchMode.START;
        }
    }


    public static TransactionInstance populatePDQmTransactionInstance(EncodingEnum format, String requestContent, String responseContent, String
            callerIP, RestOperationTypeEnum requestType) {

        TransactionInstance message = new TransactionInstance();
        message.setDomain(Domain.getDomainByKeyword(PatientManagerConstants.ITI));
        message.setSimulatedActor(Actor.findActorWithKeyword(PatientManagerConstants.PDS));
        MessageInstance requestMessageInstance = new MessageInstance();
        requestMessageInstance.setContent(requestContent);
        requestMessageInstance.setIssuingActor(Actor.findActorWithKeyword(PatientManagerConstants.PDC));
        requestMessageInstance.setIssuer(callerIP);
        requestMessageInstance.setIssuerIpAddress(callerIP);
        message.setRequest(requestMessageInstance);
        MessageInstance responseMessageInstance = new MessageInstance();
        responseMessageInstance.setIssuingActor(Actor.findActorWithKeyword(PatientManagerConstants.PDS));
        if (RestOperationTypeEnum.READ.equals(requestType)) {
            requestMessageInstance.setType(FhirConstants.PDQM_RETRIEVE_REQUEST_TYPE);
            responseMessageInstance.setType(FhirConstants.PDQM_RETRIEVE_RESPONSE);
        } else {
            requestMessageInstance.setType(FhirConstants.PDQM_REQUEST_TYPE);
            responseMessageInstance.setType(FhirConstants.PDQM_RESPONSE);
        }
        if (EncodingEnum.JSON.equals(format)) {
            message.setStandard(EStandard.FHIR_JSON);
        } else {
            message.setStandard(EStandard.FHIR_XML);
        }
        responseMessageInstance.setContent(responseContent);
        responseMessageInstance.setIssuer(PatientManagerConstants.ISSUER_DEFAULT_NAME);
        message.setResponse(responseMessageInstance);
        message.setTimestamp(new Date());
        message.setTransaction(Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI_78));
        message.setVisible(true);

        return message;
    }
}
