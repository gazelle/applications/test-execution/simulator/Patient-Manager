package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * Page Bean for Message Configuration for PDQm PDC simulator.
 */
@Name("pdqmMessageConfigurationBean")
@Scope(ScopeType.PAGE)
public class PDQmMessageConfigurationBean {

    private String patientIdentifier;

    /**
     * Empty constructor for injection.
     */
    public PDQmMessageConfigurationBean() {
    }

    /**
     * Getter for the patientIdentifier property.
     *
     * @return the value of the property.
     */
    public String getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Setter for the patientIdentifier property.
     *
     * @param patientIdentifier value to set to the property.
     */
    public void setPatientIdentifier(String patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }
}
