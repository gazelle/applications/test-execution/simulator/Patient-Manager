package net.ihe.gazelle.simulator.cat.action;

import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdqm.pdc.FhirResourceParser;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.commons.io.IOUtils;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ImportPatientResourceManager class.</p>
 *
 * @author abe
 * @version 1.0: 06/12/17
 */

@Name("importPatientResourceManager")
@Scope(ScopeType.PAGE)
public class ImportPatientResourceManager implements Serializable {

    private List<net.ihe.gazelle.simulator.pam.model.Patient> importedPatients;
    private boolean failedOnError = false;
    private List<String> errorMessages;

    @Create
    public void initialize() {
        importedPatients = new ArrayList<net.ihe.gazelle.simulator.pam.model.Patient>();
    }

    public boolean displayPatientList() {
        return importedPatients.size() > 0;
    }

    public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException {
        errorMessages = new ArrayList<String>();
        File tmpFile;
        String filename = fileUploadEvent.getUploadedFile().getName();
        tmpFile = File.createTempFile("uploaded", null);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(tmpFile);
            fos.write(fileUploadEvent.getUploadedFile().getData());
            if (filename.endsWith(".xml") || filename.endsWith("json")) {
                processOneFile(tmpFile, filename);
            } else if (filename.endsWith(".zip")) {
                processZipFile(tmpFile, filename);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Only XML, JSON and ZIP extensions are allowed");
            }
        } catch (IOException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred when reading the uploaded file");
        } finally {
            IOUtils.closeQuietly(fos);
        }
    }

    private void processZipFile(File tmpFile, String filename) {
        try {
            List<String> files = ZipUtility.getListDocumentPath(tmpFile, tmpFile.getParentFile());
            ZipUtility.unzip(tmpFile, tmpFile.getParentFile());
            tmpFile.delete();
            if (files == null || files.isEmpty()) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No file found in the archive: " + filename);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "ZIP file contains " + files.size() + " files");
                for (String path : files) {
                    processOneFile(new File(path), path);
                }
            }
        } catch (IOException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    private void processOneFile(File tmpFile, String filename) {
        FileReader reader = null;
        try {
            IParser parser;
            if (filename.endsWith("xml")) {
                parser = FhirParserProvider.getXmlParser();
            } else if (filename.endsWith("json")) {
                parser = FhirParserProvider.getJsonParser();
            } else {
                FacesMessages.instance().add("Only XML and JSON files are supported");
                parser = null;
            }
            if (parser != null) {
                reader = new FileReader(tmpFile);
                IBaseResource resource = null;
                try {
                    resource = parser.parseResource(reader);
                } catch (DataFormatException e) {
                    errorMessages.add(filename + ": " + e.getMessage());
                    this.failedOnError = true;
                }
                if (resource instanceof org.hl7.fhir.dstu3.model.Patient) {
                    org.hl7.fhir.dstu3.model.Patient fhirPatient = (org.hl7.fhir.dstu3.model.Patient) resource;
                    Patient patient = FhirResourceParser.extractSinglePatient(fhirPatient);
                    savePatient(patient);
                } else if (resource instanceof Bundle) {
                    Bundle fhirBundle = (Bundle) resource;
                    List<net.ihe.gazelle.simulator.pam.model.Patient> patients = FhirResourceParser.getPatientsFromBundle(fhirBundle);
                    for (Patient patient : patients) {
                        savePatient(patient);
                    }
                }
            }
        } catch (IOException e) {
            FacesMessages.instance().add("Cannot process: " + filename);
        }
        IOUtils.closeQuietly(reader);
        tmpFile.delete();
    }

    private void savePatient(Patient patient) {
        List<PatientIdentifier> identifiers = PatientIdentifierDAO.savePatientIdentifierList(patient.getPatientIdentifiers());
        patient.setPatientIdentifiers(identifiers);
        patient.setSimulatedActor(Actor.findActorWithKeyword(PatientManagerConstants.CONNECTATHON));
        if (Identity.instance().isLoggedIn()) {
            patient.setCreator(Identity.instance().getCredentials().getUsername());
        }
        patient = patient.savePatient(EntityManagerService.provideEntityManager());
        importedPatients.add(patient);
    }

    public GazelleListDataModel<Patient> getImportedPatients() {
        return new GazelleListDataModel<Patient>(importedPatients);
    }

    public boolean isFailedOnError() {
        return failedOnError;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }
}
