package net.ihe.gazelle.simulator.pam.model;

import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.fhir.util.ToFhirResourceConverter;
import net.ihe.gazelle.simulator.hibernate.HQLRestrictionLevenshtein;
import net.ihe.gazelle.simulator.pam.dao.PersonDAO;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.EncounterQuery;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pix.model.CrossReference;
import org.hibernate.annotations.Cascade;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * <p>Patient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "patient")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("patient")
@DiscriminatorValue("pam_patient")
public class Patient extends AbstractPatient {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static int generalFakeId = 0;
    private transient int fakeId;
    private static org.slf4j.Logger log = LoggerFactory.getLogger(Patient.class);

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<PatientPhoneNumber> phoneNumbers;

    @Column(name = "creator")
    private String creator;

    @ManyToOne
    @JoinColumn(name = "actor_id")
    @NotNull
    private Actor simulatedActor;

    @Column(name = "simulated_actor_keyword")
    private String simulatedActorKeyword;

    @Column(name = "still_active")
    private Boolean stillActive;

    @Column(name = "account_number")
    private String accountNumber;

    @XmlElement(name = "identifiers")
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "pam_patient_patient_identifier", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns = @JoinColumn(name =
            "patient_identifier_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "patient_id", "patient_identifier_id"}))
    private List<PatientIdentifier> patientIdentifiers;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    @OrderBy("creationDate asc")
    private List<Encounter> encounters;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "cross_reference_id")
    private CrossReference pixReference;

    @Column(name = "blood_group")
    private String bloodGroup;

    @Column(name = "vip_indicator")
    private String vipIndicator;

    /**
     * those extra columns are used to store the double metaphone (needed for automatically cross-referencing patients)
     */
    @Column(name = "dmetaphone_first_name")
    private String dmetaphoneFirstName;

    @Column(name = "dmetaphone_last_name")
    private String dmetaphoneLastName;

    @Column(name = "dmetaphone_mother_maiden_name")
    private String dmetaphoneMotherMaidenName;

    @Column(name = "multiple_birth_indicator")
    private Boolean multipleBirthIndicator;

    @Transient
    private String multipleBirthIndicatorAsString;

    @Transient
    private String birthOrderAsString;

    @Column(name = "birth_order")
    private Integer birthOrder;

    @Column(name = "last_update_facility")
    private String lastUpdateFacility;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "additional_demographics_id")
    private AdditionalDemographics additionalDemographics;

    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "identity_reliability_code")
    private String identityReliabilityCode;

    @Column(name = "is_patient_dead")
    private boolean patientDead;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "patient_death_time")
    private Date patientDeathTime;

    @Column(name = "is_test_data")
    private boolean testData;

    @OneToMany(mappedBy = "patient", targetEntity = Person.class)
    @Cascade(org.hibernate.annotations.CascadeType.MERGE)
    private List<Person> personalRelationships;

    @Column(name = "birth_place_name")
    private String birthPlaceName;

    @OneToMany(targetEntity = LegalCareMode.class, cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    @OrderBy("setId asc")
    private List<LegalCareMode> legalCareModes;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Constructors
     */
    public Patient() {
        super();
        this.pixReference = null;
        setFakeId();
        patientDead = false;
        patientDeathTime = null;
        if (additionalDemographics == null) {
            additionalDemographics = new AdditionalDemographics(this);
        }
        this.testData = false;
        this.stillActive = true;
        assignUUID();
    }

    private void setFakeId() {
        synchronized (Patient.class) {
            fakeId = generalFakeId++;
        }
    }

    /**
     * <p>Constructor for Patient.</p>
     *
     * @param abPatient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     */
    public Patient(AbstractPatient abPatient) {
        patientDead = false;
        patientDeathTime = null;
        this.firstName = abPatient.getFirstName();
        this.lastName = abPatient.getLastName();
        this.countryCode = abPatient.getCountryCode();
        this.genderCode = abPatient.getGenderCode();
        this.religionCode = abPatient.getReligionCode();
        this.raceCode = abPatient.getRaceCode();
        this.dateOfBirth = abPatient.getDateOfBirth();
        this.creationDate = abPatient.getCreationDate();
        this.nationalPatientIdentifier = abPatient.getNationalPatientIdentifier();
        this.characterSet = abPatient.getCharacterSet();
        this.ddsIdentifier = abPatient.getDdsIdentifier();
        this.pixReference = null;
        this.alternateFirstName = abPatient.getAlternateFirstName();
        this.alternateLastName = abPatient.getAlternateLastName();
        this.secondName = abPatient.getSecondName();
        this.alternateSecondName = abPatient.getAlternateSecondName();
        this.thirdName = abPatient.getThirdName();
        this.alternateThirdName = abPatient.getAlternateThirdName();
        this.multipleBirthIndicator = false;
        this.birthOrder = null;
        this.lastUpdateFacility = "DDS";
        additionalDemographics = new AdditionalDemographics(this);
        copyAddressList(abPatient);
        this.testData = false;
        if (abPatient.getMotherMaidenName() != null && !abPatient.getMotherMaidenName().isEmpty()) {
            if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
                Person mother = PersonDAO.newMother(abPatient.getMotherMaidenName(), this);
                if (mother != null) {
                    addPersonalRelationship(mother);
                }
            } else {
                this.motherMaidenName = abPatient.getMotherMaidenName();
                this.alternateMothersMaidenName = abPatient.getAlternateMothersMaidenName();
            }
        }
        assignUUID();
    }

    private void assignUUID() {
        UUID generatedUUID = UUID.randomUUID();
        this.uuid = generatedUUID.toString();
    }

    /**
     * <p>Constructor for Patient.</p>
     *
     * @param oldPatient     a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Patient(Patient oldPatient, Actor simulatedActor) {
        if (oldPatient != null) {
            this.firstName = oldPatient.getFirstName();
            this.lastName = oldPatient.getLastName();
            this.motherMaidenName = oldPatient.getMotherMaidenName();
            this.countryCode = oldPatient.getCountryCode();
            this.genderCode = oldPatient.getGenderCode();
            this.religionCode = oldPatient.getReligionCode();
            this.raceCode = oldPatient.getRaceCode();
            this.dateOfBirth = oldPatient.getDateOfBirth();
            this.creationDate = new Date();
            this.patientIdentifiers = new ArrayList<PatientIdentifier>(oldPatient.getPatientIdentifiers());
            copyAddressList(oldPatient);
            this.stillActive = true;
            setSimulatedActor(simulatedActor);
            this.characterSet = oldPatient.characterSet;
            this.alternateFirstName = oldPatient.getAlternateFirstName();
            this.secondName = oldPatient.getSecondName();
            this.alternateSecondName = oldPatient.getAlternateSecondName();
            this.thirdName = oldPatient.getThirdName();
            this.alternateThirdName = oldPatient.getAlternateThirdName();
            this.birthOrder = oldPatient.getBirthOrder();
            this.multipleBirthIndicator = oldPatient.getMultipleBirthIndicator();
            this.lastUpdateFacility = oldPatient.getLastUpdateFacility();
            this.email = oldPatient.getEmail();
            this.phoneNumbers = new ArrayList<PatientPhoneNumber>(oldPatient.getPhoneNumbers());
            this.accountNumber = oldPatient.getAccountNumber();
            this.lastUpdateFacility = "PatientManager";
            this.bloodGroup = oldPatient.getBloodGroup();
            this.maritalStatus = oldPatient.getMaritalStatus();
            this.identityReliabilityCode = oldPatient.getIdentityReliabilityCode();
            if (oldPatient.getAdditionalDemographics() != null) {
                this.additionalDemographics = new AdditionalDemographics(oldPatient.getAdditionalDemographics());
            }
            this.testData = oldPatient.isTestData();
            this.personalRelationships = new ArrayList<Person>(oldPatient.getPersonalRelationships());
            if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD)) {
                this.alternateLastName = oldPatient.getAlternateLastName();
                this.alternateMothersMaidenName = oldPatient.getAlternateMothersMaidenName();
            }
            copyLegalCareModes(oldPatient);
        }
        this.pixReference = null;
        patientDead = false;
        patientDeathTime = null;
        assignUUID();
    }

    /**
     * We don't want to alter the data from the copied patient, create copy of the legal care modes
     *
     * @param oldPatient
     */
    private void copyLegalCareModes(Patient oldPatient) {
        if (oldPatient != null) {
            Iterator iterator = oldPatient.getLegalCareModes().iterator();

            while (iterator.hasNext()) {
                LegalCareMode oldCareMode = (LegalCareMode) iterator.next();
                LegalCareMode copy = new LegalCareMode(oldCareMode, this);
                this.addLegalCareMode(copy);
            }
        }
    }

    /**
     * <p>isTestData.</p>
     *
     * @return a boolean.
     */
    public boolean isTestData() {
        return testData;
    }

    /**
     * <p>Setter for the field <code>testData</code>.</p>
     *
     * @param testData a boolean.
     */
    public void setTestData(boolean testData) {
        this.testData = testData;
    }

    /**
     * <p>isPatientDead.</p>
     *
     * @return a boolean.
     */
    public boolean isPatientDead() {
        return patientDead;
    }

    /**
     * <p>Setter for the field <code>patientDead</code>.</p>
     *
     * @param patientDead a boolean.
     */
    public void setPatientDead(boolean patientDead) {
        this.patientDead = patientDead;
    }

    /**
     * <p>Getter for the field <code>patientDeathTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getPatientDeathTime() {
        return patientDeathTime;
    }

    /**
     * <p>Setter for the field <code>patientDeathTime</code>.</p>
     *
     * @param patientDeathTime a {@link java.util.Date} object.
     */
    public void setPatientDeathTime(Date patientDeathTime) {
        if (patientDeathTime != null) {
            this.patientDeathTime = (Date) patientDeathTime.clone();
        } else {
            this.patientDeathTime = null;
        }
    }

    /**
     * <p>Getter for the field <code>maritalStatus</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * <p>Setter for the field <code>maritalStatus</code>.</p>
     *
     * @param maritalStatus a {@link java.lang.String} object.
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * <p>Getter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIdentityReliabilityCode() {
        return identityReliabilityCode;
    }

    /**
     * <p>Setter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @param identityReliabilityCode a {@link java.lang.String} object.
     */
    public void setIdentityReliabilityCode(String identityReliabilityCode) {
        this.identityReliabilityCode = identityReliabilityCode;
    }

    /**
     * <p>Getter for the field <code>creator</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * <p>Setter for the field <code>creator</code>.</p>
     *
     * @param creator a {@link java.lang.String} object.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        if (simulatedActor != null) {
            this.simulatedActor = simulatedActor;
            this.simulatedActorKeyword = simulatedActor.getKeyword();
        }
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>simulatedActorKeyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSimulatedActorKeyword() {
        return simulatedActorKeyword;
    }

    /**
     * <p>Setter for the field <code>simulatedActorKeyword</code>.</p>
     *
     * @param simulatedActorKeyword a {@link java.lang.String} object.
     */
    public void setSimulatedActorKeyword(String simulatedActorKeyword) {
        this.simulatedActorKeyword = simulatedActorKeyword;
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Getter for the field <code>vipIndicator</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVipIndicator() {
        return vipIndicator;
    }

    /**
     * <p>Setter for the field <code>vipIndicator</code>.</p>
     *
     * @param vipIndicator a {@link java.lang.String} object.
     */
    public void setVipIndicator(String vipIndicator) {
        this.vipIndicator = vipIndicator;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Setter for the field <code>stillActive</code>.</p>
     *
     * @param stillActive a {@link java.lang.Boolean} object.
     */
    public void setStillActive(Boolean stillActive) {
        this.stillActive = stillActive;
    }

    /**
     * <p>Getter for the field <code>stillActive</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getStillActive() {
        return stillActive;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<PatientIdentifier> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PatientIdentifier> getPatientIdentifiers() {
        if (getId() != null) {
            patientIdentifiers = HibernateHelper.getLazyValue(this, "patientIdentifiers", patientIdentifiers);
        }
        return patientIdentifiers;
    }

    /**
     * <p>Setter for the field <code>encounters</code>.</p>
     *
     * @param encounters a {@link java.util.List} object.
     */
    public void setEncounters(List<Encounter> encounters) {
        this.encounters = encounters;
    }

    /**
     * <p>Getter for the field <code>encounters</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Encounter> getEncounters() {
        return encounters;
    }

    /**
     * <p>Getter for the field <code>additionalDemographics</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.AdditionalDemographics} object.
     */
    public AdditionalDemographics getAdditionalDemographics() {
        if (additionalDemographics == null) {
            additionalDemographics = new AdditionalDemographics(this);
        }
        return additionalDemographics;
    }

    /**
     * <p>Setter for the field <code>additionalDemographics</code>.</p>
     *
     * @param additionalDemographics a {@link net.ihe.gazelle.simulator.pam.model.AdditionalDemographics} object.
     */
    public void setAdditionalDemographics(AdditionalDemographics additionalDemographics) {
        this.additionalDemographics = additionalDemographics;
    }

    /**
     * <p>Getter for the field <code>phoneNumbers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PatientPhoneNumber> getPhoneNumbers() {
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<PatientPhoneNumber>();
        }
        return phoneNumbers;
    }

    /**
     * <p>Setter for the field <code>phoneNumbers</code>.</p>
     *
     * @param phoneNumbers a {@link java.util.List} object.
     */
    public void setPhoneNumbers(List<PatientPhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    /**
     * saves and returns the patient
     *
     * @param entityManager TODO
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    @Transactional
    public Patient savePatient(EntityManager entityManager) {
        Patient inPatient = entityManager.merge(this);
        entityManager.flush();
        return inPatient;
    }

    /**
     * <p>getPermanentLink.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPermanentLink() {
        if (getId() != null) {
            String baseUrl = PreferenceService.getString("application_url");
            return baseUrl + "/patient.seam?id=" + getId();
        } else {
            return null;
        }
    }

    /**
     * called from the GUI
     *
     * @return a {@link java.util.List} object.
     */
    public List<Encounter> getAllEncountersForPatient() {
        if (this.getId() != null) {
            return getAllEncountersForPatient(EntityManagerService.provideEntityManager());
        } else {
            return null;
        }
    }

    /**
     * <p>getAllEncountersForPatient.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     *
     * @return a {@link java.util.List} object.
     */
    public List<Encounter> getAllEncountersForPatient(EntityManager entityManager) {
        EncounterQuery query = new EncounterQuery(new HQLQueryBuilder<Encounter>(entityManager, Encounter.class));
        query.patient().eq(this);
        query.visitNumber().order(true);
        return query.getListNullIfEmpty();
    }

    /**
     * <p>computeDoubleMetaphones.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient computeDoubleMetaphones(EntityManager entityManager) {
        Query query = entityManager
                .createQuery("SELECT dmetaphone(firstName), dmetaphone(lastName), dmetaphone(motherMaidenName) FROM Patient where id=:patientId");
        query.setParameter("patientId", this.getId());
        try {
            Object[] metaphones = (Object[]) query.getSingleResult();
            this.dmetaphoneFirstName = (String) metaphones[0];
            this.dmetaphoneLastName = (String) metaphones[1];
            this.dmetaphoneMotherMaidenName = (String) metaphones[2];
            Patient patient = entityManager.merge(this);
            entityManager.flush();
            return patient;
        } catch (NoResultException e) {
            log.error("Cannot compute double metaphones for patient with id " + this.getId());
            return this;
        }
    }

    /**
     * <p>searchForReferences.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     *
     * @return a {@link java.util.List} object.
     */
    public List<Patient> searchForReferences(EntityManager entityManager) {
        int maxDistance = 2;
        HQLQueryBuilder<Patient> queryBuilder = new HQLQueryBuilder<Patient>(entityManager, Patient.class);
        if ((this.getFirstName() != null) && !this.getFirstName().isEmpty()) {
            queryBuilder.addRestriction(new HQLRestrictionLevenshtein("dmetaphoneFirstName", this
                    .getDmetaphoneFirstName(), maxDistance));
        }
        if ((this.getLastName() != null) && !this.getLastName().isEmpty()) {
            queryBuilder.addRestriction(new HQLRestrictionLevenshtein("dmetaphoneLastName", this
                    .getDmetaphoneLastName(), maxDistance));
        }
        PatientQuery query = new PatientQuery(queryBuilder);
        query.id().neq(this.getId()); // we do not want to match the current patient
        query.simulatedActor().keyword().eq("PAT_IDENTITY_X_REF_MGR");
        query.stillActive().eq(true);
        if ((this.getGenderCode() != null) && !this.getGenderCode().isEmpty()) {
            query.genderCode().eq(this.getGenderCode());
        }
        if (this.getDateOfBirth() != null) { // search for patients born the same month
            Calendar firstDayOfMonth = Calendar.getInstance();
            firstDayOfMonth.setTime(this.getDateOfBirth());
            firstDayOfMonth.set(Calendar.DAY_OF_MONTH, 1);
            firstDayOfMonth.set(Calendar.HOUR, 0);
            firstDayOfMonth.set(Calendar.MINUTE, 0);
            firstDayOfMonth.set(Calendar.SECOND, 0);
            Calendar firstDayOfMonthAfter = Calendar.getInstance();
            firstDayOfMonthAfter.setTime(this.getDateOfBirth());
            firstDayOfMonthAfter.set(Calendar.HOUR, 0);
            firstDayOfMonthAfter.set(Calendar.MINUTE, 0);
            firstDayOfMonthAfter.set(Calendar.SECOND, 0);
            firstDayOfMonthAfter.set(Calendar.DAY_OF_MONTH, 1);
            if (firstDayOfMonthAfter.get(Calendar.MONTH) == 11) {
                firstDayOfMonthAfter.set(Calendar.MONTH, 0);
                firstDayOfMonthAfter.add(Calendar.YEAR, 1);
            } else {
                firstDayOfMonthAfter.add(Calendar.MONTH, 1);
            }
            query.dateOfBirth().ge(firstDayOfMonth.getTime());
            query.dateOfBirth().lt(firstDayOfMonthAfter.getTime());
        }
        return query.getListDistinct();
    }

    /**
     * <p>getBloodGroups.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getBloodGroups() {
        return ValueSetProvider.getInstance().getListOfConcepts("BLOOD_GROUP");
    }

    /**
     * <p>getBloodGroupDisplayname.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBloodGroupDisplayname() {
        if (this.bloodGroup != null) {
            return ValueSetProvider.getInstance().getDisplayNameForCode("BLOOD_GROUP", this.bloodGroup);
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>accountNumber</code>.</p>
     *
     * @param accountNumber a {@link java.lang.String} object.
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the field <code>accountNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * <p>Getter for the field <code>pixReference</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
     */
    public CrossReference getPixReference() {
        return pixReference;
    }

    /**
     * <p>Setter for the field <code>pixReference</code>.</p>
     *
     * @param pixReference a {@link net.ihe.gazelle.simulator.pix.model.CrossReference} object.
     */
    public void setPixReference(CrossReference pixReference) {
        this.pixReference = pixReference;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneFirstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneFirstName() {
        return dmetaphoneFirstName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneLastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneLastName() {
        return dmetaphoneLastName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneMotherMaidenName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneMotherMaidenName() {
        return dmetaphoneMotherMaidenName;
    }

    /**
     * <p>Getter for the field <code>bloodGroup</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBloodGroup() {
        return bloodGroup;
    }

    /**
     * <p>Setter for the field <code>bloodGroup</code>.</p>
     *
     * @param bloodGroup a {@link java.lang.String} object.
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * <p>Getter for the field <code>birthOrder</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getBirthOrder() {
        return birthOrder;
    }

    /**
     * <p>Setter for the field <code>birthOrder</code>.</p>
     *
     * @param birthOrder a {@link java.lang.Integer} object.
     */
    public void setBirthOrder(Integer birthOrder) {
        this.birthOrder = birthOrder;
    }

    /**
     * <p>Getter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastUpdateFacility() {
        return lastUpdateFacility;
    }

    /**
     * <p>Setter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @param lastUpdateFacility a {@link java.lang.String} object.
     */
    public void setLastUpdateFacility(String lastUpdateFacility) {
        this.lastUpdateFacility = lastUpdateFacility;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getMultipleBirthIndicator() {
        return multipleBirthIndicator;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @param multipleBirthIndicator a {@link java.lang.Boolean} object.
     */
    public void setMultipleBirthIndicator(Boolean multipleBirthIndicator) {
        this.multipleBirthIndicator = multipleBirthIndicator;
    }

    /**
     * <p>Getter for the field <code>birthOrderAsString</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBirthOrderAsString() {
        return birthOrderAsString;
    }

    /**
     * <p>Setter for the field <code>birthOrderAsString</code>.</p>
     *
     * @param birthOrderAsString a {@link java.lang.String} object.
     */
    public void setBirthOrderAsString(String birthOrderAsString) {
        this.birthOrderAsString = birthOrderAsString;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMultipleBirthIndicatorAsString() {
        return multipleBirthIndicatorAsString;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @param multipleBirthIndicatorAsString a {@link java.lang.String} object.
     */
    public void setMultipleBirthIndicatorAsString(String multipleBirthIndicatorAsString) {
        this.multipleBirthIndicatorAsString = multipleBirthIndicatorAsString;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object p) {
        if (p == null) {
            return false;
        }
        if (!(p instanceof Patient)) {
            return false;
        }
        Patient patient = (Patient) p;
        if (getId() == null || patient.getId() == null) {
            return fakeId == patient.fakeId;
        } else {
            return super.equals(patient);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        if (getId() == null) {
            final int prime = 31;
            int result = 1;
            result = (prime * result) + fakeId;
            return result;
        } else {
            return super.hashCode();
        }
    }

    /**
     * <p>deactivatePatient.</p>
     */
    public void deactivatePatient() {
        if (stillActive) {
            setStillActive(false);
            savePatient(EntityManagerService.provideEntityManager());
        }
    }

    /**
     * <p>activatePatient.</p>
     */
    public void activatePatient() {
        if (!stillActive) {
            setStillActive(true);
            savePatient(EntityManagerService.provideEntityManager());
        }
    }

    /**
     * <p>toFhirPatient.</p>
     *
     * @return a {@link net.ihe.gazelle.fhir.resources.PatientFhirIHE} object.
     */
    public PatientFhirIHE toFhirPatient() {
        return ToFhirResourceConverter.patientToFhirResource(this);
    }


    /**
     * <p>getPrincipalPhoneNumber.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber} object.
     */
    public PatientPhoneNumber getPrincipalPhoneNumber() {
        if (getId() != null) {
            PatientPhoneNumberQuery query = new PatientPhoneNumberQuery();
            query.patient().id().eq(this.getId());
            query.principal().eq(true);
            return query.getUniqueResult();
        } else if (this.phoneNumbers != null && !this.phoneNumbers.isEmpty()) {
            for (PatientPhoneNumber phoneNumber : this.phoneNumbers) {
                if (phoneNumber.isPrincipal()) {
                    return phoneNumber;
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    /**
     * <p>addPhoneNumber.</p>
     *
     * @param phoneNumber a {@link net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber} object.
     */
    public void addPhoneNumber(PatientPhoneNumber phoneNumber) {
        if (this.phoneNumbers == null) {
            this.phoneNumbers = new ArrayList<PatientPhoneNumber>();
        }
        this.phoneNumbers.add(phoneNumber);
    }

    /**
     * <p>Getter for the field <code>personalRelationships</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Person> getPersonalRelationships() {
        if (this.personalRelationships == null) {
            this.personalRelationships = new ArrayList<Person>();
        }
        return personalRelationships;
    }

    /**
     * <p>Setter for the field <code>personalRelationships</code>.</p>
     *
     * @param personalRelationships a {@link java.util.List} object.
     */
    public void setPersonalRelationships(List<Person> personalRelationships) {
        this.personalRelationships = personalRelationships;
    }

    /**
     * <p>addPersonalRelationship.</p>
     *
     * @param inPerson a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public void addPersonalRelationship(Person inPerson) {
        this.getPersonalRelationships().add(inPerson);
    }

    /**
     * <p>removePersonalRelationship.</p>
     *
     * @param inPerson a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public void removePersonalRelationship(Person inPerson) {
        this.getPersonalRelationships().remove(inPerson);
    }

    public String getBirthPlaceName() {
        return birthPlaceName;
    }

    public void setBirthPlaceName(String birthPlaceName) {
        this.birthPlaceName = birthPlaceName;
    }

    public List<LegalCareMode> getLegalCareModes() {
        if (legalCareModes == null) {
            this.legalCareModes = new ArrayList<LegalCareMode>();
        }
        return legalCareModes;
    }

    public void setLegalCareModes(List<LegalCareMode> legalCareModes) {
        this.legalCareModes = legalCareModes;
    }

    public void addLegalCareMode(LegalCareMode inLegalCareMode) {
        if (inLegalCareMode != null) {
            getLegalCareModes().add(inLegalCareMode);
        }
    }

    public void removeLegalCareMode(LegalCareMode inLegalCareMode) {
        if (inLegalCareMode != null) {
            getLegalCareModes().remove(inLegalCareMode);
        }
    }

    public LegalCareMode addLegalCareMode() {
        LegalCareMode careMode = new LegalCareMode(this);
        careMode.setSetId(getLegalCareModes().size() + 1);
        careMode.setAction(ActionType.INSERT);
        // TODO: waiting for instructions from Yohann
        careMode.setIncludeInMessage(true);
        addLegalCareMode(careMode);
        return careMode;
    }
}
