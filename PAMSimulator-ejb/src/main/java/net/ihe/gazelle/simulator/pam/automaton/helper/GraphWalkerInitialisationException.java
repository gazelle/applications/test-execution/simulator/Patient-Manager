package net.ihe.gazelle.simulator.pam.automaton.helper;

/**
 * <p>GraphWalkerInitialisationException class.</p>
 *
 * @author aberge
 * @version 1.0: 08/11/17
 */

public class GraphWalkerInitialisationException extends ReflectiveOperationException {

    public GraphWalkerInitialisationException(String message, Throwable cause){
        super(message, cause);
    }

    public GraphWalkerInitialisationException(String message){
        super(message);
    }

    public GraphWalkerInitialisationException(Throwable cause){
        super(cause);
    }
}
