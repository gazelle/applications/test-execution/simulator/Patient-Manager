package net.ihe.gazelle.simulator.pix.consumer;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.testdata.action.ATestDataManager;
import net.ihe.gazelle.simulator.testdata.model.PatientIdentifierTestData;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Abstract AbstractPIXQueryManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractPIXQueryManager extends ATestDataManager implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1288896687267966420L;
	/** Constant <code>PAT_IDENTITY_X_REF_MGR="PAT_IDENTITY_X_REF_MGR"</code> */
	public static final String PAT_IDENTITY_X_REF_MGR = "PAT_IDENTITY_X_REF_MGR";
	/** Constant <code>PAT_IDENTITY_CONSUMER="PAT_IDENTITY_CONSUMER"</code> */
	public static final String PAT_IDENTITY_CONSUMER = "PAT_IDENTITY_CONSUMER";

    protected PatientIdentifier searchedPid;
	protected List<AssigningAuthority> domainsReturned;
	protected AssigningAuthority currentDomainReturned;
	protected List<PatientIdentifier> receivedIdentifiers;
	protected SystemConfiguration selectedSUT;
	protected List<TransactionInstance> hl7Messages;

	/**
	 * <p>initialize.</p>
	 */
	public void initialize() {
		currentDomainReturned = new AssigningAuthority();
		searchedPid = new PatientIdentifier();
		searchedPid.setDomain(new HierarchicDesignator(DesignatorType.PATIENT_ID));
		domainsReturned = new ArrayList<AssigningAuthority>();
		hl7Messages = null;
		setSelectedTestData(null);
	}

	/**
	 * <p>addDomainToList.</p>
	 */
	public void addDomainToList() {
		if ((currentDomainReturned.getNamespace() != null)
				|| ((currentDomainReturned.getUniversalId() != null) && (currentDomainReturned.getUniversalIdType() != null))) {
			domainsReturned.add(currentDomainReturned);
			currentDomainReturned = new AssigningAuthority();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "The domain must contain at least the namespaceID or the Universal ID and the UniversalIdType");
		}
	}

	/**
	 * <p>performAnotherTest.</p>
	 */
	public void performAnotherTest() {
		receivedIdentifiers = null;
		hl7Messages = null;
	}

	/**
	 * <p>sendMessage.</p>
	 */
	public abstract void sendMessage();

	/**
	 * <p>removeAssigningAuthorityFromDomains.</p>
	 *
	 * @param domain a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
	 */
	public void removeAssigningAuthorityFromDomains(AssigningAuthority domain) {
		if (domainsReturned.contains(domain)) {
			domainsReturned.remove(domain);
		}
	}

	/**
	 * <p>Getter for the field <code>searchedPid</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public PatientIdentifier getSearchedPid() {
		return searchedPid;
	}

	/**
	 * <p>Setter for the field <code>searchedPid</code>.</p>
	 *
	 * @param searchedPid a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public void setSearchedPid(PatientIdentifier searchedPid) {
		this.searchedPid = searchedPid;
	}

	/**
	 * <p>Getter for the field <code>domainsReturned</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<AssigningAuthority> getDomainsReturned() {
		return domainsReturned;
	}

	/**
	 * <p>Setter for the field <code>domainsReturned</code>.</p>
	 *
	 * @param domainsReturned a {@link java.util.List} object.
	 */
	public void setDomainsReturned(List<AssigningAuthority> domainsReturned) {
		this.domainsReturned = domainsReturned;
	}

	/**
	 * <p>Getter for the field <code>currentDomainReturned</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
	 */
	public AssigningAuthority getCurrentDomainReturned() {
		return currentDomainReturned;
	}

	/**
	 * <p>Setter for the field <code>currentDomainReturned</code>.</p>
	 *
	 * @param currentDomainReturned a {@link net.ihe.gazelle.simulator.pdq.util.AssigningAuthority} object.
	 */
	public void setCurrentDomainReturned(AssigningAuthority currentDomainReturned) {
		this.currentDomainReturned = currentDomainReturned;
	}

	/**
	 * <p>Getter for the field <code>selectedSUT</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
	 */
	public SystemConfiguration getSelectedSUT() {
		return selectedSUT;
	}

	/**
	 * <p>Setter for the field <code>selectedSUT</code>.</p>
	 *
	 * @param selectedSUT a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
	 */
	public void setSelectedSUT(SystemConfiguration selectedSUT) {
		this.selectedSUT = selectedSUT;
	}

	/**
	 * <p>Getter for the field <code>hl7Messages</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TransactionInstance> getHl7Messages() {
		return hl7Messages;
	}

	/**
	 * <p>Getter for the field <code>receivedIdentifiers</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<PatientIdentifier> getReceivedIdentifiers() {
		return receivedIdentifiers;
	}


	/** {@inheritDoc} */
	@Override
	protected void setDeferredOption(TestData testData) {
		testData.setDeferredOption(false);
	}

	/** {@inheritDoc} */
	@Override
	protected void setCredentialsForTestData(TestData testData) {
		testData.setCredentials(null);
	}

	/** {@inheritDoc} */
	@Override
	protected void setFhirResponseFormatForTestData(TestData testData) {
		testData.setFhirReturnType(null);
	}


	/** {@inheritDoc} */
	@Override
	protected void initializePageUsingTestData(TestData selectedTestData){
		if (selectedTestData != null && selectedTestData instanceof PatientIdentifierTestData){
			PatientIdentifierTestData identifierTestDataTestData = (PatientIdentifierTestData) selectedTestData;
			searchedPid = identifierTestDataTestData.getPatientIdentifier();
			domainsReturned = getAssigningAuthoritiesFromRestrictedDomains(selectedTestData.getRestrictedDomain());
		}
	}

	/** {@inheritDoc} */
	@Override
	public void createNewTestData() {
		PatientIdentifier favoriteIdentifier = new PatientIdentifier(searchedPid);
		createPatientIdentifierTestData(favoriteIdentifier, getTestedTransaction(), domainsReturned);
	}

	/** {@inheritDoc} */
	@Override
	protected void setLimitNumberForTestData(TestData testData){
		testData.setLimitValue(null);
	}

}
