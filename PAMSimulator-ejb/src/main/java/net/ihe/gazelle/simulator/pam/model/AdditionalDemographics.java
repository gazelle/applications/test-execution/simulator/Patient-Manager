package net.ihe.gazelle.simulator.pam.model;

import org.jboss.seam.annotations.Name;

import javax.faces.model.SelectItem;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * <b>Class Description : </b>AdditionalDemographics<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */
@Entity
@Name("additionalDemographics")
@Table(name = "pam_fr_additional_demographics", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_fr_additional_demographics_sequence", sequenceName = "pam_fr_additional_demographics_id_seq", allocationSize = 1)
public class AdditionalDemographics implements Serializable {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    @GeneratedValue(generator = "pam_fr_additional_demographics_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "socio_professional_occupation")
    private String socioProfessionalOccupation;

    @Column(name = "socio_professional_group")
    private String socioProfessionalGroup;

    @Column(name = "year_of_birth")
    private Integer yearOfBirth;

    @Column(name = "week_in_month")
    private LunarWeek weekInMonth;

    @Column(name = "month")
    private Integer month;

    @Column(name = "number_of_week_of_gestation")
    private Integer numberOfWeeksOfGestation;

    @Column(name = "sms_consent")
    private Boolean smsConsent;

    @OneToOne(targetEntity = Patient.class, mappedBy = "additionalDemographics", cascade = CascadeType.MERGE)
    private Patient patient;

    @Transient
    private boolean pregnant;

    @Column(name = "date_of_birth_corrected")
    private Boolean dateOfBirthCorrected;

    @Column(name = "identity_acquisition_mode")
    private String identityAcquisitionMode;

    @Column(name = "proof_of_identity")
    private String proofOfIdentity;

    @Column(name = "document_expiration_date")
    private Date documentExpirationDate;

    @Column(name = "date_of_the_insi_webservice_request")
    private Date dateOfTheInsiWebserviceRequest;

    /**
     * <p>Constructor for AdditionalDemographics.</p>
     */
    public AdditionalDemographics() {
        super();
        if (numberOfWeeksOfGestation == null) {
            pregnant = false;
            numberOfWeeksOfGestation = 0;
        } else {
            pregnant = true;
        }
        if (smsConsent == null) {
            smsConsent = false;
        }
    }

    /**
     * <p>Constructor for AdditionalDemographics.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public AdditionalDemographics(Patient inPatient){
        super();
        this.patient = inPatient;
        if (inPatient.getDateOfBirth() != null){
            GregorianCalendar time = new GregorianCalendar();
            time.setTime(inPatient.getDateOfBirth());
            this.setYearOfBirth(time.get(Calendar.YEAR));
        }
    }

    /**
     * <p>Constructor for AdditionalDemographics.</p>
     *
     * @param add a {@link net.ihe.gazelle.simulator.pam.model.AdditionalDemographics} object.
     */
    public AdditionalDemographics(AdditionalDemographics add){
        super();
        this.numberOfWeeksOfGestation = add.getNumberOfWeeksOfGestation();
        this.socioProfessionalGroup = add.getSocioProfessionalGroup();
        this.socioProfessionalOccupation = add.getSocioProfessionalOccupation();
        this.yearOfBirth = add.getYearOfBirth();
        this.weekInMonth = add.getWeekInMonth();
        this.month = add.getMonth();
        this.smsConsent = add.getSmsConsent();
        this.pregnant = add.isPregnant();
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>socioProfessionalOccupation</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSocioProfessionalOccupation() {
        return socioProfessionalOccupation;
    }

    /**
     * <p>Setter for the field <code>socioProfessionalOccupation</code>.</p>
     *
     * @param socioProfessionalOccupation a {@link java.lang.String} object.
     */
    public void setSocioProfessionalOccupation(String socioProfessionalOccupation) {
        this.socioProfessionalOccupation = socioProfessionalOccupation;
    }

    /**
     * <p>Getter for the field <code>socioProfessionalGroup</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSocioProfessionalGroup() {
        return socioProfessionalGroup;
    }

    /**
     * <p>Setter for the field <code>socioProfessionalGroup</code>.</p>
     *
     * @param socioProfessionalGroup a {@link java.lang.String} object.
     */
    public void setSocioProfessionalGroup(String socioProfessionalGroup) {
        this.socioProfessionalGroup = socioProfessionalGroup;
    }

    /**
     * <p>Getter for the field <code>yearOfBirth</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    /**
     * <p>Setter for the field <code>yearOfBirth</code>.</p>
     *
     * @param yearOfBirth a {@link java.lang.Integer} object.
     */
    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    /**
     * <p>Getter for the field <code>weekInMonth</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.LunarWeek} object.
     */
    public LunarWeek getWeekInMonth() {
        return weekInMonth;
    }

    /**
     * <p>Setter for the field <code>weekInMonth</code>.</p>
     *
     * @param weekInMonth a {@link net.ihe.gazelle.simulator.pam.model.LunarWeek} object.
     */
    public void setWeekInMonth(LunarWeek weekInMonth) {
        this.weekInMonth = weekInMonth;
    }

    /**
     * <p>Getter for the field <code>month</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * <p>Setter for the field <code>month</code>.</p>
     *
     * @param month a {@link java.lang.Integer} object.
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /**
     * <p>Getter for the field <code>numberOfWeeksOfGestation</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getNumberOfWeeksOfGestation() {
        return numberOfWeeksOfGestation;
    }

    /**
     * <p>Setter for the field <code>numberOfWeeksOfGestation</code>.</p>
     *
     * @param numberOfWeeksOfGestation a {@link java.lang.Integer} object.
     */
    public void setNumberOfWeeksOfGestation(Integer numberOfWeeksOfGestation) {
        this.numberOfWeeksOfGestation = numberOfWeeksOfGestation;
    }

    /**
     * <p>Getter for the field <code>smsConsent</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getSmsConsent() {
        return smsConsent;
    }

    /**
     * <p>Setter for the field <code>smsConsent</code>.</p>
     *
     * @param smsConsent a {@link java.lang.Boolean} object.
     */
    public void setSmsConsent(Boolean smsConsent) {
        this.smsConsent = smsConsent;
    }

    /**
     * <p>isPregnant.</p>
     *
     * @return a boolean.
     */
    public boolean isPregnant() {
        return pregnant;
    }

    /**
     * <p>Setter for the field <code>pregnant</code>.</p>
     *
     * @param pregnant a boolean.
     */
    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    /**
     * <p>getListWeekInMonth.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getListWeekInMonth(){
        return LunarWeek.getLunarWeekValues();
    }

    public Boolean getDateOfBirthCorrected() {
        return dateOfBirthCorrected;
    }

    public void setDateOfBirthCorrected(Boolean dateOfBirthCorrected) {
        this.dateOfBirthCorrected = dateOfBirthCorrected;
    }

    public String getIdentityAcquisitionMode() {
        return identityAcquisitionMode;
    }

    public void setIdentityAcquisitionMode(String identityAcquisitionMode) {
        this.identityAcquisitionMode = identityAcquisitionMode;
    }

    public String getProofOfIdentity() {
        return proofOfIdentity;
    }

    public void setProofOfIdentity(String proofOfIdentity) {
        this.proofOfIdentity = proofOfIdentity;
    }

    public Date getDocumentExpirationDate() {
        return documentExpirationDate;
    }

    public void setDocumentExpirationDate(Date documentExpirationDate) {
        this.documentExpirationDate = documentExpirationDate;
    }

    public Date getDateOfTheInsiWebserviceRequest() {
        return dateOfTheInsiWebserviceRequest;
    }

    public void setDateOfTheInsiWebserviceRequest(Date dateOfTheInsiWebserviceRequest) {
        this.dateOfTheInsiWebserviceRequest = dateOfTheInsiWebserviceRequest;
    }
}
