package net.ihe.gazelle.pam.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;

import java.util.ArrayList;
import java.util.List;

public class EncounterFilter extends Filter<Encounter> {

	private static List<HQLCriterion<Encounter, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Encounter, ?>>();
	}

	public EncounterFilter() {
		super(Encounter.class, defaultCriterions);
	}

}
