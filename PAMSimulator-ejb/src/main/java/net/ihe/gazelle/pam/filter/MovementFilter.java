package net.ihe.gazelle.pam.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;

import java.util.ArrayList;
import java.util.List;

public class MovementFilter extends Filter<Movement> {

	private static List<HQLCriterion<Movement, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Movement, ?>>();
	}

	public MovementFilter() {
		super(Movement.class, defaultCriterions);
	}

}
