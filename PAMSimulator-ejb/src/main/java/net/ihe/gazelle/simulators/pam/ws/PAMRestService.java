package net.ihe.gazelle.simulators.pam.ws;

import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.pam.action.PatientManager;
import net.ihe.gazelle.simulator.pam.automaton.helper.AutomatonHelper;
import net.ihe.gazelle.simulator.pam.dao.EncounterDAO;
import net.ihe.gazelle.simulator.pam.dao.MovementDAO;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.utils.XMLGenerator;
import org.jboss.seam.annotations.In;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.util.ArrayList;
import java.util.List;

@Stateless
/**
 * <p>PAMRestService class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PAMRestService implements PAMRestServiceLocal {

	@In(create = true)
	public PatientService patientService;

	/** {@inheritDoc} */
	@Override
	public Response generateRandomData(Boolean getPatient, String patientsCountryCode, HttpServletRequest request) {
		Lifecycle.beginCall();
		Patient patient;
		Encounter encounter;
		if ((getPatient == null) || getPatient) {
			patient = generatePatient(patientsCountryCode);
			encounter = new Encounter(patient);
		} else {
			encounter = new Encounter();
		}
		EncounterDAO.randomlyFillEncounter(encounter);
		Movement movement = new Movement(encounter);
		movement = randomlyFillMovement(movement);
		List<Movement> movements = new ArrayList<Movement>();
		movements.add(movement);
		encounter.setMovements(movements);
		String encounterString = XMLGenerator.Encounter2XML4Rest(encounter);
		ResponseBuilder rb = Response.ok(encounterString);
		Lifecycle.endCall();
		return rb.build();
	}

	/**
	 *
	 * @param selectedMovement
	 * @return a movement with random attributes
	 */
	private Movement randomlyFillMovement(Movement selectedMovement) {
		MovementDAO.randomlyFillMovement(selectedMovement, false);
		selectedMovement.setAssignedPatientLocation(selectedMovement.buildLocation());
		return selectedMovement;
	}

	/**
	 *
	 * @param countryCodeString
	 * @return a patient for the selected country
	 */
	private Patient generatePatient(String countryCodeString) {
		AutomatonHelper helper = new AutomatonHelper(false, patientService);
		return helper.generatePatient(countryCodeString);
	}

}
