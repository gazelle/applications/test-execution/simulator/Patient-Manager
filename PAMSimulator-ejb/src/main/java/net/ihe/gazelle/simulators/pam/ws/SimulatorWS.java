package net.ihe.gazelle.simulators.pam.ws;

import net.ihe.gazelle.simulator.common.action.AbstractSimulatorManager;
import net.ihe.gazelle.simulator.common.action.ResultSendMessage;
import net.ihe.gazelle.simulator.common.action.SimulatorManagerRemote;
import net.ihe.gazelle.simulator.common.model.ConfigurationForWS;
import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.annotations.Name;
import org.jboss.ws.api.annotation.EndpointConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.util.List;

@Stateless
/**
 * <p>SimulatorWS class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("SimulatorManagerWS")
@EndpointConfig(configName = "Seam-WebService-Endpoint")
@WebService(name = "GazelleSimulatorManagerWS", serviceName = "GazelleSimulatorManagerWSService", portName = "GazelleSimulatorManagerWSPort")
public class SimulatorWS extends AbstractSimulatorManager implements SimulatorManagerRemote, Serializable {

	/** Logger */
	
	private static Logger log = LoggerFactory.getLogger(SimulatorWS.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	@Override
	@WebMethod
	public boolean startTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		log.info("Simulator:::startTestInstance");
		try {
			return super.startTestInstance(testInstanceId);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return false;
	}

	/** {@inheritDoc} */
	@Override
	@WebMethod
	public boolean stopTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		// TODO Auto-generated method stub
		return false;
	}

	/** {@inheritDoc} */
	@Override
	@WebMethod
	public boolean deleteTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		// TODO Auto-generated method stub
		return false;
	}

	/** {@inheritDoc} */
	@Override
	@WebMethod
	public String confirmMessageReception(@WebParam(name = "testInstanceId") String testInstanceId,
			@WebParam(name = "testInstanceParticipantsId") String testInstanceParticipantsId,
			@WebParam(name = "transaction") Transaction transaction, @WebParam(name = "messageType") String messageType) {
		log.info("Simulator::confirmMessageReception()");
		return null;
	}

	/** {@inheritDoc} */
	@Override
	@WebMethod
	public ResultSendMessage sendMessage(String testInstanceId, String serverTestInstanceParticipantsId,
			Transaction transaction, String messageType, ConfigurationForWS responderConfiguration,
			List<ContextualInformationInstance> listContextualInformationInstanceInput,
			List<ContextualInformationInstance> listContextualInformationInstanceOutput) throws SOAPException {
		// TODO Auto-generated method stub
		return null;
	}

}
