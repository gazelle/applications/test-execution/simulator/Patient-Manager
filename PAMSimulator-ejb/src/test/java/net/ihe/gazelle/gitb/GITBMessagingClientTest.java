package net.ihe.gazelle.gitb;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GITBMessagingClientTest {

    /**
     * tested class
     */
    private GITBMessagingClient gitbMessagingClient;

    /**
     * mock server
     */
    private WireMockServer server = new WireMockServer(wireMockConfig().port(8780));

    private static final String RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE = "response should not be null";

    private static final String SERVICE_PATH = "MessagingServiceService";

    /**
     * init the mock server and tested class
     *
     * @throws MalformedURLException if the url of the target is invalid
     */
    @Before
    public void init() throws IOException {
        server.start();
        server.stubFor(get(urlEqualTo("/" + SERVICE_PATH + "?wsdl"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBody(readFileContent("src/main/resources/gitb/gitb_ms.wsdl"))));
        server.stubFor(get(urlPathEqualTo("/gitb_core.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_core.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_ms.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_ms.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_tr.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_tr.xsd"))));
        gitbMessagingClient = new GITBMessagingClient(new URL("http://localhost:8780/" + SERVICE_PATH + "?wsdl"),
                "MessagingServiceService", "MessagingServicePort");
    }

    /**
     * reset the server
     */
    @After
    public void stopServer() {
        server.resetAll();
        server.stop();
    }

    /**
     * test getModuleDefinition function
     */
    @Test
    public void getModuleDefinitionTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/getModuleDefinitionResponse.xml");

        com.gitb.ms.v1.GetModuleDefinitionResponse actualResponse = gitbMessagingClient.getModuleDefinition(new com.gitb.ms.v1.Void());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }


    /**
     * test initiate function
     */
    @Test
    public void initiateTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/initiateResponse.xml");

        com.gitb.ms.v1.InitiateResponse actualResponse = gitbMessagingClient.initiate(new com.gitb.ms.v1.InitiateRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
        assertEquals("1", actualResponse.getSessionId(), "Session ID received should be 1 as in the sample");
    }

    /**
     * test receive function
     */
    @Test
    public void receiveTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/receiveResponse.xml");

        com.gitb.ms.v1.Void actualResponse = gitbMessagingClient.receive(new com.gitb.ms.v1.ReceiveRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     * test send function
     */
    @Test
    public void sendTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/sendResponse.xml");

        com.gitb.ms.v1.SendResponse actualResponse = gitbMessagingClient.send(new com.gitb.ms.v1.SendRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
        assertNotNull(actualResponse.getReport(), "Report in received response shall not be null !");
        assertEquals("TEST", actualResponse.getReport().getName(), "Report name in received response shall be [TEST] !");
    }

    /**
     * test beginTransaction function
     */
    @Test
    public void beginTransactionTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/beginTransactionResponse.xml");

        com.gitb.ms.v1.Void actualResponse = gitbMessagingClient.beginTransaction(new com.gitb.ms.v1.BeginTransactionRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     * test endTransaction function
     */
    @Test
    public void endTransactionTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/endTransactionResponse.xml");

        com.gitb.ms.v1.Void actualResponse = gitbMessagingClient.endTransaction(new com.gitb.ms.v1.BasicRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     * test finalize function
     */
    @Test
    public void finalizeTest() throws IOException {
        stubSoapFromFile("src/test/resources/gitb/finalizeResponse.xml");

        com.gitb.ms.v1.Void actualResponse = gitbMessagingClient.finalize(new com.gitb.ms.v1.FinalizeRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     * stub the server from file
     *
     * @param path the file path
     */
    private void stubSoapFromFile(String path) throws IOException {
        server.stubFor(post(urlEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.TEXT_XML)
                        .withBody(readFileContent(path))));
    }

    /**
     * Read file content to use as Wiremock return messages
     *
     * @param path path to the sample message
     * @return the litteral value of the sample
     * @throws IOException if the sample cannot be read properly.
     */
    private String readFileContent(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(path).getAbsolutePath()));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
}
