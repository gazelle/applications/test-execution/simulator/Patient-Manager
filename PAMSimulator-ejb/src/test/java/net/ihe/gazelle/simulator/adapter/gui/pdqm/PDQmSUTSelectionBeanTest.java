package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.annotations.Covers;
import net.ihe.gazelle.simulator.application.FHIRResponderSUTConfigurationDAO;
import net.ihe.gazelle.simulator.application.FHIRResponderSUTConfigurationFinder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests for {@link PDQmSUTSelectionBean}
 */
public class PDQmSUTSelectionBeanTest {

    /**
     * Test {@link PDQmSUTSelectionBean#initializeAvailableSystems()} does initialize the property correctly.
     */
    @Test
    public void initializeAvailableSystems() {
        PDQmSUTSelectionBean sutSelectionBean = new PDQmSUTSelectionBean();
        FHIRResponderSUTConfigurationFinder finder = new FHIRResponderSUTConfigurationFinder();
        final List<FHIRResponderSUTConfiguration> sutList = new ArrayList<>();
        FHIRResponderSUTConfiguration sutConfiguration = new FHIRResponderSUTConfiguration();
        sutConfiguration.setUrl("test");
        sutList.add(sutConfiguration);
        FHIRResponderSUTConfigurationDAO dao = new FHIRResponderSUTConfigurationDAO() {
            @Override
            public List<FHIRResponderSUTConfiguration> retrieveSUTByTransaction(String transactionKeyword) {
                return sutList;
            }
        };
        finder.setSutDAO(dao);
        sutSelectionBean.setSutFinder(finder);

        sutSelectionBean.initializeAvailableSystems();

        assertEquals(sutList, sutSelectionBean.getAvailableSystems());
    }

    /**
     * Test property sutFinder getter and setter.
     */
    @Test
    public void getSetSutFinder() {
        PDQmSUTSelectionBean sutSelectionBean = new PDQmSUTSelectionBean();
        FHIRResponderSUTConfigurationFinder finder = new FHIRResponderSUTConfigurationFinder();

        sutSelectionBean.setSutFinder(finder);

        assertEquals(finder, sutSelectionBean.getSutFinder());
    }

    /**
     * Test property selectedSUT getter and setter.
     */
    @Test
    @Covers(requirements = "MASTERSIMU-9")
    public void getSetSelectedSUT() {
        PDQmSUTSelectionBean sutSelectionBean = new PDQmSUTSelectionBean();
        FHIRResponderSUTConfiguration sutConfiguration = new FHIRResponderSUTConfiguration();

        sutSelectionBean.setSelectedSUT(sutConfiguration);

        assertEquals(sutConfiguration, sutSelectionBean.getSelectedSUT());
    }

    /**
     * Test {@link PDQmSUTSelectionBean#getAvailableSystems()} on non initialize bean.
     */
    @Test
    public void getAvailableSystemsNull() {
        PDQmSUTSelectionBean pdQmSUTSelectionBean = new PDQmSUTSelectionBean();

        assertNull(pdQmSUTSelectionBean.getAvailableSystems());
    }

    /**
     * Test {@link PDQmSUTSelectionBean#getDiagram()} returns the correct path to the diagram.
     */
    @Test
    @Covers(requirements = {"MASTERSIMU-8"})
    public void getDiagram() {
        PDQmSUTSelectionBean pdQmSUTSelectionBean = new PDQmSUTSelectionBean();

        assertEquals("diag/pdqmConsumer.png", pdQmSUTSelectionBean.getDiagram());
    }
}