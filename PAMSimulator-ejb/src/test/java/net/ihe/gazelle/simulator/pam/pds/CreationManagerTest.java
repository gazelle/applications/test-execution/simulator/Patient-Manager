package net.ihe.gazelle.simulator.pam.pds;

import net.ihe.gazelle.demographic.ws.CountryCode;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EntityManagerService.class, CreationManager.class})
public class CreationManagerTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";
    static EntityManager entityManager;
    CreationManager creationManager;


    @BeforeClass
    public static void populateH2db() throws Exception {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        HierarchicDesignator nia = new HierarchicDesignator();
        nia.setNamespaceID("ASIP-SANTE-INS-NIA");
        nia.setUniversalID("1.2.250.1.213.1.4.10");
        nia.setUniversalIDType("ISO");
        nia.setType("INS-NIA");
        nia.setUsage(DesignatorType.PATIENT_ID);
        nia.save(entityManager, false);
        HierarchicDesignator nir = new HierarchicDesignator();
        nir.setNamespaceID("ASIP-SANTE-INS-NIR");
        nir.setUniversalID("1.2.250.1.213.1.4.9");
        nir.setUniversalIDType("ISO");
        nir.setType("INS-NIR");
        nir.setUsage(DesignatorType.PATIENT_ID);
        nir.save(entityManager, false);
        entityManager.getTransaction().commit();
    }

    @Before
    public void setup() {
        creationManager = new CreationManager();
        PowerMockito.mockStatic(EntityManagerService.class);
        Mockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
    }

    //@Test
    public void generateBP6Patient() {
        creationManager = Mockito.spy(creationManager);
        creationManager.setQualifiedIdentity(true);
        creationManager.getInstanceOfITI30Manager().setBp6Mode(true);
        PatientAddress patientAddress = new PatientAddress();
        patientAddress.setCity("Rennes");
        Patient patient = new Patient();
        creationManager.setSelectedPatient(patient);
        CountryCode countryCode = new CountryCode();
        countryCode.setName("France");
        countryCode.setIso("FR");
        creationManager.setSelectedCountry(countryCode);
        Mockito.doReturn(patientAddress).when(creationManager).generateAddress("FR", true);
        Mockito.doReturn("11111").when(creationManager).getTownInseeCode("Rennes");
        Mockito.doReturn("22222").when(creationManager).getCountryInseeCode("FR");
        creationManager.generateAndSavePatient();
        assertEquals(1, creationManager.getSelectedPatient().getAddressList().size());
        assertEquals("VALI", creationManager.getSelectedPatient().getIdentityReliabilityCode());
        PatientAddress address = creationManager.getSelectedPatient().getAddressList().get(0);
        assertEquals(AddressType.BDL, address.getAddressType());
        assertEquals(false, address.isMainAddress());
    }

    @Test
    public void qualifiedIdentityCheckboxListener() {
        creationManager.setQualifiedIdentity(true);
        creationManager.setGenerateIdsForConnectathon(false);
        creationManager.setGenerateIdsForAuthorities(false);
        creationManager.qualifiedIdentityCheckboxListener();
        assertEquals(2, creationManager.getSelectedAuthorities().size());
        assertEquals(true, creationManager.isGenerateIdsForAuthorities());
        assertEquals(true, creationManager.isGenerateIdsForConnectathon());
    }

    @Test
    public void qualifiedIdentityCheckboxListener2() {
        creationManager.setQualifiedIdentity(false);
        creationManager.setGenerateIdsForConnectathon(false);
        creationManager.setGenerateIdsForAuthorities(false);
        creationManager.qualifiedIdentityCheckboxListener();
        assertEquals(null, creationManager.getSelectedAuthorities());
        assertEquals(false, creationManager.isGenerateIdsForAuthorities());
        assertEquals(false, creationManager.isGenerateIdsForConnectathon());
    }

    @Test
    public void populateBP6selectedAuthorities() {
        creationManager.setQualifiedIdentity(true);
        creationManager.populateBP6selectedAuthorities();
        assertEquals(2, creationManager.getSelectedAuthorities().size());
    }

    @Test
    public void populateBP6selectedAuthoritiesNotQualified() {
        creationManager.setQualifiedIdentity(false);
        creationManager.populateBP6selectedAuthorities();
        assertEquals(null, creationManager.getSelectedAuthorities());
    }
}
