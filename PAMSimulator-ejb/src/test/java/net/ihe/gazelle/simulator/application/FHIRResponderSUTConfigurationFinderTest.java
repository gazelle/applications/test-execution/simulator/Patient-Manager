package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link FHIRResponderSUTConfigurationFinder}.
 */
public class FHIRResponderSUTConfigurationFinderTest {

    /**
     * Test for the sutDAO property getter and setter.
     */
    @Test
    public void getSetSUTDAO() {
        FHIRResponderSUTConfigurationFinder finder = new FHIRResponderSUTConfigurationFinder();
        FHIRResponderSUTConfigurationDAO dao = new FHIRResponderSUTConfigurationDAO() {
            @Override
            public List<FHIRResponderSUTConfiguration> retrieveSUTByTransaction(String transactionKeyword) {
                return null;
            }
        };

        finder.setSutDAO(dao);

        assertEquals(dao, finder.getSutDAO());
    }

    /**
     * Test for {@link FHIRResponderSUTConfigurationFinder#retrieveSUTByTransactionKeyword(String)}
     */
    @Test
    public void retrieveSUTByTransactionKeyword() {
        FHIRResponderSUTConfigurationFinder finder = new FHIRResponderSUTConfigurationFinder();
        final List<FHIRResponderSUTConfiguration> sutList = new ArrayList<>();
        FHIRResponderSUTConfiguration sut = new FHIRResponderSUTConfiguration();
        sut.setId(12);
        sutList.add(sut);
        FHIRResponderSUTConfigurationDAO dao = new FHIRResponderSUTConfigurationDAO() {
            @Override
            public List<FHIRResponderSUTConfiguration> retrieveSUTByTransaction(String transactionKeyword) {
                return sutList;
            }
        };
        finder.setSutDAO(dao);

        List<FHIRResponderSUTConfiguration> retrievedSUT = finder.retrieveSUTByTransactionKeyword("Test");

        assertEquals(sutList, retrievedSUT);
    }
}