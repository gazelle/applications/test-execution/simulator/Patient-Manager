package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.message.ADT_A05;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AbstractHL7MessageGeneratorTest {

    @Test
    public void fillAddressIHE() throws HL7Exception {
        IHEHL7MessageGenerator abstractHL7MessageGenerator = new IHEHL7MessageGenerator();

        //init
        ADT_A05 adt_a05 = abstractHL7MessageGenerator.initADTA05();
        List<PatientAddress> patientAddresses = getPatientAddresses();

        abstractHL7MessageGenerator.fillAddress(adt_a05.getPID(), patientAddresses);
        XAD[] addresses =  adt_a05.getPID().getPatientAddress();
        assertEquals(1, addresses.length);
        XAD xad = addresses[0];
        assertEquals("BDL", xad.getAddressType().getValue());
        assertEquals("rue de blabla", xad.getStreetAddress().getStreetOrMailingAddress().getValue());
        assertEquals("FR", xad.getCountry().getValue());
        assertEquals("44444", xad.getZipOrPostalCode().getValue());
        assertEquals("Bretagne", xad.getStateOrProvince().getValue());
        assertNull(xad.getCountyParishCode().getValue());
    }

    @Test
    public void fillAddressBP6_BDL() throws HL7Exception {
        BP6HL7MessageGenerator abstractHL7MessageGenerator = new BP6HL7MessageGenerator();

        //init
        ADT_A05 adt_a05 = abstractHL7MessageGenerator.initADTA05();
        List<PatientAddress> patientAddresses = getPatientAddresses();;

        abstractHL7MessageGenerator.fillAddress(adt_a05.getPID(), patientAddresses);
        XAD[] addresses =  adt_a05.getPID().getPatientAddress();
        assertEquals(1, addresses.length);
        XAD xad = addresses[0];
        assertEquals("BDL", xad.getAddressType().getValue());
        assertNull(xad.getStreetAddress().getStreetOrMailingAddress().getValue());
        assertEquals("FR", xad.getCountry().getValue());
        assertEquals("44444", xad.getZipOrPostalCode().getValue());
        assertNull(xad.getStateOrProvince().getValue());
        assertEquals("33333", xad.getCountyParishCode().getValue());
    }

    @Test
    public void fillAddressBP6_Home() throws HL7Exception {
        BP6HL7MessageGenerator abstractHL7MessageGenerator = new BP6HL7MessageGenerator();

        //init
        ADT_A05 adt_a05 = abstractHL7MessageGenerator.initADTA05();
        List<PatientAddress> patientAddresses = getPatientAddresses();
        patientAddresses.get(0).setAddressType(AddressType.HOME);


        abstractHL7MessageGenerator.fillAddress(adt_a05.getPID(), patientAddresses);
        XAD[] addresses =  adt_a05.getPID().getPatientAddress();
        assertEquals(1, addresses.length);
        XAD xad = addresses[0];
        assertEquals("H", xad.getAddressType().getValue());
        assertEquals("rue de blabla", xad.getStreetAddress().getStreetOrMailingAddress().getValue());
        assertEquals("FR", xad.getCountry().getValue());
        assertEquals("44444", xad.getZipOrPostalCode().getValue());
        assertEquals("Bretagne", xad.getStateOrProvince().getValue());
        assertNull(xad.getCountyParishCode().getValue());
    }

    public List<PatientAddress> getPatientAddresses(){
        List<PatientAddress> patientAddresses = new ArrayList<>();
        PatientAddress patientAddress = new PatientAddress();
        patientAddress.setAddressLine("rue de blabla");
        patientAddress.setCity("Rennes");
        patientAddress.setCountryCode("FR");
        patientAddress.setZipCode("44444");
        patientAddress.setState("Bretagne");
        patientAddress.setInseeCode("33333");
        patientAddress.setAddressType(AddressType.BDL);
        patientAddresses.add(patientAddress);

        return patientAddresses;
    }
}
