package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BP6PatientBuilderTest {

    static BP6PatientBuilder bp6PatientBuilder;
    static List<HierarchicDesignator> selectedAuthorities;

    @BeforeClass
    public static void setUp(){
        bp6PatientBuilder = new BP6PatientBuilder();
        HierarchicDesignator nir = new HierarchicDesignator();
        nir.setNamespaceID("ASIP-SANTE-INS-NIR");
        nir.setUniversalID("1.2.250.1.213.1.4.9");
        nir.setUniversalIDType("ISO");
        nir.setType("INS-NIR");
        List list = new ArrayList<HierarchicDesignator>();
        list.add(nir);
        selectedAuthorities = list;
    }

    public Patient validPatient() {
        Patient inPatient = new Patient();
        inPatient.setGenderCode("M");
        inPatient.setDateOfBirth("19970519");
        PatientAddress address = new PatientAddress();
        address.setAddressType(AddressType.BDL);
        address.setInseeCode("46201");
        inPatient.addPatientAddress(address);
        inPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        bp6PatientBuilder.generateINSIdentifier(new PatientBuilder.IdentifierContext(inPatient, selectedAuthorities, false, false, false, false));
        return inPatient;
    }

    @Test
    public void generateINSIdentifierOKMale() {
        Patient inPatient = validPatient();
        List<PatientIdentifier> list = inPatient.getPatientIdentifiers();
        assertEquals(1, list.size());
        PatientIdentifier id = list.get(0);
        String[] subfields = id.getFullPatientId().split("\\^");
        assertEquals(4, subfields.length);
        assertTrue(!subfields[0].isEmpty());
        assertTrue(subfields[1].isEmpty());
        assertTrue(subfields[2].isEmpty());
        assertTrue(!subfields[3].isEmpty());
        assertEquals("INS Identifier must be 15 character long", 15, subfields[0].length());
        assertEquals("The first character must be 1 because the patient is a male", "1", subfields[0].substring(0, 1));
        assertEquals("The date of birth is not correct", "9705", subfields[0].substring(1, 5));
        assertEquals("The GOC (insee code) is not correct", "46201", subfields[0].substring(5, 10));
        Long keyLong = 97 - (Long.decode(subfields[0].substring(0, 13)) % 97);
        String key = keyLong.toString();
        if (keyLong < 10) {
            key = "0".concat(key);
        }
        assertEquals("The key is not correctly computed", key, subfields[0].substring(13, 15));
    }

    @Test
    public void generateINSIdentifierOKFemale() {
        Patient inPatient = validPatient();
        List<PatientIdentifier> list = inPatient.getPatientIdentifiers();
        PatientIdentifier id = list.get(0);
        String[] subfields = id.getFullPatientId().split("\\^");
        assertEquals(4, subfields.length);
        assertEquals("INS Identifier must be 15 character long", 15, subfields[0].length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateINSIdentifierMissingGender() {
        Patient inPatient = new Patient();
        inPatient.setDateOfBirth("19970519");
        PatientAddress address = new PatientAddress();
        address.setAddressType(AddressType.BDL);
        address.setInseeCode("46201");
        inPatient.addPatientAddress(address);
        inPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        bp6PatientBuilder.generateINSIdentifier(new PatientBuilder.IdentifierContext(inPatient, selectedAuthorities, false, false, false, false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateINSIdentifierGenderEmpty() {
        Patient inPatient = new Patient();
        inPatient.setGenderCode("");
        inPatient.setDateOfBirth("19970519");
        PatientAddress address = new PatientAddress();
        address.setAddressType(AddressType.BDL);
        address.setInseeCode("46201");
        inPatient.addPatientAddress(address);
        inPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        bp6PatientBuilder.generateINSIdentifier(new PatientBuilder.IdentifierContext(inPatient, selectedAuthorities, false, false, false, false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateINSIdentifierDate() {
        Patient inPatient = new Patient();
        inPatient.setGenderCode("M");
        PatientAddress address = new PatientAddress();
        address.setAddressType(AddressType.BDL);
        address.setInseeCode("46201");
        inPatient.addPatientAddress(address);
        inPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        bp6PatientBuilder.generateINSIdentifier(new PatientBuilder.IdentifierContext(inPatient, selectedAuthorities, false, false, false, false));
    }

    @Test(expected = RuntimeException.class)
    public void generateINSIdentifierINSSize() {
        Patient inPatient = new Patient();
        inPatient.setGenderCode("M");
        PatientAddress address = new PatientAddress();
        address.setAddressType(AddressType.BDL);
        address.setInseeCode("4620");
        inPatient.addPatientAddress(address);
        inPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        bp6PatientBuilder.generateINSIdentifier(new PatientBuilder.IdentifierContext(inPatient, selectedAuthorities, false, false, false, false));
    }

    @Test
    public void setGenderIns() {
        Patient inPatient = new Patient();
        inPatient.setGenderCode("M");
        bp6PatientBuilder.setGenderIns(inPatient);
        assertEquals("M", inPatient.getGenderCode());

        inPatient = new Patient();
        inPatient.setGenderCode("F");
        bp6PatientBuilder.setGenderIns(inPatient);
        assertEquals("F", inPatient.getGenderCode());

        inPatient = new Patient();
        inPatient.setGenderCode("U");
        bp6PatientBuilder.setGenderIns(inPatient);
        assertEquals("F", inPatient.getGenderCode());

        inPatient = new Patient();
        bp6PatientBuilder.setGenderIns(inPatient);
        assertEquals("F", inPatient.getGenderCode());
    }
}
