package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.annotations.Covers;
import net.ihe.gazelle.simulator.application.TransactionInstanceDAO;
import net.ihe.gazelle.simulator.application.TransactionInstanceFinder;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pdqm.pdc.PDCMessagingClient;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for {@link PDQmSimulationBean}
 */
public class PDQmSimulationBeanTest {

    private static final String IDENTIFIER = "identifier";

    /**
     * Test for {@link PDQmSimulationBean#getMessages()} when no message is added.
     */
    @Test
    public void getMessagesNull() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();

        assertNull(simulationBean.getMessages());
    }

    /**
     * Test for {@link PDQmSimulationBean#addMessage(TransactionInstance)}.
     * A new message should be added to the message property that should be initiated.
     */
    @Test
    @Covers(requirements = {"MASTERSIMU-13"})
    public void addMessage() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        TransactionInstance transactionInstance = new TransactionInstance();

        simulationBean.addMessage(transactionInstance);

        assertNotNull(simulationBean.getMessages());
        assertEquals(1, simulationBean.getMessages().size());
        assertEquals(transactionInstance, simulationBean.getMessages().get(0));
    }

    /**
     * Test for pdqmMessageConfigurationBean property getter and setter.
     */
    @Test
    public void getSetPdqmMessageConfigurationBean() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        PDQmMessageConfigurationBean messageConfigurationBean = new PDQmMessageConfigurationBean();

        simulationBean.setPdqmMessageConfigurationBean(messageConfigurationBean);

        assertEquals(messageConfigurationBean, simulationBean.getPdqmMessageConfigurationBean());
    }

    /**
     * Test for pdqmSUTSelectionBean property getter and setter.
     */
    @Test
    public void getSetPdqmSUTSelectionBean() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        PDQmSUTSelectionBean sutSelectionBean = new PDQmSUTSelectionBean();

        simulationBean.setPdqmSUTSelectionBean(sutSelectionBean);

        assertEquals(sutSelectionBean, simulationBean.getPdqmSUTSelectionBean());
    }

    /**
     * Test for transactionInstanceFinder property getter and setter.
     */
    @Test
    public void getTransactionInstanceFinder() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        TransactionInstanceFinder finder = new TransactionInstanceFinder();

        simulationBean.setTransactionInstanceFinder(finder);

        assertEquals(finder, simulationBean.getTransactionInstanceFinder());
    }

    /**
     * Test for {@link PDQmSimulationBean#send()}.
     */
    @Test
    public void send() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        PDQmMessageConfigurationBean messageConfigurationBean = new PDQmMessageConfigurationBean();
        messageConfigurationBean.setPatientIdentifier(IDENTIFIER);
        simulationBean.setPdqmMessageConfigurationBean(messageConfigurationBean);
        PDQmSUTSelectionBean sutSelectionBean = new PDQmSUTSelectionBean();
        FHIRResponderSUTConfiguration sutConfiguration = new FHIRResponderSUTConfiguration();
        sutConfiguration.setUrl("https://tartes-aux.pommes");
        sutSelectionBean.setSelectedSUT(sutConfiguration);
        simulationBean.setPdqmSUTSelectionBean(sutSelectionBean);
        PDCMessagingClient messagingClient = new PDCMessagingClient() {
            @Override
            public String query(String id, String sutEndpoint) {
                return "12";
            }

            @Override
            public String retrieve(String id, String sutEndpoint) {
                return null;
            }
        };
        simulationBean.setPdcMessagingClient(messagingClient);
        TransactionInstanceFinder transactionInstanceFinder = new TransactionInstanceFinder();
        final TransactionInstance transactionInstance = new TransactionInstance();
        transactionInstance.setCompanyKeyword("Tarzan");
        TransactionInstanceDAO transactionInstanceDAO = new TransactionInstanceDAO() {
            @Override
            public TransactionInstance retrieveTransactionInstanceById(String id) {
                assertEquals("Invalid transaction instance id !", "12", id);
                return transactionInstance;
            }
        };
        transactionInstanceFinder.setTransactionInstanceDAO(transactionInstanceDAO);
        simulationBean.setTransactionInstanceFinder(transactionInstanceFinder);

        simulationBean.send();

        assertNotNull(simulationBean.getMessages());
        assertEquals(1, simulationBean.getMessages().size());
        assertEquals(transactionInstance, simulationBean.getMessages().get(0));
    }

    /**
     * Test for {@link PDQmSimulationBean#resetForm()}
     */
    @Test
    public void resetForm() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        PDQmMessageConfigurationBean messageConfigurationBean = new PDQmMessageConfigurationBean();
        simulationBean.setPdqmMessageConfigurationBean(messageConfigurationBean);
        messageConfigurationBean.setPatientIdentifier("Tarte aux pommes.");

        simulationBean.resetForm();

        assertNull(simulationBean.getPdqmMessageConfigurationBean().getPatientIdentifier());
    }

    /**
     * Test that {@link PDQmSimulationBean#performAnotherTest()} does reset messages.
     */
    @Test
    public void performAnotherTestResetMessages() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        TransactionInstance transactionInstance = new TransactionInstance();
        simulationBean.addMessage(transactionInstance);

        simulationBean.performAnotherTest();

        assertNull(simulationBean.getMessages());
    }

    /**
     * Test that {@link PDQmSimulationBean#performAnotherTest()} does not reset message configuration
     */
    @Test
    public void performAnotherTestKeepMessageConfiguration() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();
        PDQmMessageConfigurationBean messageConfigurationBean = new PDQmMessageConfigurationBean();
        simulationBean.setPdqmMessageConfigurationBean(messageConfigurationBean);
        messageConfigurationBean.setPatientIdentifier(IDENTIFIER);

        simulationBean.performAnotherTest();

        assertEquals(IDENTIFIER, simulationBean.getPdqmMessageConfigurationBean().getPatientIdentifier());
    }

    /**
     * Test for {@link PDQmSimulationBean#getTitle()}
     */
    @Test
    public void getTitle() {
        PDQmSimulationBean simulationBean = new PDQmSimulationBean();

        assertEquals("PDQm Patient Demographics Consumer", simulationBean.getTitle());
    }
}