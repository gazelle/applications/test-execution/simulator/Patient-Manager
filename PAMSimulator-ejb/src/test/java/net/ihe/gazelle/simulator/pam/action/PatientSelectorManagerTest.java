package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.pds.CreationManager;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PatientSelectorManagerTest {

    static PatientSelectorManager patientSelectorManager;

    @BeforeClass
    public static void setUp(){
        patientSelectorManager = new CreationManager();
        HierarchicDesignator nir = new HierarchicDesignator();
        nir.setNamespaceID("ASIP-SANTE-INS-NIR");
        nir.setUniversalID("1.2.250.1.213.1.4.9");
        nir.setUniversalIDType("ISO");
        nir.setType("INS-NIR");
        List list = new ArrayList<HierarchicDesignator>();
        list.add(nir);
        patientSelectorManager.setSelectedAuthorities(list);
    }


    @Test
    public void addSelectedAuthoritiesNull(){
        patientSelectorManager.setSelectedAuthorities(null);
        HierarchicDesignator nir = new HierarchicDesignator();
        nir.setNamespaceID("ASIP-SANTE-INS-NIR");
        nir.setUniversalID("1.2.250.1.213.1.4.9");
        nir.setUniversalIDType("ISO");
        nir.setType("INS-NIR");
        patientSelectorManager.addSelectedAuthorities(nir);
        assertEquals(1, patientSelectorManager.getSelectedAuthorities().size());
    }

    @Test
    public void addSelectedAuthoritiesNotNull(){
        HierarchicDesignator nir = new HierarchicDesignator();
        nir.setNamespaceID("ASIP-SANTE-INS-NIA");
        nir.setUniversalID("1.2.250.1.213.1.4.10");
        nir.setUniversalIDType("ISO");
        nir.setType("INS-NIA");
        patientSelectorManager.addSelectedAuthorities(nir);
        assertEquals(2, patientSelectorManager.getSelectedAuthorities().size());
    }
}
