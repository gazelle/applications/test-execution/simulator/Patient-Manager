package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02LivingSubjectId;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FindCandidateQueryParserTest {

    private FindCandidateQueryParser findCandidateQueryParser;
    private PatientQuery patientQuery;

    @Before
    public void init(){
        findCandidateQueryParser = new FindCandidateQueryParser();
        EntityManager entityManager = Persistence.createEntityManagerFactory("PersistenceUnitTest").createEntityManager();
        patientQuery = new PatientQuery(entityManager);
    }

    @Test
    public void testQueryOnSubjectIdsNullList(){
        findCandidateQueryParser.queryOnSubjectIds(patientQuery, null);

        assertEquals(0, patientQuery.getQueryBuilder().getRestrictions().size());
    }

    @Test
    public void testQueryOnSubjectIdsEmptyList(){
        findCandidateQueryParser.queryOnSubjectIds(patientQuery, new ArrayList<PRPAMT201306UV02LivingSubjectId>());

        assertEquals(0, patientQuery.getQueryBuilder().getRestrictions().size());
    }

    @Test
    public void testQueryOnSubjectIdsUnique(){
        String idRoot = "Root";
        String idExtension = "Extension";

        List<PRPAMT201306UV02LivingSubjectId> livingSubjectIds = new ArrayList<>();
        PRPAMT201306UV02LivingSubjectId subjectId = new PRPAMT201306UV02LivingSubjectId();
        subjectId.value = new ArrayList<>();
        II id = new II();
        id.setRoot(idRoot);
        id.setExtension(idExtension);
        subjectId.value.add(id);
        livingSubjectIds.add(subjectId);

        findCandidateQueryParser.queryOnSubjectIds(patientQuery, livingSubjectIds);

        assertEquals(2, patientQuery.getQueryBuilder().getRestrictions().size());
    }

    @Test
    public void testQueryOnSubjectIdsMultiple(){
        String idRoot = "Root";
        String idRoot2 = "Root2";
        String idExtension = "Extension";
        String idExtension2 = "Extension2";

        List<PRPAMT201306UV02LivingSubjectId> livingSubjectIds = new ArrayList<>();
        PRPAMT201306UV02LivingSubjectId subjectId = new PRPAMT201306UV02LivingSubjectId();
        subjectId.value = new ArrayList<>();
        II id = new II();
        id.setRoot(idRoot);
        id.setExtension(idExtension);
        subjectId.value.add(id);
        livingSubjectIds.add(subjectId);

        PRPAMT201306UV02LivingSubjectId subjectId2 = new PRPAMT201306UV02LivingSubjectId();
        subjectId2.value = new ArrayList<>();
        II id2 = new II();
        id2.setRoot(idRoot2);
        id2.setExtension(idExtension2);
        subjectId2.value.add(id2);
        livingSubjectIds.add(subjectId2);

        findCandidateQueryParser.queryOnSubjectIds(patientQuery, livingSubjectIds);

        assertEquals(4, patientQuery.getQueryBuilder().getRestrictions().size());
    }

    @Test
    public void testQueryOnSubjectIdsMultipleWithCommunityRoot(){
        String idRoot = "Root";
        String idExtension = "Extension";
        String idExtension2 = "Extension2";

        List<PRPAMT201306UV02LivingSubjectId> livingSubjectIds = new ArrayList<>();
        PRPAMT201306UV02LivingSubjectId subjectId = new PRPAMT201306UV02LivingSubjectId();
        subjectId.value = new ArrayList<>();
        II id = new II();
        id.setRoot(idRoot);
        id.setExtension(idExtension);
        subjectId.value.add(id);
        livingSubjectIds.add(subjectId);

        PRPAMT201306UV02LivingSubjectId subjectId2 = new PRPAMT201306UV02LivingSubjectId();
        subjectId2.value = new ArrayList<>();
        II id2 = new II();
        id2.setRoot(idRoot);
        id2.setExtension(idExtension2);
        subjectId2.value.add(id2);
        livingSubjectIds.add(subjectId2);

        findCandidateQueryParser.queryOnSubjectIds(patientQuery, livingSubjectIds);

        assertEquals(4, patientQuery.getQueryBuilder().getRestrictions().size());
    }
}
