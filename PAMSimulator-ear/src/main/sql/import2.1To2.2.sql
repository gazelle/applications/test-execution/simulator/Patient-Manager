INSERT INTO app_configuration(id, variable, value) VALUES (23, 'patient_class_oid', '1.3.6.1.4.1.21367.101.104');
INSERT INTO app_configuration(id, variable, value) VALUES (24, 'doctor_oid', '1.3.6.1.4.1.21367.101.110');
INSERT INTO app_configuration(id, variable, value) VALUES (25, 'point_of_care_oid', '1.3.6.1.4.1.21367.101.106');
INSERT INTO app_configuration(id, variable, value) VALUES (26, 'bed_oid', '1.3.6.1.4.1.21367.101.108');
INSERT INTO app_configuration(id, variable, value) VALUES (27, 'room_oid', '1.3.6.1.4.1.21367.101.107');
INSERT INTO app_configuration(id, variable, value) VALUES (28, 'facility_oid', '1.3.6.1.4.1.21367.101.109');
INSERT INTO app_configuration(id, variable, value) VALUES (29, 'number_of_segments_to_display', '40');
SELECT pg_catalog.setval('app_configuration_id_seq', 29, true);

INSERT INTO sys_config (id, name, application, facility, ip_address, port, actor_id, active, shared, charset_id) VALUES (5, 'HL7 inspector PEC', 'HL7', 'Inspector', '127.0.0.1', 2100, 3, true, true, 19);
INSERT INTO sys_config (id, name, application, facility, ip_address, port, actor_id, active, shared, charset_id) VALUES (6, 'PEC Simulator', 'PAMSimulator', 'IHE', '127.0.0.1', 10021, 3, true, true, 19);
SELECT pg_catalog.setval('sys_config_id_seq', 6, true);

INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, charset_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility) VALUES (3, 'IHE PEC', 10021, 3, 1, 1, 19, '*', '*', 'net.ihe.gazelle.simulator.pam.pec.Server', '127.0.0.1', 'PAMSimulator', 'IHE');
SELECT pg_catalog.setval('hl7_simulator_responder_configuration_id_seq', 3, true);

INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (1, 'A01', 'A11', null, 'Admit inpatient', 1, true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (2, 'A04', 'A11', null, 'Register outpatient', 1, true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (3, 'A03', 'A13', null, 'Discharge patient', 1, true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (4, 'A08', null, null, 'Update patient information', 1, true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (5, 'A40', null, null, 'Merge patient identifier lists', 1, true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (6, 'A05', 'A38', 'Z99', 'Pre-admit patient', 1, false, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (7, 'A06', null, 'Z99', 'Change patient class to inpatient', 1, false, false, true);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (8, 'A07', null, 'Z99', 'Change patient class to outpatient', 1, false, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (9, 'A02', 'A12', 'Z99', 'Transfer patient', 1, false, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (10, 'A14', 'A27', 'Z99', 'Pending admit', 1, false, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (11, 'A15', 'A26', 'Z99', 'Pending transfer', 1, false, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (12, 'A16', 'A25', 'Z99', 'Pending discharge', 1, false, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (13, 'A54', 'A55', 'Z99', 'Change attending doctor', 1, false, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (14, 'A21', 'A52', 'Z99', 'Leave of absence', 1, false, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (15, 'A22', 'A53', 'Z99', 'Return from leave of absence', 1, false, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (16, 'A44', null, null, 'Move account information', 1, false, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (17, 'A09', 'A33', null, 'Patient departing - Tracking', 1, false, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (18, 'A10', 'A32', null, 'Patient arriving - Tracking', 1, false, false, false);
SELECT pg_catalog.setval('pam_encounter_management_event_id_seq', 18, true);

UPDATE validation_parameters SET message_type = 'ACK^ALL^ACK' WHERE id=37;
UPDATE validation_parameters SET message_type = 'ACK^ALL^ACK' WHERE id=38;

DELETE FROM hl7_validation_parameters_affinity_domain WHERE validation_parameters_id >= 39;
DELETE FROM validation_parameters WHERE id >=39;

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (39, 'ADT^Z99^ADT_A01', '1.3.6.1.4.12559.11.1.1.58', 4, 1);
SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 39, true);

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (2,37);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (2,38);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,39);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,7);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,10);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,11);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,8);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,9);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,12);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,13);

INSERT INTO package_name_for_profile_oid (id, profile_oid, package_name) VALUES (1, '1.3.6.1.4.12559.11.1.1.60', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
INSERT INTO package_name_for_profile_oid (id, profile_oid, package_name) VALUES (2, '1.3.6.1.4.12559.11.1.1.67', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
INSERT INTO package_name_for_profile_oid (id, profile_oid, package_name) VALUES (3, '1.3.6.1.4.12559.11.1.1.64', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
INSERT INTO package_name_for_profile_oid (id, profile_oid, package_name) VALUES (4, '1.3.6.1.4.12559.11.1.1.61', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
INSERT INTO package_name_for_profile_oid (id, profile_oid, package_name) VALUES (5, '1.3.6.1.4.12559.11.1.1.66', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
SELECT pg_catalog.setval('package_name_for_profile_oid_id_seq', 5, true);

--
-- Data for Name: cmn_home; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO cmn_home VALUES (1, 'Welcome to PAM Simulator', 'eng', '<p><span>
<p>This application is a simulator for the Patient Administration Management profile (PAM). This tool includes the following actors:</p>
<ul>
<li>Patient Demographic Consumer (ready for Beta testing)</li>
<li>Patient Demographic Supplier (ready for Beta testing)</li>
<li>Patient Encounter Consumer (not yet available)</li>
<li>Patient Encounter Supplier (not yet available)</li>
</ul>
<p>This application is still in a development state. If you encounter any issues, please report them in our issue tracker system at&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/jira/browse/PAM">http://gazelle.ihe.net/jira/browse/PAM</a></p>
<p>If it is your first time in this application, please register your system configuration&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/PAMSimulator/systemConfigurations.seam">here</a></p>
<p>In order to keep trace of the patients you have created, you can log in using the CAS (top right corner). If you do not have an account on Gazelle, please register&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/EU-CAT" target="_blank">here</a></p>
</span></p>');
INSERT INTO cmn_home VALUES (2, 'Bienvenue dans le Simulateur PAM', 'fra', '<p><span> </span></p>
<p><span>
<p>Cette application est un simulateur pour le profil PAM (Patient Administration Management). Cet outil inclut les acteurs suivants :</p>
<ul>
<li>Patient Demographic Consumer (en test)</li>
<li>Patient Demographic Supplier (en test)</li>
<li>Patient Encounter Consumer (pas encore disponible)</li>
<li>Patient Encounter Supplier (pas encore disponible)</li>
</ul>
<p>Cette application est en cours de d&eacute;veloppement. Si vous rencontrez des probl&egrave;mes, merci de les rapporter dans notre syst&egrave;me de gestion de probl&egrave;mes &agrave; l''adresse&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/jira/browse/PAM">http://gazelle.ihe.net/jira/browse/PAM</a></p>
<p>S''il s''agit de votre premi&egrave;re connexion &agrave; cette application, avant de commencer merci d''enregistrer la configuration de votre syst&egrave;me&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/PAMSimulator/systemConfigurations.seam">ici</a></p>
<p>De fa&ccedil;on &agrave; garder une trace des patients que vous avez cr&eacute;&eacute;s, vous pouvez vous connecter en utilisant le CAS (lien en haut &agrave; droite). Si vous n''avez pas encore de compte Gazelle, enregistrez-vous &agrave; l''adresse&nbsp;<a style="color: #0078d0;" href="http://gazelle.ihe.net/EU-CAT" target="_blank">ici</a></p>
</span></p>
<p>&nbsp;</p>');
--
-- Name: cmn_home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cmn_home_id_seq', 2, true);

