INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'ACC-2', 'HL70050', 'Accident code' , '1.3.6.1.4.1.12559.11.4.2.30' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID-8', 'HL70001', 'Administrative Sex' , '1.3.6.1.4.1.21367.101.101' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1-4', 'HL70007', 'Admission type' , '2.16.840.1.113883.12.7' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV2-3', 'HL79110', 'Admit reason' , '1.3.6.1.4.1.19376.1.4.1.6.5.4' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1-14 ', 'HL70023', 'Admit source' , '1.3.6.1.4.1.19376.1.4.1.6.5.10080' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'BED', 'Bed' , '1.3.6.1.4.1.21367.101.108' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1-3.5', 'HL70116', 'Bed status ' , '2.16.840.1.113883.12.116' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'OBX', 'BLOOD_GROUP', 'Blood Group' , '1.3.6.1.4.1.12559.11.10.1.3.1.42.20' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , '', 'CONTACT_ROLE' , 'Contact role' , '2.16.840.1.113883.12.131' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'DOCTOR', 'Doctors' , '1.3.6.1.4.1.21367.101.110' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'FACILITY', 'Facilities ' , '1.3.6.1.4.1.21367.101.109' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'HOSPITAL_SERVICE', 'Hospital services ' , '1.3.6.1.4.1.21367.101.111' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID-32 ', 'HL70445', 'Identity Reliability Code' , '2.16.840.1.113883.12.445' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID-16 ', 'HL70002', 'Marital status' , '1.3.6.1.4.1.12559.11.4.2.18' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID-5-7', 'HL70200', 'Name Type Code' , '1.3.6.1.4.1.12559.11.4.2.17' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1-2', 'HL70004', 'Patient class' , '1.3.6.1.4.1.21367.101.104' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'POINT_OF_CARE', 'Point of care ' , '1.3.6.1.4.1.21367.101.106' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID', 'RACE', 'Race' , '1.3.6.1.4.1.21367.101.102' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , '', 'RELATIONSHIP', 'Relationship' , '2.16.840.1.113883.12.63' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PID', 'RELIGION', 'Religion' , '1.3.6.1.4.1.21367.101.122' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1', 'ROOM', 'Room' , '1.3.6.1.4.1.21367.101.107' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'PV1-16 ', 'HL70099', 'VIP indicator' , '1.3.6.1.4.1.12559.11.4.2.33' , true , null);
