update pam_patient_identifier set identifier_type_code = 'INS' where identifier_type_code = 'INS-NIR';
update pam_patient_identifier set identifier_type_code = 'INS' where identifier_type_code = 'INS-NIA';
update pam_hierarchic_designator set type = 'INS' where type = 'INS-NIR';
update pam_hierarchic_designator set type = 'INS' where type = 'INS-NIA';
