--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: affinity_domain; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (1, 'IHE', 'IHE', 'All');


--
-- Name: affinity_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('affinity_domain_id_seq', 1, true);


--
-- Data for Name: tf_transaction; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO tf_transaction (id, description, keyword, name) VALUES (1, 'Patient Encounter Management', 'ITI-31', 'ITI-31');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (2, 'Patient Identification Management', 'ITI-30', 'ITI-30');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (3, 'Patient Demographics Query', 'ITI-21', 'ITI-21');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (4, 'Patient Demographics and Visit Query', 'ITI-22', 'ITI-22');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (5, 'Patient Identity Feed', 'ITI-8', 'Patient Identity Feed');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (6, 'PIX Query', 'ITI-9', 'PIX Query');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (7, 'PIX Update notification', 'ITI-10', 'ITI-10');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (8, 'Patient Registration', 'RAD-1', 'Patient Registration');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (10, 'Patient Demographics Query HL7v3', 'ITI-47', 'Patient Demographics Query HL7v3');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (9, 'Patient Update', 'RAD-12', 'Patient Update');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (11, 'Cross Gateway Patient Discovery', 'ITI-55', 'Cross Gateway Patient Discovery');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (12, 'Patient Location Query', 'ITI-56', 'Patient Location Query');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (13, 'Unknown transaction', 'UNKNOWN', 'UNKNOWN');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (14, 'PIXV3 Query', 'ITI-45', 'ITI-45');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (15, 'Patient Identity Feed HL7V3', 'ITI-44', 'ITI-44');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (16, 'PIXV3 Update notification', 'ITI-46', 'ITI-46');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (17, 'None', 'None', 'NONE');


--
-- Data for Name: affinity_domain_transactions; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: app_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO app_configuration (id, value, variable) VALUES (1, 'http://gazelle.ihe.net/jira/', 'application_issue_tracker_url');
INSERT INTO app_configuration (id, value, variable) VALUES (2, 'PAM Simulator', 'application_name');
INSERT INTO app_configuration (id, value, variable) VALUES (3, 'http://gazelle.ihe.net/jira/browse/PAM#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel', 'application_release_notes_url');
INSERT INTO app_configuration (id, value, variable) VALUES (4, 'UTC+01', 'application_time_zone');
INSERT INTO app_configuration (id, value, variable) VALUES (5, 'PAMSimulator', 'sending_application');
INSERT INTO app_configuration (id, value, variable) VALUES (6, 'IHE', 'sending_facility');
INSERT INTO app_configuration (id, value, variable) VALUES (7, 'http://gazelle.ihe.net', 'svs_repository_url');
INSERT INTO app_configuration (id, value, variable) VALUES (8, 'http://jumbo.irisa.fr:8080/DemographicDataServer-DemographicDataServer-ejb/DemographicDataServerBean?wsdl', 'dds_ws_endpoint');
INSERT INTO app_configuration (id, value, variable) VALUES (9, 'http://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl', 'xsl_location');
INSERT INTO app_configuration (id, value, variable) VALUES (10, 'http://ovh3.ihe-europe.net:8180/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl', 'url_EVCS_ws');
INSERT INTO app_configuration (id, value, variable) VALUES (11, '1.3.6.1.4.1.21367.101.101', 'sex_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (12, '1.3.6.1.4.1.21367.101.122', 'religion_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (13, '1.3.6.1.4.1.21367.101.102', 'race_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (14, '30000', 'timeout_for_receiving_messages');
INSERT INTO app_configuration (id, value, variable) VALUES (15, 'http://gazelle.ihe.net/content/pam-simulator-user-manual', 'documentation_url');
INSERT INTO app_configuration (id, value, variable) VALUES (16, 'http://localhost:8080/PatientManager', 'application_url');
INSERT INTO app_configuration (id, value, variable) VALUES (17, 'http://gazelle.ihe.net/GazelleHL7Validator', 'gazelle_hl7v2_validator_url');
INSERT INTO app_configuration (id, value, variable) VALUES (18, '1.3.6.1.4.1.21367.101.104', 'patient_class_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (19, '1.3.6.1.4.1.21367.101.110', 'doctor_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (20, '1.3.6.1.4.1.21367.101.106', 'point_of_care_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (21, '1.3.6.1.4.1.21367.101.108', 'bed_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (22, '1.3.6.1.4.1.21367.101.107', 'room_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (23, '1.3.6.1.4.1.21367.101.109', 'facility_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (24, '40', 'number_of_segments_to_display');
INSERT INTO app_configuration (id, value, variable) VALUES (25, 'http://localhost:8080/OrderManager/dispatcher.seam', 'create_worklist_url');
INSERT INTO app_configuration (id, value, variable) VALUES (26, 'Europe/Paris', 'time_zone');
INSERT INTO app_configuration (id, value, variable) VALUES (27, '1.3.6.1.4.1.21367.101.111', 'hospital_service_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (29, 'IHEPAM', 'application_namespace_id');
INSERT INTO app_configuration (id, value, variable) VALUES (30, 'true', 'use_ids_from_dds');
INSERT INTO app_configuration (id, value, variable) VALUES (31, 'false', 'application_works_without_cas');
INSERT INTO app_configuration (id, value, variable) VALUES (32, 'ISO', 'application_universal_id_type');
INSERT INTO app_configuration (id, value, variable) VALUES (33, 'minimal', 'dds_mode');
INSERT INTO app_configuration (id, value, variable) VALUES (34, 'https://gazelle.ihe.net/cas', 'cas_url');
INSERT INTO app_configuration (id, value, variable) VALUES (35, 'false', 'ip_login');
INSERT INTO app_configuration (id, value, variable) VALUES (36, '.*', 'ip_login_admin');
INSERT INTO app_configuration (id, value, variable) VALUES (37, 'http://localhost:8080/PatientManager/hl7v3/messages/messageDisplay.seam?id=', 'message_permanent_link');
INSERT INTO app_configuration (id, value, variable) VALUES (38, '20', 'NUMBER_OF_ITEMS_PER_PAGE');
INSERT INTO app_configuration (id, value, variable) VALUES (39, 'http://ovh3.ihe-europe.net:8180/GazelleHL7v2Validator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl', 'hl7v3_validator_url');
INSERT INTO app_configuration (id, value, variable) VALUES (40, '2.16.840.1.113883.3.3731.1.203.18', 'blood_group_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (41, 'ITI', 'default_pdq_domain');
INSERT INTO app_configuration (id, value, variable) VALUES (42, 'TBD', 'hl7v3_organization_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (43, 'TBD', 'hl7v3_pdq_pdc_device_id');
INSERT INTO app_configuration (id, value, variable) VALUES (44, 'TBD', 'hl7v3_pdq_pds_device_id');
INSERT INTO app_configuration (id, value, variable) VALUES (45, 'TBD', 'hl7v3_validation_xsl_location');
INSERT INTO app_configuration (id, value, variable) VALUES (46, 'TBD', 'hl7v3_pix_consumer_device_id');
INSERT INTO app_configuration (id, value, variable) VALUES (47, 'TBD', 'hl7v3_pix_organization_oid');
INSERT INTO app_configuration (id, value, variable) VALUES (48, 'TBD', 'hl7v3_pix_mgr_device_id');
INSERT INTO app_configuration (id, value, variable) VALUES (49, 'http://localhost:8080/PAMSimulator-ejb/PIXManager_Service/PIXManager_PortType', 'pixv3_mgr_url');
INSERT INTO app_configuration (id, value, variable) VALUES (50, 'http://localhost:8080/PAMSimulator-ejb/PDQSupplier_Service/PDQSupplier_Port_Soap12?wsdl', 'pdqv3_pds_url');
INSERT INTO app_configuration (id, value, variable) VALUES (28, '1.3.6.1.4.1.12559.11.20.1', 'application_universal_id');


--
-- Name: app_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('app_configuration_id_seq', 50, true);


--
-- Data for Name: cfg_host; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: cfg_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_sop_class; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_system; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: cfg_dicom_scp_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_dicom_scp_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_dicom_scu_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_dicom_scu_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_hl7_initiator_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_hl7_initiator_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_hl7_responder_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_hl7_responder_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_hl7_v3_initiator_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_hl7_v3_initiator_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_hl7_v3_responder_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_hl7_v3_responder_configuration_id_seq', 1, false);


--
-- Name: cfg_host_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_host_id_seq', 1, false);


--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_sop_class_id_seq', 1, false);


--
-- Data for Name: cfg_syslog_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_syslog_configuration_id_seq', 1, false);


--
-- Data for Name: cfg_web_service_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cfg_web_service_configuration_id_seq', 1, false);


--
-- Data for Name: cmn_home; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO cmn_home (id, home_title, iso3_language, main_content) VALUES (1, 'Welcome to PAM Simulator', 'eng', 'PAM simulator');
INSERT INTO cmn_home (id, home_title, iso3_language, main_content) VALUES (2, 'Bienvenue dans le Simulateur PAM', 'fra', 'PAM simulator');


--
-- Name: cmn_home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cmn_home_id_seq', 2, true);


--
-- Data for Name: tf_actor; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (1, 'Patient Demographic Consumer', 'PDC', 'Patient Demographic Consumer', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (2, 'Patient Demographic Supplier', 'PDS', 'Patient Demographic Supplier', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (3, 'Patient Encounter Consumer', 'PEC', 'Patient Encounter Consumer', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (4, 'Patient Encounter Supplier', 'PES', 'Patient Encounter Supplier', false);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (5, 'Patient Identity Source', 'PAT_IDENTITY_SRC', 'Patient Identity Source', false);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (6, 'Patient Identity Consumer', 'PAT_IDENTITY_CONSUMER', 'Patient Identity Consumer', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (7, 'Patient Identity Cross-Reference Manager', 'PAT_IDENTITY_X_REF_MGR', 'Patient Identity Cross-Reference Manager', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (8, 'ADT', 'ADT', 'ADT', false);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (9, 'MPI, OF, OP, IM, RM', 'MPI', 'ADT Client', true);
INSERT INTO tf_actor (id, description, keyword, name, can_act_as_responder) VALUES (10, 'Unknown actor', 'UNKNOWN', 'UNKNOWN', false);


--
-- Data for Name: cmn_message_instance; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cmn_message_instance_id_seq', 1, false);


--
-- Data for Name: tf_domain; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO tf_domain (id, description, keyword, name) VALUES (1, 'ITI', 'ITI', 'IT-Infrasctructure');


--
-- Data for Name: cmn_transaction_instance; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cmn_transaction_instance_id_seq', 1, false);


--
-- Data for Name: cmn_validator_usage; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('cmn_validator_usage_id_seq', 1, false);


--
-- Data for Name: hl7_message; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: evscvalidationresults; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: tm_path; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_contextual_information; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_contextual_information_id_seq', 1, false);


--
-- Data for Name: gs_test_instance_status; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_test_instance; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_test_steps_instance; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_contextual_information_instance; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_contextual_information_instance_id_seq', 1, false);


--
-- Data for Name: tf_integration_profile; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: tf_actor_integration_profile; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: tf_integration_profile_option; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: tf_actor_integration_profile_option; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_test_instance_participants; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_message; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: gs_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_message_id_seq', 1, false);


--
-- Name: gs_system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_system_id_seq', 1, false);


--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_test_instance_id_seq', 1, false);


--
-- Data for Name: tm_oid; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: gs_test_instance_oid; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_test_instance_participants_id_seq', 1, false);


--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_test_instance_status_id_seq', 1, false);


--
-- Data for Name: gs_test_instance_test_instance_participants; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('gs_test_steps_instance_id_seq', 1, false);


--
-- Data for Name: hl7_charset; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (1, 'ASCII', 'ASCII', 'US-ASCII');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (2, 'ISO8859-1', '8859/1', 'ISO8859_1');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (3, 'ISO8859-2', '8859/2', 'ISO8859_2');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (4, 'ISO8859-3', '8859/3', 'ISO8859_3');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (5, 'ISO8859-4', '8859/4', 'ISO8859_4');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (6, 'ISO8859-5', '8859/5', 'ISO8859_5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (7, 'ISO8859-6', '8859/6', 'ISO8859_6');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (8, 'ISO8859-7', '8859/7', 'ISO8859_7');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (9, 'ISO8859-8', '8859/8', 'ISO8859_8');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (10, 'ISO8859-9', '8859/9', 'ISO8859_9');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (11, 'Japanese (JIS X 0201)', 'ISO IR14', 'JIS0201');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (12, 'Japanese (JIS X 0208)', 'ISO IR87', 'JIS0208');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (13, 'Japanese (JIS X 0212)', 'ISO IR159', 'JIS0212');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (14, 'Chinese', 'GB 18030-2000', 'EUC_CN');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (15, 'Korean', 'KS X 1001', 'EUC_KR');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (16, 'Taiwanese', 'CNS 11643-1992', 'EUC_TW');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (17, 'Taiwanese (BIG 5)', 'BIG-5', 'Big5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (19, 'UTF-8', 'UNICODE UTF-8', 'UTF8');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (20, 'UTF-16', 'UNICODE UTF-16', 'UTF-16');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (21, 'UTF-32', 'UNICODE UTF-32', 'UTF-32');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (22, 'ISO8859-15', '8859/15', 'ISO8859_15');


--
-- Name: hl7_charset_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('hl7_charset_id_seq', 22, true);


--
-- Name: hl7_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('hl7_message_id_seq', 1, false);


--
-- Data for Name: hl7_simulator_responder_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO hl7_simulator_responder_configuration (id, accepted_message_type, accepted_trigger_event, hl7_protocol, ip_address, listening_port, message_encoding, name, receiving_application, receiving_facility, is_running, server_bean_name, url, affinity_domain_id, simulated_actor_id, transaction_id) VALUES (5, '*', '*', 1, '127.0.0.1', 10054, 1, 'PIX Consumer', 'PIXConsumer', 'IHE', true, 'pixConsumer', NULL, 1, 6, NULL);
INSERT INTO hl7_simulator_responder_configuration (id, accepted_message_type, accepted_trigger_event, hl7_protocol, ip_address, listening_port, message_encoding, name, receiving_application, receiving_facility, is_running, server_bean_name, url, affinity_domain_id, simulated_actor_id, transaction_id) VALUES (3, '*', '*', 1, '127.0.0.1', 10055, 1, 'PDQ PDS', 'PatientManager', 'IHE', true, 'pdqpdsServer', NULL, 1, 2, NULL);
INSERT INTO hl7_simulator_responder_configuration (id, accepted_message_type, accepted_trigger_event, hl7_protocol, ip_address, listening_port, message_encoding, name, receiving_application, receiving_facility, is_running, server_bean_name, url, affinity_domain_id, simulated_actor_id, transaction_id) VALUES (2, '*', '*', 1, '127.0.0.1', 10050, 1, 'PAM PEC', 'PatientManager', 'IHE', true, 'pecServer', NULL, 1, 3, 1);
INSERT INTO hl7_simulator_responder_configuration (id, accepted_message_type, accepted_trigger_event, hl7_protocol, ip_address, listening_port, message_encoding, name, receiving_application, receiving_facility, is_running, server_bean_name, url, affinity_domain_id, simulated_actor_id, transaction_id) VALUES (4, '*', '*', 1, '127.0.0.1', 10056, 1, 'PIX Manager', 'PIXManager', 'IHE', true, 'pixManager', NULL, 1, 7, NULL);
INSERT INTO hl7_simulator_responder_configuration (id, accepted_message_type, accepted_trigger_event, hl7_protocol, ip_address, listening_port, message_encoding, name, receiving_application, receiving_facility, is_running, server_bean_name, url, affinity_domain_id, simulated_actor_id, transaction_id) VALUES (1, '*', '*', 1, '127.0.0.1', 10010, 1, 'PAM PDC', 'PatientManager', 'IHE', true, 'pdcServer', NULL, 1, 1, 2);


--
-- Name: hl7_simulator_responder_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('hl7_simulator_responder_configuration_id_seq', 4, true);


--
-- Data for Name: validation_parameters; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (1, NULL, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (2, NULL, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (3, NULL, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (4, NULL, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (5, NULL, 'ADT^A24^ADT_A24', '1.3.6.1.4.12559.11.1.1.41', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (6, NULL, 'ADT^A37^ADT_A37', '1.3.6.1.4.12559.11.1.1.37', 2, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (7, NULL, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.60', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (8, NULL, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.67', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (9, NULL, 'ADT^A03^ADT_A03', '1.3.6.1.4.12559.11.1.1.64', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (10, NULL, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.49', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (11, NULL, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.69', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (12, NULL, 'ADT^A11^ADT_A09', '1.3.6.1.4.12559.11.1.1.61', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (13, NULL, 'ADT^A13^ADT_A01', '1.3.6.1.4.12559.11.1.1.66', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (14, NULL, 'ADT^A05^ADT_A05', '1.3.6.1.4.12559.11.1.1.71', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (15, NULL, 'ADT^A06^ADT_A06', '1.3.6.1.4.12559.11.1.1.74', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (16, NULL, 'ADT^A07^ADT_A06', '1.3.6.1.4.12559.11.1.1.46', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (17, NULL, 'ADT^A02^ADT_A02', '1.3.6.1.4.12559.11.1.1.62', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (18, NULL, 'ADT^A38^ADT_A38', '1.3.6.1.4.12559.11.1.1.57', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (19, NULL, 'ADT^A12^ADT_A12', '1.3.6.1.4.12559.11.1.1.63', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (20, NULL, 'ADT^A14^ADT_A05', '1.3.6.1.4.12559.11.1.1.70', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (21, NULL, 'ADT^A15^ADT_A15', '1.3.6.1.4.12559.11.1.1.73', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (22, NULL, 'ADT^A16^ADT_A16', '1.3.6.1.4.12559.11.1.1.45', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (23, NULL, 'ADT^A27^ADT_A21', '1.3.6.1.4.12559.11.1.1.54', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (24, NULL, 'ADT^A26^ADT_A21', '1.3.6.1.4.12559.11.1.1.50', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (25, NULL, 'ADT^A25^ADT_A21', '1.3.6.1.4.12559.11.1.1.47', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (26, NULL, 'ADT^A21^ADT_A21', '1.3.6.1.4.12559.11.1.1.65', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (27, NULL, 'ADT^A54^ADT_A54', '1.3.6.1.4.12559.11.1.1.55', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (28, NULL, 'ADT^A22^ADT_A21', '1.3.6.1.4.12559.11.1.1.68', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (29, NULL, 'ADT^A44^ADT_A43', '1.3.6.1.4.12559.11.1.1.51', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (30, NULL, 'ADT^A55^ADT_A52', '1.3.6.1.4.12559.11.1.1.56', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (31, NULL, 'ADT^A52^ADT_A52', '1.3.6.1.4.12559.11.1.1.48', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (32, NULL, 'ADT^A53^ADT_A52', '1.3.6.1.4.12559.11.1.1.52', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (33, NULL, 'ADT^A09^ADT_A09', '1.3.6.1.4.12559.11.1.1.53', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (34, NULL, 'ADT^A10^ADT_A09', '1.3.6.1.4.12559.11.1.1.59', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (35, NULL, 'ADT^A33^ADT_A21', '1.3.6.1.4.12559.11.1.1.75', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (36, NULL, 'ADT^A32^ADT_A21', '1.3.6.1.4.12559.11.1.1.72', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (38, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 1, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (37, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.44', 3, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (39, NULL, 'ADT^Z99^ADT_A01', '1.3.6.1.4.12559.11.1.1.58', 4, NULL, 1);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (40, NULL, 'QBP^Q22^QBP_Q21', '1.3.6.1.4.12559.11.1.1.31', 1, NULL, 3);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (41, NULL, 'RSP^K22^RSP_K21', '1.3.6.1.4.12559.11.1.1.35', 2, NULL, 3);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (42, NULL, 'QBP^ZV1^QBP_Q21', '1.3.6.1.4.12559.11.1.1.34', 1, NULL, 4);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (43, NULL, 'RSP^ZV2^RSP_ZV2', '1.3.6.1.4.12559.11.1.1.42', 2, NULL, 4);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (44, NULL, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (45, NULL, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (46, NULL, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (47, NULL, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (48, NULL, 'ADT^A24^ADT_A24', '1.3.6.1.4.12559.11.1.1.41', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (49, NULL, 'ADT^A37^ADT_A37', '1.3.6.1.4.12559.11.1.1.37', 5, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (50, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 7, NULL, 2);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (51, NULL, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.28', 7, NULL, 7);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (52, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 6, NULL, 7);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (53, NULL, 'ACK^A01^ACK', '1.3.6.1.4.12559.11.1.1.147', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (54, NULL, 'ACK^A04^ACK', '1.3.6.1.4.12559.11.1.1.148', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (55, NULL, 'ACK^A05^ACK', '1.3.6.1.4.12559.11.1.1.149', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (56, NULL, 'ACK^A08^ACK', '1.3.6.1.4.12559.11.1.1.150', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (57, NULL, 'ACK^A40^ACK', '1.3.6.1.4.12559.11.1.1.151', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (58, NULL, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.23', 5, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (59, NULL, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.24', 5, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (60, NULL, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.25', 5, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (61, NULL, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.26', 5, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (62, NULL, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.27', 5, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (63, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 7, NULL, 5);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (64, NULL, 'QBP^Q23^QBP_Q21', '1.3.6.1.4.12559.11.1.1.92', 6, NULL, 6);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (65, NULL, 'RSP^K23^RSP_K23', '1.3.6.1.4.12559.11.1.1.30', 7, NULL, 6);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (66, NULL, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.32', 1, NULL, 3);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (67, NULL, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.43', 1, NULL, 4);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (68, NULL, 'ACK^J01^ACK', '1.3.6.1.4.12559.11.1.1.192', 2, NULL, 3);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (69, NULL, 'ACK^J01^ACK', '1.3.6.1.4.12559.11.1.1.193', 2, NULL, 4);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (70, NULL, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.77', 8, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (71, NULL, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.79', 8, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (72, NULL, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.80', 8, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (73, NULL, 'ADT^A11^ADT_A09', '1.3.6.1.4.12559.11.1.1.78', 8, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (74, NULL, 'ADT^A38^ADT_A38', '1.3.6.1.4.12559.11.1.1.76', 8, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (75, NULL, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.82', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (76, NULL, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.87', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (77, NULL, 'ADT^A06^ADT_A06', '1.3.6.1.4.12559.11.1.1.88', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (78, NULL, 'ADT^A07^ADT_A06', '1.3.6.1.4.12559.11.1.1.81', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (79, NULL, 'ADT^A03^ADT_A03', '1.3.6.1.4.12559.11.1.1.84', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (80, NULL, 'ADT^A13^ADT_A01', '1.3.6.1.4.12559.11.1.1.86', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (81, NULL, 'ADT^A02^ADT_A02', '1.3.6.1.4.12559.11.1.1.83', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (82, NULL, 'ADT^A12^ADT_A12', '1.3.6.1.4.12559.11.1.1.85', 8, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (83, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, NULL, 8);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (84, NULL, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, NULL, 9);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (85, NULL, 'PRPA_IN201305UV02', 'PDQv3 - Patient Demographics Query', 1, NULL, 10);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (86, NULL, 'QUQI_IN000003UV01', 'PDQv3 - Patient Demographics Query HL7V3 Continuation', 1, NULL, 10);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (87, NULL, 'QUQI_IN000003UV01_Cancel', 'PDQv3 - Patient Demographics Query HL7V3 Cancellation', 1, NULL, 10);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (88, NULL, 'PRPA_IN201306UV02', 'PDQv3 - Patient Demographics Query Response', 2, NULL, 10);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (89, NULL, 'MCCI_IN000002UV01', 'PDQv3 - Accept Acknowledgement', 2, NULL, 10);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (90, NULL, 'MCCI_IN000002UV01', 'PIXV3 - Patient Identity Feed HL7V3 - Acknowledgement', 7, NULL, 15);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (91, NULL, 'PRPA_IN201309UV02', 'PIXV3 - PIXV3 Query', 6, NULL, 14);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (92, NULL, 'PRPA_IN201302UV02', 'PIXV3 - PIXV3 Update Notification', 7, NULL, 16);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (93, NULL, 'MCCI_IN000002UV01', 'PIXV3 - PIXV3 Update Notification acknowledgement', 6, NULL, 16);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (94, NULL, 'PRPA_IN201301UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Add Patient Record', 5, NULL, 15);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (95, NULL, 'PRPA_IN201302UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Revise Patient Record', 5, NULL, 15);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (96, NULL, 'PRPA_IN201304UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Patient Identity Merge', 5, NULL, 15);
INSERT INTO validation_parameters (id, java_package, message_type, profile_oid, actor_id, domain_id, transaction_id) VALUES (97, NULL, 'PRPA_IN201310UV02', 'PIXV3 - PIXV3 Query Response', 7, NULL, 14);


--
-- Data for Name: hl7_validation_parameters_affinity_domain; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (1, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (2, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (3, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (4, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (5, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (6, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (37, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (38, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (39, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (7, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (10, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (11, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (8, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (9, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (12, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (13, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (14, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (15, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (16, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (17, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (18, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (19, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (40, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (41, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (42, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (43, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (44, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (45, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (46, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (47, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (48, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (49, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (50, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (51, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (52, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (53, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (54, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (55, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (56, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (57, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (58, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (59, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (60, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (61, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (62, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (63, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (64, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (65, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (66, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (67, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (68, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (69, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (70, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (71, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (72, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (73, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (74, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (75, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (76, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (77, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (78, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (79, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (80, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (81, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (82, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (83, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (84, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (85, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (86, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (87, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (88, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (89, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (90, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (91, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (92, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (93, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (94, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (95, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (96, 1);
INSERT INTO hl7_validation_parameters_affinity_domain (validation_parameters_id, affinity_domain_id) VALUES (97, 1);


--
-- Name: ort_evscvalidationresults_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('ort_evscvalidationresults_id_seq', 1, false);


--
-- Name: ort_validation_parameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 97, true);


--
-- Data for Name: pam_hierarchic_designator; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_hierarchic_designator (id, namespace_id, type, universal_id, universal_id_type) VALUES (1, 'IHEPAM', 'Patient', '1.3.6.1.4.1.12559.11.20.1', 'ISO');
INSERT INTO pam_hierarchic_designator (id, namespace_id, type, universal_id, universal_id_type) VALUES (2, 'CATT', 'Patient', '1.3.6.1.4.1.12559.11.20.5', 'ISO');
INSERT INTO pam_hierarchic_designator (id, namespace_id, type, universal_id, universal_id_type) VALUES (3, 'CATT-GOLD', 'Patient', '1.3.6.1.4.1.12559.11.20.6', 'ISO');
INSERT INTO pam_hierarchic_designator (id, namespace_id, type, universal_id, universal_id_type) VALUES (4, 'CATT-SILVER', 'Patient', '1.3.6.1.4.1.12559.11.20.7', 'ISO');
INSERT INTO pam_hierarchic_designator (id, namespace_id, type, universal_id, universal_id_type) VALUES (5, 'UNKNOWN', 'Patient', '1.3.6.1.4.1.12559.11.20.99', 'ISO');


--
-- Data for Name: pix_cross_reference; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (1, '2015-04-08 17:13:35.745', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (2, '2015-04-08 17:13:39.663', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (3, '2015-04-08 17:13:50.386', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (4, '2015-04-08 17:13:53.481', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (5, '2015-04-08 17:14:01.648', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (6, '2015-04-08 17:14:08.154', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (7, '2015-04-08 17:16:24.422', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (8, '2015-04-08 17:16:46.44', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (9, '2015-04-08 17:17:02.85', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (10, '2015-04-08 17:17:07.081', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (11, '2015-04-08 17:17:15.054', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (12, '2015-04-08 17:17:28.142', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (13, '2015-04-08 17:17:36.775', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (14, '2015-04-08 17:17:43.133', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (15, '2015-04-08 17:17:49.106', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (16, '2015-04-08 17:17:52.024', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (17, '2015-04-08 17:18:12.177', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (18, '2015-04-08 17:18:26.765', 'admin');
INSERT INTO pix_cross_reference (id, last_changed, last_modifier) VALUES (19, '2015-04-08 17:18:29.687', 'admin');


--
-- Data for Name: pam_patient; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (1, NULL, NULL, NULL, NULL, NULL, NULL, 'Rotterdam', 'USA', '2015-04-08 17:00:25.301', '1968-10-05 00:00:00', 'Robert', 'M', 'sanders', 'Carper', '2106-3', 'CAT', NULL, 'NY', 'Bee Street', NULL, '12306', 'AN-4^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3910', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, 'Dolton', 'USA', '2015-04-08 17:00:25.385', '1956-07-16 00:00:00', 'Flavia', 'F', 'Sandberg', 'Bowler', '2106-3', 'MOS', NULL, 'IL', 'East 142nd Street', NULL, '60419', 'AN-5^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3810', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, 'Mountain View', 'USA', '2015-04-08 17:00:25.416', '2006-01-01 00:00:00', 'David', 'M', 'Sanches', 'Blanks', '2106-3', 'PRO', NULL, 'CA', 'Franklin Street', NULL, '94041', 'AN-6^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3710', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, 'El Paso City Limits', 'USA', '2015-04-08 17:00:25.449', '1949-11-14 00:00:00', 'Jacklyn', 'F', 'Sanders', 'Shah', '2106-3', 'PRO', NULL, 'Texas', 'East Franklin Avenue', NULL, '79902', 'AN-8^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3610', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (5, NULL, NULL, NULL, NULL, NULL, NULL, 'Wauwatosa', 'USA', '2015-04-08 17:00:25.495', '1999-05-25 00:00:00', 'Robert', 'M', 'Sanders', 'Meredith', '2106-3', 'PRO', NULL, 'WI', 'North 73rd Street', NULL, '53210', 'AN-9^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3510', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (6, NULL, NULL, NULL, NULL, NULL, NULL, 'Bullhead City', 'USA', '2015-04-08 17:00:25.532', '1954-10-24 00:00:00', 'Alane', 'F', 'Santos', 'Heim', '2106-3', 'JEW', NULL, 'AZ', 'Palm Avenue', NULL, '86429', 'AN-10^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3410', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (7, NULL, NULL, NULL, NULL, NULL, NULL, 'Colchester', 'USA', '2015-04-08 17:00:25.572', '1945-09-27 00:00:00', 'Richard', 'M', 'Sanders', 'Beaudry', '1002-5', 'CAT', NULL, 'CT', 'Linwood Avenue', NULL, '6415', 'AN-11^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3310', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (8, NULL, NULL, NULL, NULL, NULL, NULL, 'Reynoldsburg', 'USA', '2015-04-08 17:00:25.613', '2009-12-20 00:00:00', 'Gregory', 'M', 'Sanders', 'Willie', '2106-3', 'PRO', NULL, 'OH', 'Matterhorn Drive', NULL, '43068', 'AN-12^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3210', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (9, NULL, NULL, NULL, NULL, NULL, NULL, 'Townsend', 'USA', '2015-04-08 17:00:25.64', '1990-04-08 00:00:00', 'Hanna', 'F', 'Sanchez', 'Laird', '2106-3', 'PRO', NULL, 'MA', 'Squanicook Terrace', NULL, '1469', 'AN-13^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3110', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (10, NULL, NULL, NULL, NULL, NULL, NULL, 'Crystal', 'USA', '2015-04-08 17:00:25.66', '1943-04-14 00:00:00', 'David', 'M', 'Sanchez', 'Dailey', '2106-3', 'PRO', NULL, 'MN', 'Beltline Expressway', NULL, '55437', 'AN-17^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3020', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (11, NULL, NULL, NULL, NULL, NULL, NULL, 'Lansing', 'USA', '2015-04-08 17:00:25.689', '1968-01-15 00:00:00', 'Jordon', 'M', 'Sanchez', 'Hilton', '2106-3', 'PRO', NULL, 'IL', 'Park Avenue', NULL, '60438', 'AN-18^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3019', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (12, NULL, NULL, NULL, NULL, NULL, NULL, 'Roseburg', 'USA', '2015-04-08 17:00:25.722', '1977-06-11 00:00:00', 'David', 'M', 'Santos', 'Benge', '2054-5', 'PRO', NULL, 'OR', 'Northeast Wright Avenue', NULL, '97470', 'AN-19^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3018', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (13, NULL, NULL, NULL, NULL, NULL, NULL, 'Douglasville', 'USA', '2015-04-08 17:00:25.757', '1979-11-10 00:00:00', 'Mittie', 'F', 'Sanders', 'Tafoya', '2106-3', 'CAT', NULL, 'GA', 'Price Avenue', NULL, '30134', 'AN-20^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3017', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (14, NULL, NULL, NULL, NULL, NULL, NULL, 'Upland', 'USA', '2015-04-08 17:00:25.789', '1941-06-22 00:00:00', 'Ike', 'M', 'Santos', 'Albritton', '2054-5', 'NOE', NULL, 'CA', '8th Street', NULL, '91730', 'AN-21^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3016', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (15, NULL, NULL, NULL, NULL, NULL, NULL, 'Walpole', 'USA', '2015-04-08 17:00:25.819', '1950-05-31 00:00:00', 'Sherley', 'F', 'Sands', 'Carbajal', '2106-3', 'CAT', NULL, 'MA', 'Ellis Street', NULL, '2081', 'AN-22^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3015', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (16, NULL, NULL, NULL, NULL, NULL, NULL, 'Doral', 'USA', '2015-04-08 17:00:25.856', '1980-09-26 00:00:00', 'Cassy', 'F', 'Santos', 'Gaston', '2054-5', 'CAT', NULL, 'FL', 'Northwest 54th Doral Circle Lane', NULL, '33178', 'AN-23^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3014', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (17, NULL, NULL, NULL, NULL, NULL, NULL, 'Kingston', 'USA', '2015-04-08 17:00:25.905', '2006-04-17 00:00:00', 'Robert', 'M', 'Sanders', 'Altman', '2106-3', 'PRO', NULL, 'MA', 'Hillcrest Road', NULL, '2364', 'AN-26^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3013', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (18, NULL, NULL, NULL, NULL, NULL, NULL, 'City of Utica', 'USA', '2015-04-08 17:00:25.955', '1966-05-01 00:00:00', 'Breana', 'F', 'Sanders', 'Newell', '2106-3', 'CAT', NULL, 'NY', 'Oriskany Street West', NULL, '13502', 'AN-24^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3012', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (19, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:00:25.998', '1958-06-22 00:00:00', 'David', 'M', 'Sanders', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-25^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '754-3011', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (20, NULL, NULL, NULL, NULL, NULL, NULL, 'Chicago', 'USA', '2015-04-08 17:00:26.051', '2015-01-02 00:00:00', 'Maddy', 'F', 'Capone', 'Santos', '2054-5', 'CAT', NULL, 'IL', 'MI Av', NULL, '60654', 'AN-26^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, false, '764-3010', true, NULL, 2);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (26, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:05:59.603', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Ridenour', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Haller Avenue', NULL, '98223', 'AN-28^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (27, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:05:59.613', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Ridenour', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Haller Avenue', NULL, '98223', 'AN-29^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (28, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:05:59.624', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Carter', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Mapple Street', NULL, '98223', 'AN-29^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (29, NULL, NULL, NULL, NULL, NULL, NULL, 'Atlanta', 'USA', '2015-04-08 17:05:59.636', '1961-11-09 00:00:00', 'Sandra', 'F', 'Reileigh', 'Arquette', '2054-5', 'CAT', NULL, 'GA', 'Logan St SE', NULL, '30312', 'AN-30^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-4521', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (31, NULL, NULL, NULL, NULL, NULL, NULL, 'Middletown', 'USA', '2015-04-08 17:05:59.659', '1959-03-02 00:00:00', 'Madonna', 'F', 'Bozeman', 'Paulk', '2054-5', 'PRO', NULL, 'CT', 'Pearl Street', NULL, '06457', 'AN-31^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7456', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (32, NULL, NULL, NULL, NULL, NULL, NULL, 'Middletown', 'USA', '2015-04-08 17:05:59.674', '1949-02-03 00:00:00', 'Madelynn', 'F', 'Bauzename', 'Paulk', '2054-5', 'PRO', NULL, 'CT', 'Pearl Street', NULL, '06457', 'AN-32^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7456', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (39, NULL, NULL, NULL, NULL, NULL, NULL, 'Holland', 'USA', '2015-04-08 17:05:59.788', '1988-11-13 00:00:00', 'Stasia', 'F', 'Parent', 'Mendes', '2054-5', 'NOE', NULL, 'MI', 'East 6th Street', NULL, '49423', 'AN-38^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '123-4567', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (40, NULL, NULL, NULL, NULL, NULL, NULL, 'North Tustin', 'USA', '2015-04-08 17:05:59.804', '1963-09-05 00:00:00', 'Kathie', 'F', 'Caballero', 'Fuqua', '2054-5', 'PRO', NULL, 'CA', 'Newport Avenue', NULL, '92780', 'AN-39^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-7891', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (41, NULL, NULL, NULL, NULL, NULL, NULL, 'Lousiville', 'USA', '2015-04-08 17:05:59.815', NULL, 'Rikkie', 'F', 'Jude', 'Ladner', '2054-5', 'CAT', NULL, 'KY', 'Dixie Highway', NULL, '40272', 'AN-40^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '825-4631', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (60, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:06:12.212', '1958-06-22 00:00:00', 'David', 'M', 'Sanderse', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-1225^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (61, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:08:25.131', '1958-06-22 00:00:00', 'David', 'M', 'Sanderse', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-1225^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (62, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:08:25.142', '1958-06-22 00:00:00', 'David', 'M', 'Senders', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-4525^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (63, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:08:25.152', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-27^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (64, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:08:25.163', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-27^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (65, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:08:25.171', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-1227^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (66, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:08:25.178', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Ridenour', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Haller Avenue', NULL, '98223', 'AN-28^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (67, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:08:25.19', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Ridenour', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Haller Avenue', NULL, '98223', 'AN-29^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (68, NULL, NULL, NULL, NULL, NULL, NULL, 'Arlington', 'USA', '2015-04-08 17:08:25.201', '1948-08-29 00:00:00', 'Rosanna', 'F', 'Carter', 'Voigt', '2054-5', 'PRO', NULL, 'WA', 'East Mapple Street', NULL, '98223', 'AN-29^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '654-4721', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (69, NULL, NULL, NULL, NULL, NULL, NULL, 'Atlanta', 'USA', '2015-04-08 17:08:25.21', '1961-11-09 00:00:00', 'Sandra', 'F', 'Reileigh', 'Arquette', '2054-5', 'CAT', NULL, 'GA', 'Logan St SE', NULL, '30312', 'AN-30^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-4521', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (70, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:08:25.221', '1958-06-22 00:00:00', 'David', 'M', 'Sanderse', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-1225^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (71, NULL, NULL, NULL, NULL, NULL, NULL, 'Middletown', 'USA', '2015-04-08 17:08:25.232', '1959-03-02 00:00:00', 'Madonna', 'F', 'Bozeman', 'Paulk', '2054-5', 'PRO', NULL, 'CT', 'Pearl Street', NULL, '06457', 'AN-31^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7456', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (72, NULL, NULL, NULL, NULL, NULL, NULL, 'Middletown', 'USA', '2015-04-08 17:08:25.242', '1949-02-03 00:00:00', 'Madelynn', 'F', 'Bauzename', 'Paulk', '2054-5', 'PRO', NULL, 'CT', 'Pearl Street', NULL, '06457', 'AN-32^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7456', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (73, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:08:25.253', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-33^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (74, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:08:25.265', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-34^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (75, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:08:25.278', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-35^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (76, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:08:25.288', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-36^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (77, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:08:25.3', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-37^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (78, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:08:25.309', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-37^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (79, NULL, NULL, NULL, NULL, NULL, NULL, 'Holland', 'USA', '2015-04-08 17:08:25.319', '1988-11-13 00:00:00', 'Stasia', 'F', 'Parent', 'Mendes', '2054-5', 'NOE', NULL, 'MI', 'East 6th Street', NULL, '49423', 'AN-38^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '123-4567', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (80, NULL, NULL, NULL, NULL, NULL, NULL, 'North Tustin', 'USA', '2015-04-08 17:08:25.33', '1963-09-05 00:00:00', 'Kathie', 'F', 'Caballero', 'Fuqua', '2054-5', 'PRO', NULL, 'CA', 'Newport Avenue', NULL, '92780', 'AN-39^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-7891', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (81, NULL, NULL, NULL, NULL, NULL, NULL, 'Lousiville', 'USA', '2015-04-08 17:08:25.342', NULL, 'Rikkie', 'F', 'Jude', 'Ladner', '2054-5', 'CAT', NULL, 'KY', 'Dixie Highway', NULL, '40272', 'AN-40^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '825-4631', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (82, NULL, NULL, NULL, NULL, NULL, NULL, 'Des Plaines', 'USA', '2015-04-08 17:08:25.357', NULL, 'Madalene', 'F', 'Lemieux', 'Kingsbury', '2054-5', 'CAT', NULL, 'IL', 'Henry Avenue', NULL, '60016', 'AN-41^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (83, NULL, NULL, NULL, NULL, NULL, NULL, 'Des Plaines', 'USA', '2015-04-08 17:08:25.365', NULL, 'Madalene', 'F', 'Lemieux', 'Kingsbury', '2054-5', 'CAT', NULL, 'IL', 'Henry Avenue', NULL, '60016', 'AN-42^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (84, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:08:25.37', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-43^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (85, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:08:25.378', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-44^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (86, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:08:25.386', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-45^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (87, NULL, NULL, NULL, NULL, NULL, NULL, 'Rowland Heights', 'USA', '2015-04-08 17:08:25.394', NULL, 'Robert', 'M', 'Cerda', 'Holliman', '2054-5', 'CAT', NULL, 'CA', 'Aguiro Street', NULL, '91748', 'AN-45^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-7894', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (88, NULL, NULL, NULL, NULL, NULL, NULL, 'Rowland Heights', 'USA', '2015-04-08 17:08:25.402', NULL, 'Robert', 'M', 'Cerda', 'Holliman', '2054-5', 'CAT', NULL, 'CA', 'Aguiro Street', NULL, '91748', 'AN-46^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-7894', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (89, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:08:25.412', NULL, 'William', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-47^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (90, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:08:25.42', NULL, 'William', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-47^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (91, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:08:25.43', NULL, 'Williemae', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-48^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (92, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:08:25.445', NULL, 'Michael', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-50^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (93, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:08:25.454', NULL, 'Michel', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 2, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (94, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:08:25.462', NULL, 'Michael', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-50^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (95, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:08:25.472', NULL, 'Michel', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 2, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (96, NULL, NULL, NULL, NULL, NULL, NULL, 'Burlingame', 'USA', '2015-04-08 17:08:25.481', NULL, 'Michael', 'M', 'Neeley', 'Duggan', '2106-3', 'CAT', NULL, 'CA', 'Paloma Avenue', NULL, '94010', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (97, NULL, NULL, NULL, NULL, NULL, NULL, 'Burlingame', 'USA', '2015-04-08 17:08:25.491', NULL, 'Michael', 'M', 'Neley', 'Duggan', '2106-3', 'CAT', NULL, 'CA', 'Paloma Avenue', NULL, '94010', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (98, NULL, NULL, NULL, NULL, NULL, NULL, 'Philadelphia', 'USA', '2015-04-08 17:08:25.501', NULL, 'Mellisa', 'F', 'Neil', 'Ulrich', '2106-3', 'CAT', NULL, 'PA', 'Sansom St', NULL, '19017', 'AN-52^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '886-1044', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (99, NULL, NULL, NULL, NULL, NULL, NULL, 'Philadelphia', 'USA', '2015-04-08 17:08:25.516', NULL, 'Mellisa', 'F', 'Neil', 'Ulrich', '2106-3', 'CAT', NULL, 'PA', 'Sansom St', NULL, '19017', 'AN-53^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '886-1044', true, NULL, 5);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (37, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:05:59.756', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-37^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, 1, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (38, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:05:59.77', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-37^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, 2, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (36, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland', 'USA', '2015-04-08 17:05:59.741', '1982-08-05 00:00:00', 'Akilah', 'F', 'Treat', 'Lutz', '2054-5', 'CAT', NULL, 'TX', 'South Big Spring Street', NULL, '79701', 'AN-36^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '874-7878', true, 2, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (22, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:05:59.559', '1958-06-22 00:00:00', 'David', 'M', 'Senders', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-4525^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, 3, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (30, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:05:59.647', '1958-06-22 00:00:00', 'David', 'M', 'Sanderse', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-1225^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, 4, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (21, NULL, NULL, NULL, NULL, NULL, NULL, 'Acworth', 'USA', '2015-04-08 17:05:59.545', '1958-06-22 00:00:00', 'David', 'M', 'Sanderse', 'Cuevas', '2054-5', 'CAT', NULL, 'GA', 'Southside Drive', NULL, '30101', 'AN-1225^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '754-3011', true, 4, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (24, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:05:59.581', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-27^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, 5, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (25, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:05:59.592', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-1227^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, NULL, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (23, NULL, NULL, NULL, NULL, NULL, NULL, 'Charlotte', 'USA', '2015-04-08 17:05:59.571', '1980-05-15 00:00:00', 'Flavia', 'F', 'Thomason', 'Royer', '2054-5', 'PRO', NULL, 'NC', 'East Trade Street', NULL, '28202', 'AN-27^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '755-6545', true, 7, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (43, NULL, NULL, NULL, NULL, NULL, NULL, 'Des Plaines', 'USA', '2015-04-08 17:05:59.838', NULL, 'Madalene', 'F', 'Lemieux', 'Kingsbury', '2054-5', 'CAT', NULL, 'IL', 'Henry Avenue', NULL, '60016', 'AN-42^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, 8, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (42, NULL, NULL, NULL, NULL, NULL, NULL, 'Des Plaines', 'USA', '2015-04-08 17:05:59.827', NULL, 'Madalene', 'F', 'Lemieux', 'Kingsbury', '2054-5', 'CAT', NULL, 'IL', 'Henry Avenue', NULL, '60016', 'AN-41^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, 8, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (45, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:05:59.855', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-44^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, 9, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (46, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:05:59.862', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-45^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, 10, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (44, NULL, NULL, NULL, NULL, NULL, NULL, 'Avon', 'USA', '2015-04-08 17:05:59.848', NULL, 'Margherita', 'F', 'Gossett', 'Hume', '2054-5', 'CAT', NULL, 'OH', 'Avon Belden Road', NULL, '44011', 'AN-43^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '741-2589', true, 10, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (59, NULL, NULL, NULL, NULL, NULL, NULL, 'Philadelphia', 'USA', '2015-04-08 17:05:59.989', NULL, 'Mellisa', 'F', 'Neil', 'Ulrich', '2106-3', 'CAT', NULL, 'PA', 'Sansom St', NULL, '19017', 'AN-53^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '886-1044', true, 11, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (58, NULL, NULL, NULL, NULL, NULL, NULL, 'Philadelphia', 'USA', '2015-04-08 17:05:59.978', NULL, 'Mellisa', 'F', 'Neil', 'Ulrich', '2106-3', 'CAT', NULL, 'PA', 'Sansom St', NULL, '19017', 'AN-52^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '886-1044', true, 11, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (54, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:05:59.925', NULL, 'Michael', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-50^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, 12, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (52, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:05:59.907', NULL, 'Michael', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-50^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, 12, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (57, NULL, NULL, NULL, NULL, NULL, NULL, 'Burlingame', 'USA', '2015-04-08 17:05:59.963', NULL, 'Michael', 'M', 'Neley', 'Duggan', '2106-3', 'CAT', NULL, 'CA', 'Paloma Avenue', NULL, '94010', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, 13, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (56, NULL, NULL, NULL, NULL, NULL, NULL, 'Burlingame', 'USA', '2015-04-08 17:05:59.948', NULL, 'Michael', 'M', 'Neeley', 'Duggan', '2106-3', 'CAT', NULL, 'CA', 'Paloma Avenue', NULL, '94010', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, 13, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (55, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:05:59.936', NULL, 'Michel', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 2, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, 14, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (53, NULL, NULL, NULL, NULL, NULL, NULL, 'Dubuque', 'USA', '2015-04-08 17:05:59.915', NULL, 'Michel', 'M', 'Archie', 'Masters', '2054-5', 'CAT', NULL, 'IA', 'Main Street', NULL, '52001', 'AN-51^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 2, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', true, '456-4563', true, 14, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (34, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:05:59.703', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-34^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, 15, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (35, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:05:59.722', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-35^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, 16, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (33, NULL, NULL, NULL, NULL, NULL, NULL, 'Kearny', 'USA', '2015-04-08 17:05:59.688', '2007-06-01 00:00:00', 'Richard', 'M', 'Rodman', 'Broome', '2106-3', 'MOS', NULL, 'NJ', 'Locust Avenue', NULL, '07032', 'AN-33^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '795-7458', true, 16, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (48, NULL, NULL, NULL, NULL, NULL, NULL, 'Rowland Heights', 'USA', '2015-04-08 17:05:59.875', NULL, 'Robert', 'M', 'Cerda', 'Holliman', '2054-5', 'CAT', NULL, 'CA', 'Aguiro Street', NULL, '91748', 'AN-46^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-7894', true, 17, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (47, NULL, NULL, NULL, NULL, NULL, NULL, 'Rowland Heights', 'USA', '2015-04-08 17:05:59.868', NULL, 'Robert', 'M', 'Cerda', 'Holliman', '2054-5', 'CAT', NULL, 'CA', 'Aguiro Street', NULL, '91748', 'AN-45^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '789-7894', true, 17, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (50, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:05:59.894', NULL, 'William', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-47^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, 18, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (51, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:05:59.899', NULL, 'Williemae', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-48^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, 19, 7);
INSERT INTO pam_patient (id, alternate_first_name, alternate_last_name, alternate_mothers_maiden_name, alternate_second_name, alternate_third_name, character_set, city, country_code, creation_date, date_of_birth, first_name, gender_code, last_name, mother_maiden_name, race_code, religion_code, second_name, state, street, third_name, zip_code, account_number, birth_order, blood_group, creator, dmetaphone_first_name, dmetaphone_last_name, dmetaphone_mother_maiden_name, email, last_update_facility, multiple_birth_indicator, phone_number, still_active, cross_reference_id, actor_id) VALUES (49, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohnert Park', 'USA', '2015-04-08 17:05:59.887', NULL, 'William', 'M', 'Reis', 'Vanover', '2054-5', 'CAT', NULL, 'CA', 'Brenda Way', NULL, '94928', 'AN-47^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO^AN', 1, NULL, 'admin', NULL, NULL, NULL, NULL, ' PatientManager', false, '456-4563', true, 19, 7);


--
-- Data for Name: pam_encounter; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: pam_encounter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_encounter_id_seq', 1, false);


--
-- Data for Name: pam_encounter_management_event; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (1, true, 'A11', 'Admit inpatient', true, NULL, 'A01', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (2, true, 'A11', 'Register outpatient', true, NULL, 'A04', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (3, true, 'A13', 'Discharge patient', false, NULL, 'A03', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (4, true, NULL, 'Update patient information', false, NULL, 'A08', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (5, true, NULL, 'Merge patient identifier lists', false, NULL, 'A40', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (6, true, 'A38', 'Pre-admit patient', true, NULL, 'A05', '1', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (7, true, NULL, 'Change patient class to inpatient', false, true, 'A06', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (8, true, NULL, 'Change patient class to outpatient', false, false, 'A07', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (9, true, 'A12', 'Transfer patient', false, false, 'A02', 'IHE', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (10, true, NULL, 'Admission of an in-patient', true, NULL, 'A01', 'IHE_PIX', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (11, true, NULL, 'Registration of an out-patient', true, NULL, 'A04', 'IHE_PIX', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (12, true, NULL, 'Update Patient Information', false, NULL, 'A08', 'IHE_PIX', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (13, true, NULL, 'Merge patient - Internal ID', false, NULL, 'A40', 'IHE_PIX', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (14, true, NULL, 'Pre-admission of an in-patient', true, NULL, 'A05', 'IHE_PIX', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (15, true, 'A11', 'Admission of an in-patient', true, NULL, 'A01', 'IHE_RAD1', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (16, true, 'A11', 'Registration of an out-patient', true, NULL, 'A04', 'IHE_RAD1', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (17, true, 'A38', 'Pre-admission of an in-patient', true, NULL, 'A05', 'IHE_RAD1', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (18, true, NULL, 'Update Patient Information', false, NULL, 'A08', 'IHE_RAD12', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (19, true, NULL, 'Merge patient - Internal ID', false, NULL, 'A40', 'IHE_RAD12', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (20, true, 'A12', 'Transfer patient', false, false, 'A02', 'IHE_RAD12', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (21, true, 'A13', 'Discharge patient', false, NULL, 'A03', 'IHE_RAD12', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (22, true, NULL, 'Change patient class to inpatient', false, true, 'A06', 'IHE_RAD12', NULL);
INSERT INTO pam_encounter_management_event (id, available, cancel_trigger_event, event_name, insert_requires_new_encounter, insert_requires_outpatient, insert_trigger_event, page_id, update_trigger_event) VALUES (23, true, NULL, 'Change patient class to outpatient', false, false, 'A07', 'IHE_RAD12', NULL);


--
-- Name: pam_encounter_management_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_encounter_management_event_id_seq', 23, true);


--
-- Name: pam_hierarchic_designator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_hierarchic_designator_id_seq', 5, true);


--
-- Data for Name: pam_message_id_generator; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_message_id_generator (id, index, oid, issuing_actor_id) VALUES (1, 0, '1.2.3.1.2.3', 1);
INSERT INTO pam_message_id_generator (id, index, oid, issuing_actor_id) VALUES (2, 0, '1.2.3.1.2.4', 2);
INSERT INTO pam_message_id_generator (id, index, oid, issuing_actor_id) VALUES (3, 0, '1.2.3.1.2.5', 5);
INSERT INTO pam_message_id_generator (id, index, oid, issuing_actor_id) VALUES (4, 0, '1.2.3.1.2.6', 6);
INSERT INTO pam_message_id_generator (id, index, oid, issuing_actor_id) VALUES (5, 0, '1.2.3.1.2.7', 7);


--
-- Name: pam_message_id_generator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_message_id_generator_id_seq', 5, true);


--
-- Data for Name: pam_movement; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: pam_movement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_movement_id_seq', 1, false);


--
-- Data for Name: pam_number_generator; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (1, '1^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (2, '2^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (3, '3^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (4, '4^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (5, '5^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (6, '6^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (7, '7^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (8, '8^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (9, '9^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (10, '10^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (11, '11^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (12, '12^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (13, '13^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (14, '14^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (15, '15^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (16, '16^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (17, '17^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (18, '18^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (19, '19^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');
INSERT INTO pam_number_generator (id, generated_number, number_type) VALUES (20, '20^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', 'Patient Identifier');


--
-- Name: pam_number_generator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_number_generator_id_seq', 20, true);


--
-- Data for Name: pam_patient_history; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: pam_patient_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_patient_history_id_seq', 1, false);


--
-- Name: pam_patient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_patient_id_seq', 99, true);


--
-- Data for Name: pam_patient_identifier; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (1, '1^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '1', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (2, 'CATT-0876^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0876', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (3, '2^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '2', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (4, 'CATT-0877^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0877', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (5, '3^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '3', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (6, 'CATT-0878^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0878', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (7, '4^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '4', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (8, 'CATT-0879^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0879', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (9, '5^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '5', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (10, 'CATT-0880^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0880', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (11, '6^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '6', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (12, 'CATT-0881^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0881', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (13, '7^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '7', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (14, 'CATT-0882^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0882', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (15, '8^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '8', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (16, 'CATT-0883^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0883', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (17, '9^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '9', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (18, 'CATT-0884^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0884', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (19, '10^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '10', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (20, 'CATT-0885^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0885', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (21, '11^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '11', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (22, 'CATT-0886^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0886', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (23, '12^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '12', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (24, 'CATT-0887^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0887', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (25, '13^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '13', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (26, 'CATT-0888^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0888', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (27, '14^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '14', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (28, 'CATT-0889^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0889', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (29, '15^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '15', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (30, 'CATT-0890^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0890', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (31, '16^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '16', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (32, 'CATT-0892^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0892', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (33, '17^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '17', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (34, 'CATT-0894^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0894', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (35, '18^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '18', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (36, 'CATT-0895^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0895', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (37, '19^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '19', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (38, 'CATT-0896^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0896', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (39, '20^^^IHEPAM&1.3.6.1.4.1.12559.11.20.1&ISO', '20', 'PI', 1);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (40, 'CATT-0901^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0901', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (41, 'ID-1896^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-1896', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (42, 'SIL-4567^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-4567', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (43, 'CATT-0928^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0928', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (44, 'ID-7842^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-7842', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (45, 'SIL-07894^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-07894', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (46, 'CATT-0929^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0929', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (47, 'ID-0930^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-0930', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (48, 'CATT-9290^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-9290', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (49, 'UN4055^^^UNKNOWN&1.3.6.1.4.1.12559.11.20.99&ISO', 'UN4055', 'NH', 5);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (50, 'UN1896^^^UNKNOWN&1.3.6.1.4.1.12559.11.20.99&ISO', 'UN1896', 'NH', 5);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (51, 'CATT-7894^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-7894', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (52, 'SIL-4561^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-4561', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (53, 'SIL-1234^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-1234', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (54, 'ID-5678^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-5678', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (55, 'CATT-9012^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-9012', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (56, 'CATT-2345^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-2345', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (57, 'ID-6789^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-6789', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (58, 'SIL-0123^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-0123', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (59, 'SIL-1973^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-1973', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (60, 'CATT-0931^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0931', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (61, 'ID-8246^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-8246', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (62, 'CATT-0392^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0392', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (63, 'SIL-0392^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-0392', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (64, 'CATT-0393^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0393', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (65, 'CATT-3930^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-3930', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (66, 'SIL-3930^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-3930', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (67, 'SIL-4041^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-4041', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (68, 'CATT-1404^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-1404', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (69, 'CATT-0932^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-0932', 'NH', 2);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (70, 'SIL-0933^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-0933', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (71, 'SIL-0934^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-0934', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (72, 'SIL-0935^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-0935', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (73, 'ID-0934^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-0934', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (74, 'ID-0935^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-0935', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (75, 'ID-0936^^^CATT-GOLD&1.3.6.1.4.1.12559.11.20.6&ISO', 'ID-0936', 'NH', 3);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (76, 'SIL-5390^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-5390', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (77, 'SIL-7139^^^CATT-SILVER&1.3.6.1.4.1.12559.11.20.7&ISO', 'SIL-7139', 'NH', 4);
INSERT INTO pam_patient_identifier (id, full_patient_id, id_number, identifier_type_code, domain_id) VALUES (78, 'CATT-7139^^^CATT&1.3.6.1.4.1.12559.11.20.5&ISO', 'CATT-7139', 'NH', 2);


--
-- Name: pam_patient_identifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_patient_identifier_id_seq', 78, true);


--
-- Data for Name: pam_patient_link; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: pam_patient_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pam_patient_link_id_seq', 1, false);


--
-- Data for Name: pam_patient_patient_identifier; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (1, 1);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (1, 2);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (2, 3);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (2, 4);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (3, 5);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (3, 6);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (4, 7);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (4, 8);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (5, 9);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (5, 10);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (6, 11);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (6, 12);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (7, 13);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (7, 14);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (8, 15);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (8, 16);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (9, 17);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (9, 18);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (10, 19);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (10, 20);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (11, 21);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (11, 22);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (12, 23);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (12, 24);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (13, 25);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (13, 26);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (14, 27);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (14, 28);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (15, 29);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (15, 30);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (16, 31);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (16, 32);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (17, 33);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (17, 34);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (18, 35);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (18, 36);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (19, 37);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (19, 38);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (20, 39);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (20, 40);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (21, 41);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (22, 42);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (23, 43);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (24, 44);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (25, 45);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (26, 46);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (27, 47);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (28, 48);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (29, 49);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (30, 50);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (31, 51);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (32, 52);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (33, 53);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (34, 54);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (35, 55);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (36, 56);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (37, 57);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (38, 58);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (39, 59);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (40, 60);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (41, 61);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (42, 62);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (43, 63);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (44, 64);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (45, 65);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (46, 66);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (47, 67);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (48, 68);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (49, 69);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (50, 69);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (51, 70);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (52, 71);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (53, 72);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (54, 73);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (55, 74);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (56, 75);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (57, 76);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (58, 77);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (59, 78);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (61, 41);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (62, 42);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (63, 43);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (64, 44);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (65, 45);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (66, 46);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (67, 47);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (68, 48);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (69, 49);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (70, 50);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (71, 51);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (72, 52);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (73, 53);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (74, 54);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (75, 55);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (76, 56);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (77, 57);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (78, 58);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (79, 59);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (80, 60);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (81, 61);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (82, 62);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (83, 63);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (84, 64);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (85, 65);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (86, 66);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (87, 67);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (88, 68);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (89, 69);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (90, 69);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (91, 70);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (92, 71);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (93, 72);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (94, 73);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (95, 74);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (96, 75);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (97, 76);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (98, 77);
INSERT INTO pam_patient_patient_identifier (patient_id, patient_identifier_id) VALUES (99, 78);


--
-- Name: pix_cross_reference_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('pix_cross_reference_id_seq', 19, true);


--
-- Data for Name: system_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: usage_metadata; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (1, 'PDQV3 Supplier', 'IHE_PDQ_PDS', 1, 10);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (2, 'XCPD Responding Gateway', 'IHE_XCPD_RESP', 1, 11);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (3, 'XCPD Responding Gateway - Health Data Locator option', 'IHE_XCPD_RESP_PLQ', 1, 12);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (4, 'XCPD Initiating Gateway - Deferred response option', 'IHE_XCPD_INIT_DEFERRED', 1, 11);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (5, 'XCPD Responding Gateway - Deferred response option', 'IHE_XCPD_RESP_DEFERRED', 1, 11);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (6, 'PIXV3 Manager', 'IHE_PIX_MGR_ITI45', 1, 14);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (7, 'PIXV3 Manager', 'IHE_PIX_MGR_ITI44', 1, 15);
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (8, 'PIXV3 Consumer', 'IHE_PIX_CONS', 1, 16);


--
-- Data for Name: sys_conf_type_usages; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Data for Name: sys_config; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO sys_config (id, active, application, facility, hl7_protocol, ip_address, last_modified, message_encoding, name, port, private_ip, shared, url, username, actor_id, charset_id) VALUES (1, true, 'PatientManager', 'IHE', 1, '127.0.0.1', NULL, 1, 'PAM PDC simulator', 10010, NULL, true, NULL, NULL, 1, 19);
INSERT INTO sys_config (id, active, application, facility, hl7_protocol, ip_address, last_modified, message_encoding, name, port, private_ip, shared, url, username, actor_id, charset_id) VALUES (2, true, 'PatientManager', 'IHE', 1, '127.0.0.1', NULL, 1, 'PAM PEC Simulator', 10050, NULL, true, NULL, NULL, 3, 19);
INSERT INTO sys_config (id, active, application, facility, hl7_protocol, ip_address, last_modified, message_encoding, name, port, private_ip, shared, url, username, actor_id, charset_id) VALUES (3, true, 'PatientManager', 'IHE', 1, '127.0.0.1', NULL, 1, 'PDQ PDS Simulator', 10055, NULL, true, NULL, NULL, 2, 19);


--
-- Name: sys_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('sys_config_id_seq', 3, true);


--
-- Name: system_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('system_configuration_id_seq', 1, false);


--
-- Name: tf_actor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_actor_id_seq', 10, true);


--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_actor_integration_profile_id_seq', 1, false);


--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_actor_integration_profile_option_id_seq', 1, false);


--
-- Name: tf_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_domain_id_seq', 1, false);


--
-- Data for Name: tf_domain_integration_profiles; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_integration_profile_id_seq', 1, false);


--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_integration_profile_option_id_seq', 1, false);


--
-- Name: tf_transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tf_transaction_id_seq', 17, true);


--
-- Name: tm_oid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tm_oid_id_seq', 1, false);


--
-- Name: tm_path_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('tm_path_id_seq', 1, false);


--
-- Name: usage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gazelle
--

SELECT pg_catalog.setval('usage_id_seq', 8, true);


--
-- PostgreSQL database dump complete
--

