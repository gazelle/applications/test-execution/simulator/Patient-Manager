INSERT INTO tf_transaction(id, description, keyword, name) VALUES (3, 'Patient Demographics Query', 'ITI-21', 'Patient Demographics Query');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (4, 'Patient Demographics and Visit Query', 'ITI-22', 'Patient Demographics and Visit Query (not tested)');
SELECT pg_catalog.setval('tf_transaction_id_seq', 4, true);

UPDATE tf_actor SET can_act_as_responder = true WHERE keyword = 'PDS';

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (40, 'QBP^Q22^QBP_Q21', '1.3.6.1.4.12559.11.1.1.31', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (41, 'RSP^K22^RSP_K21', '1.3.6.1.4.12559.11.1.1.35', 2, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (42, 'QBP^ZV1^QBP_Q21', '1.3.6.1.4.12559.11.1.1.34', 1, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (43, 'RSP^ZV2^RSP_ZV2', '1.3.6.1.4.12559.11.1.1.42', 2, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (44, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.32', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (45, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.43', 1, 4);

SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 45, true);

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,40);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,41);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,42);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,43);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,44);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,45);