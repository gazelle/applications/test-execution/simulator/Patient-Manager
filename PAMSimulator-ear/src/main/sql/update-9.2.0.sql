select pg_catalog.setval('tf_actor_id_seq', 10, true);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'INIT_GW' ,'Initiating Gateway', 'Initiating Gateway', true);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'RESP_GW' ,'Responding Gateway', 'Responding Gateway', true);
INSERT INTO tf_transaction(id, keyword, name, description) VALUES (nextval('tf_transaction_id_seq'), 'ITI-55', 'Cross Gateway Patient Discovery', 'Cross Gateway Patient Discovery');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'ITI-55'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE'), 'XCPD', 'ITI_ITI55_RESP_GW');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'ITI-55'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE'), 'XCPD (Deferred response)', 'ITI_ITI55_INIT_GW');

INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_respgw_url', 'http://localhost:8380/PAMSimulator-ejb/RespondingGateway_Service/RespondingGateway_PortType?wsdl');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_respgw_device_id', '1.2.3');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_initgw_device_id', '1.2.3');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_initgw_home_community_id', '1.2.3');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_respgw_home_community_id', '1.2.3');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'hl7v3_xcpd_initgw_url', 'http://localhost:8380/PAMSimulator-ejb/InitiatingGateway_Service/InitiatingGateway_PortType?wsdl');
INSERT INTO app_configuration (id, variable, "value") VALUES (nextval('app_configuration_id_seq'), 'xcpd_respgw_cron_trigger', '1 * * * * ?');
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'PRPA_IN201305UV02', 'XCPD - Cross Gateway Patient Discovery Request', (select id from tf_actor where keyword = 'INIT_GW'), (select id from tf_transaction where keyword = 'ITI-55'), 1, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'PRPA_IN201305UV02 (deferred)', 'XCPD - Cross Gateway Patient Discovery Request (Deferred option)',  (select id from tf_actor where keyword = 'INIT_GW'), (select id from tf_transaction where keyword = 'ITI-55'), 1, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'PRPA_IN201306UV02', 'XCPD - Cross Gateway Patient Discovery Response',  (select id from tf_actor where keyword = 'RESP_GW'), (select id from tf_transaction where keyword = 'ITI-55'), 1, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'MCCI_IN000002UV01', 'XCPD - Accept Acknowledgement',  (select id from tf_actor where keyword = 'RESP_GW'), (select id from tf_transaction where keyword = 'ITI-55'), 1, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'MCCI_IN000002UV01', 'XCPD - Accept Acknowledgement',  (select id from tf_actor where keyword = 'INIT_GW'), (select id from tf_transaction where keyword = 'ITI-55'), 1, 1);
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (nextval('pam_message_id_generator_id_seq'), 0, (select id from tf_actor where keyword = 'RESP_GW'), '1.2.444');
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (nextval('pam_message_id_generator_id_seq'), 0, (select id from tf_actor where keyword = 'INIT_GW'), '1.2.555');


