ALTER TABLE sys_config ALTER COLUMN port DROP NOT NULL ;
ALTER TABLE sys_config ALTER COLUMN ip_address DROP NOT NULL ;
UPDATE sys_config SET hl7_protocol = 1;
UPDATE sys_config SET message_encoding = 1;
UPDATE hl7_message SET message_encoding = 1;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN ip_address DROP NOT NULL;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN listening_port DROP NOT NULL;
UPDATE hl7_simulator_responder_configuration SET message_encoding = 1;
UPDATE hl7_simulator_responder_configuration SET hl7_protocol = 1;
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (13, 'Unknown transaction', 'UNKNOWN', 'UNKNOWN');
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (10, 'Unknown actor', 'UNKNOWN', 'UNKNOWN', false);
-- ids must reflect the content of the database
update pam_patient set blood_group = '278147001' where id < 210;
update pam_patient set blood_group = '278149003' where id >= 210 and id < 230;
update pam_patient set blood_group = null where id >= 210;