update validation_parameters set profile_oid = 'pixmResponse_Json' where message_type = 'Return Corresponding Identifiers JSON';
update validation_parameters set profile_oid = 'pdqmResponse_Json' where message_type = 'Retrieve Patient Resource JSON';
update validation_parameters set profile_oid = 'pdqmResponse_Xml' where message_type = 'Retrieve Patient Resource XML';
update validation_parameters set profile_oid = 'pixmResponse_Xml' where message_type = 'Return Corresponding Identifiers XML';
update validation_parameters set profile_oid = 'pdqmRequest' where message_type = 'Query Patient Resource';
update validation_parameters set profile_oid = 'pixmRequest' where message_type = 'Get Corresponding Identifiers';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'DDS' and type = 'PI') where domain_id is null and full_patient_id like '%DDS%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'IHEPAM' and type = 'PI') where domain_id is null and full_patient_id like '%IHEPAM%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'FR') where domain_id is NULL  and full_patient_id LIKE '%1.2.250.1.213.1.4.2%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'DK') where domain_id is NULL  and full_patient_id LIKE '%DK%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'BE') where domain_id is NULL  and full_patient_id LIKE '%BE%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'IT') where domain_id is NULL  and full_patient_id LIKE '%IT%';
update pam_patient_identifier set domain_id = (select id from pam_hierarchic_designator where namespace_id = 'IHERED') where domain_id is NULL  and full_patient_id LIKE '%IHERED%';

-- from gazelle-xua-actors 1.0.2
ALTER TABLE xua_log ALTER COLUMN reason TYPE text;