-- validators' names changed in Gazelle HL7 Validator from version 3.2.5
UPDATE validation_parameters SET profile_oid = '[ITI-47] Patient Demographics Query' where profile_oid = 'PDQv3 - Patient Demographics Query';
UPDATE validation_parameters SET profile_oid = '[ITI-47] Patient Demographics Query Response' where profile_oid = 'PDQv3 - Patient Demographics Query Response';
UPDATE validation_parameters SET profile_oid = '[ITI-47] Patient Demographics Query HL7V3 Continuation' where profile_oid = 'PDQv3 - Patient Demographics Query HL7V3 Continuation';
UPDATE validation_parameters SET profile_oid = '[ITI-47] Accept Acknowledgement' where profile_oid = 'PDQv3 - Accept Acknowledgement';
UPDATE validation_parameters SET profile_oid = '[ITI-47] Patient Demographics Query HL7V3 Cancellation' where profile_oid = 'PDQv3 - Patient Demographics Query HL7V3 Cancellation';
UPDATE validation_parameters SET profile_oid = '[ITI-44] Patient Identity Feed HL7V3 - Add Patient Record' where profile_oid = 'PIXV3 - Patient Identity Feed HL7V3 - Add Patient Record';
UPDATE validation_parameters SET profile_oid = '[ITI-44] Patient Identity Feed HL7V3 - Revise Patient Record' where profile_oid = 'PIXV3 - Patient Identity Feed HL7V3 - Revise Patient Record';
UPDATE validation_parameters SET profile_oid = '[ITI-44] Patient Identity Feed HL7V3 - Patient Identity Merge' where profile_oid = 'PIXV3 - Patient Identity Feed HL7V3 - Patient Identity Merge';
UPDATE validation_parameters SET profile_oid = '[ITI-44] Patient Identity Feed HL7V3 - Acknowledgement' where profile_oid = 'PIXV3 - Patient Identity Feed HL7V3 - Acknowledgement';
UPDATE validation_parameters SET profile_oid = '[ITI-45] PIXV3 Query' where profile_oid = 'PIXV3 - PIXV3 Query';
UPDATE validation_parameters SET profile_oid = '[ITI-45] PIXV3 Query Response' where profile_oid = 'PIXV3 - PIXV3 Query Response';
UPDATE validation_parameters SET profile_oid = '[ITI-46] PIXV3 Update Notification' where profile_oid = 'PIXV3 - PIXV3 Update Notification';
UPDATE validation_parameters SET profile_oid = '[ITI-46] PIXV3 Update Notification acknowledgement' where profile_oid = 'PIXV3 - PIXV3 Update Notification acknowledgement';
UPDATE validation_parameters SET profile_oid = '[ITI-55] Cross Gateway Patient Discovery Request' where profile_oid = 'XCPD - Cross Gateway Patient Discovery Request';
UPDATE validation_parameters SET profile_oid = '[ITI-55] Cross Gateway Patient Discovery Request (Deferred option)' where profile_oid = 'XCPD - Cross Gateway Patient Discovery Request (Deferred option)';
UPDATE validation_parameters SET profile_oid = '[ITI-55] Cross Gateway Patient Discovery Response' where profile_oid = 'XCPD - Cross Gateway Patient Discovery Response';
UPDATE validation_parameters SET profile_oid = '[ITI-55] Accept Acknowledgement' where profile_oid = 'XCPD - Accept Acknowledgement';