UPDATE app_configuration SET variable = 'pdqm_pds_server_url' WHERE variable = 'fhir_server_url';
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable = 'pdqm_pds_server_url') , 'pixm_mgr_server_url');
