INSERT INTO app_configuration (id, variable, value) VALUES (33, 'application_namespace_id_type', 'ISO');
SELECT pg_catalog.setval('app_configuration_id_seq', 33, true);

INSERT INTO tf_transaction (id, keyword, name, description) values (8, 'RAD-1', 'Patient Registration', 'Patient Registration');
INSERT INTO tf_transaction (id, keyword, name, description) values (9, 'RAD-12', 'Patient Update', 'Patient Update');
SELECT pg_catalog.setval('tf_transaction_id_seq', 9, true);

INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (8, 'ADT', 'ADT', 'ADT', false);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (9, 'MPI', 'ADT Client', 'MPI, OF, OP, IM, RM', true);
SELECT pg_catalog.setval('tf_actor_id_seq', 9, true);

ALTER TABLE pam_encounter_management_event DROP CONSTRAINT pam_encounter_management_event_event_name_key;
ALTER TABLE pam_encounter_management_event ADD CONSTRAINT pam_encounter_management_event_event_name_page_id_key UNIQUE (event_name, page_id);

INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (15, 'A01', 'A11', null, 'Admission of an in-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (16, 'A04', 'A11', null, 'Registration of an out-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (17, 'A05', 'A38', null, 'Pre-admission of an in-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (18, 'A08', null, null, 'Update Patient Information', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (19, 'A40', null, null, 'Merge patient - Internal ID', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (20, 'A02', 'A12', null, 'Transfer patient', 'IHE_RAD12', true, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (21, 'A03', 'A13', null, 'Discharge patient', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (22, 'A06', null, null, 'Change patient class to inpatient', 'IHE_RAD12', true, false, true);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (23, 'A07', null, null, 'Change patient class to outpatient', 'IHE_RAD12', true, false, false);
SELECT pg_catalog.setval('pam_encounter_management_event_id_seq', 23, true);

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (66, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (67, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (68, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (69, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (70, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.77', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (71, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.79', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (72, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.80', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (73, 'ADT^A11^ADT_A09', '1.3.6.1.4.12559.11.1.1.78', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (74, 'ADT^A38^ADT_A38', '1.3.6.1.4.12559.11.1.1.76', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (75, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.82', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (76, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.87', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (77, 'ADT^A06^ADT_A06', '1.3.6.1.4.12559.11.1.1.88', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (78, 'ADT^A07^ADT_A06', '1.3.6.1.4.12559.11.1.1.81', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (79, 'ADT^A03^ADT_A03', '1.3.6.1.4.12559.11.1.1.84', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (80, 'ADT^A13^ADT_A01', '1.3.6.1.4.12559.11.1.1.86', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (81, 'ADT^A02^ADT_A02', '1.3.6.1.4.12559.11.1.1.83', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (82, 'ADT^A12^ADT_A12', '1.3.6.1.4.12559.11.1.1.85', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (83, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (84, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, 9);

SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 84, true);

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,66);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,67);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,68);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,69);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,70);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,71);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,72);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,73);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,74);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,75);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,76);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,77);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,78);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,79);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,80);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,81);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,82);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,83);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,84);
