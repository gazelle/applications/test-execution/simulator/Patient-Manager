INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/PatientManager/fhir/', 'pdqm_pds_url');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'file:///home/xfs/Dev/PAMConfig/fhirXmlDetailedResult.xsl', 'hl7fhir_xml_validation_xsl_location');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'file:///home/xfs/Dev/PAMConfig/fhirJsonDetailedResult.xsl', 'hl7fhir_json_validation_xsl_location');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/FhirValidator-ejb/gazelleFhirValidationWSService/gazelleFhirValidationWS?wsdl', 'fhir_validation_url');


INSERT INTO public.tf_transaction (id, description, keyword, name) VALUES (18, 'Mobile Patient Identifier Cross-reference Query', 'ITI-83', 'Mobile Patient Identifier Cross-reference Query');
INSERT INTO public.tf_transaction (id, description, keyword, name) VALUES (19, 'Mobile Patient Demographics Query', 'ITI-78', 'Mobile Patient Demographics Query');

INSERT INTO public.usage_metadata (id, action, affinity_id, transaction_id, keyword) VALUES (30, 'PIXm Consumer', 1, 18, 'IHE_PIXm_CONS');
INSERT INTO public.usage_metadata (id, action, affinity_id, transaction_id, keyword) VALUES (31, 'PDQm Consumer', 1, 19, 'IHE_PDQm_CONS');

INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (103, 'Return Corresponding Identifiers JSON', 'PIXm Resp Validator', 7, 18, '', 1, 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (104, 'Retrieve Patient Resource JSON', 'PDQm Resp Validator', 2, 19, '', 1, 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (105, 'Retrieve Patient Resource XML', 'PDQm Resp Validator', 2, 19, '', 1, 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (106, 'Return Corresponding Identifiers XML', 'PIXm Resp Validator', 7, 18, '', 1, 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (107, 'Query Patient Resource', 'PDQm Req Validator', 1, 19, '', 1, 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type) VALUES (108, 'Get Corresponding Identifiers', 'PIXm Req Validator', 6, 18, '', 1, 2);
