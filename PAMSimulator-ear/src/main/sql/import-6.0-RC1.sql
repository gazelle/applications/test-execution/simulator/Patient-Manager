INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (5, 'PAT_IDENTITY_SRC', 'Patient Identity Source', 'Patient Identity Source', false);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (6, 'PAT_IDENTITY_CONSUMER', 'Patient Identity Consumer', 'Patient Identity Consumer', true);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (7, 'PAT_IDENTITY_X_REF_MGR', 'Patient Identity Cross-Reference Manager', 'Patient Identity Cross-Reference Manager', true);
SELECT pg_catalog.setval('tf_actor_id_seq', 7, true);

INSERT INTO tf_transaction (id, keyword, name, description) values (5, 'ITI-8', 'Patient Identity Feed', 'Patient Identity Feed');
INSERT INTO tf_transaction (id, keyword, name, description) values (6, 'ITI-9', 'PIX Query', 'PIX Query');
INSERT INTO tf_transaction (id, keyword, name, description) values (7, 'ITI-10', 'PIX Update notification', 'PIX Update notification');
SELECT pg_catalog.setval('tf_transaction_id_seq', 7, true);

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (44, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (45, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (46, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (47, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (48, 'ADT^A24^ADT_A24', '1.3.6.1.4.12559.11.1.1.41', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (49, 'ADT^A37^ADT_A37', '1.3.6.1.4.12559.11.1.1.37', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (50, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 7, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (51, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.28', 7, 7);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (52, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 6, 7);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (53, 'ACK^A01^ACK', '1.3.6.1.4.12559.11.1.1.147', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (54, 'ACK^A04^ACK', '1.3.6.1.4.12559.11.1.1.148', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (55, 'ACK^A05^ACK', '1.3.6.1.4.12559.11.1.1.149', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (56, 'ACK^A08^ACK', '1.3.6.1.4.12559.11.1.1.150', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (57, 'ACK^A40^ACK', '1.3.6.1.4.12559.11.1.1.151', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (58, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.23', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (59, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.24', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (60, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.25', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (61, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.26', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (62, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.27', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (63, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (64, 'QBP^Q23^QBP_Q21', '1.3.6.1.4.12559.11.1.1.92', 6, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (65, 'RSP^K23^RSP_K23', '1.3.6.1.4.12559.11.1.1.30', 7, 6);
SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 65, true);


INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,44);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,45);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,46);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,47);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,48);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,49);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,50);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,51);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,52);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,53);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,54);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,55);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,56);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,57);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,58);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,59);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,60);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,61);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,62);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,63);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,64);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,65);


INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (10, 'A01', null, null, 'Admission of an in-patient', 'IHE_PIX', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (11, 'A04', null, null, 'Registration of an out-patient', 'IHE_PIX', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (12, 'A08', null, null, 'Update Patient Information', 'IHE_PIX', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (13, 'A40', null, null, 'Merge patient - Internal ID', 'IHE_PIX', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (14, 'A05', null, null, 'Pre-admission of an in-patient', 'IHE_PIX', true, true, null);
SELECT pg_catalog.setval('pam_encounter_management_event_id_seq', 14, true);