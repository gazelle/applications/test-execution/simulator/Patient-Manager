INSERT INTO app_configuration (id, variable, value) VALUES (31, 'time_zone', 'CET');

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,14);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,15);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,16);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,17);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,18);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,19);

UPDATE pam_encounter_management_event SET available = true WHERE insert_trigger_event = 'A02';
UPDATE pam_encounter_management_event SET available = true WHERE insert_trigger_event = 'A05';
UPDATE pam_encounter_management_event SET available = true WHERE insert_trigger_event = 'A06';
UPDATE pam_encounter_management_event SET available = true WHERE insert_trigger_event = 'A07';

