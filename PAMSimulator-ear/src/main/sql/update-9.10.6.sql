ALTER TABLE pam_simulator_feature ADD COLUMN new_keyword VARCHAR(64);
UPDATE pam_simulator_feature SET new_keyword = 'EPD' where keyword = 3;
UPDATE pam_simulator_feature SET new_keyword = 'ADT' where keyword = 4;
UPDATE pam_simulator_feature SET new_keyword = 'PAM' where keyword = 5;
UPDATE pam_simulator_feature SET new_keyword = 'PAM_AUTOMATON' where keyword = 6;
UPDATE pam_simulator_feature SET new_keyword = 'PIX' where keyword = 7;
UPDATE pam_simulator_feature SET new_keyword = 'PIXV3' where keyword = 8;
UPDATE pam_simulator_feature SET new_keyword = 'PIXM' where keyword = 9;
UPDATE pam_simulator_feature SET new_keyword = 'PDQ' where keyword = 10;
UPDATE pam_simulator_feature SET new_keyword = 'PDQV3' where keyword = 11;
UPDATE pam_simulator_feature SET new_keyword = 'PDQM' where keyword = 12;
UPDATE pam_simulator_feature SET new_keyword = 'XCPD' where keyword = 13;
UPDATE pam_simulator_feature SET new_keyword = 'XUA' where keyword = 14;
UPDATE pam_simulator_feature SET new_keyword = 'IHE' where keyword = 15;
UPDATE pam_simulator_feature SET new_keyword = 'BP6' where keyword = 16;
UPDATE pam_simulator_feature SET new_keyword = 'SEQUOIA' where keyword = 17;
UPDATE pam_simulator_feature SET new_keyword = 'MOD_WORKLIST' where keyword = 18;
UPDATE pam_simulator_feature SET new_keyword = 'SEHE' where keyword = 19;
UPDATE pam_simulator_feature SET new_keyword = 'CONNECTATHON' where keyword = 20;
ALTER TABLE pam_simulator_feature DROP COLUMN keyword;
ALTER TABLE pam_simulator_feature RENAME new_keyword TO keyword;

DELETE FROM validation_parameters where validator_type = 3;
ALTER TABLE validation_parameters ADD COLUMN new_type VARCHAR(64);
UPDATE validation_parameters SET new_type = 'V2' where validator_type = 0;
UPDATE validation_parameters SET new_type = 'V3' where validator_type = 1;
UPDATE validation_parameters SET new_type = 'FHIR' where validator_type = 2;
ALTER TABLE validation_parameters DROP COLUMN validator_type;
ALTER TABLE validation_parameters RENAME new_type TO validator_type;
UPDATE validation_parameters SET message_type = replace(message_type, ' XML', '') where validator_type = 'FHIR';

UPDATE validation_parameters SET message_type = 'Query Return Corresponding Identifiers' where message_type like 'Return Corresponding Identifiers';
UPDATE validation_parameters SET message_type = 'Query Return Corresponding Identifiers' where message_type like 'Parameters Resource';

UPDATE cmn_message_instance SET type = 'Query Return Corresponding Identifiers' where type like 'Parameters Resource';
UPDATE cmn_message_instance SET type = 'Query Return Corresponding Identifiers' where type = 'Return Corresponding Identifiers';
UPDATE cmn_message_instance SET type = replace(type, ' XML', '') where type like '%XML';
UPDATE cmn_message_instance SET type = replace(type, ' JSON', '') where type like '%JSON';

update cmn_message_instance set type = 'Retrieve Patient Resource Response' where type = 'Retrieve Patient Resource' and issuing_actor = (select id from tf_actor where keyword = 'PDS');


