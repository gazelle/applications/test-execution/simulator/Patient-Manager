# Updating database for Patient Manager

Database management is set to validate. That means that Jboss is not going to create the missing tables, columns ... when you deploy your EAR.
That means that when the changes you brought to the code imply changes in the database, you need to update the schema file and create the update-gss-XXX.sql file.

1. Make sure you already run all the update-*.sql scripts so that you are inline with the last version of the DB schema
1. First, update the PAMSimulator/pom.xml file to change the hibernate.hbm2ddl.auto property to "update".
1. Package and deploy your application locally, the database will be updated
1. Do not forget to change back the property hibernate.hbm2ddl.auto to "validate"
1. Execute pg_dump -s pam-simulator --username=gazelle > schema-{app_version}.sql, it will extract the database scheme for the current version,
1. Perform a diff with schema from the previous version and extract your changes in a file named update-{app_version}.sql
1. Finally, commit both schema-{app_version}.sql and the update-{app_version}.sql files
