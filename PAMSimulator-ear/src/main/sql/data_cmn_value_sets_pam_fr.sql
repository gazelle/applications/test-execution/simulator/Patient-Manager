--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: cmn_value_set; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PD1-2', 'HL70220', 'Living arrangement', '2.16.840.1.113883.2.8.3.3.4');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ROL-2', 'HL70287', 'Problem/Goal action code', '2.16.840.1.113883.2.8.3.3.5');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ROL-3', 'HL70443', 'Provider role', '2.16.840.1.113883.2.8.3.3.6');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'NK1-3', 'HL70063', 'Relationship', '2.16.840.1.113883.2.8.3.3.7');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'NK1-7', 'HL70131', 'Contact role', '2.16.840.1.113883.2.8.3.3.8');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-14', 'HL70023', 'Admit source', '2.16.840.1.113883.2.8.3.3.12');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-16', 'HL70099', 'VIP indicator', '2.16.840.1.113883.2.8.3.3.13');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-21', 'HL70032', 'Charge Price indicator', '2.16.840.1.113883.2.8.3.3.14');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-22', 'HL70045', 'Courtesy code', '2.16.840.1.113883.2.8.3.3.15');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-36', 'HL70112', 'Discharge disposition', '2.16.840.1.113883.2.8.3.3.16');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-41', 'HL70117', 'Account status', '2.16.840.1.113883.2.8.3.3.17');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV2-3', 'HL79110', 'Admit reason (Psychiatry)', '2.16.840.1.113883.2.8.3.3.18');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV2-7', 'HL70130', 'Visit User Code', '2.16.840.1.113883.2.8.3.3.19');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV2-30', 'HL70218', 'Charge adjustment', '2.16.840.1.113883.2.8.3.3.20');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV2-38', 'HL70430', 'Mode of arrival code', '2.16.840.1.113883.2.8.3.3.21');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZBE-9', 'HL79106', 'Nature of movement', '2.16.840.1.113883.2.8.3.3.23');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFA-1', 'HL79101', 'Patient PHR s status', '2.16.840.1.113883.2.8.3.3.24');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFA-4', 'HL79102', 'Valid access authorization to the patient s PHR', '2.16.840.1.113883.2.8.3.3.25');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFA-6', 'HL79103', 'Opposition of the patient to the bris de glace mode access', '2.16.840.1.113883.2.8.3.3.26');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFA-7', 'HL79104', 'Opposition of the patient to the centre 15 mode access', '2.16.840.1.113883.2.8.3.3.27');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFV-6-7', 'HL79105', 'address type', '2.16.840.1.113883.2.8.3.3.28');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFV-10', 'HL73302', 'RIMP Code', '2.16.840.1.113883.2.8.3.3.29');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFM-1', 'HL73303', 'DRG admit mode', '2.16.840.1.113883.2.8.3.3.30');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFM-2', 'HL73304', 'DRG discharge mode', '2.16.840.1.113883.2.8.3.3.31');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFM-3 + ZFM-4 ?', 'HL73305', 'DRG origin and destination mode', '2.16.840.1.113883.2.8.3.3.32');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'IN1-2', 'HL70068', 'Guarantor Type', '2.16.840.1.113883.2.8.3.3.33');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'IN3-5.1', 'HL70146', 'User amount type', '2.16.840.1.113883.2.8.3.3.34');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'OBX-11', 'HL70085', 'Observation status', '2.16.840.1.113883.2.8.3.3.35');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID-5-7', 'HL70200', 'Name Type Code', '2.16.840.1.113883.2.8.3.3.36');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFP-1', 'HL79107', 'Socio-Professional occupation', '2.16.840.1.113883.2.8.3.3.37');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFP-2', 'HL79108', 'Socio-Professional group', '2.16.840.1.113883.2.8.3.3.38');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFD-3', 'HL79109', 'SMS Consent', '2.16.840.1.113883.2.8.3.3.39');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'BED', 'Bed', '1.3.6.1.4.1.21367.101.108');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'OBX', 'BLOOD_GROUP', 'Blood Group (KSA)', '1.3.6.1.4.1.12559.11.10.1.3.1.42.20');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID', 'RACE', 'Race', '1.3.6.1.4.1.21367.101.102');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID', 'RELIGION', 'Religion', '1.3.6.1.4.1.21367.101.122');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'ROOM', 'Room', '1.3.6.1.4.1.21367.101.107');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZBE-7', 'MEDICAL_WARD', 'Medical ward', '2.16.840.1.113883.2.8.3.3.45');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZBE-8', 'NURSING_WARD', 'Nursing ward', '2.16.840.1.113883.2.8.3.3.46');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), '', 'FACILITY_ADDR', 'Facilities addresses', '2.16.840.1.113883.2.8.3.3.42');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'DOCTOR', 'Doctors', '1.3.6.1.4.1.21367.101.110');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'FACILITY', 'Facilities', '1.3.6.1.4.1.21367.101.109');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'HOSPITAL_SERVICE', 'Hospital services', '1.3.6.1.4.1.21367.101.111');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-2', 'HL70004', 'Patient class', '1.3.6.1.4.1.21367.101.104');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1', 'POINT_OF_CARE', 'Point of care', '1.3.6.1.4.1.21367.101.106');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID-16', 'HL70002', 'Marital status', '1.3.6.1.4.1.12559.11.4.2.18');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ACC-2', 'HL70050', 'Accident code', '1.3.6.1.4.1.12559.11.4.2.30');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID-8', 'HL70001', 'Administrative Sex', '1.3.6.1.4.1.21367.101.101');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-4', 'HL70007', 'Admission type', '2.16.840.1.113883.12.7');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PV1-3.5', 'HL70116', 'Bed status', '2.16.840.1.113883.12.116');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID-32', 'HL70445', 'Identity Reliability Code', '2.16.840.1.113883.12.445');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZBE (FR)', 'NURSING_WARD', 'Nursing Ward', '1.3.6.1.4.1.21367.101.111');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZBE (FR)', 'HL79106', 'Nature of movement', '1.3.6.1.4.1.12559.11.4.2.30');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFD-7', 'PROOF_OF_IDENTITY', 'Justificatif d’identité', '2.16.840.1.113883.2.8.1.2.60');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'ZFD-5', 'IDENTITY_ACQUISITION_MODE', 'Mode d’obtention de l’identité' , '2.16.840.1.113883.2.8.1.2.59');


--
-- PostgreSQL database dump complete
--

