INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'ZFD-7', 'PROOF_OF_IDENTITY', 'Justificatif d’identité' , '2.16.840.1.113883.2.8.1.2.60' , true , null);
INSERT INTO cmn_value_set (id , usage, value_set_keyword ,value_set_name, value_set_oid , accessible , last_check)
VALUES (nextval('cmn_value_set_id_seq') , 'ZFD-5', 'IDENTITY_ACQUISITION_MODE', 'Mode d’obtention de l’identité' , '2.16.840.1.113883.2.8.1.2.59' , true , null);

ALTER TABLE pam_fr_additional_demographics ADD COLUMN date_of_birth_on_carte_vitale timestamp without time zone;
ALTER TABLE pam_fr_additional_demographics ADD COLUMN date_of_birth_corrected BOOLEAN;
ALTER TABLE pam_fr_additional_demographics ADD COLUMN identity_acquisition_mode VARCHAR(255);
ALTER TABLE pam_fr_additional_demographics ADD COLUMN proof_of_identity VARCHAR(255);
ALTER TABLE pam_fr_additional_demographics ADD COLUMN document_expiration_date timestamp without time zone;
ALTER TABLE pam_fr_additional_demographics ADD COLUMN date_of_the_insi_webservice_request timestamp without time zone;
