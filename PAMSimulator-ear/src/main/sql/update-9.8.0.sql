delete from validation_parameters where validator_type = 3;
delete from validation_parameters where validator_type = 2;
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Parameters Resource JSON',
        '1.3.6.1.4.1.12559.11.1.2.1.15.3',
        (select id from tf_actor where keyword = 'PAT_IDENTITY_X_REF_MGR'),
        (select id from tf_transaction where keyword = 'ITI-83')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Retrieve Patient Resource',
        '1.3.6.1.4.1.12559.11.1.2.1.15.9',
        (select id from tf_actor where keyword = 'PDC'),
        (select id from tf_transaction where keyword = 'ITI-78')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Retrieve Patient Resource Response XML',
        '1.3.6.1.4.1.12559.11.1.2.1.15.10',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Retrieve Patient Resource Response JSON',
        '1.3.6.1.4.1.12559.11.1.2.1.15.11',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Operation Outcome JSON',
        '1.3.6.1.4.1.12559.11.1.2.1.15.7',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Operation Outcome XML',
        '1.3.6.1.4.1.12559.11.1.2.1.15.6',
        (select id from tf_actor where keyword = 'PAT_IDENTITY_X_REF_MGR'),
        (select id from tf_transaction where keyword = 'ITI-83')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Operation Outcome JSON',
        '1.3.6.1.4.1.12559.11.1.2.1.15.7',
        (select id from tf_actor where keyword = 'PAT_IDENTITY_X_REF_MGR'),
        (select id from tf_transaction where keyword = 'ITI-83')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Operation Outcome XML',
        '1.3.6.1.4.1.12559.11.1.2.1.15.6',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78')
  , '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Query Patient Resource Response JSON',
        '1.3.6.1.4.1.12559.11.1.2.1.15.8',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78'),
        '',
        (select id from tf_domain where keyword = 'ITI'), 3);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Query Patient Resource Response XML',
        '1.3.6.1.4.1.12559.11.1.2.1.15.5',
        (select id from tf_actor where keyword = 'PDS'),
        (select id from tf_transaction where keyword = 'ITI-78'),
        '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Parameters Resource XML',
        '1.3.6.1.4.1.12559.11.1.2.1.15.2',
        (select id from tf_actor where keyword = 'PAT_IDENTITY_X_REF_MGR'),
        (select id from tf_transaction where keyword = 'ITI-83'),
        '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Query Patient Resource',
        '1.3.6.1.4.1.12559.11.1.2.1.15.4',
        (select id from tf_actor where keyword = 'PDC'),
        (select id from tf_transaction where keyword = 'ITI-78'),
        '',
        (select id from tf_domain where keyword = 'ITI'), 2);
INSERT INTO public.validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, java_package, domain_id, validator_type)
VALUES (nextval('ort_validation_parameters_id_seq'),
        'Get Corresponding Identifiers',
        '1.3.6.1.4.1.12559.11.1.2.1.15.1',
        (select id from tf_actor where keyword = 'PAT_IDENTITY_CONSUMER'),
        (select id from tf_transaction where keyword = 'ITI-83'),
        '',
        (select id from tf_domain where keyword = 'ITI'), 2);

update app_configuration set value='http://localhost:8580/FhirValidator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl' where variable = 'fhir_validation_url';
delete from app_configuration where variable = 'hl7fhir_json_validation_xsl_location';
DELETE FROM app_configuration WHERE variable = 'hl7fhir_xml_validation_xsl_location';