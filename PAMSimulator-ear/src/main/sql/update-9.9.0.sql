INSERT INTO tf_actor (id, keyword, name, description) VALUES (nextval('tf_actor_id_seq'), 'CONNECTATHON', 'Connectathon', 'Fake actor to manage patients to be shared during connectathon');
INSERT INTO pam_simulator_feature(id, enabled, keyword, label) VALUES (nextval('pam_simulator_feature_id_seq'), true, 20, 'Connectathon demographics sharing');
ALTER TABLE pam_hierarchic_designator ADD COLUMN cat_usage boolean;
UPDATE pam_hierarchic_designator set cat_usage = FALSE ;
DELETE FROM app_configuration WHERE variable = 'cas_url';


--
-- Name: pam_systemundertest_preferences; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--
CREATE SEQUENCE pam_systemundertest_preferences_id_seq INCREMENT BY 1 MINVALUE 1 START WITH 1;

CREATE TABLE pam_systemundertest_preferences (
  id integer NOT NULL,
  last_modifier character varying(255),
  last_update timestamp without time zone,
  preferred_message integer,
  system_under_test_id integer
);


ALTER TABLE public.pam_systemundertest_preferences OWNER TO gazelle;

--
-- Name: pam_systemundertest_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY pam_systemundertest_preferences
  ADD CONSTRAINT pam_systemundertest_preferences_pkey PRIMARY KEY (id);


--
-- Name: uk_fucrqddhxhnlpyp9qa0cwmb2m; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY pam_systemundertest_preferences
  ADD CONSTRAINT uk_fucrqddhxhnlpyp9qa0cwmb2m UNIQUE (system_under_test_id);


--
-- Name: fk_fucrqddhxhnlpyp9qa0cwmb2m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pam_systemundertest_preferences
  ADD CONSTRAINT fk_fucrqddhxhnlpyp9qa0cwmb2m FOREIGN KEY (system_under_test_id) REFERENCES system_configuration(id);



--
-- Name: pam_sut_assigning_authorities; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE pam_sut_assigning_authorities (
  sut_preference_id integer NOT NULL,
  hierarchic_designator_id integer NOT NULL
);


ALTER TABLE public.pam_sut_assigning_authorities OWNER TO gazelle;

--
-- Name: uk_c8t2vy1y139qdktg3n2r1w2o; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY pam_sut_assigning_authorities
  ADD CONSTRAINT uk_c8t2vy1y139qdktg3n2r1w2o UNIQUE (sut_preference_id, hierarchic_designator_id);


--
-- Name: fk_4t73qvea7htvflxrqr9oy4yff; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pam_sut_assigning_authorities
  ADD CONSTRAINT fk_4t73qvea7htvflxrqr9oy4yff FOREIGN KEY (hierarchic_designator_id) REFERENCES pam_hierarchic_designator(id);


--
-- Name: fk_bms3hvo96444l1hh4vajd4cm2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pam_sut_assigning_authorities
  ADD CONSTRAINT fk_bms3hvo96444l1hh4vajd4cm2 FOREIGN KEY (sut_preference_id) REFERENCES pam_systemundertest_preferences(id);

ALTER TABLE system_configuration ADD COLUMN owner_company varchar(255);
