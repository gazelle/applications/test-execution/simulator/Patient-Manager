@startuml
hide footbox
title Patient Update (RAD-12)
participant PES as "ADT\n(Patient Manager)" #99FF99
participant PEC as "MPI, DSS, OP, IM, RM\n(SUT)"
PES -> PEC: ADT^A06^ADT_A06
activate PEC
PEC --> PES: ACK
deactivate PEC
@enduml
