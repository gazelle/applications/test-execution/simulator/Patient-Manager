@startuml
hide footbox
title PIX Patient Cross-Reference Manager
participant CONSUMER as "PAT_IDENTITY_CONSUMER\n(SUT)" 
participant SRC as "PAT_IDENTITY_SRC\n(SUT)"
participant MGR as "PAT_IDENTITY_X_REF_MGR\n(PatientManager)" #99FF99
group Patient Identity Feed (ITI-8)
SRC -> MGR: ADT^* (A01, A04, A05, A08, A40)
activate MGR
MGR -> SRC: ACK
deactivate MGR
end 
group Patient Identity Management (ITI-30)
SRC -> MGR: ADT^* (A28, A31, A40, A24, A37, A47)
activate MGR
MGR -> SRC: ACK
deactivate MGR
end
group PIX Query (ITI-9)
CONSUMER -> MGR: QBP^Q23^QBP_Q21
activate MGR
MGR -> CONSUMER: RSP^K23^RSP_K23
deactivate MGR
end
@enduml
