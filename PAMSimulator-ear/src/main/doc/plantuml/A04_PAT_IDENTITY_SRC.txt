@startuml
hide footbox
title Patient Identification Feed (ITI-8)
participant SRC as "PAT_IDENTITY_SRC\n(Patient Manager)" #99FF99
participant MGR as "PAT_IDENTITY_X_REF_MGR\n(SUT)"
SRC -> MGR: ADT^A04^ADT_A01
activate MGR
MGR --> SRC: ACK
deactivate MGR
@enduml
