@startuml
hide footbox
title Patient Encounter Management (ITI-31)
participant PES as "PES\n(Patient Manager)" #99FF99
participant PEC as "PEC\n(SUT)"
PES -> PEC: ADT^A55^ADT_A52
activate PEC
PEC --> PES: ACK
deactivate PEC
@enduml
