@startuml
hide footbox
title Patient Demographics Query HL7v3
participant PDC as "PDC\n(Patient Manager)" #99FF99
participant PDS as "PDS\n(SUT)"

PDC -> PDS: Patient Registry Find Candidates Query (PRPA_IN201305UV02)
activate PDS
PDS --> PDC: Patient Registry Find Candidates Query Response (PRPA_IN201306UV02)
deactivate PDS
PDC -> PDS: General Query Activate Query Continue (QUQI_IN000003UV01)
activate PDS
PDS --> PDC: Patient Registry Find Candidates Query Response (PRPA_IN201306UV02)
deactivate PDS
PDC -> PDS: General Query Activate Query Cancel (QUQI_IN000003UV01_Cancel)
activate PDS
PDS --> PDC: Accept Acknowledgment (MCCI_IN000002UV01)
deactivate PDS
@enduml
