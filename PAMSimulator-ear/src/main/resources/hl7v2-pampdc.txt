MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20171016103945||ADT^A28^ADT_A05|hl7v2pampdcbis1|P|2.5||||||UNICODE UTF-8
EVN||20171016103945
PID|||12223^^^AATEST&1.2.3.4.5.9999&ISO^PI||Lorsena^Michel^^^^^L||19940525|M|||Fredericiagade^^Holstebro^^7500^DNK|||||||AN6154^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
PV1||N

MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20170508220710||ADT^A31^ADT_A05|hl7v2pampdcbis2|P|2.5||||||UNICODE UTF-8
EVN||20170508220710
PID|||DDS-58268^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Schmidt^Karla^^^^^L|Schmidt^^^^^^M|19460118|F|||Zum Wehr^^Borna^^04552^DEU||^PRN^PH|||||AN6028^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
PV1||N

MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20170406144723||ADT^A47^ADT_A30|hl7v2pampdcbis3|P|2.5||||||UNICODE UTF-8
EVN||20170406144723
PID|||DDS-newid^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||PAMTOOL_IHE-EU_PAT_MGR_2017^Abchange^^^^^L|Schmidt^^^^^^M|19790412|U|||Löhbach^^Halver^^58553^DEU||^PRN^PH|||||AN5925^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
MRG|DDS-58085^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||||||PAMTOOL_IHE-EU_PAT_MGR_2017^Abchange^^^^^L

MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20170125211214||ADT^A37^ADT_A37|hl7v2pampdcbis4|P|2.5||||||UNICODE UTF-8
EVN||20170125211214
PID|||DDS-papone^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^MR||pam_papone^Edwin_Updated^^^^^L|Leroy^^^^^^M|19500212|M||2028-9|East Hibiscus Boulevard^^Melbourne^Florida^32901^USA||^PRN^PH||||PRO|AN5486^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
PID|||DDS-papone^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^MR||pam_papone^Edwin_Updated^^^^^L|Leroy^^^^^^M|19500212|M||2028-9|East Hibiscus Boulevard^^Melbourne^Florida^32901^USA||^PRN^PH||||PRO|AN5486^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N

MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20170313165622||ADT^A24^ADT_A24|hl7v2pampdcbis5|P|2.5||||||UNICODE UTF-8
EVN||20170313165622
PID|||1109139951121758750969^^^FR&1.2.250.1.213.1.4.2&ISO^NH~NW-57357^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||Martin^Fredéric^^^^^L|Martin^^^^^^M|19600514|M|||Boulevard Jules Janin^^Évreux^^27000^FRA||^PRN^PH|||||AN5705^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
PID|||1743345948080173561902^^^FR&1.2.250.1.213.1.4.2&ISO^NH~DDS-57358^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI~10000004^^^MED^PI||Leroy^Zoe^^^^^L|Bernard^^^^^^M|19630821|F|||Allée des Ormes^^Verneuil-sur-Seine^^78480^FRA||^PRN^PH|||||AN5706^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N

MSH|^~\&|HL7Inspector|Local|PAMSimulator|IHE|20170406145233||ADT^A40^ADT_A39|hl7v2pampdcbis6|P|2.5||||||UNICODE UTF-8
EVN||20170406145233
PID|||DDS-newid^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||PAMTOOL_IHE-EU_PAT_MGR_2017^Abchange^^^^^L|Schmidt^^^^^^M|19790412|U|||Löhbach^^Halver^^58553^DEU||^PRN^PH|||||AN5925^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^AN|||||||||||||N
MRG|DDS-58086^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO^PI||||||PamTOOL_IHE-EU_PAT_MGR_2017^Ef^^^^^L
